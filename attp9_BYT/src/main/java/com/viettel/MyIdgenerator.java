package com.viettel;

import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.sys.IdGenerator;

public class MyIdgenerator implements IdGenerator {
    private static ThreadLocal<HttpServletResponse> response = new ThreadLocal<HttpServletResponse>();
    private static AtomicInteger ai = new AtomicInteger();
 
    public String nextComponentUuid(Desktop desktop, Component comp) {
        String number;
        if ((number = (String) desktop.getAttribute("Id_Num")) == null) {
            number = "0";
            desktop.setAttribute("Id_Num", number);
        }
        int i = Integer.parseInt(number);
        i++;// Start from 1
        desktop.setAttribute("Id_Num", String.valueOf(i));
        return "t_" + i;
    }
 
    public String nextDesktopId(Desktop desktop) {
        HttpServletRequest req = (HttpServletRequest)Executions.getCurrent().getNativeRequest();
        String dtid = req.getParameter("tdtid");
        if(dtid!=null){
        }
        return dtid==null?null:dtid;
    }
 
    public String nextPageUuid(Page page) {
        return null;
    }

	@Override
	public String nextComponentUuid(Desktop desktop, Component arg1,
			ComponentInfo arg2) {
		String number;
		 if ((number = (String) desktop.getAttribute("Id_Num")) == null) {
	            number = "0";
	            desktop.setAttribute("Id_Num", number);
	        }
	        int i = Integer.parseInt(number);
	        i++;// Start from 1
	        desktop.setAttribute("Id_Num", String.valueOf(i));
	        return "t_" + i;
	}
}
