/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.base.DAO;

import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Home.Notify;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.NotifyDAOHE;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.sys.BO.Category;
import com.viettel.utils.*;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Filedownload;

/**
 *
 * @author giangpn
 */
public class AttachDAO extends BaseGenericForwardComposer {

    public void saveFileAttach(WordprocessingMLPackage wmp, String fileName, Long objectId, Long objectType, Long attachType) throws IOException {
        //Neu ung dung chua co avatar thi return
        if (wmp == null) {
            return;
        }
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = new Attachs();
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        folderPath += separator + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
        try {
            attach.setAttachPath(folderPath);
            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//van ban den
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                List<Category> cat = catDao.findCategoryByCode(attachType.toString());
                if (cat.size() > 0) {
                    attach.setAttachCode(cat.get(0).getCode());
                    attach.setAttachTypeName(cat.get(0).getName());
                }
            }
            attachDAOHE.saveOrUpdate(attach);
            String path = folderPath + separator + attach.getAttachId();
            attach.setAttachPath(path);
            attachDAOHE.saveOrUpdate(attach);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                fd.mkdirs();
            }
            File f = new File(attach.getFullPathFile());
            if (f.exists()) {
            } else {
                f.createNewFile();
            }

            wmp.save(f);
//            AttachDAO base = new AttachDAO();
//            base.downloadFileAttach(attach);

            //luu file pdf cho nay 
            InputStream is = new FileInputStream(f);
            XWPFDocument document = new XWPFDocument(is);

            // 2) Prepare Pdf options
            PdfOptions options = PdfOptions.create();

            // 3) Convert XWPFDocument to Pdf
            OutputStream out = new FileOutputStream(new File(
                    f.getAbsolutePath() + f.getName() + ".pdf"));
            PdfConverter.getInstance().convert(document, out, options);
            //String filePath = f.getAbsolutePath();
        } catch (IOException | Docx4JException ex) {
            LogUtils.addLogDB(ex);
        } finally {

        }
    }

    public void saveFileAttach(PdfDocxFile pdfDocxFile, String fileName, Long objectId, Long objectType, Long attachType) throws IOException {
        //Neu ung dung chua co avatar thi return
        if (pdfDocxFile == null) {
            return;
        }
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = new Attachs();
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        folderPath += separator + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
        OutputStream outputStream;
        try {
            attach.setAttachPath(folderPath);
            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//giay phep
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                List<Category> cat = catDao.findCategoryByCode(attachType.toString());
                if (cat.size() > 0) {
                    attach.setAttachCode(cat.get(0).getCode());
                    attach.setAttachTypeName(cat.get(0).getName());
                }
            }
            attachDAOHE.saveOrUpdate(attach);
            String path = folderPath + separator + attach.getAttachId();
            attach.setAttachPath(path);
            attachDAOHE.saveOrUpdate(attach);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                fd.mkdirs();
            }
            File f = new File(attach.getFullPathFile());
            if (f.exists()) {
            } else {
                f.createNewFile();
            }

            outputStream = new FileOutputStream(f);
            outputStream.write(pdfDocxFile.getContent());

            outputStream.close();

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        } finally {

        }
    }

    public void saveFileAttach(PdfDocxFile pdfDocxFile, String fileName, Long objectId, Long objectType, Long attachType, Boolean re) throws IOException {
        //Neu ung dung chua co avatar thi return
        if (pdfDocxFile == null) {
            return;
        }
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = null;
        if (re) {
            attach = attachDAOHE.getByObjectIdAndAttachCat(objectId, objectType);
        }

        if (attach == null) {
            attach = new Attachs();
        }
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        folderPath += separator + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
        OutputStream outputStream;
        try {
            attach.setAttachPath(folderPath);
            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//giay phep
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                List<Category> cat = catDao.findCategoryByCode(attachType.toString());
                if (cat.size() > 0) {
                    attach.setAttachCode(cat.get(0).getCode());
                    attach.setAttachTypeName(cat.get(0).getName());
                }
            }
            attachDAOHE.saveOrUpdate(attach);
            String path = folderPath + separator + attach.getAttachId();
            attach.setAttachPath(path);
            attachDAOHE.saveOrUpdate(attach);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                fd.mkdirs();
            }
            File f = new File(attach.getFullPathFile());
            if (f.exists()) {
            } else {
                f.createNewFile();
            }

            outputStream = new FileOutputStream(f);
            outputStream.write(pdfDocxFile.getContent());

            outputStream.close();

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        } finally {

        }
    }

    public void saveFileAttachPdfSign(String fileName, Long objectId, Long objectType, Long attachType) throws IOException {
        String[] attachDes = new String[]{};
        saveFileAttachPdfSign(fileName, objectId, objectType, attachType, attachDes);
    }

    //hieptq update 170315
    public void saveFileAttachPdfSign(String fileName, Long objectId, Long objectType, Long attachType,
            String... attachDes) throws IOException {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = new Attachs();
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String folderPath = rb.getString("signPdf");
        FileUtil.mkdirs(folderPath);
        File afile = new File(fileName);
        try {
            attach.setAttachPath(folderPath);
            String fName = afile.getName();
            fName = fName.substring(1, fName.length());
            attach.setAttachName(fName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//giay phep
            attach.setIsSent(0l);
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                Category cat = catDao.findById(attachType);
                if (cat != null && cat.getCategoryId() != null) {
                    attach.setAttachCode(cat.getCode());
                    attach.setAttachTypeName(cat.getName());
                }
            }
            //linhdx 22/04/2016 them moi attach_dest="VT" decho van thu ky
            if (attachDes != null && attachDes.length > 0) {
                attach.setAttachDes(attachDes[0]);
            }
            attachDAOHE.saveOrUpdate(attach);

//            String path = folderPath + attach.getAttachId() + "\\";
//            attach.setAttachPath(path);
            //  attachDAOHE.saveOrUpdate(attach);
            //xoa file temp
            String pathSignTemp = rb.getString("signTemp");
            File file1 = new File(pathSignTemp);
            for (File file : file1.listFiles()) {
                file.delete();
            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showErrorNotify("Lỗi lưu file đính kèm");
        } finally {

        }
    }

    /**
     * linhdx Create Attach Reject
     *
     * @param fileName
     * @param objectId
     * @param objectType
     * @param attachType
     * @throws IOException
     */
    public void saveFileAttachReject(String fileName, Long objectId, Long objectType, Long attachType) throws IOException {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = new Attachs();

        HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
        String folderPath = request.getRealPath("/WEB-INF/template/");
        FileUtil.mkdirs(folderPath);
        folderPath = folderPath + "\\";
        File afile = new File(fileName);
        try {
            attach.setAttachPath(folderPath);
            String fName = afile.getName();
            fName = fName.substring(1, fName.length());
            attach.setAttachName(fName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//giay phep
            attach.setIsSent(0l);
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                Category cat = catDao.findById(attachType);
                if (cat != null && cat.getCategoryId() != null) {
                    attach.setAttachCode(cat.getCode());
                    attach.setAttachTypeName(cat.getName());
                }
            }
            attachDAOHE.saveOrUpdate(attach);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showErrorNotify("Lỗi lưu file đính kèm");
        } finally {

        }
    }

    public void saveFileAttachPdfAfterSign(String fileName, Long objectId, Long objectType, Long attachType, String attachPath) throws IOException {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = new Attachs();
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)
//        String folderPath = ResourceBundleUtil.getString("dir_upload");
//        String separator = ResourceBundleUtil.getString("separator");
//        folderPath += separator + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
        //String folderPath = ResourceBundleUtil.getString("dir_upload");
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String folderPath = rb.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }

        InputStream inputStream;
        OutputStream outputStream;

        File afile = new File(fileName);
        try {
            attach.setAttachPath(folderPath);
            String fName = afile.getName();
            fName = fName.substring(1, fName.length());
            attach.setAttachName(fName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//giay phep
            attach.setIsSent(0l);
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                Category cat = catDao.findById(attachType);
                if (cat != null) {
                    attach.setAttachCode(cat.getCode());
                    attach.setAttachTypeName(cat.getName());
                }
            }
            attachDAOHE.saveOrUpdate(attach);

            // ngay hien tai
            Date date = new Date();
            if (objectType == Constants.OBJECT_TYPE.COSMETIC_PUBLIC_PROFILE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
            } //template
            else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
            } else if (objectType == Constants.OBJECT_TYPE.COSMETIC_CA_ATTACHMENT) {
                // giangnh20
                String subPathDate = String.format("%d%s%d%s%d", Calendar.getInstance().get(Calendar.YEAR), separator, Calendar.getInstance().get(Calendar.MONTH) + 1, separator, Calendar.getInstance().get(Calendar.DATE));
                folderPath += separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_STR + separator + subPathDate + separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_TYPE_CA + separator + objectId;
            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + dateFormat.format(date) + separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_FILE_TYPE_STR + separator + objectId;
            }
            folderPath += separator + attach.getAttachId();
            attach.setAttachPath(folderPath);
            attachDAOHE.saveOrUpdate(attach);
            File f = new File(attach.getFullPathFile());
            if (!f.exists()) {
                // tao folder
                if (!f.getParentFile().exists()) {
                    f.getParentFile().mkdirs();
                }
                f.createNewFile();
            }
            //save to hard disk and database
            String pathSignTemp = rb.getString("signTemp");
            if ("".equals(attachPath)) {
                inputStream = new FileInputStream(pathSignTemp + separator + fName);
            } else {
                inputStream = new FileInputStream(attachPath + separator + fileName);
            }
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }

            //File aFile = new File(pathSignTemp + separator + fileName);
            //aFile.renameTo(new File(folderPath + separator + fName));
//            String path = folderPath + attach.getAttachId() + "\\";
//            attach.setAttachPath(path);
            //  attachDAOHE.saveOrUpdate(attach);
            //xoa file temp
/*            File file1 = new File(pathSignTemp);
             for (File file : file1.listFiles()) {
             file.delete();
             }*/
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        } finally {

        }
    }

    public void saveFileAttachPDF(WordprocessingMLPackage wmp, String fileName, Long objectId, Long objectType, Long attachType) throws IOException {
        //Neu ung dung chua co avatar thi return
        if (wmp == null) {
            return;
        }
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = new Attachs();
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        try {
            attach.setAttachPath(folderPath);
            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//van ban den
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                List<Category> cat = catDao.findCategoryByCode(attachType.toString());
                if (cat.size() > 0) {
                    attach.setAttachCode(cat.get(0).getCode());
                    attach.setAttachTypeName(cat.get(0).getName());
                }
            }
            attachDAOHE.saveOrUpdate(attach);

            File fd = new File(folderPath);
            if (!fd.exists()) {
                fd.mkdirs();
            }
            File f = new File(attach.getFullPathFile());
            if (f.exists()) {
            } else {
                f.createNewFile();
            }

            wmp.save(f);
            AttachDAO base = new AttachDAO();
            base.downloadFileAttach(attach);

            //luu file pdf cho nay 
            InputStream is = new FileInputStream(f);
            XWPFDocument document = new XWPFDocument(is);

            // 2) Prepare Pdf options
            PdfOptions options = PdfOptions.create();

            // 3) Convert XWPFDocument to Pdf
            OutputStream out = new FileOutputStream(new File(
                    folderPath + f.getName() + ".pdf"));
            PdfConverter.getInstance().convert(document, out, options);
        } catch (IOException | Docx4JException ex) {
            LogUtils.addLogDB(ex);
        } finally {

        }
    }

    public void saveFileAttach(Media media, Long objectId, Long objectType, Long attachType) throws IOException {
        saveFileAttach(media, objectId, objectType, attachType, null);
    }

    public void saveFileAttachDes(Media media, Long objectId, Long objectType, Long attachType, String des) throws IOException {
        saveFileAttachDes(media, objectId, objectType, attachType, des, null);
    }

    public void saveImportFileAttach(Media media, Long objectId, Long objectType, Long attachType) throws IOException {
        saveImportFileAttach(media, objectId, objectType, attachType, null);
    }

    public void saveReportMeetingFileAttach(Media media, Long objectId, Long objectType, Long attachType) throws IOException {
        saveReportMeetingFileAttach(media, objectId, objectType, attachType, null);
    }

    /**
     * @modified by giangnh20
     * @reason: Them call back ho tro lay id attach & duong dan luu file
     * @modifiedDate: 18/03/2015
     * @param media
     * @param objectId
     * @param objectType
     * @param attachType
     * @param callback
     * @throws IOException
     */
    public void saveFileAttach(Media media, Long objectId, Long objectType, Long attachType, AttachmentCallback callback) throws IOException {
        //Neu ung dung chua co avatar thi return
        if (media == null) {
            if (callback != null) {
                callback.onUploadFailed();
            }
            return;
        }
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = new Attachs();
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)

        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }
        String fileName = media.getName();
        //ho so dung chung
//        if (objectType == Constants.OBJECT_TYPE.RAPID_TEST_PUBLIC_PROFILE) {
//            folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
//        } //template
//        else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
//            folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
//        } else {
//            Date date = new Date();
//            folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + date.getDate() + Constants.OBJECT_TYPE_STR.FILES_RAPIDTEST_STR + objectId + fileName;
//        }
        InputStream inputStream;
        OutputStream outputStream;
        try {

            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//van ban den
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                List<Category> cat = catDao.findCategoryByCode(attachType.toString());
                if (cat.size() > 0) {
                    attach.setAttachCode(cat.get(0).getCode());
                    attach.setAttachTypeName(cat.get(0).getName());
                }
            }
            attachDAOHE.saveOrUpdate(attach);
            // ngay hien tai
            Date date = new Date();
            if (objectType == Constants.OBJECT_TYPE.COSMETIC_PUBLIC_PROFILE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
            } //template
            else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
            } else if (objectType == Constants.OBJECT_TYPE.COSMETIC_CA_ATTACHMENT) {
                // giangnh20
                String subPathDate = String.format("%d%s%d%s%d", Calendar.getInstance().get(Calendar.YEAR), separator, Calendar.getInstance().get(Calendar.MONTH) + 1, separator, Calendar.getInstance().get(Calendar.DATE));
                folderPath += separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_STR + separator + subPathDate + separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_TYPE_CA + separator + objectId;
            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + dateFormat.format(date) + separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_FILE_TYPE_STR + separator + objectId;
            }
            folderPath += separator + attach.getAttachId();
            attach.setAttachPath(folderPath);
            attachDAOHE.saveOrUpdate(attach);
            File f = new File(attach.getFullPathFile());
            if (!f.exists()) {
                // tao folder
                if (!f.getParentFile().exists()) {
                    f.getParentFile().mkdirs();
                }
                f.createNewFile();
            }
            //save to hard disk and database
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            if (callback != null) {
                callback.onUploadFinished(attach, getRelativePath(attach.getFullPathFile()));
            }
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            if (callback != null) {
                callback.onUploadFailed();
            }
        } finally {

        }
    }

    public void saveFileAttachDes(Media media, Long objectId, Long objectType, Long attachType, String des, AttachmentCallback callback) throws IOException {
        //Neu ung dung chua co avatar thi return
        if (media == null) {
            if (callback != null) {
                callback.onUploadFailed();
            }
            return;
        }
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = new Attachs();
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)

        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }
        String fileName = media.getName();
        //ho so dung chung
//        if (objectType == Constants.OBJECT_TYPE.RAPID_TEST_PUBLIC_PROFILE) {
//            folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
//        } //template
//        else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
//            folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
//        } else {
//            Date date = new Date();
//            folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + date.getDate() + Constants.OBJECT_TYPE_STR.FILES_RAPIDTEST_STR + objectId + fileName;
//        }
        InputStream inputStream;
        OutputStream outputStream;
        try {

            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//van ban den
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                List<Category> cat = catDao.findCategoryByCode(attachType.toString());
                if (cat.size() > 0) {
                    attach.setAttachCode(cat.get(0).getCode());
                    attach.setAttachTypeName(cat.get(0).getName());
                }
            }
            attach.setAttachDes(des);
            attachDAOHE.saveOrUpdate(attach);
            // ngay hien tai
            Date date = new Date();
            if (objectType == Constants.OBJECT_TYPE.COSMETIC_PUBLIC_PROFILE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
            } //template
            else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
            } else if (objectType == Constants.OBJECT_TYPE.COSMETIC_CA_ATTACHMENT) {
                // giangnh20
                String subPathDate = String.format("%d%s%d%s%d", Calendar.getInstance().get(Calendar.YEAR), separator, Calendar.getInstance().get(Calendar.MONTH) + 1, separator, Calendar.getInstance().get(Calendar.DATE));
                folderPath += separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_STR + separator + subPathDate + separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_TYPE_CA + separator + objectId;
            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + dateFormat.format(date) + separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_FILE_TYPE_STR + separator + objectId;
            }
            folderPath += separator + attach.getAttachId();
            attach.setAttachPath(folderPath);
            attachDAOHE.saveOrUpdate(attach);
            File f = new File(attach.getFullPathFile());
            if (!f.exists()) {
                // tao folder
                if (!f.getParentFile().exists()) {
                    f.getParentFile().mkdirs();
                }
                f.createNewFile();
            }
            //save to hard disk and database
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            if (callback != null) {
                callback.onUploadFinished(attach, getRelativePath(attach.getFullPathFile()));
            }
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            if (callback != null) {
                callback.onUploadFailed();
            }
        } finally {

        }
    }

    public void saveImportFileAttach(Media media, Long objectId, Long objectType, Long attachType, AttachmentCallback callback) throws IOException {
        //Neu ung dung chua co avatar thi return
        if (media == null) {
            if (callback != null) {
                callback.onUploadFailed();
            }
            return;
        }
        Attachs attach = null;
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        if (objectType.equals(Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL)) {
            attach = attachDAOHE.getByObjectIdAndAttachCat(objectId, objectType);
        }
        if (attach == null) {
            attach = new Attachs();
        }
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)

        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }
        String fileName = media.getName();
        fileName = FileUtil.getSafeFileNameAll(fileName);
        //ho so dung chung
//        if (objectType == Constants.OBJECT_TYPE.RAPID_TEST_PUBLIC_PROFILE) {
//            folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
//        } //template
//        else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
//            folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
//        } else {
//            Date date = new Date();
//            folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + date.getDate() + Constants.OBJECT_TYPE_STR.FILES_RAPIDTEST_STR + objectId + fileName;
//        }
        InputStream inputStream;
        OutputStream outputStream;
        try {

            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//van ban den
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                Category cat = catDao.findById(attachType);

                attach.setAttachCode(cat.getCode());
                attach.setAttachTypeName(cat.getName());
            }
            attachDAOHE.saveOrUpdate(attach);
            // ngay hien tai
            Date date = new Date();
            if (objectType == Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
            } //template
            else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
            } else if (objectType == Constants.OBJECT_TYPE.IMPORT_CA_ATTACHMENT) {
                // giangnh20
                String subPathDate = String.format("%d%s%d%s%d", Calendar.getInstance().get(Calendar.YEAR), separator, Calendar.getInstance().get(Calendar.MONTH) + 1, separator, Calendar.getInstance().get(Calendar.DATE));
                folderPath += separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_STR + separator + subPathDate + separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_TYPE_CA + separator + objectId;
            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + dateFormat.format(date) + separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_FILE_TYPE_STR + separator + objectId;
            }
            folderPath += separator + attach.getAttachId();
            attach.setAttachPath(folderPath);
            attachDAOHE.saveOrUpdate(attach);
            File f = new File(attach.getFullPathFile());
            if (!f.exists()) {
                // tao folder
                if (!f.getParentFile().exists()) {
                    f.getParentFile().mkdirs();
                }
                f.createNewFile();
            }
            //save to hard disk and database
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            if (callback != null) {
                callback.onUploadFinished(attach, getRelativePath(attach.getFullPathFile()));
            }
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            if (callback != null) {
                callback.onUploadFailed();
            }
        } finally {

        }
    }

    public void saveReportMeetingFileAttach(Media media, Long objectId, Long objectType, Long attachType, AttachmentCallback callback) throws IOException {
        //Neu ung dung chua co avatar thi return
        if (media == null) {
            if (callback != null) {
                callback.onUploadFailed();
            }
            return;
        }
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        Attachs attach = attachDAOHE.getLastByObjectIdAndType(objectId, attachType);
        if (attach == null) {
            attach = new Attachs();
        }
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)

        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }
        String fileName = media.getName();
        //ho so dung chung
//        if (objectType == Constants.OBJECT_TYPE.RAPID_TEST_PUBLIC_PROFILE) {
//            folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
//        } //template
//        else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
//            folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
//        } else {
//            Date date = new Date();
//            folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + date.getDate() + Constants.OBJECT_TYPE_STR.FILES_RAPIDTEST_STR + objectId + fileName;
//        }
        InputStream inputStream;
        OutputStream outputStream;
        try {

            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//van ban den
            attach.setAttachType(attachType);
            attach.setAttachTypeName("Biên bản họp hội đồng");
            attach.setIsSent(1L);
            attachDAOHE.saveOrUpdate(attach);
            // ngay hien tai
            Date date = new Date();
            if (objectType == Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
            } //template
            else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
            } else if (objectType == Constants.OBJECT_TYPE.BOARD_SECRETARY_CA_ATTACHMENT) {
                // giangnh20
                String subPathDate = String.format("%d%s%d%s%d", Calendar.getInstance().get(Calendar.YEAR), separator, Calendar.getInstance().get(Calendar.MONTH) + 1, separator, Calendar.getInstance().get(Calendar.DATE));
                folderPath += separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_STR + separator + subPathDate + separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_TYPE_CA + separator + objectId;
            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + dateFormat.format(date) + separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_FILE_TYPE_STR + separator + objectId;
            }
            folderPath += separator + attach.getAttachId();
            attach.setAttachPath(folderPath);
            attachDAOHE.saveOrUpdate(attach);
            File f = new File(attach.getFullPathFile());
            if (!f.exists()) {
                // tao folder
                if (!f.getParentFile().exists()) {
                    f.getParentFile().mkdirs();
                }
                f.createNewFile();
            }
            //save to hard disk and database
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            if (callback != null) {
                callback.onUploadFinished(attach, getRelativePath(attach.getFullPathFile()));
            }
            outputStream.close();
            inputStream.close();

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            if (callback != null) {
                callback.onUploadFailed();
            }
        } finally {

        }
    }

    //nghiepnc
    public void saveFileAttach(Media media, Long attachId, Attachs att, Long objectId, Long objectType, Long attachType, String attachDes) throws IOException {
        AttachDAOHE attachDAOHE = new AttachDAOHE();

        Attachs attach = new Attachs();
        if (media == null) {
            try {
                //truong hop sưa
//                  attach.setAttachId(attachId);
//                    attach.setCreatorId(getUserId());
//            attach.setCreatorName(getFullName());
//            attach.setDateCreate(new Date());
//            attach.setModifierId(getUserId());
//            attach.setDateModify(new Date());
//           attach.setAttachCat(objectType);
//           attach.setAttachPath(attachDes);
//            attach.setAttachDes(attachDes);
//            attach.setAttachPath(att.getAttachPath());
//            attach.setAttachName(att.getAttachName());
//             attach.setIsActive(Constants.Status.ACTIVE);
//               attach.setObjectId(objectId);
//            if (attachType != null) {
//                attach.setAttachType(attachType);
//            }
                att.setAttachDes(attachDes);
                att.setModifierId(getUserId());
                att.setDateModify(new Date());

                if (attachType != null) {
                    attach.setAttachType(attachType);
                    CategoryDAOHE catDao = new CategoryDAOHE();
                    List<Category> cat = catDao.findCategoryByCode(attachType.toString());
                    if (cat.size() > 0) {
                        attach.setAttachCode(cat.get(0).getCode());
                        attach.setAttachTypeName(cat.get(0).getName());
                    }
                }

                attachDAOHE.saveOrUpdate(att);

            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            } finally {
            }
            return;
        }

        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        InputStream inputStream;
        OutputStream outputStream;
        try {
            attach.setAttachPath(folderPath);
            attach.setAttachId(attachId);
            attach.setAttachName(media.getName());
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);
            attach.setAttachDes(attachDes);
            if (attachType != null) {
                attach.setAttachType(attachType);
                CategoryDAOHE catDao = new CategoryDAOHE();
                List<Category> cat = catDao.findCategoryByCode(attachType.toString());
                if (cat.size() > 0) {
                    attach.setAttachCode(cat.get(0).getCode());
                    attach.setAttachTypeName(cat.get(0).getName());
                }
            }
            attachDAOHE.saveOrUpdate(attach);
            if (objectType == Constants.OBJECT_TYPE.COSMETIC_PUBLIC_PROFILE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_PUBLIC_PROFILE_STR;
            } //template
            else if (objectType == Constants.OBJECT_TYPE.TEMPLATE) {
                folderPath += separator + Constants.OBJECT_TYPE_STR.TEMPLATE_STR;
            } else {
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy");

                folderPath += separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_DOCUMENT_STR + separator + dateFormat.format(date) + separator + Constants.OBJECT_TYPE_STR.RAPID_TEST_FILE_TYPE_STR + separator + objectId;
            }
            String path = folderPath + separator + attach.getAttachId();
            attach.setAttachPath(path);
            attachDAOHE.saveOrUpdate(attach);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                fd.mkdirs();
            }
            File f = new File(attach.getFullPathFile());
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            //save to hard disk and database
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }

            outputStream.close();

            inputStream.close();

            //String filePath = f.getAbsolutePath();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        } finally {

        }
    }

    /**
     * Tra ve duong dan tuong doi, bo di thu muc upload cau hinh trong file
     * config
     *
     * @param absolutePath
     * @return
     */
    public static String getRelativePath(String absolutePath) {
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }
        if (!folderPath.endsWith(separator)) {
            folderPath += separator;
        }
        if (absolutePath.startsWith(folderPath)) {
            return absolutePath.replace(folderPath, "").trim();
        }
        return absolutePath;
    }

    /**
     * Xoa file
     *
     * @author giangnh20
     * @param path
     * @param absolute
     * @return
     */
    public static boolean removeFile(String path, boolean absolute) {
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }
        if (!absolute) {
            path = folderPath + (path.startsWith("/") ? path : (separator + path));
        }
        try {
            File f = new File(path);
            f.delete();
            return true;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return false;
    }

    /**
     * Xoa file voi duong dan tuong doi
     *
     * @author giangnh20
     * @param path
     * @return
     */
    public static boolean removeFile(String path) {
        return removeFile(path, false);
    }

    public Attachs saveFileAttachNotify(Media media, Notify notify)
            throws IOException {
        // Neu ung dung chua co avatar thi return
        if (media == null) {
            return null;
        }

        AttachDAOHE attachDAOHE = new AttachDAOHE();
        // Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder
        // target)
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        InputStream inputStream;
        OutputStream outputStream;
        Attachs attach = null;
        boolean saveSuccess = true;
        try {
            attach = new Attachs();
            attach.setAttachPath(folderPath);
            String fileName = media.getName();
            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(notify.getNotifyId());
            attach.setCreatorId(getUserId());
            attach.setCreatorName(getFullName());
            attach.setAttachCat(Constants.OBJECT_TYPE.NOTIFY);
            attachDAOHE.saveOrUpdate(attach);

            File fd = new File(folderPath);
            if (!fd.exists()) {
                fd.mkdirs();
            }
            //
            File f = new File(attach.getFullPathFile());

            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            // save to hard disk and database
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            if (attach.getAttachId() != null) {
                NotifyDAOHE notifyDAOHE = new NotifyDAOHE();
                notify.setAttachId(attach.getAttachId());
                notifyDAOHE.saveOrUpdate(notify);
            }

            outputStream.close();

            inputStream.close();

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            saveSuccess = false;

        } finally {

        }
        if (saveSuccess) {
            return attach;
        } else {
            return null;
        }
    }

    public void downloadFileAttach(Attachs curAtt) throws FileNotFoundException {
        if (curAtt != null) {

            String path = curAtt.getFullPathFile();
            File f = new File(path);
            if (f.exists()) {
                File tempFile = FileUtil.createTempFile(f, curAtt.getAttachName());
                Filedownload.save(tempFile, path);
            } else {
                showNotify("File không còn tồn tại trên hệ thống!!");
            }
        } else {
            showNotify("File không còn tồn tại trên hệ thống!");
        }
    }

    public void downloadFile(String fullFilePath, String fileName) throws FileNotFoundException {
        if (fileName != "") {

            String path = fullFilePath;
            File f = new File(path);
            if (f.exists()) {
                File tempFile = FileUtil.createTempFile(f, fileName);
                Filedownload.save(tempFile, path);
            } else {
                showNotify("File không còn tồn tại trên hệ thống!!");
            }
        } else {
            showNotify("File không còn tồn tại trên hệ thống!");
        }
    }

    public String getFileName(String pathFile) {
        String separator = ResourceBundleUtil.getString("separator");
        int pos = pathFile.lastIndexOf(separator);
        if (pos > 0) {
            return pathFile.substring(pos + 1);
        }
        return "";
    }

    public String getFileNameNoExtend(String pathFile) {
        String separator = ResourceBundleUtil.getString("separator");
        int pos = pathFile.lastIndexOf(separator);
        if (pos > 0) {
            int dotPos = pathFile.substring(pos + 1).lastIndexOf(".");
            if (dotPos > 0) {
                return pathFile.substring(pos + 1).substring(0, dotPos);
            }
            return pathFile.substring(pos + 1);
        }
        return "";
    }

}
