/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.base.DAO;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.user.DAO.RoleUserDeptDAOHE;
import com.viettel.core.user.model.DeptNode;
import com.viettel.module.importfood.DAO.IntegratorVMC;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Window;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Textbox;

/**
 *
 * @author HaVM2
 */
public abstract class BaseComposer extends SelectorComposer<Component> implements IntegratorVMC{

    /**
     *
     */
    private static final long serialVersionUID = -2593028752703160467L;
    private Boolean isFileClerk = null;
    
     protected final int maxSearchYear = 50;

    protected Integer year;

    protected String classView;

    public String getClassView() {
        return classView;
    }

    public void setClassView(String classView) {
        this.classView = classView;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    protected Boolean flagSearchLike;

    public Boolean getFlagSearchLike() {
        return flagSearchLike;
    }

    public void setFlagSearchLike(Boolean flagSearchLike) {
        this.flagSearchLike = flagSearchLike;
    }

    protected Long getLongValue(String str) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }
        return Long.parseLong(str);
    }

    protected IntegratorVMC previousViewModelController;
    protected Map<String, IntegratorVMC> childrenVMC;
    private static final Logger LOGGER = Logger.getLogger(BaseComposer.class);

    public Map<String, IntegratorVMC> getChildrenVM() {
        return childrenVMC;
    }

    @Override
    public void handleEvent(String eventName, Object data) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> T getData(String eventName, Class<T> type, Map<String, Object> arguments) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addChildren(String id, IntegratorVMC child) {
        // TODO Auto-generated method stub

    }

    public UserToken getToken() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk;

    }

    public Long getUserId() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getUserId();
    }

    public String getUserFullName() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getUserFullName();
    }

    public String getUserName() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getUserName();
    }

    public Long getDeptId() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getDeptId();
    }

    public String getDeptName() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getDeptName();
    }
    @SuppressWarnings("rawtypes")
    public Window createWindow(String id, String url, Map arg, int mode) {
        Window window;

        if (this.getPage().hasFellow(id)) {
            window = (Window) this.getPage().getFellow(id);
            window.setVisible(true);
        } else {
            Div div = (Div) Path.getComponent("/bodyContent");
            window = (Window) Executions.createComponents(url, div, arg);
            window.setMode(mode);
            switch (mode) {
                case Window.EMBEDDED:
                    window.doEmbedded();
                    break;
                case Window.HIGHLIGHTED:
                    window.doHighlighted();
                    break;
                case Window.MODAL:
                    window.doModal();
                    break;
                case Window.OVERLAPPED:
                    window.doOverlapped();
                    break;
                case Window.POPUP:
                    window.doPopup();
                    break;
            }
        }
        return window;
    }

    public boolean isFileClerk() {
        if (isFileClerk != null) {
            return isFileClerk;
        } else {
            RoleUserDeptDAOHE rolesDAOHE = new RoleUserDeptDAOHE();
            isFileClerk = rolesDAOHE.isFileClerk(getUserId(), getDeptId());
            return isFileClerk;
        }
    }

    public void showNotification(String message, String type, int time) {
        Clients.showNotification(message, type, null, "after_end", time, true);
    }

    public void showNotification(String message, String type) {
        Clients.showNotification(message, type, null, "after_end", 5000, true);
    }

    public void showNotification(String message) {
        Clients.showNotification(message, Constants.Notification.ERROR, null,
                "after_end", 5000, true);
    }

    public void showSuccessNotification(String message) {
        Clients.showNotification(message, Constants.Notification.INFO, null,
                "after_end", 5000, true);
    }

    public void doExpandTree(Collection<Treeitem> ti, List<Long> parentIds,
            Long idSelected, Tree treeDept) {
        for (Treeitem tt : ti) {
            DeptNode dn = (DeptNode) tt.getValue();
            Long deptId = dn.getId();
            if (parentIds.contains(deptId)) {
                if (idSelected.equals(deptId)) {
                    treeDept.setSelectedItem(tt);
                    tt.setOpen(true);
                } else {
                    tt.setOpen(true);
                    doExpandTree(tt.getTreechildren().getItems(), parentIds,
                            idSelected, treeDept);
                }
                break;
            }
        }
    }

    public void addLog(Long actionType, String actionName, Long objectId, Long objectType, String objectTitle) {
        HttpServletRequest req = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
        String ip = req.getHeader("X-FORWARDED-FOR");
        if (ip == null) {
            ip = req.getRemoteAddr();
        }
        LogUtils.addLog(getDeptId(), getUserId(), getUserName(),
                actionType, actionName,
                objectType, objectId, objectType, objectTitle, ip);

    }

    public void addLog(Long actionType, String actionName, Long objectId, Long objectType, String objectTitle, Object obj) {
        HttpServletRequest req = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
        String ip = req.getHeader("X-FORWARDED-FOR");
        if (ip == null) {
            ip = req.getRemoteAddr();
        }
        LogUtils.addLog(getDeptId(), getUserId(), getUserName(),
                actionType, actionName,
                objectType, objectId, objectType, objectTitle, ip, obj);

    }

    public Date getSysDate() {
        return new Date();
    }

    public String convertDateToString(java.util.Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        if (date == null) {
            return "";
        }
        return dateFormat.format(date);
    }
    
    public String getLabelRt(String key) {
        try {
            return ResourceBundleUtil.getString(key, getConfigLanguageFile());
        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLogDB(ex);
        }
        return "";
    }
    
    public  String getLabelCos(String key){
        return getLabelCos(key, "");
    }
    
    public  String getLabelCos(String key, String defaultValue){
        try {
            return ResourceBundleUtil.getString(key,"language_COSMETIC_vi");
        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLogDB(ex);
        }
        return defaultValue;
    }

    /**
    * Override this method to change language file
    * @author giangnh20
    * @return 
    */
    public String getConfigLanguageFile() {
        return "language_XNN_vi";
    }
    
    /**
     * Method get string value from language file
     * 
     * @author giangnh20
     * @param key
     * @param defaultValue
     * @return
     */
    public String getLabelName(String key, String defaultValue) {
        return getString(key, null, defaultValue);
    }

    /**
     * Method get string value from language file
     * 
     * @author giangnh20
     * @param key
     * @return
     */
    public String getLabelName(String key) {
        return getString(key, null, "");
    }

    public String getString(String key, String languageFile, String defaultValue) {
        try {
            if (languageFile == null || "".equals(languageFile)) {
                languageFile = getConfigLanguageFile();
            }
            ResourceBundle bundle = ResourceBundle.getBundle(languageFile);
            return bundle.getString(key);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return defaultValue;
    }
    
    public String getString(String key, String languageFile) {
        return getString(key, languageFile, "");
    }
    
    public String convertDateTimeToString(Object obj) {
        Date date = (Date) obj;
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        if (date != null) {
            return dateFormat.format(date);
        }
        return "";
    }
    
    /**
     * @author giangnh20
     * Ham cat chuoi & them ... vao sau.
     * @param input
     * @param maxLength
     * @param force
     * @return
     */
    public String cutString(String input, int maxLength, Boolean force) {
        StringBuilder sb = new StringBuilder();
        if (input != null && (input.length() > maxLength)) {
            if (force || !input.contains(" ")) {
                sb.append(input.substring(0, maxLength - 1));
            } else {
                int i = maxLength - 1;
                while (input.charAt(i) != ' ' && input.charAt(i) != '.' && input.charAt(i) != ',' && i < input.length() - 1) {
                    i++;
                }
                sb.append(input.substring(0, i));
            }
            sb.append("...");
            return sb.toString();
        }
        return (input == null) ? "" : input;
    }
    
    public String getFormatDate(Date mDate) {
        String sTemp = "";
        if (mDate != null) {
            sTemp = CommonFns.formatDate(mDate, "dd/MM/yyyy");
        }
        return sTemp;

    }
    
    public String cutString(String input, int maxLength) {
        return cutString(input, maxLength, false);
    }
    
    /**
     * Lưu lại evenListener được lắng nghe bởi textbox
     * Khi đóng cửa sổ window phải remove listener này đi tránh duplicate evenlistener.
     **/
    private EventListener currentEvenListener;
    
    /**
     * Hien thi popup danh sach cac ky tu dac biet
     * Cach dung: them <button label="Symbol" onClick="$composer.openSpecialWnd(window, tbBrandName)"/> 
     * vao trong file zul.
     * @author giangnh20
     * @param parentWindow
     * @param textBox 
     */
    public void openSpecialWnd(final Window parentWindow, final Textbox textBox) {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("parentWindow", parentWindow);
        args.put("textBox", textBox);
        createWindow("specialSymbolWnd", "/Pages/module/cosmetic/specialSymbol.zul", args, Window.OVERLAPPED);
        
        if (textBox != null && parentWindow != null) {
            
            textBox.addEventListener("onAddSymbol", currentEvenListener = new EventListener() {

                @Override
                public void onEvent(Event e) throws Exception {
                    if (textBox != null && e.getData() != null) {
                        StringBuilder sb = new StringBuilder(textBox.getValue());
                        sb.append(e.getData().toString());
                        textBox.setValue(sb.toString());
                    }
                }
            });
            
            parentWindow.addEventListener("onSymbolWndClose", new EventListener() {

                @Override
                public void onEvent(Event t) throws Exception {
                    textBox.removeEventListener("onAddSymbol", currentEvenListener);
                }

            });
        }
        
    }
    
    /**
     * Clients Notification Position
     * Reference at http://books.zkoss.org/wiki/ZK_Developer%27s_Reference/UI_Patterns/Useful_Java_Utilities#showNotification
     */
    public enum NOTIFICATION_POSITION {
        TOP_LEFT("top_left"), TOP_CENTER("top_center"),TOP_RIGHT("top_right"), 
        MIDDLE_LEFT("middle_left"), MIDDLE_CENTER("middle_center"), MIDDLE_RIGHT("middle_right"),
        BOTTOM_LEFT("bottom_left"), BOTTOM_CENTER("bottom_center"), BOTTOM_RIGHT("bottom_right");
        
        private final String value;
        
        private NOTIFICATION_POSITION(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    
    /**
     * Lay gia tri Textbox & trim gia tri
     * @param tb
     * @return 
     */
    public String textBoxGetValue(Textbox tb) {
        if (tb != null) {
            try {
                if (tb.getValue() != null) {
                    return tb.getValue().trim();
                }
            } catch(WrongValueException ex) {
                LogUtils.addLogDB(ex);
            }
            return tb.getValue();
        }
        return null;
    }
    
    /**
     * Lay gia tri file properties
     * @author giangnh20
     * @param key
     * @param file
     * @return 
     */
    public String getConfigValue(String key, String file) {
        return getConfigValue(key, file, "");
    }
    
    /**
     * Lay gia tri file properties co defaultValue
     * @author giangnh20
     * @param key
     * @param file
     * @param defaultValue
     * @return 
     */
    public String getConfigValue(String key, String file, String defaultValue) {
        try {
            ResourceBundle rb = ResourceBundle.getBundle(file);
            return rb.getString(key);
        } catch(Exception e) {
            LogUtils.addLogDB(e);
        }
        return defaultValue;
        
    }
    
    /**
     * Ham formatNumber voi pattern
     * ex: ##,###,###.00 se format cac so thanh chuoi co 2 so sau dau phay
     * ex: #.# doi voi so co dinh dang bat ky
     * @author giangnh20
     * @param number
     * @param pattern
     * @param dotToComma
     * @return 
     */
    public String formatNumber(Number number, String pattern, Boolean dotToComma) {
        return StringUtils.formatNumber(pattern, number, dotToComma);
    }
    
    public String formatNumber(Number number, String pattern) {
        return StringUtils.formatNumber(pattern, number, true);
    }
    
    /**
     * Helper call replace string in zul file
     * @author giangnh20
     * @param input
     * @param target
     * @param replacement
     * @return 
     */
    public String replaceString(String input, String target, String replacement) {
        if (input != null && !"".equals(input) && target != null && !"".equals(target) && replacement != null) {
            return input.replace(target, replacement);
        }
        return input;
    }
    
    protected String label(String key) {
        return Labels.getLabel(key);
    }
}
