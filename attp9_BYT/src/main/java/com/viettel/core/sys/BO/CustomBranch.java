/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.core.sys.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "CUSTOM_BRANCH")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomBranch.findAll", query = "SELECT c FROM CustomBranch c"),
    @NamedQuery(name = "CustomBranch.findById", query = "SELECT c FROM CustomBranch c WHERE c.id = :id"),
    @NamedQuery(name = "CustomBranch.findByName", query = "SELECT c FROM CustomBranch c WHERE c.name = :name"),
    @NamedQuery(name = "CustomBranch.findByCode", query = "SELECT c FROM CustomBranch c WHERE c.code = :code"),
    @NamedQuery(name = "CustomBranch.findByProvinceCode", query = "SELECT c FROM CustomBranch c WHERE c.provinceCode = :provinceCode"),
    @NamedQuery(name = "CustomBranch.findByCreatedBy", query = "SELECT c FROM CustomBranch c WHERE c.createdBy = :createdBy"),
    @NamedQuery(name = "CustomBranch.findByCreatedDate", query = "SELECT c FROM CustomBranch c WHERE c.createdDate = :createdDate"),
    @NamedQuery(name = "CustomBranch.findByIsActive", query = "SELECT c FROM CustomBranch c WHERE c.isActive = :isActive")})
public class CustomBranch implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "CUSTOM_BRANCH_SEQ", sequenceName = "CUSTOM_BRANCH_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUSTOM_BRANCH_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 400)
    @Column(name = "NAME")
    private String name;
    @Size(max = 100)
    @Column(name = "CODE")
    private String code;
    @Size(max = 100)
    @Column(name = "PROVINCE_CODE")
    private String provinceCode;
    @Size(max = 20)
    @Column(name = "CREATED_BY")
    private String createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    @Column(name = "IS_ACTIVE")
    private Long isActive;

    public CustomBranch() {
    }

    public CustomBranch(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomBranch)) {
            return false;
        }
        CustomBranch other = (CustomBranch) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.core.sys.BO.CustomBranch[ id=" + id + " ]";
    }
    
}
