/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.core.sys.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "PRODUCT_TARGET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductTarget.findAll", query = "SELECT p FROM ProductTarget p"),
    @NamedQuery(name = "ProductTarget.findById", query = "SELECT p FROM ProductTarget p WHERE p.id = :id"),
    @NamedQuery(name = "ProductTarget.findByCode", query = "SELECT p FROM ProductTarget p WHERE p.code = :code"),
    @NamedQuery(name = "ProductTarget.findByName", query = "SELECT p FROM ProductTarget p WHERE p.name = :name"),
    @NamedQuery(name = "ProductTarget.findByCost", query = "SELECT p FROM ProductTarget p WHERE p.cost = :cost"),
    @NamedQuery(name = "ProductTarget.findByIsActive", query = "SELECT p FROM ProductTarget p WHERE p.isActive = :isActive"),
    @NamedQuery(name = "ProductTarget.findByCreatedDate", query = "SELECT p FROM ProductTarget p WHERE p.createdDate = :createdDate"),
    @NamedQuery(name = "ProductTarget.findByCreatedBy", query = "SELECT p FROM ProductTarget p WHERE p.createdBy = :createdBy"),
    @NamedQuery(name = "ProductTarget.findByType", query = "SELECT p FROM ProductTarget p WHERE p.type = :type"),
    @NamedQuery(name = "ProductTarget.findByTestMethod", query = "SELECT p FROM ProductTarget p WHERE p.testMethod = :testMethod"),
    @NamedQuery(name = "ProductTarget.findByTypeName", query = "SELECT p FROM ProductTarget p WHERE p.typeName = :typeName")})
public class ProductTarget implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "PRODUCT_TARGET_SEQ", sequenceName = "PRODUCT_TARGET_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCT_TARGET_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 200)
    @Column(name = "CODE")
    private String code;
    @Size(max = 2000)
    @Column(name = "NAME")
    private String name;
    @Column(name = "COST")
    private Long cost;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    @Column(name = "CREATED_BY")
    private Long createdBy;
    @Size(max = 100)
    @Column(name = "TYPE")
    private String type;
    @Size(max = 2000)
    @Column(name = "TEST_METHOD")
    private String testMethod;
    @Size(max = 400)
    @Column(name = "TYPE_NAME")
    private String typeName;

    public ProductTarget() {
    }

    public ProductTarget(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTestMethod() {
        return testMethod;
    }

    public void setTestMethod(String testMethod) {
        this.testMethod = testMethod;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductTarget)) {
            return false;
        }
        ProductTarget other = (ProductTarget) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.core.sys.BO.ProductTarget[ id=" + id + " ]";
    }
    
}
