/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.ErrorLogDAOHE;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.ErrorLog;
import java.io.IOException;
import java.util.Date;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

/**
 *
 * @author linhdx
 */
public class ErrorLogController extends BaseComposer {

    @Wire
    Textbox txtUserName, txtObjectTitle;
    ListModelArray lstUser;
    @Wire
    Listbox lstItems, lbModun, lbActionType;
    @Wire
    Datebox dbFromDate, dbToDate;
    @Wire
    Paging userPagingBottom;
    ErrorLog searchForm;
    Long userId;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        userPagingBottom.setActivePage(0); 	
        if (userId == null) {
            dbFromDate.setValue(new Date());
            dbToDate.setValue(new Date());
        }
        onSearch();
    }

    private void fillDataToList() {
        ErrorLogDAOHE ldhe = new ErrorLogDAOHE();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = ldhe.searchLog(searchForm, dbFromDate.getValue(), dbToDate.getValue(), start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstItems.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() throws IOException {userPagingBottom.setActivePage(0);
        fillDataToList();
    }

  
}
