/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.utils.Constants;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.module.rapidtest.BO.Template;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class TemplateCreateController extends BaseComposer {

    @Wire
    Textbox txtId;
    @Wire
    Textbox txtTemplateName, txtDeptName, txtDeptId;
    @Wire
    Listbox lbProcedure, lbTemplateType;
    @Wire
    Window templateCreateWnd;
    Window showDeptDlg;
    private Attachs attachCurrent;
    @Wire
    private Vlayout flist;
    private List<Media> listMedia;
    // Attach file
    private List<Attachs> listFileAttach;

    public TemplateCreateController() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        CategoryDAOHE cdhe = new CategoryDAOHE();
        List lstProcedure = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.PROCEDURE);
        ListModelArray lstModelProcedure = new ListModelArray(lstProcedure);
        lbProcedure.setModel(lstModelProcedure);
        lbProcedure.renderAll();
        
        List lstTemplateType = cdhe.findAllCategoryStatic(Constants.CATEGORY_TYPE.PROCEDURE_TEMPLATE_TYPE);
        ListModelArray lstModelTemplateType = new ListModelArray(lstTemplateType);
        lbTemplateType.setModel(lstModelTemplateType);
        lbTemplateType.renderAll();
        
        loadInfoToForm();
        listMedia = new ArrayList<>();

    }

    public void loadInfoToForm() {
        Long id = (Long) Executions.getCurrent().getArg().get("id");
        if (id != null) {
            TemplateDAOHE objhe = new TemplateDAOHE();
            Template rs = objhe.findById(id);
            if (rs.getTemplateId() != null) {
                txtId.setValue(rs.getTemplateId().toString());
            }
            if (rs.getTemplateName() != null) {
                txtTemplateName.setValue(rs.getTemplateName());
            }
            if (rs.getDeptId() != null) {
                txtDeptId.setValue(rs.getDeptId().toString());
            }
            if (rs.getDeptName() != null) {
                txtDeptName.setValue(rs.getDeptName());
            }

            if (rs.getProcedureId() != null) {
                for (int i = 0; i < lbProcedure.getListModel().getSize(); i++) {
                    Category ct = (Category) lbProcedure.getListModel().getElementAt(i);
                    if (rs.getProcedureId().equals(ct.getCategoryId())) {
                        lbProcedure.setSelectedIndex(i);
                        break;
                    }
                }
            }
            
            if (rs.getTemplateTypeId()!= null) {
                for (int i = 0; i < lbTemplateType.getListModel().getSize(); i++) {
                    Category ct = (Category) lbTemplateType.getListModel().getElementAt(i);
                    if (rs.getTemplateTypeId().equals(ct.getCategoryId())) {
                        lbTemplateType.setSelectedIndex(i);
                        break; 
                    }
                }
            }

            AttachDAOHE attachDAOHE = new AttachDAOHE();
            List<Attachs> lstAtt = attachDAOHE.getByObjectIdAndType(id, Constants.OBJECT_TYPE.TEMPLATE);
            if (lstAtt.size() > 0) {
                Attachs att = lstAtt.get(0);
                setAttachCurrent(att);
            }

            AttachDAOHE attachsDAOHE = new AttachDAOHE();
            listFileAttach = attachsDAOHE.getByObjectId(id, Constants.OBJECT_TYPE.TEMPLATE);
            loadFileAttach(listFileAttach);

        } else {
            txtId.setValue("");
            txtTemplateName.setValue("");
            txtDeptId.setValue("");
            txtDeptName.setValue("");
            lbProcedure.setSelectedIndex(0);
            lbTemplateType.setSelectedIndex(0);
            listFileAttach = new ArrayList();
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() throws IOException {
        Template rs = new Template();
        if (txtId.getValue() != null && !txtId.getValue().isEmpty()) {
            rs.setTemplateId(Long.parseLong(txtId.getValue()));
        }
        if (txtTemplateName.getValue() != null && txtTemplateName.getValue().trim().length() == 0) {
            showNotification("Tên biểu mẫu không thể để trống", Constants.Notification.ERROR);
            txtTemplateName.focus();
            return;
        } else {
            rs.setTemplateName(txtTemplateName.getValue());
        }
        if (lbProcedure.getSelectedItem() != null) {
            int idx = lbProcedure.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setProcedureId((Long) lbProcedure.getSelectedItem().getValue());
                rs.setProcedureName(lbProcedure.getSelectedItem().getLabel());
            } else {
                showNotification("Thủ tục không thể để trống", Constants.Notification.ERROR);
                lbProcedure.focus();
                return;
            }
        }
        if (lbTemplateType.getSelectedItem() != null) {
            int idx = lbTemplateType.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setTemplateTypeId((Long) lbTemplateType.getSelectedItem().getValue());
                rs.setTemplateTypeName(lbTemplateType.getSelectedItem().getLabel());
            } else {
                showNotification("Loại biểu mẫu trong hồ sơ không thể để trống", Constants.Notification.ERROR);
                lbTemplateType.focus();
                return;
            }
        }

        if (txtDeptId.getValue() != null && txtDeptId.getValue().trim().length() == 0) {
            showNotification("Đơn vị không thể để trống", Constants.Notification.ERROR);
            txtDeptId.focus();
            return;
        } else {
            rs.setDeptId(Long.parseLong(txtDeptId.getValue()));
        }

        if (txtDeptName.getValue() != null && txtDeptName.getValue().trim().length() == 0) {
            showNotification("Đơn vị không thể để trống", Constants.Notification.ERROR);
            txtDeptName.focus();
            return;
        } else {
            rs.setDeptName(txtDeptName.getValue());
        }

        rs.setIsActive(1L);
        TemplateDAOHE rdhe = new TemplateDAOHE();
        if (rdhe.hasDuplicate(rs)) {
            showNotification("Trùng dữ liệu đã tạo trên hệ thống");
            return;
        }
        rdhe.saveOrUpdate(rs);
        

        if (listMedia != null) {
            AttachDAO attDAO = new AttachDAO();
            for (Media media : listMedia) {
                attDAO.saveFileAttach(media, rs.getTemplateId(), Constants.OBJECT_TYPE.TEMPLATE, null);
            }

        }

        if (txtId.getValue() != null && !txtId.getValue().isEmpty()) {
            //update thì detach window
            templateCreateWnd.detach();
        } else {
            // them moi thi clear window
            loadInfoToForm();
        }
        showNotification("Lưu thành công", Constants.Notification.INFO);

        Window parentWnd = (Window) Path.getComponent("/templateManageWnd");
        Events.sendEvent(new Event("onReload", parentWnd, null));
    }

    @Listen("onClick=#btnShowDept")
    public void showDept() {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("idOfDisplayNameComp", "/templateCreateWnd/txtDeptName");
        args.put("idOfDisplayIdComp", "/templateCreateWnd/txtDeptId");
        showDeptDlg = (Window) Executions.createComponents("/Pages/admin/user/userDept.zul", null, args);
        showDeptDlg.doModal();
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }
            // luu file vao danh sach file
            listMedia.add(media);
            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listMedia.remove(media);
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    /**
     * Load danh sach cac file dinh kem cua van ban den va hien thi tren giao
     * dien
     *
     * @param listFileAttach
     */
    private void loadFileAttach(List<Attachs> listFileAttach) {
        if (listFileAttach != null) {
            for (final Attachs attach : listFileAttach) {
                // layout hien thi ten file va nut "Xoa"
                final Hlayout hl = new Hlayout();
                hl.setSpacing("6px");
                hl.setClass("newFile");
                hl.appendChild(new Label(attach.getAttachName()));
                A rm = new A("Xóa");
                rm.addEventListener(Events.ON_CLICK,
                        new org.zkoss.zk.ui.event.EventListener() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                hl.detach();
                                // Set attach deactive
                                attach.setIsActive(Constants.Status.INACTIVE);
                                AttachDAOHE daoHE = new AttachDAOHE();
                                daoHE.saveOrUpdate(attach);
                            }
                        });
                hl.appendChild(rm);
                flist.appendChild(hl);
            }
        }
    }

    public Attachs getAttachCurrent() {
        return attachCurrent;
    }

    public void setAttachCurrent(Attachs attachCurrent) {
        this.attachCurrent = attachCurrent;
    }

}
