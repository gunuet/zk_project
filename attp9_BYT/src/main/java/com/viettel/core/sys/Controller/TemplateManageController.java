/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.utils.Constants;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.ProductTarget;
import com.viettel.module.rapidtest.BO.Template;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.module.importOrder.DAO.ProductTargetDAO;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.File;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class TemplateManageController extends BaseComposer {

    //Control tim kiem
    @Wire
    Listbox lbProcedure;
    @Wire
    Textbox txtTemplateName, txtDeptName, txtDeptId;

    //Danh sach
    @Wire
    Listbox lbTemplate;

    Window showDeptDlg;
    @Wire
    Paging userPagingBottom;
    Template searchForm;

    @Override
    public void doAfterCompose(Component window) {
        try {
            super.doAfterCompose(window);

            CategoryDAOHE cdhe = new CategoryDAOHE();
            List lstProcedure = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.PROCEDURE);
            ListModelArray lstModelProcedure = new ListModelArray(lstProcedure);
            lbProcedure.setModel(lstModelProcedure);
            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {
            userPagingBottom.setActivePage(0);
            searchForm = new Template();
            if (txtTemplateName.getValue() != null && !"".equals(txtTemplateName.getValue())) {
                searchForm.setTemplateName(txtTemplateName.getValue());
            }
            if (lbProcedure.getSelectedItem() != null) {
                Long procedureId = (Long) lbProcedure.getSelectedItem().getValue();
                searchForm.setProcedureId(procedureId);
            }
            if (txtDeptId.getValue() != null && !"".equals(txtDeptId.getValue())) {
                Long deptId = Long.valueOf(txtDeptId.getValue());
                searchForm.setDeptId(deptId);
            }
            //Fill danh sach loai danh muc
            fillDataToList();
        } catch (WrongValueException | NumberFormatException ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    @Listen("onReload=#templateManageWnd")
    public void onReload() {
        onSearch();
    }

    private void fillDataToList() {
        TemplateDAOHE objhe = new TemplateDAOHE();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbTemplate.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException {
        Window window = (Window) Executions.createComponents(
                "/Pages/admin/template/templateCreate.zul", null, null);
        window.doModal();
    }

    @Listen("onEdit=#lbTemplate")
    public void onUpdate(Event ev) throws IOException {
        Template pfs = (Template) lbTemplate.getSelectedItem().getValue();
        Map args = new ConcurrentHashMap();
        args.put("id", pfs.getTemplateId());
        Window window = (Window) Executions.createComponents("/Pages/admin/template/templateCreate.zul", null, args);
        window.doModal();
    }

    @Listen("onDownloadFile=#lbTemplate")
    public void onDownloadFile(Event ev) throws IOException {
        Template pfs = (Template) lbTemplate.getSelectedItem().getValue();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> lstAttach = attDAOHE.getByObjectIdAndType(pfs.getTemplateId(), Constants.OBJECT_TYPE.TEMPLATE);
        if (lstAttach != null && lstAttach.size() > 0) {
            Attachs att = lstAttach.get(0);
            AttachDAO attDAO = new AttachDAO();
            attDAO.downloadFileAttach(att);
        }

    }

    @Listen("onDelete=#lbTemplate")
    public void onDelete(Event ev) throws IOException {
        Messagebox.show("Bạn có chắc chắn muốn xóa không?", "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    try {
                                        Template rs = (Template) lbTemplate.getSelectedItem().getValue();
                                        TemplateDAOHE daohe = new TemplateDAOHE();
                                        daohe.delete(rs.getTemplateId());
                                        onSearch();
                                        // onSearch();
                                    } catch (Exception ex) {
                                        LogUtils.addLogDB(ex);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });

    }

    @Listen("onClick=#btnShowDept")
    public void showDept() {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("idOfDisplayNameComp", "/templateManageWnd/txtDeptName");
        args.put("idOfDisplayIdComp", "/templateManageWnd/txtDeptId");
        showDeptDlg = (Window) Executions.createComponents("/Pages/admin/user/userDept.zul", null, args);
        showDeptDlg.doModal();
    }

}
