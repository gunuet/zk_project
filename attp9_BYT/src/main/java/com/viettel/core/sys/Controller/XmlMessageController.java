/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this nation file, choose Tools | Nations
 * and open the nation in the editor.
 */
package com.viettel.core.sys.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.XmlMessage;
import com.viettel.core.sys.DAO.PlaceDAOHE;
import com.viettel.core.sys.DAO.XmlMessageDAOHE;
import com.viettel.utils.Constants;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
@SuppressWarnings("serial")
public class XmlMessageController extends BaseComposer {

    @Wire
    Textbox txtFunc, txtType, txtNswFilecode, txtContentMessage;

    @Wire
    Listbox lbMessage;
    @Wire
    Paging userPagingBottom;
    XmlMessage searchForm;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        onSearch();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	
        searchForm = new XmlMessage();
        if (txtFunc.getValue() != null && !"".equals(txtFunc.getValue())) {
            searchForm.setFunc(txtFunc.getValue());
        }
        if (txtType.getValue() != null && !"".equals(txtType.getValue())) {
            searchForm.setType(txtType.getValue());
        }
        if (txtNswFilecode.getValue() != null && !"".equals(txtNswFilecode.getValue())) {
            searchForm.setNswFileCode(txtNswFilecode.getValue());
        }

        fillDataToList();
    }

    @Listen("onReload=#wnd")
    public void onReload() {
        onSearch();
    }

    private void fillDataToList() {
    	XmlMessageDAOHE objhe = new XmlMessageDAOHE();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        @SuppressWarnings({ "rawtypes", "unchecked" })
		ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbMessage.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }



    @Listen("onView=#lbMessage")
    public void onUpdate(Event ev) throws IOException {
    	XmlMessage obj = (XmlMessage) lbMessage.getSelectedItem().getValue();
    	txtContentMessage.setValue(obj.getMessage());
    }



}
