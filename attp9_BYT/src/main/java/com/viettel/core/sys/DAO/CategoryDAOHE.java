/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.core.sys.model.CategorySearchForm;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.BO.CategoryType;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.base.model.TreeItem;
import com.viettel.utils.Constants_XNN;
import com.viettel.voffice.BO.Files;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Administrator
 */
public class CategoryDAOHE extends GenericDAOHibernate<Category, Long> {

    public CategoryDAOHE() {
        super(Category.class);
    }

    @Override
    public void saveOrUpdate(Category o) {
        if (o != null) {
            super.saveOrUpdate(o);
            getSession().flush();
        }
    }

    @Override
    public Category findById(Long id) {
        Query query = getSession().getNamedQuery("Category.findByCategoryId");
        query.setParameter("categoryId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Category) result.get(0);
        }
    }

    public Category findCategoryByValue(String typecode, String value) {
        Query query = getSession().createQuery("Select c from Category c where c.value = :value AND c.categoryTypeCode = :typecode AND c.isActive = 1 ");
        query.setParameter("typecode", typecode);
        query.setParameter("value", value);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Category) result.get(0);
        }
    }

    public Category findCategory(String code, String type) {
        Query query = getSession().createQuery("Select c from Category c where c.code = :code AND c.categoryTypeCode = :type AND c.isActive = 1 ");
        query.setParameter("code", code);
        query.setParameter("type", type);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Category) result.get(0);
        }
    }

    public List getSelectCategoryByType(String type) {
        List<Category> lstCategory = new ArrayList();
        try {
            if (type != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and a.categoryTypeCode=?"
                                + " order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, type);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        Category zero = new Category(-1l);
        zero.setName("--- Chọn ---");
        lstCategory.add(0, zero);
        return lstCategory;
    }

    public List getSelectCategoryByTypeOrderByCode(String type) {
        List<Category> lstCategory = new ArrayList();
        try {
            if (type != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and a.categoryTypeCode=?"
                                + " order by a.code asc) ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, type);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        Category zero = new Category(-1l);
        zero.setName("--- Chọn ---");
        lstCategory.add(0, zero);
        return lstCategory;
    }

    public List getSelectCategoryByParentCode(String parentCode) {
        List<Category> lstCategory = new ArrayList();
        try {
            if (parentCode != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and a.parentCode=?"
                                + " order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, parentCode);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        Category zero = new Category(-1l);
        zero.setName("--- Chọn ---");
        lstCategory.add(0, zero);
        return lstCategory;
    }

    public List getSelectCategoryByParentCode(String type, String parentCode) {
        List<Category> lstCategory = new ArrayList();
        try {
            if (parentCode != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and a.parentCode=?"
                                + " and a.categoryTypeCode=? "
                                + " order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, parentCode);
                query.setParameter(1, type);

                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        Category zero = new Category(-1l);
        zero.setName("--- Chọn ---");
        lstCategory.add(0, zero);
        return lstCategory;
    }

    public List getByCodeAndTypeCode(String type_code, String code) {
        List<Category> lstCategory = new ArrayList();
        try {
            if (code != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and a.code=?"
                                + " and a.categoryTypeCode=? "
                                + " order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, code);
                query.setParameter(1, type_code);

                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        Category zero = new Category(-1l);
        zero.setName("--- Chọn ---");
        lstCategory.add(0, zero);
        return lstCategory;
    }

    public List getSelectCategoryByType(String type, String order) {
        List<Category> lstCategory = new ArrayList();
        try {
            if (type != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = ? and a.categoryTypeCode=?");
                if (order != null) {
                    if ("name".equals(order)) {
                        stringBuilder.append(" order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese')");
                    } else {
                        stringBuilder.append(" order by " + order);
                    }
                }
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, Constants_XNN.Status.ACTIVE);
                query.setParameter(1, type);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        Category zero = new Category(-1l);
        zero.setName("--- Chọn ---");
        lstCategory.add(0, zero);
        return lstCategory;
    }

    public List findAllCategory(String type) {
        List<Category> lstCategory = new ArrayList();
        try {
            if (type != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and a.categoryTypeCode=?"
                                + " order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, type);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        Category zero = new Category(-1l);
        zero.setName("--- Chọn ---");
        zero.setValue("-1");
        lstCategory.add(0, zero);
        return lstCategory;
    }

    public List findAllCategoryByCode(String type, String code) {
        List<Category> lstCategory = null;
        try {
            if (type != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and (a.categoryTypeCode=? or parentCode=?) and a.code=?"
                                + " order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, type);
                query.setParameter(1, type);
                query.setParameter(2, code);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lstCategory;
    }

    public List findAllCategoryByCodeString(String type, String code) {
        List<Category> lstCategory = null;
        try {
            if (type != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and (a.categoryTypeCode=? or parentCode=?) and a.code in (" + code + ")"
                                + " order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, type);
                query.setParameter(1, type);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lstCategory;
    }

    /**
     * linhdx Cach lam Tim tat ca cac code trong bang category ma co code giong
     * voi autocode (Lay la catTypeCode);
     *
     * @param catTypeCode
     * @return
     */
    public String getCategorybyLastCode(String catTypeCode) {
        List<String> lstCode;
        Long currentId = 1L;
        try {
            if (catTypeCode != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " select a.code from Category a ");
                stringBuilder
                        .append("  where a.isActive = 1 and lower(a.code) LIKE ? ESCAPE '/' ");

                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, StringUtils.toLikeString(catTypeCode));
                lstCode = query.list();

                if (lstCode != null && lstCode.size() > 0) {
                    for (String code : lstCode) {
                        String lasIdStr = code.replace(catTypeCode, "");
                        try {
                            Long lastId = Long.valueOf(lasIdStr);
                            // lay id dang sau
                            //Neu id nay lon hon id hien tai thi gan = id nay
                            if (currentId < lastId) {
                                currentId = lastId;
                            }
                        } catch (Exception ex) {
                            LogUtils.addLogDB(ex);
                        }
                    }
                    currentId++;//Tang chi so len 1

                }

            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

        }
        return catTypeCode + currentId.toString();
    }

    // hoangnv28
    // Tim tat ca category ma 1 don vi co the su dung theo type
    @SuppressWarnings("unchecked")
    public List<Category> findAllCategoryByTypeAndDept(String categoryTypeCode,
            Long deptId) {
        List<Category> lstCategory = null;
        DepartmentDAOHE ddhe = new DepartmentDAOHE();
        List<Long> lstParents = ddhe.getParents(deptId);
        try {
            if (categoryTypeCode != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category c ");
                stringBuilder
                        .append("  where c.isActive = 1 and c.categoryTypeCode=? "
                                + " AND (c.deptId  IS NULL OR c.deptId = ? OR c.deptId IN (:listParents))"
                                + " order by nlssort(lower(ltrim(c.name)),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, categoryTypeCode);
                query.setParameter(1, deptId);
                query.setParameterList("listParents", lstParents);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lstCategory;
    }

    public List<Category> findAllByType(String[] catTypes) {
        try {
            String hql = "select u from Category u where u.isActive = 1 ";
            if (catTypes.length > 0) {
                hql += " and u.categoryTypeCode in ("
                        + StringUtils.join(catTypes, ",", true) + ") ";
            }
            hql += " order by u.name";
            Query query = session.createQuery(hql);
            List<Category> lstCategory = query.list();
            return lstCategory;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public List<Category> findStatusByVal(Object[] val) {
        try {
            String hql = "select u from Category u where u.isActive = 1 And u.categoryTypeCode = ?";
            if (val.length > 0) {
                hql += " and u.value in ("
                        + StringUtils.join(val, ",", true) + ") ";
            }
            hql += " order by u.name";
            Query query = session.createQuery(hql);
            query.setParameter(0, Constants.CATEGORY_TYPE.FILE_STATUS);
            List<Category> lstCategory = query.list();
            Category zero = new Category(-1L, "--- Chọn ---");
            lstCategory.add(0, zero);
            return lstCategory;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public List findAllByType(List<Category> catTypes, String type) {
        try {
            List lstCat = new ArrayList();
            if (catTypes.size() > 0) {
                for (Category c : catTypes) {
                    if (c.getCategoryTypeCode().equals(type)) {
                        lstCat.add(c);
                    }
                }
            }
            Category zero = new Category(-1L, "--- Chọn ---");
            lstCat.add(0, zero);
            return lstCat;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public List<Category> findAllCategoryOrderByCode(String type) {
        List<Category> lstCategory = new ArrayList<Category>();
        try {
            if (type != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder.append("  where a.isActive = ? and a.type=?"
                        + " order by a.code ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, Constants.Status.ACTIVE);
                query.setParameter(1, type);
                lstCategory = query.list();

                if (lstCategory != null && lstCategory.size() > 0) {
                    for (int i = 0; i < lstCategory.size(); i++) {
                        String name = lstCategory.get(i).getName();
                        name = StringUtils.escapeHtml(name);
                        if (name.length() > 255) {
                            name = name.substring(0, 254);
                        }
                        lstCategory.get(i).setName(name);
                    }
                }
            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lstCategory;
    }

    public Category findCategoryByName(String type, String searchName) {
        List<Category> lstCategory;
        Category item = null;
        try {
            if (type != null) {
                StringBuilder stringBuilder = new StringBuilder(
                        " from Category a ");
                stringBuilder
                        .append("  where a.isActive = ? and a.type=? and lower(a.name) like ? escape'!' "
                                + " order by nlssort(lower(a.name),'nls_sort = Vietnamese') ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, Constants.Status.ACTIVE);
                query.setParameter(1, type);
                query.setParameter(2, convertToLikeString(searchName));
                lstCategory = query.list();

                if (lstCategory != null && lstCategory.size() > 0) {
                    item = lstCategory.get(0);
                }
            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return item;
    }

    public List<Category> findAllCategorySearch(String type) {
        try {
            StringBuilder stringBuilder = new StringBuilder(" from Category a ");
            stringBuilder.append("  where a.isActive = ? and a.categoryTypeCode=? ");
            if (("VOBQ").equals(type)) {
                stringBuilder.append(" order by a.code");
            } else if (Constants.CATEGORY_TYPE.FILE_STATUS.equals(type)) {
                stringBuilder.append(" order by a.code");
            } else {
                stringBuilder
                        .append(" order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese')");
            }

            // Thoi han bao quan xep theo code
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, Constants.Status.ACTIVE);
            query.setParameter(1, type);
            List<Category> lstCategory = query.list();
            Category a = new Category();
            a.setCategoryId(Constants.COMBOBOX_HEADER_VALUE);
            a.setName(Constants.COMBOBOX_HEADER_TEXT_SELECT);
            List<Category> lstCategoryFull = new ArrayList();
            lstCategoryFull.add(a);
            lstCategoryFull.addAll(lstCategory);
            return lstCategoryFull;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return null;
    }

    public List<Category> findAllCategorySearchValue(String type, Boolean isHeader) {
        try {
            StringBuilder stringBuilder = new StringBuilder(" from Category a ");
            stringBuilder.append("  where a.isActive = ? and a.categoryTypeCode=? ");
            stringBuilder.append("  and a.value in (select v.statusfile from VCosPaymentInfo v) ");
            if (("VOBQ").equals(type)) {
                stringBuilder.append(" order by a.code");
            } else {
                stringBuilder
                        .append(" order by a.value");
            }

            // Thoi han bao quan xep theo code
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, Constants.Status.ACTIVE);
            query.setParameter(1, type);
            List<Category> lstCategory = query.list();
            List<Category> lstCategoryFull = new ArrayList();
            if (isHeader) {
                Category a = new Category();
                a.setCategoryId(Constants.COMBOBOX_HEADER_VALUE);
                a.setValue("-1");
                a.setName(Constants.COMBOBOX_HEADER_TEXT_SELECT);
                lstCategoryFull.add(a);
            }
            lstCategoryFull.addAll(lstCategory);
            return lstCategoryFull;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return null;
    }

    public List<Category> findAllCategorySearch(String type, Boolean isHeader) {
        try {
            StringBuilder stringBuilder = new StringBuilder(" from Category a ");
            stringBuilder.append("  where a.isActive = ? and a.categoryTypeCode=? ");
            if (("VOBQ").equals(type)) {
                stringBuilder.append(" order by a.code");
            } else {
                stringBuilder
                        .append(" order by a.sortOrder");
            }

            // Thoi han bao quan xep theo code
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, Constants.Status.ACTIVE);
            query.setParameter(1, type);
            List<Category> lstCategory = query.list();
            List<Category> lstCategoryFull = new ArrayList();
            if (isHeader) {
                Category a = new Category();
                a.setCategoryId(Constants.COMBOBOX_HEADER_VALUE);
                a.setValue("-1");
                a.setName(Constants.COMBOBOX_HEADER_TEXT_SELECT);
                lstCategoryFull.add(a);
            }
            lstCategoryFull.addAll(lstCategory);
            return lstCategoryFull;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return null;
    }

    public List<Category> findAllCategoryStatic(String type) {
        if (Constants.CATEGORY_TYPE.PROCEDURE_TEMPLATE_TYPE.equals(type)) {
            List<Category> lstCategoryFull = new ArrayList();
            Category a = new Category();
            a.setCategoryId(Constants.COMBOBOX_HEADER_VALUE);
            a.setName(Constants.COMBOBOX_HEADER_TEXT);
            lstCategoryFull.add(a);

            Category b = new Category();
            b.setCategoryId(Constants.PROCEDURE_TEMPLATE_TYPE.ADDITIONAL_REQUEST);
            b.setName(Constants.PROCEDURE_TEMPLATE_TYPE.ADDITIONAL_REQUEST_STR);
            lstCategoryFull.add(b);
            Category c = new Category();
            c.setCategoryId(Constants.PROCEDURE_TEMPLATE_TYPE.PERMIT);
            c.setName(Constants.PROCEDURE_TEMPLATE_TYPE.PERMIT_STR);
            lstCategoryFull.add(c);

            Category d = new Category();
            d.setCategoryId(Constants.PROCEDURE_TEMPLATE_TYPE.PHILEPHI);
            d.setName(Constants.PROCEDURE_TEMPLATE_TYPE.PHILEPHI_STR);
            lstCategoryFull.add(d);

            return lstCategoryFull;
        }

        return null;
    }

    // Tim kiem Category theo don vi tao(DM kho luu tru)
    public List<Category> findAllCategorySearch(String type, Long deptId) {
        try {
            StringBuilder stringBuilder = new StringBuilder(" from Category a ");
            stringBuilder
                    .append("  where a.isActive = 1 and a.categoryTypeCode = ? ");
            if (deptId != null) {
                stringBuilder
                        .append(" and (a.createdBy = ? or a.deptId = ? or a.createdBy is null or a.deptId is null )");
            } else {
                stringBuilder
                        .append(" and (a.createdBy is null or a.deptId is null )");
            }
            stringBuilder
                    .append(" order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, type);
            if (deptId != null) {
                query.setParameter(1, deptId);
                query.setParameter(2, deptId);
            }

            List<Category> lstCategory = query.list();
            Category a = new Category();
            a.setCategoryId(Constants.COMBOBOX_HEADER_VALUE);
            a.setName(Constants.COMBOBOX_HEADER_TEXT);
            List<Category> lstCategoryFull = new ArrayList();
            lstCategoryFull.add(a);
            lstCategoryFull.addAll(lstCategory);
            return lstCategoryFull;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return null;
    }

    public List<Category> findCategory(CategorySearchForm form) {
        List listParam = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder(
                    "select a from Category a where a.isActive <> -1 ");
            if (form != null) {
                if (form.getIsActive() != null) {
                    strBuf.append(" and a.isActive = ? ");
                    listParam.add(form.getIsActive());
                }
                if (form.getType() != null && !"".equals(form.getType())) {
                    strBuf.append(" and a.categoryTypeCode = ? ");
                    listParam.add(form.getType());
                }
                if (form.getCode() != null && !"".equals(form.getCode())) {
                    strBuf.append(" and lower(a.code) LIKE ? ESCAPE '/' ");
                    listParam.add(StringUtils.toLikeString(form.getCode()
                            .trim().toLowerCase()));
                }

                if (form.getName() != null && !"".equals(form.getName())) {
                    strBuf.append(" and lower(a.name) LIKE ? ESCAPE '/' ");
                    listParam.add(StringUtils.toLikeString(form.getName()
                            .trim().toLowerCase()));
                }
            }
            strBuf.append(" order by a.isActive desc ");
            Query query = session.createQuery(strBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
            }

            return query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public List getSelectCategory(String categoryTypeCode) {
        CategorySearchForm form = new CategorySearchForm();
        form.setType(categoryTypeCode);

        List lstCat = findCategory(form);
        if (lstCat == null) {
            lstCat = new ArrayList();
        }
        Category zero = new Category(-1L, "--- Chọn ---");
        zero.setName("--- Chọn ---");
        lstCat.add(0, zero);
        return lstCat;
    }

    /**
     * Tim kiem Loai danh muc
     *
     * @param form
     * @param start
     * @param count
     * @param sortField
     * @return
     */
    public List<Category> findCategory(CategorySearchForm form, int start,
            AtomicInteger count, String sortField) {
        List paramList = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder(
                    " from Category a where a.isActive <> 0 ");// binhnt update
            // active =
            // 1;deactive =
            // 0;
            if (form != null) {
                if (form.getType() != null && !"".equals(form.getType())) {
                    strBuf.append(" and a.type= ? ");
                    paramList.add(form.getType());

                    // Neu la kho luu tru thi tim theo don vi tao Kho luu tru
                    if ("VOLT".equals(form.getType())) {
                        strBuf.append(" and a.createdBy = ? ");
                        paramList.add(form.getCreatedBy());
                    }
                }

                if (form.getCode() != null && !"".equals(form.getCode())) {
                    strBuf.append(" and lower(a.code) LIKE ? ESCAPE '/' ");
                    paramList.add(StringUtils.toLikeString(form.getCode()
                            .trim().toLowerCase()));
                }

                if (form.getName() != null && !"".equals(form.getName())) {
                    strBuf.append(" and lower(a.name) LIKE ? ESCAPE '/' ");
                    paramList.add(StringUtils.toLikeString(form.getName()
                            .trim().toLowerCase()));
                }
                if (form.getIsActive() != 0) {
                    strBuf.append(" and a.isActive = ? ");
                    paramList.add(form.getIsActive());
                } else {
                }
            }

            Query query = getSession().createQuery(
                    "SELECT count(*) " + strBuf.toString());
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i, paramList.get(i));
            }
            int total = Integer.parseInt(query.list().get(0).toString());

            String sortType = null;
            if (sortField != null) {
                if (sortField.indexOf('-') != -1) {
                    sortType = " asc";
                    sortField = sortField.substring(1); // not use in this case
                } else {
                    sortType = " desc";
                }
            }

            if (sortField != null) {
                strBuf.append(" order by a.")
                        .append(validateColumnName(sortField)).append(" ")
                        .append(sortType);
            } else {
                strBuf.append("  order by nlssort(lower(a.name),'nls_sort = Vietnamese') ");
            }

            query = getSession().createQuery(strBuf.toString());
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i, paramList.get(i));
            }

            query.setFirstResult(start);
            query.setMaxResults(count.intValue());

            List<Category> lstCategory = query.list();
            count.set(total);
            return lstCategory;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    /*
     * 
     * Hàm cập nhật danh mục
     */
    public boolean onCreateOrUpdate(Category catUpdate, boolean isUpdate) {
        try {
            if (catUpdate == null) {
                LogUtils.addLog("Category");
            }
            if (isUpdate) {
                update(catUpdate);
            } else {
                create(catUpdate);
            }
            // getSession().getTransaction().commit();
            return true;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    public Category checkBO(Category bo) {
        try {
            StringBuilder strBuf = new StringBuilder(" from Category a ");
            strBuf.append(" where a.isActive = :isActive ");

            if (bo.getCategoryId() != null) {
                strBuf.append(" and a.categoryId != :categoryId ");
            }
            // if (bo.getType() != null) {
            // strBuf.append(" and a.type = :type ");
            // }
            if (bo.getCreatedBy() != null) {
                strBuf.append(" and a.createdBy = :createdBy ");
            }

            if (bo.getCode() != null && bo.getName() != null) {
                strBuf.append(" and (a.code = :code  or a.name = :name)");
            }

            Query query = getSession().createQuery(strBuf.toString());
            query.setParameter("isActive", "1");

            if (bo.getCategoryId() != null) {
                query.setParameter("categoryId", bo.getCategoryId());
            }
            // if (bo.getType() != null) {
            // query.setParameter("type", bo.getType().trim());
            // }
            if (bo.getCreatedBy() != null) {
                query.setParameter("createdBy", bo.getCreatedBy());
            }
            if (bo.getCode() != null && bo.getName() != null) {
                query.setParameter("code", bo.getCode().trim());
                query.setParameter("name", bo.getName().trim());
            }

            List<Category> lstCategory = query.list();
            if (!lstCategory.isEmpty()) {
                return lstCategory.get(0);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }

        return null;
    }

    public void formToBO(CategorySearchForm categoryAddEditForm, Category bo) {
        if (categoryAddEditForm.getType() != null) {
            // bo.setType(categoryAddEditForm.getType().trim());
        }

        if (categoryAddEditForm.getCode() != null) {
            bo.setCode(categoryAddEditForm.getCode().trim());
        }

        if (categoryAddEditForm.getName() != null) {
            bo.setName(categoryAddEditForm.getName().trim());
        }
        if (categoryAddEditForm.getDescription() != null) {
            bo.setDescription(categoryAddEditForm.getDescription());
        }
        // if (categoryAddEditForm.getIsActive() != null) {
        // bo.setIsActive(categoryAddEditForm.getIsActive());
        // }
    }

    public CategorySearchForm boToForm(Category bo) {
        CategorySearchForm form = new CategorySearchForm();
        // if (bo.getType() != null) {
        // form.setType(bo.getType().trim());
        // }

        if (bo.getCode() != null) {
            form.setCode(bo.getCode().trim());
        }

        if (bo.getName() != null) {
            form.setName(bo.getName().trim());
        }
        form.setCategoryId(bo.getCategoryId());
        return form;
    }

    public Category getCategoryById(long id) throws Exception {
        Criteria cri = getSession().createCriteria(Category.class);
        cri.add(Restrictions.eq("categoryId", id));
        List<Category> cats = cri.list();
        if (cats.isEmpty()) {
            return null;
        } else {
            return cats.get(0);
        }
    }

    public String getNameById(Long categoryId) {
        Category category;
        try {
            StringBuilder strBuf = new StringBuilder(" from Category a ");
            strBuf.append(" where a.isActive = ? AND a.categoryId=?  ");

            Query query = getSession().createQuery(strBuf.toString());
            query.setParameter(0, Constants.Status.ACTIVE);
            query.setParameter(1, categoryId);
            category = (Category) query.uniqueResult();
            if (category != null) {
                return category.getName();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return "";
        }
        return "";
    }

    public List<Category> findByCategoryName(String categoryName) {
        List<Category> category;
        try {
            StringBuilder strBuf = new StringBuilder(" from Category a ");
            strBuf.append(" where a.isActive = ? AND lower(a.name) = ? ");

            Query query = getSession().createQuery(strBuf.toString());
            query.setParameter(0, Constants.Status.ACTIVE);
            query.setParameter(1, categoryName.toLowerCase());
            category = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return category;
    }

    //
    public boolean isDuplicate(CategorySearchForm form) {
        if (form == null) {
            return false;
        }
        List lstParam = new ArrayList();
        String hql = "select count(c) from Category c where c.isActive = 1 ";
        if (form.getCategoryId() != null && form.getCategoryId() > 0l) {
            hql += " and c.categoryId <> ? ";
            lstParam.add(form.getCategoryId());
        }
        if (form.getCode() != null && form.getCode().trim().length() > 0) {
            hql += " and c.code = ? ";
            lstParam.add(form.getCode());
        }
        if (form.getName() != null && form.getName().trim().length() > 0) {
            hql += " and lower(c.name) = ?";
            lstParam.add(form.getName().toLowerCase());
        }
        Query query = getSession().createQuery(hql);
        for (int i = 0; i < lstParam.size(); i++) {
            query.setParameter(i, lstParam.get(i));
        }

        Long count = Long.parseLong(query.uniqueResult().toString());
        boolean bReturn = false;
        if (count >= 1l) {
            bReturn = true;
        }
        return bReturn;
    }

    /*
     * hoangnv28 Them dong --Chon-- cho data
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List addOptionalCategory(List data) {
        Category optionalCategory = new Category(
                Constants.COMBOBOX_HEADER_VALUE);
        optionalCategory.setName(Constants.COMBOBOX_HEADER_TEXT_SELECT);
        data.add(0, optionalCategory);
        return data;
    }

    //
    // Tree
    //
    public int getCountCategory(Long categoryTypeId) {
        CategoryTypeDAOHE ctdhe = new CategoryTypeDAOHE();
        CategoryType ct = ctdhe.findById(categoryTypeId);
        String hql = "select count(c) from Category c where c.isActive = 1 and c.categoryTypeCode = ?";
        Query query = session.createQuery(hql);
        query.setParameter(0, ct.getCode());
        int count = ((Long) query.uniqueResult()).intValue();
        return count;
    }

    public TreeItem getTreeItem(Long categoryTypeId, int index) {
        CategoryTypeDAOHE ctdhe = new CategoryTypeDAOHE();
        CategoryType ct = ctdhe.findById(categoryTypeId);
        String hql = "select c from Category c where c.isActive = 1 and c.categoryTypeCode = ?";
        Query query = session.createQuery(hql);
        query.setParameter(0, ct.getCode());
        query.setFirstResult(index);
        query.setMaxResults(1);
        Category item = (Category) query.list().get(0);
        TreeItem treeItem = new TreeItem(item.getCategoryId(), item.getName(),
                2l);
        return treeItem;
    }

    /**
     * Lấy loại văn bản ở các menu
     *
     * @param userId
     * @param deptId
     * @param isFileClerk
     * @param menuType
     * @return
     */
    public List getDocTypeInMenu(Long userId, Long deptId, boolean isFileClerk, int menuType) {
        try {
            StringBuilder hqlBuilder = new StringBuilder("SELECT DISTINCT(c) FROM Category c WHERE "
                    + " c.categoryId IN ");
            List listParams = new ArrayList<>();
            if (Constants.DOCUMENT_MENU.ALL != menuType && Constants.DOCUMENT_MENU.REPORT != menuType) {
                hqlBuilder.append(" (SELECT d.documentType FROM DocumentReceive d, Process p "
                        + " WHERE d.status >= 0 AND d.documentReceiveId = p.objectId AND p.objectType = ? "
                        + " AND p.isActive = 1 ");
                listParams.add(Constants.OBJECT_TYPE.DOCUMENT_RECEIVE);
                if (isFileClerk) {
                    hqlBuilder
                            .append(" AND (p.receiveUserId = ? "
                                    + " OR (EXISTS (SELECT bd FROM BookDocument bd, Books b WHERE "
                                    + " b.deptId = ? AND bd.documentId = d.documentReceiveId AND b.bookObjectTypeId = ? AND b.bookId = bd.bookId ) "
                                    + " AND p.receiveUserId IS NULL AND p.receiveGroupId = ? )) ");
                    listParams.add(userId);
                    listParams.add(deptId);
                    listParams.add(Constants.OBJECT_TYPE.DOCUMENT_RECEIVE);
                    listParams.add(deptId);
                } else {
                    hqlBuilder
                            .append(" AND p.receiveUserId = ? ");
                    listParams.add(userId);
                }
            }
            switch (menuType) {
                case Constants.DOCUMENT_MENU.ALL:
                    hqlBuilder
                            .append(" (SELECT d.documentType FROM DocumentReceive d "
                                    + " WHERE d.status >= 0 AND d.documentReceiveId NOT IN (SELECT bd.documentId "
                                    + " FROM BookDocument bd, Books b WHERE b.deptId = ? AND b.bookObjectTypeId = ? "
                                    + " AND b.bookId = bd.bookId AND b.status >= 0) AND d.documentReceiveId IN (SELECT p.objectId "
                                    + " FROM Process p WHERE p.isActive = 1 AND p.objectType = ? AND p.receiveGroupId = ? "
                                    + " AND p.receiveUserId IS NULL AND (p.status = ? OR p.status = ?))) ");
                    listParams.add(deptId);
                    listParams.add(Constants.OBJECT_TYPE.DOCUMENT_RECEIVE);
                    listParams.add(Constants.OBJECT_TYPE.DOCUMENT_RECEIVE);
                    listParams.add(deptId);
                    listParams.add(Constants.PROCESS_STATUS.NEW);
                    listParams.add(Constants.PROCESS_STATUS.READ);
                    break;
                case Constants.DOCUMENT_MENU.REPORT:
                    hqlBuilder
                            .append(" (SELECT d.documentType FROM DocumentReceive d "
                                    + " WHERE d.status >= 0 AND d.documentReceiveId IN (SELECT bd.documentId "
                                    + " FROM BookDocument bd, Books b WHERE b.deptId = ? AND b.bookObjectTypeId = ? "
                                    + " AND b.bookId = bd.bookId AND b.status >= 0)) ");
                    listParams.add(deptId);
                    listParams.add(Constants.OBJECT_TYPE.DOCUMENT_RECEIVE);
                    break;
                case Constants.DOCUMENT_MENU.WAITING_PROCESS:
                    hqlBuilder.append(" AND p.processType <> ? AND p.processType <> ? "
                            + " AND (d.status = ? OR d.status = ? OR d.status = ? OR d.status = ? ) ");

                    listParams.add(Constants.PROCESS_TYPE.COMMENT);
                    listParams.add(Constants.PROCESS_TYPE.RECEIVE_TO_KNOW);

                    listParams.add(Constants.DOCUMENT_STATUS.NEW);
                    listParams.add(Constants.DOCUMENT_STATUS.PROCESSING);
                    listParams.add(Constants.DOCUMENT_STATUS.RETRIEVE_ALL);
                    listParams.add(Constants.DOCUMENT_STATUS.RETURN_ALL);

                    hqlBuilder.append(" AND (1=2 ");
                    for (Long processStatus : Constants.PROCESS_STATUS.getWaitingProcessStatus()) {
                        hqlBuilder.append(" OR p.status = ? ");
                        listParams.add(processStatus);
                    }
                    hqlBuilder.append(" )) ");
                    break;
                case Constants.DOCUMENT_MENU.PROCESSING:
                    hqlBuilder.append(" AND p.processType <> ? AND p.processType <> ? "
                            + " AND d.status = ? ");

                    listParams.add(Constants.PROCESS_TYPE.COMMENT);
                    listParams.add(Constants.PROCESS_TYPE.RECEIVE_TO_KNOW);

                    listParams.add(Constants.DOCUMENT_STATUS.PROCESSING);

                    hqlBuilder.append(" AND (1=2 ");
                    for (Long processStatus : Constants.PROCESS_STATUS.getProcessingStatus()) {
                        hqlBuilder.append(" OR p.status = ? ");
                        listParams.add(processStatus);
                    }
                    hqlBuilder.append(" )) ");
                    break;
                case Constants.DOCUMENT_MENU.PROCESSED:
                    hqlBuilder.append(" AND p.processType <> ? AND p.processType <> ? "
                            + " AND (( d.status = ? AND p.status <> ? AND p.status <> ? AND p.status <> ?) "
                            + "  OR ((p.status = ? OR p.status = ? OR p.status = ? OR p.status = ?) AND d.status >= 0 )))");

                    listParams.add(Constants.PROCESS_TYPE.COMMENT);
                    listParams.add(Constants.PROCESS_TYPE.RECEIVE_TO_KNOW);

                    listParams.add(Constants.DOCUMENT_STATUS.PROCESSED);
                    listParams.add(Constants.PROCESS_STATUS.RETURN);
                    listParams.add(Constants.PROCESS_STATUS.FINISH_2);
                    listParams.add(Constants.PROCESS_STATUS.PUBLISHED);

                    listParams.add(Constants.PROCESS_STATUS.RETURN);
                    listParams.add(Constants.PROCESS_STATUS.FINISH_2);
                    listParams.add(Constants.PROCESS_STATUS.PUBLISHED);
                    listParams.add(Constants.PROCESS_STATUS.FINISH_1);
                    break;
                case Constants.DOCUMENT_MENU.RETRIEVED:
                    hqlBuilder.append(" AND p.processType <> ? AND p.processType <> ? "
                            + " AND p.status = ? )");

                    listParams.add(Constants.PROCESS_TYPE.COMMENT);
                    listParams.add(Constants.PROCESS_TYPE.RECEIVE_TO_KNOW);
                    listParams.add(Constants.PROCESS_STATUS.RETRIEVE);
                    break;
                case Constants.DOCUMENT_MENU.RECEIVE_TO_KNOW:
                    hqlBuilder.append(" AND p.processType <> ? )");
                    listParams.add(Constants.PROCESS_TYPE.RECEIVE_TO_KNOW);
                    break;
                case Constants.DOCUMENT_MENU.WAITING_GIVE_OPINION:
                    hqlBuilder.append(" AND p.processType <> ? ");
                    listParams.add(Constants.PROCESS_TYPE.COMMENT);

                    hqlBuilder.append(" AND (1=2 ");
                    for (Long processStatus : Constants.PROCESS_STATUS.getWaitingProcessStatus()) {
                        hqlBuilder.append(" OR p.status = ? ");
                        listParams.add(processStatus);
                    }
                    hqlBuilder.append(" )) ");
                    break;
                case Constants.DOCUMENT_MENU.GAVE_OPINION:
                    hqlBuilder.append(" AND p.processType <> ? ");
                    listParams.add(Constants.PROCESS_TYPE.COMMENT);

                    hqlBuilder.append(" AND (1=2 ");
                    for (Long processStatus : Constants.PROCESS_STATUS.getProcessingStatus()) {
                        hqlBuilder.append(" OR p.status = ? ");
                        listParams.add(processStatus);
                    }
                    hqlBuilder.append(" )) ");
                    break;
            }
            Query query = getSession().createQuery(hqlBuilder.toString());
            for (int i = 0; i < listParams.size(); i++) {
                query.setParameter(i, listParams.get(i));
            }
            return query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
    }

    public List<Category> findCategoryByCodeAndCatType(String code, String categoryTypeCode) {
        List<Category> lstCategory = null;
        try {
            if (code != null && categoryTypeCode != null) {
                StringBuilder stringBuilder = new StringBuilder(" from Category a ");
                stringBuilder.append("  where a.isActive = 1 "
                        + " and a.categoryTypeCode = ? "
                        + " and a.code = ? ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, categoryTypeCode);
                query.setParameter(1, code);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lstCategory;
    }

    public List<Category> findCategoryByCode(String code) {
        List<Category> lstCategory = null;
        try {
            if (code != null) {
                StringBuilder stringBuilder = new StringBuilder(" from Category a ");
                stringBuilder.append("  where a.isActive = 1 "
                        + " and a.code = ? ");
                Query query = getSession()
                        .createQuery(stringBuilder.toString());
                query.setParameter(0, code);
                lstCategory = query.list();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lstCategory;
    }

    public String LayTruongPermitId(Long sTruongKhoa) {
        String sql = "SELECT r.permitId FROM CosPermit r WHERE r.fileId=?";
        Query fileQuery = session.createQuery(sql);
        fileQuery.setParameter(0, sTruongKhoa);
        if (fileQuery.list().size() > 0) {
            return (String) fileQuery.list().get(0).toString();
        } else {
            return null;
        }

    }

    public String LayTruongcosFileId(Long sTruongKhoa) {
        String sql = "SELECT r.cosFileId FROM CosFile r WHERE r.fileId=?";
        Query fileQuery = session.createQuery(sql);
        fileQuery.setParameter(0, sTruongKhoa);
        if (fileQuery.list().size() > 0) {
            return (String) fileQuery.list().get(0).toString();
        } else {
            return null;
        }

    }

    public String LayTruongCategory_Name(Long sTruongKhoa) {
        String sql= "SELECT r.name FROM Category r WHERE r.categoryId=?";
        Query fileQuery = session.createQuery(sql);
        fileQuery.setParameter(0, sTruongKhoa);
        if (fileQuery.list().size() > 0) {
            return (String) fileQuery.list().get(0).toString();
        } else {
            return null;
        }
    }

    public String LayTruongPlace_Name(Long sTruongKhoa) {
        String SQL = "SELECT r.name FROM Place r WHERE r.placeId=?";
        Query fileQuery = session.createQuery(SQL);
        fileQuery.setParameter(0, sTruongKhoa);
        if (fileQuery.list().size() > 0) {
            return (String) fileQuery.list().get(0).toString();
        } else {
            return null;
        }

    }

    public List<Category> findAllCategorySearch(String type, Boolean bTitle, String sTitle) {
        try {
            StringBuilder stringBuilder = new StringBuilder(" from Category a ");
            stringBuilder.append("  where a.isActive = ? and a.categoryTypeCode=? ");
            if (("VOBQ").equals(type)) {
                stringBuilder.append(" order by a.code");
            } else {
                stringBuilder
                        .append(" order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese')");
            }

            // Thoi han bao quan xep theo code
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, Constants.Status.ACTIVE);
            query.setParameter(1, type);
            List<Category> lstCategory = query.list();
            if (bTitle == true) {
                Category a = new Category();
                a.setCategoryId(Constants.COMBOBOX_HEADER_VALUE);
                a.setName(sTitle);
                lstCategory.add(0, a);
            }
            return lstCategory;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return null;
    }
}
