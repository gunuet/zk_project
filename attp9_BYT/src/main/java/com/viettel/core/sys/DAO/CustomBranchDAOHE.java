/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.sys.BO.CustomBranch;
import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author ChucHV
 */
public class CustomBranchDAOHE extends GenericDAOHibernate<CustomBranch, Long> {

    public CustomBranchDAOHE() {
        super(CustomBranch.class);
    }

    @Override
    public void saveOrUpdate(CustomBranch o) {
        if (o != null) {
            super.saveOrUpdate(o);
            getSession().flush();
        }
    }

    @Override
    public void delete(CustomBranch port) {
        port.setIsActive(0L);
        getSession().saveOrUpdate(port);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public CustomBranch findById(Long id) {
        Query query = getSession().getNamedQuery("CustomBranch.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CustomBranch) result.get(0);
        }
    }

    @SuppressWarnings("rawtypes")
    public CustomBranch findByCode(String code) {
        Query query = getSession().getNamedQuery("CustomBranch.findByCode");
        query.setParameter("code", code);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CustomBranch) result.get(0);
        }
    }

    @SuppressWarnings("unchecked")
    public List<CustomBranch> getCustomBranchByProvinceCode(String code) {
        if (code.isEmpty()) {
            return null;
        }

        String hql = "SELECT o FROM CustomBranch o WHERE o.provinceCode = :code AND o.isActive = 1 "
                + " ORDER BY o.name ASC";

        Query query = getSession().createQuery(hql);
        query.setParameter("code", code);
        List<CustomBranch> result = query.list();
        return result;
    }
}
