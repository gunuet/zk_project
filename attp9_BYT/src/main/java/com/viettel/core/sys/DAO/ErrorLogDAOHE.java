/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.StringUtils;
import com.viettel.core.sys.BO.ActionLog;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.ErrorLog;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author ChucHV
 */
public class ErrorLogDAOHE extends GenericDAOHibernate<ErrorLog, Long> {

    public ErrorLogDAOHE() {
        super(ErrorLog.class);
    }

    public PagingListModel searchLog(ErrorLog search, Date startDate, Date endDate, int start, int take) {
        StringBuilder hql = new StringBuilder(" from ErrorLog l where 1 = 1 ");
        List lstParam = new ArrayList();
        if (startDate != null) {
            hql.append(" and l.createDate >= ?");
            startDate = DateTimeUtils.setStartTimeOfDate(startDate);
            lstParam.add(startDate);
        }

        if (endDate != null) {
            hql.append(" and l.createDate < ?");
            endDate = DateTimeUtils.addOneDay(endDate);
            endDate = DateTimeUtils.setStartTimeOfDate(endDate);
            lstParam.add(endDate);
        }

        String selectHql = "select l " + hql + " order by l.createDate desc";
        String countHql = "select count(l) " + hql;
        Query query = session.createQuery(selectHql);
        Query countQuery = session.createQuery(countHql);
        for (int i = 0; i < lstParam.size(); i++) {
            query.setParameter(i, lstParam.get(i));
            countQuery.setParameter(i, lstParam.get(i));
        }
        Long count = (Long) countQuery.uniqueResult();
        query.setFirstResult(start);
        query.setMaxResults(take);
        List lstLog = query.list();
        PagingListModel model = new PagingListModel(lstLog, count);
        return model;
    }
}
