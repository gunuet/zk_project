/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.TimeHoliday;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author hungpv32
 */
public class HolidaysManagementDAOHE extends GenericDAOHibernate<TimeHoliday, Long> {

    public HolidaysManagementDAOHE() {
        super(TimeHoliday.class);
    }

    public PagingListModel search(TimeHoliday searchForm, int start, int take) {
        List listParam = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder("select r from TimeHoliday r where r.isActive = 1 ");
            StringBuilder strCountBuf = new StringBuilder("select count(r) from TimeHoliday r where r.isActive = 1 ");

            StringBuilder hql = new StringBuilder();
            if (searchForm != null) {
                if (searchForm.getTimeDate() != null) {
                    hql.append(" and r.timeDate = ? ");
                    listParam.add(searchForm.getTimeDate());
                }

                if (searchForm.getDateType() != null) {
                    hql.append(" and r.dateType = ? ");
                    listParam.add(searchForm.getDateType());
                }
                if (searchForm.getArrange() != null) {
                    if (searchForm.getArrange() == 1) {
                        hql.append(" order by nlssort(lower(trim(r.name)),'nls_sort = Vietnamese') asc ");
                    } else if (searchForm.getArrange() == 2) {
                        hql.append(" order by nlssort(lower(trim(r.name)),'nls_sort = Vietnamese') desc ");
                    } else if (searchForm.getArrange() == 3) {
                        hql.append(" ORDER BY r.timeDate asc ");
                    } else if (searchForm.getArrange() == 4) {
                        hql.append(" ORDER BY r.timeDate desc ");
                    }
                }
            }

            strBuf.append(hql);
            strCountBuf.append(hql);

            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public boolean hasDuplicate(TimeHoliday timeHoliday) {
        StringBuilder hql = new StringBuilder("select count(r) from TimeHoliday r where r.isActive = 1 ");
        List lstParams = new ArrayList();
        if (timeHoliday.getTimeHolidayId() != null && timeHoliday.getTimeHolidayId() > 0L) {
            hql.append(" and r.timeHolidayId <> ?");
            lstParams.add(timeHoliday.getTimeHolidayId());
        }

        if (timeHoliday.getTimeDate() != null) {
            hql.append(" and r.timeDate = ? ");
            lstParams.add(timeHoliday.getTimeDate());
        }

        Query query = session.createQuery(hql.toString());
        for (int i = 0; i < lstParams.size(); i++) {
            query.setParameter(i, lstParams.get(i));
        }
        Long count = (Long) query.uniqueResult();
        return count > 0L;
    }

    public void delete(Long id) {
        TimeHoliday obj = findById(id);
        obj.setIsActive(Constants.HOLIDAY.INACTIVE);
        update(obj);
    }

    // hàm trả về ngày xử lý xong
    public List getAllHolidays() {
        String stringQuery = "select * from Time_Holiday r where r.is_Active = 1 and r.DATE_TYPE = 1 order by r.time_Date asc";
        Query query = session.createSQLQuery(stringQuery).addEntity(TimeHoliday.class);
        List<TimeHoliday> Holidays = query.list();
        return Holidays;
    }

    public List getAllWorkSatSun() {
        String stringQuery = "select * from Time_Holiday r where r.is_Active = 1 and r.DATE_TYPE = 0 order by r.time_Date asc";
        Query query = session.createSQLQuery(stringQuery).addEntity(TimeHoliday.class);
        List<TimeHoliday> WorkSatSun = query.list();
        return WorkSatSun;
    }

    public boolean checkIsWeekend(Calendar input) {
        boolean ck = false;
        if (input.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || input.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            ck = true;
        }
        return ck;
    }

    public Date getDeadline(Date startDate, Long processTime) {
        Calendar calStart = new GregorianCalendar();
        calStart.setTime(startDate);

        Calendar calEnd = new GregorianCalendar();
        calEnd.setTime(startDate);
        calEnd.add(Calendar.DATE, Integer.valueOf(processTime.toString()));

        int countDayAdd = 0;
        int dayCheck = Integer.valueOf(processTime.toString());
        int checkLoop = 1;

        List<TimeHoliday> WorkSatSun = getAllWorkSatSun();
        List<TimeHoliday> Holidays = getAllHolidays();

        do {
            
            LogUtils.addLog("checkLoop " + checkLoop + ": dayCheck: " + dayCheck);
            checkLoop++;

            for (int i = 0; i <= dayCheck; i++) {

                // kiểm tra xem có phải ngày cuối tuần không
                if (checkIsWeekend(calStart)) {
                    
                    for (int j = 0; j < WorkSatSun.size(); j++) {
                        
                        // ngày cuối tuần là ngày làm bù thì trừ 1
                        if (WorkSatSun.get(j).getTimeDate().getTime() >= calStart.getTime().getTime()
                                && WorkSatSun.get(j).getTimeDate().getTime() <= calEnd.getTime().getTime()) {
                            countDayAdd--;
                            LogUtils.addLog("bù: " + WorkSatSun.get(j).getTimeDate());
                        }
                    }
                    
                    // ngày cuối tuần không là ngày làm bù thì cộng 1
                    countDayAdd++;
                    LogUtils.addLog("nghỉ t7, cn: " + calStart.getTime());
                    
                } else {
                    
                    // ngày thường là ngày nghỉ thì cộng 1
                    for (int k = 0; k < Holidays.size(); k++) {
                        if (Holidays.get(k).getTimeDate().getTime() == calStart.getTime().getTime()) {
                            countDayAdd++;
                            LogUtils.addLog("nghỉ lễ: " + Holidays.get(k).getTimeDate());
                        }
                    }
                    
                }

                // tiếp tục kiểm tra ngày tiếp theo
                if (i != dayCheck) {
                    calStart.add(Calendar.DATE, 1);
                }

            }
            
            calStart.add(Calendar.DATE, 1);
            calEnd.add(Calendar.DATE, countDayAdd);
            
            dayCheck = countDayAdd;
            countDayAdd = 0;
            
        } while (dayCheck != 0);
        
        return calEnd.getTime();
    }
    // hàm trả về ngày xử lý xong
}