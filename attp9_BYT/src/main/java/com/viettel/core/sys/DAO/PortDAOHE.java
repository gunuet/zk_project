/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.sys.BO.Port;
import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author ChucHV
 */
public class PortDAOHE extends GenericDAOHibernate<Port, Long> {

    public PortDAOHE() {
        super(Port.class);
    }

    @Override
    public void saveOrUpdate(Port o) {
        if (o != null) {
            super.saveOrUpdate(o);
            getSession().flush();
        }
    }

    @Override
    public void delete(Port port) {
        port.setIsActive(0L);
        getSession().saveOrUpdate(port);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Port findById(Long id) {
        Query query = getSession().getNamedQuery("Port.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Port) result.get(0);
        }
    }

     @SuppressWarnings("rawtypes")
    public Port findByCode(String code) {
        Query query = getSession().getNamedQuery("Port.findByCode");
        query.setParameter("code", code);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Port) result.get(0);
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<Port> getPortByNationalCode(String code) {
        if (code.isEmpty()) {
            return null;
        }

        String hql = "SELECT o FROM Port o WHERE o.nationalCode = :code AND o.isActive = 1 "
                + " ORDER BY o.name ASC";

        Query query = getSession().createQuery(hql);
        query.setParameter("code", code);
        List<Port> result = query.list();
        return result;
    }
}
