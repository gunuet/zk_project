/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.sys.BO.ProductTarget;
import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author ChucHV
 */
public class ProductTargetDAOHE extends GenericDAOHibernate<ProductTarget, Long> {

    public ProductTargetDAOHE() {
        super(ProductTarget.class);
    }

    @Override
    public void saveOrUpdate(ProductTarget o) {
        if (o != null) {
            super.saveOrUpdate(o);
            getSession().getTransaction().commit();
        }
    }

    @Override
    public void delete(ProductTarget port) {
        port.setIsActive(0L);
        getSession().saveOrUpdate(port);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public ProductTarget findById(Long id) {
        Query query = getSession().getNamedQuery("ProductTarget.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ProductTarget) result.get(0);
        }
    }

    @SuppressWarnings("unchecked")
    public List<ProductTarget> getProductTargetTypeCode(String typeCode) {
        if (typeCode.isEmpty()) {
            return null;
        }

        String hql = "SELECT o FROM ProductTarget o WHERE o.type = :code AND o.isActive = 1 "
                + " ORDER BY o.name ASC";

        Query query = getSession().createQuery(hql);
        query.setParameter("code", typeCode);
        List<ProductTarget> result = query.list();
        return result;
    }
}
