/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Register;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class RegisterDAOHE extends GenericDAOHibernate<Register, Long> {

    public RegisterDAOHE() {
        super(Register.class);
    }

    @Override
    public void saveOrUpdate(Register register) {
        if (register != null) {
            super.saveOrUpdate(register);
        }
        getSession().flush();
    }


    public Register findViewByFileId(Long registerId) {
        Query query = getSession().createQuery("select a from Register a where a.registerId = :registerId");
        query.setParameter("registerId", registerId);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Register) result.get(0);
        }
    }

    public PagingListModel search(Register searchForm, int start, int take) {
        List listParam = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder("select r from Register r where 1 = 1  ");
            StringBuilder strCountBuf = new StringBuilder("select count(r) from Register r where 1 = 1 ");
            StringBuilder hql = new StringBuilder();
            if (searchForm != null) {
                if (searchForm.getBusinessNameVi() != null && !"".equals(searchForm.getBusinessNameVi())) {
                    hql.append(" and lower(r.businessNameVi) like ? ");
                    listParam.add(StringUtils.toLikeString(searchForm.getBusinessNameVi()));
                }
                if (searchForm.getBusinessTaxCode() != null && !"".equals(searchForm.getBusinessTaxCode())) {
                    hql.append(" and lower(r.businessTaxCode) like ? ");
                    listParam.add(StringUtils.toLikeString(searchForm.getBusinessTaxCode()));
                }
                if (searchForm.getUserFullName() != null && !"".equals(searchForm.getUserFullName())) {
                    hql.append(" and lower(r.userFullName) like ? ");
                    listParam.add(StringUtils.toLikeString(searchForm.getUserFullName()));
                }
                if (searchForm.getManageEmail() != null && !"".equals(searchForm.getManageEmail())) {
                    hql.append(" and lower(r.manageEmail) like ? ");
                    listParam.add(StringUtils.toLikeString(searchForm.getManageEmail()));
                }
                if (searchForm.getStatus() != null && searchForm.getStatus() != -2L) {
                    hql.append(" and lower(r.status) like ? ");
                    listParam.add(searchForm.getStatus());
                }
            }
            hql.append(" order by r.dateCreate DESC, r.businessTaxCode ");
            strBuf.append(hql);
            strCountBuf.append(hql);

            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
    
    public boolean checkBusinessTaxCode(String businessTaxCode)
    {
        String HQL="SELECT COUNT(d) FROM Register d WHERE Status IN(0,1) AND businessTaxCode =:businessTaxCode";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("businessTaxCode", businessTaxCode);
        Long count=(Long)query.list().get(0);
        if(count>0) return true;
        return false;
    }

}
