/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.rapidtest.BO.Template;
import com.viettel.utils.Constants;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class TemplateDAOHE extends GenericDAOHibernate<Template, Long> {

    public TemplateDAOHE() {
        super(Template.class);
    }

    public boolean hasDuplicate(Template rs) {
        StringBuilder hql = new StringBuilder("select count(r) from Template r where r.isActive = 1 ");
        List lstParams = new ArrayList();
        if (rs.getTemplateId() != null && rs.getTemplateId() > 0L) {
            hql.append(" and r.templateId <> ?");
            lstParams.add(rs.getTemplateId());
        }

        if (rs.getDeptId() != null && rs.getProcedureId() != 0) {
            hql.append(" and (r.deptId = ? and r.procedureId = ? and r.templateTypeId = ?) ");
            lstParams.add(rs.getDeptId());
            lstParams.add(rs.getProcedureId());
            lstParams.add(rs.getTemplateTypeId());
        }

        Query query = session.createQuery(hql.toString());
        for (int i = 0; i < lstParams.size(); i++) {
            query.setParameter(i, lstParams.get(i));
        }
        Long count = (Long) query.uniqueResult();
        return count > 0L;
    }

    public void delete(Long id) {
        Template obj = findById(id);
        obj.setIsActive(0l);
        update(obj);
    }

    public PagingListModel search(Template searchForm, int start, int take) {
        List listParam = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder("select r from Template r where r.isActive = 1 ");
            StringBuilder strCountBuf = new StringBuilder("select count(r) from Template r where r.isActive = 1 ");
            StringBuilder hql = new StringBuilder();
            if (searchForm != null) {
                if (searchForm.getProcedureId() != null && searchForm.getProcedureId() > 0l) {
                    hql.append(" and r.procedureId = ? ");
                    listParam.add(searchForm.getProcedureId());
                }

                if (searchForm.getTemplateName() != null && !"".equals(searchForm.getTemplateName())) {
                    hql.append(" and lower(r.templateName) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getTemplateName()));
                }

            }
            strBuf.append(hql);
            strCountBuf.append(hql);

            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public String findPathTemplate( Long templateTypeId) {
        Query query = getSession().createQuery("select b from Template a, Attachs b where "
                + " a.templateTypeId = :templateTypeId "
                + "and a.templateId=b.objectId and b.attachCat = :attachCat and b.isActive = :isActive");
        query.setParameter("templateTypeId", templateTypeId);
        query.setParameter("attachCat", Constants.OBJECT_TYPE.TEMPLATE);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List<Attachs> lstAttach = query.list();
        if (lstAttach != null && lstAttach.size() > 0) {
            Attachs att = lstAttach.get(0);
            String path = att.getFullPathFile();
            return path;
        }
        return null;
    }
    
    public String findPathTemplate(Long deptId, Long procedureId, Long templateTypeId) {
        Query query = getSession().createQuery("select b from Template a, Attachs b where a.deptId = :deptId "
                + "and a.procedureId = :procedureId and a.templateTypeId = :templateTypeId "
                + "and a.templateId=b.objectId and b.attachCat = :attachCat and b.isActive = :isActive");
        query.setParameter("deptId", deptId);
        query.setParameter("procedureId", procedureId);
        query.setParameter("templateTypeId", templateTypeId);
        query.setParameter("attachCat", Constants.OBJECT_TYPE.TEMPLATE);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List<Attachs> lstAttach = query.list();
        if (lstAttach != null && lstAttach.size() > 0) {
            Attachs att = lstAttach.get(0);
            String path = att.getFullPathFile();
            return path;
        }
        return null;
    }
    
    
}
