/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.sys.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.core.sys.BO.ActionLog;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.Applications;
import com.viettel.core.sys.BO.XmlMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author ChucHV
 */
public class XmlMessageDAOHE extends GenericDAOHibernate<XmlMessage, Long> {

    public XmlMessageDAOHE() {
        super(XmlMessage.class);
    }


    @Override
    public void saveOrUpdate(XmlMessage xmlMessage) {
        if (xmlMessage != null) {
            super.saveOrUpdate(xmlMessage);
            getSession().flush();
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
   	public PagingListModel search(XmlMessage searchForm, int start, int take) {
   		List listParam = new ArrayList();
           try {
               StringBuilder strBuf = new StringBuilder("select r from XmlMessage r where 1 = 1 ");
               StringBuilder strCountBuf = new StringBuilder("select count(r) from XmlMessage r where 1 = 1 ");
               StringBuilder hql = new StringBuilder();
               if (searchForm != null) {

                   if (searchForm.getFunc() != null) {
                       hql.append(" and lower(r.func) like ? escape '/' ");
                       listParam.add(StringUtils.toLikeString(searchForm.getFunc()));
                   }
                   if (searchForm.getType() != null) {
                       hql.append(" and lower(r.type) like ? escape '/' ");
                       listParam.add(StringUtils.toLikeString(searchForm.getType()));
                   }
                   if (searchForm.getNswFileCode() != null) {
                       hql.append(" and lower(r.nswFileCode) like ? escape '/' ");
                       listParam.add(StringUtils.toLikeString(searchForm.getNswFileCode()));
                   }
               }
               hql.append(" ORDER BY id desc ");
               strBuf.append(hql);
               strCountBuf.append(hql);

               Query query = session.createQuery(strBuf.toString());
               Query countQuery = session.createQuery(strCountBuf.toString());

               for (int i = 0; i < listParam.size(); i++) {
                   query.setParameter(i, listParam.get(i));
                   countQuery.setParameter(i, listParam.get(i));
               }

               query.setFirstResult(start);
               if (take < Integer.MAX_VALUE) {
                   query.setMaxResults(take);
               }

               List lst = query.list();
               Long count = (Long) countQuery.uniqueResult();
               PagingListModel model = new PagingListModel(lst, count);
               return model;
           } catch (Exception ex) {
               LogUtils.addLog(ex);
               return null;
           }

    }
}
