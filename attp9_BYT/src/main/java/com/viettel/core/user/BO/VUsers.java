/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.user.BO;

import java.io.Serializable;
import java.util.Date;
import java.util.Random;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author HaVM2
 */
@Entity
@Table(name = "V_USERS")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "VUsers.findAll", query = "SELECT u FROM VUsers u"),
		@NamedQuery(name = "VUsers.findByUserId", query = "SELECT u FROM VUsers u WHERE u.userId = :userId"),
		@NamedQuery(name = "VUsers.findByUserName", query = "SELECT u FROM VUsers u WHERE u.userName = :userName"),
		@NamedQuery(name = "VUsers.findByFullName", query = "SELECT u FROM VUsers u WHERE u.fullName = :fullName"),
		@NamedQuery(name = "VUsers.findByGender", query = "SELECT u FROM VUsers u WHERE u.gender = :gender"),
		@NamedQuery(name = "VUsers.findByDeptId", query = "SELECT u FROM VUsers u WHERE u.deptId = :deptId"),
		@NamedQuery(name = "VUsers.findByDeptName", query = "SELECT u FROM VUsers u WHERE u.deptName = :deptName"),
		@NamedQuery(name = "VUsers.findByPosId", query = "SELECT u FROM VUsers u WHERE u.posId = :posId"),
		@NamedQuery(name = "VUsers.findByPosName", query = "SELECT u FROM VUsers u WHERE u.posName = :posName"),
		@NamedQuery(name = "VUsers.findByEmail", query = "SELECT u FROM VUsers u WHERE u.email = :email"),
		@NamedQuery(name = "VUsers.findByTelephone", query = "SELECT u FROM VUsers u WHERE u.telephone = :telephone"),
		@NamedQuery(name = "VUsers.findByPassword", query = "SELECT u FROM VUsers u WHERE u.password = :password"),
		@NamedQuery(name = "VUsers.findByPasswordChanged", query = "SELECT u FROM VUsers u WHERE u.passwordChanged = :passwordChanged"),
		@NamedQuery(name = "VUsers.findByLastResetPassword", query = "SELECT u FROM VUsers u WHERE u.lastResetPassword = :lastResetPassword"),
		@NamedQuery(name = "VUsers.findByAvartarPath", query = "SELECT u FROM VUsers u WHERE u.avartarPath = :avartarPath"),
		@NamedQuery(name = "VUsers.findByStaffCode", query = "SELECT u FROM VUsers u WHERE u.staffCode = :staffCode"),
		@NamedQuery(name = "VUsers.findByIp", query = "SELECT u FROM VUsers u WHERE u.ip = :ip"),
		@NamedQuery(name = "VUsers.findByStatus", query = "SELECT u FROM VUsers u WHERE u.status = :status") })
public class VUsers implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "USER_ID")
	private Long userId;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "USER_NAME")
	private String userName;
	@Size(max = 500)
	@Column(name = "FULL_NAME")
	private String fullName;
	@Column(name = "GENDER")
	private Long gender;
	@Column(name = "DEPT_ID")
	private Long deptId;
	@Size(max = 300)
	@Column(name = "DEPT_NAME")
	private String deptName;
	@Column(name = "POS_ID")
	private Long posId;
	@Size(max = 400)
	@Column(name = "POS_NAME")
	private String posName;
	@Size(max = 50)
	@Column(name = "EMAIL")
	private String email;
	@Size(max = 30)
	@Column(name = "TELEPHONE")
	private String telephone;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "PASSWORD")
	private String password;
	@Basic(optional = false)
	@NotNull
	@Column(name = "PASSWORD_CHANGED")
	private short passwordChanged;
	@Column(name = "LAST_RESET_PASSWORD")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastResetPassword;
	@Size(max = 500)
	@Column(name = "AVARTAR_PATH")
	private String avartarPath;
	@Size(max = 50)
	@Column(name = "STAFF_CODE")
	private String staffCode;
	@Size(max = 1000)
	@Column(name = "IP")
	private String ip;
	@Basic(optional = false)
	@NotNull
	@Column(name = "STATUS")
	private Long status;
	@Column(name = "BIRTHDAY")
	@Temporal(TemporalType.DATE)
	private Date birthday;
	@Column(name = "IDNUMBER")
	private String idNumber;
	@Transient
	private int random;
	@Column(name = "USER_TYPE")
	private Long userType;
	@Column(name = "BUSINESS_ID")
	private Long businessId;
	@Column(name = "BUSINESS_NAME")
	private String businessName;
	@Column(name = "ROLE")
	private String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public VUsers() {
	}

	public VUsers(Long userId) {
		this.userId = userId;
	}

	public VUsers(Long deptId, String deptName) {
		this.deptId = deptId;
		this.deptName = deptName;
	}

	public VUsers(Long userId, String userName, Long deptId, String deptName,
			String posName) {
		this.userId = userId;
		this.userName = userName;
		this.deptId = deptId;
		this.deptName = deptName;
		this.posName = posName;
	}

	public VUsers(String fullName, Long userId) {
		this.fullName = fullName;
		this.userId = userId;
	}

	public VUsers(Long userId, String userName, String password,
			short passwordChanged, Long status) {
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.passwordChanged = passwordChanged;
		this.status = status;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getGender() {
		return gender;
	}

	public void setGender(Long gender) {
		this.gender = gender;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Long getPosId() {
		return posId;
	}

	public void setPosId(Long posId) {
		this.posId = posId;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public short getPasswordChanged() {
		return passwordChanged;
	}

	public void setPasswordChanged(short passwordChanged) {
		this.passwordChanged = passwordChanged;
	}

	public Date getLastResetPassword() {
		return lastResetPassword;
	}

	public void setLastResetPassword(Date lastResetPassword) {
		this.lastResetPassword = lastResetPassword;
	}

	public String getAvartarPath() {
		return avartarPath;
	}

	public void setAvartarPath(String avartarPath) {
		this.avartarPath = avartarPath;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public int getRandom() {
		Random ran = new java.security.SecureRandom();
		return ran.nextInt(100);
	}

	public void setRandom(int random) {
		this.random = random;
	}

	public Long getUserType() {
		return userType;
	}

	public void setUserType(Long userType) {
		this.userType = userType;
	}

	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (userId != null ? userId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof VUsers)) {
			return false;
		}
		VUsers other = (VUsers) object;
		if ((this.userId == null && other.userId != null)
				|| (this.userId != null && !this.userId.equals(other.userId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.viettel.voffice.BO.VUsers[ userId=" + userId + " ]";
	}
}
