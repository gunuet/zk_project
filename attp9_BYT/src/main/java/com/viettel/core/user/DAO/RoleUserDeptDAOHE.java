/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.user.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.core.user.BO.RoleUserDept;
import com.viettel.utils.ResourceBundleUtil;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author HaVM2
 */
public class RoleUserDeptDAOHE extends GenericDAOHibernate<RoleUserDept, Long> {

	public RoleUserDeptDAOHE() {
		super(RoleUserDept.class);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean isFileClerk(Long userId, Long deptId) {
		StringBuilder hqlBuilder = new StringBuilder(
				"SELECT ro FROM RoleObject ro WHERE "
						+ " ro.roleId IN (SELECT rud.roleId FROM RoleUserDept rud WHERE rud.userId = ? AND rud.deptId = ? AND rud.isActive = 1)"
						+ " AND ro.isActive = 1 AND ro.objectId = ? ");
		Query query = getSession().createQuery(hqlBuilder.toString());
		List listParams = new ArrayList<>();
		listParams.add(userId);
		listParams.add(deptId);
		listParams.add(Constants.OBJECT_ID.TIEP_NHAN_VAN_BAN);
		for (int i = 0; i < listParams.size(); i++) {
			query.setParameter(i, listParams.get(i));
		}
		try {
			List result = query.list();
			if (result.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} catch (Exception ex) {
                        LogUtils.addLogDB(ex);
			return false;
		}
	}

	/**
	 * hoangnv28 Tìm tất cả vai trò của người dùng thuộc 1 đơn vị
	 * 
	 * @param userId
	 * @param deptId
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List findRoleUserDept(Long userId, Long deptId) {
		StringBuilder hqlBuilder = new StringBuilder(
				"SELECT rud FROM RoleUserDept rud WHERE "
						+ " rud.userId = ? AND rud.deptId = ? AND rud.isActive = 1 ");
		List listParams = new ArrayList<>();
		listParams.add(userId);
		listParams.add(deptId);
		Query query = getSession().createQuery(hqlBuilder.toString());
		for (int i = 0; i < listParams.size(); i++) {
			query.setParameter(i, listParams.get(i));
		}
		try {
			return query.list();
		} catch (Exception ex) {
                        LogUtils.addLogDB(ex);
			return new ArrayList<>();
		}
	}
        /**
         * Kiểm tra quyền hủy hồ sơ
         * @param userId
         * @param deptId
         * @return 
         */
        public boolean checkRoleCanDeleteFile(Long userId, Long deptId) throws UnsupportedEncodingException {
		StringBuilder hqlBuilder = new StringBuilder(
				"SELECT ro FROM Roles ro WHERE "
						+ " ro.roleId IN (SELECT rud.roleId FROM RoleUserDept rud WHERE rud.userId = ? AND rud.deptId = ? AND rud.isActive = 1)"
						+ " AND  ro.roleCode = ? ");
		Query query = getSession().createQuery(hqlBuilder.toString());
		List listParams = new ArrayList<>();
		listParams.add(userId);
		listParams.add(deptId);
		listParams.add(ResourceBundleUtil.getString("ROLE_CAN_DELETE_FILE", "config"));
		for (int i = 0; i < listParams.size(); i++) {
			query.setParameter(i, listParams.get(i));
		}
		try {
			List result = query.list();
			if (result.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} catch (Exception ex) {
                        LogUtils.addLogDB(ex);
			return false;
		}
	}
}
