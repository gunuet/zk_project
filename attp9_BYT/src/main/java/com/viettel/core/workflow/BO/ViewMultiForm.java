/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.core.workflow.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "VIEW_MULTI_FORM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ViewMultiForm.findAll", query = "SELECT v FROM ViewMultiForm v"),
    @NamedQuery(name = "ViewMultiForm.findById", query = "SELECT v FROM ViewMultiForm v WHERE v.id = :id"),
    @NamedQuery(name = "ViewMultiForm.findByStatus", query = "SELECT v FROM ViewMultiForm v WHERE v.status = :status"),
    @NamedQuery(name = "ViewMultiForm.findByFileType", query = "SELECT v FROM ViewMultiForm v WHERE v.fileType = :fileType"),
    @NamedQuery(name = "ViewMultiForm.findByFormName", query = "SELECT v FROM ViewMultiForm v WHERE v.formName = :formName"),
    @NamedQuery(name = "ViewMultiForm.findByCreateDate", query = "SELECT v FROM ViewMultiForm v WHERE v.createDate = :createDate"),
    @NamedQuery(name = "ViewMultiForm.findByIsActive", query = "SELECT v FROM ViewMultiForm v WHERE v.isActive = :isActive")})
public class ViewMultiForm implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
   
    @SequenceGenerator(name = "VIEW_MULTI_FORM_SEQ", sequenceName = "VIEW_MULTI_FORM_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VIEW_MULTI_FORM_SEQ")
    @Column(name = "ID")
    private Long id;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Size(max = 1000)
    @Column(name = "FORM_NAME")
    private String formName;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Column(name = "IS_ACTIVE")
    private Long isActive;

    public ViewMultiForm() {
    }

    public ViewMultiForm(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ViewMultiForm)) {
            return false;
        }
        ViewMultiForm other = (ViewMultiForm) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.core.workflow.BO.ViewMultiForm[ id=" + id + " ]";
    }
    
}
