/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.workflow.DAO;

import com.viettel.utils.Constants;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.BO.Node;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.user.BO.Users;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.ws.BO.ErrorSendMs;
import com.viettel.ws.ErrorSendMsDAO;

import java.util.Date;
import java.util.List;

import org.apache.xml.security.exceptions.Base64DecodingException;
import org.bouncycastle.cms.jcajce.JcaSignerInfoVerifierBuilder;
import org.hibernate.Query;

import com.viettel.ws.Helper;
import com.viettel.ws.Helper_QTest;
import com.viettel.ws.Helper_VofficeMoh;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author duv
 */
public class CoreProcessDAOHE extends GenericDAOHibernate<Process, Long> {

    public CoreProcessDAOHE() {
        super(Process.class);
    }

    public CoreProcessDAOHE(Class<Process> type) {
        super(type);
    }

    // Tim node bat dau cua 1 luong
    // Tham so: flow object
    public Node getFirstNodeOfFlow(Flow flow) {
        Node firstNode;
        firstNode = this.getFirstNodeOfFlow(flow.getFlowId());
        return firstNode;
    }

    // Tim node bat dau cua 1 luong 
    // Tham so: flowId & kieu cua node=NODE_TYPE_START
    public Node getFirstNodeOfFlow(Long flowId) {
        Node firstNode = null;
        String hql = " select n from Node n where n.isActive = 1 and "
                + " n.type=:type and n.flowId = :flowId ";
        Query query = session.createQuery(hql);
        query.setParameter("flowId", flowId);
        query.setParameter("type", Constants.NODE_TYPE.NODE_TYPE_START);
        List<Node> listNodes = query.list();
        if (listNodes != null && !listNodes.isEmpty()) {
            firstNode = listNodes.get(0);
        }
        return firstNode;
    }

    // Tim cac NodeDeptUser tuong ung voi 1 node trong luong 
    public List<NodeDeptUser> getNodeDeptUserOfNode(Long nodeId) {
        String hql = " select ndu from NodeDeptUser ndu where "
                + " ndu.nodeId=:nodeId ";
        Query query = session.createQuery(hql);
        query.setParameter("nodeId", nodeId);
        List<NodeDeptUser> listNodes = query.list();
        return listNodes;
    }

    // Tim cac user theo chuc danh POS_ID & DEPT_ID duoc cau hinh trong 1 node
    public List<Users> getUsersOfNodeByPos(NodeDeptUser node, Long posId) {
        UserDAOHE userDAOHE = new UserDAOHE();
        List<Users> listNodes = userDAOHE.getUserByDeptPosID(node.getDeptId(), posId);
        return listNodes;
    }

    // Tim cac user theo DEPT_ID duoc cau hinh trogn 1 node
    public List<Users> getUsersOfNodeByDept(NodeDeptUser node) {
        String hql = " select u from Users u where u.status = 1 and "
                + " u.deptId=:deptId ";
        Query query = session.createQuery(hql);
        query.setParameter("deptId", node.getDeptId());
        List<Users> listUsers = query.list();
        return listUsers;
    }

    // Tim tat ca cac ban ghi xu ly trong luong 
    // order by orderProcess de khi can co the lay gia tri lon nhat cua order
    public List<Process> getProcessByFlow(Long flowId, Long objectId) {
        String hql = " select p from Process p where "
                + " p.objectId=:objectId and "
                + " p.nodeId in "
                + " ( select n.nodeId from Node n where n.flowId=:flowId "
                + " ) "
                + " order by p.orderProcess ASC";
        Query query = session.createQuery(hql);
        query.setParameter("objectId", objectId);
        query.setParameter("flowId", flowId);
        List<Process> listProcess = query.list();
        return listProcess;
    }

    // Tim tat ca cac ban ghi xu ly trong luong 
    // order by orderProcess de khi can co the lay gia tri lon nhat cua order
    public List<Process> getProcessByObjType(Long objectId, Long objectType) {
        String hql = " select p from Process p where "
                + " p.objectId=:objectId and "
                + " p.objectType=:objectType "
                + " order by p.orderProcess ASC";
        Query query = session.createQuery(hql);
        query.setParameter("objectId", objectId);
        query.setParameter("objectType", objectType);
        List<Process> listProcess = query.list();
        return listProcess;
    }

    public Process insertProcess(
            Long objectId, Long objectType,
            Long sendUserId, String sendUsername,
            Long sendGroupId, String sendGroupname,
            Long receiveUserId, String receiveUsername,
            Long receiveGroupId, String receiveGroupname,
            Long nodeId, Long processParentId,
            Long processType, Long status,
            Date deadline, Date finish,
            Long actionType, String actionName,
            Long order, String note,
            Long previousNodeId,
            MessageModel message, Boolean re, Boolean active, Boolean fn) {
        Process process = new Process();
        process.setObjectId(objectId);
        process.setObjectType(objectType);

        process.setSendUserId(sendUserId);
        process.setSendUser(sendUsername);
        process.setSendGroupId(sendGroupId);
        process.setSendGroup(sendGroupname);

        process.setReceiveUserId(receiveUserId);
        process.setReceiveUser(receiveUsername);
        process.setReceiveGroupId(receiveGroupId);
        process.setReceiveGroup(receiveGroupname);

        process.setNodeId(nodeId);
        process.setPreviousNodeId(previousNodeId);
        process.setParentId(processParentId);

        process.setProcessType(processType);
        process.setStatus(status);

        process.setSendDate(new Date());
        if (deadline != null) {
            process.setDeadline(deadline);
        }
        if (finish != null) {
            process.setFinishDate(finish);
        }
        if (!active || fn) {
            process.setFinishDate(new Date());
        }
        process.setActionType(actionType);
        process.setActionName(actionName);

        process.setOrderProcess(order);
        process.setNote(note);

        process.setIsActive(Constants.Status.ACTIVE);

        try {
            ProcessDAOHE processDAOHE = new ProcessDAOHE();
            processDAOHE.saveOrUpdate(process);
            //linhdx
            try {
                if (message != null && message.getCode() != null && !re && active) {
                    Boolean k = callSendMessageToBusiness(message,
                            process.getProcessId());
                    if (k.equals(false)) {
                        ErrorSendMsDAO errDAO = new ErrorSendMsDAO();
                        ErrorSendMs err = new ErrorSendMs();
                        err.setMsFunction(message.getFunctionName());
                        err.setMsId(process.getProcessId().toString());
                        err.setMsCode(objectType == null ? null : objectType.toString());
                        err.setActions(actionName);
                        err.setFileId(objectId);
                        err.setMsPhase(message.getPhase() == null ? null : message.getPhase().toString());
                        err.setMsUpdate(message.isFeeUpdate() == true ? 1L : 0L);
                        errDAO.saveOrUpdate(err);
                        //    if ("true".equals(ResourceBundleUtil.getString("send_service", "config"))) {
                        processDAOHE.delete(process);
                        process = null;
                        Process prevProcess = processDAOHE.findById(processParentId);
                        if (prevProcess == null) {
                            prevProcess = processDAOHE.getLastByUserId(objectId, sendUserId);
                        }
                        if (prevProcess != null) {
                            prevProcess.setFinishDate(null);
                            processDAOHE.saveOrUpdate(prevProcess);
                        }
                        //}
                    }
                }
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }

            return process;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    private Boolean callSendMessageToBusiness(MessageModel message, Long processId) throws FileNotFoundException, IOException, Base64DecodingException, Exception {
        switch (message.getCode()) {
            case Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT:
                Helper helper = new Helper();
                switch (message.getFunctionName()) {
                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_04:// Tao moi van ban
                        return helper.senMS_04(message.getFileId(), message.getPhase(), processId, message.isFeeUpdate());
                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_38:// Tao moi van ban
                        return helper.senMS_38(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_11_12_13_14:// Tao moi van ban
                        return helper.senMS_11_12_13_14(message.getFileId(), message.isFeeUpdate(), message.getPhase(), processId);

                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_10:// Tao moi van ban
                        return helper.senMS_10(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_23:// Tao moi van ban
                        return helper.senMS_23(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_15:// Tao moi van ban
                        return helper.senMS_15(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_16:// Tao moi van ban
                        return helper.senMS_16(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_24:// Tao moi van ban
                        return helper.senMS_24(message.getFileId(), processId);
                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_18:// Tao moi van ban
                        return helper.senMS_18(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_26:// dong y xu ly doi voi mat hang khong dat
                        return helper.senMS_26(message.getFileId(), processId);
                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_27:// yeu cau bo sung xu ly voi mat hang khong dat
                        return helper.senMS_27(message.getFileId(), processId);
                    case Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_29:// dong y voi bao cao xu ly mat hang khong dat
                        return helper.senMS_29(message.getFileId(), processId);
                }
                break;
            case Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT:
                Helper_QTest helperQ = new Helper_QTest();
                switch (message.getFunctionName()) {
                    case Constants.FUNCTION_MESSAGE_RT.SendMs_03:// Tao moi van ban
                        return helperQ.sendMs_03(message.getFileId(), processId);
                    case Constants.FUNCTION_MESSAGE_RT.SendMs_04:// Tao moi van ban
                        return helperQ.sendMs_04(message.getFileId(), processId);
                    case Constants.FUNCTION_MESSAGE_RT.SendMs_05:// Tao moi van ban
                        return helperQ.sendMs_05(message.getFileId(), processId, message.getPhase());

                    case Constants.FUNCTION_MESSAGE_RT.SendMs_06:// Tao moi van ban
                        return helperQ.sendMs_06(message.getFileId(), processId);
                    case Constants.FUNCTION_MESSAGE_RT.SendMs_07:// Tao moi van ban
                        return helperQ.sendMs_07(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_RT.SendMs_30:// Tao moi van ban
                        return helperQ.sendMs_30(message.getFileId(), processId, message.isFeeUpdate());

                    case Constants.FUNCTION_MESSAGE_RT.SendMs_11:// Tao moi van ban
                        return helperQ.sendMs_11(message.getFileId(), processId);

//                    case "sendMs_31":// Tao moi van ban
//                        return helperQ.sendMs_31(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_RT.SendMs_37:// Tao moi van ban
                        return helperQ.sendMs_37(message.getFileId(), processId);

                }
                break;
            case Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT:
                Helper_VofficeMoh hpMOH = new Helper_VofficeMoh();
                switch (message.getFunctionName()) {
                    case Constants.FUNCTION_MESSAGE_ID.SendMs_39:// Tao moi van ban
                        return hpMOH.SendMs_39(message.getFileId(), message.getFileId(), message.getPhase());
                    case Constants.FUNCTION_MESSAGE_ID.SendMs_42:// Tao moi van ban
                        return hpMOH.sendMS_42(message.getFileId(), processId);

                    case Constants.FUNCTION_MESSAGE_ID.SendMs_43:// Tao moi van ban
                        return hpMOH.sendMS_43(message.getFileId(), message.getPhase(), processId);

                }
                break;
            default:
                return false;
        }
        return false;
    }

    public List<Node> findNodeByFlowIdNStatus(Long flowId, Long status) {
        String hql = "SELECT n FROM Node n WHERE n.flowId = :flowId AND "
                + " n.status =:status";
        Query query = getSession().createQuery(hql);
        query.setParameter("flowId", flowId);
        query.setParameter("status", status);
        List<Node> listNodes = query.list();
        return listNodes;
    }

    public List<Node> findNodeByFlowIdNActionStatus(Long flowId, Long status) {
        String hql = "SELECT n FROM Node n WHERE n.flowId =:flowId AND"
                + " n.nodeId IN ("
                + " SELECT f.nextId FROM NodeToNode f "
                + " WHERE f.status =:status) ";
        Query query = getSession().createQuery(hql);
        query.setParameter("flowId", flowId);
        query.setParameter("status", status);
        List<Node> listNodes = query.list();
        return listNodes;
    }

    public List<Node> findNodeByFlowIdNActionStatus(Long flowId, List<Long> lstStatus) {
        String hql = "SELECT n FROM Node n WHERE n.flowId =:flowId AND"
                + " n.nodeId IN ("
                + " SELECT f.nextId FROM NodeToNode f "
                + " WHERE f.isActive = 1 and f.status in (:lstStatus)) ";
        Query query = getSession().createQuery(hql);
        query.setParameter("flowId", flowId);
        query.setParameterList("lstStatus", lstStatus);
        List<Node> listNodes = query.list();
        return listNodes;
    }

    public List<Process> findProcessParent(Long nodeId, Long status) {
        String hql = "SELECT p FROM Process p WHERE p.processId IN "
                + "(SELECT a.parentId FROM Process a WHERE "
                + " a.nodeId =:nodeId AND "
                + " a.status =:status"
                + ")";
        Query query = getSession().createQuery(hql);
        query.setParameter("nodeId", nodeId);
        query.setParameter("status", status);
        List<Process> listNodes = query.list();
        return listNodes;
    }

    public List<Process> findPreviousProcess(Long docId, Long docType, List<Long> lstStatus) {
        String hql = "SELECT p FROM Process p WHERE "
                + " p.objectId =:docId AND "
                + " p.objectType =:docType AND "
                + " p.status in (:lstStatus) AND p.finishDate is null"
                + " ORDER BY p.sendDate, p.orderProcess ";
        Query query = getSession().createQuery(hql);
        query.setParameter("docId", docId);
        query.setParameter("docType", docType);
        if (lstStatus.size() == 0) {
            lstStatus.add(-1L);
            //tao 1 ban ghi de cau truy van status in khong bi treo
        }
        query.setParameterList("lstStatus", lstStatus);
        List<Process> listNodes = query.list();
        return listNodes;
    }

    public List<Process> findALlCurrentProcess(Long docId, Long docType, Long status) {
        String hql;

        if (!docType.equals(Constants.DOC_TYPE_CODE_THEMMOI)) {
            hql = "SELECT p FROM Process p WHERE "
                    + " p.objectId =:docId AND "
                    + " p.objectType =:docType AND "
                    + " p.status =:status "
                    + " ORDER BY p.sendDate, p.orderProcess ";
        } else {
            hql = "SELECT p FROM Process p WHERE "
                    + " p.objectId =:docId AND "
                    + " p.objectType in (:docType," + Constants.DOC_TYPE_CODE_GIAHAN + "," + Constants.DOC_TYPE_CODE_BOSUNG + ") AND "
                    + " p.status =:status "
                    + " ORDER BY p.sendDate, p.orderProcess ";
        }

        Query query = getSession().createQuery(hql);
        query.setParameter("docId", docId);
        query.setParameter("docType", docType);
        query.setParameter("status", status);
        List<Process> listNodes = query.list();
        return listNodes;
    }

    /*
     * public Long findFlowIdByObjectId(Long objectId){ String hql = "SELECT
     * f.flow_id FROM YcnkFile f WHERE " + " f.file_id = :objectId"; Query query
     * = getSession().createQuery(hql); query.setParameter("objectId",
     * objectId); Long flowId = (Long)query.uniqueResult(); return flowId; }
     */
    public List<NodeToNode> findActionsByNodeId(Long nodeId) {
		String hql = "SELECT n FROM NodeToNode n WHERE "
                + " n.isActive = 1 and n.previousId = :nodeId"
                + " and n.type != :type"
                + " order by n.action asc ";
        Query query = getSession().createQuery(hql);
        query.setParameter("nodeId", nodeId);
        query.setParameter("type", Constants.NODE_ASSOCIATE_TYPE.TRANSPARENT);
        List<NodeToNode> listActions = query.list();
        return listActions;
    }

    public List<NodeDeptUser> findNDUsByNodeId(Long nodeId, Boolean curentDept) {
        if (!curentDept) {
            String hql = "SELECT n FROM NodeDeptUser n WHERE "
                    + " n.nodeId = :nodeId";
            Query query = getSession().createQuery(hql);
            query.setParameter("nodeId", nodeId);
            List<NodeDeptUser> listNDUs = query.list();
            return listNDUs;
        } else {
            UserToken us = (UserToken) Sessions.getCurrent(true).getAttribute(
                    "userToken");
            DepartmentDAOHE depDAO = new DepartmentDAOHE();
            Department d = depDAO.findById(us.getDeptId());
            String hql = "SELECT n FROM NodeDeptUser n WHERE "
                    + " n.nodeId = :nodeId AND ( n.deptId = :deptId OR n.deptId = :parentdeptId)";
            Query query = getSession().createQuery(hql);
            query.setParameter("nodeId", nodeId);
            query.setParameter("deptId", us.getDeptId());
            query.setParameter("parentdeptId", d.getParentId());
            List<NodeDeptUser> listNDUs = query.list();
            return listNDUs;
        }
    }

    public List<NodeDeptUser> findNDUsByNodeId(Long nodeId, Long receiveUserId) {
        if (receiveUserId == null) {
            return findNDUsByNodeId(nodeId, false);
        }
        String hql = "SELECT n FROM NodeDeptUser n WHERE "
                + " n.nodeId = :nodeId AND n.userId = :userId ";
        Query query = getSession().createQuery(hql);
        query.setParameter("nodeId", nodeId);
        query.setParameter("userId", receiveUserId);
        List<NodeDeptUser> listNDUs = query.list();
        return listNDUs;
    }

    public Process findSourceProcess(Long documentId, Long documentType) {
        String hql = "SELECT p FROM Process p WHERE "
                + " p.objectId = :objectId AND "
                + " p.objectType = :objectType "
                + " ORDER BY p.processId ASC ";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", documentId);
        query.setParameter("objectType", documentType);
        List<Process> listProcess = query.list();
        return listProcess.get(0);
    }

    public Process findProcessByStatusAndReceiverId(Long documentId, Long documentType,
            Long status, Long receiverId) {
        String hql = "SELECT p FROM Process p WHERE "
                + " p.objectId = :objectId AND "
                + " p.objectType = :objectType AND "
                + " p.status = :status AND "
                + " p.receiveUserId = :receiverId "
                + " ORDER BY p.processId ASC ";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", documentId);
        query.setParameter("objectType", documentType);
        query.setParameter("status", status);
        query.setParameter("receiverId", receiverId);
        List<Process> listProcess = query.list();
        if (listProcess.isEmpty()) {
            return null;
        }
        return listProcess.get(0);
    }

    /**
     * linhdx tim danh sach trang tahai
     *
     * @param documentId
     * @param documentType
     * @param receiverId
     * @return
     */
    public List<Long> findListProcessStatusByReceiverId(Long documentId, Long documentType, Long receiverId) {
        String hql = "SELECT p.status FROM Process p WHERE "
                + " p.objectId = :objectId AND "
                + " p.objectType = :objectType AND "
                + //" p.finishDate is null AND " +
                " p.receiveUserId = :receiverId "
                + " ORDER BY p.processId ASC ";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", documentId);
        query.setParameter("objectType", documentType);
        query.setParameter("receiverId", receiverId);
        List<Long> lstStatus = query.list();
        return lstStatus;
    }

    /**
     * linhdx Tim danh sach process
     *
     * @param documentId
     * @param documentType
     * @param receiverId
     * @return
     */
    public List<Process> findListProcessByReceiverId(Long documentId, Long documentType, Long receiverId) {
        String hql = "SELECT p FROM Process p WHERE "
                + " p.objectId = :objectId AND "
                + " p.objectType = :objectType AND "
                + " p.finishDate is null AND "
                + " p.receiveUserId = :receiverId "
                + " ORDER BY p.processId ASC ";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", documentId);
        query.setParameter("objectType", documentType);
        query.setParameter("receiverId", receiverId);
        List<Process> lstProcess = query.list();
        return lstProcess;
    }

    public List<Process> findListProcessByReceiverIdAndStatus(Long documentId, Long documentType, Long receiverId, Long status) {
        String hql = "SELECT p FROM Process p WHERE "
                + " p.objectId = :objectId AND "
                + " p.objectType = :objectType AND "
                + " p.finishDate is null AND "
                + " p.receiveUserId = :receiverId "
                + " ORDER BY p.processId ASC ";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", documentId);
        query.setParameter("objectType", documentType);
        query.setParameter("receiverId", receiverId);
        List<Process> lstProcess = query.list();
        return lstProcess;
    }

    /**
     * linhdx Tim danh sach process chua hoan thanh xu lý
     *
     * @param documentId
     * @param documentType
     * @param receiverId
     * @return
     */
    public List<Process> findAllProcessNotFinish(Long documentId, Long documentType) {
        String hql = "SELECT p FROM Process p WHERE "
                + " p.objectId = :objectId AND "
                + " p.objectType = :objectType AND "
                + " p.finishDate is null "
                + " ORDER BY p.processId ASC ";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", documentId);
        query.setParameter("objectType", documentType);
        List<Process> lstProcess = query.list();
        return lstProcess;
    }

    public List<Process> findAllProcessContainFinish(Long documentId, Long documentType) {
        String hql = "SELECT p FROM Process p WHERE "
                + " p.objectId = :objectId AND "
                + " p.objectType = :objectType  "
                + " ORDER BY p.processId ASC ";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", documentId);
        query.setParameter("objectType", documentType);
        List<Process> lstProcess = query.list();
        return lstProcess;
    }

    /**
     * linhdx tim tat ca nodedeptuser khi biet userId
     *
     * @param userId
     * @return
     */
    public List<NodeDeptUser> findNDUsByUserId(List<Long> lstUserId, Long nodeId) {
        String hql = "SELECT n FROM NodeDeptUser n WHERE "
                + " n.userId in (:lstUserId)"
                + " and n.nodeId = :nodeId";
        Query query = getSession().createQuery(hql);
        query.setParameterList("lstUserId", lstUserId);
        query.setParameter("nodeId", nodeId);
        List<NodeDeptUser> listNDUs = query.list();
        return listNDUs;
    }
}
