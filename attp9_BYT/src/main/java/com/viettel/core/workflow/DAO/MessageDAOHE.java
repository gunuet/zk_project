/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.workflow.DAO;


import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.MessageEmail;

/**
 *
 * @author duv
 */
public class MessageDAOHE extends GenericDAOHibernate<MessageEmail, Long> {

    public MessageDAOHE() {
        super(MessageEmail.class);
    }

    public MessageDAOHE(Class<MessageEmail> type) {
        super(type);
    }
    
    public void updateMessageEmailFlag() {
        this.getSession().createQuery("update MessageEmailFlag set flag = 1").executeUpdate();
    }
    
}
