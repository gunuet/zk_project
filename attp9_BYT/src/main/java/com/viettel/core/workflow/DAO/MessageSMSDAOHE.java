/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.workflow.DAO;


import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.MessageSms;

/**
 *
 * @author duv
 */
public class MessageSMSDAOHE extends GenericDAOHibernate<MessageSms, Long> {

    public MessageSMSDAOHE() {
        super(MessageSms.class);
    }

    public MessageSMSDAOHE(Class<MessageSms> type) {
        super(type);
    }
    
    public void updateMessageSmsFlag() {
        this.getSession().createQuery("update MessageSmsFlag set flag = 1").executeUpdate();
    }
    
}
