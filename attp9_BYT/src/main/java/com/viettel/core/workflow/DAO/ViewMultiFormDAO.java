/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.workflow.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.workflow.BO.ViewMultiForm;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author E5420
 */
public class ViewMultiFormDAO extends GenericDAOHibernate<ViewMultiForm, Long> {

    public ViewMultiFormDAO() {
        super(ViewMultiForm.class);
    }

    @Override
    public void saveOrUpdate(ViewMultiForm form) {
        if (form != null) {
            super.saveOrUpdate(form);
        }
        getSession().flush();

    }

    @Override
    public ViewMultiForm findById(Long id) {
        Query query = getSession().getNamedQuery("ViewMultiForm.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ViewMultiForm) result.get(0);
        }
    }
    public ViewMultiForm getByStatusAndType(Long status,Long filetype)
    {
        String hql = "SELECT v FROM ViewMultiForm v WHERE v.isActive = 1 "
                + " AND v.status = :status "
                + " AND v.fileType = :fileType  ";  
        Query query = getSession().createQuery(hql);
        query.setParameter("status", status);
        query.setParameter("fileType", filetype);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ViewMultiForm) result.get(0);
        }
    }
}
