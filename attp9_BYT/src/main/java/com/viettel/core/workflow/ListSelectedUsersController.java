package com.viettel.core.workflow;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import com.viettel.utils.Constants;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Label;
import java.util.ArrayList;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Textbox;

public class ListSelectedUsersController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = -3120854764294466129L;

    @Wire
    private Listbox lbNodeDeptUser;
    private List<NodeDeptUser> listNDU;

    private String actionName;
    @Wire
    private Textbox lbNDU;

    @Listen("onChangeProcessType = #lbNodeDeptUser")
    public String onChangeProcessType(NodeDeptUser ndu) {
        // Neu la "Xin y kien" thi ko cho thay doi loai xu li
        if (isGetOpinion(actionName)) {
            return loadProcessTypeName(ndu.getProcessType());
        }

        if (ndu.getProcessType() == null) {
            return loadProcessTypeName(Constants.PROCESS_TYPE.COOPERATE);
        } else {
            if (ndu.getProcessType() < Constants.PROCESS_TYPE.RECEIVE_TO_KNOW) {
                ndu.setProcessType(ndu.getProcessType() + 1);
                return loadProcessTypeName(ndu.getProcessType());
            } else {
                ndu.setProcessType(Constants.PROCESS_TYPE.COOPERATE);
                return loadProcessTypeName(Constants.PROCESS_TYPE.COOPERATE);
            }
        }
    }

    @Listen("onLoadModel = #lbNodeDeptUser")
    public void onLoadModel(Event event) {
        Map<String, Object> arguments = (Map<String, Object>) event.getData();
        listNDU = (List<NodeDeptUser>) arguments.get("listNDU");
        actionName = (String) arguments.get("actionName");
        ListModelList model = new ListModelList(listNDU);
        lbNodeDeptUser.setModel(model);
        lbNodeDeptUser.renderAll();
        Gson gs = new Gson();
        lbNDU.setValue(gs.toJson(listNDU));
    }

    public void onDeleteListitem(int index) {
        Gson gs = new Gson();
        java.lang.reflect.Type listType = new TypeToken<ArrayList<NodeDeptUser>>() {
        }.getType();
        List<NodeDeptUser> ls = new Gson().fromJson(lbNDU.getValue(), listType);
        ls.remove(ls.get(index));
        lbNodeDeptUser.setModel(new ListModelList(ls));
        lbNDU.setValue(gs.toJson(ls));
    }

    @Listen("onAfterRender = #lbNodeDeptUser")
    public void onAfterRenderListbox() {
        NodeDeptUser ndu;
        for (Listitem item : lbNodeDeptUser.getItems()) {

            ndu = item.getValue();
            if (ndu != null && Objects.equals(ndu.getNodeDeptUserId(), -1L)) {
                // viethd3 18/03/2015
                // thay doi nghiep vu: trong danh sach cac NDU duoc add vao 
                // man hinh xu ly ProcessingWindow thi khong disable nhung 
                // NDU da duoc chon (duoc gui xu ly trong qua khu)
                // -> comment thiet lap setDisable
                //item.setDisabled(true);
            }
        }
    }

    public String loadProcessTypeName(Long processType) {
        return ProcessDAOHE.loadProcessTypeName(processType);
    }

    public boolean isGetOpinion(String actionName) {
        return "xin ý kiến".equals(actionName.toLowerCase());
    }

}
