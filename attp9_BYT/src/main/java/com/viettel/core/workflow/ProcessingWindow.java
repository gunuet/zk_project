/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.workflow;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Include;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox;
import com.google.common.base.Objects;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BO.Node;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.DAO.CoreProcessDAOHE;
import com.viettel.core.workflow.DAO.NodeDAOHE;
import com.viettel.core.workflow.DAO.NodeToNodeDAOHE;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zul.*;

/**
 *
 * @author duv
 */
public class ProcessingWindow extends BaseComposer {

    private static final long serialVersionUID = 1L;
    public static final String TREE_USER_SELECTOR_PAGE = "/Pages/core/workflow/treeUserSelector.zul";
    @Wire
    private Vlayout vlayoutMain;
    @Wire
    private Include incBusinessPage;
    @Wire
    private Window mainWindow;
    @Wire
    private Vlayout includeWindow;
    @Wire
    private Textbox txtNote;
    @Wire
    private Button btnOK, btnChoose;
    @Wire
    private Window windowProcessing;
    @Wire
    protected Include incSelectObjectsToSendProcess;
    //Cuongvv
    @Wire
    private Vlayout layoutCCV;
    @Wire
    private Listbox lbcvxlc;
    @Wire("#incUserSelector #wdListObjectsToSend #lbNodeDeptUser")
    protected Listbox lbNodeDeptUser;
    @Wire("#incUserSelector #wdListObjectsToSend #lbNDU")
    protected Textbox lbNDU;
    @Wire("#cbListNDU")
    protected Caption cbListNDU;
    @Wire("#grbListNDU")
    protected Groupbox grbListNDU;
    @Wire("#grbChoose")
    protected Groupbox grbChoose;
    @Wire("#incUserSelector #wdListObjectsToSend #lhDelete")
    protected Listheader lhDelete;
    @Wire("#incUserSelector #wdListObjectsToSend")
    protected Window wdListObjectsToSend;
    protected Window windowParent;
    protected Process process;
    protected Long docId;
    protected Long docType;
    protected Long actionId;
    protected String actionName;
    protected Long actionType;
    protected Long nextId;
    protected int typeProcess = 0;
    protected List<NodeToNode> listNodeToNode;
    protected List<NodeDeptUser> listAvailableNDU = new ArrayList();
    protected List<Long> listUserToSend;
    protected List<Long> listDeptToSend;
    private List<Process> listProcessCurrent;
    // private List<VRtPaymentInfo> listFileRtfile;
    private List<Long> listDocId, listDocType;
    private List<NodeToNode> lstNextAction;
    @Wire
    Listbox lbAction;
    private Map<String, Object> staticArgument;
    Map<String, Object> arguments;
    Map<String, Object> argumentsListLDU;
    // linhdx thong diep trao doi
    private MessageModel message;
    private Boolean curentDept = false;
    private Files files;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        staticArgument = (Map<String, Object>) Executions.getCurrent().getArg();
        loadDataAfterCompose();
        layoutCCV.setVisible(false);
        //Cuongvv
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        Files file = filesDAOHE.findById(docId);
        loadForm();
        Long statusTPPC = 31000002L;
        Long statusTPPC2 = 31000038L;
        Long statusTPPC3 = 31000053L;
        Long depId = 3411L;
        if ((file.getStatus().equals(statusTPPC) || file.getStatus().equals(statusTPPC2) || file.getStatus().equals(statusTPPC3)) && getDeptId().equals(depId)) {
            layoutCCV.setVisible(true);
            lbNodeDeptUser.setVisible(false);
            grbChoose.setVisible(false);
            lbNDU.setVisible(false);
            cbListNDU.setVisible(false);
            grbListNDU.setVisible(false);
            lhDelete.setVisible(false);
            wdListObjectsToSend.setVisible(false);
            btnChoose.setVisible(false);
            loadCBB();
        }
    }

//Cuongvv
    private void loadCBB() {
        UserDAOHE uDAO = new UserDAOHE();
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        List<Users> lsu = uDAO.getUserByDeptPosID(tk.getDeptId(), 23L);
        lbcvxlc.setModel(new ListModelList(lsu));
    }

    private void loadForm() {
        String formName;
        // find formName depending on action clicked
        // formName is base on business template page:
        // "/Pages/core/workflow/businessPage.zul";
        NodeToNodeDAOHE ntnDAOHE = new NodeToNodeDAOHE();
        NodeToNode action = ntnDAOHE.getActionById(actionId);
        Long formId = action.getFormId();
        CategoryDAOHE catDAOHE = new CategoryDAOHE();
        Category actionForm = catDAOHE.findById(formId);
        formName = actionForm.getValue();
        // linhdx
        String[] lstFormName;
        lstFormName = formName.split("\\?");
        if (lstFormName.length > 1) {
            arguments = (Map<String, Object>) Executions.getCurrent().getArg();
            for (int i = 1; i < lstFormName.length; i++) {
                String obj = lstFormName[i];
                String[] params = obj.split("=");
                try {
                    staticArgument.put(params[0], params[1]);
                    arguments.put(params[0], params[1]);
                } catch (Exception ex) {
                    LogUtils.addLog(ex);
                }

            }
        }
        formName = lstFormName[0];
        addBusinessPage(formName);

        // check if is final node
        // linhdx 20150514 kiem tra xem co bien defaultReceiveUserTypeOnEval
        // nghiep vu dycnk
        // Bien nay de tim nguoi dung mac dinh trong luong de gui
        // Dung trong truong hop nghiep vu datycnk
        // Chuyen vien chon CQKT dat trong bang EvaluationRecord
        // Lanh dao se lay thong tin CQKT do de fill vào danh sach
        try {
            if (arguments != null) {

                String evalType = (String) arguments.get(Constants.DEFAULT_RECEIVE_USER_TYPE_ON_EVAL.DEFAULT_RECEIVE_USER_TYPE_ON_EVAL_STR);
                if (evalType != null) {
                    Long evalTypeL = Long.valueOf(evalType);
                    if (Constants.DEFAULT_RECEIVE_USER_TYPE_ON_EVAL.IO_CQKT_DEFAULT_RECEIVE_USER_TYPE_ON_EVAL.equals(evalTypeL)) {
                        // Co nguoi nhan mac dinh
                        EvaluationRecordDAO evalDAO = new EvaluationRecordDAO();
                        EvaluationRecord eval = evalDAO.getLastEvaluationByEvalType(Constants.DEFAULT_RECEIVE_USER_TYPE_ON_EVAL.IO_CQKT_DEFAULT_RECEIVE_USER_TYPE_ON_EVAL);
                        if (eval != null) {
                            String formContent = eval.getFormContent();
                            Gson gson = new Gson();
                            EvaluationModel model = gson.fromJson(formContent,
                                    EvaluationModel.class);
                            Long checkDeptId = model.getCheckDeptId();
                            UserDAOHE uDHE = new UserDAOHE();
                            List<Long> lstUserId = uDHE.getAllUserFromRootId(
                                    checkDeptId,
                                    Constants.DEFAULT_RECEIVE_USER_TYPE_ON_EVAL.IO_CQKT_DEFAULT_POSITION_RECEIVE_USER_CODE);
                            // listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByUserId(lstUserId,
                            // nextId));

                            lbNodeDeptUser.setModel(new ListModelList(
                                    WorkflowAPI.getInstance().findNDUsByUserId(
                                            lstUserId, nextId)));
                            // argumentsListLDU.put("listNDU",
                            // listAvailableNDU);
                            listAvailableNDU = new ArrayList<>();
                            listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, curentDept));
                            //Hieuld them
                            Gson gs = new Gson();
                            lbNDU.setValue(gs.toJson(listAvailableNDU));
                            return;
                        }
                    } else if (Constants.DEFAULT_RECEIVE_USER_TYPE_ON_EVAL.RT_VICE_LEADER_DEFAULT_RECEIVE_USER_TYPE_ON_EVAL.equals(evalTypeL)) {
                        // Co nguoi nhan mac dinh
                        EvaluationRecordDAO evalDAO = new EvaluationRecordDAO();
                        EvaluationRecord eval = evalDAO.getLastEvaluationByEvalType(Constants.DEFAULT_RECEIVE_USER_TYPE_ON_EVAL.IO_CQKT_DEFAULT_RECEIVE_USER_TYPE_ON_EVAL);
                        if (eval != null) {
                            String formContent = eval.getFormContent();
                            Gson gson = new Gson();
                            EvaluationModel model = gson.fromJson(formContent,
                                    EvaluationModel.class);
                            Long viceDeptId = model.getViceDeptId();
                            List<Long> lstUserId = new ArrayList();
                            lstUserId.add(viceDeptId);
                            // listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByUserId(lstUserId,
                            // nextId));

                            lbNodeDeptUser.setModel(new ListModelList(
                                    WorkflowAPI.getInstance().findNDUsByUserId(
                                            lstUserId, nextId)));
                            // argumentsListLDU.put("listNDU",
                            // listAvailableNDU);
                            listAvailableNDU = new ArrayList<>();
                            listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, curentDept));
                            //Hieuld them
                            Gson gs = new Gson();
                            lbNDU.setValue(gs.toJson(listAvailableNDU));
                            return;
                        }
                    }
                }

            }

        } catch (Exception ex) {
            LogUtils.addLog(ex);
        }

        Textbox txtNextUser = null;
        try {
            Window businessWindow = (Window) includeWindow.getFellow("businessWindow");
            businessWindow.setTitle(null);
            txtNextUser = (Textbox) businessWindow.getFellow("txtNextUser");
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
        }
        if (txtNextUser != null && StringUtils.validString(txtNextUser.getValue())) {
            listAvailableNDU = new ArrayList<>();
            listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(
                    nextId, Long.parseLong(txtNextUser.getValue())));
            if (listAvailableNDU.size() > 0) {
                addToAlwaysSendList(true);
                return;
            }
        }
        Textbox txtCurrentDept = null;
        try {
            Window businessWindow = (Window) includeWindow.getFellow("businessWindow");
            txtCurrentDept = (Textbox) businessWindow.getFellow("txtCurrentDept");
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
        }
        if (txtCurrentDept != null && "1".equals(txtCurrentDept.getValue())) {
            curentDept = true;
        }
        boolean isFinalNode = this.checkNextNodeIsFinal();
        boolean isReturnPrevious = this.checkIsReturnPreviousUser();
        boolean isReturnAuthor = this.checkIsReturnAuthor();
        if (isFinalNode || isReturnAuthor) {
            lbNodeDeptUser.setModel(new ListModelList(listAvailableNDU));
        } else if (checkIsAlwaysSend()) {
            listAvailableNDU = new ArrayList<>();
            listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(
                    nextId, curentDept));
            addToAlwaysSendList(false);
        } else if (isReturnPrevious) {
            listAvailableNDU = new ArrayList<>();
            listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(
                    nextId, curentDept));
            addToSendPreviousList();
        } else {
            // linhdx
            // Xoa danh sach nguoi nhan mac dinh

            listAvailableNDU = new ArrayList<>();
            listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(
                    nextId, curentDept));
            argumentsListLDU.put("listNDU", listAvailableNDU);

            lbNodeDeptUser.setModel(new ListModelList(new ArrayList()));

            removeAlwaysSendList();
        }

        if (Constants.NODE_ASSOCIATE_TYPE.RETURN_AUTHOR.equals(actionType)) {
            disableBtnChoose();
        }
        //Hieuld them
        Gson gs = new Gson();

        lbNDU.setValue(gs.toJson(listAvailableNDU));
    }

    private void addBusinessPage(String formName) {
        // incBusinessPage.setAttribute("fileId", docId);
        // incBusinessPage.setSrc(formName);
        includeWindow.getChildren().clear();
        includeWindow.appendChild(this.createWindow("includeWindow", formName,
                staticArgument, Window.EMBEDDED));
    }

    public void loadDataAfterCompose() throws Exception {
        Map<String, Object> arguments = (Map<String, Object>) Executions.getCurrent().getArg();
        if (arguments.get("typeProcess") != null) {
            typeProcess = (int) arguments.get("typeProcess");
        }
        // windowParent = (Window) arguments.get("windowParent");
        windowParent = (Window) arguments.get("parentWindow");
        actionId = (Long) arguments.get("actionId");
        actionName = (String) arguments.get("actionName");
        actionType = (Long) arguments.get("actionType");
        // listAvailableNDU = (List<NodeDeptUser>)
        // arguments.get("lstAvailableNDU");
        FilesDAOHE fDAO = new FilesDAOHE();
        docId = (Long) arguments.get("docId");
        files = fDAO.findById(docId);
        nextId = (Long) arguments.get("nextId");
        process = (Process) arguments.get("process");
        docType = (Long) arguments.get("docType");
        argumentsListLDU = (Map<String, Object>) Executions.getCurrent().getArg();
        lstNextAction = (List<NodeToNode>) arguments.get("lstNextAction");
        if (lstNextAction != null && lstNextAction.size() > 1) {
            NodeToNode a = new NodeToNode();
            a.setId(Constants.COMBOBOX_HEADER_VALUE);
            a.setAction(Constants.COMBOBOX_HEADER_PROCESS_SELECT);
            lstNextAction.add(0, a);
        }

        ListModelArray lstModelNextAction = new ListModelArray(lstNextAction);
        lbAction.clearSelection();
        lbAction.setModel(lstModelNextAction);
        lbAction.renderAll();
        // if (actionId != null) {
        // for (int i = 0; i < lbAction.getListModel().getSize(); i++) {
        // NodeToNode ct = (NodeToNode) lbAction.getListModel().getElementAt(i);
        // if (actionId.equals(ct.getId())) {
        // lbAction.setSelectedIndex(i);
        // break;
        // }
        // }
        // }

        lbAction.setSelectedIndex(0);
        //linhdx 20170216
        //Neu la nghiep vu TTB va trang thai la thu ky cap nhat BBH cap phep
        Long statusThukyCapnhatBienbanhopcapphep = 31000021L;
        if(statusThukyCapnhatBienbanhopcapphep.equals(files.getStatus())){
        	if (lstNextAction != null && lstNextAction.size() > 1) {
        		lbAction.setSelectedIndex(1);
        	}
        }
        lbAction.renderAll();

        // if (typeProcess ==
        // Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_MOT_HO_SO) {
        // process = (Process) arguments.get("process");
        // docId = (Long) arguments.get("docId");
        // docType = (Long) arguments.get("docType");
        //
        // } else
        if (typeProcess == Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO) {
            listProcessCurrent = (List<Process>) arguments.get("listProcess");
            listDocId = (List<Long>) arguments.get("listDocId");
            listDocType = (List<Long>) arguments.get("listDocType");
        }
        btnOK.setLabel(actionName);
        listUserToSend = new ArrayList<>();
        listDeptToSend = new ArrayList<>();

    }

    private boolean checkIsReturnPreviousUser() {
        if (Constants.NODE_ASSOCIATE_TYPE.RETURN_PREVIOUS.equals(actionType)) {
            // NodeDeptUser ndu = new NodeDeptUser();
            // Process sourceProcess = process;
            // ndu.setUserId(sourceProcess.getSendUserId());
            // ndu.setDeptId(sourceProcess.getSendGroupId());
            // ndu.setUserName(sourceProcess.getSendUser());
            // ndu.setDeptName(sourceProcess.getSendGroup());
            // ndu.setProcessType(Constants.PROCESS_TYPE.MAIN);
            // listAvailableNDU.clear();
            // listAvailableNDU.add(ndu);
            return true;
        }
        return false;
    }

    private boolean checkIsAlwaysSend() {
        if (Constants.NODE_ASSOCIATE_TYPE.ALWAYS_SEND.equals(actionType)) {
            return true;
        }
        return false;
    }

    public void onDeleteListitem(int index) {

        listAvailableNDU.remove(index);
        lbNodeDeptUser.setModel(new ListModelList(listAvailableNDU));
        argumentsListLDU.put("listNDU", listAvailableNDU);
    }

    private void addToAlwaysSendList(Boolean pase) {

        List<NodeDeptUser> listAlwaysSend = new ArrayList();

        // ProcessDAOHE processDAOHE = new ProcessDAOHE();
        // List listSentProcess = null;
        // if (process != null && process.getProcessId() != null) {
        // listSentProcess = processDAOHE.getSentProcess(process);
        // }
        //Cuongvv
        List<NodeDeptUser> listAlwaysSend1 = new ArrayList();
        List<Process> lstProcess = WorkflowAPI.getInstance().getAllProcessContainFinish(docId, docType);
        Long statusCVCGHS = 31000004L;
        Long statusCVTDD = 31000005L;
        Long statusTKHDTL = 31000018L;
        Long statusTKHDTLLD = 31000008L;
        boolean chooseTLHS = false;

        if (listAvailableNDU != null && !listAvailableNDU.isEmpty()) {
            for (NodeDeptUser ndu : listAvailableNDU) {
                // Long configuredUser = ndu.getUserId();
                if (pase || Objects.equal(WorkflowAPI.OPTION_ALWAYS_SEND,
                        ndu.getOptionSelected()) || Objects.equal(WorkflowAPI.OPTION_ALWAYS_CHOOSE,
                                ndu.getOptionSelected())) {
                    listAlwaysSend.add(ndu);

                }
                if (Objects.equal(null, ndu.getOptionSelected()) && (files.getStatus().equals(statusTKHDTL))) {
                    listAlwaysSend.add(ndu);
                }
                //Cuongvv neu gui lai ho so thi chi gui 1 nguoi
                if (Objects.equal(WorkflowAPI.OPTION_ALWAYS_CHOOSE,
                        ndu.getOptionSelected()) && (files.getStatus().equals(statusCVTDD) || files.getStatus().equals(statusTKHDTL))) {
                    Long userId = ndu.getUserId();
                    if (isContainReceipt(lstProcess, userId, statusCVCGHS) || isContainReceipt(lstProcess, userId, statusTKHDTLLD)) {
                        listAlwaysSend1.add(ndu);
                        chooseTLHS = true;
                    }
                }
            }
        }
        listAvailableNDU.clear();
        if (!chooseTLHS) {
            listAvailableNDU.addAll(listAlwaysSend);
        } else {
            listAvailableNDU.add(listAlwaysSend1.get(0));

        }

//        listAvailableNDU.addAll(listAlwaysSend);
        lbNodeDeptUser.setModel(new ListModelList(listAvailableNDU));
        argumentsListLDU.put("listNDU", listAvailableNDU);
        disableBtnChoose();
    }

    /**
     * linhdx Thm vao danh sach gui tra lai cho nguoi truoc day da tham gia
     * luong 1. Khi 1 nguoi da xu ly ho so, qua 1 so buoc Khi gui lai thi mong
     * muon chinh nguoi do nhan ho so
     */
    private void addToSendPreviousList() {
        List<Process> lstProcess = WorkflowAPI.getInstance().getAllProcessContainFinish(docId, docType);
        List<NodeDeptUser> lstSendPrevious = new ArrayList();
        List<NodeDeptUser> lstSendPrevious1 = new ArrayList();
        List<NodeDeptUser> lstSendPreviousCVC = new ArrayList();
        List<NodeDeptUser> lstSendPreviousCVCGCGHC = new ArrayList();
//        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
//                "userToken");
        //Cuongvv neu trang thai DN gui lai hs thi chi gui cho ng yeu cau
        Long statusDNGLHS = 31000007L;
        Long statusCVTDD = 31000005L;
        Long statusCVCGCGHC = 31000040L;//Chuyen vien chinh gui chuyen gia hoa chat
        boolean chooseTLHS = false;
        boolean chooseCVCGCGHC = false;
        if (listAvailableNDU != null && !listAvailableNDU.isEmpty()) {
            for (NodeDeptUser ndu : listAvailableNDU) {
                Long userId = ndu.getUserId();
                if (isContainSend(lstProcess, userId, statusDNGLHS)) {
                    lstSendPrevious1.add(ndu);
                }
                if (isContainSend(lstProcess, userId, statusCVTDD)) {
                    lstSendPreviousCVC.add(ndu);
                    chooseTLHS = true;
                }
                if (isContainSend(lstProcess, userId, statusCVCGCGHC)) {
                    lstSendPreviousCVCGCGHC.add(ndu);
                    chooseCVCGCGHC = true;
                }
                if (isContain(lstProcess, userId)) {
                    lstSendPrevious.add(ndu);
                }
            }
            listAvailableNDU.clear();
        }

        if (listAvailableNDU != null) {
            if (files.getStatus()
                    .equals(statusDNGLHS)) {
                listAvailableNDU.addAll(lstSendPrevious1);
            } else {
                if (chooseTLHS) {
                    listAvailableNDU.add(lstSendPreviousCVC.get(0));
                } else if (chooseCVCGCGHC) {
                    listAvailableNDU.add(lstSendPreviousCVCGCGHC.get(0));
                } else {
                    if (lstSendPrevious.size() > 0) {
                        listAvailableNDU.add(lstSendPrevious.get(0));
                    }

                }
            }
        }

        lbNodeDeptUser.setModel(new ListModelList(listAvailableNDU));
        argumentsListLDU.put("listNDU", listAvailableNDU);
        disableBtnChoose();
    }

    private boolean isContain(List<Process> lstProcess, Long userId) {
        for (Process process : lstProcess) {
            if (process.getReceiveUserId() != null
                    && process.getReceiveUserId().equals(userId) //linhdx comment truong hop phe duyet don dang ky thi finishdate = null
                    //&& process.getFinishDate() != null
                    ) {
                return true;
            }
        }
        return false;
    }

    //Cuongvv
    private boolean isContainReceipt(List<Process> lstProcess, Long userId, Long status) {
        long processOrder = 0;
        for (Process process : lstProcess) {
            if ((long) process.getOrderProcess() > processOrder) {
                processOrder = (long) process.getOrderProcess();
            }
        }
        for (Process process : lstProcess) {
            if (process.getReceiveUserId() != null
                    && process.getReceiveUserId().equals(userId) && process.getFinishDate() != null && process.getStatus().equals(status) && process.getOrderProcess() == (processOrder - 1)) {
                return true;
            }
        }
        return false;
    }

    // kiem tra nguoi xu ly cuoi cung
    private boolean isContainSend(List<Process> lstProcess, Long userId, Long status) {
        long processOrder = 0;
        for (Process process : lstProcess) {
            if ((long) process.getOrderProcess() > processOrder) {
                processOrder = (long) process.getOrderProcess();
            }
        }
        for (Process process : lstProcess) {
            if (process.getSendUserId() != null
                    && process.getSendUserId().equals(userId) && process.getStatus().equals(status) && process.getOrderProcess() == processOrder) {
                return true;
            }
        }
        return false;
    }

    private void removeAlwaysSendList() {
        enableBtnChoose();
    }

    private void disableBtnChoose() {
        btnChoose.setDisabled(true);
        btnChoose.setVisible(false);
    }

    private void enableBtnChoose() {
        btnChoose.setDisabled(false);
        btnChoose.setVisible(true);
    }

    private boolean checkIsReturnAuthor() {
        if (Constants.NODE_ASSOCIATE_TYPE.RETURN_AUTHOR.equals(actionType)) {
            CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
            NodeDeptUser ndu = new NodeDeptUser();
            Process sourceProcess = coreDAOHE.findSourceProcess(docId, docType);
            ndu.setUserId(sourceProcess.getSendUserId());
            ndu.setDeptId(sourceProcess.getSendGroupId());
            ndu.setUserName(sourceProcess.getSendUser());
            ndu.setDeptName(sourceProcess.getSendGroup());
            ndu.setProcessType(Constants.PROCESS_TYPE.MAIN);
            listAvailableNDU.clear();
            listAvailableNDU.add(ndu);
            return true;
        }
        return false;
    }

    private boolean checkNextNodeIsFinal() {
        Node nextNode;
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        NodeToNodeDAOHE actionDAOHE = new NodeToNodeDAOHE();
        NodeToNode action = actionDAOHE.getActionById(actionId);
        Long nextNodeId = (action == null) ? 0 : action.getNextId();
        NodeDAOHE nodeDAOHE = new NodeDAOHE();
        nextNode = nodeDAOHE.findById(nextNodeId);
        NodeDeptUser ndu = new NodeDeptUser();
        if (nextNode != null
                && nextNode.getType().equals(
                        Constants.NODE_TYPE.NODE_TYPE_FINISH)) {
            Process sourceProcess = coreDAOHE.findSourceProcess(docId, docType);
            ndu.setUserId(sourceProcess.getSendUserId());
            ndu.setDeptId(sourceProcess.getSendGroupId());
            ndu.setUserName(sourceProcess.getSendUser());
            ndu.setDeptName(sourceProcess.getSendGroup());
            ndu.setProcessType(Constants.PROCESS_TYPE.MAIN);
            listAvailableNDU.clear();
            listAvailableNDU.add(ndu);
            return true;
        } else {
            return false;
        }
    }

    @Listen("onClick = #btnChoose")
    public void openTree() {

        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("actionName", actionName);
        arguments.put("listAvailableNDU", listAvailableNDU);
        arguments.put("target", lbNodeDeptUser);
        arguments.put("processCurrent", process);
        arguments.put("listChoosedNDU", getListChoosedNDU());

        ProcessDAOHE processDAOHE = new ProcessDAOHE();
        List listSentProcess = null;
        if (process != null && process.getProcessId() != null) {
            listSentProcess = processDAOHE.getSentProcess(process);
        }
        arguments.put("listSentProcess", listSentProcess);
        String formName = TREE_USER_SELECTOR_PAGE;
        createWindow("wdTree", formName, arguments, Window.MODAL);
    }

    public void processSingle() throws UnsupportedEncodingException {
        List<NodeDeptUser> lstChoosenUser = getListChoosedNDU();
        NodeToNodeDAOHE ntnDAOHE = new NodeToNodeDAOHE();
        NodeToNode action = ntnDAOHE.getActionById(actionId);
        Long processParentId = null;
        if (process != null) {
            processParentId = process.getProcessId();
        }
        String note = txtNote.getText();
        Boolean re = false;
        // Thêm re tránh gửi nhiều lần sang NSW
        // Thêm check fix luồng sau khi phê duyệt đơn đăng ký
        Long check = 3L;

        //linhdx 20160320
        Boolean checkIsXNDYCNK = false;
        WorkflowAPI w = new WorkflowAPI();
        if (docType.equals(w.getProcedureTypeByCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT).getCategoryId())) {
            checkIsXNDYCNK = true;
            if ("Phê duyệt đơn đăng ký".equals(action.getAction())) {
                check = checkActionPDDDK(docId);
            }
        }
        Boolean fn = false;
        try {
            Textbox txtFinishDate;
            Window businessWindow = (Window) includeWindow.getFellow("businessWindow");
            businessWindow.setTitle(null);
            txtFinishDate = (Textbox) businessWindow.getFellow("txtFinishDate");
            if (txtFinishDate != null && "1".equals(txtFinishDate.getValue())) {
                fn = true;
                FilesDAOHE fDAO = new FilesDAOHE();
                Files f = fDAO.findById(docId);
                f.setFinishDate(new Date());
                fDAO.saveOrUpdate(f);

            }
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
        }
        Boolean l = WorkflowAPI.getInstance().
                sendDocToListNDUs(docId, docType, action, note, processParentId, lstChoosenUser, message, re, check != 1L, fn);
        if (!l) {
            showNotification("Lỗi kết nối hệ thống NSW");
            return;
        }
        if (check == 2L || (check == 3L && !fn)) {
            re = true;
        }
        Boolean m;

        if (checkIsXNDYCNK) {
            m = WorkflowAPI.getInstance().
                    sendTransparentXNDYCNK(docId, docType, action, note, processParentId, lstChoosenUser, message, re, check != 2L, fn);
        } else {
            m = WorkflowAPI.getInstance().
                    sendTransparent(docId, docType, action, note, processParentId, lstChoosenUser, message, re, check != 2L, fn);

        }

        if (!m) {
            showNotification("Lỗi kết nối hệ thống NSW");
            return;
        }
        windowProcessing.onClose();
        showNotification(String.format(Constants.Notification.SENDED_SUCCESS, "hồ sơ"), Constants.Notification.INFO);
        // windowParent.invalidate();
    }

    private Long checkActionPDDDK(Long fileId) {
        ImportOrderProductDAO impDAO = new ImportOrderProductDAO();
        List<ImportOrderProduct> lsImP = impDAO.findAllIdByFileId(fileId);
        if (lsImP != null && lsImP.size() != 0) {
            for (ImportOrderProduct p : lsImP) {
                if ("CHAT".equals(p.getCheckMethodCode()) || "THUONG".equals(p.getCheckMethodCode())) {
                    return 2L;
                }
            }
        }
        return 1L;
    }

    public void processMulti() throws UnsupportedEncodingException {
        List<NodeDeptUser> lstChoosenUser = getListChoosedNDU();
        NodeToNode action = (new NodeToNodeDAOHE()).getActionById(actionId);
        Long processParentId = null;
        int sizeProcess = listProcessCurrent.size();
        Boolean fn = false;
        try {
            Textbox txtFinishDate;

            Window businessWindow = (Window) includeWindow.getFellow("businessWindow");

            txtFinishDate = (Textbox) businessWindow.getFellow("txtFinishDate");
            if (txtFinishDate != null && "1".equals(txtFinishDate.getValue())) {
                fn = true;
                FilesDAOHE fDAO = new FilesDAOHE();
                Files f = fDAO.findById(docId);
                f.setFinishDate(new Date());
                fDAO.saveOrUpdate(f);
            }
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
        }
        Process tempProcess;
        for (int i = 0; i < sizeProcess; i++) {
            tempProcess = listProcessCurrent.get(i);
            if (tempProcess != null) {
                processParentId = tempProcess.getProcessId();
            }
            String note = txtNote.getText();
            Boolean re = false;
            Boolean k = WorkflowAPI.getInstance().sendDocToListNDUs(listDocId.get(i),
                    listDocType.get(i), action, note, processParentId,
                    lstChoosenUser, message, re, !fn, fn);
            if (!k) {
                showNotification("Lỗi kết nối hệ thống NSW");
                return;
            }
        }
        windowProcessing.onClose();
    }

    public boolean validate() {
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        Files file = filesDAOHE.findById(docId);
        if (!file.getStatus().equals(files.getStatus())) {
            showNotification("Hồ sơ đã được cán bộ khác xử lý !!!");
            return false;
        }
        // linhdx
        // vi luong xu lý song song
        // commentvalidate
        // if(validateUserRight(action, process, file)){
        // showNotification("Bạn không có quyền xử lý hồ sơ!");
        // return false;

        if (this.getListChoosedNDU().isEmpty()) {
            showNotification("Bạn chưa chọn người xử lý tiếp theo!");
            return false;
        }

        return true;
    }

    // viethd3 check:
    // if return true then cannot allow send process!!!
    private boolean validateUserRight(NodeToNode action,
            Process processCurrent, Files file) {

        // check if status of Process is different to status of Files
        if (processCurrent != null && file != null) {
            if (!processCurrent.getStatus().equals(file.getStatus())) {
                return true;
            }
        }

        // check if receiverId is diferent to current user id
        // find all process having current status of document
        if (file == null) {
            return false;
        }
        List<Process> listCurrentProcess = WorkflowAPI.getInstance().findAllCurrentProcess(file.getFileId(), file.getFileType(),
                file.getStatus());
        Long userId = getUserId();
        Long creatorId = file.getCreatorId();
        // truong hop User dang nhap khong phai la Creator cua ho so
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return true;
            }
        } else {
            if (processCurrent == null) {
                // Kiem tra xem co truong hop nao ma User dang nhap nam trong
                // danh sach cac Receiver
                boolean needToProcess = false;
                for (Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                // truong hop User dang nhap khong nam trong danh sach cac
                // Receiver ung voi trang thai hien tai cua ho so
                // nghia la User do khong duoc xu ly ho so -> return true
                if (!needToProcess) {
                    return true;
                }
            }
        }
        // cac truong hop duoc xu ly ho so (khong cam xu ly ho so), return false
        return false;
    }

    @Listen("onClose=#windowProcessing")
    public void onClickClose() {
        refresh();
    }

    @Listen("onClick=#btnOK")
    public void onClickOK() {
        if ((Long) lbAction.getSelectedItem().getValue() == -1L) {
            showNotification("Phải chọn thao tác xử lý");
            return;
        }
        if (!validate()) {
            return;
        }

        Window businessWindow = (Window) includeWindow.getFellow("businessWindow");

        Button submit = (Button) businessWindow.getFellow("btnSubmit");

        Textbox txtValidate = null;
        try {
            txtValidate = (Textbox) businessWindow.getFellow("txtValidate");
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
        }
        Events.sendEvent(new Event("onClick", submit, null));

        message = null;
        Textbox txtMessage = null;
        try {
            txtMessage = (Textbox) businessWindow.getFellow("txtMessage");
            if ((txtMessage != null) && (txtMessage.getText() != null)) {
                Gson gson = new Gson();
                message = gson.fromJson(txtMessage.getText(),
                        MessageModel.class);
            }

        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
        }
        if ((txtValidate != null) && (txtValidate.getText() != null)) {
            String sValidate = txtValidate.getText().trim();
            if (Long.parseLong(sValidate) == Constants.IS_VALIDATE_OK) {
                if (typeProcess == Constants.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO) {
                    String message = Constants.Notification.CONFIRM_GENERAL;
                    Messagebox.show(message, "Xác nhận", Messagebox.YES | Messagebox.NO,
                            Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                                @Override
                                public void onEvent(Event event) throws UnsupportedEncodingException {
                                    if (null != event.getName()) {
                                        switch (event.getName()) {
                                            case Messagebox.ON_YES:
                                                processMulti();
                                                showNotification(String.format(Constants.Notification.SENDED_SUCCESS, "hồ sơ"), Constants.Notification.INFO);
                                                refresh();
                                                break;
                                            case Messagebox.ON_NO:
                                                break;
                                        }
                                    }
                                }
                            });

                } else {

                    try {
                        processSingle();
                        refresh();
                        closeView();
                    } catch (UnsupportedEncodingException ex) {
                        LogUtils.addLog(ex);
                    }

//                    String message = Constants.Notification.CONFIRM_GENERAL;
//                    Messagebox.show(message, "Xác nhận", Messagebox.YES | Messagebox.NO,
//                            Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
//
//                                @Override
//                                public void onEvent(Event event) throws UnsupportedEncodingException {
//                                    if (null != event.getName()) {
//                                        switch (event.getName()) {
//                                            case Messagebox.ON_YES:
//                                                
//                                                //showNotification(String.format(Constants.Notification.SENDED_SUCCESS,"hồ sơ"), Constants.Notification.INFO);
//                                                break;
//                                            case Messagebox.ON_NO:
//                                                break;
//                                        }
//                                    }
//                                }
//                            });
                }

            } else {
                // TODO: tai sao return ???
                return;
            }
        } else {
            String message = Constants.Notification.CONFIRM_GENERAL;
            Messagebox.show(message, "Xác nhận", Messagebox.YES | Messagebox.NO,
                    Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) throws UnsupportedEncodingException {
                            if (null != event.getName()) {
                                switch (event.getName()) {
                                    case Messagebox.ON_YES:
                                        processSingle();
                                        refresh();
                                        //showNotification(String.format(Constants.Notification.SENDED_SUCCESS,"hồ sơ"), Constants.Notification.INFO);
                                        break;
                                    case Messagebox.ON_NO:
                                        break;
                                }
                            }
                        }
                    });
        }

    }

    private void refresh() {
        if (windowParent != null) {
            Events.sendEvent("onRefresh", windowParent, null);

        }

        LogUtils.addLog(ProcessingWindow.class
                .getName() + ":"
                + " send document:" + docId + " succesfully!");
    }

    private void closeView() {
        //linhdx 20160301 dong cua so view
        try {
            Window windowview = (Window) Path.getComponent("/windowView");
            if (windowview != null) {
                Events.sendEvent("onClose", windowview, null);
            }
        } catch (Exception ex) {
            LogUtils.addLog(ex);
        }
    }

    public List<NodeDeptUser> getListChoosedNDU() {
        /**
         * Cuongvv
         */
        List list = new ArrayList<>();
        Long statusTPPC = 31000002L;
        Long statusTPPC2 = 31000038L;
        Long statusTPPC3 = 31000053L;
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        Files file = filesDAOHE.findById(docId);
        if ((file.getStatus().equals(statusTPPC) || file.getStatus().equals(statusTPPC2) || file.getStatus().equals(statusTPPC3)) && getDeptId() == 3411) {
            UserDAOHE uDAO = new UserDAOHE();
            UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                    "userToken");
            List<Users> lsu = uDAO.getUserByDeptPosID(tk.getDeptId(), 23L);
            String username = "";
            if (lbcvxlc.getSelectedItem() != null) {
                for (int i = 0; i < lsu.size(); i++) {
                    if (lsu.get(i).getUserId() == lbcvxlc.getSelectedItem().getValue()) {
                        username = lsu.get(i).getUserName();
                    }
                }
                NodeDeptUser deptUser = new NodeDeptUser(Long.parseLong(lbcvxlc.getSelectedItem().getValue().toString()), username);

                Users us = uDAO.getUserById(deptUser.getUserId());
                deptUser.setUserName(us.getFullName());
                list.add(deptUser);

            } else {
                showNotification("Bạn chưa chọn người xử lý tiếp theo!");
            }
        }

        if (lbNodeDeptUser != null) {
            for (Listitem item : lbNodeDeptUser.getItems()) {
                if (!item.isDisabled()) {
                    list.add((NodeDeptUser) item.getValue());
                }
            }
        }
        return list;
    }

    @Listen("onSelect = #lbAction")
    public void selectAction() {
        Listitem item = lbAction.getSelectedItem();
        if ((Long) lbAction.getSelectedItem().getValue() == -1L) {
            showNotification("Phải chọn thao tác xử lý");
            btnOK.setLabel("Xử lý hồ sơ");
            lbAction.setFocus(true);
            return;
        }
        actionId = (Long) item.getValue();
        NodeToNode action = (new NodeToNodeDAOHE()).findById(actionId);
        actionName = (String) action.getAction();
        actionType = (Long) action.getType();
        nextId = (Long) action.getNextId();
        btnOK.setLabel(actionName);
        listUserToSend = new ArrayList<>();
        listDeptToSend = new ArrayList<>();

        loadForm();

    }
}
