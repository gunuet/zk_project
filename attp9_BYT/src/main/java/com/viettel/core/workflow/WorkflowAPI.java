/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.core.workflow;

import com.viettel.utils.Constants;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.DAO.CoreProcessDAOHE;
import com.viettel.core.workflow.BO.Node;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.DAO.NodeDAOHE;
import com.viettel.core.workflow.DAO.NodeToNodeDAOHE;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.DAO.*;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.cosmetic.BO.MessageEmail;
import com.viettel.module.cosmetic.BO.MessageSms;
import com.viettel.module.cosmetic.DAO.CosmeticDAO;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Files;

import com.viettel.voffice.DAO.FilesDAOHE;
import java.io.UnsupportedEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author duv
 */
public class WorkflowAPI extends BaseComposer {

    private static final long serialVersionUID = 1L;
    public static final long OPTION_ALWAYS_SEND = 2L;
    public static final long OPTION_ALWAYS_CHOOSE = 1L;
    public static final long OPTION_NONE = 0L;
    public static final String PROCESSING_GENERAL_PAGE = "/Pages/core/workflow/processingPage.zul";
    /**
     * Fixed string to select all records of file status in CATEGORY table
     * condition: CATEGORY_TYPE_CODE equals FILE_STATUS
     */
    public static final String CAT_FILE_STATUS = "FILE_STATUS";
    /**
     * Fixed string to select all records of form name in CATEGORY table
     * condition: CATEGORY_TYPE_CODE equals FORM attention: form is an uri *.zul
     */
    public static final String CAT_ACTION_FORM = "FORM";
    public static final String CAT_PROCEDURE = "OBJECT";
    public static final String IMPORT_PROCEDURE = "IMPORT_FILE_TYPE";
    public static final long IS_DEFAULT_SELECTER = 0L;
    private static volatile WorkflowAPI instance;

    public static WorkflowAPI getInstance() {
        if (instance == null) {
            instance = new WorkflowAPI();
        }
        return instance;
    }

    public WorkflowAPI() {
    }

    public Boolean checkCA(X509Certificate x509Cert) {
        Boolean rs = true;
        if (x509Cert == null) {
            return false;
        }
//        if (x509Cert.getNotAfter().before(new Date()) || x509Cert.getNotBefore().after(new Date())) {
//            return false;
//        }
        if (x509Cert.getPublicKey() == null) {
            return false;
        }
        if (x509Cert.getSerialNumber() == null) {
            return false;
        }
        return rs;
    }

    public Department getParentDeptByUsId(Long usId) {
        Department rs = null;
        UserDAOHE usDAO = new UserDAOHE();
        Users u = usDAO.findById(usId);
        if (u != null && u.getDeptId() != null) {
            DepartmentDAOHE dpDAO = new DepartmentDAOHE();
            Department child = dpDAO.findById(u.getDeptId());
            if (child != null && child.getParentId() != null) {
                rs = dpDAO.findById(child.getParentId());
            }
        }
        return rs;
    }

    private Process saveProcess(
            Long nodeType, Long processParentId,
            Node previousNode, Node nextNode,
            Long docId, Long docType, String note,
            NodeDeptUser configuredUserNDU, Long status, MessageModel message, Boolean re, Boolean active, Boolean fn) throws UnsupportedEncodingException {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        List<NodeDeptUser> listUserConfiguredInNode
                = coreDAOHE.getNodeDeptUserOfNode(nextNode.getNodeId());
        UserToken user = (UserToken) Sessions.getCurrent().getAttribute("userToken");
        Long userId = user.getUserId();
        Long deptId = user.getDeptId();
        String username = user.getUserFullName();
        String deptname = user.getDeptName();
        //Long posId = user.getPosId();
        // Trang thai cua ho so: moi tao (NEW)
        // Trang thai cua ho so sau nay se lay theo truong STATUS trong bang NODE
        //linhdx comment
        //Long status = -1l;
        //status = Constants.DOCUMENT_STATUS.NEW;
        //if(previousNode!=null)
        //    status = previousNode.getStatus();

        Long order = 0l;
        List<Process> listProcessOfFlow = coreDAOHE.getProcessByObjType(docId, docType);
        if (listProcessOfFlow.size() > 0) {
            Process maxProcess = listProcessOfFlow.get(listProcessOfFlow.size() - 1);
            order = maxProcess.getOrderProcess() + 1;
        }

        if (note == null) {
            note = "--NOTE--";
        }

        // Kieu chuyen action: Khoi tao luong
        // Ten kieu chuyen action: Khoi tao luong
        Long actionType = null;
        String actionTypeName = null;

        Long previousId = null;
        Long nextId = null;
        if (previousNode != null) {
            previousId = previousNode.getNodeId();
        }
        if (nextNode != null) {
            nextId = nextNode.getNodeId();
        }

        if (Constants.NODE_ASSOCIATE_TYPE.START == nodeType) {
            actionType = Constants.NODE_ASSOCIATE_TYPE.START;
            actionTypeName = "Khởi tạo luồng";
            status = Constants.PROCESS_STATUS.INITIAL;
            listUserConfiguredInNode.clear();
            NodeDeptUser ndu = new NodeDeptUser();
            ndu.setUserId(getUserId());
            ndu.setUserName(getUserFullName());
            ndu.setDeptId(getDeptId());
            ndu.setDeptName(getDeptName());
            ndu.setProcessType(Constants.PROCESS_TYPE.MAIN);
            listUserConfiguredInNode.add(ndu);
        } else {
            NodeToNodeDAOHE node2node = new NodeToNodeDAOHE();
            List<NodeToNode> lstActionNodes
                    = node2node.getNodeToNodes(previousId, nextId);
            if (!lstActionNodes.isEmpty()) {
                NodeToNode actionNode = lstActionNodes.get(0);
                actionType = actionNode.getType();
                actionTypeName = actionNode.getAction();
                // viethd 28/01/2015
                // using status of NodeToNode object
                //linhdx comment
                //status = actionNode.getStatus();
            }
        }

        Date deadline = null;

        //Cuongvv neu la van thu bo thi tao finishdate
        Date finish = null;

        Long receiveUserID;
        String receiveUserStr = ResourceBundleUtil.getString("USER_ID_VTB", "config");
        if (receiveUserStr != null) {
            receiveUserID = Long.valueOf(receiveUserStr);
        } else {
            receiveUserID = -1L;
        }
        if (getUserId().equals(receiveUserID)) {
            finish = new Date();
        }

        Long receiverId;
        String receiverName = null;
        Long receiverGroupId = null;
        String receiverGroupName = null;
        UserDAOHE userDAOHE = new UserDAOHE();
        Users receiver;
        Long processType;
        //linhdx cap nhat truong finish date de thong bao hoan thanh xu ly process
//        if (nextNode != null
//                && Constants.NODE_TYPE.NODE_TYPE_FINISH.equals(nextNode.getType())) {
//            finish = new Date();
//        }
        //finish = new Date();
        receiverId = configuredUserNDU.getUserId();
        if (receiverId != null) {// case specific receiver configured
            receiverName = configuredUserNDU.getUserName();
            receiver = userDAOHE.getUserById(receiverId);
            receiverGroupId = receiver.getDeptId();
            receiverGroupName = receiver.getDeptName();
        } else {// case deparment configured

            if (configuredUserNDU.getPosId() != null) {// case user's position configured
                receiverGroupId = configuredUserNDU.getDeptId();
                receiverGroupName = configuredUserNDU.getDeptName();
            } else {// case only department configured
                // TODO: for each user who has the identical position
                //          insert into Process ?
            }
        }
        processType = configuredUserNDU.getProcessType();

        Process p = coreDAOHE.insertProcess(
                docId, docType,
                userId, username, deptId, deptname,
                receiverId, receiverName, receiverGroupId, receiverGroupName,
                nextId, processParentId,
                processType, status,
                deadline, finish,
                actionType, actionTypeName,
                order, note,
                previousId, message, re, active, fn);
        String hopdong = ResourceBundleUtil.getString("hopdong", "config");
        if ("true".equals(hopdong)) {
            sendEmailSMS(receiverId, docId, username, actionTypeName, userId);
        }

        return p;
    }
    //linhdx comment
//    // save process record
//    // and update status of Document
//    public void saveProcessData(Long nextNodeId, Long previousNodeId,
//            Long docId, Long docType, NodeDeptUser configuredUserNDU,
//            Long processParentId, String note) {
//        NodeDAOHE nodeDAOHE = new NodeDAOHE();
//        Node nextNode = nodeDAOHE.getNodeById(nextNodeId);
//        Node previousNode = nodeDAOHE.getNodeById(previousNodeId);
//        Long ACTION_TYPE = Constants.NODE_ASSOCIATE_TYPE.NORMAL;
//        if (previousNode == null) {
//            ACTION_TYPE = Constants.NODE_ASSOCIATE_TYPE.START;
//        }
//
//        // save process information of document
//        this.saveProcess(
//                ACTION_TYPE, processParentId,
//                previousNode, nextNode,
//                docId, docType, note, configuredUserNDU);
//
//        // update status of document YCNK_FILE
//        Long status;
//        if (previousNode == null) //case initiation of flow
//        {
//            status = Constants.PROCESS_STATUS.INITIAL;
//        } else {
//            status = previousNode.getStatus();
//        }
//        YcnkFile currentDoc = (new YcnkFileDAOHE()).findById(docId);
//        currentDoc.setStatusCode(status);
//    }

    // save process record
    // and update status of Document
    public Boolean sendDocToOneNDU(
            Long docId, Long docType,
            NodeToNode action, String note,
            Long processParentId, NodeDeptUser configuredUserNDU, Long status, MessageModel message, Boolean re, Boolean active, Boolean fn) throws UnsupportedEncodingException {
        NodeDAOHE nodeDAOHE = new NodeDAOHE();
        Node nextNode = nodeDAOHE.getNodeById(action.getNextId());
        Node previousNode = nodeDAOHE.getNodeById(action.getPreviousId());
        Long ACTION_TYPE = Constants.NODE_ASSOCIATE_TYPE.NORMAL;
        if (previousNode == null) {
            ACTION_TYPE = Constants.NODE_ASSOCIATE_TYPE.START;
        }

        // save process information of document
        Process k = this.saveProcess(
                ACTION_TYPE, processParentId,
                previousNode, nextNode,
                docId, docType, note, configuredUserNDU, status, message, re, active, fn);
        return k != null;
    }

    public Boolean sendDocToListNDUs(Long docId, Long docType,
            NodeToNode action, String note,
            Long processParentId, List<NodeDeptUser> lstChoosenUser, MessageModel message, Boolean re, Boolean active, Boolean fn) throws UnsupportedEncodingException {
        // send to selected users
        // process parent is processCurrent
        Long status = action.getStatus();
        Long previousNodeId = action.getPreviousId();
        //linhdx 20150513

        NodeToNodeDAOHE ntnDAOHE = new NodeToNodeDAOHE();
        List<Long> lstStatus = ntnDAOHE.getPrevioustAction(previousNodeId);
        this.updatePrcessStatus(docId, docType, lstStatus);
        for (NodeDeptUser ndu : lstChoosenUser) {
            Boolean k = sendDocToOneNDU(docId, docType, action, note, processParentId, ndu, status, message, re, active, fn);
            re = true;
            if (k == false) {
                return false;
            }
            //Cuongvv
            Long receiveUserID;
            String receiveUserStr = ResourceBundleUtil.getString("USER_ID_VTB", "config");
            if (receiveUserStr != null) {
                receiveUserID = Long.valueOf(receiveUserStr);
            } else {
                receiveUserID = -1L;
            }
            if (!getUserId().equals(receiveUserID)) {
                this.updateDocStatus(action, docId);
            }

            // TODO: should change to using LogUtils          
        }
        return true;
    }

    /**
     * linhdx luong gui tu dong Tim node tiep theo Tim cac luong gui tu dong de
     * gui
     *
     * @param docId
     * @param docType
     * @param action
     * @param note
     * @param processParentId
     * @param lstChoosenUser
     */
    public Boolean sendTransparent(Long docId, Long docType,
            NodeToNode action, String note,
            Long processParentId, List<NodeDeptUser> lstChoosenUser, MessageModel message, Boolean re, Boolean active, Boolean fn) throws UnsupportedEncodingException {

        //linhdx tim node tiep theo
        Long nextNodeId = action.getNextId();

        NodeToNodeDAOHE ntnDAOHE = new NodeToNodeDAOHE();
        List<NodeToNode> listTransparentActions = ntnDAOHE.getNextAction(
                nextNodeId, Constants.NODE_ASSOCIATE_TYPE.TRANSPARENT);

        if (listTransparentActions != null
                && !listTransparentActions.isEmpty()) {
            // Tao moi 1 process va chuyen xu li den node duoc forward
            for (NodeToNode ntnTemp : listTransparentActions) {
                //linhdx lay status cua node tiep theo
                Long status = ntnTemp.getStatus();
                NodeDeptUserDAOHE nduDAOHE = new NodeDeptUserDAOHE();
                List<NodeDeptUser> listNDU = nduDAOHE.getDetailedNodeDeptUser(ntnTemp.getNextId(),
                        lstChoosenUser.get(0).getDeptId());
                for (NodeDeptUser nduTemp : listNDU) {
                    Boolean l = this.sendDocToOneNDU(docId, docType, action, note, processParentId, nduTemp, status, message, re, active, fn);
                    re = true;
                    if (!l) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * linhdx 20160320 ham gui tu dong nghiep vu XNDYCNK. Chi gui cho nguoi cung
     * don vi
     *
     * @param docId
     * @param docType
     * @param action
     * @param note
     * @param processParentId
     * @param lstChoosenUser
     */
    public Boolean sendTransparentXNDYCNK(Long docId, Long docType,
            NodeToNode action, String note,
            Long processParentId, List<NodeDeptUser> lstChoosenUser, MessageModel message, Boolean re, Boolean active, Boolean fn) throws UnsupportedEncodingException {

        //linhdx tim node tiep theo
        Long nextNodeId = action.getNextId();

        NodeToNodeDAOHE ntnDAOHE = new NodeToNodeDAOHE();
        List<NodeToNode> listTransparentActions = ntnDAOHE.getNextAction(
                nextNodeId, Constants.NODE_ASSOCIATE_TYPE.TRANSPARENT);

        if (listTransparentActions != null
                && !listTransparentActions.isEmpty()) {
            // Tao moi 1 process va chuyen xu li den node duoc forward
            for (NodeToNode ntnTemp : listTransparentActions) {
                //linhdx lay status cua node tiep theo
                Long status = ntnTemp.getStatus();
                NodeDeptUserDAOHE nduDAOHE = new NodeDeptUserDAOHE();
                //linhdx 20160504 
                // Truong hop gui phe duyet don dang ky gui cho truong phong, ma truong phong khac don vi voi lanh dao Cuc
                List<NodeDeptUser> listNDU = nduDAOHE.getNodeDeptUserXNDYCNK(ntnTemp.getNextId(),
                        getDeptId());

                for (NodeDeptUser nduTemp : listNDU) {
                    Boolean l = this.sendDocToOneNDU(docId, docType, action, note, processParentId, nduTemp, status, message, re, active, fn);
                    re = true;
                    if (!l) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void updateDocStatus(NodeToNode action, Long docId) {
        // update status of document (in table FILES)
        Long status;
        if (action.getPreviousId() == null) //case: initiation of flow
        {
            status = Constants.PROCESS_STATUS.INITIAL;
        } else {
            status = action.getStatus();
        }
        Files currentDoc = (new FilesDAOHE()).findById(docId);
        currentDoc.setStatus(status);
        currentDoc.setModifyDate(new Date());
    }

    public void updatePrcessStatus(Long docId, Long docType, List<Long> lstStatus) {
        //List<Process> lstProcess  = getProcess(docId, docType, userId,status );

        List<Process> lstProcess = findPreviousProcess(docId, docType, lstStatus);
        ProcessDAOHE pHE = new ProcessDAOHE();
        for (Process p : lstProcess) {
            p.setFinishDate(new Date());
        }
        pHE.saveOrUpdate(lstProcess);
    }

    public static String getStatusName(Long status) {
        CategoryDAOHE catDAOHE = new CategoryDAOHE();
        Category cat = catDAOHE.findCategoryByValue(CAT_FILE_STATUS, status.toString());
        return cat == null ? "" : cat.getName();
    }

    public static String getDocumentTypeCode(Long documentTypeCode) {
        String documentTypeCodeName = "";
        if (documentTypeCode == -1L) {
            documentTypeCodeName = "Chưa chọn loại hồ sơ";
        } else if (documentTypeCode == Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI) {
            documentTypeCodeName = Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI_STR;
        } else if (documentTypeCode == Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG) {
            documentTypeCodeName = Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG_STR;
        } else if (documentTypeCode == Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN) {
            documentTypeCodeName = Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN_STR;
        }
        return documentTypeCodeName;
    }

    public Category getProcedureTypeByCode(String code) {
        Category c;
        // viethd:
        // find flow id due to FLOW_CODE from category tables
        CategoryDAOHE catDAOHE = new CategoryDAOHE();
        List<Category> listCats = catDAOHE.findCategoryByCodeAndCatType(code, CAT_PROCEDURE);

        if (listCats.isEmpty()) {
            return null;
        } else {
            c = listCats.get(0);
        }
        return c;
    }

    public Long getProcedureTypeIdByCode(String code) {
        Long flowId;
        // viethd:
        // find flow id due to FLOW_CODE from category tables
        CategoryDAOHE catDAOHE = new CategoryDAOHE();
        List<Category> listCats = catDAOHE.findCategoryByCodeAndCatType(code, CAT_PROCEDURE);

        if (listCats.isEmpty()) {
            return null;
        } else {
            flowId = listCats.get(0).getCategoryId();
        }
        return flowId;
    }

    public String getProcedureNameByCode(String code) {
        String flowName;
        // viethd:
        // find flow id due to FLOW_CODE from category tables
        CategoryDAOHE catDAOHE = new CategoryDAOHE();
        List<Category> listCats = catDAOHE.findCategoryByCodeAndCatType(code, CAT_PROCEDURE);

        if (listCats.isEmpty()) {
            return null;
        } else {
            flowName = listCats.get(0).getName();
        }
        return flowName;
    }

    public Long getCatIdByCode(String code) {
        Long id = -1l;
        CategoryDAOHE catDAOHE = new CategoryDAOHE();
        List<Category> listCats = catDAOHE.findCategoryByCode(code);
        if (!listCats.isEmpty()) {
            id = listCats.get(0).getCategoryId();
        }
        return id;
    }

    public Flow getFlowByFileId(Long fileId) {
        FilesDAOHE fileDAOHE = new FilesDAOHE();
        Files f = fileDAOHE.findById(fileId);
        return (new FlowDAOHE()).getFlowById(f.getFlowId());
    }

    public List getFlowByDeptNObject(Long deptId, Long docType) {
        List<Flow> lstFlows = (new FlowDAOHE()).getFlowByDeptNObject(deptId, docType);
        return lstFlows;
    }

    public List getNextActionOfNodeId(Long nodeId) {
        NodeToNodeDAOHE node2NodeDAOHE = new NodeToNodeDAOHE();
        return node2NodeDAOHE.getNextAction(nodeId);
    }

    public Node getNodeById(Long nodeId) {
        NodeDAOHE nodeDAOHE = new NodeDAOHE();
        return nodeDAOHE.getNodeById(nodeId);
    }

    public List<Node> findNodeByFlowIdNStatus(Long flowId, Long status) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findNodeByFlowIdNStatus(flowId, status);
    }

    public List<Node> findNodeByFlowIdNActionStatus(Long flowId, Long status) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findNodeByFlowIdNActionStatus(flowId, status);
    }
    /*
     * linhdx
     * tim theo list status
     */

    public List<Node> findNodeByFlowIdNActionStatus(Long flowId, List<Long> lstStatus) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findNodeByFlowIdNActionStatus(flowId, lstStatus);
    }

    public List<NodeToNode> findActionsByNodeId(Long nodeId) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findActionsByNodeId(nodeId);
    }

    /**
     *
     * @param flow
     * @return
     */
    public Node getFirstNodeOfFlow(Flow flow) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.getFirstNodeOfFlow(flow);
    }

    /**
     *
     * @param docId ID of document
     * @param docType Type of document
     * @param status Status of document
     * @param receiverId ID of user receiving this document
     * @param receiverGroupId ID of department receiving this document
     * @return list of Process objects which are parents of current process
     */
    public List<Process> findPreviousProcess(Long docId, Long docType, List<Long> lstStatus) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findPreviousProcess(docId, docType, lstStatus);
    }

    public List<Process> findAllCurrentProcess(Long docId, Long docType, Long status) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findALlCurrentProcess(docId, docType, status);
    }

    /**
     * Find all records NodeDeptUser of a node in flow
     *
     * @param nodeId having ID=nodeId
     * @return
     */
    public List<NodeDeptUser> findNDUsByNodeId(Long nodeId, Boolean curentDept) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findNDUsByNodeId(nodeId, curentDept);
    }

    public List<NodeDeptUser> findNDUsByNodeId(Long nodeId, Long receiveUserId) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findNDUsByNodeId(nodeId, receiveUserId);
    }

    public List<NodeDeptUser> findNDUsByAction(NodeToNode action) {
        Long nextId = action.getNextId();
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findNDUsByNodeId(nextId, false);
    }

    public List<NodeDeptUser> findNDUsByActionId(Long actionId) {
        NodeToNodeDAOHE actionDAOHE = new NodeToNodeDAOHE();
        NodeToNode action = actionDAOHE.findById(actionId);
        if (action == null) {
            return null;
        }
        Long nextId = action.getNextId();
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findNDUsByNodeId(nextId, false);
    }

    // viethd3 30/01/2015:
    // find all available next-actions of document 
    // depending on document status and processing deparment
    // algorithme 
    /*
     * public List<NodeToNode> findAvaiableNextActions(Long docType, Long
     * docStatus, Long deptId) {
     *
     * List<Flow> lstFlows =
     * WorkflowAPI.getInstance().getFlowByDeptNObject(deptId, docType); Flow f =
     * lstFlows.get(0); List<Node> lstNode; List<Node> lstNextNodes = new
     * ArrayList<>(); Node nextNode; if(docStatus ==
     * Constants.PROCESS_STATUS.INITIAL) nextNode = WorkflowAPI.getInstance()
     * .getFirstNodeOfFlow(f); else{ lstNode= WorkflowAPI.getInstance()
     * .findNodeByFlowIdNStatus(f.getFlowId(), docStatus); for(Node node:
     * lstNode){ List<NodeToNode> actions = WorkflowAPI.getInstance()
     * .findActionsByNodeId(node.getNodeId()); for(NodeToNode action : actions){
     * Node temp = WorkflowAPI.getInstance() .getNodeById(action.getNextId());
     * lstNextNodes.add(temp); } } nextNode = null; } List<NodeToNode> actions =
     * new ArrayList<>(); if(nextNode != null) actions =
     * WorkflowAPI.getInstance().getNextActionOfNodeId(nextNode.getNodeId());
     * else{ for(Node node:lstNextNodes){
     * actions.addAll(WorkflowAPI.getInstance().getNextActionOfNodeId(node.getNodeId()));
     * } } return actions; }
     */
    public List<NodeToNode> findAvaiableNextActions(Long docType, Long docStatus, Long deptId) {
        WorkflowAPI workflowInstance = WorkflowAPI.getInstance();
        List<Flow> lstFlows = workflowInstance.getFlowByDeptNObject(deptId, docType);
        Flow f = lstFlows.get(0);
        List<Node> lstNode;
        Node nextNode;
        List<NodeToNode> actions = new ArrayList<>();
        if (docStatus == Constants.PROCESS_STATUS.INITIAL) {
            nextNode = WorkflowAPI.getInstance().getFirstNodeOfFlow(f);
        } else {
            lstNode = workflowInstance.findNodeByFlowIdNActionStatus(f.getFlowId(), docStatus);
            for (Node node : lstNode) {
                actions.addAll(workflowInstance.findActionsByNodeId(node.getNodeId()));
            }
            nextNode = null;
        }
        if (nextNode != null) {
            actions = workflowInstance.getNextActionOfNodeId(nextNode.getNodeId());
        } else {
        }
        return actions;
    }

    public List<NodeToNode> findAvaiableNextActions(Long docType, List<Long> lstStatus, Long deptId) {
        WorkflowAPI workflowInstance = WorkflowAPI.getInstance();
        List<Flow> lstFlows = workflowInstance.getFlowByDeptNObject(deptId, docType);
        Flow f = lstFlows.get(0);
        List<Node> lstNode;
        Node nextNode;
        List<NodeToNode> actions = new ArrayList<>();
        if (lstStatus == null || lstStatus.isEmpty()) {
            nextNode = WorkflowAPI.getInstance().getFirstNodeOfFlow(f);
        } else if ((lstStatus.size() == 1) && lstStatus.get(0) == Constants.PROCESS_STATUS.INITIAL) {
            nextNode = WorkflowAPI.getInstance().getFirstNodeOfFlow(f);
        } else if ((lstStatus.size() == 1) && lstStatus.get(0) == Constants.PROCESS_STATUS.NO_NEED_PROCESS) {
            //linhdx neu co flag khong phai xu ly thi return khong co action nao
            return new ArrayList();
        } else {
            lstNode = workflowInstance.findNodeByFlowIdNActionStatus(f.getFlowId(), lstStatus);
            for (Node node : lstNode) {
                actions.addAll(workflowInstance.findActionsByNodeId(node.getNodeId()));
            }
            nextNode = null;
        }
        if (nextNode != null) {
            actions = workflowInstance.getNextActionOfNodeId(nextNode.getNodeId());
        } else {
        }
        return actions;
    }

    public Process getCurrentProcess(Long docId, Long docType, Long status, Long receiverId) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        Process p = coreDAOHE.findProcessByStatusAndReceiverId(docId, docType, status, receiverId);
        if (p != null) {
            return p;
        }
        return null;
    }

    public static String getCtypeFile(String fileType) {
        String sTemp = "";
        if ("bmp".equals(fileType)) {
            sTemp = "image/bmp";
        } else if ("css".equals(fileType)) {
            sTemp = "text/css";
        } else if ("dtd".equals(fileType)) {
            sTemp = "application/xml-dtd";
        } else if ("doc".equals(fileType)) {
            sTemp = "application/msword";
        } else if ("docx".equals(fileType)) {
            sTemp = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        } else if ("dotx".equals(fileType)) {
            sTemp = "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
        } else if ("es".equals(fileType)) {
            sTemp = "application/ecmascript";
        } else if ("exe".equals(fileType)) {
            sTemp = "application/octet-stream";
        } else if ("gif".equals(fileType)) {
            sTemp = "image/gif";
        } else if ("gz".equals(fileType)) {
            sTemp = "application/x-gzip";
        } else if ("hqx".equals(fileType)) {
            sTemp = "application/mac-binhex40";
        } else if ("html".equals(fileType)) {
            sTemp = "text/html";
        } else if ("jpg".equals(fileType)) {
            sTemp = "image/jpeg";
        } else if ("js".equals(fileType)) {
            sTemp = "application/x-javascript";
        } else if ("mpeg".equals(fileType)) {
            sTemp = "video/mpeg";
        } else if ("ogg".equals(fileType)) {
            sTemp = "audio/vorbis, application/ogg";
        } else if ("pdf".equals(fileType)) {
            sTemp = "application/pdf";
        } else if ("png".equals(fileType)) {
            sTemp = "text/css";
        } else if ("potx".equals(fileType)) {
            sTemp = "application/vnd.openxmlformats-officedocument.presentationml.template";
        } else if ("ppsx".equals(fileType)) {
            sTemp = "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
        } else if ("ppt".equals(fileType)) {
            sTemp = "application/vnd.ms-powerpointtd";
        } else if ("pptx".equals(fileType)) {
            sTemp = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
        } else if ("svg".equals(fileType)) {
            sTemp = "image/svg+xml";
        } else if ("swf".equals(fileType)) {
            sTemp = "application/x-shockwave-flash";
        } else if ("tiff".equals(fileType)) {
            sTemp = "image/tiff";
        } else if ("txt".equals(fileType)) {
            sTemp = "text/plain";
        } else if ("xls".equals(fileType)) {
            sTemp = "application/vnd.ms-excel";
        } else if ("xlsb".equals(fileType)) {
            sTemp = "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
        } else if ("xlsx".equals(fileType)) {
            sTemp = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        } else if ("xltx".equals(fileType)) {
            sTemp = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
        } else if ("xml".equals(fileType)) {
            sTemp = "application/xml";
        }
        return sTemp;
    }

    public long getProcedureTypeIdByCode(Long IMPORT_ORDER_FILE) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * linhdx Lay danh sach trang thai tu process
     *
     * @param fileId
     * @param userId
     * @return
     */
    public List<Long> getProcessStatus(Long fileId, Long fileType, Long receiverId) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        List<Long> lstStatus = coreDAOHE.findListProcessStatusByReceiverId(fileId, fileType, receiverId);
        return lstStatus;
    }

    public List<Process> getProcess(Long fileId, Long fileType, Long receiverId) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        List<Process> lstProces = coreDAOHE.findListProcessByReceiverId(fileId, fileType, receiverId);
        return lstProces;
    }

    public List<Process> getProcess(Long fileId, Long fileType, Long receiverId, Long status) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        List<Process> lstProces = coreDAOHE.findListProcessByReceiverIdAndStatus(fileId, fileType, receiverId, status);
        return lstProces;
    }

    public List<Process> getAllProcessNotFinish(Long fileId, Long fileType) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        List<Process> lstProces = coreDAOHE.findAllProcessNotFinish(fileId, fileType);
        return lstProces;
    }

    public List<Process> getAllProcessContainFinish(Long fileId, Long fileType) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        List<Process> lstProces = coreDAOHE.findAllProcessContainFinish(fileId, fileType);
        return lstProces;
    }

    public List<NodeDeptUser> findNDUsByUserId(List<Long> lstUserId, Long nodeId) {
        CoreProcessDAOHE coreDAOHE = new CoreProcessDAOHE();
        return coreDAOHE.findNDUsByUserId(lstUserId, nodeId);
    }

    private void sendEmailSMS(Long receiverId, Long docId, String username, String actionTypeName, Long userId) {
        try {
            // Insert vao bang MESSAGE
            UserDAOHE userDAO = new UserDAOHE();
            Users userReceive = userDAO.findById(receiverId);
            Files files = new FilesDAOHE().findById(docId);
            MessageEmail msgEmail = new MessageEmail();
            String content = username + " vừa thực hiện '" + actionTypeName + "', mã hồ sơ: " + files.getNswFileCode() + ". Đề nghị vào hệ thống xử lý";
            msgEmail.setContent(content);
            msgEmail.setSenderId(userId);
            msgEmail.setIsSent(0L);
            msgEmail.setSentTimeReq(new Date());
            msgEmail.setSendCount(0L);
            msgEmail.setReceiveEmail(userReceive.getEmail());
            MessageDAOHE messageDAOHE = new MessageDAOHE();
            messageDAOHE.saveOrUpdate(msgEmail);
            messageDAOHE.updateMessageEmailFlag();
            MessageSMSDAOHE messageSmsDAOHE = new MessageSMSDAOHE();
            MessageSms msgSms = new MessageSms();
            msgSms.setContent(content);
            msgSms.setSenderId(userId);
            msgSms.setIsSent(0L);
            msgSms.setSentTimeReq(new Date());
            msgSms.setSendCount(0L);
            msgSms.setPhoneNumber(userReceive.getTelephone());
            messageSmsDAOHE.saveOrUpdate(msgSms);
            messageSmsDAOHE.updateMessageSmsFlag();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }

    }
}
