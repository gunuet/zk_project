// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 8/15/2012 3:00:27 PM
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   VsaFilter.java
package com.viettel.filter;

import com.viettel.core.sys.model.Menu;
import com.viettel.core.user.model.UserToken;
import com.viettel.utils.HibernateUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

// Referenced classes of package viettel.passport.util:
//            Connector
public class SessionFilter implements Filter {

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        boolean errorWhenExcuteAction = false;
        try {
            HttpServletRequest req = null;
            HttpServletResponse res = null;
            if (request instanceof HttpServletRequest) {
                req = (HttpServletRequest) request;
            }

            if (response instanceof HttpServletResponse) {
                res = (HttpServletResponse) response;
            }
            if(res == null){
                return;
            }

            if (req != null && !"POST".equals(req.getMethod())) {
                if (req.getSession().getAttribute("userToken") == null) {
                    boolean check = checkAllowUrl(req);
                    if (check) {
                        chain.doFilter(request, response);
                    } else {
                        res.sendRedirect(req.getContextPath() + "/Pages/homepage.zul");
                    }
                    //linhdx end
                } else {
                    if (checkPermission(req)) {
                        chain.doFilter(request, response);
                    }
                }
            } else {
                chain.doFilter(request, response);
            }
        } catch (IOException | ServletException en) {
            LogUtils.addLog(en);
            errorWhenExcuteAction = true;
        } finally {
            if (!errorWhenExcuteAction) {
                try {
                    HibernateUtil.commitCurrentSessions();
                } catch (Exception ex) {
                    LogUtils.addLog(ex);
                } finally {
                    try {
                        HibernateUtil.closeCurrentSessions();
                    } catch (Exception ex) {
                        LogUtils.addLog(ex);
                    }
                }
            }
        }
    }

    private boolean checkAllowUrl(HttpServletRequest req) {
        String url = ResourceBundleUtil.getString("url_path_nologin");
        String[] arrUrl;
        String regex = ",";
        arrUrl = url.split(regex);
        //linhdx begin
        boolean check = false;
        for (String arrUrl1 : arrUrl) {
            if (!arrUrl1.isEmpty() && !arrUrl1.trim().isEmpty() && req.getRequestURL().indexOf(req.getContextPath() + arrUrl1.trim()) >= 0) {
                check = true;
                break;
            }
        }
        return check;

    }

    @Override
    public void destroy() {
    }

    private boolean checkPermission(HttpServletRequest req) {
        boolean check = checkAllowUrl(req);
        if(check){
            return true;
        }
        String url = req.getRequestURI();
//        if (url.contains("/Pages/login.zul") || url.contains("/index.zul") || url.contains("/Share/upload/")
//                || url.contains("countMenu") || url.contains("/Pages/tracuu.zul")) {
//            return true;
//        }
        UserToken token = (UserToken) req.getSession().getAttribute("userToken");
        if (token == null) {
            return false;
        }
        if (url.equals(req.getContextPath() + "/") || url.equals(req.getContextPath() + "/index.zul") || url.equals(req.getContextPath() + "/Pages/logout.zul")) {
            return true;
        }
        if (token.getLstMenu() != null) {
            for (int i = 0; i < token.getLstMenu().size(); i++) {
                Menu menu = (Menu) token.getLstMenu().get(i);
                if (url.equals(menu.getMenuUrl())) {
                    return true;
                } else if (menu.getLstMenu() != null) {
                    for (int j = 0; j < menu.getLstMenu().size(); j++) {
                        Menu childMenu = (Menu) menu.getLstMenu().get(j);
                        if (url.equals(childMenu.getMenuUrl())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
