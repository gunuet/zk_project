/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.BO.test;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "TEST_BILL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TestBill.findAll", query = "SELECT t FROM TestBill t")
    , @NamedQuery(name = "TestBill.findByTestBillId", query = "SELECT t FROM TestBill t WHERE t.testBillId = :testBillId")
    , @NamedQuery(name = "TestBill.findByTestBillcode", query = "SELECT t FROM TestBill t WHERE t.testBillcode = :testBillcode")
    , @NamedQuery(name = "TestBill.findByTesBillName", query = "SELECT t FROM TestBill t WHERE t.tesBillName = :tesBillName")
    , @NamedQuery(name = "TestBill.findByBillNumberMove", query = "SELECT t FROM TestBill t WHERE t.billNumberMove = :billNumberMove")
    , @NamedQuery(name = "TestBill.findByBillNumberBill", query = "SELECT t FROM TestBill t WHERE t.billNumberBill = :billNumberBill")
    , @NamedQuery(name = "TestBill.findByBillNumberContract", query = "SELECT t FROM TestBill t WHERE t.billNumberContract = :billNumberContract")
    , @NamedQuery(name = "TestBill.findByTimeImport", query = "SELECT t FROM TestBill t WHERE t.timeImport = :timeImport")
    , @NamedQuery(name = "TestBill.findByGateOut", query = "SELECT t FROM TestBill t WHERE t.gateOut = :gateOut")
    , @NamedQuery(name = "TestBill.findByGateIn", query = "SELECT t FROM TestBill t WHERE t.gateIn = :gateIn")
    , @NamedQuery(name = "TestBill.findByCustomsBranch", query = "SELECT t FROM TestBill t WHERE t.customsBranch = :customsBranch")
    , @NamedQuery(name = "TestBill.findByOrganirationCheck", query = "SELECT t FROM TestBill t WHERE t.organirationCheck = :organirationCheck")
    , @NamedQuery(name = "TestBill.findByPlaceCheck", query = "SELECT t FROM TestBill t WHERE t.placeCheck = :placeCheck")
    , @NamedQuery(name = "TestBill.findByDateCheck", query = "SELECT t FROM TestBill t WHERE t.dateCheck = :dateCheck")})
public class TestBill implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TEST_BILL_ID")
    private Long testBillId;
    @Size(max = 20)
    @Column(name = "TEST_BILLCODE")
    private String testBillcode;
    @Size(max = 20)
    @Column(name = "TES_BILL_NAME")
    private String tesBillName;
    @Size(max = 20)
    @Column(name = "BILL_NUMBER_MOVE")
    private String billNumberMove;
    @Size(max = 20)
    @Column(name = "BILL_NUMBER_BILL")
    private String billNumberBill;
    @Size(max = 20)
    @Column(name = "BILL_NUMBER_CONTRACT")
    private String billNumberContract;
    @Size(max = 20)
    @Column(name = "TIME_IMPORT")
    private String timeImport;
    @Size(max = 20)
    @Column(name = "GATE_OUT")
    private String gateOut;
    @Size(max = 20)
    @Column(name = "GATE_IN")
    private String gateIn;
    @Size(max = 20)
    @Column(name = "CUSTOMS_BRANCH")
    private String customsBranch;
    @Size(max = 20)
    @Column(name = "ORGANIRATION_CHECK")
    private String organirationCheck;
    @Size(max = 20)
    @Column(name = "PLACE_CHECK")
    private String placeCheck;
    @Column(name = "DATE_CHECK")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCheck;

    public TestBill() {
    }

    public TestBill(Long testBillId) {
        this.testBillId = testBillId;
    }

    public Long getTestBillId() {
        return testBillId;
    }

    public void setTestBillId(Long testBillId) {
        this.testBillId = testBillId;
    }

    public String getTestBillcode() {
        return testBillcode;
    }

    public void setTestBillcode(String testBillcode) {
        this.testBillcode = testBillcode;
    }

    public String getTesBillName() {
        return tesBillName;
    }

    public void setTesBillName(String tesBillName) {
        this.tesBillName = tesBillName;
    }

    public String getBillNumberMove() {
        return billNumberMove;
    }

    public void setBillNumberMove(String billNumberMove) {
        this.billNumberMove = billNumberMove;
    }

    public String getBillNumberBill() {
        return billNumberBill;
    }

    public void setBillNumberBill(String billNumberBill) {
        this.billNumberBill = billNumberBill;
    }

    public String getBillNumberContract() {
        return billNumberContract;
    }

    public void setBillNumberContract(String billNumberContract) {
        this.billNumberContract = billNumberContract;
    }

    public String getTimeImport() {
        return timeImport;
    }

    public void setTimeImport(String timeImport) {
        this.timeImport = timeImport;
    }

    public String getGateOut() {
        return gateOut;
    }

    public void setGateOut(String gateOut) {
        this.gateOut = gateOut;
    }

    public String getGateIn() {
        return gateIn;
    }

    public void setGateIn(String gateIn) {
        this.gateIn = gateIn;
    }

    public String getCustomsBranch() {
        return customsBranch;
    }

    public void setCustomsBranch(String customsBranch) {
        this.customsBranch = customsBranch;
    }

    public String getOrganirationCheck() {
        return organirationCheck;
    }

    public void setOrganirationCheck(String organirationCheck) {
        this.organirationCheck = organirationCheck;
    }

    public String getPlaceCheck() {
        return placeCheck;
    }

    public void setPlaceCheck(String placeCheck) {
        this.placeCheck = placeCheck;
    }

    public Date getDateCheck() {
        return dateCheck;
    }

    public void setDateCheck(Date dateCheck) {
        this.dateCheck = dateCheck;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testBillId != null ? testBillId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TestBill)) {
            return false;
        }
        TestBill other = (TestBill) object;
        if ((this.testBillId == null && other.testBillId != null) || (this.testBillId != null && !this.testBillId.equals(other.testBillId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.BO.test.TestBill[ testBillId=" + testBillId + " ]";
    }
    
}
