/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.BO.test;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "TEST_FILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TestFile.findAll", query = "SELECT t FROM TestFile t")
    , @NamedQuery(name = "TestFile.findByTestFileId", query = "SELECT t FROM TestFile t WHERE t.testFileId = :testFileId")
    , @NamedQuery(name = "TestFile.findByTestFileName", query = "SELECT t FROM TestFile t WHERE t.testFileName = :testFileName")
    , @NamedQuery(name = "TestFile.findByTestFileCode", query = "SELECT t FROM TestFile t WHERE t.testFileCode = :testFileCode")
    , @NamedQuery(name = "TestFile.findByTestFileStatus", query = "SELECT t FROM TestFile t WHERE t.testFileStatus = :testFileStatus")
    , @NamedQuery(name = "TestFile.findByOwnerId", query = "SELECT t FROM TestFile t WHERE t.ownerId = :ownerId")
    , @NamedQuery(name = "TestFile.findByOwnerName", query = "SELECT t FROM TestFile t WHERE t.ownerName = :ownerName")
    , @NamedQuery(name = "TestFile.findByOwnerCode", query = "SELECT t FROM TestFile t WHERE t.ownerCode = :ownerCode")
    , @NamedQuery(name = "TestFile.findByOwnerAddress", query = "SELECT t FROM TestFile t WHERE t.ownerAddress = :ownerAddress")
    , @NamedQuery(name = "TestFile.findByOwnerPhone", query = "SELECT t FROM TestFile t WHERE t.ownerPhone = :ownerPhone")
    , @NamedQuery(name = "TestFile.findByOwnerFax", query = "SELECT t FROM TestFile t WHERE t.ownerFax = :ownerFax")
    , @NamedQuery(name = "TestFile.findByOwnerEmail", query = "SELECT t FROM TestFile t WHERE t.ownerEmail = :ownerEmail")
    , @NamedQuery(name = "TestFile.findByDateIn", query = "SELECT t FROM TestFile t WHERE t.dateIn = :dateIn")
    , @NamedQuery(name = "TestFile.findByResponseBussinessId", query = "SELECT t FROM TestFile t WHERE t.responseBussinessId = :responseBussinessId")
    , @NamedQuery(name = "TestFile.findByResponseBussinessCode", query = "SELECT t FROM TestFile t WHERE t.responseBussinessCode = :responseBussinessCode")
    , @NamedQuery(name = "TestFile.findByResponseBussinessName", query = "SELECT t FROM TestFile t WHERE t.responseBussinessName = :responseBussinessName")
    , @NamedQuery(name = "TestFile.findByResponseBussinessAddress", query = "SELECT t FROM TestFile t WHERE t.responseBussinessAddress = :responseBussinessAddress")
    , @NamedQuery(name = "TestFile.findByResponseBussinessPhone", query = "SELECT t FROM TestFile t WHERE t.responseBussinessPhone = :responseBussinessPhone")
    , @NamedQuery(name = "TestFile.findByResponseBussinessFax", query = "SELECT t FROM TestFile t WHERE t.responseBussinessFax = :responseBussinessFax")
    , @NamedQuery(name = "TestFile.findByResponseBussinessEmail", query = "SELECT t FROM TestFile t WHERE t.responseBussinessEmail = :responseBussinessEmail")
    , @NamedQuery(name = "TestFile.findByExportBussinessId", query = "SELECT t FROM TestFile t WHERE t.exportBussinessId = :exportBussinessId")
    , @NamedQuery(name = "TestFile.findByExportBussinessCode", query = "SELECT t FROM TestFile t WHERE t.exportBussinessCode = :exportBussinessCode")
    , @NamedQuery(name = "TestFile.findByExportBussinessName", query = "SELECT t FROM TestFile t WHERE t.exportBussinessName = :exportBussinessName")
    , @NamedQuery(name = "TestFile.findByExportBussinessAddress", query = "SELECT t FROM TestFile t WHERE t.exportBussinessAddress = :exportBussinessAddress")
    , @NamedQuery(name = "TestFile.findByExportBussinessPhone", query = "SELECT t FROM TestFile t WHERE t.exportBussinessPhone = :exportBussinessPhone")
    , @NamedQuery(name = "TestFile.findByExportBussinessFax", query = "SELECT t FROM TestFile t WHERE t.exportBussinessFax = :exportBussinessFax")
    , @NamedQuery(name = "TestFile.findByExportBussinessEmail", query = "SELECT t FROM TestFile t WHERE t.exportBussinessEmail = :exportBussinessEmail")})
public class TestFile implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TEST_FILE_ID")
    private Long testFileId;
    @Size(max = 200)
    @Column(name = "TEST_FILE_NAME")
    private String testFileName;
    @Size(max = 100)
    @Column(name = "TEST_FILE_CODE")
    private String testFileCode;
    @Size(max = 20)
    @Column(name = "TEST_FILE_STATUS")
    private String testFileStatus;
    @Column(name = "OWNER_ID")
    private Long ownerId;
    @Size(max = 100)
    @Column(name = "OWNER_NAME")
    private String ownerName;
    @Size(max = 100)
    @Column(name = "OWNER_CODE")
    private String ownerCode;
    @Size(max = 100)
    @Column(name = "OWNER_ADDRESS")
    private String ownerAddress;
    @Size(max = 100)
    @Column(name = "OWNER_PHONE")
    private String ownerPhone;
    @Size(max = 100)
    @Column(name = "OWNER_FAX")
    private String ownerFax;
    @Size(max = 100)
    @Column(name = "OWNER_EMAIL")
    private String ownerEmail;
    @Column(name = "DATE_IN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateIn;
    @Column(name = "RESPONSE_BUSSINESS_ID")
    private Long responseBussinessId;
    @Size(max = 100)
    @Column(name = "RESPONSE_BUSSINESS_CODE")
    private String responseBussinessCode;
    @Size(max = 100)
    @Column(name = "RESPONSE_BUSSINESS_NAME")
    private String responseBussinessName;
    @Size(max = 100)
    @Column(name = "RESPONSE_BUSSINESS_ADDRESS")
    private String responseBussinessAddress;
    @Size(max = 100)
    @Column(name = "RESPONSE_BUSSINESS_PHONE")
    private String responseBussinessPhone;
    @Size(max = 100)
    @Column(name = "RESPONSE_BUSSINESS_FAX")
    private String responseBussinessFax;
    @Size(max = 100)
    @Column(name = "RESPONSE_BUSSINESS_EMAIL")
    private String responseBussinessEmail;
    @Column(name = "EXPORT_BUSSINESS_ID")
    private BigInteger exportBussinessId;
    @Size(max = 100)
    @Column(name = "EXPORT_BUSSINESS_CODE")
    private String exportBussinessCode;
    @Size(max = 100)
    @Column(name = "EXPORT_BUSSINESS_NAME")
    private String exportBussinessName;
    @Size(max = 100)
    @Column(name = "EXPORT_BUSSINESS_ADDRESS")
    private String exportBussinessAddress;
    @Size(max = 100)
    @Column(name = "EXPORT_BUSSINESS_PHONE")
    private String exportBussinessPhone;
    @Size(max = 100)
    @Column(name = "EXPORT_BUSSINESS_FAX")
    private String exportBussinessFax;
    @Size(max = 100)
    @Column(name = "EXPORT_BUSSINESS_EMAIL")
    private String exportBussinessEmail;

    public TestFile() {
    }

    public TestFile(Long testFileId) {
        this.testFileId = testFileId;
    }

    public Long getTestFileId() {
        return testFileId;
    }

    public void setTestFileId(Long testFileId) {
        this.testFileId = testFileId;
    }

    public String getTestFileName() {
        return testFileName;
    }

    public void setTestFileName(String testFileName) {
        this.testFileName = testFileName;
    }

    public String getTestFileCode() {
        return testFileCode;
    }

    public void setTestFileCode(String testFileCode) {
        this.testFileCode = testFileCode;
    }

    public String getTestFileStatus() {
        return testFileStatus;
    }

    public void setTestFileStatus(String testFileStatus) {
        this.testFileStatus = testFileStatus;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerCode() {
        return ownerCode;
    }

    public void setOwnerCode(String ownerCode) {
        this.ownerCode = ownerCode;
    }

    public String getOwnerAddress() {
        return ownerAddress;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public String getOwnerFax() {
        return ownerFax;
    }

    public void setOwnerFax(String ownerFax) {
        this.ownerFax = ownerFax;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public Date getDateIn() {
        return dateIn;
    }

    public void setDateIn(Date dateIn) {
        this.dateIn = dateIn;
    }

    public Long getResponseBussinessId() {
        return responseBussinessId;
    }

    public void setResponseBussinessId(Long responseBussinessId) {
        this.responseBussinessId = responseBussinessId;
    }

    public String getResponseBussinessCode() {
        return responseBussinessCode;
    }

    public void setResponseBussinessCode(String responseBussinessCode) {
        this.responseBussinessCode = responseBussinessCode;
    }

    public String getResponseBussinessName() {
        return responseBussinessName;
    }

    public void setResponseBussinessName(String responseBussinessName) {
        this.responseBussinessName = responseBussinessName;
    }

    public String getResponseBussinessAddress() {
        return responseBussinessAddress;
    }

    public void setResponseBussinessAddress(String responseBussinessAddress) {
        this.responseBussinessAddress = responseBussinessAddress;
    }

    public String getResponseBussinessPhone() {
        return responseBussinessPhone;
    }

    public void setResponseBussinessPhone(String responseBussinessPhone) {
        this.responseBussinessPhone = responseBussinessPhone;
    }

    public String getResponseBussinessFax() {
        return responseBussinessFax;
    }

    public void setResponseBussinessFax(String responseBussinessFax) {
        this.responseBussinessFax = responseBussinessFax;
    }

    public String getResponseBussinessEmail() {
        return responseBussinessEmail;
    }

    public void setResponseBussinessEmail(String responseBussinessEmail) {
        this.responseBussinessEmail = responseBussinessEmail;
    }

    public BigInteger getExportBussinessId() {
        return exportBussinessId;
    }

    public void setExportBussinessId(BigInteger exportBussinessId) {
        this.exportBussinessId = exportBussinessId;
    }

    public String getExportBussinessCode() {
        return exportBussinessCode;
    }

    public void setExportBussinessCode(String exportBussinessCode) {
        this.exportBussinessCode = exportBussinessCode;
    }

    public String getExportBussinessName() {
        return exportBussinessName;
    }

    public void setExportBussinessName(String exportBussinessName) {
        this.exportBussinessName = exportBussinessName;
    }

    public String getExportBussinessAddress() {
        return exportBussinessAddress;
    }

    public void setExportBussinessAddress(String exportBussinessAddress) {
        this.exportBussinessAddress = exportBussinessAddress;
    }

    public String getExportBussinessPhone() {
        return exportBussinessPhone;
    }

    public void setExportBussinessPhone(String exportBussinessPhone) {
        this.exportBussinessPhone = exportBussinessPhone;
    }

    public String getExportBussinessFax() {
        return exportBussinessFax;
    }

    public void setExportBussinessFax(String exportBussinessFax) {
        this.exportBussinessFax = exportBussinessFax;
    }

    public String getExportBussinessEmail() {
        return exportBussinessEmail;
    }

    public void setExportBussinessEmail(String exportBussinessEmail) {
        this.exportBussinessEmail = exportBussinessEmail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testFileId != null ? testFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TestFile)) {
            return false;
        }
        TestFile other = (TestFile) object;
        if ((this.testFileId == null && other.testFileId != null) || (this.testFileId != null && !this.testFileId.equals(other.testFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.BO.test.TestFile[ testFileId=" + testFileId + " ]";
    }
    
}
