/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.BO.test;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "TEST_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TestProduct.findAll", query = "SELECT t FROM TestProduct t")
    , @NamedQuery(name = "TestProduct.findByTestProductId", query = "SELECT t FROM TestProduct t WHERE t.testProductId = :testProductId")
    , @NamedQuery(name = "TestProduct.findByTestProductName", query = "SELECT t FROM TestProduct t WHERE t.testProductName = :testProductName")
    , @NamedQuery(name = "TestProduct.findByTestProductCode", query = "SELECT t FROM TestProduct t WHERE t.testProductCode = :testProductCode")
    , @NamedQuery(name = "TestProduct.findByProductDepcription", query = "SELECT t FROM TestProduct t WHERE t.productDepcription = :productDepcription")
    , @NamedQuery(name = "TestProduct.findByMadeIn", query = "SELECT t FROM TestProduct t WHERE t.madeIn = :madeIn")
    , @NamedQuery(name = "TestProduct.findByNumberPublish", query = "SELECT t FROM TestProduct t WHERE t.numberPublish = :numberPublish")
    , @NamedQuery(name = "TestProduct.findByNumbers", query = "SELECT t FROM TestProduct t WHERE t.numbers = :numbers")
    , @NamedQuery(name = "TestProduct.findByWeights", query = "SELECT t FROM TestProduct t WHERE t.weights = :weights")
    , @NamedQuery(name = "TestProduct.findByCost", query = "SELECT t FROM TestProduct t WHERE t.cost = :cost")})
public class TestProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TEST_PRODUCT_ID")
    private Long testProductId;
    @Size(max = 100)
    @Column(name = "TEST_PRODUCT_NAME")
    private String testProductName;
    @Size(max = 100)
    @Column(name = "TEST_PRODUCT_CODE")
    private String testProductCode;
    @Size(max = 100)
    @Column(name = "PRODUCT_DEPCRIPTION")
    private String productDepcription;
    @Size(max = 100)
    @Column(name = "MADE_IN")
    private String madeIn;
    @Size(max = 100)
    @Column(name = "NUMBER_PUBLISH")
    private String numberPublish;
    @Column(name = "NUMBERS")
    private BigInteger numbers;
    @Column(name = "WEIGHTS")
    private Serializable weights;
    @Column(name = "COST")
    private BigInteger cost;

    public TestProduct() {
    }

    public TestProduct(Long testProductId) {
        this.testProductId = testProductId;
    }

    public Long getTestProductId() {
        return testProductId;
    }

    public void setTestProductId(Long testProductId) {
        this.testProductId = testProductId;
    }

    public String getTestProductName() {
        return testProductName;
    }

    public void setTestProductName(String testProductName) {
        this.testProductName = testProductName;
    }

    public String getTestProductCode() {
        return testProductCode;
    }

    public void setTestProductCode(String testProductCode) {
        this.testProductCode = testProductCode;
    }

    public String getProductDepcription() {
        return productDepcription;
    }

    public void setProductDepcription(String productDepcription) {
        this.productDepcription = productDepcription;
    }

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }

    public String getNumberPublish() {
        return numberPublish;
    }

    public void setNumberPublish(String numberPublish) {
        this.numberPublish = numberPublish;
    }

    public BigInteger getNumbers() {
        return numbers;
    }

    public void setNumbers(BigInteger numbers) {
        this.numbers = numbers;
    }

    public Serializable getWeights() {
        return weights;
    }

    public void setWeights(Serializable weights) {
        this.weights = weights;
    }

    public BigInteger getCost() {
        return cost;
    }

    public void setCost(BigInteger cost) {
        this.cost = cost;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (testProductId != null ? testProductId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TestProduct)) {
            return false;
        }
        TestProduct other = (TestProduct) object;
        if ((this.testProductId == null && other.testProductId != null) || (this.testProductId != null && !this.testProductId.equals(other.testProductId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.BO.test.TestProduct[ testProductId=" + testProductId + " ]";
    }
    
}
