/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.BO.test;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "TESTING_IMPORTED_FOOD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TestingImportedFood.findAll", query = "SELECT t FROM TestingImportedFood t")
    , @NamedQuery(name = "TestingImportedFood.findByFileId", query = "SELECT t FROM TestingImportedFood t WHERE t.fileId = :fileId")
    , @NamedQuery(name = "TestingImportedFood.findByNswFileCode", query = "SELECT t FROM TestingImportedFood t WHERE t.nswFileCode = :nswFileCode")
    , @NamedQuery(name = "TestingImportedFood.findByCheckMethod", query = "SELECT t FROM TestingImportedFood t WHERE t.checkMethod = :checkMethod")
    , @NamedQuery(name = "TestingImportedFood.findByCreatedDate", query = "SELECT t FROM TestingImportedFood t WHERE t.createdDate = :createdDate")
    , @NamedQuery(name = "TestingImportedFood.findByCreatedBy", query = "SELECT t FROM TestingImportedFood t WHERE t.createdBy = :createdBy")
    , @NamedQuery(name = "TestingImportedFood.findByModifiedDate", query = "SELECT t FROM TestingImportedFood t WHERE t.modifiedDate = :modifiedDate")
    , @NamedQuery(name = "TestingImportedFood.findByGoodsOwnerName", query = "SELECT t FROM TestingImportedFood t WHERE t.goodsOwnerName = :goodsOwnerName")
    , @NamedQuery(name = "TestingImportedFood.findByGoodsOwnerAddress", query = "SELECT t FROM TestingImportedFood t WHERE t.goodsOwnerAddress = :goodsOwnerAddress")
    , @NamedQuery(name = "TestingImportedFood.findByGoodsOwnerPhone", query = "SELECT t FROM TestingImportedFood t WHERE t.goodsOwnerPhone = :goodsOwnerPhone")
    , @NamedQuery(name = "TestingImportedFood.findByGoodsOwnerFax", query = "SELECT t FROM TestingImportedFood t WHERE t.goodsOwnerFax = :goodsOwnerFax")
    , @NamedQuery(name = "TestingImportedFood.findByGoodsOwnerEmail", query = "SELECT t FROM TestingImportedFood t WHERE t.goodsOwnerEmail = :goodsOwnerEmail")
    , @NamedQuery(name = "TestingImportedFood.findByResponsiblePersonName", query = "SELECT t FROM TestingImportedFood t WHERE t.responsiblePersonName = :responsiblePersonName")
    , @NamedQuery(name = "TestingImportedFood.findByResponsiblePersonAddress", query = "SELECT t FROM TestingImportedFood t WHERE t.responsiblePersonAddress = :responsiblePersonAddress")
    , @NamedQuery(name = "TestingImportedFood.findByResponsiblePersonPhone", query = "SELECT t FROM TestingImportedFood t WHERE t.responsiblePersonPhone = :responsiblePersonPhone")
    , @NamedQuery(name = "TestingImportedFood.findByResponsiblePersonFax", query = "SELECT t FROM TestingImportedFood t WHERE t.responsiblePersonFax = :responsiblePersonFax")
    , @NamedQuery(name = "TestingImportedFood.findByResponsiblePersonEmail", query = "SELECT t FROM TestingImportedFood t WHERE t.responsiblePersonEmail = :responsiblePersonEmail")
    , @NamedQuery(name = "TestingImportedFood.findByExporterName", query = "SELECT t FROM TestingImportedFood t WHERE t.exporterName = :exporterName")
    , @NamedQuery(name = "TestingImportedFood.findByExporterAddress", query = "SELECT t FROM TestingImportedFood t WHERE t.exporterAddress = :exporterAddress")
    , @NamedQuery(name = "TestingImportedFood.findByExporterPhone", query = "SELECT t FROM TestingImportedFood t WHERE t.exporterPhone = :exporterPhone")
    , @NamedQuery(name = "TestingImportedFood.findByExporterEmail", query = "SELECT t FROM TestingImportedFood t WHERE t.exporterEmail = :exporterEmail")
    , @NamedQuery(name = "TestingImportedFood.findByComingDate", query = "SELECT t FROM TestingImportedFood t WHERE t.comingDate = :comingDate")
    , @NamedQuery(name = "TestingImportedFood.findByExporterGateCode", query = "SELECT t FROM TestingImportedFood t WHERE t.exporterGateCode = :exporterGateCode")
    , @NamedQuery(name = "TestingImportedFood.findByExporterGateName", query = "SELECT t FROM TestingImportedFood t WHERE t.exporterGateName = :exporterGateName")
    , @NamedQuery(name = "TestingImportedFood.findByImporterGateCode", query = "SELECT t FROM TestingImportedFood t WHERE t.importerGateCode = :importerGateCode")
    , @NamedQuery(name = "TestingImportedFood.findByImporterGateName", query = "SELECT t FROM TestingImportedFood t WHERE t.importerGateName = :importerGateName")
    , @NamedQuery(name = "TestingImportedFood.findByCheckTime", query = "SELECT t FROM TestingImportedFood t WHERE t.checkTime = :checkTime")
    , @NamedQuery(name = "TestingImportedFood.findByCheckPlace", query = "SELECT t FROM TestingImportedFood t WHERE t.checkPlace = :checkPlace")
    , @NamedQuery(name = "TestingImportedFood.findByDeptCode", query = "SELECT t FROM TestingImportedFood t WHERE t.deptCode = :deptCode")
    , @NamedQuery(name = "TestingImportedFood.findByDeptName", query = "SELECT t FROM TestingImportedFood t WHERE t.deptName = :deptName")
    , @NamedQuery(name = "TestingImportedFood.findByIsDeleted", query = "SELECT t FROM TestingImportedFood t WHERE t.isDeleted = :isDeleted")})
public class TestingImportedFood implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "NSW_FILE_CODE")
    private Long nswFileCode;
    @Column(name = "CHECK_METHOD")
    private Short checkMethod;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Size(max = 14)
    @Column(name = "CREATED_BY")
    private String createdBy;
    @Column(name = "MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_NAME")
    private String goodsOwnerName;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_ADDRESS")
    private String goodsOwnerAddress;
    @Size(max = 50)
    @Column(name = "GOODS_OWNER_PHONE")
    private String goodsOwnerPhone;
    @Size(max = 20)
    @Column(name = "GOODS_OWNER_FAX")
    private String goodsOwnerFax;
    @Size(max = 210)
    @Column(name = "GOODS_OWNER_EMAIL")
    private String goodsOwnerEmail;
    @Size(max = 255)
    @Column(name = "RESPONSIBLE_PERSON_NAME")
    private String responsiblePersonName;
    @Size(max = 255)
    @Column(name = "RESPONSIBLE_PERSON_ADDRESS")
    private String responsiblePersonAddress;
    @Size(max = 50)
    @Column(name = "RESPONSIBLE_PERSON_PHONE")
    private String responsiblePersonPhone;
    @Size(max = 20)
    @Column(name = "RESPONSIBLE_PERSON_FAX")
    private String responsiblePersonFax;
    @Size(max = 210)
    @Column(name = "RESPONSIBLE_PERSON_EMAIL")
    private String responsiblePersonEmail;
    @Size(max = 255)
    @Column(name = "EXPORTER_NAME")
    private String exporterName;
    @Size(max = 255)
    @Column(name = "EXPORTER_ADDRESS")
    private String exporterAddress;
    @Size(max = 50)
    @Column(name = "EXPORTER_PHONE")
    private String exporterPhone;
    @Size(max = 210)
    @Column(name = "EXPORTER_EMAIL")
    private String exporterEmail;
    @Column(name = "COMING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comingDate;
    @Size(max = 6)
    @Column(name = "EXPORTER_GATE_CODE")
    private String exporterGateCode;
    @Size(max = 255)
    @Column(name = "EXPORTER_GATE_NAME")
    private String exporterGateName;
    @Size(max = 6)
    @Column(name = "IMPORTER_GATE_CODE")
    private String importerGateCode;
    @Size(max = 255)
    @Column(name = "IMPORTER_GATE_NAME")
    private String importerGateName;
    @Column(name = "CHECK_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkTime;
    @Size(max = 255)
    @Column(name = "CHECK_PLACE")
    private String checkPlace;
    @Size(max = 12)
    @Column(name = "DEPT_CODE")
    private String deptCode;
    @Size(max = 255)
    @Column(name = "DEPT_NAME")
    private String deptName;
    @Column(name = "IS_DELETED")
    private Short isDeleted;

    public TestingImportedFood() {
    }

    public TestingImportedFood(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(Long nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Short getCheckMethod() {
        return checkMethod;
    }

    public void setCheckMethod(Short checkMethod) {
        this.checkMethod = checkMethod;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return goodsOwnerEmail;
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = goodsOwnerEmail;
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public String getResponsiblePersonAddress() {
        return responsiblePersonAddress;
    }

    public void setResponsiblePersonAddress(String responsiblePersonAddress) {
        this.responsiblePersonAddress = responsiblePersonAddress;
    }

    public String getResponsiblePersonPhone() {
        return responsiblePersonPhone;
    }

    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
        this.responsiblePersonPhone = responsiblePersonPhone;
    }

    public String getResponsiblePersonFax() {
        return responsiblePersonFax;
    }

    public void setResponsiblePersonFax(String responsiblePersonFax) {
        this.responsiblePersonFax = responsiblePersonFax;
    }

    public String getResponsiblePersonEmail() {
        return responsiblePersonEmail;
    }

    public void setResponsiblePersonEmail(String responsiblePersonEmail) {
        this.responsiblePersonEmail = responsiblePersonEmail;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterEmail() {
        return exporterEmail;
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = exporterEmail;
    }

    public Date getComingDate() {
        return comingDate;
    }

    public void setComingDate(Date comingDate) {
        this.comingDate = comingDate;
    }

    public String getExporterGateCode() {
        return exporterGateCode;
    }

    public void setExporterGateCode(String exporterGateCode) {
        this.exporterGateCode = exporterGateCode;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateCode() {
        return importerGateCode;
    }

    public void setImporterGateCode(String importerGateCode) {
        this.importerGateCode = importerGateCode;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Short getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Short isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileId != null ? fileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TestingImportedFood)) {
            return false;
        }
        TestingImportedFood other = (TestingImportedFood) object;
        if ((this.fileId == null && other.fileId != null) || (this.fileId != null && !this.fileId.equals(other.fileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.BO.test.TestingImportedFood[ fileId=" + fileId + " ]";
    }
    
}
