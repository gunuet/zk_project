/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.Pdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.viettel.newsignature.plugin.SignPdfFile;
import com.viettel.utils.Constants_CKS;
import com.viettel.utils.LogUtils;
import com.viettel.ws.PrintTextLocations;
import com.viettel.ws.findTexPos;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hieptq
 */
public class Pdf {

    private byte[] pdfFile;
    private float lx = 0;
    private float ly = 0;
    private float rx = 0;
    private float ry = 0;
    private int pageNumber;
    public String base64Hash;

    /**
     *
     * @param linkFileIn
     * @param linkFileOut
     * @param linkImage
     * @param typeSign
     * @throws IOException
     */
    public void insertImage(String linkFileIn, String linkFileOut, String linkImage, String typeSign) throws Exception {
        setPageNumber(searchLocationToSign(null, linkFileIn));
        if (getPageNumber() == -1) {
            return;
        }
        try {
            if (typeSign.equals(Constants_CKS.TYPE_USER.LD)) {
                addSignatureImage(linkFileIn, linkFileOut, linkImage, lx + 60, ly + 15, Constants_CKS.TYPE_USER.LD);
            }
            if (typeSign.equals(Constants_CKS.TYPE_USER.VT)) {
                addSignatureImage(linkFileIn, linkFileOut, linkImage, lx, ly, Constants_CKS.TYPE_USER.VT);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Chen anh chu ki vao cong van
     *
     * @param linkFileIn
     * @param linkFileOut
     * @param linkImageSign
     * @param linkImageStamp
     * @throws IOException
     */
    //Ký lib mới lệch 60_15
    public SignPdfFile createSignHash(String filePath, String temp, Certificate[] chain, String imageFile, String signName) throws Exception {
        this.pageNumber = searchLocationToSign(null, filePath);
        if (pageNumber == -1) {
            return null;
        }
        SignPdfFile pdfSig = new SignPdfFile();
        setBase64Hash(pdfSig.createHash(filePath, temp, chain, pageNumber, imageFile, new Float(lx).intValue()+60, new Float(ly).intValue()+15, 100, 100, signName));
        return pdfSig;
    }
    public SignPdfFile createHash(String filePath, String temp, Certificate[] chain, String imageFile, String signName) throws Exception {
        this.pageNumber = searchLocationToSign(null, filePath);
        if (pageNumber == -1) {
            return null;
        }
        SignPdfFile pdfSig = new SignPdfFile();
        setBase64Hash(pdfSig.createHash(filePath, temp, chain, pageNumber, imageFile, new Float(lx).intValue(), new Float(ly).intValue(), 100, 100, signName));
        return pdfSig;
    }
    /**
     * linhdx
     * 20160622
     * Chuc nang cho van thu bo dong dau
     * Do lanh dao bo ky so dung ham createHash nen khi dong dau phai dung ham createStampHash
     * de dau lenh voi chu ky 2/3
     * @param filePath
     * @param temp
     * @param chain
     * @param imageFile
     * @param signName
     * @return
     * @throws Exception 
     */
    public SignPdfFile createStampHash(String filePath, String temp, Certificate[] chain, String imageFile, String signName) throws Exception {
        this.pageNumber = searchLocationToSign(null, filePath);
        if (pageNumber == -1) {
            return null;
        }
        SignPdfFile pdfSig = new SignPdfFile();
        setBase64Hash(pdfSig.createHash(filePath, temp, chain, pageNumber, imageFile, new Float(lx).intValue()-60, new Float(ly).intValue()-15, 100, 100, signName));
        return pdfSig;
    }

    public SignPdfFile createHash(String filePath, String temp, Certificate[] chain, String imageFile, String imageFile2, String signName) throws Exception {
        this.pageNumber = searchLocationToSign(null, filePath);
        if (pageNumber == -1) {
            return null;
        }
        SignPdfFile pdfSig = new SignPdfFile();
        setBase64Hash(pdfSig.createHash(filePath, temp, chain, pageNumber, imageFile, imageFile2, new Float(lx).intValue(), new Float(ly).intValue(), 100, 100, signName));
        return pdfSig;
    }

    public String getBase64Hash() {
        return base64Hash;
    }

    public void setBase64Hash(String base64Hash) {
        this.base64Hash = base64Hash;
    }

    public void insertImageAll(String linkFileIn, String linkFileOut, String linkImageSign, String linkImageStamp, String sToFind) throws Exception {
        this.pageNumber = searchLocationToSign(sToFind, linkFileIn);
        if (pageNumber == -1) {
            return;
        }
        try {
            addSignatureImageAll(linkFileIn, linkFileOut, linkImageSign, linkImageStamp, lx, ly, pageNumber);
        } catch (DocumentException ex) {
            LogUtils.addLogDB(ex);
        }

    }

    /**
     * Chen anh chu ki vao cong van
     *
     * @param inputFile
     * @param outputFile
     * @param imageFile
     * @param x
     * @param y
     * @param type
     * @throws IOException
     * @throws DocumentException
     */
    public void addSignatureImage(String inputFile, String outputFile, String imageFile, float x, float y, String type)
            throws IOException, DocumentException {
        PdfReader reader = new PdfReader(inputFile);
        FileOutputStream os = new FileOutputStream(outputFile);
        PdfStamper stamper = new PdfStamper(reader, os);
        PdfContentByte pdfContentByte = stamper.getOverContent(1);
        Image image = Image.getInstance(imageFile);
        image.setAbsolutePosition(x, y);
        if (type.equals(Constants_CKS.TYPE_USER.LD)) {
            image.scaleToFit(100, 100);
        }
        if (type.equals(Constants_CKS.TYPE_USER.VT)) {
            image.scaleToFit(100, 100);
        }
        pdfContentByte.addImage(image);
        stamper.close();
        os.close();
    }

    public void addSignatureImageAll(String inputFile, String outputFile, String imageFileSign, String imageFileStamp, float x, float y, int pageNum)
            throws IOException, DocumentException {
        PdfReader reader = new PdfReader(inputFile);
        FileOutputStream os = new FileOutputStream(outputFile);
        PdfStamper stamper = new PdfStamper(reader, os);
        if (pageNum > 0) {
            PdfContentByte pdfContentByte = stamper.getOverContent(pageNum);

            if (imageFileSign != null) {
                Image imageSign = Image.getInstance(imageFileSign);
                imageSign.setAbsolutePosition(x + 60, y + 15);
                imageSign.scaleToFit(100, 100);
                pdfContentByte.addImage(imageSign);
            }

            if (imageFileStamp != null) {
                Image imageStamp = Image.getInstance(imageFileStamp);
                imageStamp.setAbsolutePosition(x, y);
                imageStamp.scaleToFit(100, 100);
                pdfContentByte.addImage(imageStamp);
            }
        }
        stamper.close();
        os.close();
    }

    public byte[] read(File file) throws IOException {
        ByteArrayOutputStream ous;
        InputStream ios;
        byte[] value;
        try {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            ios = new FileInputStream(file);
            int read;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
            value = ous.toByteArray();
            ous.close();
            ios.close();
            
        } finally {
        }
        return value;

    }

    public int searchLocationToSign(String sToFind, String path) throws Exception {
        float[] rs = PrintTextLocations.getpos(sToFind, path); 
        if (rs == null) {
            rs = findTexPos.getpos(sToFind, path);
            if (rs != null) {
                lx = rs[0];
                ly = rs[1];
                return new Float(rs[2]).intValue();
            } else {
                return -1;
            }
        } else {
            lx = rs[0];
            ly = rs[1];
            return new Float(rs[2]).intValue();
        }
    }

    /**
     * @return the pageNumber
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * @param pageNumber the pageNumber to set
     */
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
}
