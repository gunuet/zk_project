/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.Pdf;

import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.utils.LogUtils;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author duv
 */
public class XMLConverter {
    public static void main(String[] args) {
//        String output = "D:/cosfile.xml";
//        VFileCosfile cosfile = new VFileCosfile();
//        XMLConverter converter = new XMLConverter(cosfile, output);
    }
    
    public XMLConverter(VFileCosfile cosFile, String output) {
        try {
            File file = new File(output);
            JAXBContext jaxbContext = JAXBContext.newInstance(cosFile.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(cosFile, file);
            jaxbMarshaller.marshal(cosFile, System.out);

        } catch (JAXBException ex) {
            LogUtils.addLogDB(ex);
        }
    }
}
