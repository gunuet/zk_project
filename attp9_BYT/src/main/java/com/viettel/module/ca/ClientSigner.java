/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.ca;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.utils.LogUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author duv
 */
public class ClientSigner extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    private Textbox txtFlowId, txtDocId, txtDocType;
    @Wire
    private Textbox txtPrevId, txtNextId, txtDeptId;
    @Wire
    private Button btnCreate, btnOK;
    @Wire
    private Textbox txtStatus;

    @Wire
    Window caIntegrationForm;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        // get fileId from passing parameters
//        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
    }

    @Listen("onUpload = #caIntegrationForm")
    public void onUpload(Event event) {

        //Media signedFile = event.getMedia();
        //String extFile = signedFile.getFormat();
        String data = event.getData().toString();
        String filePath = "C:/";
        String fileName = "signed_" + (new Date()).toString() + ".pdf";

        File newFile = new File(filePath, fileName);

        try {
            if (!newFile.exists()) {
                FileOutputStream fos = new FileOutputStream(newFile);
                fos.write(Base64.decode(data));
                fos.flush();
                fos.close();
            }
        } catch (Base64DecodingException | IOException ex) {
            LogUtils.addLogDB(ex);
        }
    }

}
