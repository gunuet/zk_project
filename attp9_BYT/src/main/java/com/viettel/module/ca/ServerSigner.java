/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.ca;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.LogUtils;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author duv
 */
public class ServerSigner extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    Window caIntegrationForm;

    @Wire
    Textbox txtCertSerial, txtBase64Hash;

    private String fileToSign;
    //private String fileSigned;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fileToSign = "C:/bancongbomypham.pdf";
    }

    @Listen("onUploadCert = #caIntegrationForm")
    public void onUploadCert(Event event) throws Exception {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        SignPdfFile pdfSig = new SignPdfFile();
        //pdfSig = new SignPdfFile();
        String base64Hash;
        String filePath = "C:/";
        String outputFileFinalName = "_" + (new Date()).getTime() + ".pdf";
        String outPutFileFinal = filePath + outputFileFinalName;
        String linkImageSign = "C:/232.png";
        String linkImageStamp = "C:/attpStamp.png";
        Pdf pdfProcess = new Pdf();
        try {
            pdfProcess.insertImageAll(fileToSign, outPutFileFinal, linkImageSign, linkImageStamp, null);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }
//        if (pdfProcess == null) {
//            showNotification("Ký số không thành công");
//            LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
//            return;
//        }

        base64Hash = pdfSig.createHash(outPutFileFinal, new Certificate[]{x509Cert});
        String certSerial = x509Cert.getSerialNumber().toString(16);
        Session session = Sessions.getCurrent();
        session.setAttribute("certSerial", certSerial);
        session.setAttribute("base64Hash", base64Hash);
        txtBase64Hash.setValue(base64Hash);
        txtCertSerial.setValue(certSerial);
        session.setAttribute("PDFSignature", pdfSig);
        Clients.evalJavaScript("signAndSubmit();");
        LogUtils.addLog("Upload certificate sucessfully, file to sign: " + outPutFileFinal);
    }

    @Listen("onSign = #caIntegrationForm")
    public void onSign(Event event) {
        String data = event.getData().toString();
        String filePath = "C:/";
        String outputFileName = "signed_" + (new Date()).getTime() + ".pdf";
        String outputFile = filePath + outputFileName;

        String signature = data;
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");

        try {
            pdfSig.insertSignature(signature, outputFile);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        LogUtils.addLog("Signed file: " + outputFile);
    }

}
