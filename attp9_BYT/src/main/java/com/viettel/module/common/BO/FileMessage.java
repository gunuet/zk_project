/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.common.BO;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author
 */
@Entity
@Table(name = "FILE_MESSAGE")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "FileMessage.findAll", query = "SELECT f FROM FileMessage f"),
		@NamedQuery(name = "FileMessage.findByFileId", query = "SELECT f FROM FileMessage f WHERE f.fileId = :fileId"),
		@NamedQuery(name = "FileMessage.findByMessage", query = "SELECT f FROM FileMessage f WHERE f.message = :message"),
		@NamedQuery(name = "FileMessage.findByCreateDate", query = "SELECT f FROM FileMessage f WHERE f.createDate = :createDate") })
public class FileMessage implements Serializable {
	private static long serialVersionUID = 1L;

	/**
	 * @return the serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	/**
	 * @param aSerialVersionUID
	 *            the serialVersionUID to set
	 */
	public static void setSerialVersionUID(long aSerialVersionUID) {
		serialVersionUID = aSerialVersionUID;
	}

	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@SequenceGenerator(name = "FILE_MESSAGE_SEQ", sequenceName = "FILE_MESSAGE_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILE_MESSAGE_SEQ")
	@Column(name = "FILE_MESSAGE_ID")
	private Long fileMessageId;

	@Column(name = "FILE_ID")
	private Long fileId;

	@Column(name = "MESSAGE")
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "CREATE_DATE")
        @Temporal(javax.persistence.TemporalType.DATE)
	private Date createDate;

	@Column(name = "MODIFY_DATE")
        @Temporal(javax.persistence.TemporalType.DATE)
	private Date modifyDate;

	@Column(name = "CREATE_DEPT_ID")
	private Long createDeptId;

	@Column(name = "CREATE_DEPT_NAME")
	private String createDeptName;

	@Column(name = "CREATOR_ID")
	private Long creatorId;

	@Column(name = "CREATOR_NAME")
	private String creatorName;

	@Column(name = "IS_ACTIVE")
	private Long isActive;

	public FileMessage() {
	}

	public Long getFileMessageId() {
		return this.fileMessageId;
	}

	public void setFileMessageId(Long fileMessageId) {
		this.fileMessageId = fileMessageId;
	}

//	public Timestamp getCreateDate() {
//		return this.createDate;
//	}
//
//	public void setCreateDate(Timestamp createDate) {
//		this.createDate = createDate;
//	}
	public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
	

	public Long getCreateDeptId() {
		return this.createDeptId;
	}

	public void setCreateDeptId(Long createDeptId) {
		this.createDeptId = createDeptId;
	}

	public String getCreateDeptName() {
		return this.createDeptName;
	}

	public void setCreateDeptName(String createDeptName) {
		this.createDeptName = createDeptName;
	}

	public Long getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreatorName() {
		return this.creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public Long getFileId() {
		return this.fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

//	public Timestamp getModifyDate() {
//		return this.modifyDate;
//	}
//
//	public void setModifyDate(Timestamp modifyDate) {
//		this.modifyDate = modifyDate;
//	}
	public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}
