/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.common.BO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Van Trung
 */
@Entity
@Table(name = "V_FILE_DEPT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VFileDept.findAll", query = "SELECT v FROM VFileDept v"),
    @NamedQuery(name = "VFileDept.findByFileId", query = "SELECT v FROM VFileDept v WHERE v.fileId = :fileId"),
    @NamedQuery(name = "VFileDept.findByFileType", query = "SELECT v FROM VFileDept v WHERE v.fileType = :fileType"),
    @NamedQuery(name = "VFileDept.findByFileTypeName", query = "SELECT v FROM VFileDept v WHERE v.fileTypeName = :fileTypeName"),
    @NamedQuery(name = "VFileDept.findByFileCode", query = "SELECT v FROM VFileDept v WHERE v.fileCode = :fileCode"),
    @NamedQuery(name = "VFileDept.findByFileName", query = "SELECT v FROM VFileDept v WHERE v.fileName = :fileName"),
    @NamedQuery(name = "VFileDept.findByStatus", query = "SELECT v FROM VFileDept v WHERE v.status = :status"),
    @NamedQuery(name = "VFileDept.findByTaxCode", query = "SELECT v FROM VFileDept v WHERE v.taxCode = :taxCode"),
    @NamedQuery(name = "VFileDept.findByBusinessId", query = "SELECT v FROM VFileDept v WHERE v.businessId = :businessId"),
    @NamedQuery(name = "VFileDept.findByBusinessName", query = "SELECT v FROM VFileDept v WHERE v.businessName = :businessName"),
    @NamedQuery(name = "VFileDept.findByBusinessAddress", query = "SELECT v FROM VFileDept v WHERE v.businessAddress = :businessAddress"),
    @NamedQuery(name = "VFileDept.findByBusinessPhone", query = "SELECT v FROM VFileDept v WHERE v.businessPhone = :businessPhone"),
    @NamedQuery(name = "VFileDept.findByBusinessFax", query = "SELECT v FROM VFileDept v WHERE v.businessFax = :businessFax"),
    @NamedQuery(name = "VFileDept.findByCreateDate", query = "SELECT v FROM VFileDept v WHERE v.createDate = :createDate"),
    @NamedQuery(name = "VFileDept.findByModifyDate", query = "SELECT v FROM VFileDept v WHERE v.modifyDate = :modifyDate"),
    @NamedQuery(name = "VFileDept.findByCreatorId", query = "SELECT v FROM VFileDept v WHERE v.creatorId = :creatorId"),
    @NamedQuery(name = "VFileDept.findByCreatorName", query = "SELECT v FROM VFileDept v WHERE v.creatorName = :creatorName"),
    @NamedQuery(name = "VFileDept.findByCreateDeptId", query = "SELECT v FROM VFileDept v WHERE v.createDeptId = :createDeptId"),
    @NamedQuery(name = "VFileDept.findByCreateDeptName", query = "SELECT v FROM VFileDept v WHERE v.createDeptName = :createDeptName"),
    @NamedQuery(name = "VFileDept.findByIsActive", query = "SELECT v FROM VFileDept v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VFileDept.findByIsTemp", query = "SELECT v FROM VFileDept v WHERE v.isTemp = :isTemp"),
    @NamedQuery(name = "VFileDept.findByParentFileId", query = "SELECT v FROM VFileDept v WHERE v.parentFileId = :parentFileId"),
    @NamedQuery(name = "VFileDept.findByFlowId", query = "SELECT v FROM VFileDept v WHERE v.flowId = :flowId"),
    @NamedQuery(name = "VFileDept.findByStartDate", query = "SELECT v FROM VFileDept v WHERE v.startDate = :startDate"),
    @NamedQuery(name = "VFileDept.findByNumDayProcess", query = "SELECT v FROM VFileDept v WHERE v.numDayProcess = :numDayProcess"),
    @NamedQuery(name = "VFileDept.findByDeadline", query = "SELECT v FROM VFileDept v WHERE v.deadline = :deadline"),
    @NamedQuery(name = "VFileDept.findByFlagView", query = "SELECT v FROM VFileDept v WHERE v.flagView = :flagView"),
    @NamedQuery(name = "VFileDept.findByBusinessEmail", query = "SELECT v FROM VFileDept v WHERE v.businessEmail = :businessEmail"),
    @NamedQuery(name = "VFileDept.findByNswFileCode", query = "SELECT v FROM VFileDept v WHERE v.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "VFileDept.findByDeptTestingId", query = "SELECT v FROM VFileDept v WHERE v.deptTestingId = :deptTestingId"),
    @NamedQuery(name = "VFileDept.findByStatusName", query = "SELECT v FROM VFileDept v WHERE v.statusName = :statusName"),
    @NamedQuery(name = "VFileDept.findByFileDate", query = "SELECT v FROM VFileDept v WHERE v.fileDate = :fileDate"),
    @NamedQuery(name = "VFileDept.findByDocumentTypeCode", query = "SELECT v FROM VFileDept v WHERE v.documentTypeCode = :documentTypeCode")})
public class VFileDept implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Size(max = 255)
    @Column(name = "FILE_TYPE_NAME")
    private String fileTypeName;
    @Size(max = 31)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 255)
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "STATUS")
    private Long status;
    @Size(max = 31)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Column(name = "BUSINESS_ID")
    private Long businessId;
    @Size(max = 255)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 500)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 31)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 31)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 255)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 255)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "IS_TEMP")
    private Short isTemp;
    @Column(name = "PARENT_FILE_ID")
    private Long parentFileId;
    @Column(name = "FLOW_ID")
    private Long flowId;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "NUM_DAY_PROCESS")
    private Long numDayProcess;
    @Column(name = "DEADLINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;
    @Column(name = "FLAG_VIEW")
    private Long flagView;
    @Size(max = 250)
    @Column(name = "BUSINESS_EMAIL")
    private String businessEmail;
    @Size(max = 100)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "DEPT_TESTING_ID")
    private Long deptTestingId;
    @Size(max = 200)
    @Column(name = "STATUS_NAME")
    private String statusName;
    @Size(max = 10)
    @Column(name = "FILE_DATE")
    private String fileDate;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;

    public VFileDept() {
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Short getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(Short isTemp) {
        this.isTemp = isTemp;
    }

    public Long getParentFileId() {
        return parentFileId;
    }

    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Long getNumDayProcess() {
        return numDayProcess;
    }

    public void setNumDayProcess(Long numDayProcess) {
        this.numDayProcess = numDayProcess;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Long getFlagView() {
        return flagView;
    }

    public void setFlagView(Long flagView) {
        this.flagView = flagView;
    }

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getDeptTestingId() {
        return deptTestingId;
    }

    public void setDeptTestingId(Long deptTestingId) {
        this.deptTestingId = deptTestingId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getFileDate() {
        return fileDate;
    }

    public void setFileDate(String fileDate) {
        this.fileDate = fileDate;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }
    
}
