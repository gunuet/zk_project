package com.viettel.module.common.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Files;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

/**
 *
 * @author THANHDV
 */
public class ChoosePeopleController extends BaseComposer {

    private static final Object UserId = null;
    //
    // @Wire
    // Textbox tbId, tbName, tbDescription;
    @Wire
    Datebox dbTimeDate;
    @Wire
    Listbox lbDayType;
    @Wire
    Window createUpdate;
    @Wire
    Textbox tbMessage, tbcreatorname, tbrtdeptcreator, tbId, tbIdMessage,
            tbCreatorName, tbCreatorDeptName, tbCreateDate;
    @Wire
    // đây là các biến chung
    private String crudMode;
    private Files files;
    private Long fileId;
    private Long id;
    List<Category> itemsCheckedList = new ArrayList();
    @Wire
    Listbox lbPeopleChoose;
    
    private Window parentWindow;
    @Wire
    Window selectUserDlg;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Execution execution = Executions.getCurrent();
        setParentWindow((Window) execution.getArg().get("parentWindow"));
        CategoryDAOHE catDaoHe = new CategoryDAOHE();
        List<Category> lst = catDaoHe.findAllCategorySearch(Constants.CATEGORY_TYPE.IMPORT_FILE_HOIDONG, false);
        ListModelArray lstModel = new ListModelArray(lst);
        lstModel.setMultiple(true);
        lbPeopleChoose.setModel(lstModel);

    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Listen("onClick=#btnSave")
    public void onSave() throws IOException, Exception {
        Set<Listitem> liselect = lbPeopleChoose.getSelectedItems();
        for (Listitem item : liselect) {
            itemsCheckedList.add((Category) item.getValue());
        }
        selectionSort(itemsCheckedList);
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("lstPeopleChoose", itemsCheckedList);
        Events.sendEvent(new Event("onSelectedPeople", parentWindow, arguments));
        selectUserDlg.detach();
    }
    
      private static void selectionSort(List<Category> unsortedArray)
            throws Exception {
        int min;
        boolean ismin;
        int size = unsortedArray.size();
        //Neu null thi set = 10000 de so sanh phai duoi
        for (int i = 0; i < size; i++) {
            Category cat = unsortedArray.get(i);
            if(cat.getSortOrder() == null){
                cat.setSortOrder(10000L);
            }
        } 
        for (int i = 0; i < size; i++) {
            min = i;
            ismin = false;
            for (int j = i + 1; j < size; j++) {
                if (unsortedArray.get(j).getSortOrder().compareTo(unsortedArray.get(min).getSortOrder()) < 0) {
                    min = j;
                    ismin = true;
                }
            }
            if (ismin == true) {
                Category temp = unsortedArray.get(i);
                unsortedArray.set(i, unsortedArray.get(min));
                unsortedArray.set(min, temp);
            }
        }
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }
    
    
}
