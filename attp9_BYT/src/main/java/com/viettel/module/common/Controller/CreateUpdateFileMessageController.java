package com.viettel.module.common.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.common.DAO.FileMessageDAO;
import com.viettel.module.common.BO.FileMessage;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class CreateUpdateFileMessageController extends BaseComposer {
	private static final Object UserId = null;
	//
	// @Wire
	// Textbox tbId, tbName, tbDescription;
	@Wire
	Datebox dbTimeDate;
	@Wire
	Listbox lbDayType;
	@Wire
	Window createUpdate;
	@Wire
	Textbox tbMessage, tbcreatorname, tbrtdeptcreator, tbId, tbIdMessage,
			tbCreatorName, tbCreatorDeptName, tbCreateDate;
	@Wire
	// đây là các biến chung
	private String crudMode;
	private Files files;
	private Long fileId;
	private Long id;

	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
		crudMode = (String) arguments.get("CRUDMode");
		switch (crudMode) {
		case "CREATE":
			fileId = (Long) arguments.get("fileId");
			break;
		case "UPDATE":
			loadInfoToForm();
			break;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Listen("onClick=#btnSave")
	public void onSave() throws IOException {
		// Map<String, Object> arguments = (Map)
		// Executions.getCurrent().getArg();
		// crudMode = (String) arguments.get("CRUDMode");
		switch (crudMode) {
		case "CREATE":
			FileMessage b = new FileMessage();
			if (tbMessage.getValue() != null
					&& tbMessage.getValue().trim().length() == 0) {
				showNotification("Phải nhập nội dung tin nhắn ",
						Constants.Notification.ERROR);
				tbMessage.focus();
				return;
			} else {
				b.setMessage(tbMessage.getValue());
			}
			b.setFileId(fileId);
			b.setCreatorName(getUserFullName());
			b.setCreateDeptName(getDeptName());
			b.setCreatorId(getUserId());
			b.setCreateDeptId(getDeptId());
			b.setIsActive(1L);
			Date dateNow = new Date();
			b.setCreateDate(dateNow);

			FileMessageDAO fileMessageDAO1 = new FileMessageDAO();
			fileMessageDAO1.saveOrUpdate(b);
			if (tbId.getValue() != null && !tbId.getValue().isEmpty()) {
				// update thì detach window
				createUpdate.detach();
			} else {
				// them moi thi clear window
				loadInfoToForm();
			}
			showNotification("Lưu thành công", Constants.Notification.INFO);

			Window parentWnd1 = (Window) Path.getComponent("/windowView");
                        if(parentWnd1 == null){
                            parentWnd1 = (Window) Path.getComponent("/windowViewGiahan");
                        }
			Events.sendEvent(new Event("onReload", parentWnd1, null));
			break;

		case "UPDATE":
			FileMessage a;
			FileMessageDAO fileMessageDAO = new FileMessageDAO();

			a = fileMessageDAO.findById(id);

			if (tbIdMessage.getValue() != null
					&& !tbIdMessage.getValue().isEmpty()) {
				a.setFileMessageId(Long.parseLong(tbIdMessage.getValue()));
			}
			if (tbId.getValue() != null && !tbId.getValue().isEmpty()) {
				a.setFileId(Long.parseLong(tbId.getValue()));
			}
			if (tbMessage.getValue() != null
					&& tbMessage.getValue().trim().length() == 0) {
				showNotification("Phải nhập nội dung tin nhắn ",
						Constants.Notification.ERROR);
				tbMessage.focus();
				return;
			} else {
				a.setMessage(tbMessage.getValue());
			}
			Date dateNow1 = new Date();
			a.setModifyDate(dateNow1);

			fileMessageDAO.saveOrUpdate(a);
			if (tbId.getValue() != null && !tbId.getValue().isEmpty()) {
				// update thì detach window
				createUpdate.detach();
			} else {
				// them moi thi clear window
				loadInfoToForm();
			}
			showNotification("Lưu thành công", Constants.Notification.INFO);

			Window parentWnd = (Window) Path.getComponent("/windowView");
                         if(parentWnd == null){
                            parentWnd = (Window) Path.getComponent("/windowViewGiahan");
                        }
			Events.sendEvent(new Event("onReload", parentWnd, null));
			break;
		}

	}

	public void loadInfoToForm() {
		id = (Long) Executions.getCurrent().getArg().get("id");
		if (id != null) {
			FileMessageDAO objhe = new FileMessageDAO();
			FileMessage rs = objhe.findById(id);
			// if (rs.getFileMessageId() != null) {
			// tbIdMessage.setValue(rs.getFileMessageId().toString());
			// }
			if (rs.getMessage() != null) {
				tbMessage.setValue(rs.getMessage().toString());
			}
		} else {
			tbMessage.setValue("");
		}
	}

	@Override
	public String getConfigLanguageFile() {
		return "language_COSMETIC_vi";
	}
}
