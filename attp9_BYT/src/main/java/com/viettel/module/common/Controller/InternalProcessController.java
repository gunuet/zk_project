package com.viettel.module.common.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.InternalProcessing;
import com.viettel.module.importDevice.DAO.InternalProcessingDAO;
import com.viettel.utils.Constants;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class InternalProcessController extends BaseComposer {

    @Wire
    Datebox dbdateInternal;
    @Wire
    Listbox lbListInternalProcess;
    @Wire
    Textbox tbContent;
    @Wire
    Paging pagingBottomInternalProcess;
    @Wire
    protected Window windowInternalprocess;
    Long meetingId;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        meetingId = (Long) arguments.get("meetingId");
        fillDataToList();
    }

    private void fillDataToList() {
        int take = pagingBottomInternalProcess.getPageSize();
        int start = pagingBottomInternalProcess.getActivePage() * pagingBottomInternalProcess.getPageSize();
        InternalProcessingDAO idDAO = new InternalProcessingDAO();
        PagingListModel lstInternalProcessing = idDAO.getInternalProcessList(start, take, tbContent.getValue(), dbdateInternal.getValue(), meetingId);
        if (lstInternalProcessing != null) {
            pagingBottomInternalProcess.setTotalSize(lstInternalProcessing.getCount());
            if (lstInternalProcessing.getCount() == 0) {
                pagingBottomInternalProcess.setVisible(false);
            } else {
                pagingBottomInternalProcess.setVisible(true);
            }
            ListModelArray lstRlst = new ListModelArray(lstInternalProcessing.getLstReturn());
            lbListInternalProcess.setModel(lstRlst);
        }
    }

    @Listen("onPaging = #pagingBottomInternalProcess")
    public void onPagingList(Event event) {
        fillDataToList();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Listen("onClick=#btnSearchIntenal")
    public void onSearch() throws IOException {
        fillDataToList();
    }

    @Listen("onClick = #btnCreateInternalProcess")
    public void onOpenCreateInternalProcess(Event event) {
        Map args = new ConcurrentHashMap();
        args.put("meetingId", meetingId);
        args.put("internalProcessId", 0L);
        args.put("parentWindow", windowInternalprocess);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/internalProcess_popup_create.zul", null, args);
        window.doModal();
    }

    @Listen("onEdit = #lbListInternalProcess")
    public void onEdit(Event event) throws Exception {
        Map args = new ConcurrentHashMap();
        args.put("meetingId", meetingId);
        Long internalProcessId = Long.parseLong(event.getData().toString());
        args.put("internalProcessId", internalProcessId);
        args.put("parentWindow", windowInternalprocess);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/internalProcess_popup_create.zul", null, args);
        window.doModal();
    }

    @Listen("onDelete = #lbListInternalProcess")
    public void onDelete(Event event) throws Exception {
        final Long internalProcessId = Long.parseLong(event.getData().toString());
        EventListener eventListener = new EventListener() {
            @Override
            public void onEvent(Event evt) throws InterruptedException {
                if (Messagebox.ON_OK.equals(evt.getName())) {
                    InternalProcessingDAO idDAO = new InternalProcessingDAO();
                    InternalProcessing inter = idDAO.findInternalProcessById(internalProcessId);
                    inter.setIsActive(0L);
                    idDAO.saveOrUpdate(inter);
                    fillDataToList();
                }
            }
        };
        Messagebox.show("Bạn có đồng ý xoá bản ghi này?", "Thông báo",
                Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                eventListener);

    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    @Listen("onRefreshList = #windowInternalprocess")
    public void onRefreshList(Event event) {
        showNotification("Thao tác thành công!", Constants.Notification.INFO);
        fillDataToList();
    }
}
