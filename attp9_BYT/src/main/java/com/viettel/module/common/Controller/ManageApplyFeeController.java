package com.viettel.module.common.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.AppliesFee;
import com.viettel.module.importDevice.DAO.AppliesFeeDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class ManageApplyFeeController extends BaseComposer {

    @Wire
    Datebox dbdateInternal;
    @Wire
    Listbox lbListManageFee, lbAction;
    @Wire
    Textbox tbFeeType;
    @Wire
    Decimalbox tbAmountFee;
    @Wire
    Paging pagingBottomFee;
    @Wire
    protected Window windowApplyFee;
    Long importDeviceFileId;
    protected Window windowParent;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        importDeviceFileId = (Long) arguments.get("importDeviceFileId");
        windowParent = (Window) arguments.get("windowParent");
        fillDataToList();
    }

    private void fillDataToList() {
        try {
            int take = pagingBottomFee.getPageSize();
            int start = pagingBottomFee.getActivePage() * pagingBottomFee.getPageSize();
            AppliesFeeDAO idDAO = new AppliesFeeDAO();
            Long isActiveSelected = null;
            if (lbAction.getSelectedItem() != null && lbAction.getSelectedItem().getValue() != null && !"".equals(lbAction.getSelectedItem().getValue().toString())) {
                isActiveSelected = Long.parseLong(lbAction.getSelectedItem().getValue().toString());
            }
            String amountValue = null;
            if (tbAmountFee.getValue() != null) {
                amountValue = tbAmountFee.getValue().toString();
            }
            PagingListModel lstAppliesFee = idDAO.getAppliesFeeList(start, take, isActiveSelected, tbFeeType.getValue(), amountValue, importDeviceFileId);
            if (lstAppliesFee != null) {
                pagingBottomFee.setTotalSize(lstAppliesFee.getCount());
                if (lstAppliesFee.getCount() == 0) {
                    pagingBottomFee.setVisible(false);
                } else {
                    pagingBottomFee.setVisible(true);
                }
                ListModelArray lstRlst = new ListModelArray(lstAppliesFee.getLstReturn());
                lbListManageFee.setModel(lstRlst);
            }
        } catch (NumberFormatException | WrongValueException ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onPaging = #pagingBottomFee")
    public void onPagingList(Event event) {
        fillDataToList();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Listen("onClick = #btnSearchAppliesFee")
    public void onSearch() throws IOException {pagingBottomFee.setActivePage(0);
        fillDataToList();
    }

    @Listen("onClick = #btnCreateAppliesFee")
    public void onOpenCreateAppliesFee(Event event) {
        Map args = new ConcurrentHashMap();
        args.put("importDeviceFileId", importDeviceFileId);
        args.put("id", 0L);
        args.put("parentWindow", windowApplyFee);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/appliesFee_popup_create.zul", null, args);
        window.doModal();
    }

    @Listen("onEdit = #lbListManageFee")
    public void onEdit(Event event) throws Exception {
        Map args = new ConcurrentHashMap();
        args.put("importDeviceFileId", importDeviceFileId);
        Long id = Long.parseLong(event.getData().toString());
        args.put("id", id);
        args.put("parentWindow", windowApplyFee);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/appliesFee_popup_create.zul", null, args);
        window.doModal();
    }

    @Listen("onDelete = #lbListManageFee")
    public void onDelete(Event event) throws Exception {
        final Long appliesFeeId = Long.parseLong(event.getData().toString());
        EventListener eventListener = new EventListener() {
            @Override
            public void onEvent(Event evt) throws InterruptedException {
                if (Messagebox.ON_OK.equals(evt.getName())) {
                    AppliesFeeDAO idDAO = new AppliesFeeDAO();
                    AppliesFee inter = idDAO.findAppliesFeeById(appliesFeeId);
                    inter.setIsDeleted(0L);
                    idDAO.saveOrUpdate(inter);
                    fillDataToList();
                }
            }
        };
        Messagebox.show("Bạn có đồng ý xoá bản ghi này?", "Thông báo",
                Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                eventListener);

    }
    
    @Listen("onApply = #lbListManageFee")
    public void onDestroyOrApply(Event event){
        try{
            Long appliesFeeId = Long.parseLong(event.getData().toString());
            AppliesFeeDAO idDAO = new AppliesFeeDAO();
            AppliesFee inter = idDAO.findAppliesFeeById(appliesFeeId);
            if(inter.getIs_Active() == 1L){
                inter.setIs_Active(0L);
            }else{
                inter.setIs_Active(1L);
            }            
            idDAO.saveOrUpdate(inter);            
            showNotification("Thao tác thành công!", Constants.Notification.INFO);
            fillDataToList();
        }catch(Exception ex){
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onClick = #btnAddToText")
    public void onAddToText(Event event) {
        //linhdx 20160301 dong cua so view
        try {
//            Window windowview = (Window) Path.getComponent("/" + windowParent);
            if (windowParent != null) {
                AppliesFee appliesFeeHadChoose = (AppliesFee) lbListManageFee.getSelectedItem().getValue();
                if (appliesFeeHadChoose != null && appliesFeeHadChoose.getIs_Active() == 1) {
                    Events.sendEvent("onAddText", windowParent, appliesFeeHadChoose.getAmountFee());
                } else {
                    showNotification("Loại phí đang ở trạng thái Không hoạt động! Yêu cầu chuyển trạng thái trước khi áp phí!", Constants.Notification.WARNING);
                }
            }
            windowApplyFee.detach();
        } catch (Exception ex) {
            LogUtils.addLog(ex);
        }
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    @Listen("onRefreshList = #windowApplyFee")
    public void onRefreshList(Event event) {
        showNotification("Thao tác thành công!", Constants.Notification.INFO);
        fillDataToList();
    }
}
