package com.viettel.module.common.Controller;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.exporter.excel.ExcelExporter;
import org.zkoss.exporter.pdf.PdfExporter;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.common.BO.VFileDept;
import com.viettel.module.common.DAO.VFileDeptDAO;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.utils.Constants;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.model.SearchModel;

/**
 *
 * @author
 */
public class SearchFileController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #dbFromDayModify")
    private Datebox dbFromDayModify;
    @Wire("#incSearchFullForm #dbToDayModify")
    private Datebox dbToDayModify;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #lboxStatus")
    private Listbox lboxStatus;
    @Wire("#incSearchFullForm #tbCosmeticCompanyName")
    private Textbox tbCosmeticCompanyName;
    @Wire("#incSearchFullForm #tbCosmeticCompanyAddress")
    private Textbox tbCosmeticCompanyAddress;
    @Wire("#incSearchFullForm #tbCosmeticDistributePersonName")
    private Textbox tbCosmeticDistributePersonName;
    @Wire("#incSearchFullForm #tbBusinessTaxCode")
    private Textbox tbBusinessTaxCode;
    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    private Window importOrderAll;
    private CosmeticFileModel lastSearchModel;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;;
    private Long DEPT_ID;
    Long UserId = getUserId();
    CategoryDAOHE dao = new CategoryDAOHE();
    // Long RoleId = Long.parseLong(dao.LayTruong("RoleUserDept", "userId",
    // UserId.toString(), "roleId"));
    private Users user;
    private String sPositionType;
    private Constants constants = new Constants();
    String sBoSung = Constants.PROCESS_STATUS.SENT_RETURN.toString();
    String statusStr;
    // list cac checkbox đã check trong list
    List<VFileDept> itemsCheckedList, lstCosmetic;
    // list cac checkbox đã check trong 1 trang
    List<Listitem> itemsChecksedPage;
    @Wire("#incSearchFullForm #lbxDepartment")
    Listbox lbxDepartment;

    @Wire("#incSearchFullForm #lboxDocumentTypeCode")
    Listbox lboxDocumentTypeCode;

    // luong
    private VFileImportOrder vFileCosfile;
    private Files files;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        itemsCheckedList = new ArrayList<VFileDept>();
        lastSearchModel = new CosmeticFileModel();
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        FILE_TYPE_STR = (String) arguments.get("filetype");
        String deptIdStr = (String) arguments.get("deptid");
        String menuTypeStr = (String) arguments.get("menuType");
        sPositionType = (String) arguments.get("PositionType");
        statusStr = (String) arguments.get("status");
        Long statusL = null;
        try {
            if (statusStr != null) {
                statusL = Long.parseLong(statusStr);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        if (statusL != null) {
            lastSearchModel.setStatus(statusL);
        }

        lastSearchModel.setMenuTypeStr(menuTypeStr);
        try {
            DEPT_ID = Long.parseLong(deptIdStr);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            DEPT_ID = -1l;
        }
        FILE_TYPE = WorkflowAPI.getInstance().getProcedureTypeIdByCode(
                FILE_TYPE_STR);

        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);
        CategoryDAOHE cdhe = new CategoryDAOHE();
        VFileDeptDAO objDAOHE = new VFileDeptDAO();
        List lstStatusAll = cdhe.getSelectCategoryByType(
                Constants.CATEGORY_TYPE.FILE_STATUS, "name");
        List lstStatus;

        lastSearchModel.setFileType(FILE_TYPE);

        lstStatus = objDAOHE.findListStatus(lastSearchModel, getUserId(),
                getDeptId());
        dbFromDay.setValue(null);
        lastSearchModel
                .setCreateDateFrom(null);
        dbToDay.setValue(new Date());
        lastSearchModel.setCreateDateTo(new Date());

        int countlstStatusAll = lstStatusAll.size();
        int countlstStatus = lstStatus.size();
        boolean check;
        for (int i = countlstStatusAll - 1; i > 0; i--) {
            check = false;
            Object obj = lstStatusAll.get(i);
            String value = "";
            try {
                Field field = obj.getClass().getDeclaredField("value");
                field.setAccessible(true);
                value = String.valueOf(field.get(obj));
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
            for (int j = 0; j < countlstStatus; j++) {
                if (value.equals(lstStatus.get(j).toString())) {
                    check = true;
                    break;
                }
            }
            if (check == false) {
                lstStatusAll.remove(i);
            }
        }
        // truong hop danh sach bo sung
        countlstStatus = lstStatusAll.size();
        if (statusStr != null) {
            if (statusStr.equals(sBoSung)) {
                for (int i = countlstStatus - 1; i > 0; i--) {
                    Object obj = lstStatusAll.get(i);
                    String value = "";
                    try {
                        Field field = obj.getClass().getDeclaredField("value");
                        field.setAccessible(true);
                        value = String.valueOf(field.get(obj));
                        if (value.equals(sBoSung)) {
                            continue;
                        } else {
                            lstStatusAll.remove(i);
                        }
                    } catch (Exception ex) {
                        LogUtils.addLogDB(ex);
                    }
                }
            }
        }
        // sap xep lstStatusAll theo value
        // bubbleSort(lstStatusAll, lstStatusAll.size());
        ListModelArray lstModelProcedure = new ListModelArray(lstStatusAll);
        lboxStatus.setModel(lstModelProcedure);

        if (lbxDepartment != null) {
            loadCheckDepartment();
        }

        // set multi check
        onSearch();
    }

    private static void bubbleSort(List unsortedArray, int length)
            throws Exception {
        int counter, index;
        Object temp;
        String value, value1;
        Object obj;
        Object obj1;
        for (counter = 0; counter < length - 1; counter++) { // Loop once for
            // each element
            // in the array.
            for (index = 1; index < length - 1 - counter; index++) { // Once for
                // each
                // element,
                // minus
                // the
                // counter.
                Field field = unsortedArray.get(counter).getClass()
                        .getDeclaredField("name");
                field.setAccessible(true);
                obj = unsortedArray.get(index);
                obj1 = unsortedArray.get(index + 1);
                value = String.valueOf(field.get(obj));
                value1 = String.valueOf(field.get(obj1));
                if (value.compareTo(value1) > 0) { // Test if need a swap or
                    // not.
                    temp = obj; // These three lines just swap the two elements:
                    unsortedArray.set(index, obj1);
                    unsortedArray.set(index + 1, temp);
                }
            }
        }
    }

    private List<Long> convert(String str) {
        List<Long> lstLong = new ArrayList<>();
        if (str != null) {
            String[] statusLst = str.split(";");
            for (String statusStr : statusLst) {
                try {
                    if (statusStr != null) {
                        Long statusL = Long.parseLong(statusStr);
                        lstLong.add(statusL);
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }

            }
        }
        return lstLong;
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        // validate search
        // neu an nut search reset lại itemsCheckedList
        itemsCheckedList = new ArrayList<VFileDept>();
        if (dbFromDay != null && dbToDay != null && dbFromDay.getValue() != null && dbToDay.getValue() != null) {
            if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                showNotification(
                        "Thời gian từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                return;
            }
        }
        if (dbFromDayModify != null && dbToDayModify != null) {
            if (dbFromDayModify.getValue().compareTo(dbToDayModify.getValue()) > 0) {
                showNotification(
                        "Thời gian từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                return;
            }
        }

        lastSearchModel.setCreateDateFrom(dbFromDay == null ? null : dbFromDay
                .getValue());
        lastSearchModel.setCreateDateTo(dbToDay == null ? null : dbToDay
                .getValue());
        lastSearchModel.setModifyDateFrom(dbFromDayModify == null ? null
                : dbFromDayModify.getValue());
        lastSearchModel.setModifyDateTo(dbToDayModify == null ? null
                : dbToDayModify.getValue());
        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        lastSearchModel.setBusinessName(tbCosmeticCompanyName == null ? null
                : tbCosmeticCompanyName.getValue());
        lastSearchModel
                .setBusinessAddress(tbCosmeticCompanyAddress == null ? null
                                : tbCosmeticCompanyAddress.getValue());
        lastSearchModel
                .setDistributePersonName(tbCosmeticDistributePersonName == null ? null
                                : tbCosmeticDistributePersonName.getValue());
        lastSearchModel.setTaxCode(tbBusinessTaxCode == null ? null
                : tbBusinessTaxCode.getValue());

        if (lboxStatus.getSelectedItem() != null) {
            try {
                Object value = lboxStatus.getSelectedItem().getValue();
                if (value != null && value instanceof String) {
                    Long valueL = Long.valueOf((String) value);
                    if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                        lastSearchModel.setStatus(valueL);
                    }
                } else {
                    lastSearchModel.setStatus(null);
                }
            } catch (NumberFormatException ex) {
                LogUtils.addLogDB(ex);
            }

        }

        if (lbxDepartment != null) {
            if (lbxDepartment.getSelectedItem() != null) {
                try {
                    Object value = lbxDepartment.getSelectedItem().getValue();
                    if (value != null) {
                        Long valueL = Long.valueOf(value.toString());
                        if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                            lastSearchModel.setDeptTestingId(valueL);
                        } else {
                            lastSearchModel.setDeptTestingId(null);
                        }
                    } else {
                        lastSearchModel.setDeptTestingId(null);
                    }
                } catch (NumberFormatException ex) {
                    LogUtils.addLogDB(ex);
                }

            }
        }

        if (lboxDocumentTypeCode != null) {
            if (lboxDocumentTypeCode.getSelectedItem() != null) {
                try {
                    Object value = lboxDocumentTypeCode.getSelectedItem().getValue();
                    if (value != null) {
                        Long valueL = Long.valueOf(value.toString());
                        if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                            lastSearchModel.setDocumentTypeCode(valueL);
                        } else {
                            lastSearchModel.setDocumentTypeCode(null);
                        }
                    } else {
                        lastSearchModel.setDocumentTypeCode(null);
                    }
                } catch (NumberFormatException ex) {
                    LogUtils.addLogDB(ex);
                }

            }
        }

        if (statusStr != null) {
            if (statusStr.equals(sBoSung)) {
                lastSearchModel.setStatus(Long.parseLong(sBoSung));
            }
        }

        lastSearchModel.setFileType(FILE_TYPE);

        userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);

        reloadModel(lastSearchModel);

    }

    private void reloadModel(SearchModel searchModel) {
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage()
                * userPagingBottom.getPageSize();
        PagingListModel plm;

        VFileDeptDAO objDAOHE = new VFileDeptDAO();
        plm = objDAOHE.searchFileDept(searchModel, getUserId(), getDeptId(),
                start, take);

        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstCosmetic = plm.getLstReturn();
        lstModel.setMultiple(true);

        lbList.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #importOrderAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch=#importOrderAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            // Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    // Show popup khi an nut thao tac
    @Listen("onShowActionMulti=#importOrderAll")
    public void onShowActionMulti() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("lstCosmetic", itemsCheckedList);
        Window window = createWindow("MultiProcess",
                "/Pages/module/cosmetic/viewMultiProcess.zul", arguments,
                Window.MODAL);
        window.setMode(Window.Mode.MODAL);
    }

    @Listen("onChangeTime=#importOrderAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            if (dbFromDay != null) {
                dbFromDay.setValue(model.getFromDate());
            }
            if (dbToDay != null) {
                dbToDay.setValue(model.getToDate());
            }
            if (dbFromDayModify != null) {
                dbFromDayModify.setValue(model.getFromDate());
            }
            if (dbToDayModify != null) {
                dbToDayModify.setValue(model.getToDate());
            }

            onSearch();
        }
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", importOrderAll);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("procedureId", FILE_TYPE);

        arguments.put("deptId", DEPT_ID);
        return arguments;

    }

    @Listen("onOpenCreate=#importOrderAll")
    public void onOpenCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("CRUDMode", "CREATE");
        setParam(arguments);
        Window window = createWindow("windowCRUD",
                "/Pages/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
        window.setMode(Window.Mode.EMBEDDED);
        // importOrderAll.setVisible(false);
    }

    @Listen("onSelect =#incList #lbList")
    public void onSelect(Event event) {
        Listbox lb = (Listbox) event.getTarget();
        List<Listitem> li = lb.getItems();
        Set<Listitem> liselect = lb.getSelectedItems();
        // loai bo nhưng item trong lb co trong itemsCheckedList
        for (Listitem itemlb : li) {
            for (VFileDept items : itemsCheckedList) {
                if (((VFileDept) itemlb.getValue()).getFileId().equals(
                        items.getFileId())) {
                    itemsCheckedList.remove(items);
                    break;
                }
            }
        }
        // add item check vao itemsCheckedList
        for (Listitem item : liselect) {
            itemsCheckedList.add((VFileDept) item.getValue());
        }
    }

    @Listen("onOpenView =#incList #lbList")
    public void onOpenView(Event event) {
        VFileDept obj = (VFileDept) event.getData();

        if (FILE_TYPE_STR.equals(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT)) {
            createWindowViewTtb(obj.getFileId(), Constants.DOCUMENT_MENU.ALL,
                    importOrderAll);
        } else if (FILE_TYPE_STR.equals(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT)) {
            createWindowViewXnn(obj.getFileId(), Constants.DOCUMENT_MENU.ALL,
                    importOrderAll);
        } else {
            createWindowView(obj.getFileId(), Constants.DOCUMENT_MENU.ALL,
                    importOrderAll);
        }
        importOrderAll.setVisible(false);

    }

    @Listen("onRefresh =#importOrderAll")
    public void onRefresh(Event event) {
        onOpenView(event);
    }

    @Listen("onSelectedProcess = #importOrderAll")
    public void onSelectedProcess(Event event) {
        importOrderAll.setVisible(false);

    }

    private void createWindowView(Long id, int menuType, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView",
                "/Pages/module/importorder/importOrderView.zul", arguments,
                Window.EMBEDDED);
    }

    private void createWindowViewTtb(Long id, int menuType, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView",
                "/Pages/module/importreq/importdeviceView.zul", arguments,
                Window.EMBEDDED);
    }

    private void createWindowViewXnn(Long id, int menuType, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView", "/Pages/rapidTest/viewRapidTest.zul",
                arguments, Window.EMBEDDED);
    }

    @Listen("onSave = #importOrderAll")
    public void onSave(Event event) {
        importOrderAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi
     * windowDocIn len Con cac su kien save, update thi da xu li hien thi
     * windowDocIn trong phuong thuc onSave
     */
    @Listen("onVisible =  #importOrderAll")
    public void onVisible() {
        reloadModel(lastSearchModel);
        importOrderAll.setVisible(true);
    }

    @Listen("onViewFlow = #lbList")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    @Listen("onClick=#incSearchFullForm #btnReset")
    public void doResetForm() throws Exception {
        tbCosmeticCompanyAddress.setValue("");
        tbCosmeticCompanyName.setValue("");
        tbCosmeticDistributePersonName.setValue("");
        tbNSWFileCode.setValue("");
        if (tbBusinessTaxCode != null) {
            tbBusinessTaxCode.setValue("");
        }
        if (dbFromDay != null) {
            dbFromDay.setValue(null);
        }
        if (dbToDay != null) {
            dbToDay.setValue(new Date());
        }
        if (dbFromDayModify != null) {
            dbFromDayModify.setValue(null);
        }
        if (dbToDayModify != null) {
            dbToDayModify.setValue(new Date());
        }
        lboxStatus.setSelectedIndex(0);

        onSearch();
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public Constants getConstants() {
        return constants;
    }

    public void setConstants(Constants constants) {
        this.constants = constants;
    }

    @Listen("onClick=#searchToolbar #btnExportExcel")
    public void doExportExcel() throws Exception {

        ExcelExporter exporter = new ExcelExporter();

        Listbox newList = (Listbox) lbList.clone();

        PagingListModel plm;

        VFileDeptDAO objDAOHE = new VFileDeptDAO();
        plm = objDAOHE.searchFileDept(lastSearchModel, getUserId(),
                getDeptId(), 0, 0);

        newList.setModel(new ListModelArray(plm.getLstReturn()));
        newList.renderAll();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        exporter.export(newList, out);

        AMedia amedia = new AMedia("List.xlsx", "xls", "application/file",
                out.toByteArray());
        Filedownload.save(amedia);

        out.close();
    }

    @Listen("onClick=#searchToolbar #btnExportPdf")
    public void doExportPdf() throws Exception {

        PdfExporter exporter = new PdfExporter();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        exporter.export(lbList, out);

        AMedia amedia = new AMedia("List.pdf", "pdf", "application/pdf",
                out.toByteArray());
        Filedownload.save(amedia);

        out.close();
    }

    // load co quan kiem tra
    private void loadCheckDepartment() {
        List<Department> departmentsList;

        DepartmentDAOHE dept = new DepartmentDAOHE();
        Long cqkt = dept.GetDeptParent(getDeptId(),
                Constants.DEPARTMENT.DEPT_TYPE_CHECK);

        if (cqkt != null) {
            departmentsList = dept.getListDeptById(cqkt);
        } else {
            departmentsList = dept
                    .findByDeptTypeSearch(Constants.DEPARTMENT.DEPT_TYPE_CHECK);
        }

        ListModel<Department> listModel = new ListModelArray<Department>(
                departmentsList);
        lbxDepartment.setModel(listModel);
        lbxDepartment.renderAll();
        lbxDepartment.setSelectedIndex(0);
    }
}
