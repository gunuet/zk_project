package com.viettel.module.common.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importDevice.BO.AppliesFee;
import com.viettel.module.importDevice.DAO.AppliesFeeDAO;
import com.viettel.utils.LogUtils;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class UpdateAppliesFeeController extends BaseComposer {

    @Wire
    Textbox tbFeeType, tbContent;
    @Wire
    Decimalbox tbAmountFee;
    @Wire
    Button btnSaveAppliesFee;
    @Wire
    Textbox tbContentPopupInternal;
    @Wire
    Label lbTopWarning;
    @Wire
    Listbox lbAction;
    @Wire
    Window createAppliesFeePopup;
    Long importDeviceFileId;
    Long appliesFeeId;
    protected Window windowParent;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        importDeviceFileId = (Long) arguments.get("importDeviceFileId");
        appliesFeeId = (Long) arguments.get("id");
        windowParent = (Window) arguments.get("parentWindow");
        if (appliesFeeId != null && appliesFeeId > 0) {
            AppliesFeeDAO idDAO = new AppliesFeeDAO();
            AppliesFee inter = idDAO.findAppliesFeeById(appliesFeeId);
            if (inter != null) {
                tbFeeType.setValue(inter.getFeeType());
                tbAmountFee.setValue(inter.getAmountFee());
                tbContent.setValue(inter.getNote());
                if (inter.getIs_Active() == 1L) {
                    lbAction.setSelectedIndex(0);
                } else {
                    lbAction.setSelectedIndex(1);
                }
                btnSaveAppliesFee.setLabel("Cập nhật");
            }
        }
    }

    @Listen("onClick = #btnSaveAppliesFee")
    public void onSave(Event event) {
        try {
            AppliesFeeDAO idDAO = new AppliesFeeDAO();
            if (checkValidate()) {
                AppliesFee inter = new AppliesFee();
                if (appliesFeeId != null && appliesFeeId > 0) {

                    inter = idDAO.findAppliesFeeById(appliesFeeId);
                } else {
                    inter.setIsDeleted(0L);
                    inter.setImportDeviceFileId(importDeviceFileId);
                }

                inter.setFeeType(tbFeeType.getValue());
                inter.setAmountFee(tbAmountFee.getValue().toString());
                inter.setIsDeleted(0L);
                inter.setNote(tbContent.getValue());
                inter.setIs_Active(Long.parseLong(lbAction.getSelectedItem().getValue().toString()));
                idDAO.saveOrUpdate(inter);
                closeView();
                createAppliesFeePopup.detach();

            }
        } catch (WrongValueException | NumberFormatException ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onClick = #btnCancelAppliesFee")
    public void onClosePopup(Event event) {
        createAppliesFeePopup.detach();
    }

    private void closeView() {
        //linhdx 20160301 dong cua so view
        try {
//            Window windowview = (Window) Path.getComponent("/" + windowParent);
            if (windowParent != null) {
                Events.sendEvent("onRefreshList", windowParent, null);
            }
        } catch (Exception ex) {
            LogUtils.addLog(ex);
        }
    }

    private boolean checkValidate() {
        if (tbFeeType.getValue() == null) {
            lbTopWarning.setValue("Bạn chưa nhập loại phí!");
            return false;
        }
        if (tbAmountFee.getValue() == null || "".equals(tbAmountFee.getValue())) {
            lbTopWarning.setValue("Bạn chưa nhập mức phí!");
            return false;
        }
        return true;
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }
}
