package com.viettel.module.common.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importDevice.BO.InternalProcessing;
import com.viettel.module.importDevice.DAO.InternalProcessingDAO;
import com.viettel.utils.LogUtils;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class UpdateInternalProcessController extends BaseComposer {

    @Wire
    Datebox dbdateCreateInternal;
    @Wire
    Button btnSaveIntenal;
    @Wire
    Textbox tbContentPopupInternal;
    @Wire
    Label lbTopWarning;
    @Wire
    Window createInternalProcessPopup;
    Long meetingId;
    Long internalProcessId;
    protected Window windowParent;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        meetingId = (Long) arguments.get("meetingId");
        internalProcessId = (Long) arguments.get("internalProcessId");
        windowParent = (Window) arguments.get("parentWindow");
        if(internalProcessId != null && internalProcessId > 0){
            InternalProcessingDAO idDAO = new InternalProcessingDAO();
            InternalProcessing inter = idDAO.findInternalProcessById(internalProcessId);
            dbdateCreateInternal.setValue(inter.getCreateDate());
            tbContentPopupInternal.setValue(inter.getContent());
            btnSaveIntenal.setLabel("Cập nhật");
        }
    }

    @Listen("onClick = #btnSaveIntenal")
    public void onSave(Event event) {
        try {
            InternalProcessingDAO idDAO = new InternalProcessingDAO();
            if (checkValidate()) {
                InternalProcessing inter = new InternalProcessing();
                if (internalProcessId != null && internalProcessId > 0) {
                    
                    inter = idDAO.findInternalProcessById(internalProcessId);
                } else {
                    inter.setIsActive(1L);
                    inter.setMeetingId(meetingId);
                }
                
                inter.setCreateDate(dbdateCreateInternal.getValue());
                inter.setContent(tbContentPopupInternal.getValue());
                idDAO.saveOrUpdate(inter);
                closeView();
                createInternalProcessPopup.detach();
                
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onClick = #btnCancelInternalProcess")
    public void onClosePopup(Event event) {
        createInternalProcessPopup.detach();
    }
    private void closeView() {
        //linhdx 20160301 dong cua so view
        try {
//            Window windowview = (Window) Path.getComponent("/" + windowParent);
            if (windowParent != null) {
                Events.sendEvent("onRefreshList", windowParent, null);
            }
        } catch (Exception ex) {
            LogUtils.addLog(ex);
        }
    }
    private boolean checkValidate() {
        if (dbdateCreateInternal.getValue() == null) {
            lbTopWarning.setValue("Bạn chưa nhập ngày tạo!");
            return false;
        }
        if (tbContentPopupInternal.getValue() == null || "".equals(tbContentPopupInternal.getValue())) {
            lbTopWarning.setValue("Bạn chưa nhập phần nội dung!");
            return false;
        }else{
            if(tbContentPopupInternal.getValue().length() > 2000){
                lbTopWarning.setValue("Trường nội dung không được nhập quá 2000 ký tự!");
                return false;
            }
        }
        return true;
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }
}
