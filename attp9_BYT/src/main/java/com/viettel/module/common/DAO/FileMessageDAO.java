/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.common.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.common.BO.FileMessage;

//import com.viettel.module.rapidtest.BO.Template;
import com.viettel.utils.Constants;
import com.viettel.utils.StringUtils;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import org.hibernate.Query;





/**
 *
 * @author Administrator
 */
public class FileMessageDAO extends GenericDAOHibernate<FileMessage, Long> {

    public FileMessageDAO() {
        super(FileMessage.class);
    }
    
    @Override
    public void saveOrUpdate(FileMessage obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        } 
    }
    public List<FileMessage> getListFileMessage(Long fileId) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
        		" SELECT f FROM FileMessage f WHERE f.isActive = 1 AND f.fileId =:fileId ");

        docHQL.append(" order by f.fileMessageId desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter("fileId", fileId);
        List<FileMessage> listObj = fileQuery.list();

        return listObj;
    }
    
    public void deleteAttach(FileMessage attach) {
        attach.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(attach);
    }
    
}
