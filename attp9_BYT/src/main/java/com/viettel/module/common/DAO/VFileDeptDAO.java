/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.common.DAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.common.BO.VFileDept;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.model.SearchModel;


/**
 *
 * @author Thanhdv
 */
public class VFileDeptDAO extends
        GenericDAOHibernate<VFileDept, Long> {

    public VFileDeptDAO() {
        super(VFileDept.class);
    }

    public List findListStatus(SearchModel searchModel, Long receiverId, Long receiveDeptId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) ");
        StringBuilder hql = new StringBuilder();
        
        hql.append("  FROM VFileDept n");
        
        hql.append(" WHERE n.fileType = ?");
        listParam.add(searchModel.getFileType());
        
        /*
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                            + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                            + " AND no.type = ? "
                            + " ) AND "
                            //+ " 1 = 1 ");
                            + " p.receiveUserId = ? ");
                    listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " (p.receiveUserId is null AND "
                            + " p.receiveGroupId = ? ) ");
                    listParam.add(receiveDeptId);
                    break;
                default:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }
        //  hql.append(" order by n.fileId desc");
         */
        
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }

      public PagingListModel searchFileDept(SearchModel searchModel,
            Long receiverId, Long receiveDeptId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) ");
        StringBuilder strCountBuf = new StringBuilder("select count(n) ");
        StringBuilder hql = new StringBuilder();  
        
        hql.append("  FROM VFileDept n");
        
        hql.append(" WHERE n.fileType = ?");
        listParam.add(searchModel.getFileType());
        
        /*
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append("  FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            //+ " n.status = p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                           // + " n.status != p.status AND "
                            + " p.finishDate is not null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            //+ " n.status != p.status AND "
                            //+ " p.finishDate is null AND "
                            //+ " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                            //+ " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                            //+ " AND no.type = ? "
                            //+ " ) AND "
                            //+ " 1 = 1 ");
                            + " p.receiveUserId = ? ");
                    
                    hql.append("AND ((n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + ") "
                    );
                    //listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    listParam.add(receiverId);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_GIAM_XNK);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_CHAT_XNK);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LDC_PHE_DUYET_XU_LY_LO_HANG_KHONG_DAT_XNK);                    
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            //+ " (p.receiveUserId is null AND "
                            + " p.receiveGroupId = ? ) ");
                    listParam.add(receiveDeptId);
                    break;
                default:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }
        */

        if (searchModel != null) {

            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getModifyDateFrom() != null) {
                hql.append(" AND n.modifyDate >= ? ");
                listParam.add(searchModel.getModifyDateFrom());
            }
            if (searchModel.getModifyDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getModifyDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(n.cosmeticNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }
            
            if (searchModel.getDocumentTypeCode() != null) {
                hql.append(" AND n.documentTypeCode = ?");
                listParam.add(searchModel.getDocumentTypeCode());
            }
            
            if (searchModel.getDeptTestingId() != null) {
                hql.append(" AND n.deptTestingId = ?");
                listParam.add(searchModel.getDeptTestingId());
            }
            
            if (searchModel.getDocumentTypeCode() != null) {
                hql.append(" AND n.documentTypeCode = ?");
                listParam.add(searchModel.getDocumentTypeCode());
            }

            if (searchModel instanceof CosmeticFileModel) {
                CosmeticFileModel cosmeticFileModel = (CosmeticFileModel) searchModel;
                if (cosmeticFileModel.getBrandName() != null && cosmeticFileModel.getBrandName().trim().length() > 0) {
                    hql.append(" AND lower(n.brandName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBrandName()));
                }

                if (cosmeticFileModel.getProductName() != null && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getProductName()));
                }

                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null && cosmeticFileModel.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessAddress()));
                }

                if (cosmeticFileModel.getDistributePersonName() != null && cosmeticFileModel.getDistributePersonName().trim().length() > 0) {
                    hql.append(" AND lower(n.distributePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getDistributePersonName()));
                }

                if (cosmeticFileModel.getIntendedUse() != null && cosmeticFileModel.getIntendedUse().trim().length() > 0) {
                    hql.append(" AND lower(n.intendedUse) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getIntendedUse()));
                }
                if (cosmeticFileModel.getTaxCode()!= null && cosmeticFileModel.getTaxCode().trim().length() > 0) {
                    hql.append(" AND lower(n.taxCode) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getTaxCode()));
                }

            }

        }

        hql.append(" order by n.createDate desc,n.fileCode desc");

        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }
    
}
