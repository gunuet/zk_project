/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.controller.testManageAdditionalFile;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.BO.test.TestFile;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class TestFileDaohe extends GenericDAOHibernate<TestFile,Long> {

    public TestFileDaohe() {
        super(TestFile.class);
    }
    public List<TestFile> findListTestFile(TestFile search){
        List<TestFile> searchList =  new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM TestFile t WHERE 1=1");
        Query query = session.createQuery(strBuil.toString());
        return query.list();
    }
    
    public PagingListModel searchFile(TestFile search, int start, int take){
        List listParam = new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM TestFile t WHERE 1=1");
        StringBuilder strCountBuil = new StringBuilder(" select count(t.testFileId)  FROM TestFile t WHERE 1=1");
        StringBuilder sql = new StringBuilder();
        if(search != null){
            if(search.getTestFileCode()!= null){
                sql.append("AND t.testFileCode like ?");
                listParam.add(search.getTestFileCode());
            }
            if(search.getOwnerName() != null){
                sql.append("AND t.ownerName like ?");
                listParam.add(search.getOwnerName());
            }
            if(search.getTestFileStatus() != null){
                sql.append("AND t.testFileStatus like ?");
                listParam.add(search.getTestFileStatus());
            }
            if(search.getDateIn() != null){
                sql.append("AND t.dateIn > ?");
                listParam.add(search.getDateIn());
            }
        }
        strBuil.append(sql);
        strCountBuil.append(sql);
        Query query = session.createQuery(strBuil.toString());
        Query queryCount = session.createQuery(strCountBuil.toString());
        for(int i = 0; i<listParam.size(); i++){
            query.setParameter(i, listParam.get(i));
            queryCount.setParameter(i, listParam.get(i));
        }
        query.setFirstResult(start);
        query.setMaxResults(take);
        List lst =query.list();
        Long count = (Long) queryCount.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }
    
    public PagingListModel findFile( int start, int take){
        StringBuilder strBuil = new StringBuilder("SELECT t FROM TestFile t WHERE 1=1");
        StringBuilder strCountBuil = new StringBuilder(" select count(t.testFileId)  FROM TestFile t WHERE 1=1");
        Query query = session.createQuery(strBuil.toString());
        Query queryCount = session.createQuery(strCountBuil.toString());
        query.setFirstResult(start);
        query.setMaxResults(take);
        List lst =query.list();
        Long count = (Long) queryCount.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }
    
}
