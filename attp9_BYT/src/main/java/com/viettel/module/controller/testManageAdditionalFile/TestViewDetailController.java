/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.controller.testManageAdditionalFile;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.BO.test.TestFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Window;

/**
 *
 * @author Administrator
 */
public class TestViewDetailController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    private TestFile testFile = new TestFile();
    private Long fileID;

    @Wire
    private Window windowViewDetail;
    


    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        fileID = (Long) Executions.getCurrent().getArg().get("id");
        TestFileDaohe dao = new TestFileDaohe();
        testFile = (TestFile) dao.getById("testFileId", fileID);
        return super.doBeforeCompose(page, parent, compInfo);

    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
        } catch (Exception ex) {
            Logger.getLogger(viewManageAdditionalFileController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onLoadInfor() {

    }

    public void setTestFile(TestFile testFile) {
        this.testFile = testFile;
    }

    public TestFile getTestFile() {
        return testFile;
    }
    
    public Long getFileID(){
        return fileID;
    }

    public void setFileID(Long fileID){
        this.fileID = fileID;
    }
}
