/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.controller.testManageAdditionalFile;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.BO.test.TestingImportedFood;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Window;

/**
 *
 * @author Administrator
 */
public class testImportFoodController extends BaseComposer{
    private static final long serialVersionUID = 1L;
     @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbListTestFile")
    private Listbox lbListTestFile;
    @Wire
    private Window viewListTestFile;
    private TestingImportedFood searchModel;
    
    @Override
    public void doAfterCompose(Component comp){
         try {
            super.doAfterCompose(comp);
            searchModel = new TestingImportedFood();
            onSearch();
         }catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }
    
    public void onSearch(){
         userPagingBottom.setActivePage(0);
        userPagingTop.setActivePage(0);
        int start = userPagingBottom.getActivePage()*userPagingBottom.getPageSize();
        int take = userPagingBottom.getPageSize();
        testImportFoodDAO dao = new testImportFoodDAO();
         PagingListModel paging = dao.findFileFood(searchModel,start, take);
        userPagingBottom.setTotalSize(paging.getCount());
        ListModelArray lstModel = new ListModelArray(paging.getLstReturn());
        lbListTestFile.setModel(lstModel);
    }
    
}
