/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.controller.testManageAdditionalFile;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.BO.test.TestingImportedFood;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class testImportFoodDAO extends GenericDAOHibernate<TestingImportedFood, Long>{
    
    public testImportFoodDAO() {
        super(TestingImportedFood.class);
    }
    
    public PagingListModel findFileFood(TestingImportedFood searchForm, int start, int take){
         List listParam = new ArrayList();
        try {
            StringBuilder hql = new StringBuilder(" from TestingImportedFood q where 1 = 1 ");
            Query countQuery = session.createQuery("select count(q) " + hql.toString());
            Query query = session.createQuery("select q " + hql.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
}
