/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.controller.testManageAdditionalFile;

import com.viettel.core.base.DAO.BaseComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Administrator
 */
public class testSearchController extends BaseComposer{
    private static final long serialVersionUID = 1L;
    
    @Wire 
    private Textbox txtTestFileCode,txtTestOwnerName,txtTestStatusFile;
    @Wire 
    private Datebox dbDateForm,dbToDay;
    
    @Wire 
    private Groupbox testSearchGbx;
    
    @Wire
    private Combobox cbTestCheckMethod,cbTestStatusFile;
    
    @Override
    public void doAfterCompose(Component  cmpt){
        try{
            super.doAfterCompose(cmpt);
        }catch(Exception e){
            
        }
    }
    
}
