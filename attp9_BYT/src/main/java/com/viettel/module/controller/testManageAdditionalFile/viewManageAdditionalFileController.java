/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.controller.testManageAdditionalFile;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.BO.test.TestFile;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Administrator
 */
public class viewManageAdditionalFileController extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbListTestFile")
    private Listbox lbListTestFile;
    @Wire
    private Window viewListTestFile;
    private TestFile searchFile;

    @Wire("#incTestSeach #testSearchGbx")
    private Groupbox testSearchGbx;

    @Wire("#incTestSeach #txtTestFileCode")
    private Textbox txtTestFileCode;

    @Wire("#incTestSeach #txtTestOwnerName")
    private Textbox txtTestOwnerName;

    @Wire("#incTestSeach #dbDateForm")
    private Datebox dbDateForm;

    @Wire("#incTestSeach #dbToDay")
    private Datebox dbToDay;

    @Wire("#incTestSeach #cbTestStatusFile")
    private Combobox cbTestStatusFile;

    @Wire("#incTestSeach #cbTestCheckMethod")
    private Combobox cbTestCheckMethod;


    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
        } catch (Exception ex) {
            Logger.getLogger(viewManageAdditionalFileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        searchFile = new TestFile();
        onSearch();

    }

    @Listen("onClick=#incTestSeach #btnSearch")
    public void onSearch() {
        
        
        if (!"".equals(txtTestFileCode.getValue().trim()) && txtTestFileCode.getValue() != null) {
            searchFile.setTestFileCode(txtTestFileCode.getValue());
        }
        if (!"".equals(txtTestOwnerName.getValue().trim()) && txtTestOwnerName.getValue() != null) {
            searchFile.setOwnerName(txtTestOwnerName.getValue());
        }
       

        userPagingBottom.setActivePage(0);
        userPagingTop.setActivePage(0);
        userPagingBottom.setPageSize(10);
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        int take = userPagingBottom.getPageSize();
        TestFileDaohe dao = new TestFileDaohe();
        PagingListModel paging = dao.searchFile(searchFile, start, take);
        userPagingBottom.setTotalSize(paging.getCount());
        ListModelArray lstModel = new ListModelArray(paging.getLstReturn());
        lbListTestFile.setModel(lstModel);
    }

    @Listen("onOpenView= #incList #lbListTestFile ")
    public void onOpenView(Event evt) {
        TestFile viewFile = (TestFile) evt.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", viewFile.getTestFileId());
        arguments.put("parentWindow", viewListTestFile);
        createWindow("testWindView", "/Pages/TEST_ManageAdditionalFile/viewDetail.zul", arguments, Window.EMBEDDED);
        viewListTestFile.setVisible(false);
    }
}
