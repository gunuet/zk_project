/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GPCP_BINHNT53
 */
@Entity
@Table(name = "ANNEXE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Annexe.findAll", query = "SELECT a FROM Annexe a"),
    @NamedQuery(name = "Annexe.findBySubstance", query = "SELECT a FROM Annexe a WHERE a.substance = :substance"),
    @NamedQuery(name = "Annexe.findByAnnexeId", query = "SELECT a FROM Annexe a WHERE a.annexeId = :annexeId"),
    @NamedQuery(name = "Annexe.findByCasNumber", query = "SELECT a FROM Annexe a WHERE a.casNumber = :casNumber"),
    @NamedQuery(name = "Annexe.findByRefNo", query = "SELECT a FROM Annexe a WHERE a.refNo = :refNo"),
    @NamedQuery(name = "Annexe.findByFieldUse", query = "SELECT a FROM Annexe a WHERE a.fieldUse = :fieldUse"),
    @NamedQuery(name = "Annexe.findByAnnexeType", query = "SELECT a FROM Annexe a WHERE a.annexeType = :annexeType"),
    @NamedQuery(name = "Annexe.findByMaximumAuthorized", query = "SELECT a FROM Annexe a WHERE a.maximumAuthorized = :maximumAuthorized"),
    @NamedQuery(name = "Annexe.findByLimitationRequirements", query = "SELECT a FROM Annexe a WHERE a.limitationRequirements = :limitationRequirements"),
    @NamedQuery(name = "Annexe.findByConditionsOfUse", query = "SELECT a FROM Annexe a WHERE a.conditionsOfUse = :conditionsOfUse"),
    @NamedQuery(name = "Annexe.findByAllowedUntil", query = "SELECT a FROM Annexe a WHERE a.allowedUntil = :allowedUntil"),
    @NamedQuery(name = "Annexe.findByAnnexeTypeName", query = "SELECT a FROM Annexe a WHERE a.annexeTypeName = :annexeTypeName"),
    @NamedQuery(name = "Annexe.findByIsActive", query = "SELECT a FROM Annexe a WHERE a.isActive = :isActive"),
    @NamedQuery(name = "Annexe.findByNote", query = "SELECT a FROM Annexe a WHERE a.note = :note")})
public class Annexe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "SUBSTANCE")
    private String substance;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "ANNEXE_SEQ", sequenceName = "ANNEXE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANNEXE_SEQ")
    @Column(name = "ANNEXE_ID")
    private Long annexeId;
    @Column(name = "CAS_NUMBER")
    private String casNumber;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "FIELD_USE")
    private String fieldUse;
    @Column(name = "ANNEXE_TYPE")
    private Long annexeType;
    @Column(name = "MAXIMUM_AUTHORIZED")
    private String maximumAuthorized;
    @Column(name = "LIMITATION_REQUIREMENTS")
    private String limitationRequirements;
    @Column(name = "CONDITIONS_OF_USE")
    private String conditionsOfUse;
    @Column(name = "ALLOWED_UNTIL")
    @Temporal(TemporalType.DATE)
    private Date allowedUntil;
    @Column(name = "ANNEXE_TYPE_NAME")
    private String annexeTypeName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "NOTE")
    private String note;

    public Annexe() {
    }

    public Annexe(Long annexeId) {
        this.annexeId = annexeId;
    }

    public String getSubstance() {
        return substance;
    }

    public void setSubstance(String substance) {
        this.substance = substance;
    }

    public Long getAnnexeId() {
        return annexeId;
    }

    public void setAnnexeId(Long annexeId) {
        this.annexeId = annexeId;
    }

    public String getCasNumber() {
        return casNumber;
    }

    public void setCasNumber(String casNumber) {
        this.casNumber = casNumber;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getFieldUse() {
        return fieldUse;
    }

    public void setFieldUse(String fieldUse) {
        this.fieldUse = fieldUse;
    }

    public Long getAnnexeType() {
        return annexeType;
    }

    public void setAnnexeType(Long annexeType) {
        this.annexeType = annexeType;
    }

    public String getMaximumAuthorized() {
        return maximumAuthorized;
    }

    public void setMaximumAuthorized(String maximumAuthorized) {
        this.maximumAuthorized = maximumAuthorized;
    }

    public String getLimitationRequirements() {
        return limitationRequirements;
    }

    public void setLimitationRequirements(String limitationRequirements) {
        this.limitationRequirements = limitationRequirements;
    }

    public String getConditionsOfUse() {
        return conditionsOfUse;
    }

    public void setConditionsOfUse(String conditionsOfUse) {
        this.conditionsOfUse = conditionsOfUse;
    }

    public Date getAllowedUntil() {
        return allowedUntil;
    }

    public void setAllowedUntil(Date allowedUntil) {
        this.allowedUntil = allowedUntil;
    }

    public String getAnnexeTypeName() {
        return annexeTypeName;
    }

    public void setAnnexeTypeName(String annexeTypeName) {
        this.annexeTypeName = annexeTypeName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (annexeId != null ? annexeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Annexe)) {
            return false;
        }
        Annexe other = (Annexe) object;
        if ((this.annexeId == null && other.annexeId != null) || (this.annexeId != null && !this.annexeId.equals(other.annexeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.Annexe[ annexeId=" + annexeId + " ]";
    }

}
