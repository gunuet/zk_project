/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author giangnh20
 */
@Entity
@Table(name = "CA_USER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CaUser.findAll", query = "SELECT c FROM CaUser c"),
    @NamedQuery(name = "CaUser.findByCaId", query = "SELECT c FROM CaUser c WHERE c.caId = :caId"),
    @NamedQuery(name = "CaUser.findByCaSerial", query = "SELECT c FROM CaUser c WHERE c.caSerial = :caSerial"),
    @NamedQuery(name = "CaUser.findByIsActive", query = "SELECT c FROM CaUser c WHERE c.isActive = :isActive"),
    @NamedQuery(name = "CaUser.findByStamper", query = "SELECT c FROM CaUser c WHERE c.stamper = :stamper"),
    @NamedQuery(name = "CaUser.findBySignature", query = "SELECT c FROM CaUser c WHERE c.signature = :signature"),
    @NamedQuery(name = "CaUser.findByUserId", query = "SELECT c FROM CaUser c WHERE c.userId = :userId")})
public class CaUser implements Serializable {
    
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "CA_USER_SEQ", sequenceName = "CA_USER_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_USER_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "CA_ID")
    private Long caId;
    @Size(max = 255)
    @Column(name = "CA_SERIAL")
    private String caSerial;
    @Size(max = 255)
    @Column(name = "STAMPER")
    private String stamper;
    @Size(max = 255)
    @Column(name = "SIGNATURE")
    private String signature;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private Long userId;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    
    public CaUser() {
    }

    public CaUser(Long caId) {
        this.caId = caId;
    }

    public CaUser(Long caId, long userId) {
        this.caId = caId;
        this.userId = userId;
    }

    public Long getCaId() {
        return caId;
    }

    public void setCaId(Long caId) {
        this.caId = caId;
    }

    public String getCaSerial() {
        return caSerial;
    }

    public void setCaSerial(String caSerial) {
        this.caSerial = caSerial;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public String getStamper() {
        return stamper;
    }

    public void setStamper(String stamper) {
        this.stamper = stamper;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (caId != null ? caId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaUser)) {
            return false;
        }
        CaUser other = (CaUser) object;
        if ((this.caId == null && other.caId != null) || (this.caId != null && !this.caId.equals(other.caId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CaUser[ caId=" + caId + " ]";
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    
}
