/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.lang.Long;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LONG HOANG GIANG
 */
@Entity
@Table(name = "COS_ADDITIONAL_REQUEST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosAdditionalRequest.findAll", query = "SELECT c FROM CosAdditionalRequest c"),
    @NamedQuery(name = "CosAdditionalRequest.findByAdditionalRequestId", query = "SELECT c FROM CosAdditionalRequest c WHERE c.additionalRequestId = :additionalRequestId"),
    @NamedQuery(name = "CosAdditionalRequest.findByCreatorId", query = "SELECT c FROM CosAdditionalRequest c WHERE c.creatorId = :creatorId"),
    @NamedQuery(name = "CosAdditionalRequest.findByCreatorName", query = "SELECT c FROM CosAdditionalRequest c WHERE c.creatorName = :creatorName"),
    @NamedQuery(name = "CosAdditionalRequest.findByCreateDate", query = "SELECT c FROM CosAdditionalRequest c WHERE c.createDate = :createDate"),
    @NamedQuery(name = "CosAdditionalRequest.findByFileId", query = "SELECT c FROM CosAdditionalRequest c WHERE c.fileId = :fileId"),
    @NamedQuery(name = "CosAdditionalRequest.findByCreateDeptId", query = "SELECT c FROM CosAdditionalRequest c WHERE c.createDeptId = :createDeptId"),
    @NamedQuery(name = "CosAdditionalRequest.findByCreateDeptName", query = "SELECT c FROM CosAdditionalRequest c WHERE c.createDeptName = :createDeptName"),
    @NamedQuery(name = "CosAdditionalRequest.findByVersion", query = "SELECT c FROM CosAdditionalRequest c WHERE c.version = :version"),
    @NamedQuery(name = "CosAdditionalRequest.findByContent", query = "SELECT c FROM CosAdditionalRequest c WHERE c.content = :content"),
    @NamedQuery(name = "CosAdditionalRequest.findByCommentType", query = "SELECT c FROM CosAdditionalRequest c WHERE c.commentType = :commentType"),
    @NamedQuery(name = "CosAdditionalRequest.findByStatus", query = "SELECT c FROM CosAdditionalRequest c WHERE c.status = :status"),
    @NamedQuery(name = "CosAdditionalRequest.findByAttachId", query = "SELECT c FROM CosAdditionalRequest c WHERE c.attachId = :attachId"),
    @NamedQuery(name = "CosAdditionalRequest.findByIsActive", query = "SELECT c FROM CosAdditionalRequest c WHERE c.isActive = :isActive"),
    @NamedQuery(name = "CosAdditionalRequest.findByReceiveNo", query = "SELECT c FROM CosAdditionalRequest c WHERE c.receiveNo = :receiveNo"),
    @NamedQuery(name = "CosAdditionalRequest.findByReceiveDate", query = "SELECT c FROM CosAdditionalRequest c WHERE c.receiveDate = :receiveDate")})
public class CosAdditionalRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_ADDITIONAL_REQUEST_SEQ", sequenceName = "COS_ADDITIONAL_REQUEST_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_ADDITIONAL_REQUEST_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADDITIONAL_REQUEST_ID")
    private Long additionalRequestId;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 255)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 255)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Column(name = "VERSION")
    private Long version;
    @Size(max = 2000)
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "COMMENT_TYPE")
    private Long commentType;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "ATTACH_ID")
    private Long attachId;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "RECEIVE_NO")
    private String receiveNo;
    @Column(name = "RECEIVE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date receiveDate;

    public CosAdditionalRequest() {
    }

    public CosAdditionalRequest(Long additionalRequestId) {
        this.additionalRequestId = additionalRequestId;
    }

    public Long getAdditionalRequestId() {
        return additionalRequestId;
    }

    public void setAdditionalRequestId(Long additionalRequestId) {
        this.additionalRequestId = additionalRequestId;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCommentType() {
        return commentType;
    }

    public void setCommentType(Long commentType) {
        this.commentType = commentType;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getAttachId() {
        return attachId;
    }

    public void setAttachId(Long attachId) {
        this.attachId = attachId;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (additionalRequestId != null ? additionalRequestId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosAdditionalRequest)) {
            return false;
        }
        CosAdditionalRequest other = (CosAdditionalRequest) object;
        if ((this.additionalRequestId == null && other.additionalRequestId != null) || (this.additionalRequestId != null && !this.additionalRequestId.equals(other.additionalRequestId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosAdditionalRequest[ additionalRequestId=" + additionalRequestId + " ]";
    }
    
}
