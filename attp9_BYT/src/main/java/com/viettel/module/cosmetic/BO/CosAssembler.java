/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "COS_ASSEMBLER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosAssembler.findAll", query = "SELECT c FROM CosAssembler c"),
    @NamedQuery(name = "CosAssembler.findByAssemblerId", query = "SELECT c FROM CosAssembler c WHERE c.assemblerId = :assemblerId"),
    @NamedQuery(name = "CosAssembler.findByAssemblerMain", query = "SELECT c FROM CosAssembler c WHERE c.assemblerMain = :assemblerMain"),
    @NamedQuery(name = "CosAssembler.findByAssemblerSub", query = "SELECT c FROM CosAssembler c WHERE c.assemblerSub = :assemblerSub"),
    @NamedQuery(name = "CosAssembler.findByName", query = "SELECT c FROM CosAssembler c WHERE c.name = :name"),
    @NamedQuery(name = "CosAssembler.findByAddress", query = "SELECT c FROM CosAssembler c WHERE c.address = :address"),
    @NamedQuery(name = "CosAssembler.findByPhone", query = "SELECT c FROM CosAssembler c WHERE c.phone = :phone"),
    @NamedQuery(name = "CosAssembler.findByFax", query = "SELECT c FROM CosAssembler c WHERE c.fax = :fax"),
    @NamedQuery(name = "CosAssembler.findByCosFileId", query = "SELECT c FROM CosAssembler c WHERE c.cosFileId = :cosFileId")})
public class CosAssembler implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_ASSEMBLER_SEQ", sequenceName = "COS_ASSEMBLER_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_ASSEMBLER_SEQ")
    @Column(name = "ASSEMBLER_ID")
    private Long assemblerId;
    @Column(name = "ASSEMBLER_MAIN")
    private Long assemblerMain;
    @Column(name = "ASSEMBLER_SUB")
    private Long assemblerSub;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Size(max = 500)
    @Column(name = "ADDRESS")
    private String address;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "PHONE")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "FAX")
    private String fax;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Transient
    private Boolean mainAssembler;
    @Transient
    private Boolean subAssembler;
    

    public CosAssembler() {
    }

    public CosAssembler(Long assemblerId) {
        this.assemblerId = assemblerId;
    }

    public CosAssembler(Long assemblerId, Long cosFileId) {
        this.assemblerId = assemblerId;
        this.cosFileId = cosFileId;
    }

    public Long getAssemblerId() {
        return assemblerId;
    }

    public void setAssemblerId(Long assemblerId) {
        this.assemblerId = assemblerId;
    }

    public Long getAssemblerMain() {
        return assemblerMain;
    }

    public void setAssemblerMain(Long assemblerMain) {
        this.assemblerMain = assemblerMain;
    }

    public Long getAssemblerSub() {
        return assemblerSub;
    }

    public void setAssemblerSub(Long assemblerSub) {
        this.assemblerSub = assemblerSub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Boolean getMainAssembler() {
        if(mainAssembler == null){
            if(assemblerMain != null && assemblerMain.equals(1l)){
                mainAssembler = true;
            } else {
                mainAssembler = false;
            }
        }
        return mainAssembler;
    }

    public void setMainAssembler(Boolean mainAssembler) {
        this.mainAssembler = mainAssembler;
    }

    public Boolean getSubAssembler() {
        if(subAssembler == null){
            if(assemblerSub != null && assemblerSub.equals(1l)){
                subAssembler = true;
            } else {
                subAssembler = false;
            }
        }
        return subAssembler;
    }

    public void setSubAssembler(Boolean subAssembler) {
        this.subAssembler = subAssembler;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assemblerId != null ? assemblerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosAssembler)) {
            return false;
        }
        CosAssembler other = (CosAssembler) object;
        if ((this.assemblerId == null && other.assemblerId != null) || (this.assemblerId != null && !this.assemblerId.equals(other.assemblerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosAssembler[ assemblerId=" + assemblerId + " ]";
    }
}
