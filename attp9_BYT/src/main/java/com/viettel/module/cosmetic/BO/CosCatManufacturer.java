/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.lang.Long;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nguoibay
 */
@Entity
@Table(name = "COS_CAT_MANUFACTURER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosCatManufacturer.findAll", query = "SELECT c FROM CosCatManufacturer c"),
    @NamedQuery(name = "CosCatManufacturer.findByCatManufacturerId", query = "SELECT c FROM CosCatManufacturer c WHERE c.catManufacturerId = :catManufacturerId"),
    @NamedQuery(name = "CosCatManufacturer.findByName", query = "SELECT c FROM CosCatManufacturer c WHERE c.name = :name"),
    @NamedQuery(name = "CosCatManufacturer.findByAddress", query = "SELECT c FROM CosCatManufacturer c WHERE c.address = :address"),
    @NamedQuery(name = "CosCatManufacturer.findByPhone", query = "SELECT c FROM CosCatManufacturer c WHERE c.phone = :phone"),
    @NamedQuery(name = "CosCatManufacturer.findByFax", query = "SELECT c FROM CosCatManufacturer c WHERE c.fax = :fax"),
    @NamedQuery(name = "CosCatManufacturer.findByIsActive", query = "SELECT c FROM CosCatManufacturer c WHERE c.isActive = :isActive")})
public class CosCatManufacturer implements Serializable {

    private static final long serialVersionUID = 1L;
    @SequenceGenerator(name = "COS_CAT_MANUFACTURER_SEQ", sequenceName = "COS_CAT_MANUFACTURER_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_CAT_MANUFACTURER_SEQ")
    @Column(name = "CAT_MANUFACTURER_ID")
    private Long catManufacturerId;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Size(max = 255)
    @Column(name = "NAME_VALID")
    private String nameValid;
    @Size(max = 500)
    @Column(name = "ADDRESS")
    private String address;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "PHONE")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "FAX")
    private String fax;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "USER_ID")
    private Long userId;

    public CosCatManufacturer() {
    }

    public CosCatManufacturer(Long catManufacturerId) {
        this.catManufacturerId = catManufacturerId;
    }

    public Long getCatManufacturerId() {
        return catManufacturerId;
    }

    public void setCatManufacturerId(Long catManufacturerId) {
        this.catManufacturerId = catManufacturerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public String getNameValid() {
        return nameValid;
    }

    public void setNameValid(String nameValid) {
        this.nameValid = nameValid;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catManufacturerId != null ? catManufacturerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosCatManufacturer)) {
            return false;
        }
        CosCatManufacturer other = (CosCatManufacturer) object;
        if ((this.catManufacturerId == null && other.catManufacturerId != null) || (this.catManufacturerId != null && !this.catManufacturerId.equals(other.catManufacturerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosCatManufacturer[ catManufacturerId=" + catManufacturerId + " ]";
    }

}
