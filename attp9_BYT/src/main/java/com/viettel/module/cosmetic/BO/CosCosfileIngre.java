/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "COS_COSFILE_INGRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosCosfileIngre.findAll", query = "SELECT c FROM CosCosfileIngre c"),
    @NamedQuery(name = "CosCosfileIngre.findByCosfileIngreId", query = "SELECT c FROM CosCosfileIngre c WHERE c.cosfileIngreId = :cosfileIngreId"),
    @NamedQuery(name = "CosCosfileIngre.findByCosFileId", query = "SELECT c FROM CosCosfileIngre c WHERE c.cosFileId = :cosFileId"),
    @NamedQuery(name = "CosCosfileIngre.findByIngredientId", query = "SELECT c FROM CosCosfileIngre c WHERE c.ingredientId = :ingredientId"),
    @NamedQuery(name = "CosCosfileIngre.findByPercent", query = "SELECT c FROM CosCosfileIngre c WHERE c.percent = :percent"),
    @NamedQuery(name = "CosCosfileIngre.findByIsActive", query = "SELECT c FROM CosCosfileIngre c WHERE c.isActive = :isActive")})
public class CosCosfileIngre implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_COSFILE_INGRE_SEQ", sequenceName = "COS_COSFILE_INGRE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_COSFILE_INGRE_SEQ")
    @Column(name = "COSFILE_INGRE_ID")
    private Long cosfileIngreId;
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    @Column(name = "INGREDIENT_ID")
    private Long ingredientId;
    @Column(name = "PERCENT")
    private Double percent;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "INGREDIENT_NAME")
    private String ingredientName;
    
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "VARIANT_OR_SHADE")
    private String variantOrShade;

    public CosCosfileIngre() {
    }

    public CosCosfileIngre(Long cosfileIngreId) {
        this.cosfileIngreId = cosfileIngreId;
    }

    public Long getCosfileIngreId() {
        return cosfileIngreId;
    }

    public void setCosfileIngreId(Long cosfileIngreId) {
        this.cosfileIngreId = cosfileIngreId;
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVariantOrShade() {
        return variantOrShade;
    }

    public void setVariantOrShade(String variantOrShade) {
        this.variantOrShade = variantOrShade;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cosfileIngreId != null ? cosfileIngreId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosCosfileIngre)) {
            return false;
        }
        CosCosfileIngre other = (CosCosfileIngre) object;
        if ((this.cosfileIngreId == null && other.cosfileIngreId != null) || (this.cosfileIngreId != null && !this.cosfileIngreId.equals(other.cosfileIngreId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosCosfileIngre[ cosfileIngreId=" + cosfileIngreId + " ]";
    }
}
