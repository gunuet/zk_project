/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import com.viettel.utils.Constants;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nguoibay
 */
@Entity
@Table(name = "COS_EVALUATION_RECORD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosEvaluationRecord.findAll", query = "SELECT c FROM CosEvaluationRecord c"),
    @NamedQuery(name = "CosEvaluationRecord.findByEvaluationRecordId", query = "SELECT c FROM CosEvaluationRecord c WHERE c.evaluationRecordId = :evaluationRecordId"),
    @NamedQuery(name = "CosEvaluationRecord.findByCreateDate", query = "SELECT c FROM CosEvaluationRecord c WHERE c.createDate = :createDate"),
    @NamedQuery(name = "CosEvaluationRecord.findByBusinessName", query = "SELECT c FROM CosEvaluationRecord c WHERE c.businessName = :businessName"),
    @NamedQuery(name = "CosEvaluationRecord.findByBusinessAddress", query = "SELECT c FROM CosEvaluationRecord c WHERE c.businessAddress = :businessAddress"),
    @NamedQuery(name = "CosEvaluationRecord.findByProductName", query = "SELECT c FROM CosEvaluationRecord c WHERE c.productName = :productName"),
    @NamedQuery(name = "CosEvaluationRecord.findByMainContent", query = "SELECT c FROM CosEvaluationRecord c WHERE c.mainContent = :mainContent"),
    @NamedQuery(name = "CosEvaluationRecord.findByLegal", query = "SELECT c FROM CosEvaluationRecord c WHERE c.legal = :legal"),
    @NamedQuery(name = "CosEvaluationRecord.findByLegalContent", query = "SELECT c FROM CosEvaluationRecord c WHERE c.legalContent = :legalContent"),
    @NamedQuery(name = "CosEvaluationRecord.findByUserId", query = "SELECT c FROM CosEvaluationRecord c WHERE c.userId = :userId"),
    @NamedQuery(name = "CosEvaluationRecord.findByUserName", query = "SELECT c FROM CosEvaluationRecord c WHERE c.userName = :userName"),
    @NamedQuery(name = "CosEvaluationRecord.findByFileId", query = "SELECT c FROM CosEvaluationRecord c WHERE c.fileId = :fileId"),
    @NamedQuery(name = "CosEvaluationRecord.findByFileType", query = "SELECT c FROM CosEvaluationRecord c WHERE c.fileType = :fileType"),
    @NamedQuery(name = "CosEvaluationRecord.findByStatus", query = "SELECT c FROM CosEvaluationRecord c WHERE c.status = :status"),
    @NamedQuery(name = "CosEvaluationRecord.findByIsActive", query = "SELECT c FROM CosEvaluationRecord c WHERE c.isActive = :isActive"),
    @NamedQuery(name = "CosEvaluationRecord.findByProcessId", query = "SELECT c FROM CosEvaluationRecord c WHERE c.processId = :processId"),
    @NamedQuery(name = "CosEvaluationRecord.findByAttachId", query = "SELECT c FROM CosEvaluationRecord c WHERE c.attachId = :attachId")})
public class CosEvaluationRecord implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_EVALUATION_RECORD_SEQ", sequenceName = "COS_EVALUATION_RECORD_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_EVALUATION_RECORD_SEQ")
    @Column(name = "EVALUATION_RECORD_ID")
    private Long evaluationRecordId;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Size(max = 255)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 500)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 500)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 2000)
    @Column(name = "MAIN_CONTENT")
    private String mainContent;
    @Column(name = "LEGAL")
    private Long legal;
    @Size(max = 2000)
    @Column(name = "LEGAL_CONTENT")
    private String legalContent;
    @Column(name = "USER_ID")
    private Long userId;
    @Size(max = 250)
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "PROCESS_ID")
    private Long processId;

    @Column(name = "ATTACH_ID")
    private Long attachId;

    @Column(name = "CRITERIA")
    private Long criteria;
    @Size(max = 2000)
    @Column(name = "CRITERIA_CONTENT")
    private String criteriaContent;

    @Column(name = "MECHANISM")
    private Long mechanism;
    @Size(max = 2000)
    @Column(name = "MECHANISM_CONTENT")
    private String mechanismContent;

    @Column(name = "NAMEPRODUCT")
    private Long nameproduct;
    @Size(max = 2000)
    @Column(name = "NAMEPRODUCT_CONTENT")
    private String nameproductContent;

    public CosEvaluationRecord() {
    }

    public CosEvaluationRecord(Long evaluationRecordId) {
        this.evaluationRecordId = evaluationRecordId;
    }

    public Long getEvaluationRecordId() {
        return evaluationRecordId;
    }

    public void setEvaluationRecordId(Long evaluationRecordId) {
        this.evaluationRecordId = evaluationRecordId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getMainContent() {
        return mainContent;
    }

    public void setMainContent(String mainContent) {
        this.mainContent = mainContent;
    }

    public Long getLegal() {
        return legal;
    }

    public void setLegal(Long legal) {
        this.legal = legal;
    }

    public String getLegalContent() {
        return legalContent;
    }

    public void setLegalContent(String legalContent) {
        this.legalContent = legalContent;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    public Long getAttachId() {
        return attachId;
    }

    public void setAttachId(Long attachId) {
        this.attachId = attachId;
    }

    public Long getCriteria() {
        return criteria;
    }

    public void setCriteria(Long criteria) {
        this.criteria = criteria;
    }

    public String getCriteriaContent() {
        return criteriaContent;
    }

    public void setCriteriaContent(String criteriaContent) {
        this.criteriaContent = criteriaContent;
    }

    public Long getMechanism() {
        return mechanism;
    }

    public void setMechanism(Long mechanism) {
        this.mechanism = mechanism;
    }

    public String getMechanismContent() {
        return mechanismContent;
    }

    public void setMechanismContent(String mechanismContent) {
        this.mechanismContent = mechanismContent;
    }

    public Long getNameproduct() {
        return nameproduct;
    }

    public void setNameproduct(Long nameproduct) {
        this.nameproduct = nameproduct;
    }

    public String getNameproductContent() {
        return nameproductContent;
    }

    public void setNameproductContent(String nameproductContent) {
        this.nameproductContent = nameproductContent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaluationRecordId != null ? evaluationRecordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosEvaluationRecord)) {
            return false;
        }
        CosEvaluationRecord other = (CosEvaluationRecord) object;
        if ((this.evaluationRecordId == null && other.evaluationRecordId != null) || (this.evaluationRecordId != null && !this.evaluationRecordId.equals(other.evaluationRecordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosEvaluationRecord[ evaluationRecordId=" + evaluationRecordId + " ]";
    }

    public String getStaffEvaluationValue() {
        String staffEvaluationValue;
        if (Constants.EVALUTION.FILE_NEED_ADD.equals(status)) {
            staffEvaluationValue = "Chuyên viên KL :SDBS";
        } else if (Constants.EVALUTION.FILE_OK.equals(status)) {
            staffEvaluationValue = "Chuyên viên KL :Đạt";
        } else if (Constants.EVALUTION.FILE_NOK.equals(status)) {
            staffEvaluationValue = "Chuyên viên KL :Không đạt";
        } else {
            staffEvaluationValue = "Chuyên viên KL :Không đạt";
        }
        return staffEvaluationValue;
    }

    public String getStaffEvaluationContent() {
        return mainContent;
    }

    public void copy(CosEvaluationRecord obj) {
        status = obj.getStatus();
        createDate = obj.getCreateDate();
        businessName = obj.getBusinessName();
        businessAddress = obj.getBusinessAddress();
        productName = obj.getProductName();
        mainContent = obj.getMainContent();
        criteria = obj.getCriteria();
        criteriaContent = obj.getCriteriaContent();
        mechanism = obj.getMechanism();
        mechanismContent = obj.getMechanismContent();
        nameproduct = obj.getNameproduct();
        nameproductContent = obj.getNameproductContent();
        legal = obj.getLegal();
        legalContent = obj.getLegalContent();
        fileId = obj.getFileId();
        fileType = obj.getFileType();
        processId = obj.getProcessId();

    }

}
