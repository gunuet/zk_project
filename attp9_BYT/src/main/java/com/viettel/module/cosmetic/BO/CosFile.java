/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "COS_FILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosFile.findAll", query = "SELECT c FROM CosFile c"),
    @NamedQuery(name = "CosFile.findByCosFileId", query = "SELECT c FROM CosFile c WHERE c.cosFileId = :cosFileId"),
    @NamedQuery(name = "CosFile.findByExtensionNo", query = "SELECT c FROM CosFile c WHERE c.extensionNo = :extensionNo"),
    @NamedQuery(name = "CosFile.findByFileId", query = "SELECT c FROM CosFile c WHERE c.fileId = :fileId"),
    @NamedQuery(name = "CosFile.findByDocumentTypeCode", query = "SELECT c FROM CosFile c WHERE c.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "CosFile.findByCosmeticNo", query = "SELECT c FROM CosFile c WHERE c.cosmeticNo = :cosmeticNo"),
    @NamedQuery(name = "CosFile.findByBrandName", query = "SELECT c FROM CosFile c WHERE c.brandName = :brandName"),
    @NamedQuery(name = "CosFile.findByProductName", query = "SELECT c FROM CosFile c WHERE c.productName = :productName"),
    @NamedQuery(name = "CosFile.findByProductType", query = "SELECT c FROM CosFile c WHERE c.productType = :productType"),
    @NamedQuery(name = "CosFile.findByOtherProductType", query = "SELECT c FROM CosFile c WHERE c.otherProductType = :otherProductType"),
    @NamedQuery(name = "CosFile.findByIntendedUse", query = "SELECT c FROM CosFile c WHERE c.intendedUse = :intendedUse"),
    @NamedQuery(name = "CosFile.findByProductPresentation", query = "SELECT c FROM CosFile c WHERE c.productPresentation = :productPresentation"),
    @NamedQuery(name = "CosFile.findByDistributorName", query = "SELECT c FROM CosFile c WHERE c.distributorName = :distributorName"),
    @NamedQuery(name = "CosFile.findByDistributorAddress", query = "SELECT c FROM CosFile c WHERE c.distributorAddress = :distributorAddress"),
    @NamedQuery(name = "CosFile.findByDistributorPhone", query = "SELECT c FROM CosFile c WHERE c.distributorPhone = :distributorPhone"),
    @NamedQuery(name = "CosFile.findByDistributorFax", query = "SELECT c FROM CosFile c WHERE c.distributorFax = :distributorFax"),
    @NamedQuery(name = "CosFile.findByDistributorRegisNumber", query = "SELECT c FROM CosFile c WHERE c.distributorRegisNumber = :distributorRegisNumber"),
    @NamedQuery(name = "CosFile.findByDistributePersonName", query = "SELECT c FROM CosFile c WHERE c.distributePersonName = :distributePersonName"),
    @NamedQuery(name = "CosFile.findByDistributePersonPhone", query = "SELECT c FROM CosFile c WHERE c.distributePersonPhone = :distributePersonPhone"),
    @NamedQuery(name = "CosFile.findByDistributePersonPosition", query = "SELECT c FROM CosFile c WHERE c.distributePersonPosition = :distributePersonPosition"),
    @NamedQuery(name = "CosFile.findByImporterName", query = "SELECT c FROM CosFile c WHERE c.importerName = :importerName"),
    @NamedQuery(name = "CosFile.findByImporterAddress", query = "SELECT c FROM CosFile c WHERE c.importerAddress = :importerAddress"),
    @NamedQuery(name = "CosFile.findByImporterPhone", query = "SELECT c FROM CosFile c WHERE c.importerPhone = :importerPhone"),
    @NamedQuery(name = "CosFile.findByImporterFax", query = "SELECT c FROM CosFile c WHERE c.importerFax = :importerFax"),
    @NamedQuery(name = "CosFile.findByTested", query = "SELECT c FROM CosFile c WHERE c.tested = :tested"),
    @NamedQuery(name = "CosFile.findByRoled", query = "SELECT c FROM CosFile c WHERE c.roled = :roled"),
    @NamedQuery(name = "CosFile.findByCirculatingNo", query = "SELECT c FROM CosFile c WHERE c.circulatingNo = :circulatingNo"),
    @NamedQuery(name = "CosFile.findBySignPlace", query = "SELECT c FROM CosFile c WHERE c.signPlace = :signPlace"),
    @NamedQuery(name = "CosFile.findBySignDate", query = "SELECT c FROM CosFile c WHERE c.signDate = :signDate"),
    @NamedQuery(name = "CosFile.findBySignName", query = "SELECT c FROM CosFile c WHERE c.signName = :signName"),
    @NamedQuery(name = "CosFile.findByDateIssue", query = "SELECT c FROM CosFile c WHERE c.dateIssue = :dateIssue"),
    @NamedQuery(name = "CosFile.findByDateEffect", query = "SELECT c FROM CosFile c WHERE c.dateEffect = :dateEffect"),
    @NamedQuery(name = "CosFile.findByContents", query = "SELECT c FROM CosFile c WHERE c.contents = :contents"),
    @NamedQuery(name = "CosFile.findByAttachmentInfos", query = "SELECT c FROM CosFile c WHERE c.attachmentInfos = :attachmentInfos")})
public class CosFile implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_FILE_SEQ", sequenceName = "COS_FILE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_FILE_SEQ")
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    @Size(max = 31)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "EXTENSION_NO")
    private Long extensionNo;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Size(max = 31)
    @Column(name = "COSMETIC_NO")
    private String cosmeticNo;
    @Size(max = 255)
    @Column(name = "BRAND_NAME")
    private String brandName;
    @Size(max = 500)
    @Column(name = "LIST_VARIANT_OR_SHADE")
    private String listVariantOrShade;
    @Size(max = 255)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 255)
    @Column(name = "PRODUCT_TYPE")
    private String productType;
    @Size(max = 255)
    @Column(name = "OTHER_PRODUCT_TYPE")
    private String otherProductType;
    @Size(max = 25)
    @Column(name = "INTENDED_USE")
    private String intendedUse;
    @Size(max = 255)
    @Column(name = "PRODUCT_PRESENTATION")
    private String productPresentation;
    @Size(max = 255)
    @Column(name = "OTHER_PRODUCT_PRESENTATION")
    private String otherProductPresentation;
    @Size(max = 255)
    @Column(name = "DISTRIBUTOR_NAME")
    private String distributorName;
    @Size(max = 500)
    @Column(name = "DISTRIBUTOR_ADDRESS")
    private String distributorAddress;
    @Size(max = 31)
    @Column(name = "DISTRIBUTOR_PHONE")
    private String distributorPhone;
    @Size(max = 31)
    @Column(name = "DISTRIBUTOR_FAX")
    private String distributorFax;
    @Size(max = 31)
    @Column(name = "DISTRIBUTOR_REGIS_NUMBER")
    private String distributorRegisNumber;
    @Column(name = "DISTRIBUTE_PERSON_NAME")
    private String distributePersonName;
    @Column(name = "DISTRIBUTE_PERSON_PHONE")
    private String distributePersonPhone;
    @Column(name = "DISTRIBUTE_PERSON_EMAIL")
    private String distributePersonEmail;
    @Size(max = 31)
    @Column(name = "DISTRIBUTE_PERSON_POSITION")
    private String distributePersonPosition;
    @Size(max = 255)
    @Column(name = "IMPORTER_NAME")
    private String importerName;
    @Size(max = 255)
    @Column(name = "IMPORTER_ADDRESS")
    private String importerAddress;
    @Size(max = 31)
    @Column(name = "IMPORTER_PHONE")
    private String importerPhone;
    @Size(max = 31)
    @Column(name = "IMPORTER_FAX")
    private String importerFax;
    @Column(name = "TESTED")
    private Long tested;
    @Column(name = "ROLED")
    private Long roled;
    @Size(max = 10)
    @Column(name = "CIRCULATING_NO")
    private String circulatingNo;
    @Size(max = 255)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    @Size(max = 255)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Column(name = "DATE_ISSUE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateIssue;
    @Size(max = 255)
    @Column(name = "DATE_EFFECT")
    private String dateEffect;
    @Size(max = 500)
    @Column(name = "CONTENTS")
    private String contents;
    @Size(max = 255)
    @Column(name = "ATTACHMENT_INFOS")
    private String attachmentInfos;
    @Column(name = "INGRE_CONFIRM1")
    private Long ingreConfirm1;
    @Column(name = "INGRE_CONFIRM2")
    private Long ingreConfirm2;

    public CosFile() {
    }

    public CosFile(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getExtensionNo() {
        return extensionNo;
    }

    public void setExtensionNo(Long extensionNo) {
        this.extensionNo = extensionNo;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getCosmeticNo() {
        return cosmeticNo;
    }

    public void setCosmeticNo(String cosmeticNo) {
        this.cosmeticNo = cosmeticNo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getOtherProductType() {
        return otherProductType;
    }

    public void setOtherProductType(String otherProductType) {
        this.otherProductType = otherProductType;
    }

    public String getIntendedUse() {
        return intendedUse;
    }

    public void setIntendedUse(String intendedUse) {
        this.intendedUse = intendedUse;
    }

    public String getProductPresentation() {
        return productPresentation;
    }

    public void setProductPresentation(String productPresentation) {
        this.productPresentation = productPresentation;
    }

    public String getOtherProductPresentation() {
        return otherProductPresentation;
    }

    public void setOtherProductPresentation(String otherProductPresentation) {
        this.otherProductPresentation = otherProductPresentation;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorPhone() {
        return distributorPhone;
    }

    public void setDistributorPhone(String distributorPhone) {
        this.distributorPhone = distributorPhone;
    }

    public String getDistributorFax() {
        return distributorFax;
    }

    public void setDistributorFax(String distributorFax) {
        this.distributorFax = distributorFax;
    }

    public String getDistributorRegisNumber() {
        return distributorRegisNumber;
    }

    public void setDistributorRegisNumber(String distributorRegisNumber) {
        this.distributorRegisNumber = distributorRegisNumber;
    }

    public String getDistributePersonName() {
        return distributePersonName;
    }

    public void setDistributePersonName(String distributePersonName) {
        this.distributePersonName = distributePersonName;
    }

    public String getDistributePersonPhone() {
        return distributePersonPhone;
    }

    public void setDistributePersonPhone(String distributePersonPhone) {
        this.distributePersonPhone = distributePersonPhone;
    }

    public String getDistributePersonEmail() {
        return distributePersonEmail;
    }

    public void setDistributePersonEmail(String distributePersonEmail) {
        this.distributePersonEmail = distributePersonEmail;
    }

    public String getDistributePersonPosition() {
        return distributePersonPosition;
    }

    public void setDistributePersonPosition(String distributePersonPosition) {
        this.distributePersonPosition = distributePersonPosition;
    }

    public String getImporterName() {
        return importerName;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public void setImporterAddress(String importerAddress) {
        this.importerAddress = importerAddress;
    }

    public String getImporterPhone() {
        return importerPhone;
    }

    public void setImporterPhone(String importerPhone) {
        this.importerPhone = importerPhone;
    }

    public String getImporterFax() {
        return importerFax;
    }

    public void setImporterFax(String importerFax) {
        this.importerFax = importerFax;
    }

    public Long getTested() {
        return tested;
    }

    public void setTested(Long tested) {
        this.tested = tested;
    }

    public Long getRoled() {
        return roled;
    }

    public void setRoled(Long roled) {
        this.roled = roled;
    }

    public String getCirculatingNo() {
        return circulatingNo;
    }

    public void setCirculatingNo(String circulatingNo) {
        this.circulatingNo = circulatingNo;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public Date getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    public String getDateEffect() {
        return dateEffect;
    }

    public void setDateEffect(String dateEffect) {
        this.dateEffect = dateEffect;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getAttachmentInfos() {
        return attachmentInfos;
    }

    public void setAttachmentInfos(String attachmentInfos) {
        this.attachmentInfos = attachmentInfos;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getIngreConfirm1() {
        return ingreConfirm1;
    }

    public void setIngreConfirm1(Long ingreConfirm1) {
        this.ingreConfirm1 = ingreConfirm1;
    }

    public Long getIngreConfirm2() {
        return ingreConfirm2;
    }

    public void setIngreConfirm2(Long ingreConfirm2) {
        this.ingreConfirm2 = ingreConfirm2;
    }

    public String getListVariantOrShade() {
        return listVariantOrShade;
    }

    public void setListVariantOrShade(String listVariantOrShade) {
        this.listVariantOrShade = listVariantOrShade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cosFileId != null ? cosFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosFile)) {
            return false;
        }
        CosFile other = (CosFile) object;
        if ((this.cosFileId == null && other.cosFileId != null) || (this.cosFileId != null && !this.cosFileId.equals(other.cosFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosFile[ cosFileId=" + cosFileId + " ]";
    }
}
