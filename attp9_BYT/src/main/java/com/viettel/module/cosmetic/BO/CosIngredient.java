/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "COS_INGREDIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosIngredient.findAll", query = "SELECT c FROM CosIngredient c"),
    @NamedQuery(name = "CosIngredient.findByIngredientId", query = "SELECT c FROM CosIngredient c WHERE c.ingredientId = :ingredientId"),
    @NamedQuery(name = "CosIngredient.findByCode", query = "SELECT c FROM CosIngredient c WHERE c.code = :code"),
    @NamedQuery(name = "CosIngredient.findByName", query = "SELECT c FROM CosIngredient c WHERE c.name = :name"),
    @NamedQuery(name = "CosIngredient.findByIsActive", query = "SELECT c FROM CosIngredient c WHERE c.isActive = :isActive")})
public class CosIngredient implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_INGREDIENT_SEQ", sequenceName = "COS_INGREDIENT_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_INGREDIENT_SEQ")
    @Column(name = "INGREDIENT_ID")
    private Long ingredientId;
    @Size(max = 10)
    @Column(name = "CODE")
    private String code;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Column(name = "IS_ACTIVE")
    private Long isActive;

    public CosIngredient() {
    }

    public CosIngredient(Long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ingredientId != null ? ingredientId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosIngredient)) {
            return false;
        }
        CosIngredient other = (CosIngredient) object;
        if ((this.ingredientId == null && other.ingredientId != null) || (this.ingredientId != null && !this.ingredientId.equals(other.ingredientId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosIngredient[ ingredientId=" + ingredientId + " ]";
    }
    
}
