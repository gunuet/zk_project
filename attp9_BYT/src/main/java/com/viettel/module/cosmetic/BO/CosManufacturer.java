/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "COS_MANUFACTURER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosManufacturer.findAll", query = "SELECT c FROM CosManufacturer c"),
    @NamedQuery(name = "CosManufacturer.findByManufacturerId", query = "SELECT c FROM CosManufacturer c WHERE c.manufacturerId = :manufacturerId"),
    @NamedQuery(name = "CosManufacturer.findByName", query = "SELECT c FROM CosManufacturer c WHERE c.name = :name"),
    @NamedQuery(name = "CosManufacturer.findByAddress", query = "SELECT c FROM CosManufacturer c WHERE c.address = :address"),
    @NamedQuery(name = "CosManufacturer.findByPhone", query = "SELECT c FROM CosManufacturer c WHERE c.phone = :phone"),
    @NamedQuery(name = "CosManufacturer.findByFax", query = "SELECT c FROM CosManufacturer c WHERE c.fax = :fax"),
    @NamedQuery(name = "CosManufacturer.findByCosFileId", query = "SELECT c FROM CosManufacturer c WHERE c.cosFileId = :cosFileId")})
public class CosManufacturer implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_MANUFACTURER_SEQ", sequenceName = "COS_MANUFACTURER_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_MANUFACTURER_SEQ")
    @Column(name = "MANUFACTURER_ID")
    private Long manufacturerId;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Size(max = 500)
    @Column(name = "ADDRESS")
    private String address;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "PHONE")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "FAX")
    private String fax;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    
    @Column(name = "IS_ACTIVE")
    private Long isActive;

    public CosManufacturer() {
    }

    public CosManufacturer(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public CosManufacturer(Long manufacturerId, Long cosFileId) {
        this.manufacturerId = manufacturerId;
        this.cosFileId = cosFileId;
    }

    public Long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (manufacturerId != null ? manufacturerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosManufacturer)) {
            return false;
        }
        CosManufacturer other = (CosManufacturer) object;
        if ((this.manufacturerId == null && other.manufacturerId != null) || (this.manufacturerId != null && !this.manufacturerId.equals(other.manufacturerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosManufacturer[ manufacturerId=" + manufacturerId + " ]";
    }
}
