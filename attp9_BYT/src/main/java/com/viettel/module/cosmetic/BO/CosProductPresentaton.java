/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "COS_PRODUCT_PRESENTATON")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosProductPresentaton.findAll", query = "SELECT c FROM CosProductPresentaton c"),
    @NamedQuery(name = "CosProductPresentaton.findByProductPresentationId", query = "SELECT c FROM CosProductPresentaton c WHERE c.productPresentationId = :productPresentationId"),
    @NamedQuery(name = "CosProductPresentaton.findByNameVi", query = "SELECT c FROM CosProductPresentaton c WHERE c.nameVi = :nameVi"),
    @NamedQuery(name = "CosProductPresentaton.findByNameEn", query = "SELECT c FROM CosProductPresentaton c WHERE c.nameEn = :nameEn"),
    @NamedQuery(name = "CosProductPresentaton.findByIsActive", query = "SELECT c FROM CosProductPresentaton c WHERE c.isActive = :isActive")})
public class CosProductPresentaton implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_PRODUCT_PRESENTATION_SEQ", sequenceName = "COS_PRODUCT_PRESENTATION_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_PRODUCT_PRESENTATION_SEQ")
    @Column(name = "PRODUCT_PRESENTATION_ID")
    private Long productPresentationId;
    @Size(max = 255)
    @Column(name = "NAME_VI")
    private String nameVi;
    @Size(max = 255)
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "IS_DIFFERENT")
    private Long isDifferent;
    @Transient
    private Boolean check = false;
    @Transient String nameFull;

    public CosProductPresentaton() {
    }

    public CosProductPresentaton(Long productPresentationId) {
        this.productPresentationId = productPresentationId;
    }

    public Long getProductPresentationId() {
        return productPresentationId;
    }

    public void setProductPresentationId(Long productPresentationId) {
        this.productPresentationId = productPresentationId;
    }

    public String getNameVi() {
        return nameVi;
    }

    public void setNameVi(String nameVi) {
        this.nameVi = nameVi;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getIsDifferent() {
        return isDifferent;
    }

    public void setIsDifferent(Long isDifferent) {
        this.isDifferent = isDifferent;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public String getNameFull() {
        return nameFull;
    }

    public void setNameFull(String nameFull) {
        this.nameFull = nameFull;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productPresentationId != null ? productPresentationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosProductPresentaton)) {
            return false;
        }
        CosProductPresentaton other = (CosProductPresentaton) object;
        if ((this.productPresentationId == null && other.productPresentationId != null) || (this.productPresentationId != null && !this.productPresentationId.equals(other.productPresentationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosProductPresentaton[ productPresentationId=" + productPresentationId + " ]";
    }
}
