/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "COS_PRODUCT_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosProductType.findAll", query = "SELECT c FROM CosProductType c"),
    @NamedQuery(name = "CosProductType.findByProductTypeId", query = "SELECT c FROM CosProductType c WHERE c.productTypeId = :productTypeId"),
    @NamedQuery(name = "CosProductType.findByName", query = "SELECT c FROM CosProductType c WHERE c.name = :name"),
    @NamedQuery(name = "CosProductType.findByIsActive", query = "SELECT c FROM CosProductType c WHERE c.isActive = :isActive")})
public class CosProductType implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_PRODUCT_TYPE_SEQ", sequenceName = "COS_PRODUCT_TYPE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_PRODUCT_TYPE_SEQ")
    @Column(name = "PRODUCT_TYPE_ID")
    private Long productTypeId;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "ORD")
    private Long ord;
    @Transient
    private Boolean check=false;
    @Transient
    private String fullName;

    public CosProductType() {
    }

    public CosProductType(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getOrd() {
        return ord;
    }

    public void setOrd(Long ord) {
        this.ord = ord;
    }

    public Boolean getCheck() {
        if(check == null){
            check = false;
        }
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productTypeId != null ? productTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosProductType)) {
            return false;
        }
        CosProductType other = (CosProductType) object;
        if ((this.productTypeId == null && other.productTypeId != null) || (this.productTypeId != null && !this.productTypeId.equals(other.productTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosProductType[ productTypeId=" + productTypeId + " ]";
    }

}
