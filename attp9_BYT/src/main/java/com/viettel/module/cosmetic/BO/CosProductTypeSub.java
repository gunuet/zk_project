/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "COS_PRODUCT_TYPE_SUB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosProductTypeSub.findAll", query = "SELECT c FROM CosProductTypeSub c"),
    @NamedQuery(name = "CosProductTypeSub.findByProductTypeSubId", query = "SELECT c FROM CosProductTypeSub c WHERE c.productTypeSubId = :productTypeSubId"),
    @NamedQuery(name = "CosProductTypeSub.findByNameVi", query = "SELECT c FROM CosProductTypeSub c WHERE c.nameVi = :nameVi"),
    @NamedQuery(name = "CosProductTypeSub.findByNameEn", query = "SELECT c FROM CosProductTypeSub c WHERE c.nameEn = :nameEn"),
    @NamedQuery(name = "CosProductTypeSub.findByIsActive", query = "SELECT c FROM CosProductTypeSub c WHERE c.isActive = :isActive"),
    @NamedQuery(name = "CosProductTypeSub.findByProductTypeId", query = "SELECT c FROM CosProductTypeSub c WHERE c.productTypeId = :productTypeId")})
public class CosProductTypeSub implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_PRODUCT_TYPE_SUB_SEQ", sequenceName = "COS_PRODUCT_TYPE_SUB_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_PRODUCT_TYPE_SUB_SEQ")
    @Column(name = "PRODUCT_TYPE_SUB_ID")
    private Long productTypeSubId;
    @Size(max = 255)
    @Column(name = "NAME_VI")
    private String nameVi;
    @Size(max = 500)
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "ORD")
    private Long ord;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRODUCT_TYPE_ID")
    private Long productTypeId;

    public CosProductTypeSub() {
    }

    public CosProductTypeSub(Long productTypeSubId) {
        this.productTypeSubId = productTypeSubId;
    }

    public CosProductTypeSub(Long productTypeSubId, Long productTypeId) {
        this.productTypeSubId = productTypeSubId;
        this.productTypeId = productTypeId;
    }

    public Long getProductTypeSubId() {
        return productTypeSubId;
    }

    public void setProductTypeSubId(Long productTypeSubId) {
        this.productTypeSubId = productTypeSubId;
    }

    public String getNameVi() {
        return nameVi;
    }

    public void setNameVi(String nameVi) {
        this.nameVi = nameVi;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Long getOrd() {
        return ord;
    }

    public void setOrd(Long ord) {
        this.ord = ord;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productTypeSubId != null ? productTypeSubId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosProductTypeSub)) {
            return false;
        }
        CosProductTypeSub other = (CosProductTypeSub) object;
        if ((this.productTypeSubId == null && other.productTypeSubId != null) || (this.productTypeSubId != null && !this.productTypeSubId.equals(other.productTypeSubId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosProductTypeSub[ productTypeSubId=" + productTypeSubId + " ]";
    }
    
}
