/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nguoibay
 */
@Entity
@Table(name = "COS_REJECT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CosReject.findAll", query = "SELECT c FROM CosReject c"),
    @NamedQuery(name = "CosReject.findByrejectId", query = "SELECT c FROM CosReject c WHERE c.rejectId = :rejectId"),
    @NamedQuery(name = "CosReject.findByFileId", query = "SELECT c FROM CosReject c WHERE c.fileId = :fileId"),
    @NamedQuery(name = "CosReject.findByReceiveNo", query = "SELECT c FROM CosReject c WHERE c.receiveNo = :receiveNo"),
    @NamedQuery(name = "CosReject.findByReceiveDate", query = "SELECT c FROM CosReject c WHERE c.receiveDate = :receiveDate"),
    @NamedQuery(name = "CosReject.findByReceiveDeptName", query = "SELECT c FROM CosReject c WHERE c.receiveDeptName = :receiveDeptName"),
    @NamedQuery(name = "CosReject.findBySignerName", query = "SELECT c FROM CosReject c WHERE c.signerName = :signerName"),
    @NamedQuery(name = "CosReject.findBySignDate", query = "SELECT c FROM CosReject c WHERE c.signDate = :signDate"),
    @NamedQuery(name = "CosReject.findByBusinessName", query = "SELECT c FROM CosReject c WHERE c.businessName = :businessName"),
    @NamedQuery(name = "CosReject.findByBusinessId", query = "SELECT c FROM CosReject c WHERE c.businessId = :businessId"),
    @NamedQuery(name = "CosReject.findByTelephone", query = "SELECT c FROM CosReject c WHERE c.telephone = :telephone"),
    @NamedQuery(name = "CosReject.findByFax", query = "SELECT c FROM CosReject c WHERE c.fax = :fax"),
    @NamedQuery(name = "CosReject.findByEmail", query = "SELECT c FROM CosReject c WHERE c.email = :email"),
    @NamedQuery(name = "CosReject.findByProductName", query = "SELECT c FROM CosReject c WHERE c.productName = :productName"),
    @NamedQuery(name = "CosReject.findByIsActive", query = "SELECT c FROM CosReject c WHERE c.isActive = :isActive"),
    @NamedQuery(name = "CosReject.findByAttachId", query = "SELECT c FROM CosReject c WHERE c.attachId = :attachId"),
    @NamedQuery(name = "CosReject.findByStatus", query = "SELECT c FROM CosReject c WHERE c.status = :status")})
public class CosReject implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "COS_REJECT_SEQ", sequenceName = "COS_REJECT_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COS_REJECT_SEQ")
    @Column(name = "REJECT_ID")
    private Long rejectId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 31)
    @Column(name = "RECEIVE_NO")
    private String receiveNo;
    @Column(name = "RECEIVE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date receiveDate;
    @Size(max = 500)
    @Column(name = "RECEIVE_DEPT_NAME")
    private String receiveDeptName;
    @Size(max = 255)
    @Column(name = "SIGNER_NAME")
    private String signerName;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    @Size(max = 255)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Column(name = "BUSINESS_ID")
    private Long businessId;
    @Size(max = 31)
    @Column(name = "TELEPHONE")
    private String telephone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "FAX")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 31)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 500)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "ATTACH_ID")
    private Long attachId;
    @Column(name = "STATUS")
    private Long status;

    public CosReject() {
    }

    public Long getRejectId() {
        return rejectId;
    }

    public void setRejectId(Long rejectId) {
        this.rejectId = rejectId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getReceiveDeptName() {
        return receiveDeptName;
    }

    public void setReceiveDeptName(String receiveDeptName) {
        this.receiveDeptName = receiveDeptName;
    }

    public String getSignerName() {
        return signerName;
    }

    public void setSignerName(String signerName) {
        this.signerName = signerName;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getAttachId() {
        return attachId;
    }

    public void setAttachId(Long attachId) {
        this.attachId = attachId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rejectId != null ? rejectId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CosReject)) {
            return false;
        }
        CosReject other = (CosReject) object;
        if ((this.rejectId == null && other.rejectId != null) || (this.rejectId != null && !this.rejectId.equals(other.rejectId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.CosReject[ rejectId=" + rejectId + " ]";
    }
}
