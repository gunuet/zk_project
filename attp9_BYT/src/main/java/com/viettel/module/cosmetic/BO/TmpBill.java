/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GPCP_BINHNT53
 */
@Entity
@Table(name = "TMP_BILL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpBill.findAll", query = "SELECT t FROM TmpBill t"),
    @NamedQuery(name = "TmpBill.findByTmpBillId", query = "SELECT t FROM TmpBill t WHERE t.tmpBillId = :tmpBillId"),
    @NamedQuery(name = "TmpBill.findByBillDate", query = "SELECT t FROM TmpBill t WHERE t.billDate = :billDate"),
    @NamedQuery(name = "TmpBill.findByBillNo", query = "SELECT t FROM TmpBill t WHERE t.billNo = :billNo"),
    @NamedQuery(name = "TmpBill.findByType", query = "SELECT t FROM TmpBill t WHERE t.type = :type"),
    @NamedQuery(name = "TmpBill.findByCode", query = "SELECT t FROM TmpBill t WHERE t.code = :code"),
    @NamedQuery(name = "TmpBill.findByProductCode", query = "SELECT t FROM TmpBill t WHERE t.productCode = :productCode"),
    @NamedQuery(name = "TmpBill.findByBusinessName", query = "SELECT t FROM TmpBill t WHERE t.businessName = :businessName"),
    @NamedQuery(name = "TmpBill.findByPrice", query = "SELECT t FROM TmpBill t WHERE t.price = :price")})
public class TmpBill implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "TMP_BILL_SEQ", sequenceName = "TMP_BILL_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TMP_BILL_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "TMP_BILL_ID")
    private Long tmpBillId;
    @Column(name = "BILL_DATE")
    @Temporal(TemporalType.DATE)
    private Date billDate;
    @Size(max = 300)
    @Column(name = "TYPE")
    private String type;
    @Size(max = 300)
    @Column(name = "CODE")
    private String code;
    @Size(max = 300)
    @Column(name = "PRODUCT_CODE")
    private String productCode;
    @Size(max = 1000)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Column(name = "PRICE")
    private String price;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "IS_LAST_IMP")
    private Long isLastImp;
    @Column(name = "BILL_NO")
    private String billNo;

    public TmpBill() {
    }

    public TmpBill(Long tmpBillId) {
        this.tmpBillId = tmpBillId;
    }

    public Long getTmpBillId() {
        return tmpBillId;
    }

    public void setTmpBillId(Long tmpBillId) {
        this.tmpBillId = tmpBillId;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tmpBillId != null ? tmpBillId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpBill)) {
            return false;
        }
        TmpBill other = (TmpBill) object;
        if ((this.tmpBillId == null && other.tmpBillId != null) || (this.tmpBillId != null && !this.tmpBillId.equals(other.tmpBillId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.cosmetic.BO.TmpBill[ tmpBillId=" + tmpBillId + " ]";
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getIsLastImp() {
        return isLastImp;
    }

    public void setIsLastImp(Long isLastImp) {
        this.isLastImp = isLastImp;
    }
}
