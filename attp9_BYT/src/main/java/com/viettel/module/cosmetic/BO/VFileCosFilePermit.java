/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.cosmetic.BO;

import java.util.Date;

/**
 *
 * @author giangnh20
 */
public class VFileCosFilePermit {
    
    private Long cosFileId;
    private Long fileId;
    private String businessName;
    private String businessAddress;
    private String productName;
    private String receiveNo;
    private Date receiveDate;

    public VFileCosFilePermit() {
        
    }
    
    public VFileCosFilePermit(VFileCosfile cosFile, CosPermit cosPermit) {
        if (cosFile != null) {
            businessName = cosFile.getBusinessName();
            businessAddress = cosFile.getBusinessAddress();
            productName = cosFile.getProductName();
            cosFileId = cosFile.getCosFileId();
            fileId = cosFile.getFileId();
        }
        if (cosPermit != null) {
            receiveDate = cosPermit.getReceiveDate();
            receiveNo = cosPermit.getReceiveNo();
        }
    }
    
    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    
}
