/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import com.viettel.utils.Constants;
import com.viettel.utils.Constants_CKS;
import com.viettel.utils.DateTimeUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import com.viettel.utils.Constants_CKS;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "V_FILE_COSFILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VFileCosfile.findAll", query = "SELECT v FROM VFileCosfile v"),
    @NamedQuery(name = "VFileCosfile.findByCosFileId", query = "SELECT v FROM VFileCosfile v WHERE v.cosFileId = :cosFileId"),
    @NamedQuery(name = "VFileCosfile.findByExtensionNo", query = "SELECT v FROM VFileCosfile v WHERE v.extensionNo = :extensionNo"),
    @NamedQuery(name = "VFileCosfile.findByFileId", query = "SELECT v FROM VFileCosfile v WHERE v.fileId = :fileId"),
    @NamedQuery(name = "VFileCosfile.findByDocumentTypeCode", query = "SELECT v FROM VFileCosfile v WHERE v.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "VFileCosfile.findByCosmeticNo", query = "SELECT v FROM VFileCosfile v WHERE v.cosmeticNo = :cosmeticNo"),
    @NamedQuery(name = "VFileCosfile.findByBrandName", query = "SELECT v FROM VFileCosfile v WHERE v.brandName = :brandName"),
    @NamedQuery(name = "VFileCosfile.findByProductName", query = "SELECT v FROM VFileCosfile v WHERE v.productName = :productName"),
    @NamedQuery(name = "VFileCosfile.findByProductType", query = "SELECT v FROM VFileCosfile v WHERE v.productType = :productType"),
    @NamedQuery(name = "VFileCosfile.findByOtherProductType", query = "SELECT v FROM VFileCosfile v WHERE v.otherProductType = :otherProductType"),
    @NamedQuery(name = "VFileCosfile.findByIntendedUse", query = "SELECT v FROM VFileCosfile v WHERE v.intendedUse = :intendedUse"),
    @NamedQuery(name = "VFileCosfile.findByProductPresentation", query = "SELECT v FROM VFileCosfile v WHERE v.productPresentation = :productPresentation"),
    @NamedQuery(name = "VFileCosfile.findByOtherProductPresentation", query = "SELECT v FROM VFileCosfile v WHERE v.otherProductPresentation = :otherProductPresentation"),
    @NamedQuery(name = "VFileCosfile.findByDistributorName", query = "SELECT v FROM VFileCosfile v WHERE v.distributorName = :distributorName"),
    @NamedQuery(name = "VFileCosfile.findByDistributorAddress", query = "SELECT v FROM VFileCosfile v WHERE v.distributorAddress = :distributorAddress"),
    @NamedQuery(name = "VFileCosfile.findByDistributorPhone", query = "SELECT v FROM VFileCosfile v WHERE v.distributorPhone = :distributorPhone"),
    @NamedQuery(name = "VFileCosfile.findByDistributorFax", query = "SELECT v FROM VFileCosfile v WHERE v.distributorFax = :distributorFax"),
    @NamedQuery(name = "VFileCosfile.findByDistributorRegisNumber", query = "SELECT v FROM VFileCosfile v WHERE v.distributorRegisNumber = :distributorRegisNumber"),
    @NamedQuery(name = "VFileCosfile.findByDistributePersonName", query = "SELECT v FROM VFileCosfile v WHERE v.distributePersonName = :distributePersonName"),
    @NamedQuery(name = "VFileCosfile.findByDistributePersonPhone", query = "SELECT v FROM VFileCosfile v WHERE v.distributePersonPhone = :distributePersonPhone"),
    @NamedQuery(name = "VFileCosfile.findByDistributePersonEmail", query = "SELECT v FROM VFileCosfile v WHERE v.distributePersonEmail = :distributePersonEmail"),
    @NamedQuery(name = "VFileCosfile.findByDistributePersonPosition", query = "SELECT v FROM VFileCosfile v WHERE v.distributePersonPosition = :distributePersonPosition"),
    @NamedQuery(name = "VFileCosfile.findByImporterName", query = "SELECT v FROM VFileCosfile v WHERE v.importerName = :importerName"),
    @NamedQuery(name = "VFileCosfile.findByImporterAddress", query = "SELECT v FROM VFileCosfile v WHERE v.importerAddress = :importerAddress"),
    @NamedQuery(name = "VFileCosfile.findByImporterPhone", query = "SELECT v FROM VFileCosfile v WHERE v.importerPhone = :importerPhone"),
    @NamedQuery(name = "VFileCosfile.findByImporterFax", query = "SELECT v FROM VFileCosfile v WHERE v.importerFax = :importerFax"),
    @NamedQuery(name = "VFileCosfile.findByTested", query = "SELECT v FROM VFileCosfile v WHERE v.tested = :tested"),
    @NamedQuery(name = "VFileCosfile.findByRoled", query = "SELECT v FROM VFileCosfile v WHERE v.roled = :roled"),
    @NamedQuery(name = "VFileCosfile.findByCirculatingNo", query = "SELECT v FROM VFileCosfile v WHERE v.circulatingNo = :circulatingNo"),
    @NamedQuery(name = "VFileCosfile.findBySignPlace", query = "SELECT v FROM VFileCosfile v WHERE v.signPlace = :signPlace"),
    @NamedQuery(name = "VFileCosfile.findBySignDate", query = "SELECT v FROM VFileCosfile v WHERE v.signDate = :signDate"),
    @NamedQuery(name = "VFileCosfile.findBySignName", query = "SELECT v FROM VFileCosfile v WHERE v.signName = :signName"),
    @NamedQuery(name = "VFileCosfile.findByDateIssue", query = "SELECT v FROM VFileCosfile v WHERE v.dateIssue = :dateIssue"),
    @NamedQuery(name = "VFileCosfile.findByDateEffect", query = "SELECT v FROM VFileCosfile v WHERE v.dateEffect = :dateEffect"),
    @NamedQuery(name = "VFileCosfile.findByContents", query = "SELECT v FROM VFileCosfile v WHERE v.contents = :contents"),
    @NamedQuery(name = "VFileCosfile.findByAttachmentInfos", query = "SELECT v FROM VFileCosfile v WHERE v.attachmentInfos = :attachmentInfos"),
    @NamedQuery(name = "VFileCosfile.findByFileType", query = "SELECT v FROM VFileCosfile v WHERE v.fileType = :fileType"),
    @NamedQuery(name = "VFileCosfile.findByFileTypeName", query = "SELECT v FROM VFileCosfile v WHERE v.fileTypeName = :fileTypeName"),
    @NamedQuery(name = "VFileCosfile.findByFileCode", query = "SELECT v FROM VFileCosfile v WHERE v.fileCode = :fileCode"),
    @NamedQuery(name = "VFileCosfile.findByFileName", query = "SELECT v FROM VFileCosfile v WHERE v.fileName = :fileName"),
    @NamedQuery(name = "VFileCosfile.findByStatus", query = "SELECT v FROM VFileCosfile v WHERE v.status = :status"),
    @NamedQuery(name = "VFileCosfile.findByTaxCode", query = "SELECT v FROM VFileCosfile v WHERE v.taxCode = :taxCode"),
    @NamedQuery(name = "VFileCosfile.findByBusinessId", query = "SELECT v FROM VFileCosfile v WHERE v.businessId = :businessId"),
    @NamedQuery(name = "VFileCosfile.findByBusinessName", query = "SELECT v FROM VFileCosfile v WHERE v.businessName = :businessName"),
    @NamedQuery(name = "VFileCosfile.findByBusinessAddress", query = "SELECT v FROM VFileCosfile v WHERE v.businessAddress = :businessAddress"),
    @NamedQuery(name = "VFileCosfile.findByBusinessPhone", query = "SELECT v FROM VFileCosfile v WHERE v.businessPhone = :businessPhone"),
    @NamedQuery(name = "VFileCosfile.findByBusinessFax", query = "SELECT v FROM VFileCosfile v WHERE v.businessFax = :businessFax"),
    @NamedQuery(name = "VFileCosfile.findByCreateDate", query = "SELECT v FROM VFileCosfile v WHERE v.createDate = :createDate"),
    @NamedQuery(name = "VFileCosfile.findByModifyDate", query = "SELECT v FROM VFileCosfile v WHERE v.modifyDate = :modifyDate"),
    @NamedQuery(name = "VFileCosfile.findByCreatorId", query = "SELECT v FROM VFileCosfile v WHERE v.creatorId = :creatorId"),
    @NamedQuery(name = "VFileCosfile.findByCreatorName", query = "SELECT v FROM VFileCosfile v WHERE v.creatorName = :creatorName"),
    @NamedQuery(name = "VFileCosfile.findByCreateDeptId", query = "SELECT v FROM VFileCosfile v WHERE v.createDeptId = :createDeptId"),
    @NamedQuery(name = "VFileCosfile.findByCreateDeptName", query = "SELECT v FROM VFileCosfile v WHERE v.createDeptName = :createDeptName"),
    @NamedQuery(name = "VFileCosfile.findByIsActive", query = "SELECT v FROM VFileCosfile v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VFileCosfile.findByVersion", query = "SELECT v FROM VFileCosfile v WHERE v.version = :version"),
    @NamedQuery(name = "VFileCosfile.findByParentFileId", query = "SELECT v FROM VFileCosfile v WHERE v.parentFileId = :parentFileId"),
    @NamedQuery(name = "VFileCosfile.findByIsTemp", query = "SELECT v FROM VFileCosfile v WHERE v.isTemp = :isTemp")})
public class VFileCosfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @NotNull
    @Id
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    @Column(name = "EXTENSION_NO")
    private Long extensionNo;
    @Basic(optional = false)
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Size(max = 31)
    @Column(name = "COSMETIC_NO")
    private String cosmeticNo;
    @Size(max = 81)
    @Column(name = "BRAND_NAME")
    private String brandName;
    @Size(max = 81)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 255)
    @Column(name = "PRODUCT_TYPE")
    private String productType;
    @Size(max = 255)
    @Column(name = "OTHER_PRODUCT_TYPE")
    private String otherProductType;
    @Size(max = 25)
    @Column(name = "INTENDED_USE")
    private String intendedUse;
    @Size(max = 255)
    @Column(name = "PRODUCT_PRESENTATION")
    private String productPresentation;
    @Size(max = 255)
    @Column(name = "OTHER_PRODUCT_PRESENTATION")
    private String otherProductPresentation;
    @Size(max = 255)
    @Column(name = "DISTRIBUTOR_NAME")
    private String distributorName;
    @Size(max = 500)
    @Column(name = "DISTRIBUTOR_ADDRESS")
    private String distributorAddress;
    @Size(max = 31)
    @Column(name = "DISTRIBUTOR_PHONE")
    private String distributorPhone;
    @Size(max = 31)
    @Column(name = "DISTRIBUTOR_FAX")
    private String distributorFax;
    @Size(max = 31)
    @Column(name = "DISTRIBUTOR_REGIS_NUMBER")
    private String distributorRegisNumber;
    @Column(name = "DISTRIBUTE_PERSON_NAME")
    private String distributePersonName;
    @Column(name = "DISTRIBUTE_PERSON_PHONE")
    private String distributePersonPhone;
    @Column(name = "DISTRIBUTE_PERSON_EMAIL")
    private String distributePersonEmail;
    @Size(max = 31)
    @Column(name = "DISTRIBUTE_PERSON_POSITION")
    private String distributePersonPosition;
    @Size(max = 255)
    @Column(name = "IMPORTER_NAME")
    private String importerName;
    @Size(max = 255)
    @Column(name = "IMPORTER_ADDRESS")
    private String importerAddress;
    @Size(max = 31)
    @Column(name = "IMPORTER_PHONE")
    private String importerPhone;
    @Size(max = 31)
    @Column(name = "IMPORTER_FAX")
    private String importerFax;
    @Column(name = "TESTED")
    private Long tested;
    @Column(name = "ROLED")
    private Long roled;
    @Size(max = 10)
    @Column(name = "CIRCULATING_NO")
    private String circulatingNo;
    @Size(max = 255)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    @Size(max = 255)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Column(name = "DATE_ISSUE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateIssue;
    @Size(max = 255)
    @Column(name = "DATE_EFFECT")
    private String dateEffect;
    @Size(max = 500)
    @Column(name = "CONTENTS")
    private String contents;
    @Size(max = 255)
    @Column(name = "ATTACHMENT_INFOS")
    private String attachmentInfos;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Size(max = 255)
    @Column(name = "FILE_TYPE_NAME")
    private String fileTypeName;
    @Size(max = 31)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 255)
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "STATUS")
    private Long status;
    @Size(max = 31)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Column(name = "BUSINESS_ID")
    private Long businessId;
    @Size(max = 255)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 500)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 31)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 31)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 255)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 255)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "VERSION")
    private Long version;
    @Column(name = "PARENT_FILE_ID")
    private Long parentFileId;
    @Column(name = "IS_TEMP")
    private Long isTemp;
    @Column(name = "INGRE_CONFIRM1")
    private Long ingreConfirm1;
    @Column(name = "INGRE_CONFIRM2")
    private Long ingreConfirm2;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "NUM_DAY_PROCESS")
    private Long numDayProcess;
    @Column(name = "DEADLINE")
    @Temporal(TemporalType.DATE)
    private Date deadline;

    public VFileCosfile() {
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getExtensionNo() {
        return extensionNo;
    }

    public void setExtensionNo(Long extensionNo) {
        this.extensionNo = extensionNo;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getCosmeticNo() {
        return cosmeticNo;
    }

    public void setCosmeticNo(String cosmeticNo) {
        this.cosmeticNo = cosmeticNo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getOtherProductType() {
        return otherProductType;
    }

    public void setOtherProductType(String otherProductType) {
        this.otherProductType = otherProductType;
    }

    public String getIntendedUse() {
        return intendedUse;
    }

    public void setIntendedUse(String intendedUse) {
        this.intendedUse = intendedUse;
    }

    public String getProductPresentation() {
        return productPresentation;
    }

    public void setProductPresentation(String productPresentation) {
        this.productPresentation = productPresentation;
    }

    public String getOtherProductPresentation() {
        return otherProductPresentation;
    }

    public void setOtherProductPresentation(String otherProductPresentation) {
        this.otherProductPresentation = otherProductPresentation;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorPhone() {
        return distributorPhone;
    }

    public void setDistributorPhone(String distributorPhone) {
        this.distributorPhone = distributorPhone;
    }

    public String getDistributorFax() {
        return distributorFax;
    }

    public void setDistributorFax(String distributorFax) {
        this.distributorFax = distributorFax;
    }

    public String getDistributorRegisNumber() {
        return distributorRegisNumber;
    }

    public void setDistributorRegisNumber(String distributorRegisNumber) {
        this.distributorRegisNumber = distributorRegisNumber;
    }

    public String getDistributePersonName() {
        return distributePersonName;
    }

    public void setDistributePersonName(String distributePersonName) {
        this.distributePersonName = distributePersonName;
    }

    public String getDistributePersonPhone() {
        return distributePersonPhone;
    }

    public void setDistributePersonPhone(String distributePersonPhone) {
        this.distributePersonPhone = distributePersonPhone;
    }

    public String getDistributePersonEmail() {
        return distributePersonEmail;
    }

    public void setDistributePersonEmail(String distributePersonEmail) {
        this.distributePersonEmail = distributePersonEmail;
    }

    public String getDistributePersonPosition() {
        return distributePersonPosition;
    }

    public void setDistributePersonPosition(String distributePersonPosition) {
        this.distributePersonPosition = distributePersonPosition;
    }

    public String getImporterName() {
        return importerName;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public void setImporterAddress(String importerAddress) {
        this.importerAddress = importerAddress;
    }

    public String getImporterPhone() {
        return importerPhone;
    }

    public void setImporterPhone(String importerPhone) {
        this.importerPhone = importerPhone;
    }

    public String getImporterFax() {
        return importerFax;
    }

    public void setImporterFax(String importerFax) {
        this.importerFax = importerFax;
    }

    public Long getTested() {
        return tested;
    }

    public void setTested(Long tested) {
        this.tested = tested;
    }

    public Long getRoled() {
        return roled;
    }

    public void setRoled(Long roled) {
        this.roled = roled;
    }

    public String getCirculatingNo() {
        return circulatingNo;
    }

    public void setCirculatingNo(String circulatingNo) {
        this.circulatingNo = circulatingNo;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public Date getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    public String getDateEffect() {
        return dateEffect;
    }

    public void setDateEffect(String dateEffect) {
        this.dateEffect = dateEffect;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getAttachmentInfos() {
        return attachmentInfos;
    }

    public void setAttachmentInfos(String attachmentInfos) {
        this.attachmentInfos = attachmentInfos;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getParentFileId() {
        return parentFileId;
    }

    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public Long getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(Long isTemp) {
        this.isTemp = isTemp;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public String getSignDateStr() {
        if (signDate == null) {
            return null;
        }
        return DateTimeUtils.convertDateToString(signDate);
    }

    public String getDateIssueStr() {
        if (dateIssue == null) {
            return null;
        }
        return DateTimeUtils.convertDateToString(dateIssue);
    }

    public Long getIngreConfirm1() {
        return ingreConfirm1;
    }

    public void setIngreConfirm1(Long ingreConfirm1) {
        this.ingreConfirm1 = ingreConfirm1;
    }

    public Long getIngreConfirm2() {
        return ingreConfirm2;
    }

    public void setIngreConfirm2(Long ingreConfirm2) {
        this.ingreConfirm2 = ingreConfirm2;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Long getNumDayProcess() {
        return numDayProcess;
    }

    public void setNumDayProcess(Long numDayProcess) {
        this.numDayProcess = numDayProcess;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getDocumentTypeCodeToStr() {
        String documentTypeCodeStr = "";
        if (Objects.equals(documentTypeCode, Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI)) {
            documentTypeCodeStr = Constants.COSMETIC.DOCUMENT_TYPE_CODE_TAOMOI_STR;
        } else if (Objects.equals(documentTypeCode, Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN)) {
            documentTypeCodeStr = Constants.COSMETIC.DOCUMENT_TYPE_CODE_BOSUNG_STR;
        } else if (Objects.equals(documentTypeCode, Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG)) {
            documentTypeCodeStr = Constants.COSMETIC.DOCUMENT_TYPE_CODE_GIAHAN_STR;
        }
        return documentTypeCodeStr;

    }

    public Long getDeadlineWarning() {
        Date currentDate = new Date();
        Long value = Constants.WARNING.DEADLINE_ON;
        if (deadline != null) {
            if (!deadline.after(currentDate)) {
                if(Constants_CKS.FILE_STATUS_CODE.FINISH != status
                        && Constants_CKS.FILE_STATUS_CODE.STATUS_DATHONGBAOSDBS != status){
                    value = Constants.WARNING.DEADLINE_MISS;
                }
                
            }
            
        }


        return value;
    }
}
