/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GPCP_BINHNT53
 */
@Entity
@Table(name = "V_LOOKUP_COS_PERMIT_HOMEPAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VLookupCosPermitHomepage.findAll", query = "SELECT v FROM VLookupCosPermitHomepage v"),
    @NamedQuery(name = "VLookupCosPermitHomepage.findByBusinessName", query = "SELECT v FROM VLookupCosPermitHomepage v WHERE v.businessName = :businessName"),
    @NamedQuery(name = "VLookupCosPermitHomepage.findByBusinessAddress", query = "SELECT v FROM VLookupCosPermitHomepage v WHERE v.businessAddress = :businessAddress"),
    @NamedQuery(name = "VLookupCosPermitHomepage.findByProductName", query = "SELECT v FROM VLookupCosPermitHomepage v WHERE v.productName = :productName"),
    @NamedQuery(name = "VLookupCosPermitHomepage.findByReceiveNo", query = "SELECT v FROM VLookupCosPermitHomepage v WHERE v.receiveNo = :receiveNo"),
    @NamedQuery(name = "VLookupCosPermitHomepage.findByReceiveDate", query = "SELECT v FROM VLookupCosPermitHomepage v WHERE v.receiveDate = :receiveDate")})
public class VLookupCosPermitHomepage implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 510)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 1000)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 1000)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 62)
    @Column(name = "RECEIVE_NO")
    @Id
    private String receiveNo;
    @Column(name = "RECEIVE_DATE")
    @Temporal(TemporalType.DATE)
    private Date receiveDate;
    private transient Long arrange;
    private transient Date receiveDateFrom;
    private transient Date receiveDateTo;

    public VLookupCosPermitHomepage() {
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public Long getArrange() {
        return arrange;
    }

    public void setArrange(Long arrange) {
        this.arrange = arrange;
    }

    public Date getReceiveDateFrom() {
        return receiveDateFrom;
    }

    public void setReceiveDateFrom(Date receiveDateFrom) {
        this.receiveDateFrom = receiveDateFrom;
    }

    public Date getReceiveDateTo() {
        return receiveDateTo;
    }

    public void setReceiveDateTo(Date receiveDateTo) {
        this.receiveDateTo = receiveDateTo;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

}
