/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GPCP_BINHNT53
 */
@Entity
@Table(name = "V_LOOKUP_PERMIT_HOMEPAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VLookupPermitHomepage.findAll", query = "SELECT v FROM VLookupPermitHomepage v"),
    @NamedQuery(name = "VLookupPermitHomepage.findByBusinessName", query = "SELECT v FROM VLookupPermitHomepage v WHERE v.businessName = :businessName"),
    @NamedQuery(name = "VLookupPermitHomepage.findByBusinessAddress", query = "SELECT v FROM VLookupPermitHomepage v WHERE v.businessAddress = :businessAddress"),
    @NamedQuery(name = "VLookupPermitHomepage.findByProductName", query = "SELECT v FROM VLookupPermitHomepage v WHERE v.productName = :productName"),
    @NamedQuery(name = "VLookupPermitHomepage.findByReceiveNo", query = "SELECT v FROM VLookupPermitHomepage v WHERE v.receiveNo = :receiveNo"),
    @NamedQuery(name = "VLookupPermitHomepage.findByEffectiveDate", query = "SELECT v FROM VLookupPermitHomepage v WHERE v.effectiveDate = :effectiveDate")})
public class VLookupPermitHomepage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 510)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 1000)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 1000)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 62)
    @Column(name = "RECEIVE_NO")
    @Id
    private String receiveNo;
    @Column(name = "EFFECTIVE_DATE")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    private transient Long arrange;
    private transient Date effectiveDateFrom;
    private transient Date effectiveDateTo;

    public VLookupPermitHomepage() {
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Long getArrange() {
        return arrange;
    }

    public void setArrange(Long arrange) {
        this.arrange = arrange;
    }

    public Date getEffectiveDateFrom() {
        return effectiveDateFrom;
    }

    public void setEffectiveDateFrom(Date effectiveDateFrom) {
        this.effectiveDateFrom = effectiveDateFrom;
    }

    public Date getEffectiveDateTo() {
        return effectiveDateTo;
    }

    public void setEffectiveDateTo(Date effectiveDateTo) {
        this.effectiveDateTo = effectiveDateTo;
    }

}
