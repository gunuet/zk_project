/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.cosmetic.BO;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LONG HOANG GIANG
 */
@Entity
@Table(name = "V_PRODUCT_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VProductType.findAll", query = "SELECT v FROM VProductType v"),
    @NamedQuery(name = "VProductType.findByProductTypeId", query = "SELECT v FROM VProductType v WHERE v.productTypeId = :productTypeId"),
    @NamedQuery(name = "VProductType.findByName", query = "SELECT v FROM VProductType v WHERE v.name = :name"),
    @NamedQuery(name = "VProductType.findByIsActive", query = "SELECT v FROM VProductType v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VProductType.findByOrd", query = "SELECT v FROM VProductType v WHERE v.ord = :ord"),
    @NamedQuery(name = "VProductType.findByNameVi", query = "SELECT v FROM VProductType v WHERE v.nameVi = :nameVi"),
    @NamedQuery(name = "VProductType.findByNameEn", query = "SELECT v FROM VProductType v WHERE v.nameEn = :nameEn")})
public class VProductType implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRODUCT_TYPE_ID")
    @Id
    private Long productTypeId;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "ORD")
    private Long ord;
    @Size(max = 255)
    @Column(name = "NAME_VI")
    private String nameVi;
    @Size(max = 500)
    @Column(name = "NAME_EN")
    private String nameEn;

    public VProductType() {
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getOrd() {
        return ord;
    }

    public void setOrd(Long ord) {
        this.ord = ord;
    }

    public String getNameVi() {
        return nameVi;
    }

    public void setNameVi(String nameVi) {
        this.nameVi = nameVi;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }
    
}
