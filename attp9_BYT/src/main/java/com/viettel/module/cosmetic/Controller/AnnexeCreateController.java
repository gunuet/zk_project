/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.Annexe;
import com.viettel.module.cosmetic.DAO.AnnexeDAO;
import com.viettel.utils.Constants;
import java.io.IOException;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class AnnexeCreateController extends BaseComposer {

    @Wire
    Datebox dbAllowedUntil;
    @Wire
    Textbox tbAnnexeId, txtSubstance, txtCasNumber, txtRefNo, txtFieldUse, txtMaximumAuthorized, txtLimitationRequirements, txtConditionsOfUse, txtNote;
    @Wire
    Listbox lbIsActive, lbAnnexeType;
    @Wire
    Window annexeCreateWnd;
    @Wire
    private Label lbWarning;

    public AnnexeCreateController() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        loadInfoToForm();
    }

    @Listen("onClick=#btnSave")
    public void onSave() throws IOException {
        Annexe rs = new Annexe();
        if (tbAnnexeId.getValue() != null && !tbAnnexeId.getValue().isEmpty()) {
            rs.setAnnexeId(Long.parseLong(tbAnnexeId.getValue()));
        }
        if (txtCasNumber.getValue() != null && txtCasNumber.getValue().trim().length() == 0) {
//            showNotification("", Constants.Notification.ERROR);
//            txtCasNumber.focus();
//            return;
        } else {
            rs.setCasNumber(txtCasNumber.getValue());
        }
        if (txtConditionsOfUse.getValue() != null && txtConditionsOfUse.getValue().trim().length() == 0) {
//            showNotification("", Constants.Notification.ERROR);
//            txtConditionsOfUse.focus();
//            return;
        } else {
            rs.setConditionsOfUse(txtConditionsOfUse.getValue());
        }
        if (txtFieldUse.getValue() != null && txtFieldUse.getValue().trim().length() == 0) {
//            showNotification("", Constants.Notification.ERROR);
//            txtFieldUse.focus();
//            return;
        } else {
            rs.setFieldUse(txtFieldUse.getValue());
        }
        if (txtLimitationRequirements.getValue() != null && txtLimitationRequirements.getValue().trim().length() == 0) {
//            showNotification("", Constants.Notification.ERROR);
//            txtFieldUse.focus();
//            return;
        } else {
            rs.setLimitationRequirements(txtLimitationRequirements.getValue());
        }
        if (txtMaximumAuthorized.getValue() != null && txtMaximumAuthorized.getValue().trim().length() == 0) {
//            showNotification("", Constants.Notification.ERROR);
//            txtMaximumAuthorized.focus();
//            return;
        } else {
            rs.setMaximumAuthorized(txtMaximumAuthorized.getValue());
        }
        if (txtNote.getValue() != null && txtNote.getValue().trim().length() == 0) {
//            showNotification("", Constants.Notification.ERROR);
//            txtNote.focus();
//            return;
        } else {
            rs.setNote(txtNote.getValue());
        }
        if (txtRefNo.getValue() != null && txtRefNo.getValue().trim().length() == 0) {
//            showNotification("", Constants.Notification.ERROR);
//            txtRefNo.focus();
//            return;
        } else {
            rs.setRefNo(txtRefNo.getValue());
        }
        if (txtSubstance.getValue() != null && txtSubstance.getValue().trim().length() == 0) {
//            showNotification("", Constants.Notification.ERROR);
//            txtSubstance.focus();
//            return;
        } else {
            rs.setSubstance(txtSubstance.getValue());
        }
        if (dbAllowedUntil.getValue() == null) {
//            showNotification("", Constants.Notification.ERROR);
//            txtSubstance.focus();
//            return;
        } else {
            rs.setAllowedUntil(dbAllowedUntil.getValue());
        }
        if (lbAnnexeType.getSelectedItem() != null) {
            int idx = lbAnnexeType.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setAnnexeType(Long.parseLong(lbAnnexeType.getSelectedItem().getValue().toString()));
                rs.setAnnexeTypeName(lbAnnexeType.getSelectedItem().getLabel());
            } else {
                showNotification("Loại không thể để trống", Constants.Notification.ERROR);
                lbAnnexeType.focus();
                return;
            }
        }
        if (lbIsActive.getSelectedItem() != null) {
            int idx = lbIsActive.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setIsActive(Long.parseLong(lbIsActive.getSelectedItem().getValue().toString()));
            } else {
                showNotification("Trạng thái không thể để trống", Constants.Notification.ERROR);
                lbIsActive.focus();
                return;
            }
        }
        AnnexeDAO rdhe = new AnnexeDAO();
//        if (rdhe.hasDuplicate(rs)) {
//            showNotification("Trùng dữ liệu đã tạo trên hệ thống");
//            return;
//        }
        rdhe.saveOrUpdate(rs);
        showNotification("Lưu thành công", Constants.Notification.INFO);

        Window parentWnd = (Window) Path.getComponent("/annexeManageWnd");
        Events.sendEvent(new Event("onReload", parentWnd, null));
    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
    }

    public void loadInfoToForm() {
        Long id = (Long) Executions.getCurrent().getArg().get("id");
        if (id != null) {
            AnnexeDAO objhe = new AnnexeDAO();
            Annexe rs = objhe.findById(id);
            if (rs.getSubstance() != null) {
                txtSubstance.setValue(rs.getSubstance());
            }
            if (rs.getAnnexeId() != null) {
                tbAnnexeId.setValue(rs.getAnnexeId().toString());
            }
            if (rs.getCasNumber() != null) {
                txtCasNumber.setValue(rs.getCasNumber());
            }
            if (rs.getConditionsOfUse() != null) {
                txtConditionsOfUse.setValue(rs.getConditionsOfUse());
            }
            if (rs.getFieldUse() != null) {
                txtFieldUse.setValue(rs.getFieldUse());
            }
            if (rs.getLimitationRequirements() != null) {
                txtLimitationRequirements.setValue(rs.getLimitationRequirements());
            }
            if (rs.getMaximumAuthorized() != null) {
                txtMaximumAuthorized.setValue(rs.getMaximumAuthorized());
            }
            if (rs.getNote() != null) {
                txtNote.setValue(rs.getNote());
            }
            if (rs.getRefNo() != null) {
                txtRefNo.setValue(rs.getRefNo());
            }
            if (rs.getAllowedUntil() != null) {
                dbAllowedUntil.setValue(rs.getAllowedUntil());
            }

            if (rs.getAnnexeType() != null) {
                for (int i = 0; i < lbAnnexeType.getItemCount(); i++) {
                    if (rs.getAnnexeType().equals(Long.valueOf(lbAnnexeType.getItemAtIndex(i).getValue().toString()))) {
                        lbAnnexeType.setSelectedIndex(i);
                        break;
                    }
                }
            }
            if (rs.getIsActive() != null) {
                for (int i = 0; i < lbIsActive.getItemCount(); i++) {
                    if (rs.getIsActive().equals(Long.valueOf(lbIsActive.getItemAtIndex(i).getValue().toString()))) {
                        lbIsActive.setSelectedIndex(i);
                        break;
                    }
                }
            }
        } else {
            txtSubstance.setValue("");
            tbAnnexeId.setValue(null);
            txtCasNumber.setValue("");
            txtConditionsOfUse.setValue("");
            txtFieldUse.setValue("");
            txtLimitationRequirements.setValue("");
            txtMaximumAuthorized.setValue("");
            txtNote.setValue("");
            txtRefNo.setValue("");
            dbAllowedUntil.setValue(null);
        }
    }
}
