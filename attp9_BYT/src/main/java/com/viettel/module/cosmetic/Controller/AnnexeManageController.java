/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.Annexe;
import com.viettel.module.cosmetic.DAO.AnnexeDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class AnnexeManageController extends BaseComposer {

    //Control tim kiem
    Window showDeptDlg;
    @Wire
    Listbox lbAnnexe, lbAnnexeType, lbIsActive;
    @Wire
    Paging userPagingBottom;
    @Wire
    Textbox txtSubstance, txtCasNumber, txtRefNo, txtFieldUse, txtMaximumAuthorized, txtLimitationRequirements, txtConditionsOfUse, txtNote;
    Annexe searchForm;
    @Wire
    Datebox dbAllowedUntil;

    @Override
    public void doAfterCompose(Component window) {
        try {
            super.doAfterCompose(window);
            fillDataToList();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Listen("onReload=#annexeManageWnd")
    public void onReload() {
        onSearch();
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event
    ) {
        fillDataToList();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {

            userPagingBottom.setActivePage(0);
            searchForm = new Annexe();
            if (txtCasNumber.getValue() != null && !"".equals(txtCasNumber.getValue())) {
                searchForm.setCasNumber(txtCasNumber.getValue());
            }
            if (txtConditionsOfUse.getValue() != null && !"".equals(txtConditionsOfUse.getValue())) {
                searchForm.setConditionsOfUse(txtConditionsOfUse.getValue());
            }
            if (txtFieldUse.getValue() != null && !"".equals(txtFieldUse.getValue())) {
                searchForm.setFieldUse(txtFieldUse.getValue());
            }
            if (txtLimitationRequirements.getValue() != null && !"".equals(txtLimitationRequirements.getValue())) {
                searchForm.setLimitationRequirements(txtLimitationRequirements.getValue());
            }
            if (txtMaximumAuthorized.getValue() != null && !"".equals(txtMaximumAuthorized.getValue())) {
                searchForm.setMaximumAuthorized(txtMaximumAuthorized.getValue());
            }
            if (txtNote.getValue() != null && !"".equals(txtNote.getValue())) {
                searchForm.setNote(txtNote.getValue());
            }
            if (txtRefNo.getValue() != null && !"".equals(txtRefNo.getValue())) {
                searchForm.setRefNo(txtRefNo.getValue());
            }
            if (txtSubstance.getValue() != null && !"".equals(txtSubstance.getValue())) {
                searchForm.setSubstance(txtSubstance.getValue());
            }

            if (txtMaximumAuthorized.getValue() != null && !"".equals(txtMaximumAuthorized.getValue())) {
                searchForm.setMaximumAuthorized(txtMaximumAuthorized.getValue());
            }
            if (dbAllowedUntil.getValue() != null) {
                searchForm.setAllowedUntil(dbAllowedUntil.getValue());
            }
            if (lbAnnexeType.getSelectedItem() != null && lbAnnexeType.getSelectedItem().getValue() != null) {
                Long annexeType = Long.valueOf(lbAnnexeType.getSelectedItem().getValue().toString());
                if (annexeType > -1L) {
                    searchForm.setAnnexeType(annexeType);
                }
            }
            if (lbIsActive.getSelectedItem() != null && lbIsActive.getSelectedItem().getValue() != null) {
                Long isActive = Long.valueOf(lbIsActive.getSelectedItem().getValue().toString());
                if (isActive > -1L) {
                    searchForm.setIsActive(isActive);
                }
            }
            if (dbAllowedUntil.getValue() != null && !"".equals(dbAllowedUntil.getValue())) {
                searchForm.setAllowedUntil(dbAllowedUntil.getValue());
            }
            fillDataToList();
        } catch (WrongValueException | NumberFormatException ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException {
        Window window = (Window) Executions.createComponents(
                "/Pages/admin/annexe/annexeCreate.zul", null, null);
        window.doModal();
    }

    @Listen("onEdit=#lbAnnexe")
    public void onUpdate(Event ev) throws IOException {
        Annexe abo = (Annexe) lbAnnexe.getSelectedItem().getValue();
        Map args = new ConcurrentHashMap();
        args.put("id", abo.getAnnexeId());
        Window window = (Window) Executions.createComponents("/Pages/admin/annexe/annexeCreate.zul", null, args);
        window.doModal();
    }

    @Listen("onDelete=#lbAnnexe")
    public void onDelete(Event ev) throws IOException {
        Messagebox.show("Bạn có chắc chắn muốn xóa không?", "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    try {
                                        Annexe rs = (Annexe) lbAnnexe.getSelectedItem().getValue();
                                        AnnexeDAO daohe = new AnnexeDAO();
                                        daohe.delete(rs.getAnnexeId());
                                        onSearch();
                                    } catch (Exception ex) {
                                        LogUtils.addLogDB(ex);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    private void fillDataToList() {
        AnnexeDAO objhe = new AnnexeDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbAnnexe.setModel(lstModel);
    }
}
