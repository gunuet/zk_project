/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.AttachmentCallback;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.cosmetic.Model.CRUDMode;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.ValidatorUtil;
import com.viettel.voffice.BO.Document.Attachs;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author giangnh20
 */
public class CaCRUDController extends BaseComposer {

    private Window parentWindow;
    private CRUDMode crudMode;
    private CaUser caUser;

    @Wire
    private Textbox tbSerial;

    @Wire
    private Window caCRUDWindow;

    @Wire
    private Button btnAdd, btnAddNClose, btnUpdate;

    @Wire
    private Vlayout flist, flist1;

    @Wire
    private Hlayout signatureAttachedLayout, stamperAttachedLayout;

    private Media signatureMedia, stamperMedia;

    private final List<Hlayout> hlayoutList = new ArrayList();
    @Wire
    Textbox txtCertSerial, txtBase64Hash;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        Map<String, Object> args = (Map<String, Object>) Executions.getCurrent().getArg();
        parentWindow = (Window) args.get("parentWindow");
        crudMode = (CRUDMode) args.get("crudMode");
        caUser = (CaUser) args.get("currentItem");
        if (caUser == null) {
            caUser = new CaUser();
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onClick=#btnGetSerial")
    public void onLoadTokenSerial() {

        String tokenSerial = getDeviceSerial();
        tbSerial.setValue(tokenSerial);
    }

    @Listen("onClose=#caCRUDWindow")
    public void onCloseWindow() {
        Map<String, Object> args = new ConcurrentHashMap();
        Events.sendEvent("onChildWindowClosed", parentWindow, args);
        if (caCRUDWindow != null) {
            caCRUDWindow.detach();
        }
    }

    @Listen("onUploadCert = #caCRUDWindow")
    public void onUploadCert(Event event) {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        String certSerial = x509Cert.getSerialNumber().toString(16);
        tbSerial.setValue(certSerial);
    }

    // Ham xu ly viec lay serial token
    public String getDeviceSerial() {
        //TODO: xu ly lay serial token
        java.security.SecureRandom ran = new java.security.SecureRandom();
        int random = (int) (ran.nextInt() * 1000000 + 1);
        
        return String.valueOf(random);
    }

    @Listen("#caCRUDWindow #btnAddNClose, #caCRUDWindow #btnUpdate")
    public void onAddOrUpdate() {
        if (onAddOrEdit()) {
            onCloseWindow();
        }
    }

    @Listen("#caCRUDWindow #btnAdd")
    public boolean onAddOrEdit() {
        String msg;
        if ((msg = ValidatorUtil.validateTextbox(tbSerial, true, 100)) != null) {
            showNotification(String.format(msg, getLabelCos("ca_crud_serial_label")), Clients.NOTIFICATION_TYPE_WARNING);
            return false;
        }

//        // Kiem tra trung lap
//        if (new CaUserDAO().isExistCa(tbSerial.getValue()) && crudMode != CRUDMode.EDIT) {
//            showNotification(getLabelCos("ca_notification_duplicate_serial"), Clients.NOTIFICATION_TYPE_WARNING);
//            return false;
//        }
        CaUserDAO caDAO = new CaUserDAO();
        if (crudMode == CRUDMode.CREATE) {
            caUser = new CaUser();
            caUser.setUserId(getUserId());
            caUser.setIsActive(1L);
            caUser.setCaSerial(textBoxGetValue(tbSerial));
            caUser.setCreatedAt(new Date());
            caDAO.saveOrUpdate(caUser);
        }

        if (signatureMedia != null) {
            try {
                if (crudMode == CRUDMode.EDIT && caUser.getSignature() != null) {
                    if (AttachDAO.removeFile(caUser.getSignature(), false)) {
                        caUser.setSignature(null);
                    }
                }
                AttachDAO attachDAO = new AttachDAO();
                attachDAO.saveFileAttach(signatureMedia, caUser.getCaId(), Constants.OBJECT_TYPE.COSMETIC_CA_ATTACHMENT, null, new AttachmentCallback() {

                    @Override
                    public void onUploadFinished(Attachs attachObj, String uploadPath) {
                        caUser.setSignature(uploadPath);
                    }

                    @Override
                    public void onUploadFailed() {
                    }
                });
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
        }

        if (stamperMedia != null) {
            try {
                if (crudMode == CRUDMode.EDIT && caUser.getStamper() != null) {
                    if (AttachDAO.removeFile(caUser.getStamper(), false)) {
                        caUser.setSignature(null);
                    }
                }
                AttachDAO attachDAO = new AttachDAO();
                attachDAO.saveFileAttach(stamperMedia, caUser.getCaId(), Constants.OBJECT_TYPE.COSMETIC_CA_ATTACHMENT, null, new AttachmentCallback() {

                    @Override
                    public void onUploadFinished(Attachs attachObj, String uploadPath) {
                        caUser.setStamper(uploadPath);
                    }

                    @Override
                    public void onUploadFailed() {
                    }
                });

            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
        }
        if (crudMode == CRUDMode.EDIT) {
            caUser.setUpdatedAt(new Date());
        }
        caDAO.saveOrUpdate(caUser);
        showNotification(getLabelCos(crudMode == CRUDMode.CREATE ? "ca_notification_add_success" : "ca_notification_update_success"), Clients.NOTIFICATION_TYPE_INFO);
        tbSerial.setValue("");
        signatureMedia = null;
        stamperMedia = null;
        for (Hlayout item : hlayoutList) {
            if (item != null) {
                item.detach();
            }
        }
        caDAO.flush();
        return true;
    }

    @Listen("onUpload=#btnAttachStamper")
    public void onUploadStamper(UploadEvent e) throws UnsupportedEncodingException {
        final Media[] medias = e.getMedias();
        if (medias != null && medias.length > 0) {
            Media media = medias[0];
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileTypeImage(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file_image", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }
            stamperMedia = media;

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A(getLabelCos("ca_crud_upload_label_delete"));
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            hlayoutList.remove(hl);
                            // xoa file khoi danh sach file
                            stamperMedia = null;
                        }
                    });
            hl.appendChild(rm);
            flist1.appendChild(hl);
            hlayoutList.add(hl);
        }

    }

    @Listen("onUpload=#btnAttachSignature")
    public void onUploadSignature(UploadEvent e) throws UnsupportedEncodingException {
        final Media[] medias = e.getMedias();
        if (medias != null && medias.length > 0) {
            Media media = medias[0];
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileTypeImage(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file_image", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }
            signatureMedia = media;

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A(getLabelCos("ca_crud_upload_label_delete"));
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            hlayoutList.remove(hl);
                            // xoa file khoi danh sach file
                            signatureMedia = null;
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
            hlayoutList.add(hl);
        }

    }

    @Listen("onDeleteAttach=#caCRUDWindow")
    public void onDeleteAttach(ForwardEvent e) {
        Object v = e.getData();
        Integer attachType = Integer.valueOf(v.toString());
        if (caUser != null) {
            // 1L = stamper
            if (attachType == 1L) {
                if (caUser.getStamper() != null && !"".equals(caUser.getStamper())) {
                    AttachDAO.removeFile(caUser.getStamper(), false);
                    caUser.setStamper(null);
                    if (stamperAttachedLayout != null) {
                        stamperAttachedLayout.detach();
                    }
                }
            } else {
                if (caUser.getSignature() != null && !"".equals(caUser.getSignature())) {
                    AttachDAO.removeFile(caUser.getSignature(), false);
                    caUser.setSignature(null);
                    if (signatureAttachedLayout != null) {
                        signatureAttachedLayout.detach();
                    }
                }
            }
        }
    }

    public CRUDMode getCrudMode() {
        return crudMode;
    }

    public void setCrudMode(CRUDMode crudMode) {
        this.crudMode = crudMode;
    }

    public CaUser getCaUser() {
        return caUser;
    }

    public void setCaUser(CaUser caUser) {
        this.caUser = caUser;
    }

}
