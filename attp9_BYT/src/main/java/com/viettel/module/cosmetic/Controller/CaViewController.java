/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.cosmetic.Model.CRUDMode;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author giangnh20
 */
public class CaViewController extends BaseComposer {

    @Wire
    private Window caViewWindow;

    @Wire
    private Textbox tbSearchSerial;

    @Wire
    private Listbox lbCAList;

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            fillListData();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onClick=#btnAddNew")
    public void onAddNew() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", caViewWindow);
        arguments.put("crudMode", CRUDMode.CREATE);
        //arguments.put("currentItem", null);
        createWindow("caCreateWindow", "/Pages/module/cosmetic/caCRUD.zul", arguments, Window.MODAL);
    }

    @Listen("onChildWindowClosed=#caViewWindow")
    public void onChildWindowClosed() {
        fillListData();
    }

    @Listen("onDeleteItem=#caViewWindow")
    public void onDeleteItem(ForwardEvent e) {
        Messagebox.show(getLabelCos("ca_view_messagebox_confirm_delete"), getLabelCos("ca_view_messagebox_title_notify"), Messagebox.OK | Messagebox.CANCEL, Messagebox.EXCLAMATION, Messagebox.CANCEL, new EventListener<Event>() {

            @Override
            public void onEvent(Event e) throws Exception {
                if (Messagebox.ON_OK.equals(e.getName())) {
                    Listitem selectedItem = lbCAList.getSelectedItem();
                    if (selectedItem != null) {
                        CaUser ca = selectedItem.getValue();
                        CaUserDAO caDAO = new CaUserDAO();
                        caDAO.delete(ca);
                        fillListData();
                    }
                }
            }
        });
    }

    @Listen("onEditItem=#caViewWindow")
    public void onEditItem(ForwardEvent e) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", caViewWindow);
        arguments.put("crudMode", CRUDMode.EDIT);
        CaUser selectedItem = null;
        if (lbCAList.getSelectedItem() != null) {
            selectedItem = (CaUser) lbCAList.getSelectedItem().getValue();
        }
        arguments.put("currentItem", selectedItem);
        createWindow("caCreateWindow", "/Pages/module/cosmetic/caCRUD.zul", arguments, Window.MODAL);
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {
            if (tbSearchSerial.getText() != null && !"".equals(tbSearchSerial.getText()) && tbSearchSerial.getText().length() > 100) {
                showNotification(getLabelCos("ca_view_search_warning_max_length"), Clients.NOTIFICATION_TYPE_INFO);
                return;
            }
            fillListData();           
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    public void fillListData() {
        CaUserDAO caDAO = new CaUserDAO();
        List<CaUser> caList = caDAO.findCaBySerial(tbSearchSerial.getValue(), 1L, getUserId());
        lbCAList.setModel(new ListModelArray(caList));
    }

    @Listen("onDownloadAttach=#caViewWindow")
    public void onDownloadAttach(ForwardEvent e) throws FileNotFoundException {
        Object v = e.getData();
        Integer attachType = Integer.valueOf(v.toString());
        if (lbCAList.getSelectedItem() != null) {
            CaUser item = (CaUser) lbCAList.getSelectedItem().getValue();
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            if (!"/".equals(separator) && !"\\".equals(separator)) {
                separator = "/";
            }
            String filePath = null;
            if (attachType == 0) {
                if (item.getStamper() != null && !"".equals(item.getStamper())) {
                    filePath = folderPath + separator + item.getStamper();
                }
            } else {
                if (item.getSignature() != null && !"".equals(item.getSignature())) {
                    filePath = folderPath + separator + item.getSignature();
                }
            }
            if (filePath == null) {
                return;
            }
            File f = new File(filePath);
            if (f.exists()) {
                Filedownload.save(f, "image/jpeg");
            } else {
                showNotification(getLabelCos("ca_notification_file_not_exists"));
            }
        }

    }
}
