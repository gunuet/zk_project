package com.viettel.module.cosmetic.Controller;

import com.viettel.voffice.BO.Files;
import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.DAO.CosmeticDAO;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.model.SearchModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Linhdx
 */
public class CosmeticAllController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #dbFromDayModify")
    private Datebox dbFromDayModify;
    @Wire("#incSearchFullForm #dbToDayModify")
    private Datebox dbToDayModify;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #lboxStatus")
    private Listbox lboxStatus;
    @Wire("#incSearchFullForm #tbCosmeticBrand")
    private Textbox tbCosmeticBrand;
    @Wire("#incSearchFullForm #tbCosmeticProductName")
    private Textbox tbCosmeticProductName;
    @Wire("#incSearchFullForm #tbCosmeticCompanyName")
    private Textbox tbCosmeticCompanyName;
    @Wire("#incSearchFullForm #tbCosmeticCompanyAddress")
    private Textbox tbCosmeticCompanyAddress;
    @Wire("#incSearchFullForm #tbCosmeticDistributePersonName")
    private Textbox tbCosmeticDistributePersonName;
    @Wire("#incSearchFullForm #tbBusinessTaxCode")
    private Textbox tbBusinessTaxCode;
    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    private Window cosmeticAll;
    private CosmeticFileModel lastSearchModel;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;
    Long UserId = getUserId();
    CategoryDAOHE dao = new CategoryDAOHE();
    //  Long RoleId = Long.parseLong(dao.LayTruong("RoleUserDept", "userId", UserId.toString(), "roleId"));
    private Users user;
    private String sPositionType;
    private Constants constants = new Constants();
    String sBoSung = Constants.PROCESS_STATUS.SENT_RETURN.toString();
    String statusStr;
    //list cac checkbox đã check trong list
    List<VFileCosfile> itemsCheckedList, lstCosmetic;
    //list cac checkbox đã check trong 1 trang
    List<Listitem> itemsChecksedPage;

    //luong
    private VFileCosfile vFileCosfile;
    private Files files;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        itemsCheckedList = new ArrayList<VFileCosfile>();
        lastSearchModel = new CosmeticFileModel();
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        FILE_TYPE_STR = (String) arguments.get("filetype");
        String deptIdStr = (String) arguments.get("deptid");
        String menuTypeStr = (String) arguments.get("menuType");
        sPositionType = (String) arguments.get("PositionType");
        statusStr = (String) arguments.get("status");
        Long statusL = null;
        try {
            if (statusStr != null) {
                statusL = Long.parseLong(statusStr);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        if (statusL != null) {
            lastSearchModel.setStatus(statusL);
        }

        lastSearchModel.setMenuTypeStr(menuTypeStr);
        try {
            DEPT_ID = Long.parseLong(deptIdStr);
            DEPT_ID = 3300L;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            DEPT_ID = -1l;
        }
        FILE_TYPE = WorkflowAPI.getInstance().getProcedureTypeIdByCode(FILE_TYPE_STR);

        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);
        CategoryDAOHE cdhe = new CategoryDAOHE();
        CosmeticDAO objDAOHE = new CosmeticDAO();
        List lstStatusAll = cdhe.getSelectCategoryByType(Constants.CATEGORY_TYPE.FILE_STATUS, "name");
        List lstStatus;
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            lstStatus = objDAOHE.findListStatusByCreatorId(getUserId());
            dbFromDay.setValue(null);
            lastSearchModel.setCreateDateFrom(null);
            dbToDay.setValue(new Date());
            lastSearchModel.setCreateDateTo(new Date());
        } else {
            lstStatus = objDAOHE.findListStatusByReceiverAndDeptId(lastSearchModel, getUserId(), getDeptId());
            dbFromDayModify.setValue(null);
            lastSearchModel.setModifyDateFrom(null);
            dbToDayModify.setValue(new Date());
            lastSearchModel.setModifyDateTo(new Date());
        }
        int countlstStatusAll = lstStatusAll.size();
        int countlstStatus = lstStatus.size();
        boolean check;
        for (int i = countlstStatusAll - 1; i > 0; i--) {
            check = false;
            Object obj = lstStatusAll.get(i);
            String value = "";
            try {
                Field field = obj.getClass().getDeclaredField("value");
                field.setAccessible(true);
                value = String.valueOf(field.get(obj));
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
            for (int j = 0; j < countlstStatus; j++) {
                if (value.equals(lstStatus.get(j).toString())) {
                    check = true;
                    break;
                }
            }
            if (check == false) {
                lstStatusAll.remove(i);
            }
        }
        //truong hop danh sach bo sung
        countlstStatus = lstStatusAll.size();
        if (statusStr != null) {
            if (statusStr.equals(sBoSung)) {
                for (int i = countlstStatus - 1; i > 0; i--) {
                    Object obj = lstStatusAll.get(i);
                    String value = "";
                    try {
                        Field field = obj.getClass().getDeclaredField("value");
                        field.setAccessible(true);
                        value = String.valueOf(field.get(obj));
                        if (value.equals(sBoSung)) {
                            continue;
                        } else {
                            lstStatusAll.remove(i);
                        }
                    } catch (Exception ex) {
                        LogUtils.addLogDB(ex);
                    }
                }
            }
        }
        //sap xep lstStatusAll theo value
        //bubbleSort(lstStatusAll, lstStatusAll.size());
        ListModelArray lstModelProcedure = new ListModelArray(lstStatusAll);
        lboxStatus.setModel(lstModelProcedure);

        // set multi check
        onSearch();
    }

    private static void bubbleSort(List unsortedArray, int length) throws Exception {
        int counter, index;
        Object temp;
        String value, value1;
        Object obj;
        Object obj1;
        for (counter = 0; counter < length - 1; counter++) { //Loop once for each element in the array.
            for (index = 1; index < length - 1 - counter; index++) { //Once for each element, minus the counter.
                Field field = unsortedArray.get(counter).getClass().getDeclaredField("name");
                field.setAccessible(true);
                obj = unsortedArray.get(index);
                obj1 = unsortedArray.get(index + 1);
                value = String.valueOf(field.get(obj));
                value1 = String.valueOf(field.get(obj1));
                if (value.compareTo(value1) > 0) { //Test if need a swap or not.
                    temp = obj; //These three lines just swap the two elements:
                    unsortedArray.set(index, obj1);
                    unsortedArray.set(index + 1, temp);
                }
            }
        }
    }

    private List<Long> convert(String str) {
        List<Long> lstLong = new ArrayList<>();
        if (str != null) {
            String[] statusLst = str.split(";");
            for (String statusStr : statusLst) {
                try {
                    if (statusStr != null) {
                        Long statusL = Long.parseLong(statusStr);
                        lstLong.add(statusL);
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }

            }
        }
        return lstLong;
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        //validate search
        //neu an nut search reset lại itemsCheckedList
        itemsCheckedList = new ArrayList<VFileCosfile>();
        if (dbFromDay != null && dbToDay != null) {
            if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                showNotification("Thời gian từ ngày không được lớn hơn đến ngày", Constants.Notification.WARNING, 2000);
                return;
            }
        }
        if (dbFromDayModify != null && dbToDayModify != null) {
            if (dbFromDayModify.getValue().compareTo(dbToDayModify.getValue()) > 0) {
                showNotification("Thời gian từ ngày không được lớn hơn đến ngày", Constants.Notification.WARNING, 2000);
                return;
            }
        }

        lastSearchModel.setCreateDateFrom(dbFromDay == null ? null : dbFromDay.getValue());
        lastSearchModel.setCreateDateTo(dbToDay == null ? null : dbToDay.getValue());
        lastSearchModel.setModifyDateFrom(dbFromDayModify == null ? null : dbFromDayModify.getValue());
        lastSearchModel.setModifyDateTo(dbToDayModify == null ? null : dbToDayModify.getValue());
        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        lastSearchModel.setBrandName(tbCosmeticBrand.getValue());
        lastSearchModel.setProductName(tbCosmeticProductName.getValue());
        lastSearchModel.setBusinessName(tbCosmeticCompanyName == null ? null : tbCosmeticCompanyName.getValue());
        lastSearchModel.setBusinessAddress(tbCosmeticCompanyAddress == null ? null : tbCosmeticCompanyAddress.getValue());
        lastSearchModel.setDistributePersonName(tbCosmeticDistributePersonName == null ? null : tbCosmeticDistributePersonName.getValue());
        lastSearchModel.setTaxCode(tbBusinessTaxCode == null ? null : tbBusinessTaxCode.getValue());
        if (lboxStatus.getSelectedItem() != null) {
            try {
                Object value = lboxStatus.getSelectedItem().getValue();
                if (value != null && value instanceof String) {
                    Long valueL = Long.valueOf((String) value);
                    if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                        lastSearchModel.setStatus(valueL);
                    }
                } else {
                    lastSearchModel.setStatus(null);
                }
            } catch (NumberFormatException ex) {
                LogUtils.addLogDB(ex);
            }

        }
        if (statusStr != null) {
            if (statusStr.equals(sBoSung)) {
                lastSearchModel.setStatus(Long.parseLong(sBoSung));
            }
        }
//        if (lboxDocumentTypeCode.getSelectedItem() != null) {
        //            String value = lboxDocumentTypeCode.getSelectedItem().getValue();
        //            if (value != null) {
        //                Long valueL = Long.valueOf(value);
        //                if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
        //                    lastSearchModel.setDocumentTypeCode(valueL);
        //                }
        //            }
        //        }
        {
            userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        }
        reloadModel(lastSearchModel);

    }

    private void reloadModel(SearchModel searchModel) {
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm;
        CosmeticDAO objDAOHE = new CosmeticDAO();
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(), start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel, getUserId(), getDeptId(), start, take);
        }

        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstCosmetic = plm.getLstReturn();
        lstModel.setMultiple(true);

        //itemsCheckedList set selected
        lbList.setModel(lstModel);
        lbList.renderAll();
        List<Listitem> listItem = lbList.getItems();
        Set<Listitem> itemselected = new HashSet();
        for (Listitem itemlb : listItem) {
            for (int i = 0; i < itemsCheckedList.size(); i++) {
                if (itemsCheckedList.get(i).getFileId().equals(((VFileCosfile) itemlb.getValue()).getFileId())) {
                    itemselected.add(itemlb);
                    break;
                }
            }
        }
        lbList.setSelectedItems(itemselected);
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #cosmeticAll")
    public void onSearchFullText(Event event
    ) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch=#cosmeticAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    //Show popup khi an nut thao tac
    @Listen("onShowActionMulti=#cosmeticAll")
    public void onShowActionMulti() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("lstCosmetic", itemsCheckedList);
        Window window = createWindow("MultiProcess", "/Pages/module/cosmetic/viewMultiProcess.zul", arguments, Window.MODAL);
        window.setMode(Window.Mode.MODAL);
    }

    @Listen("onChangeTime=#cosmeticAll")
    public void onChangeTime(Event e
    ) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            if (dbFromDay != null) {
                dbFromDay.setValue(model.getFromDate());
            }
            if (dbToDay != null) {
                dbToDay.setValue(model.getToDate());
            }
            if (dbFromDayModify != null) {
                dbFromDayModify.setValue(model.getFromDate());
            }
            if (dbToDayModify != null) {
                dbToDayModify.setValue(model.getToDate());
            }

            onSearch();
        }
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", cosmeticAll);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("procedureId", FILE_TYPE);

        arguments.put("deptId", DEPT_ID);
        return arguments;

    }

    @Listen("onOpenCreate=#cosmeticAll")
    public void onOpenCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("CRUDMode", "CREATE");
        setParam(arguments);
        Window window = createWindow("windowCRUD",
                "/Pages/cosmetic/cosmeticCRUD.zul", arguments,
                Window.EMBEDDED);
        window.setMode(Window.Mode.EMBEDDED);
//        cosmeticAll.setVisible(false);
    }

    @Listen("onSelect =#incList #lbList")
    public void onSelect(Event event) {
        Listbox lb = (Listbox) event.getTarget();
        List<Listitem> li = lb.getItems();
        Set<Listitem> liselect = lb.getSelectedItems();
        //loai bo nhưng item trong lb co trong itemsCheckedList
        for (Listitem itemlb : li) {
            for (VFileCosfile items : itemsCheckedList) {
                if (((VFileCosfile) itemlb.getValue()).getFileId().equals(items.getFileId())) {
                    itemsCheckedList.remove(items);
                    break;
                }
            }
        }
        // add item check vao  itemsCheckedList
        for (Listitem item : liselect) {
            itemsCheckedList.add((VFileCosfile) item.getValue());
        }
    }

    @Listen("onOpenView =#incList #lbList")
    public void onOpenView(Event event) {
        VFileCosfile obj = (VFileCosfile) event.getData();
        createWindowView(obj.getFileId(), Constants.DOCUMENT_MENU.ALL,
                cosmeticAll);
        cosmeticAll.setVisible(false);

    }

    @Listen("onRefresh =#cosmeticAll")
    public void onRefresh(Event event) {
        onOpenView(event);
    }

    @Listen("onSelectedProcess = #cosmeticAll")
    public void onSelectedProcess(Event event) {
        cosmeticAll.setVisible(false);

    }

    private void createWindowView(Long id, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView", "/Pages/module/cosmetic/cosmeticView.zul",
                arguments, Window.EMBEDDED);
    }

    @Listen("onSave = #cosmeticAll")
    public void onSave(Event event) {
        cosmeticAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi
     * windowDocIn len Con cac su kien save, update thi da xu li hien thi
     * windowDocIn trong phuong thuc onSave
     */
    @Listen("onVisible =  #cosmeticAll")
    public void onVisible() {
        reloadModel(lastSearchModel);
        cosmeticAll.setVisible(true);
    }

    @Listen("onOpenUpdate = #incList #lbList")
    public void onOpenUpdate(Event event) {
        VFileCosfile obj = (VFileCosfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowCRUDRapidTest", "/Pages/module/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
        cosmeticAll.setVisible(false);
    }

    @Listen("onDownLoadGiayPhep = #incList #lbList")
    public void onDownLoadGiayPhep(Event event) throws FileNotFoundException, IOException {
        VFileCosfile obj = (VFileCosfile) event.getData();
        Long fileid = obj.getFileId();
        CosmeticDAO cosDao = new CosmeticDAO();
        CategoryDAOHE catDaohe = new CategoryDAOHE();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        //co giay phep
        if (check == 1) {
            AttachDAOHE attachDaoHe = new AttachDAOHE();
            Long cospermit = Long.parseLong(catDaohe.LayTruongPermitId(fileid));

            Attachs attachs = attachDaoHe.getByObjectIdAndAttachCat(cospermit, Constants.OBJECT_TYPE.COSMETIC_PERMIT);
            //Nếu có file
            if (attachs != null) {
//                attDAO.downloadFileAttach(attachs);

                String path = attachs.getFullPathFile();
                File f = new File(path);
                //neu ton tai
                if (f.exists()) {
                    File tempFile = FileUtil.createTempFile(f, attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                } //Khong se tao file 
                else {
                    String folderPath = ResourceBundleUtil.getString("dir_upload");
                    FileUtil.mkdirs(folderPath);
                    String separator = ResourceBundleUtil.getString("separator");
                    folderPath += separator + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
                    File fd = new File(folderPath);
                    if (!fd.exists()) {
                        fd.mkdirs();
                    }
                    f = new File(attachs.getFullPathFile());
                    if (f.exists()) {
                    } else {
                        f.createNewFile();
                    }
                    File tempFile = FileUtil.createTempFile(f, attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                }
            } //chua có file sẽ tạo trên hệ thông
            else {
                //showNotification("File không còn tồn tại trên hệ thống!");
            }
        }
        //se bo sung truong hop sua goi bo sung
        //Kiem tra bang permit
    }

    @Listen("onViewFlow = #lbList")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul", args, Window.MODAL);
    }

    @Listen("onDelete = #incList #lbList")
    public void onDelete(Event event) {
        final VFileCosfile obj = (VFileCosfile) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {
                                        if (isAbleToDelete(obj)) {
                                            FilesDAOHE objDAOHE = new FilesDAOHE();
                                            objDAOHE.delete(obj.getFileId());
                                            onSearch();
                                            showNotification(String.format(Constants.Notification.DELETE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.INFO);
                                        } else {
                                            showNotification("Bạn không có quyền xóa");
                                        }

                                    } catch (Exception ex) {
                                        showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                        LogUtils.addLogDB(ex);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    @Listen("onClick=#incSearchFullForm #btnReset")
    public void doResetForm() {
        tbCosmeticBrand.setValue("");
        tbCosmeticCompanyAddress.setValue("");
        tbCosmeticCompanyName.setValue("");
        tbCosmeticProductName.setValue("");
        tbCosmeticDistributePersonName.setValue("");
        tbNSWFileCode.setValue("");
        if (tbBusinessTaxCode != null) {
            tbBusinessTaxCode.setValue("");
        }
        if (dbFromDay != null) {
            dbFromDay.setValue(null);
        }
        if (dbToDay != null) {
            dbToDay.setValue(new Date());
        }
        if (dbFromDayModify != null) {
            dbFromDayModify.setValue(null);
        }
        if (dbToDayModify != null) {
            dbToDayModify.setValue(new Date());
        }
        lboxStatus.setSelectedIndex(0);

        onSearch();
    }

    @Listen("onCopy = #incList #lbList")
    public void onCopy(Event event) {

//        VFileCosfile obj = (VFileCosfile) event.getData();
//        Map<String, Object> arguments = new ConcurrentHashMap<>();
//
//        Files files = createFile(obj);
//        CosFile cosFile = createCosFile(obj);
//
//        arguments.put("CRUDMode", "COPY");
//        arguments.put("files", files);
//        arguments.put("cosFile", cosFile);
//        setParam(arguments);
//        createWindow("windowCRUDRapidTest", "/Pages/module/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
        VFileCosfile obj = (VFileCosfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "COPY");
        setParam(arguments);
        createWindow("windowCRUDRapidTest", "/Pages/module/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
        cosmeticAll.setVisible(false);

    }

    private Files createFile(VFileCosfile obj) {
        Files files = new Files();
        files.setCreateDate(obj.getCreateDate());
        files.setModifyDate(obj.getModifyDate());
        files.setCreatorId(obj.getCreatorId());
        files.setCreatorName(obj.getCreatorName());
        files.setCreateDeptId(obj.getCreateDeptId());
        files.setCreateDeptName(obj.getCreateDeptName());
        files.setIsActive(Constants.Status.ACTIVE);

        // viethd 29/01/2015
        // set status of file is INIT
        files.setStatus(Constants.PROCESS_STATUS.INITIAL);
        files.setFileType(FILE_TYPE);
        CategoryDAOHE che = new CategoryDAOHE();
        Category cat = che.findById(FILE_TYPE);
        if (cat != null) {
            String fileTypeName = cat.getName();
            files.setFileTypeName(fileTypeName);
        }
        List<Flow> flows = WorkflowAPI.getInstance().getFlowByDeptNObject(DEPT_ID, FILE_TYPE);
        files.setFlowId(flows.get(0).getFlowId());
        return files;
    }

    private CosFile createCosFile(VFileCosfile obj) {
        CosFile cosFile = new CosFile();
        cosFile.setFileId(obj.getFileId());

        cosFile.setDocumentTypeCode(obj.getDocumentTypeCode());
        cosFile.setBrandName(obj.getBrandName());
        cosFile.setProductName(obj.getProductName());
        //cosFile.setProductType(tbProductType.getValue());
        cosFile.setIntendedUse(obj.getIntendedUse());
//                cosFile.setProductPresentation(tbProductPresentation.getValue());
        cosFile.setDistributorName(obj.getDistributorName());
        cosFile.setDistributorAddress(obj.getDistributorAddress());
        cosFile.setDistributorPhone(obj.getDistributorPhone());
        cosFile.setDistributorFax(obj.getDistributorFax());
        cosFile.setDistributorRegisNumber(obj.getDistributorRegisNumber());
        cosFile.setDistributePersonName(obj.getDistributePersonName());
        cosFile.setDistributePersonPhone(obj.getDistributePersonPhone());
        cosFile.setDistributePersonEmail(obj.getDistributePersonEmail());
        cosFile.setDistributePersonPosition(obj.getDistributePersonPosition());
        cosFile.setImporterName(obj.getImporterName());
        cosFile.setImporterAddress(obj.getImporterAddress());
        cosFile.setImporterPhone(obj.getImporterPhone());
        cosFile.setImporterFax(obj.getImporterFax());
        cosFile.setOtherProductPresentation(obj.getProductPresentation());
        cosFile.setOtherProductType(obj.getOtherProductType());
        cosFile.setIngreConfirm1(obj.getIngreConfirm1());
        cosFile.setIngreConfirm2(obj.getIngreConfirm2());

        cosFile.setSignPlace(obj.getSignPlace());
        cosFile.setSignName(obj.getSignName());
        cosFile.setSignDate(obj.getSignDate());
        cosFile.setProductPresentation(obj.getProductPresentation());
        cosFile.setProductType(obj.getProductType());

        return cosFile;
    }

    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(VFileCosfile obj) {
        if (user.getUserId() != null && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.VT_RETURN))) {
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean isAbleToModify(VFileCosfile obj) {
        if (user.getUserId() != null && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.SENT_DISPATCH))
                    || (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.SENT_RETURN))
                    || (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.VT_RETURN))) {
                return true;
            }
            return false;
        }
        return false;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public int CheckGiayPhep(Long fileid) {
        CosmeticDAO cosDao = new CosmeticDAO();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        return check;
    }

    public int checkDispathSDBS(Long fileid) {
        CosmeticDAO cosDao = new CosmeticDAO();
        int check = cosDao.checkDispathSDBS(fileid, getUserId());
        return check;
    }

//    public boolean CheckView(String name) {
//        if (name.equals("Edit")) {
//            if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDC)) {
//                return false;
//            } else if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDP)) {
//                return false;
//            } else {
//                return true;
//            }
//        }
//        if (name.equals("Delete")) {
//            if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDC)) {
//                return false;
//            }
//            return true;
//        }
//        return true;
//    }
    @Listen("onViewSDBS = #incList #lbList")
    public void onViewSDBS(Event event) throws FileNotFoundException, IOException {
        VFileCosfile obj = (VFileCosfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowViewSDBS", "/Pages/module/cosmetic/viewDispatch.zul", arguments, Window.MODAL);

    }

    public Constants getConstants() {
        return constants;
    }

    public void setConstants(Constants constants) {
        this.constants = constants;
    }
}
