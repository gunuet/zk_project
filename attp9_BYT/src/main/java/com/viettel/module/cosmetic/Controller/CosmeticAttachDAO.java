/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.RapidTest.VImportFileRtAttach;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class CosmeticAttachDAO extends
        GenericDAOHibernate<Attachs, Long> {

    public CosmeticAttachDAO() {
        super(Attachs.class);
    }

    @Override
    public void saveOrUpdate(Attachs rapidTestAttach) {
        if (rapidTestAttach != null) {
            super.saveOrUpdate(rapidTestAttach);
            //getSession().getTransaction().commit();
        }
    }

    @Override
    public Attachs findById(Long id) {
        Query query = getSession().getNamedQuery(
                "CosmeticAttach.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Attachs) result.get(0);
        }
    }

    @Override
    public void delete(Attachs rapidTestAttach) {
        rapidTestAttach.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(rapidTestAttach);
    }

    public void delete(Long id) {
        Query query = getSession().createQuery("update Attachs a set a.isActive = 0 where a.id = ?");
        query.setParameter(0, id);
        query.executeUpdate();
    }

    public List<VFileRtAttach> findCosmeticAttach(Long rapidTestId) {
        Query query = getSession().createQuery("select a from VFileRtAttach a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");

        query.setParameter(0, Constants.OBJECT_TYPE.COSMETIC_FILE_TYPE);
        query.setParameter(1, rapidTestId);
        List<VFileRtAttach> lst = query.list();
        return lst;
    }

    public List<VImportFileRtAttach> findImportAttach(Long rapidTestId) {
        Query query = getSession().createQuery("select a from VImportFileRtAttach a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");

        query.setParameter(0, Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE);
        query.setParameter(1, rapidTestId);
        List<VImportFileRtAttach> lst = query.list();
        return lst;
    }
    
    public List<Attachs> findReportAttach(Long fileId) {
        Query query = getSession().createQuery("select a from Attachs a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");

        query.setParameter(0, Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE);
        query.setParameter(1, fileId);
        List<Attachs> lst = query.list();
        return lst;
    }
    
    public List<Attachs> findReportRejectAttach(Long fileId) {
        Query query = getSession().createQuery("select a from Attachs a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");

        query.setParameter(0, Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE);
        query.setParameter(1, fileId);
        List<Attachs> lst = query.list();
        return lst;
    }
    
    public List<VImportFileRtAttach> findImportOrderFilecAttach(Long fileId) {
         Query query = getSession().createQuery("select a from VImportFileRtAttach a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");

        query.setParameter(0, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE);
        query.setParameter(1, fileId);
        List<VImportFileRtAttach> lsroder = query.list();
        return lsroder;
      
    }
}

