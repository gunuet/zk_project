package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.CosAdditionalRequest;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.BO.CosPermit;
import com.viettel.module.cosmetic.DAO.CosAdditionalRequestDAO;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.cosmetic.DAO.CosPermitDAO;
import com.viettel.module.cosmetic.DAO.CosmeticDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.util.List;

/**
 *
 * @author ChucHV
 */
public class CosmeticBaseController extends BaseComposer {
    //thong tin chi tiet hien thi qua trinh xu ly

//    protected CosAdditionalRequest additionalRequest = new CosAdditionalRequest();
//    protected BookDocument bookDocument;
//    protected CosEvaluationRecord evaluationRecord = new CosEvaluationRecord();
//    protected CosPermit permit = new CosPermit();

//    public void setProcessingView(Long fileId, Long fileType) {
////        if (fileId != null && fileType != null) {
////
////
////            //linhdx 13032015 Load danh sach yeu cau sdbs
////
////            BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
////            BookDocument bookDocument2 = bookDocumentDAOHE.getBookInFromDocumentId(fileId, fileType);
////            if (bookDocument2 != null) {
////                bookDocument = bookDocument2;
////            }
////
////            //linhdx 13032015 Load danh sach yeu cau sdbs
////            CosAdditionalRequestDAO cosAdditionalRequestDAO = new CosAdditionalRequestDAO();
////            List<CosAdditionalRequest> lstAddition = cosAdditionalRequestDAO.findAllActiveByFileId(fileId);
////            if (lstAddition != null && lstAddition.size() > 0) {
////                additionalRequest = lstAddition.get(0);
////            }
////
////            CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
////            evaluationRecord = dao.getLastEvaluation(fileId);
////
////
////            CosPermitDAO cosPermitDAO = new CosPermitDAO();
////            List<CosPermit> lstPermit = cosPermitDAO.findAllActiveByFileId(fileId);
////            if (lstPermit != null && lstPermit.size() > 0) {
////                permit = lstPermit.get(0);
////            }
////
////        }
//
//    }

    public int checkDispathSDBS(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        CosmeticDAO cosDao = new CosmeticDAO();
        int check = cosDao.checkDispathSDBS(fileId, getUserId());
        return check;
    }
    public int checkDispathReject(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        CosmeticDAO cosDao = new CosmeticDAO();
        int check = cosDao.checkDispathReject(fileId, getUserId());
        return check;
    }

    public int checkBooked(Long fileId, Long fileType) {
        if (fileId == null || fileType == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        BookDocumentDAOHE dao = new BookDocumentDAOHE();
        int check = dao.checkBookedCossmetic(fileId, fileType);
        return check;
    }

    public int checkViewProcess(Long fileId) {
        return Constants.CHECK_VIEW.VIEW;
    }

    public int checkEvaluation(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
        int check = dao.checkEvaluation(fileId);
        return check;
    }

    public int checkPermit(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        CosPermitDAO dao = new CosPermitDAO();
        int check = dao.checkPermit(fileId);
        return check;
    }
    
    public int checkPayment(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        PaymentInfoDAO dao = new PaymentInfoDAO();
        int check = dao.checkPayment(fileId);
        return check;
    }

//    public CosAdditionalRequest getAdditionalRequest() {
//        return additionalRequest;
//    }
//
//    public void setAdditionalRequest(CosAdditionalRequest additionalRequest) {
//        this.additionalRequest = additionalRequest;
//    }
//
//    public BookDocument getBookDocument() {
//        return bookDocument;
//    }
//
//    public void setBookDocument(BookDocument bookDocument) {
//        this.bookDocument = bookDocument;
//    }
//
//    public CosEvaluationRecord getEvaluationRecord() {
//        return evaluationRecord;
//    }
//
//    public void setEvaluationRecord(CosEvaluationRecord evaluationRecord) {
//        this.evaluationRecord = evaluationRecord;
//    }
//
//    public CosPermit getPermit() {
//        return permit;
//    }
//
//    public void setPermit(CosPermit permit) {
//        this.permit = permit;
//    }
}
