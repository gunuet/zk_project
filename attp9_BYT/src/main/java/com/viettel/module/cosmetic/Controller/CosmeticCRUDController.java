package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.*;
import com.viettel.module.cosmetic.DAO.*;
import com.viettel.module.cosmetic.Model.CosProductTypeSubModel;
import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.*;
import com.viettel.voffice.BO.Business;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.VFeeProcedure;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.BusinessDAOHE;
import com.viettel.voffice.DAOHE.VFeeProcedureDAOHE;
import com.viettel.voffice.model.AttachCategoryModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Br;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;

/**
 *
 * @author Linhdx
 */
public class CosmeticCRUDController extends CosmeticBaseController {

    private final int SAVE = 1;
    private final int SAVE_CLOSE = 2;
    private final int SAVE_COPY = 3;
    private final int RAPID_TEST_FILE_TYPE = 1;
    private long FILE_TYPE;
    ;
    private long DEPT_ID;
    @Wire
    private Vlayout flist, fListImportExcel;
    private List<Media> listMedia, listFileExcel;
    private List listCosmeticFileType;
    // label for validating data
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbDownloadExcelTemplate;
    @Wire
    private Label lbManufacturerWarning, lbAssemblerWarning, lbCosfileIngreWarning, lbImportExcelWarning;
    // Ý kiến lãnh đạo
    @Wire
    private Window windowCRUDCosmetic;
    private Window parentWindow;
    private String crudMode;
    private String isSave;
    private ListModel model;
    private CosFile cosFile;
    private Files files;
    Long documentTypeCode;
    @Wire
    private Textbox tbBrandName, tbProductName, tbProductType, tbOtherProductType, tbIntendedUse,
            tbProductPresentation, tbOtherProductPresentation,
            tbDistributorName, tbDistributorAddress, tbDistributorPhone, tbDistributorFax, tbDistributorRegisNumber,
            tbDistributePersonName, tbDistributePersonPhone, tbDistributePersonEmail, tbDistributePersonPosition,
            tbImporterName, tbImporterAddress, tbImporterPhone, tbImporterFax,
            tbSignPlace, tbSignName, tbListVariantOrShade, tbIngredientDescription, tbIngredientVariantOrShade;
    @Wire
    private Textbox tbManufacturerId, tbManufacturerIndex, tbManufacturerName, tbManufacturerAddress, tbManufacturerPhone, tbManufacturerFax;
    @Wire
    private Listbox lbManufacturer;
    private List<CosManufacturer> lstManufacturer = new ArrayList();
    @Wire
    private Textbox tbAssemblerId, tbAssemblerIndex, tbAssemblerName, tbAssemblerAddress, tbAssemblerPhone, tbAssemblerFax;
    @Wire
    private Listbox lbAssembler;
    @Wire
    Checkbox cbAssemblerMain, cbAssemblerSub, cbIngreConfirm1, cbIngreConfirm2;
    private List<CosAssembler> lstAssembler = new ArrayList();
    @Wire
    private Textbox tbCosfileIngreId, tbCosfileIngreIndex, tbIngredientName, tbIngredientPercent;
    @Wire
    private Listbox lbCosfileIngre;
    private List<CosCosfileIngre> lstCosfileIngre = new ArrayList();
    @Wire
    private Listbox lbIngredient;
    @Wire
    private Datebox dbSignDate;
    @Wire
    private Listbox lbCosmeticFileType, //loai tep dinh kem
            fileListbox, lbProductPresentation, lbProductType, finalFileListbox;
    @Wire
    private Tabbox tb;
    @Wire
    private Tabpanel mainpanel;
    @Wire
    Textbox txtCertSerial, txtBase64Hash;
    private CosAdditionalRequest additionalRequest;
    private BookDocument bookDocument = new BookDocument();
    private CosEvaluationRecord evaluationRecord = new CosEvaluationRecord();
    private CosPermit permit = new CosPermit();
    private PaymentInfo paymentInfo = new PaymentInfo();
    private String nswFileCode;
    // luu filesId trong truong hop copy
    private Long originalFilesId;
    private Long originalCosFileId;
    private Long originalFileType;
    private List<CosManufacturer> originalLstManufacturer = new ArrayList<>();
    private List<CosAssembler> originalLstAssembler = new ArrayList<>();
    private List<CosCosfileIngre> originalLstCosCosfileIngre = new ArrayList<>();
    private String fileSignOut = "";

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();

        parentWindow = (Window) arguments.get("parentWindow");
        crudMode = (String) arguments.get("CRUDMode");
        listMedia = new ArrayList();

        //Tu file con
//        DEPT_ID = (Long) arguments.get("deptId");
//        FILE_TYPE = (Long) arguments.get("procedureId");
        //Vao truc tiep file
        WorkflowAPI w = new WorkflowAPI();
        FILE_TYPE = w.getProcedureTypeIdByCode(Constants.CATEGORY_TYPE.COSMETIC_OBJECT);
        DEPT_ID = Constants.CUC_QLD_ID;//Cục QLD
//        args.put("CRUDMode", "CREATE");
        documentTypeCode = Constants.COSMETIC.DOCUMENT_TYPE_CODE_TAOMOI;
        Long id;
        FilesDAOHE fileDAOHE;
        CosmeticDAO objDAOHE;
        switch (crudMode) {
            case "CREATE":
                //documentTypeCode = (Long) arguments.get("documentTypeCode");
                files = new Files();
                cosFile = new CosFile();
                cosFile.setNswFileCode(null);
                loadBusinessInfo();
                nswFileCode = getAutoNswFileCode(documentTypeCode);
                break;
            case "UPDATE":
                id = (Long) arguments.get("id");
                fileDAOHE = new FilesDAOHE();
                files = fileDAOHE.findById(id);
                objDAOHE = new CosmeticDAO();
                cosFile = objDAOHE.findByFileId(id);
                nswFileCode = cosFile.getNswFileCode();
                originalFilesId = files.getFileId();
                originalFileType = files.getFileType();
                originalCosFileId = cosFile.getCosFileId();
                break;
            case "COPY":
//                files = (Files) arguments.get("files");
//                cosFile = (CosFile) arguments.get("cosFile");
//                cosFile.setNswFileCode(null);
//                loadBusinessInfo();
                nswFileCode = getAutoNswFileCode(documentTypeCode);
                id = (Long) arguments.get("id");
                fileDAOHE = new FilesDAOHE();
                Files filesOld = fileDAOHE.findById(id);
                files = copy(filesOld);
                //files = new Files();
                objDAOHE = new CosmeticDAO();
                CosFile cosFileOld = objDAOHE.findByFileId(id);
                cosFile = copy(cosFileOld);
                //nswFileCode = cosFile.getNswFileCode();

                originalFilesId = filesOld.getFileId();
                originalFileType = filesOld.getFileType();
                originalCosFileId = cosFileOld.getCosFileId();
                break;
        }

        //Xem qua trinh xu ly
        //setProcessingView(files.getFileId(), files.getFileType()); 
        //Xem qua trinh xu ly
        setProcessingView(originalFilesId, originalFileType);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    private Files copy(Files old) {
        Files newObj = new Files();
        newObj.setBusinessAddress(old.getBusinessAddress());
        newObj.setBusinessFax(old.getBusinessFax());
        newObj.setBusinessId(old.getBusinessId());
        newObj.setBusinessName(old.getBusinessName());
        newObj.setBusinessPhone(old.getBusinessPhone());
        newObj.setCreateDate(new Date());
        newObj.setCreateDeptId(old.getCreateDeptId());
        newObj.setCreateDeptName(old.getCreateDeptName());
        newObj.setCreatorId(old.getCreatorId());
        newObj.setCreatorName(old.getCreatorName());
        newObj.setFileCode(old.getFileCode());
        newObj.setFileName(old.getFileName());
        newObj.setFileType(old.getFileType());
        newObj.setFileTypeName(old.getFileTypeName());
        newObj.setFlowId(old.getFlowId());
        newObj.setIsActive(old.getIsActive());
        newObj.setIsTemp(old.getIsTemp());
        newObj.setTaxCode(old.getTaxCode());
        return newObj;
    }

    private CosFile copy(CosFile old) {
        CosFile newObj = new CosFile();
        newObj.setAttachmentInfos(old.getAttachmentInfos());
        newObj.setBrandName(old.getBrandName());
        newObj.setCirculatingNo(old.getCirculatingNo());
        newObj.setContents(old.getContents());
        newObj.setCosmeticNo(old.getCosmeticNo());
        newObj.setDateEffect(old.getDateEffect());
        newObj.setDateIssue(old.getDateIssue());
        newObj.setDistributePersonEmail(old.getDistributePersonEmail());
        newObj.setDistributePersonName(old.getDistributePersonName());
        newObj.setDistributePersonPhone(old.getDistributePersonPhone());
        newObj.setDistributePersonPosition(old.getDistributePersonPosition());
        newObj.setDistributorAddress(old.getDistributorAddress());
        newObj.setDistributorFax(old.getDistributorFax());
        newObj.setDistributorName(old.getDistributorName());
        newObj.setDistributorPhone(old.getDistributorPhone());
        newObj.setDistributorRegisNumber(old.getDistributorRegisNumber());
        newObj.setDocumentTypeCode(old.getDocumentTypeCode());
        newObj.setExtensionNo(old.getExtensionNo());
        newObj.setImporterAddress(old.getImporterAddress());
        newObj.setImporterFax(old.getImporterFax());
        newObj.setImporterName(old.getImporterName());
        newObj.setImporterPhone(old.getImporterPhone());
        newObj.setIngreConfirm1(old.getIngreConfirm1());
        newObj.setIngreConfirm2(old.getIngreConfirm2());
        newObj.setIntendedUse(old.getIntendedUse());
        newObj.setListVariantOrShade(old.getListVariantOrShade());
        newObj.setOtherProductPresentation(old.getOtherProductPresentation());
        newObj.setOtherProductType(old.getOtherProductType());
        newObj.setProductName(old.getProductName());
        newObj.setProductPresentation(old.getProductPresentation());
        newObj.setProductType(old.getProductType());
        newObj.setRoled(old.getRoled());
        newObj.setSignDate(old.getSignDate());
        newObj.setSignName(old.getSignName());
        newObj.setSignPlace(old.getSignPlace());
        newObj.setTested(old.getTested());

        return newObj;
    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        loadItemForEdit();
        if ("UPDATE".equals(crudMode) || "COPY".equals(crudMode)) {
            fillFileListbox(originalFilesId);
            fillFinalFileListbox(originalFilesId);
            CosManufacturerDAO cmdao = new CosManufacturerDAO();
            lstManufacturer = cmdao.findByCosFileId(originalCosFileId);
            ListModelArray lstModelManufature = new ListModelArray(lstManufacturer);
            lbManufacturer.setModel(lstModelManufature);

            for (CosManufacturer obj : lstManufacturer) {
                CosManufacturer manufacturer = new CosManufacturer();
                manufacturer.setAddress(obj.getAddress());
                manufacturer.setCosFileId(obj.getCosFileId());
                manufacturer.setFax(obj.getFax());
                manufacturer.setIsActive(obj.getIsActive());
                manufacturer.setManufacturerId(obj.getManufacturerId());
                manufacturer.setName(obj.getName());
                manufacturer.setPhone(obj.getPhone());
                originalLstManufacturer.add(manufacturer);
            }

            CosAssemblerDAO cmAssemblerdao = new CosAssemblerDAO();
            lstAssembler = cmAssemblerdao.findByCosFileId(originalCosFileId);
            ListModelArray lstModelAssembler = new ListModelArray(lstAssembler);
            lbAssembler.setModel(lstModelAssembler);

            for (CosAssembler obj : lstAssembler) {
                CosAssembler assembler = new CosAssembler();
                assembler.setAddress(obj.getAddress());
                assembler.setAssemblerId(obj.getAssemblerId());
                assembler.setAssemblerMain(obj.getAssemblerMain());
                assembler.setAssemblerSub(obj.getAssemblerSub());
                assembler.setCosFileId(obj.getCosFileId());
                assembler.setFax(obj.getFax());
                assembler.setIsActive(obj.getIsActive());
                assembler.setName(obj.getName());
                assembler.setPhone(obj.getPhone());
                originalLstAssembler.add(assembler);
            }

            CosCosfileIngreDAO cidao = new CosCosfileIngreDAO();
            lstCosfileIngre = cidao.findByCosFileId(originalCosFileId);
            ListModelArray lstModelIngredient = new ListModelArray(lstCosfileIngre);
            lbCosfileIngre.setModel(lstModelIngredient);

            for (CosCosfileIngre obj : lstCosfileIngre) {
                CosCosfileIngre cosfileIngre = new CosCosfileIngre();
                cosfileIngre.setCosFileId(obj.getCosFileId());
                cosfileIngre.setCosfileIngreId(obj.getCosfileIngreId());
                cosfileIngre.setDescription(obj.getDescription());
                cosfileIngre.setIngredientId(obj.getIngredientId());
                cosfileIngre.setIngredientName(obj.getIngredientName());
                cosfileIngre.setIsActive(obj.getIsActive());
                cosfileIngre.setPercent(obj.getPercent());
                cosfileIngre.setVariantOrShade(obj.getVariantOrShade());
                originalLstCosCosfileIngre.add(cosfileIngre);
            }
        }
    }

    public void setProcessingView(Long fileId, Long fileType) {
        if (fileId != null && fileType != null) {

            //linhdx 13032015 Load danh sach yeu cau sdbs
            BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
            BookDocument bookDocument2 = bookDocumentDAOHE.getBookInFromDocumentId(fileId, fileType);
            if (bookDocument2 != null) {
                bookDocument = bookDocument2;
            }

            //linhdx 13032015 Load danh sach yeu cau sdbs
            CosAdditionalRequestDAO cosAdditionalRequestDAO = new CosAdditionalRequestDAO();
            List<CosAdditionalRequest> lstAddition = cosAdditionalRequestDAO.findAllActiveByFileId(fileId);
            if (lstAddition != null && lstAddition.size() > 0) {
                additionalRequest = lstAddition.get(0);
            }

            CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
            evaluationRecord = dao.getLastEvaluation(fileId);

            CosPermitDAO cosPermitDAO = new CosPermitDAO();
            List<CosPermit> lstPermit = cosPermitDAO.findAllActiveByFileId(fileId);
            if (lstPermit != null && lstPermit.size() > 0) {
                permit = lstPermit.get(0);
            }

            PaymentInfoDAO paymentInfoDAO = new PaymentInfoDAO();
            List<PaymentInfo> lstPayment = paymentInfoDAO.getListPayment(fileId, Constants.PAYMENT.PHASE.EVALUATION);
            if (lstPayment != null && lstPayment.size() > 0) {
                paymentInfo = lstPayment.get(0);
            }

        }

    }

    @Listen("onAfterRender = #lbProductPresentation")
    public void onAfterRenderProductPresentation() {
        loadProductPresentationCheck();
        onSelectProductPresentation();

    }

    @Listen("onSelect = #lbProductPresentation")
    public void onSelectProductPresentation() {
        lbProductPresentation.getSelectedItem();
        boolean check = false;
        Set<Listitem> attListProductPresentation = lbProductPresentation.getSelectedItems();
        if (attListProductPresentation.size() > 0) {
            for (Listitem selectedItem : attListProductPresentation) {
                CosProductPresentaton at = (CosProductPresentaton) selectedItem.getValue();
                if (at.getProductPresentationId() == -1L) {
                    check = true;
                }
            }
        }
        if (check) {
            tbOtherProductPresentation.setDisabled(false);

        } else {
            tbOtherProductPresentation.setValue("");
            tbOtherProductPresentation.setDisabled(true);

        }
        //loadProductPresentationCheck();
    }

    @Listen("onSelect = #lbProductType")
    public void onSelectProductType() {
        lbProductType.getSelectedItem();
        boolean check = false;
        Set<Listitem> attListProductType = lbProductType.getSelectedItems();
        if (attListProductType.size() > 0) {
            for (Listitem selectedItem : attListProductType) {
                CosProductTypeSubModel at = (CosProductTypeSubModel) selectedItem.getValue();
                if (at.getProductTypeId() == -1L) {
                    check = true;
                }
            }
        }
        if (check) {
            tbOtherProductType.setDisabled(false);
        } else {
            tbOtherProductType.setValue("");
            tbOtherProductType.setDisabled(true);

        }
    }

    @Listen("onAfterRender = #lbProductType")
    public void onAfterRenderProductType() {
        for (int i = 0; i < lbProductType.getItemCount(); i++) {
            Listitem item = lbProductType.getItemAtIndex(i);
            Listcell cell = (Listcell) item.getChildren().get(0);
            CosProductTypeSubModel cos = item.getValue();
            List<CosProductTypeSub> lstCosProductTypeSub = cos.getLstCosProductTypeSub();
            if (lstCosProductTypeSub != null && lstCosProductTypeSub.size() > 0) {
                for (CosProductTypeSub obj : lstCosProductTypeSub) {
                    Label lb = new Label("- " + obj.getNameVi());
                    lb.setClass("product-type-sub");
                    Label enLb = new Label(obj.getNameEn());
                    enLb.setClass("product-type-sub-en");
                    Br br = new Br();
                    cell.appendChild(br);
                    cell.appendChild(lb);
                    cell.appendChild(new Br());
                    cell.appendChild(enLb);
                }
            }

        }
        lbProductType.renderAll();

        loadProductTypeCheck();
        onSelectProductType();
    }

    private void loadProductPresentationCheck() {
        String productPresentation = cosFile.getProductPresentation();
        if (productPresentation != null) {
            String[] lst = productPresentation.split(";");
            List<Listitem> items = lbProductPresentation.getItems();
            for (Listitem item : items) {
                item.setSelected(false);
            }
            for (String idCosPresent : lst) {
                Long id = Long.valueOf(idCosPresent);
                for (Listitem item : items) {
                    CosProductPresentaton cos = item.getValue();
                    if (Objects.equals(cos.getProductPresentationId(), id)) {
                        item.setSelected(true);
                    }

                }
            }
        }

    }

    private void loadProductTypeCheck() {
        String productType = cosFile.getProductType();
        if (productType != null) {
            String[] lst = productType.split(";");
            List<Listitem> items = lbProductType.getItems();
            for (Listitem item : items) {
                item.setSelected(false);
            }
            for (String idCosPresent : lst) {
                Long id = Long.valueOf(idCosPresent);
                for (Listitem item : items) {
                    CosProductTypeSubModel cos = item.getValue();
                    if (Objects.equals(cos.getProductTypeId(), id)) {
                        item.setSelected(true);
                    }

                }
            }
        }

    }

    private void loadBusinessInfo() {
        Long userId = getUserId();
        BusinessDAOHE bhe = new BusinessDAOHE();
        Business business = bhe.findByUserId(userId);
        UserDAOHE uhe = new UserDAOHE();
        if (business != null) {

            Users user = uhe.getUserInBusiness(business.getBusinessId());
            files.setBusinessId(business.getBusinessId());
            files.setBusinessName(business.getBusinessName());
            files.setBusinessAddress(business.getBusinessAddress());
            files.setBusinessPhone(business.getBusinessTelephone());
            files.setBusinessFax(business.getBusinessFax());
            files.setTaxCode(business.getBusinessTaxCode());

            cosFile.setDistributorName(business.getBusinessName());
            cosFile.setDistributorAddress(business.getBusinessAddress());
            cosFile.setDistributorPhone(business.getBusinessTelephone());
            cosFile.setDistributorFax(business.getBusinessFax());
            cosFile.setDistributorRegisNumber(business.getBusinessTaxCode());
            cosFile.setDistributePersonPosition(user.getPosName());
            cosFile.setDistributePersonName(user.getFullName());
            cosFile.setDistributePersonEmail(user.getEmail());
            cosFile.setDistributePersonPhone(user.getTelephone());

            cosFile.setImporterName(business.getBusinessName());
            cosFile.setImporterAddress(business.getBusinessAddress());
            cosFile.setImporterPhone(business.getBusinessTelephone());
            cosFile.setImporterFax(business.getBusinessFax());

        }
    }

    private void loadItemForEdit() {
        if (cosFile.getDocumentTypeCode() != null) {
            documentTypeCode = cosFile.getDocumentTypeCode();
        }

//        CategoryDAOHE cdhe = new CategoryDAOHE();
//        List lstIngredient = cdhe.findAllCategory(Constants.CATEGORY_TYPE.COS_INGREDIENT);
//        ListModelArray lstModelIngredient = new ListModelArray(lstIngredient);
        // Tam thoi disable di
//        lbIngredient.setModel(lstModelIngredient);
//        lbIngredient.renderAll();
//        if (lstModelIngredient.getSize() > 0) {
//            lbIngredient.setSelectedIndex(0);
//        }
        CosProductPresentatonDAO cppDAO = new CosProductPresentatonDAO();
        List lstCosProductPresentation = cppDAO.getAllActivePlusDifferent();
        ListModelArray lstModelPresentation = new ListModelArray(lstCosProductPresentation);
        lstModelPresentation.setMultiple(true);
        lbProductPresentation.setModel(lstModelPresentation);

        CosProductTypeSubDAO cptsDAO = new CosProductTypeSubDAO();
        List<CosProductTypeSub> lstCosProductTypeSub = cptsDAO.getAllActiveOrderByProductType();
        List<CosProductTypeSubModel> lstCosProductTypeSubModel = new ArrayList();
        int i = 0;
        CosProductTypeSub previeous = new CosProductTypeSub();
        for (Object currentObject : lstCosProductTypeSub) {
            CosProductTypeSub current = (CosProductTypeSub) currentObject;
            //Neu ban ghi hien tai khong co productTypeId bang ban ghi cu thi add ban ghi moi
            if (!Objects.equals(current.getProductTypeId(), previeous.getProductTypeId())) {
                CosProductTypeSubModel cModel = new CosProductTypeSubModel();
                cModel.setProductTypeId(current.getProductTypeId());
                cModel.setNameVi(current.getNameVi());
                cModel.setNameEn(current.getNameEn());
                cModel.setIsActive(current.getIsActive());
                cModel.setNameLink("");
                lstCosProductTypeSubModel.add(cModel);
                i++;
            } else {
                //Neu giong thi chi add them link
                CosProductTypeSubModel obj = lstCosProductTypeSubModel.get(i - 1);
                List<CosProductTypeSub> lstCosProductTypeSub2;
                lstCosProductTypeSub2 = obj.getLstCosProductTypeSub();
                if (lstCosProductTypeSub2 == null) {
                    lstCosProductTypeSub2 = new ArrayList<>();
                }
                lstCosProductTypeSub2.add(current);
                obj.setLstCosProductTypeSub(lstCosProductTypeSub2);

                //obj.setNameLink(obj.getNameLink() + "\r\n" + current.getNameVi() + "(" + current.getNameEn() + ")");
            }
            previeous = current;
        }

        CosProductTypeSubModel cModel = new CosProductTypeSubModel();
        cModel.setProductTypeId(-1L);
        cModel.setNameVi("Các dạng khác (Đề nghị ghi rõ)");
        cModel.setNameEn("Others (please specify)");
        cModel.setIsActive(1L);
        cModel.setIsDifferent(1L);
        cModel.setNameLink("");
        lstCosProductTypeSubModel.add(cModel);
        ListModelArray lstModelType = new ListModelArray(lstCosProductTypeSubModel);
        lstModelType.setMultiple(true);
        lbProductType.setModel(lstModelType);

    }

//    public String getNameLink(Long id) {
//        return "123\n\r456";
//    }
//    @Listen("onAfterRender = #lbProductType")
//    public void onAfterRenderProductType() {
//        List<Listitem> items = lbProductType.getItems();
//                for (Listitem item : items) {
//                    item.;
//
//
//                }
//    }
//    public void setValueProductType(Textbox textbox) {
//        String id = textbox.getId();
//        String value = textbox.getValue();
//        Label label = new Label();
//        label.setValue("123");
//        Br br = new Br();
//        textbox.getParent().appendChild(br);
//        textbox.getParent().appendChild(label);
//
//    }
    /**
     * linhdx Hàm load danh sach trong dropdown control
     *
     * @param type
     * @return
     */
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case RAPID_TEST_FILE_TYPE:
                categoryDAOHE = new CategoryDAOHE();
                listCosmeticFileType = categoryDAOHE.findAllCategory(
                        Constants.CATEGORY_TYPE.COSMETIC_FILE_TYPE);
                lstModel = new ListModelList(listCosmeticFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    /**
     * linhdx Ham tao doi tuong tu form de luu lai
     *
     * @return
     */
    private CosFile createCosFile() {
        // giangnh20
        if ("COPY".equals(crudMode)) {
            cosFile.setFileId(null);
            cosFile.setCosFileId(null);
            cosFile.setNswFileCode(null);
            if (files != null && originalFilesId != null) {
                AttachDAOHE daoHE = new AttachDAOHE();
                daoHE.copyAttachs(originalFilesId, files.getFileId());
            }
        }
        cosFile.setFileId(files.getFileId());
        if (documentTypeCode != null) {
            cosFile.setDocumentTypeCode(documentTypeCode);
            nswFileCode = getAutoNswFileCode(documentTypeCode);
            //linhdx neu chua co ma ho so thi tao moi
            //Neu co roi thi khong lam gi
            if (cosFile.getNswFileCode() == null) {
                cosFile.setNswFileCode(nswFileCode);
            }

            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
                cosFile.setBrandName(textBoxGetValue(tbBrandName));
                cosFile.setProductName(textBoxGetValue(tbProductName));
                cosFile.setListVariantOrShade(textBoxGetValue(tbListVariantOrShade));
                //cosFile.setProductType(tbProductType.getValue());
                cosFile.setIntendedUse(textBoxGetValue(tbIntendedUse));
//                cosFile.setProductPresentation(tbProductPresentation.getValue());
                cosFile.setDistributorName(textBoxGetValue(tbDistributorName));
                cosFile.setDistributorAddress(textBoxGetValue(tbDistributorAddress));
                cosFile.setDistributorPhone(textBoxGetValue(tbDistributorPhone));
                cosFile.setDistributorFax(textBoxGetValue(tbDistributorFax));
                cosFile.setDistributorRegisNumber(textBoxGetValue(tbDistributorRegisNumber));
                cosFile.setDistributePersonName(textBoxGetValue(tbDistributePersonName));
                cosFile.setDistributePersonPhone(textBoxGetValue(tbDistributePersonPhone));
                cosFile.setDistributePersonEmail(textBoxGetValue(tbDistributePersonEmail));
                cosFile.setDistributePersonPosition(textBoxGetValue(tbDistributePersonPosition));
                cosFile.setImporterName(textBoxGetValue(tbImporterName));
                cosFile.setImporterAddress(textBoxGetValue(tbImporterAddress));
                cosFile.setImporterPhone(textBoxGetValue(tbImporterPhone));
                cosFile.setImporterFax(textBoxGetValue(tbImporterFax));
                cosFile.setOtherProductPresentation(textBoxGetValue(tbOtherProductPresentation));
                cosFile.setOtherProductType(textBoxGetValue(tbOtherProductType));
                cosFile.setIngreConfirm1(cbIngreConfirm1.isChecked() ? 1L : 0L);
                cosFile.setIngreConfirm2(cbIngreConfirm2.isChecked() ? 1L : 0L);

                cosFile.setSignPlace(textBoxGetValue(tbSignPlace));
                cosFile.setSignName(textBoxGetValue(tbSignName));
                cosFile.setSignDate(dbSignDate.getValue());
                Set<Listitem> attListProductPresentation = lbProductPresentation.getSelectedItems();
                if (attListProductPresentation.size() > 0) {
                    //Cong id
                    String productPresentation = "";
                    int i = 0;
                    for (Listitem selectedItem : attListProductPresentation) {
                        CosProductPresentaton at = (CosProductPresentaton) selectedItem.getValue();
                        if (i == 0) {
                            productPresentation = String.valueOf(at.getProductPresentationId());
                        } else {
                            productPresentation += ";" + String.valueOf(at.getProductPresentationId());
                        }
                        i++;
                    }
                    cosFile.setProductPresentation(productPresentation);
                }

                Set<Listitem> attListProductType = lbProductType.getSelectedItems();
                if (attListProductType.size() > 0) {
                    //Cong idfileListbox
                    String productType = "";
                    int i = 0;
                    for (Listitem selectedItem : attListProductType) {
                        CosProductTypeSubModel at = (CosProductTypeSubModel) selectedItem.getValue();
                        if (i == 0) {
                            productType = String.valueOf(at.getProductTypeId());
                        } else {
                            productType += ";" + String.valueOf(at.getProductTypeId());
                        }
                        i++;
                    }
                    cosFile.setProductType(productType);
                }

            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
                cosFile.setBrandName(textBoxGetValue(tbBrandName));
                cosFile.setSignPlace(textBoxGetValue(tbSignPlace));
                cosFile.setSignName(textBoxGetValue(tbSignName));
                cosFile.setSignDate(dbSignDate.getValue());
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
                cosFile.setBrandName(textBoxGetValue(tbBrandName));
                cosFile.setSignPlace(textBoxGetValue(tbSignPlace));
                cosFile.setSignName(textBoxGetValue(tbSignName));
                cosFile.setSignDate(dbSignDate.getValue());
            }

        }

        return cosFile;
    }

    private Files createFile() throws Exception {
        Date dateNow = new Date();
        files.setCreateDate(dateNow);
        files.setModifyDate(dateNow);
        files.setCreatorId(getUserId());
        files.setCreatorName(getUserFullName());
        files.setCreateDeptId(getDeptId());
        files.setCreateDeptName(getDeptName());
        files.setIsActive(Constants.Status.ACTIVE);

        // viethd 29/01/2015
        // set status of file is INIT
        //linhdx neu la them moi thi status Initial
        //Neu bo sung thi khong cap nhat trang thai
        if (files.getStatus() == null) {
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
        }

        files.setFileType(FILE_TYPE);
        CategoryDAOHE che = new CategoryDAOHE();
        Category cat = che.findById(FILE_TYPE);
        if (cat != null) {
            String fileTypeName = cat.getName();
            files.setFileTypeName(fileTypeName);
        }
        List<Flow> flows = WorkflowAPI.getInstance().getFlowByDeptNObject(DEPT_ID, FILE_TYPE);
        files.setFlowId(flows.get(0).getFlowId());
        return files;
    }

    private Files createFileUpdate() throws Exception {
        Date dateNow = new Date();
        files.setModifyDate(dateNow);
        return files;
    }

    public String getAutoNswFileCode(Long documentTypeCode) {
//        CategoryDAOHE obj = new CategoryDAOHE();
        String autoNumber;
//        if (lstCategory != null && lstCategory.size() > 0) {
//            Category ca = lstCategory.get(0);
//            autoNumber = ca.getValue();
////            ca.setValue(String.valueOf(Long.valueOf(autoNumber) + 1));
////            obj.saveOrUpdate(ca);//Tang chi so len
//        }
        CosmeticDAO cosmeticDAO = new CosmeticDAO();
        Long autoNumberL = cosmeticDAO.countCosfile();
        autoNumberL += 1L;//Tránh số 0 (linhdx)
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

//    public void saveAutoNswFileCode(Long documentTypeCode) {
//        CategorySearchForm form = new CategorySearchForm();
//        form.setType(Constants.RAPID_TEST.TYPE_CONFIG);
//        form.setCode(Constants.RAPID_TEST.CODE_CONFIG);
//        CategoryDAOHE obj = new CategoryDAOHE();
//        List<Category> lstCategory = obj.findCategory(form);
//        String autoNumber = "000001";
//        if (lstCategory != null && lstCategory.size() > 0) {
//            Category ca = lstCategory.get(0);
//            autoNumber = ca.getValue();
//            ca.setValue(String.valueOf(Long.valueOf(autoNumber) + 1));
//            obj.saveOrUpdate(ca);//Tang chi so len
//        }
//
//    }
    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (documentTypeCode != null) {

            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
                if (tbBrandName.getText().matches("\\s*")) {
                    showWarningMessage("Nhãn hàng  không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbBrandName.focus();
                    return false;
                }
                if (tbProductName.getText().matches("\\s*")) {
                    showWarningMessage("Tên sản phẩm không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbProductName.focus();
                    return false;
                }

            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
                if (tbBrandName.getText().matches("\\s*")) {
                    showWarningMessage("Nhãn hàng  không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbBrandName.focus();
                    return false;
                }
                if (tbProductName.getText().matches("\\s*")) {
                    showWarningMessage("Tên sản phẩm không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbProductName.focus();
                    return false;
                }
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
                if (tbBrandName.getText().matches("\\s*")) {
                    showWarningMessage("Nhãn hàng  không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbBrandName.focus();
                    return false;
                }
                if (tbProductName.getText().matches("\\s*")) {
                    showWarningMessage("Tên sản phẩm không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbProductName.focus();
                    return false;
                }
            }

        }
        return true;
    }

    private boolean isValidatedFile() {
        Long valueFileType = Long.valueOf((String) lbCosmeticFileType.getSelectedItem().getValue());
        Long headerValue = (Long) Constants.COMBOBOX_HEADER_VALUE;
        if (lbCosmeticFileType.getSelectedItem() == null
                || headerValue.equals(valueFileType)) {
            showWarningMessage("Loại tệp đính kèm không thể để trống");
            lbCosmeticFileType.focus();
            return false;
        }

        if (listMedia == null || listMedia.isEmpty()) {
            showWarningMessage("Chưa chọn đính kèm");
            return false;
        }
        if (listMedia.size() > 1) {
            showWarningMessage("Chỉ được chọn 1 file đính kèm");
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbManufacturerWarning.setValue("");
        lbAssemblerWarning.setValue("");
        lbCosfileIngreWarning.setValue("");
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSave(SAVE);
                break;
            case KeyEvent.F7:
                onSave(SAVE_CLOSE);
                break;
            case KeyEvent.F8:
                onSave(SAVE_COPY);
                break;
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave(int typeSave) throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }

        try {
            if (null != crudMode) {
                // Save to 
                if (!validateData()) {
                    return;
                }

                switch (crudMode) {
                    case "CREATE": {
                        createObject();
                        showNotification(String.format(
                                Constants.Notification.SAVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                                Constants.Notification.INFO);
                        break;
                    }
                    case "UPDATE":
                        createOldVersion();
                        createObject();
                        createFileUpdate();
                        showNotification(String.format(
                                Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                                Constants.Notification.INFO);
                        break;
                    case "COPY":
                        //linhdx - Gán lại trạng thái update
                        createObject();
                        if ("COPY".equals(crudMode)) {
                            crudMode = "UPDATE";
                        }
                        //Reset lai cac gia tri moi
                        originalFilesId = files.getFileId();
                        originalFileType = files.getFileType();
                        originalCosFileId = cosFile.getCosFileId();
                        showNotification(String.format(
                                Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                                Constants.Notification.INFO);

                }
                createPayment();
                addManufacturer(originalCosFileId);
            }

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void createOldVersion() {
        //Tao 1 file verion cu isActive = 0
        Files fileVerion = copy(files);
        fileVerion.setIsActive(Constants.Status.INACTIVE);
        if (fileVerion.getStatus() == null) {
            fileVerion.setStatus(Constants.PROCESS_STATUS.INITIAL);
        }
        fileVerion.setModifyDate(new Date());
        fileVerion.setParentFileId(files.getFileId());
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        filesDAOHE.saveOrUpdate(fileVerion);

        //Tao cosfile cu co fileId bang file Id vua tao
        CosFile cosFileVersion = copy(cosFile);
        cosFileVersion.setFileId(fileVerion.getFileId());
        cosFileVersion.setNswFileCode(cosFile.getNswFileCode());
        CosmeticDAO cosFileDAO = new CosmeticDAO();
        cosFileDAO.saveOrUpdate(cosFileVersion);

        saveVersionManufacturer(cosFileVersion.getCosFileId());
        saveVersionAssembler(cosFileVersion.getCosFileId());
        saveVersionIngredient(cosFileVersion.getCosFileId());
    }

    @Listen("onClick = #btnAddManufacturerCat")
    public void AddManufactuerFromCat() {
        Map<String, Object> arg = new ConcurrentHashMap<>();
        arg.put("parentWindow", windowCRUDCosmetic);
        Window window = (Window) Executions.createComponents("/Pages/module/cosmetic/ManufacturerCat/ManufacturerCatList.zul", null, arg);
        window.doOverlapped();

    }

    private void createPayment() {
        VFeeProcedureDAOHE vFeeFileDAOHE = new VFeeProcedureDAOHE();
        List<VFeeProcedure> lstVFeeProcedure = vFeeFileDAOHE.getListFee(Constants.CATEGORY_TYPE.COSMETIC_OBJECT);
        Long fileId = cosFile.getFileId();
        if (lstVFeeProcedure.size() > 0) {
            PaymentInfoDAO paymentInfoDAOHE = new PaymentInfoDAO();
            List lst = paymentInfoDAOHE.getListPayment(fileId);
            if (lst.isEmpty()) {
                //Neu chua co ban ghi thanh toan thi luu lai 
                for (VFeeProcedure obj : lstVFeeProcedure) {

                    PaymentInfo paymentInfo = new PaymentInfo();
                    paymentInfo.setFileId(fileId);
                    paymentInfo.setFeeId(obj.getFeeId());
                    paymentInfo.setFeeName(obj.getName());
                    paymentInfo.setIsActive(Constants.Status.ACTIVE);
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_NEW);
                    paymentInfo.setPhase(obj.getPhase());
                    String value = obj.getCost();
                    Long valueL = 0L;
                    try {
                        valueL = Long.valueOf(value);
                    } catch (Exception ex) {
                        LogUtils.addLogDB(ex);
                    }
                    paymentInfo.setCost(valueL);
                    paymentInfoDAOHE.saveOrUpdate(paymentInfo);

                }
            }

        }
    }

    private void createObject() throws Exception {
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        files = createFile();
        filesDAOHE.saveOrUpdate(files);
        CosmeticDAO cosFileDAO = new CosmeticDAO();
        cosFile = createCosFile();
        cosFileDAO.saveOrUpdate(cosFile);
//        saveAutoNswFileCode(cosFile.getDocumentTypeCode());

        saveManufacturer(cosFile.getCosFileId());
        saveAssembler(cosFile.getCosFileId());
        saveIngredient(cosFile.getCosFileId());
    }

    @Listen("onClick=#btnCreate")
    public void onCreateCosmeticFileType() throws IOException, Exception {
        if (saveObject() == false) {
            return;
        }
        if (listMedia.isEmpty()) {
            showWarningMessage("Chưa chọn file đính kèm");
        }
        Long cosFileFileType = Long.valueOf((String) lbCosmeticFileType.getSelectedItem().getValue());
        AttachDAO base = new AttachDAO();

        for (Media media : listMedia) {
            base.saveFileAttach(media, files.getFileId(), Constants.OBJECT_TYPE.COSMETIC_FILE_TYPE, cosFileFileType);
        }
        fillFileListbox(files.getFileId());
        listMedia.clear();
        flist.getChildren().clear();
    }

    @Listen("onClick=#btnCreateProFile")
    public void onCreateProFile() throws IOException, Exception {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", windowCRUDCosmetic);
        Window window = (Window) Executions.createComponents(
                "/Pages/rapidTest/PublicFileManageList.zul", null, arguments);
        window.doModal();
    }

    private void fillFileListbox(Long fileId) {
        CosmeticAttachDAO rDaoHe = new CosmeticAttachDAO();
        List<VFileRtAttach> lstCosmeticAttach = rDaoHe.findCosmeticAttach(fileId);
        this.fileListbox.setModel(new ListModelArray(lstCosmeticAttach));

    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long fileId = obj.getObjectId();
        CosmeticAttachDAO rDAOHE = new CosmeticAttachDAO();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(fileId);
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    /**
     * Tinh tong dung luong file attach da upload, da luu vao table ATTACHS & co
     * objectId
     *
     * @author giangnh20
     * @return
     */
    public Long getAttachmentSize() {
        if (files != null && files.getFileId() != null) {

            AttachDAOHE daoHE = new AttachDAOHE();
            Long currentSize = 0L;
            List<Attachs> attachs = daoHE.findByObjectId(files.getFileId());
            for (Attachs item : attachs) {
                try {
                    File f = new File(item.getFullPathFile());
                    if (f.exists()) {
                        currentSize += f.length();
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }
            }
            return currentSize;
        }
        return 0L;
    }

    /**
     * Xu ly su kien upload file
     *
     * @param event
     */
    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();
        listMedia.clear();
        Long currentSize = getAttachmentSize();
        Long limitSize = Long.parseLong(getConfigValue("limitSizeAttachPerDraft", "config", "20971520"));

        for (final Media media : medias) {
            Long mediaSize = Integer.valueOf(media.getByteData().length).longValue();
            if (currentSize + mediaSize > limitSize) {
                showNotification("Tổng dung lượng đính kèm không được vượt quá 20M", Clients.NOTIFICATION_TYPE_INFO);
                return;
            }
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    /**
     * Dong cua so khi o dang popup
     */
    @Listen("onClose = #windowCRUDCosmetic")
    public void onClose() {
        try {
            if (windowCRUDCosmetic != null) {
                windowCRUDCosmetic.detach();
            }
            Events.sendEvent("onVisible", parentWindow, null);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
//        windowCRUDCosmetic.onClose();
//        Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onVisible = #windowCRUDCosmetic")
    public void onVisible() {
        windowCRUDCosmetic.setVisible(true);
    }
    //su kien chon tu ho so dung chung

    @Listen("onChooseProFile = #windowCRUDCosmetic")
    public void onChooseProFile(Event event) throws IOException, Exception {
        if (saveObject() == false) {
            return;
        }
        Map<String, Object> args = (Map<String, Object>) event.getData();
        List list = (List) args.get("documentReceiveProcess");
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        for (Object list1 : list) {
            AttachCategoryModel Process = (AttachCategoryModel) list1;
            Attachs attach = new Attachs();
            attach.setAttachPath(Process.getAttach().getAttachPath());
            String fileName = (Process.getAttach().getAttachName());
            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(files.getFileId());
            attach.setCreatorId(getUserId());
            attach.setCreatorName(Process.getAttach().getCreatorName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(Constants.OBJECT_TYPE.COSMETIC_FILE_TYPE);
            attach.setAttachType(Process.getAttach().getAttachType());
            attach.setAttachDes(Process.getAttach().getAttachDes());
            attachDAOHE.saveOrUpdate(attach);
        }
        fillFileListbox(files.getFileId());

    }

    @Listen("onChooseManufacturer = #windowCRUDCosmetic")
    public void onChooseManufacturer(Event event) throws IOException, Exception {
        Map<String, Object> args = (Map<String, Object>) event.getData();
        List list = (List) args.get("documentReceiveProcess");
        for (Object list1 : list) {
            CosCatManufacturer objCat = (CosCatManufacturer) list1;
            CosManufacturer obj = new CosManufacturer();
            obj.setName(objCat.getName());
            obj.setAddress(objCat.getAddress());
            obj.setPhone(objCat.getPhone());
            obj.setFax(objCat.getFax());
            Boolean check = true;
            if (lbManufacturer.getSelectedItem() == null) {
                // check trung lap
                //giangnh20
                ListModel<CosManufacturer> lbCosManufacturerListModel = lbManufacturer.getListModel();
                if (lbCosManufacturerListModel != null) {
                    for (int i = 0; i < lbCosManufacturerListModel.getSize(); i++) {
                        CosManufacturer item = lbCosManufacturerListModel.getElementAt(i);
                        if (item.getName().toLowerCase().equals(objCat.getName().toLowerCase())) {
                            lbManufacturerWarning.setValue("Tên nhà sản xuất đã có trong danh sách");
                            check = false;
                            break;
                        }
                    }
                }
            }
            if (check) {
                lstManufacturer.add(obj);
            }
        }
        ListModelArray lstModelManufature = new ListModelArray(lstManufacturer);
        lbManufacturer.setModel(lstModelManufature);

    }

    //Manufacturer
    private void saveManufacturer(Long cosFileId) {
        CosManufacturerDAO cmdao = new CosManufacturerDAO();
        List<Long> lstManufacturerIdFromDB = cmdao.findAllIdByCosFileId(cosFileId);
        for (Long id : lstManufacturerIdFromDB) {
            if (!isExistManufacturer(id, lstManufacturer)) {
                cmdao.delete(id);
            }
        }
        List<Long> lstId = new ArrayList<>();
        for (CosManufacturer obj : lstManufacturer) {
            obj.setCosFileId(cosFileId);
            obj.setIsActive(Constants.Status.ACTIVE);
            if ("COPY".equals(crudMode)) {
                obj.setManufacturerId(null);
            }
            cmdao.saveOrUpdate(obj);
            lstId.add(obj.getManufacturerId());

        }

//        cmdao.delete(lstId);
    }

    /**
     * Tao phien ban Nha san xuat
     */
    private void saveVersionManufacturer(Long versionCosfileId) {
        CosManufacturerDAO cmdao = new CosManufacturerDAO();
        for (CosManufacturer obj : originalLstManufacturer) {
            CosManufacturer manufacturer = new CosManufacturer();
            manufacturer.setAddress(obj.getAddress());
            manufacturer.setCosFileId(versionCosfileId);
            manufacturer.setFax(obj.getFax());
            manufacturer.setIsActive(Constants.Status.ACTIVE);
            manufacturer.setName(obj.getName());
            manufacturer.setPhone(obj.getPhone());
            manufacturer.setManufacturerId(null);
            cmdao.saveOrUpdate(manufacturer);
        }
    }

    private Boolean isExistManufacturer(Long id, List<CosManufacturer> lst) {
        for (CosManufacturer itemInList : lst) {
            if (id != null && id.equals(itemInList.getManufacturerId())) {
                return true;
            }
        }
        return false;
    }

    @Listen("onClick = #btnAddManufacturer")
    public void onAddManufacturer() throws Exception {
        String msg;
        if ((msg = ValidatorUtil.validateTextbox(tbManufacturerName, true, 255)) != null) {
            lbManufacturerWarning.setValue(String.format(msg, "Tên nhà sản xuất"));
            tb.setSelectedPanel(mainpanel);
            tbManufacturerName.focus();
            return;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbManufacturerAddress, true, 255)) != null) {
            lbManufacturerWarning.setValue(String.format(msg, "Địa chỉ nhà sản xuất"));
            tb.setSelectedPanel(mainpanel);
            tbManufacturerAddress.focus();
            return;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbManufacturerPhone, false, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            lbManufacturerWarning.setValue(String.format(msg, "Số điện thoại"));
            tb.setSelectedPanel(mainpanel);
            tbManufacturerPhone.focus();
            return;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbManufacturerFax, false, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            lbManufacturerWarning.setValue(String.format(msg, "Số Fax"));
            tb.setSelectedPanel(mainpanel);
            tbManufacturerFax.focus();
            return;
        }

        if (lbManufacturer.getSelectedItem() == null) {
            // check trung lap
            //giangnh20
            ListModel<CosManufacturer> lbCosManufacturerListModel = lbManufacturer.getListModel();
            if (lbCosManufacturerListModel != null) {
                for (int i = 0; i < lbCosManufacturerListModel.getSize(); i++) {
                    CosManufacturer item = lbCosManufacturerListModel.getElementAt(i);
                    if (item.getName().toLowerCase().equals(tbManufacturerName.getValue().toLowerCase())) {
                        lbManufacturerWarning.setValue("Tên nhà sản xuất đã có trong danh sách");
                        tb.setSelectedPanel(mainpanel);
                        tbManufacturerName.focus();
                        return;
                    }
                }
            }
        }
        CosManufacturer obj;
        if (tbManufacturerIndex.getValue() != null && !tbManufacturerIndex.getValue().isEmpty() && lstManufacturer.size() > 0) {
            int index = Integer.parseInt(tbManufacturerIndex.getValue());
            obj = lstManufacturer.get(index);
        } else {
            obj = new CosManufacturer();
        }
        if (tbManufacturerId.getValue() != null && !tbManufacturerId.getValue().isEmpty()) {
            obj.setManufacturerId(Long.parseLong(tbManufacturerId.getValue()));
        }
        obj.setName(tbManufacturerName.getValue());
        obj.setAddress(tbManufacturerAddress.getValue());
        obj.setPhone(tbManufacturerPhone.getValue());
        obj.setFax(tbManufacturerFax.getValue());

        if (tbManufacturerIndex.getValue() != null && !tbManufacturerIndex.getValue().isEmpty()) {
            //donothing
        } else {
            lstManufacturer.add(obj);
        }

        ListModelArray lstModelManufature = new ListModelArray(lstManufacturer);
        lbManufacturer.setModel(lstModelManufature);
        resetManufacturer();
        lbManufacturerWarning.setValue("");
    }

    private void resetManufacturer() {
        tbManufacturerName.setValue("");
        tbManufacturerAddress.setValue("");
        tbManufacturerPhone.setValue("");
        tbManufacturerFax.setValue("");
        tbManufacturerId.setValue("");
        tbManufacturerIndex.setValue("");
    }

    @Listen("onEdit=#lbManufacturer")
    public void onEditManufacturer(Event ev) throws IOException {
        CosManufacturer cm = (CosManufacturer) lbManufacturer.getSelectedItem().getValue();
        tbManufacturerName.setValue(cm.getName());
        tbManufacturerAddress.setValue(cm.getAddress());
        tbManufacturerId.setValue(cm.getManufacturerId() == null ? "" : String.valueOf(cm.getManufacturerId()));
        tbManufacturerPhone.setValue(cm.getPhone());
        tbManufacturerFax.setValue(cm.getFax());
        tbManufacturerIndex.setValue(String.valueOf(lbManufacturer.getSelectedIndex()));
    }

    @Listen("onDelete=#lbManufacturer")
    public void onDeleteManufacturer(Event ev) throws IOException {
        CosManufacturer cm = (CosManufacturer) lbManufacturer.getSelectedItem().getValue();
        lstManufacturer.remove(cm);
        ListModelArray lstModelManufacturer = new ListModelArray(lstManufacturer);
        lbManufacturer.setModel(lstModelManufacturer);
    }

    @Listen("onAddToAssembler=#lbManufacturer")
    public void onAddToAssembler(Event ev) throws IOException {

        CosManufacturer cm = (CosManufacturer) lbManufacturer.getSelectedItem().getValue();

        boolean hasMainAssembler = false;
        boolean hasAssemblerName = false;
        if (lstAssembler != null) {
            for (CosAssembler obj : lstAssembler) {
                if (obj.getAssemblerMain() == 1L && !hasMainAssembler) {
                    hasMainAssembler = true;
                }
                if (obj.getName().equals(cm.getName()) && !hasAssemblerName) {
                    hasAssemblerName = true;
                }
                if (hasAssemblerName && hasMainAssembler) {
                    break;
                }
            }
        }
        if (hasAssemblerName) {
            showNotification("Đơn vị đóng gói đã tồn tại trong danh sách");
            return;
        }
        CosAssembler obj = new CosAssembler();
        obj.setName(cm.getName());
        obj.setAddress(cm.getAddress());
        obj.setPhone(cm.getPhone());
        obj.setFax(cm.getFax());
        obj.setAssemblerMain(hasMainAssembler ? 0L : 1L);
        obj.setAssemblerSub(0L);
        if (lstAssembler != null) {
            lstAssembler.add(obj);
            ListModelArray lstModelManufature = new ListModelArray(lstAssembler);
            lbAssembler.setModel(lstModelManufature);
        }

    }

    /**
     * Tao phien ban Nha dong goi
     *
     * @param cosFileId
     */
    private void saveVersionAssembler(Long versionCosfileId) {
        CosAssemblerDAO cmdao = new CosAssemblerDAO();
        for (CosAssembler obj : originalLstAssembler) {
            CosAssembler assembler = new CosAssembler();
            assembler.setAddress(obj.getAddress());
            assembler.setAssemblerMain(obj.getAssemblerMain());
            assembler.setAssemblerSub(obj.getAssemblerSub());
            assembler.setCosFileId(versionCosfileId);
            assembler.setFax(obj.getFax());
            assembler.setName(obj.getName());
            assembler.setPhone(obj.getPhone());
            assembler.setIsActive(Constants.Status.ACTIVE);
            assembler.setAssemblerId(null);
            cmdao.saveOrUpdate(assembler);
        }

    }

    //Assembler
    private void saveAssembler(Long cosFileId) {
        CosAssemblerDAO cmdao = new CosAssemblerDAO();
        List<Long> lstAssemblerIdFromDB = cmdao.findAllIdByCosFileId(cosFileId);
        for (Long id : lstAssemblerIdFromDB) {
            if (!isExistAssembler(id, lstAssembler)) {
                cmdao.delete(id);
            }
        }

        for (CosAssembler obj : lstAssembler) {
            obj.setCosFileId(cosFileId);
            obj.setIsActive(Constants.Status.ACTIVE);
            if ("COPY".equals(crudMode)) {
                obj.setAssemblerId(null);
            }
            cmdao.saveOrUpdate(obj);
        }

    }

    private Boolean isExistAssembler(Long id, List<CosAssembler> lst) {
        for (CosAssembler itemInList : lst) {

            if (id != null && id.equals(itemInList.getAssemblerId())) {
                return true;
            }

        }
        return false;
    }

    @Listen("onClick = #btnAddAssembler")
    public void onAddAssembler() throws Exception {
        if (cbAssemblerMain.isChecked()) {
            if (lbAssembler.getSelectedItem() == null) {
                for (CosAssembler obj : lstAssembler) {
                    if (obj.getAssemblerMain() == 1L) {
                        lbAssemblerWarning.setValue("Không được phép thêm 2 đơn vị đóng gói chính");
                        tbAssemblerFax.focus();
                        return;
                    }
                }
            }
        }
        if (tbAssemblerName.getText().matches("\\s*")) {
            lbAssemblerWarning.setValue("Tên nhà sản xuất không thể để trống");
            tbAssemblerName.focus();
            return;
        }
        if (tbAssemblerAddress.getText().matches("\\s*")) {
            lbAssemblerWarning.setValue("Địa chỉ nhà sản xuất không thể để trống");
            tbAssemblerAddress.focus();
            return;
        }
        String msg;
        if ((msg = ValidatorUtil.validateTextbox(tbAssemblerPhone, false, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            lbAssemblerWarning.setValue(String.format(msg, "Số điện thoại"));
            tb.setSelectedPanel(mainpanel);
            tbAssemblerPhone.focus();
            return;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbAssemblerFax, false, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            lbAssemblerWarning.setValue(String.format(msg, "Số Fax"));
            tb.setSelectedPanel(mainpanel);
            tbAssemblerFax.focus();
            return;
        }

        if (lbAssembler.getSelectedItem() == null) {
            ListModel<CosAssembler> lbCosAssemblerListModel = lbAssembler.getListModel();
            if (lbCosAssemblerListModel != null) {
                for (int i = 0; i < lbCosAssemblerListModel.getSize(); i++) {
                    CosAssembler item = lbCosAssemblerListModel.getElementAt(i);
                    if (item.getName().toLowerCase().equals(tbAssemblerName.getValue().toLowerCase())) {
                        lbAssemblerWarning.setValue("Tên đơn vị đóng gói đã có trong danh sách");
                        tb.setSelectedPanel(mainpanel);
                        tbAssemblerName.focus();
                        return;
                    }
                }
            }
        }
        CosAssembler obj;
        if (tbAssemblerIndex.getValue() != null && !tbAssemblerIndex.getValue().isEmpty() && lstAssembler.size() > 0) {
            int index = Integer.parseInt(tbAssemblerIndex.getValue());
            obj = lstAssembler.get(index);
        } else {
            obj = new CosAssembler();
        }
        if (tbAssemblerId.getValue() != null && !tbAssemblerId.getValue().isEmpty()) {
            obj.setAssemblerId(Long.parseLong(tbAssemblerId.getValue()));
        }
        obj.setName(tbAssemblerName.getValue());
        obj.setAddress(tbAssemblerAddress.getValue());
        obj.setPhone(tbAssemblerPhone.getValue());
        obj.setFax(tbAssemblerFax.getValue());
        obj.setAssemblerMain(cbAssemblerMain.isChecked() ? 1L : 0L);
        obj.setAssemblerSub(cbAssemblerSub.isChecked() ? 1L : 0L);

        if (tbAssemblerIndex.getValue() != null && !tbAssemblerIndex.getValue().isEmpty()) {
            //donothing
        } else {
            lstAssembler.add(obj);
        }

        ListModelArray lstModelManufature = new ListModelArray(lstAssembler);
        lbAssembler.setModel(lstModelManufature);
        lbAssemblerWarning.setValue("");
        resetAssembler();
    }

    private void resetAssembler() {
        tbAssemblerName.setValue("");
        tbAssemblerAddress.setValue("");
        tbAssemblerPhone.setValue("");
        tbAssemblerFax.setValue("");
        tbAssemblerId.setValue("");
        tbAssemblerIndex.setValue("");
        cbAssemblerMain.setChecked(false);
        cbAssemblerSub.setChecked(false);
    }

    @Listen("onEdit=#lbAssembler")
    public void onEditAssembler(Event ev) throws IOException {
        CosAssembler cm = (CosAssembler) lbAssembler.getSelectedItem().getValue();
        tbAssemblerName.setValue(cm.getName());
        tbAssemblerId.setValue(cm.getAssemblerId() == null ? "" : String.valueOf(cm.getAssemblerId()));
        tbAssemblerPhone.setValue(cm.getPhone());
        tbAssemblerFax.setValue(cm.getFax());
        tbAssemblerAddress.setValue(cm.getAddress());
        tbAssemblerIndex.setValue(String.valueOf(lbAssembler.getSelectedIndex()));
        cbAssemblerMain.setChecked(cm.getAssemblerMain() == 1L ? true : false);
        cbAssemblerSub.setChecked(cm.getAssemblerSub() == 1L ? true : false);
    }

    @Listen("onDelete=#lbAssembler")
    public void onDeleteAssembler(Event ev) throws IOException {
        CosAssembler cm = (CosAssembler) lbAssembler.getSelectedItem().getValue();
        lstAssembler.remove(cm);
        ListModelArray lstModelAssembler = new ListModelArray(lstAssembler);
        lbAssembler.setModel(lstModelAssembler);
    }

    //Ingredient
    private void saveIngredient(Long cosFileId) {
        CosCosfileIngreDAO cmdao = new CosCosfileIngreDAO();
        List<Long> lstCosfileIngreIdFromDB = cmdao.findAllIdByCosFileId(cosFileId);
        for (Long id : lstCosfileIngreIdFromDB) {
            if (!isExistIngredient(id, lstCosfileIngre)) {
                cmdao.delete(id);
            }
        }

        for (CosCosfileIngre obj : lstCosfileIngre) {
            obj.setCosFileId(cosFileId);
            obj.setIsActive(Constants.Status.ACTIVE);
            if ("COPY".equals(crudMode)) {
                obj.setCosfileIngreId(null);
            }
            cmdao.saveOrUpdate(obj);

        }

    }

    //Ingredient
    private void saveVersionIngredient(Long versonCosFileId) {
        CosCosfileIngreDAO cmdao = new CosCosfileIngreDAO();
        for (CosCosfileIngre obj : originalLstCosCosfileIngre) {
            CosCosfileIngre cosfileIngre = new CosCosfileIngre();
            cosfileIngre.setCosFileId(versonCosFileId);
            cosfileIngre.setDescription(obj.getDescription());
            cosfileIngre.setIngredientId(obj.getIngredientId());
            cosfileIngre.setIngredientName(obj.getIngredientName());
            cosfileIngre.setPercent(obj.getPercent());
            cosfileIngre.setVariantOrShade(obj.getVariantOrShade());
            cosfileIngre.setIsActive(Constants.Status.ACTIVE);
            cosfileIngre.setCosfileIngreId(null);
            cmdao.saveOrUpdate(cosfileIngre);

        }

    }

    private Boolean isExistIngredient(Long id, List<CosCosfileIngre> lst) {
        for (CosCosfileIngre itemInList : lst) {

            if (id != null && id.equals(itemInList.getCosfileIngreId())) {
                return true;
            }

        }
        return false;
    }

    @Listen("onClick = #btnAddIngredient")
    public void onAddIngredient() throws Exception {
        if (tbIngredientName.getText().matches("\\s*")) {
            lbCosfileIngreWarning.setValue("Tên thành phần không thể để trống");
            tb.setSelectedPanel(mainpanel);
            tbIngredientName.focus();
            return;
        }
        String msg;
        if ((msg = ValidatorUtil.validateTextbox(tbIngredientPercent, false, 30, "^[0-9]+[0-9\\.]*$")) != null) {
            lbCosfileIngreWarning.setValue(String.format(msg, "Tỷ lệ thành phần"));
            tb.setSelectedPanel(mainpanel);
            tbIngredientPercent.focus();
            return;
        }
        ListModel<CosCosfileIngre> lbCosfileIngreListModel = lbCosfileIngre.getListModel();
        if (lbCosfileIngre.getSelectedItem() == null) {
            // Truong hop them moi
            if ("".equals(tbCosfileIngreId.getValue())) {
                if (lbCosfileIngreListModel != null) {
                    for (int i = 0; i < lbCosfileIngreListModel.getSize(); i++) {
                        try {
                            CosCosfileIngre item = lbCosfileIngreListModel.getElementAt(i);
                            if (item.getIngredientName().toLowerCase().equals(tbIngredientName.getValue().toLowerCase())) {
                                lbCosfileIngreWarning.setValue("Tên thành phần đã có trong danh sách");
                                tb.setSelectedPanel(mainpanel);
                                tbIngredientName.focus();
                                return;
                            }
                        } catch (Exception ex) {
                            LogUtils.addLogDB(ex);
                        }

                    }
                }
            }
        }

        if (tbIngredientPercent.getText() != null && !"".equals(tbIngredientPercent.getText())) {
            try {
                Double a = Double.parseDouble(tbIngredientPercent.getText());
                if (a < 0D) {
                    lbCosfileIngreWarning.setValue("Tỷ lệ không thể < 0");
                    tb.setSelectedPanel(mainpanel);
                    tbIngredientPercent.focus();
                    return;
                }
                if (a > 100D) {
                    lbCosfileIngreWarning.setValue("Tỷ lệ không thể > 100");
                    tb.setSelectedPanel(mainpanel);
                    tbIngredientPercent.focus();
                    return;
                }
                // Kiem tra tong thanh phan khong duoc vuot qua 100%
                if ("".equals(tbCosfileIngreId.getValue())) {
                    Double currentTotalPercent = 0.0;
                    if (lbCosfileIngreListModel != null) {
                        for (int i = 0; i < lbCosfileIngreListModel.getSize(); i++) {
                            CosCosfileIngre item = lbCosfileIngreListModel.getElementAt(i);
                            if (item.getPercent() != null) {
                                currentTotalPercent += item.getPercent();
                            }

                        }
                        if (lbCosfileIngre.getSelectedItem() == null && tbIngredientPercent.getText() != null && !"".equals(tbIngredientPercent.getText())) {
                            currentTotalPercent += Double.parseDouble(tbIngredientPercent.getText());
                        }
                        if (currentTotalPercent > 100.0) {
                            lbCosfileIngreWarning.setValue("Tổng tỷ lệ thành phần không vượt quá 100%");
                            tb.setSelectedPanel(mainpanel);
                            tbIngredientPercent.focus();
                            return;
                        }
                    }
                }
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
                lbCosfileIngreWarning.setValue("Tỷ lệ phải nhập dạng số");
                tb.setSelectedPanel(mainpanel);
                tbIngredientPercent.focus();
                return;
            }
        }

        if ((msg = ValidatorUtil.validateTextbox(tbIngredientDescription, false, 500)) != null) {
            lbCosfileIngreWarning.setValue(String.format(msg, "Mô tả của thành phần không được vượt quá 500 ký tự"));
            tb.setSelectedPanel(mainpanel);
            tbIngredientDescription.focus();
            return;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbIngredientVariantOrShade, false, 255)) != null) {
            lbCosfileIngreWarning.setValue(String.format(msg, "Dạng hoặc màu"));
            tb.setSelectedPanel(mainpanel);
            tbIngredientVariantOrShade.focus();
            return;
        }

        boolean hasVariantOrShare = false;
        if (lstCosfileIngre != null) {
            for (CosCosfileIngre item : lstCosfileIngre) {
                if (item.getVariantOrShade() != null && !"".equals(item.getVariantOrShade())) {
                    hasVariantOrShare = true;
                    break;
                }
            }
        }

        boolean addNew = "".equals(tbCosfileIngreIndex.getValue());

        if (lstCosfileIngre != null && lstCosfileIngre.size() > 0) {

            boolean check = ((addNew && lstCosfileIngre.size() > 0) || (!addNew && lstCosfileIngre.size() > 1));
            // Truong hop co dang mau trong danh sach
            if (hasVariantOrShare) {
                if (check && (tbIngredientVariantOrShade.getValue() == null || "".equals(tbIngredientVariantOrShade.getValue()))) {
                    lbCosfileIngreWarning.setValue("Dạng hoặc màu không được để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbIngredientVariantOrShade.focus();
                    return;
                }
            }
            // Truong hop khong co dang mau trong danh sach
            if (!hasVariantOrShare) {
                tbIngredientVariantOrShade.setDisabled(check);
                if (check && (tbIngredientVariantOrShade.getValue() != null && !"".equals(tbIngredientVariantOrShade.getValue()))) {
                    tbIngredientVariantOrShade.setValue("");
                }
            }
        }

        lbCosfileIngreWarning.setValue("");
        CosCosfileIngre obj;
        if (tbCosfileIngreIndex.getValue() != null && !tbCosfileIngreIndex.getValue().isEmpty() && lstCosfileIngre.size() > 0) {
            int index = Integer.parseInt(tbCosfileIngreIndex.getValue());
            obj = lstCosfileIngre.get(index);
        } else {
            obj = new CosCosfileIngre();
        }
        if (tbCosfileIngreId.getValue() != null && !tbCosfileIngreId.getValue().isEmpty()) {
            obj.setCosfileIngreId(Long.parseLong(tbCosfileIngreId.getValue()));
        }
        obj.setIngredientName(tbIngredientName.getValue());
//        obj.setIngredientId((Long) lbIngredient.getSelectedItem().getValue());
//        obj.setIngredientName((String) lbIngredient.getSelectedItem().getLabel());
        if (tbIngredientPercent.getValue() != null && !"".equals(tbIngredientPercent.getValue())) {
            obj.setPercent(Double.parseDouble(tbIngredientPercent.getValue()));
        }
        obj.setDescription(textBoxGetValue(tbIngredientDescription));
        obj.setVariantOrShade(textBoxGetValue(tbIngredientVariantOrShade));
        if (tbCosfileIngreIndex.getValue() != null && !tbCosfileIngreIndex.getValue().isEmpty()) {
            //donothing
        } else {
            lstCosfileIngre.add(obj);
        }
        ListModelArray lstModelManufature = new ListModelArray(lstCosfileIngre);
        lbCosfileIngre.setModel(lstModelManufature);
        resetIngredient();
    }

    private void resetIngredient() {
        tbIngredientName.setValue("");
        tbIngredientPercent.setValue("");
        tbCosfileIngreId.setValue("");
        tbCosfileIngreIndex.setValue("");
        tbIngredientDescription.setValue("");
    }

    @Listen("onEdit=#lbCosfileIngre")
    public void onEditIngredient(Event ev) throws IOException {
        CosCosfileIngre cm = (CosCosfileIngre) lbCosfileIngre.getSelectedItem().getValue();
        tbIngredientName.setValue(cm.getIngredientName());
//        Long ingredientId = cm.getIngredientId();
//        if (ingredientId != null) {
//        }
//        if (ingredientId != null) {
//            for (int i = 0; i < lbIngredient.getListModel().getSize(); i++) {
//                Category ct = (Category) lbIngredient.getListModel().getElementAt(i);
//                if (ingredientId.equals(ct.getCategoryId())) {
//                    lbIngredient.setSelectedIndex(i);
//                    break;
//                }
//            }
//        }
        tbCosfileIngreId.setValue(cm.getCosfileIngreId() == null ? "" : String.valueOf(cm.getCosfileIngreId()));
        tbIngredientPercent.setValue(cm.getPercent() == null ? "" : String.valueOf(cm.getPercent()));
        tbIngredientDescription.setValue(cm.getDescription() == null ? "" : cm.getDescription());
        tbIngredientVariantOrShade.setValue(cm.getVariantOrShade() == null ? "" : cm.getVariantOrShade());
        tbCosfileIngreIndex.setValue(String.valueOf(lbCosfileIngre.getSelectedIndex()));
        if (tbIngredientVariantOrShade.isDisabled()) {
            tbIngredientVariantOrShade.setDisabled((lstCosfileIngre != null && lstCosfileIngre.size() > 1));
        }
    }

    @Listen("onDelete=#lbCosfileIngre")
    public void onDeleteIngredient(Event ev) throws IOException {
        CosCosfileIngre cm = (CosCosfileIngre) lbCosfileIngre.getSelectedItem().getValue();
        lstCosfileIngre.remove(cm);
        ListModelArray lstModelIngredient = new ListModelArray(lstCosfileIngre);
        lbCosfileIngre.setModel(lstModelIngredient);
        if (tbIngredientVariantOrShade.isDisabled()) {
            boolean isDisable = false;
            if (lstCosfileIngre.size() > 1) {
                isDisable = true;

            }
            tbIngredientVariantOrShade.setDisabled(isDisable);
        }
    }

//    them moi ho so tu file excel
    /**
     * Xu ly su kien upload file
     *
     * @param event
     */
    @Listen("onUpload = #btnImportExcel")
    public void onUploadExcel(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        listFileExcel = new ArrayList<Media>();
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getFormat();
            if (!("xlsx".equals(extFile.toLowerCase())
                    || "xls".equals(extFile.toLowerCase()))) {
                showNotification("Định dạng file không được phép tải lên",
                        Constants.Notification.WARNING);
                continue;
            }

            // luu file vao danh sach file
            listFileExcel.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listFileExcel.remove(media);
                }
            });
            hl.appendChild(rm);
            fListImportExcel.appendChild(hl);
        }
    }

    @Listen("onClick=#btnCreateFromImport")
    public void onCreateFromImportExcel(Event event) throws IOException, Exception {
        if (listFileExcel == null || listFileExcel.isEmpty()) {
            lbImportExcelWarning.setValue("Chọn tệp tải lên trước khi thêm mới!");
            return;
        }
        lbImportExcelWarning.setValue(null);
        for (Media media : listFileExcel) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String fileName = media.getName();
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            folderPath += separator + dateFormat.format(date);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                //tạo forlder
                fd.mkdirs();
            }
            File f = new File(folderPath + separator + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            //save to hard disk 
            InputStream inputStream;
            OutputStream outputStream;
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(f));

            // thong tin ve san pham my pham
            XSSFSheet sheet = wb.getSheetAt(0);
            cosFile.setBrandName(sheet.getRow(1).getCell(1).toString());
            tbBrandName.setValue(cosFile.getBrandName());
            cosFile.setProductName(sheet.getRow(1).getCell(8).toString());
            tbProductName.setValue(cosFile.getProductName());
            cosFile.setListVariantOrShade(sheet.getRow(2).getCell(1).toString());
            tbListVariantOrShade.setValue(cosFile.getListVariantOrShade());
            String tempStr = "";
            for (int i = 1; i < Constants.IMPORT_EXCEL.PRODUCT_TYPE_ROW_INDEX; i++) {
                if ("TRUE".equals(sheet.getRow(i).getCell(Constants.IMPORT_EXCEL.PRODUCT_TYPE_COL_INDEX).toString())) {
                    if (i != (Constants.IMPORT_EXCEL.PRODUCT_TYPE_ROW_INDEX - 1)) {
                        tempStr += ";" + i;
                    } else {
                        tempStr += ";-1";
                    }
                }
            }
            if (!"".equals(tempStr)) {
                tempStr = tempStr.trim().substring(1, tempStr.length());
                cosFile.setProductType(tempStr);
                if (tempStr.contains("-1")) {
                    tbOtherProductType.setDisabled(false);
                    tbOtherProductType.setValue(sheet.getRow(5).getCell(1).toString());
                } else {
                    tbOtherProductType.setDisabled(true);
                    tbOtherProductType.setValue(null);
                }
            } else {
                cosFile.setProductType(null);
            }
            cosFile.setIntendedUse(sheet.getRow(6).getCell(1).toString());
            tbIntendedUse.setValue(cosFile.getIntendedUse());
            tempStr = "";
            String[] productPresenttationList = Constants.IMPORT_EXCEL.getProductPresenttationList();
            for (int i = 1; i < Constants.IMPORT_EXCEL.PRODUCT_PRESENTATION_ROW_INDEX; i++) {
                if ("TRUE".equals(sheet.getRow(i).getCell(Constants.IMPORT_EXCEL.PRODUCT_PRESENTATION_INDEX).toString())) {

                    switch (i) {
                        case 1:
                            tempStr += productPresenttationList[0];
                            break;
                        case 2:
                            tempStr += productPresenttationList[1];
                            break;
                        case 3:
                            tempStr += productPresenttationList[2];
                            break;
                        case 4:
                            tempStr += productPresenttationList[3];
                            break;
                        case 5:
                            tempStr += productPresenttationList[4];
                            break;
                    }
                }
            }
            if (!"".equals(tempStr)) {
                tempStr = tempStr.trim().substring(1, tempStr.length());
                cosFile.setProductPresentation(tempStr);
                if (tempStr.contains("-1")) {
                    tbOtherProductPresentation.setDisabled(false);
                    tbOtherProductPresentation.setValue(sheet.getRow(8).getCell(1).toString());
                } else {
                    tbOtherProductPresentation.setDisabled(true);
                    tbOtherProductPresentation.setValue(null);
                }
            } else {
                cosFile.setProductPresentation(null);
            }
            tbDistributorName.setValue(sheet.getRow(12).getCell(1).toString());
            tbDistributorAddress.setValue(sheet.getRow(12).getCell(8).toString());
            tbDistributorPhone.setValue(sheet.getRow(13).getCell(1).toString());
            tbDistributorFax.setValue(sheet.getRow(13).getCell(8).toString());
            tbDistributorRegisNumber.setValue(sheet.getRow(14).getCell(1).toString());

            tbDistributePersonName.setValue(sheet.getRow(16).getCell(1).toString());
            tbDistributePersonPhone.setValue(sheet.getRow(16).getCell(8).toString());
            tbDistributePersonEmail.setValue(sheet.getRow(17).getCell(1).toString());
            tbDistributePersonPosition.setValue(sheet.getRow(17).getCell(8).toString());

            tbImporterName.setValue(sheet.getRow(19).getCell(1).toString());
            tbImporterAddress.setValue(sheet.getRow(19).getCell(8).toString());
            tbImporterPhone.setValue(sheet.getRow(20).getCell(1).toString());
            tbImporterFax.setValue(sheet.getRow(20).getCell(8).toString());

            tbSignPlace.setValue(sheet.getRow(23).getCell(1).toString());
            if (!"".equals(sheet.getRow(23).getCell(8).toString())) {
                dbSignDate.setValue(new Date(sheet.getRow(23).getCell(8).toString()));
            }
            tbSignName.setValue(sheet.getRow(24).getCell(1).toString());

            // thong tin ve nha san xuat
            XSSFSheet sheet1 = wb.getSheetAt(1);
            int j = 2;
            lstManufacturer.clear();
            while (sheet1.getRow(j) != null && sheet1.getRow(j).getCell(1) != null
                    && !"".equals(sheet1.getRow(j).getCell(1).toString())) {
                CosManufacturer cosManu = new CosManufacturer();
                cosManu.setName(sheet1.getRow(j).getCell(1).toString());
                cosManu.setAddress(sheet1.getRow(j).getCell(2).toString());
                cosManu.setPhone(sheet1.getRow(j).getCell(3).toString());
                cosManu.setFax(sheet1.getRow(j).getCell(4).toString());
                lstManufacturer.add(cosManu);
                j++;
            }
            ListModelArray lstModelManufacturer = new ListModelArray(lstManufacturer);
            lbManufacturer.setModel(lstModelManufacturer);

            // thong tin ve don vi dong goi
            XSSFSheet sheet2 = wb.getSheetAt(2);
            j = 2;
            lstAssembler.clear();
            boolean isAssemblerMain = false;
            while (sheet2.getRow(j) != null && sheet2.getRow(j).getCell(1) != null
                    && !"".equals(sheet2.getRow(j).getCell(1).toString())) {
                CosAssembler cosAss = new CosAssembler();
                cosAss.setName(sheet2.getRow(j).getCell(1).toString());
                cosAss.setAddress(sheet2.getRow(j).getCell(2).toString());
                cosAss.setPhone(sheet2.getRow(j).getCell(3).toString());
                cosAss.setFax(sheet2.getRow(j).getCell(4).toString());
                if (sheet2.getRow(j).getCell(7) != null
                        && "TRUE".equals(sheet2.getRow(j).getCell(7).toString())
                        && !isAssemblerMain) {
                    cosAss.setAssemblerMain(1L);
                    isAssemblerMain = true;
                } else {
                    cosAss.setAssemblerMain(0L);
                }
                if (sheet2.getRow(j).getCell(8) != null
                        && "TRUE".equals(sheet2.getRow(j).getCell(8).toString())) {
                    cosAss.setAssemblerSub(1L);
                } else {
                    cosAss.setAssemblerSub(0L);
                }
                lstAssembler.add(cosAss);
                j++;
            }
            ListModelArray lstModelAssembler = new ListModelArray(lstAssembler);
            lbAssembler.setModel(lstModelAssembler);

            // danh sach thanh phan
            XSSFSheet sheet3 = wb.getSheetAt(3);
            j = 2;
            lstCosfileIngre.clear();
            while (sheet3.getRow(j) != null && sheet3.getRow(j).getCell(1) != null
                    && !"".equals(sheet3.getRow(j).getCell(1).toString())) {
                CosCosfileIngre cosCosfile = new CosCosfileIngre();
                cosCosfile.setIngredientName(sheet3.getRow(j).getCell(1).toString());
                cosCosfile.setPercent(Double.parseDouble(sheet3.getRow(j).getCell(2).toString()));
                lstCosfileIngre.add(cosCosfile);
                j++;
            }
            ListModelArray lstModelIngredient = new ListModelArray(lstCosfileIngre);
            lbCosfileIngre.setModel(lstModelIngredient);
            loadProductPresentationCheck();
            loadProductTypeCheck();
            tb.setSelectedPanel(mainpanel);
            tbProductName.focus();
            inputStream.close();
            outputStream.close();
        }
        return;
    }

    @Listen("onDownloadFile = #lbDownloadExcelTemplate")
    public void onDownloadFile() throws FileNotFoundException {
        String folderPath = Executions.getCurrent().getDesktop().getWebApp().getRealPath(Constants.UPLOAD.ATTACH_PATH);
        String path = folderPath + "\\BM_CongBo_MyPham.xlsx";
        File f = new File(path);
//        if (f != null) {
        if (f.exists()) {
            File tempFile = FileUtil.createTempFile(f, f.getName());
            Filedownload.save(tempFile, path);
        } else {
            Clients.showNotification("File không còn tồn tại trên hệ thống!", Constants.Notification.INFO, null, "middle_center", 1500);
        }
//        } else {
//            Clients.showNotification("File không còn tồn tại trên hệ thống!", Constants.Notification.INFO, null, "middle_center", 1500);
//        }
    }

    public boolean saveObject() throws IOException, Exception {
        boolean returnValue = true;
        //neu chua co fileID thi them moi
        if (files.getFileId() == null) {
            //linhdx
            //Luu thong tin ho so truoc khi tao
            if (validateData()) {
                createObject();

                return true;
            } else {
                return false;
            }
        }
        return returnValue;

    }

    public boolean validateData() {

        String msg;
        if ((msg = ValidatorUtil.validateTextbox(tbBrandName, true, 255)) != null) {
            showWarningMessage(String.format(msg, "Nhãn hàng"));
            tb.setSelectedPanel(mainpanel);
            tbBrandName.focus();
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbProductName, true, 255)) != null) {
            showWarningMessage(String.format(msg, "Tên sản phẩm"));
            tb.setSelectedPanel(mainpanel);
            tbProductName.focus();
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbListVariantOrShade, false, 255)) != null) {
            showWarningMessage(String.format(msg, "Danh sách các dạng hoặc màu"));
            tb.setSelectedPanel(mainpanel);
            tbListVariantOrShade.focus();
            return false;
        }

        if (lbProductType.getSelectedItems() != null) {
            Object[] productTypeSelected = lbProductType.getSelectedItems().toArray();
            if (productTypeSelected.length == 0) {
                showWarningMessage("Phải chọn tối thiểu một dạng sản phẩm");
                tb.setSelectedPanel(mainpanel);
                lbProductType.focus();
                return false;
            }
            for (Object iitem : productTypeSelected) {
                Listitem item = (Listitem) iitem;
                CosProductTypeSubModel productTypeItem = item.getValue();
                if (productTypeItem != null && productTypeItem.getProductTypeId() == -1) {
                    if ((msg = ValidatorUtil.validateTextbox(tbOtherProductType, true, 255)) != null) {
                        tb.setSelectedPanel(mainpanel);
                        tbOtherProductType.focus();
                        showWarningMessage(String.format(msg, "Dạng sản phẩm khác"));
                        Clients.showNotification(String.format(msg, "Dạng sản phẩm khác"), Clients.NOTIFICATION_TYPE_INFO, tbOtherProductType, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
                        return false;
                    }
                    break;
                }
            }
        }

        if ((msg = ValidatorUtil.validateTextbox(tbIntendedUse, true, 500)) != null) {
            showWarningMessage(String.format(msg, "Mục đích sử dụng"));
            Clients.showNotification(String.format(msg, "Mục đích sử dụng"), Clients.NOTIFICATION_TYPE_INFO, tbIntendedUse, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            tb.setSelectedPanel(mainpanel);
            tbIntendedUse.focus();
            return false;
        }

        Object[] productPresentationSelected = lbProductPresentation.getSelectedItems().toArray();
        if (productPresentationSelected.length == 0) {
            tb.setSelectedPanel(mainpanel);
            lbProductPresentation.focus();
            showWarningMessage("Phải chọn tối thiểu một dạng trình bày");
            Clients.showNotification("Phải chọn tối thiểu một dạng trình bày", Clients.NOTIFICATION_TYPE_INFO, lbProductPresentation, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        for (Object iitem : productPresentationSelected) {
            Listitem item = (Listitem) iitem;
            CosProductPresentaton presentationItem = item.getValue();
            if (presentationItem != null && presentationItem.getProductPresentationId() == -1) {
                if ((msg = ValidatorUtil.validateTextbox(tbOtherProductPresentation, true, 255)) != null) {
                    tb.setSelectedPanel(mainpanel);
                    tbOtherProductPresentation.focus();
                    showWarningMessage(String.format(msg, "Dạng trình bày khác"));
                    Clients.showNotification(String.format(msg, "Dạng trình bày khác"), Clients.NOTIFICATION_TYPE_INFO, tbOtherProductPresentation, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
                    return false;
                }
                break;
            }
        }

        ListModel<CosManufacturer> lbCosManufacturerListModel = lbManufacturer.getListModel();
        if (lbCosManufacturerListModel == null || lbCosManufacturerListModel.getSize() == 0) {
            tb.setSelectedPanel(mainpanel);
            tbManufacturerName.focus();
            showWarningMessage("Cần có ít nhất một nhà sản xuất");
            Clients.showNotification("Cần có ít nhất một nhà sản xuất", Clients.NOTIFICATION_TYPE_INFO, tbManufacturerName, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        ListModel<CosAssembler> lbCosAssemblerListModel = lbAssembler.getListModel();

        if (lbCosAssemblerListModel == null || lbCosAssemblerListModel.getSize() == 0) {
            tb.setSelectedPanel(mainpanel);
            tbAssemblerName.focus();
            showWarningMessage("Cần có ít nhất một đơn vị đóng gói");
            Clients.showNotification("Cần có ít nhất một đơn vị đóng gói", Clients.NOTIFICATION_TYPE_INFO, tbAssemblerName, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        boolean hasMainAssembler = false;
        if (null != lbCosAssemblerListModel) {
            for (int i = 0; i < lbCosAssemblerListModel.getSize(); i++) {
                CosAssembler tmpItem = lbCosAssemblerListModel.getElementAt(i);
                hasMainAssembler = tmpItem.getAssemblerMain() == 1L;
                if (hasMainAssembler) {
                    break;
                }
            }
        }
        if (!hasMainAssembler) {
            tb.setSelectedPanel(mainpanel);
            tbAssemblerName.focus();
            showWarningMessage("Cần có ít nhất một đơn vị đóng gói chính");
            Clients.showNotification("Cần có ít nhất một đơn vị đóng gói chính", Clients.NOTIFICATION_TYPE_INFO, tbAssemblerName, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbDistributorName, true, 255)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbDistributorName.focus();
            showWarningMessage(String.format(msg, "Tên công ty đưa sản phẩm ra thị trường"));
            Clients.showNotification(String.format(msg, "Tên công ty đưa sản phẩm ra thị trường"), Clients.NOTIFICATION_TYPE_INFO, tbDistributorName, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbDistributorAddress, true, 255)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbDistributorAddress.focus();
            showWarningMessage(String.format(msg, "Địa chỉ công ty đưa sản phẩm ra thị trường"));
            Clients.showNotification(String.format(msg, "Địa chỉ công ty đưa sản phẩm ra thị trường"), Clients.NOTIFICATION_TYPE_INFO, tbDistributorAddress, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbDistributePersonName, true, 255)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbDistributePersonName.focus();
            showWarningMessage(String.format(msg, "Tên người đại diện"));
            Clients.showNotification(String.format(msg, "Tên người đại diện"), Clients.NOTIFICATION_TYPE_INFO, tbDistributePersonName, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbDistributePersonPhone, true, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbDistributePersonPhone.focus();
            showWarningMessage(String.format(msg, "Số điện thoại người đại diện"));
            Clients.showNotification(String.format(msg, "Số điện thoại người đại diện"), Clients.NOTIFICATION_TYPE_INFO, tbDistributePersonPhone, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbDistributePersonEmail, true, 50, ValidatorUtil.PATTERN_CHECK_EMAIL)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbDistributePersonEmail.focus();
            showWarningMessage(String.format(msg, "Email người đại diện"));
            Clients.showNotification(String.format(msg, "Email người đại diện"), Clients.NOTIFICATION_TYPE_INFO, tbDistributePersonEmail, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbDistributePersonPosition, true, 100)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbDistributePersonPosition.focus();
            showWarningMessage(String.format(msg, "Chức vụ người đại diện"));
            Clients.showNotification(String.format(msg, "Chức vụ người đại diện"), Clients.NOTIFICATION_TYPE_INFO, tbDistributePersonPosition, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbImporterName, true, 255)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbImporterName.focus();
            showWarningMessage(String.format(msg, "Tên công ty nhập khẩu"));
            Clients.showNotification(String.format(msg, "Tên công ty nhập khẩu"), Clients.NOTIFICATION_TYPE_INFO, tbImporterName, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbImporterAddress, true, 255)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbImporterAddress.focus();
            showWarningMessage(String.format(msg, "Địa chỉ công ty nhập khẩu"));
            Clients.showNotification(String.format(msg, "Địa chỉ công ty nhập khẩu"), Clients.NOTIFICATION_TYPE_INFO, tbImporterAddress, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbImporterPhone, true, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbImporterPhone.focus();
            showWarningMessage(String.format(msg, "Số điện thoại công ty nhập khẩu"));
            Clients.showNotification(String.format(msg, "Số điện thoại công ty nhập khẩu"), Clients.NOTIFICATION_TYPE_INFO, tbImporterPhone, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbImporterFax, true, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbImporterFax.focus();
            showWarningMessage(String.format(msg, "Số Fax công ty nhập khẩu"));
            Clients.showNotification(String.format(msg, "Số Fax công ty nhập khẩu"), Clients.NOTIFICATION_TYPE_INFO, tbImporterFax, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        ListModel<CosCosfileIngre> lbCosfileIngreListModel = lbCosfileIngre.getListModel();

        if (lbCosfileIngreListModel == null || lbCosfileIngreListModel.getSize() == 0) {
            tb.setSelectedPanel(mainpanel);
            lbCosfileIngre.focus();
            showWarningMessage("Cần phải nhập thành phần đầy đủ");
            Clients.showNotification("Cần phải nhập thành phần đầy đủ", Clients.NOTIFICATION_TYPE_INFO, lbCosfileIngre, NOTIFICATION_POSITION.MIDDLE_CENTER.getValue(), 2000, true);
            return false;
        }

        // Kiem tra tong thanh phan phai du 100%
        if (lbCosfileIngreListModel != null) {
            Double totalPercent = 0.0;
            for (int i = 0; i < lbCosfileIngreListModel.getSize(); i++) {
                CosCosfileIngre item = lbCosfileIngreListModel.getElementAt(i);
                if (item != null && item.getPercent() != null) {
                    totalPercent += item.getPercent();
                }
            }
            if (totalPercent > 100.0) {
                showWarningMessage("Tổng thành phần không được vượt quá 100%");
                tb.setSelectedPanel(mainpanel);
                tbIngredientName.focus();
                return false;
            }
        }

        if (!cbIngreConfirm1.isChecked() || !cbIngreConfirm2.isChecked()) {
            showWarningMessage("Bạn cần đồng ý với 2 điều kiện đảm bảo danh sách thành phần!");
            cbIngreConfirm1.focus();
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbSignPlace, false, 255)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbSignPlace.focus();
            showWarningMessage(String.format(msg, "Nơi ký"));
            return false;
        }

        if ((msg = ValidatorUtil.validateTextbox(tbSignName, false, 255)) != null) {
            tb.setSelectedPanel(mainpanel);
            tbSignName.focus();
            showWarningMessage(String.format(msg, "Người ký"));
            return false;
        }

        showWarningMessage("");
        return true;
    }

    // <editor-fold defaultstate="collapsed" desc="clear gia tri">
    private void clearAllValue() {
//        if (crudMode.equals("CREATE")) {
//            tbBrandName.setValue("");
//            tbProductName.setValue("");
//            tbListVariantOrShade.setValue("");
//            lbProductType.clearSelection();
//            tbOtherProductType.setValue("");
//            tbIntendedUse.setValue("");
//            lbProductPresentation.clearSelection();
//            tbOtherProductPresentation.setValue("");
//            tbManufacturerName.setValue("");
//            tbManufacturerAddress.setValue("");
//            tbManufacturerPhone.setValue("");
//            tbManufacturerFax.setValue("");
//            tbAssemblerName.setValue("");
//            tbAssemblerAddress.setValue("");
//            tbAssemblerPhone.setValue("");
//            tbAssemblerFax.setValue("");
//            lbCosfileIngre.setModel(new ListModelArray(0));
//            lbManufacturer.setModel(new ListModelArray(0));
//            lbAssembler.setModel(new ListModelArray(0));
//        }
    }
    // </editor-fold>

    /**
     * Đinh kem ho so goc
     */
    @Listen("onClick = #btnOpenAttachFileFinal")
    public void onCreateAttachFileFinal() throws IOException, Exception {

        if (saveObject() == false) {
            return;
        }
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("CRUDMode", "CREATE");
        arguments.put("fileId", files.getFileId());
        arguments.put("cosFileId", cosFile.getCosFileId());
        arguments.put("parentWindow", windowCRUDCosmetic);
        Window window = createWindow("wdAttachFinalFileCRUD",
                "/Pages/module/cosmetic/attachFinalFileCRUD.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onLoadFinalFile = #windowCRUDCosmetic")
    public void onLoadFinalFile() {
        fillFinalFileListbox(files.getFileId());
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId, Constants.OBJECT_TYPE.COSMETIC_HO_SO_GOC);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Listen("onUploadCert = #windowCRUDCosmetic")
    public void onUploadCert(Event event) throws Exception {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        actionSignCA(event, actionPrepareSign());
    }

    public String actionPrepareSign() {
        String fileToSign = "";
        try {
            fileToSign = createFileToSign();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    public String createFileToSign() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return "fail";
        }

        FilesModel fileModel = new FilesModel(files.getFileId());
        ExportFileDAO exp = new ExportFileDAO();
        return exp.exportFinalFileNoSign(fileModel, false);
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        SignPdfFile pdfSig = new SignPdfFile();//pdfSig = new SignPdfFile();        
        String base64Hash;
        String certSerial = x509Cert.getSerialNumber().toString(16);

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");
        FileUtil.mkdirs(filePath);
        String outputFileFinalName = "_" + (new Date()).getTime() + ".pdf";
        String outPutFileFinal = filePath + outputFileFinalName;
//        String linkImage = rb.getString("signImage");
//        String linkImageSign = linkImage + "232.png";
//        String linkImageStamp = linkImage + "attpStamp.png";
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");//not use binhnt53 u230315
            String linkImageSign = folderPath + separator + caur.get(0).getSignature();
            String linkImageStamp = folderPath + separator + caur.get(0).getStamper();
            //fileSignOut = outPutFileFinal;
            Pdf pdfProcess = new Pdf();
            try {//chen chu ki
                String sToFind = Constants.REPLATE_CHARACTER_WHEN_FIND_LOCATION_TO_SIGN.LOCATION_BUSINESS;
                pdfProcess.insertImageAll(fileToSign, outPutFileFinal, linkImageSign, linkImageStamp, sToFind);
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
            if (pdfProcess.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }

            base64Hash = pdfSig.createHash(outPutFileFinal, new Certificate[]{x509Cert});

            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64Hash.setValue(base64Hash);
            txtCertSerial.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitFinalFile();");
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    @Listen("onSign = #windowCRUDCosmetic")
    public void onSign(Event event) {
        String data = event.getData().toString();

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
//        String inputFileName = "_" + (new Date()).getTime() + ".pdf";//NOT ƯSE BINHNT53 230315
        String outputFileName = "_signed_" + (new Date()).getTime() + ".pdf";
//        String inputFile = filePath + inputFileName;//NOT ƯSE BINHNT53 230315
        String outputFile = filePath + outputFileName;
        fileSignOut = outputFile;
        String signature = data;
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");

        try {
            pdfSig.insertSignature(signature, outputFile);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        try {
            onSignFinalFile();
            fileSignOut = "";
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
    }

    /**
     *
     * @throws Exception
     */
    public void onSignFinalFile() throws Exception {
        ExportFileDAO exp = new ExportFileDAO();
        FilesModel fileModel = new FilesModel(files.getFileId());
        fileModel.setCosmeticPermitId(files.getFileId());
        fileModel.setCosmeticPermitType(Constants.OBJECT_TYPE.COSMETIC_HO_SO_GOC);
        exp.updateAttachSignFile(fileModel, fileSignOut);
        showNotification("Ký số thành công!", Constants.Notification.INFO);
        fillFinalFileListbox(files.getFileId());

    }

    public CosAdditionalRequest getAdditionalRequest() {
        return additionalRequest;
    }

    public void setAdditionalRequest(CosAdditionalRequest additionalRequest) {
        this.additionalRequest = additionalRequest;
    }

    public BookDocument getBookDocument() {
        return bookDocument;
    }

    public void setBookDocument(BookDocument bookDocument) {
        this.bookDocument = bookDocument;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public CosPermit getPermit() {
        return permit;
    }

    public void setPermit(CosPermit permit) {
        this.permit = permit;
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public int checkDispathSDBS() {
        return checkDispathSDBS(files.getFileId());
    }

    public int checkBooked() {
        return checkBooked(files.getFileId(), files.getFileType());
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public ListModel getModel() {
        return model;
    }

    public void setModel(ListModel model) {
        this.model = model;
    }

    public CosFile getCosFile() {
        return cosFile;
    }

    public void setCosFile(CosFile cosFile) {
        this.cosFile = cosFile;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getIsSave() {
        return isSave;
    }

    public void setIsSave(String isSave) {
        this.isSave = isSave;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    //luu danh muc nha san xuat
    public void addManufacturer(Long CosFileId) {
        CosManufacturerDAO cmdao = new CosManufacturerDAO();
        CosCatManufacturerDAO coscat = new CosCatManufacturerDAO();
        List<CosManufacturer> list = cmdao.findByCosFileId(CosFileId);
        Long count;
        String sName;
        for (int i = 0; i < list.size(); i++) {
            CosManufacturer obj = (CosManufacturer) list.get(i);
            sName = CheckValidNameManufacturer(obj.getName());
            count = cmdao.CheckManufacturerName(sName);
            if (count <= 0) {
                CosCatManufacturer objCat = new CosCatManufacturer();
                objCat.setName(obj.getName());
                objCat.setNameValid(sName);
                objCat.setAddress(obj.getAddress());
                objCat.setFax(obj.getFax());
                objCat.setPhone(obj.getPhone());
                objCat.setIsActive(Constants.Status.ACTIVE);
                objCat.setUserId(getUserId());
                coscat.saveOrUpdate(objCat);
                //them no vao db
            }
        }
    }

    public String CheckValidNameManufacturer(String Name) {
        String sNameValid = Name;
        sNameValid = sNameValid.trim();
        sNameValid = sNameValid.toLowerCase();
        sNameValid = sNameValid.replace(",", "");
        sNameValid = sNameValid.replace(".", "");
        sNameValid = sNameValid.replace("-", "");
        sNameValid = sNameValid.replaceAll("\\s+", " ");
        return sNameValid;
    }

    //d
}
