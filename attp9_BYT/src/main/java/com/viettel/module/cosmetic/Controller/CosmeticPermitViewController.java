/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.CosPermit;
import com.viettel.module.cosmetic.BO.CosReject;
import com.viettel.module.cosmetic.BO.VFileCosFilePermit;
import com.viettel.module.cosmetic.BO.VFileCosFileReject;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.DAO.VFileCosfileDAO;
import com.viettel.module.cosmetic.Model.CosmeticSearchModel;
import com.viettel.utils.Constants;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author giangnh20
 */
public class CosmeticPermitViewController extends BaseComposer {

    public static final String TYPE_PERMIT = "PERMIT";
    public static final String TYPE_REJECT = "REJECT";
    
    @Wire
    private Window cosmeticPermitViewWindow;
    
    @Wire
    private Listbox lbSort, lbPermit;
    
    @Wire
    private Textbox tbBusinessName, tbProductName, tbBusinessAddress, tbReceiveNo;
    
    @Wire
    private Datebox dbReceiveDateTo, dbReceiveDateFrom;
    
    private CosmeticSearchModel searchModel = new CosmeticSearchModel();
    private List cosFilePermitOrRejectLst = new ArrayList();
    
    private String cosFilePermitType;
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillDataToList();
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        cosFilePermitType = String.valueOf(arguments.get("TYPE"));
        if (!cosFilePermitType.equals(TYPE_PERMIT) && !cosFilePermitType.equals(TYPE_REJECT)) {
            cosFilePermitType = TYPE_PERMIT;
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }
    
    @Listen("onClick=#btnSearch")
    public void onSearch() {
        fillDataToList();
    }
    
    @Listen("onSelect=#lbPermit")
    public void onSelectedPermitItem(SelectEvent e) {
        if (lbPermit.getSelectedItem() != null) {
            Object item = lbPermit.getSelectedItem().getValue();
            if (item instanceof VFileCosFilePermit) {
                createWindowView(((VFileCosFilePermit) item).getFileId(), cosmeticPermitViewWindow);
            } else {
                createWindowView(((VFileCosFileReject) item).getFileId(), cosmeticPermitViewWindow);
            }
            cosmeticPermitViewWindow.setVisible(false);
        }
    }
    
    public void fillDataToList() {
        
        if (dbReceiveDateFrom != null && dbReceiveDateFrom.getValue() != null && !"".equals(dbReceiveDateFrom.getValue())) {
            searchModel.setReceiveDateFrom(dbReceiveDateFrom.getValue());
        }
        if (dbReceiveDateTo != null && dbReceiveDateTo.getValue() != null && !"".equals(dbReceiveDateTo.getValue())) {
            searchModel.setReceiveDateTo(dbReceiveDateTo.getValue());
        }
        if (!lbSort.getSelectedItem().getValue().equals(Constants.HOLIDAY.COMBOBOX_HEADER_SELECT)) {
            Long arangge = Long.valueOf(lbSort.getSelectedItem().getValue().toString());
            searchModel.setArrange(arangge);
        }
        searchModel.setProductName(textBoxGetValue(tbProductName));
        searchModel.setBusinessName(textBoxGetValue(tbBusinessName));
        searchModel.setBusinessAddress(textBoxGetValue(tbBusinessAddress));
        searchModel.setReceiveNo(textBoxGetValue(tbReceiveNo));
        
        VFileCosfileDAO vfileCosfileDAO = new VFileCosfileDAO();
        List result;
        if (cosFilePermitType.equals(TYPE_PERMIT)) {
            result = vfileCosfileDAO.searchPermit(searchModel);
        } else {
            result = vfileCosfileDAO.searchReject(searchModel);
        }
        if (cosFilePermitOrRejectLst.size() > 0) {
            cosFilePermitOrRejectLst.clear();
        }
        for (int i=0;i<result.size();i++) {
            Object[] item = (Object[]) result.get(i);
            if (item.length == 2) {
                Object iitem = item[1];
                if (iitem instanceof CosPermit) {
                    VFileCosFilePermit filePermit = new VFileCosFilePermit((VFileCosfile) item[0], (CosPermit) item[1]);
                    cosFilePermitOrRejectLst.add(filePermit);
                } else if (iitem instanceof CosReject) {
                    VFileCosFileReject fileReject = new VFileCosFileReject((VFileCosfile) item[0], (CosReject) item[1]);
                    cosFilePermitOrRejectLst.add(fileReject);
                }
            }
        }
        
        ListModelArray lstModel = new ListModelArray(cosFilePermitOrRejectLst);
        lbPermit.setModel(lstModel);
    }
    
    private void createWindowView(Long id, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("parentWindow", parentWindow);
//        arguments.put("menuType", menuType);
//        arguments.put("filetype", FILE_TYPE_STR);
//        arguments.put("deptid", DEPT_ID);
        createWindow("windowView", "/Pages/module/cosmetic/cosmeticView.zul",
                arguments, Window.EMBEDDED);
    }
    
    @Listen("onVisible=#cosmeticPermitViewWindow")
    public void onVisible() {
        cosmeticPermitViewWindow.setVisible(true);
    }

    public String getCosFilePermitType() {
        return cosFilePermitType;
    }
}
