/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author giangnh20
 */
public class CosmeticSpecialSymbolController extends BaseComposer {

    public static final int ITEM_PER_ROWS = 8;

    @Wire
    private Window specialSymbolWnd;

    @Wire("#buttonLayout")
    private Vlayout columnLayout;

    private Window parentWindow;

    private Textbox textBox;

    private List<String> specialChar = new ArrayList<String>();

    private final String defaultSpecialSymbol = "®;©;™;℠;⌀;°;π;±;√;‰;Ω;∞;≈;÷;~;≠;¹;²;³;½;¼;¾;‰;℅;℁;⅍;£;¥;¢;₰;$;€;₫;€;µ;α;β;÷;≥;≤;≠;±;∞";

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        initializeData();
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        textBox = (Textbox) arguments.get("textBox");
        if (specialChar.size() > 0) {
            specialChar.clear();
        }
        String specialSymbolStr = getLabelCos("cosmetic_special_symbol_list") == null ? defaultSpecialSymbol : getLabelCos("cosmetic_special_symbol_list");
        specialSymbolStr = StringUtils.strip(specialSymbolStr, ";");
        if (!"".equals(specialSymbolStr)) {
            if (!specialSymbolStr.contains(";")) {
                specialChar.add(specialSymbolStr);
            } else {
                String[] tmp = StringUtils.split(specialSymbolStr, ";");
                specialChar.addAll(Arrays.asList(tmp));
            }
        }

        return super.doBeforeCompose(page, parent, compInfo);
    }

    private void initializeData() {
        Hlayout rowLayout = null;
        for (int i = 0; i < specialChar.size(); i++) {
            if (i % ITEM_PER_ROWS == 0) {
                rowLayout = new Hlayout();
                rowLayout.setVflex("1");
            }
            if (rowLayout != null) {
                rowLayout.appendChild(getButton(specialChar.get(i), specialChar.get(i)));
                if (i % ITEM_PER_ROWS == ITEM_PER_ROWS - 1) {
                    columnLayout.appendChild(rowLayout);
                }
            }

        }
    }

    private Button getButton(String label, final String value) {
        Button btn = new Button(label);
        ButtonEventListener btnListener = new ButtonEventListener(value);
        btn.addEventListener(Events.ON_CLICK, btnListener);
        btn.setHflex("1");
        btn.setVflex("2");
        return btn;
    }

    @Listen("onClose=#specialSymbolWnd")
    public void onClose() {
        Events.sendEvent("onSymbolWndClose", parentWindow, this);
        specialSymbolWnd.detach();
    }

    public class ButtonEventListener implements EventListener<Event> {

        private final String value;

        public ButtonEventListener(String value) {
            this.value = value;
        }

        @Override
        public void onEvent(Event t) throws Exception {
            Events.sendEvent("onAddSymbol", textBox, value);
        }

    }

}
