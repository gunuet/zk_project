/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Model.CosProductTypeSubModel;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.FileNotFoundException;
import org.zkoss.zhtml.Br;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import com.viettel.module.cosmetic.BO.*;
import com.viettel.module.cosmetic.DAO.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

/**
 *
 * @author giangnh20
 */
public class CosmeticViewCVController extends CosmeticBaseController {

    @Wire
    private Listbox fileListbox, lbProductPresentation,
            lbProductType, finalFileListbox, lbManufacturer, lbAssembler, lbCosfileIngre;
    @Wire("#incList #lbCosfileIngreCheck")
    private Listbox lbCosfileIngreCheck;
    private List ingredientListCheck;
    private Long fileId;
    private CosFile cosFile;
    private Files files;
    Long documentTypeCode;
    private List<CosManufacturer> lstManufacturer = new ArrayList();
    private List<CosAssembler> lstAssembler = new ArrayList();
    private List<CosCosfileIngre> lstCosfileIngre = new ArrayList();

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("id");
        cosFile = (new CosmeticDAO()).findByFileId(fileId);
        files = (new FilesDAOHE()).findById(fileId);
        documentTypeCode = Constants.COSMETIC.DOCUMENT_TYPE_CODE_TAOMOI;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        loadItemForEdit();

        //LIST NHA SAN XUAT
        CosManufacturerDAO cmdao = new CosManufacturerDAO();
        lstManufacturer = cmdao.findByCosFileId(cosFile.getCosFileId());
        ListModelArray lstModelManufature = new ListModelArray(lstManufacturer);
        lbManufacturer.setModel(lstModelManufature);

        //LIST NHA DONG GOI
        CosAssemblerDAO cmAssemblerdao = new CosAssemblerDAO();
        lstAssembler = cmAssemblerdao.findByCosFileId(cosFile.getCosFileId());
        ListModelArray lstModelAssembler = new ListModelArray(lstAssembler);
        lbAssembler.setModel(lstModelAssembler);

        //LIST THANH PHAN DAY DU
        CosCosfileIngreDAO cidao = new CosCosfileIngreDAO();
        lstCosfileIngre = cidao.findByCosFileId(cosFile.getCosFileId());
        ListModelArray lstModelIngredient = new ListModelArray(lstCosfileIngre);
        lbCosfileIngre.setModel(lstModelIngredient);
//        ingredientListCheck = cidao.findListIngreAnnexe(cosFile.getCosFileId());
//        lbCosfileIngreCheck.setModel(new ListModelArray(lstCosfileIngre));

        //tep dinh kem
        fillFileListbox(cosFile.getFileId());
        //ho so goc
        fillFinalFileListbox(cosFile.getFileId());

    }

    private void loadItemForEdit() {
        if (cosFile.getDocumentTypeCode() != null) {
            documentTypeCode = cosFile.getDocumentTypeCode();
        }

        CosProductPresentatonDAO cppDAO = new CosProductPresentatonDAO();
        List lstCosProductPresentation = cppDAO.getAllActivePlusDifferent();
        ListModelArray lstModelPresentation = new ListModelArray(lstCosProductPresentation);
        lstModelPresentation.setMultiple(true);
        lbProductPresentation.setModel(lstModelPresentation);

        CosProductTypeSubDAO cptsDAO = new CosProductTypeSubDAO();
        List<CosProductTypeSub> lstCosProductTypeSub = cptsDAO.getAllActiveOrderByProductType();
        List<CosProductTypeSubModel> lstCosProductTypeSubModel = new ArrayList();
        int i = 0;
        CosProductTypeSub previeous = new CosProductTypeSub();
        for (Object currentObject : lstCosProductTypeSub) {
            CosProductTypeSub current = (CosProductTypeSub) currentObject;
            //Neu ban ghi hien tai khong co productTypeId bang ban ghi cu thi add ban ghi moi
            if (!Objects.equals(current.getProductTypeId(), previeous.getProductTypeId())) {
                CosProductTypeSubModel cModel = new CosProductTypeSubModel();
                cModel.setProductTypeId(current.getProductTypeId());
                cModel.setNameVi(current.getNameVi());
                cModel.setNameEn(current.getNameEn());
                cModel.setIsActive(current.getIsActive());
                cModel.setNameLink("");
                lstCosProductTypeSubModel.add(cModel);
                i++;
            } else {
                //Neu giong thi chi add them link
                CosProductTypeSubModel obj = lstCosProductTypeSubModel.get(i - 1);
                List<CosProductTypeSub> lstCosProductTypeSub2;
                lstCosProductTypeSub2 = obj.getLstCosProductTypeSub();
                if (lstCosProductTypeSub2 == null) {
                    lstCosProductTypeSub2 = new ArrayList<>();
                }
                lstCosProductTypeSub2.add(current);
                obj.setLstCosProductTypeSub(lstCosProductTypeSub2);

                obj.setNameLink(obj.getNameLink() + "\r\n" + current.getNameVi() + "(" + current.getNameEn() + ")");
            }
            previeous = current;
        }

        CosProductTypeSubModel cModel = new CosProductTypeSubModel();
        cModel.setProductTypeId(-1L);
        cModel.setNameVi("Các dạng khác (Đề nghị ghi rõ)");
        cModel.setNameEn("Others (please specify)");
        cModel.setIsActive(1L);
        cModel.setIsDifferent(1L);
        cModel.setNameLink("");
        lstCosProductTypeSubModel.add(cModel);
        ListModelArray lstModelType = new ListModelArray(lstCosProductTypeSubModel);
        lstModelType.setMultiple(true);
        lbProductType.setModel(lstModelType);

    }

    @Listen("onAfterRender = #lbProductType")
    public void onAfterRenderProductType() {
        for (int i = 0; i < lbProductType.getItemCount(); i++) {
            Listitem item = lbProductType.getItemAtIndex(i);
            Listcell cell = (Listcell) item.getChildren().get(0);
            CosProductTypeSubModel cos = item.getValue();
            List<CosProductTypeSub> lstCosProductTypeSub = cos.getLstCosProductTypeSub();
            if (lstCosProductTypeSub != null && lstCosProductTypeSub.size() > 0) {
                for (CosProductTypeSub obj : lstCosProductTypeSub) {
                    Label lb = new Label("- " + obj.getNameVi());
                    lb.setClass("product-type-sub");
                    lb.setStyle("font-style: normal !important;");
                    Label enLb = new Label(obj.getNameEn());
                    enLb.setClass("product-type-sub-en");
                    Br br = new Br();
                    cell.appendChild(br);
                    cell.appendChild(lb);
                    cell.appendChild(new Br());
                    cell.appendChild(enLb);
                }
            }
        }
        lbProductType.renderAll();
        loadProductTypeCheck();
        disableListItem(lbProductType);
    }

    @Listen("onAfterRender = #lbProductPresentation")
    public void onAfterRenderProductPresentation() {
        loadProductPresentationCheck();
        disableListItem(lbProductPresentation);
    }

    private void loadProductTypeCheck() {
        String productType = cosFile.getProductType();
        if (productType != null) {
            String[] lst = productType.split(";");
            for (String idCosPresent : lst) {
                Long id = Long.valueOf(idCosPresent);
                List<Listitem> items = lbProductType.getItems();
                for (Listitem item : items) {
                    CosProductTypeSubModel cos = item.getValue();
                    if (Objects.equals(cos.getProductTypeId(), id)) {
                        item.setSelected(true);
                    }

                }
            }
        }

    }

    private void loadProductPresentationCheck() {
        String productPresentation = cosFile.getProductPresentation();
        if (productPresentation != null) {
            String[] lst = productPresentation.split(";");
            for (String idCosPresent : lst) {
                Long id = Long.valueOf(idCosPresent);
                List<Listitem> items = lbProductPresentation.getItems();
                for (Listitem item : items) {
                    CosProductPresentaton cos = item.getValue();
                    if (Objects.equals(cos.getProductPresentationId(), id)) {
                        item.setSelected(true);
                    }

                }
            }
        }

    }

    public void disableListItem(Listbox lb) {
        if (lb != null) {
            for (Listitem item : lb.getItems()) {
                item.setDisabled(true);
            }
        }
    }

    //tep dinh kem
    private void fillFileListbox(Long fileId) {
        CosmeticAttachDAO rDaoHe = new CosmeticAttachDAO();
        List<VFileRtAttach> lstCosmeticAttach = rDaoHe.findCosmeticAttach(fileId);
        this.fileListbox.setModel(new ListModelArray(lstCosmeticAttach));
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    //ho so goc
    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId, Constants.OBJECT_TYPE.COSMETIC_HO_SO_GOC);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    public CosFile getCosFile() {
        return cosFile;
    }

    public Files getFiles() {
        return files;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }
}
