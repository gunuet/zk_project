package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.DAO.FlowDAOHE;
import com.viettel.core.workflow.DAO.NodeDeptUserDAOHE;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.*;
import com.viettel.module.cosmetic.DAO.*;
import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.module.rapidtest.DAO.RapidTestProcessController;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.DAO.DocIn.DocInReturnController;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

/**
 *
 * @author ChucHV
 */
public class CosmeticViewControllerOld extends CosmeticBaseController {

    /**
     *
     */
    private static final long serialVersionUID = 8261140945077364743L;
    @Wire
    private Listbox fileListbox;
    @Wire
    private Div divToolbarBottom;
    private List<Component> listTopActionComp;
    @Wire
    private Div divToolbarTop;
    private List<Component> listBottomActionComp;
    @Wire
    private Window windowView;
    private Window windowParent;
    private VFileCosfile vFileCosfile;
    private Process processCurrent;// process dang duoc xu li
    private Integer menuType;
    @Wire
    private Groupbox gbRapidCRUD1, gbRapidCRUD2, gbRapidCRUD3;
    @Wire
    private Tabbox tb;
    private CosmeticDAO cosmeticDAO = new CosmeticDAO();
    private long FILE_TYPE;;
    private Long DEPT_ID;
    @Wire
    private Listbox lbAssemblerList;
    @Wire
    private Listbox lbManufactureList;
    @Wire
    private Listbox lbIngredientList;
    @Wire
    private Listbox lbPresentationProduct;
    @Wire
    private Listbox lbProductType;
    @Wire("#incListEvaluation #lbListEvaluation")
    private Listbox lbListEvaluation;

    @Wire
    private Paging  userPagingBottom;
    private CosFile cosFile;
    private Long fileId;

    protected CosAdditionalRequest additionalRequest = new CosAdditionalRequest();
    protected BookDocument bookDocument;
    protected CosEvaluationRecord evaluationRecord = new CosEvaluationRecord();
    protected CosPermit permit = new CosPermit();

    @SuppressWarnings("unchecked")
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map<String, Object>) Executions.getCurrent().getArg();

        String filetype = (String) arguments.get("filetype");
        DEPT_ID = (Long) arguments.get("deptid");
        FILE_TYPE = WorkflowAPI.getInstance().getProcedureTypeIdByCode(filetype);

        fileId = (Long) arguments.get("id");
        cosmeticDAO = new CosmeticDAO();
        vFileCosfile = cosmeticDAO.findViewByFileId(fileId);
        processCurrent = WorkflowAPI.getInstance().
                getCurrentProcess(vFileCosfile.getFileId(), vFileCosfile.getFileType(),
                        vFileCosfile.getStatus(), getUserId());
        windowParent = (Window) arguments.get("parentWindow");
        menuType = (Integer) arguments.get("menuType");

        CosmeticDAO objDAOHE = new CosmeticDAO();
        cosFile = objDAOHE.findByFileId(fileId);

        //Xem qua trinh xu ly
        setProcessingView(fileId, FILE_TYPE);

        //end Xem qua trinh
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        listTopActionComp = new ArrayList<>();
        listBottomActionComp = new ArrayList<>();
//        Long documentTypeCode = vFileCosfile.getDocumentTypeCode();
        initializeData(cosFile);
//        if (documentTypeCode != null) {
//            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
//                tb.setVisible(true);
//                gbRapidCRUD1.setVisible(true);
//                gbRapidCRUD2.setVisible(false);
//                gbRapidCRUD3.setVisible(false);
//            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
//                tb.setVisible(true);
//                gbRapidCRUD1.setVisible(false);
//                gbRapidCRUD2.setVisible(true);
//                gbRapidCRUD3.setVisible(false);
//            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
//                tb.setVisible(true);
//                gbRapidCRUD1.setVisible(false);
//                gbRapidCRUD2.setVisible(false);
//                gbRapidCRUD3.setVisible(true);
//            } else {
//                tb.setVisible(false);
//            }
//
//        }
        if (menuType != null && menuType == Constants.DOCUMENT_MENU.VIEW) {
            return;
        }

        addAllNextActions();

        loadActionsToToolbar(menuType, vFileCosfile, processCurrent, windowView);

        setListEvaluation();
    }

    public void setProcessingView(Long fileId, Long fileType) {
        if (fileId != null && fileType != null) {

            //linhdx 13032015 Load danh sach yeu cau sdbs
            BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
            BookDocument bookDocument2 = bookDocumentDAOHE.getBookInFromDocumentId(fileId, fileType);
            if (bookDocument2 != null) {
                bookDocument = bookDocument2;
            }

            //linhdx 13032015 Load danh sach yeu cau sdbs
            CosAdditionalRequestDAO cosAdditionalRequestDAO = new CosAdditionalRequestDAO();
            List<CosAdditionalRequest> lstAddition = cosAdditionalRequestDAO.findAllActiveByFileId(fileId);
            if (lstAddition != null && lstAddition.size() > 0) {
                additionalRequest = lstAddition.get(0);
            }

            CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
            evaluationRecord = dao.getLastEvaluation(fileId);

            CosPermitDAO cosPermitDAO = new CosPermitDAO();
            List<CosPermit> lstPermit = cosPermitDAO.findAllActiveByFileId(fileId);
            if (lstPermit != null && lstPermit.size() > 0) {
                permit = lstPermit.get(0);
            }

        }

    }

    public void setListEvaluation() {
        //lay danh sach cac tham dinh
        if (checkEvaluation() == 1) {
            CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
            List<CosEvaluationRecord> lstCosEvaluationRecord = dao.getAllEvaluation(fileId);
            lbListEvaluation.setModel(new ListModelArray(lstCosEvaluationRecord));
        }

    }

    @Listen("onOpenView = #incListEvaluation #lbListEvaluation")
    public void onOpenView(Event event) {
        CosEvaluationRecord evaluationRecord = (CosEvaluationRecord) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("evaluationRecord", evaluationRecord);
        Window window = createWindow("windowViewEvalution", "/Pages/module/cosmetic/viewEvaluation.zul",
                arguments, Window.POPUP);
        window.doModal();

    }

    private void initializeData(CosFile cosFile) {
        fillFileListbox(cosFile.getFileId());
        fillAssemblerListbox(cosFile.getCosFileId());
        fillManufactureListbox(cosFile.getCosFileId());

        CosCosfileIngreDAO ingredientDAO = new CosCosfileIngreDAO();
        List<CosCosfileIngre> ingredientList = ingredientDAO.findByCosFileId(cosFile.getCosFileId());
        lbIngredientList.setModel(new ListModelArray(ingredientList));

        // Dang hien thi san pham
        List<CosProductPresentaton> presentationList = null;
        if (cosFile.getProductPresentation() != null) {
            CosProductPresentatonDAO presentationDAO = new CosProductPresentatonDAO();
            presentationList = presentationDAO.findByPresentationStringList(cosFile.getProductPresentation());
        }
        if (presentationList == null) {
            presentationList = new ArrayList();
        }
        if (cosFile.getOtherProductPresentation() != null && !"".equals(cosFile.getOtherProductPresentation())) {
            CosProductPresentaton pp = new CosProductPresentaton();
            pp.setNameVi(cosFile.getOtherProductPresentation());
            presentationList.add(pp);
        }
        lbPresentationProduct.setModel(new ListModelArray(presentationList));

        // Dang hien thi san pham
        List<VProductType> productTypeList = new ArrayList();
        if (cosFile.getProductType() != null) {
            CosProductTypeDAO productTypeDAO = new CosProductTypeDAO();
            List<Object[]> results = productTypeDAO.findProductTypeAndSub(cosFile.getProductType());
            for (Object[] item : results) {
                VProductType vProductType = new VProductType();
                CosProductType cosProductType = (CosProductType) item[0];
                CosProductTypeSub cosProductTypeSub = (CosProductTypeSub) item[1];
                vProductType.setProductTypeId(cosProductType.getProductTypeId());
                vProductType.setName(cosProductType.getName());
                vProductType.setIsActive(cosProductType.getIsActive());
                vProductType.setNameEn(cosProductTypeSub.getNameEn());
                vProductType.setNameVi(cosProductTypeSub.getNameVi());

                productTypeList.add(vProductType);
            }
        } else if (cosFile.getOtherProductType() != null && !"".equals(cosFile.getOtherProductType())) {
            VProductType vProductType = new VProductType();
            vProductType.setName(cosFile.getOtherProductType());
        }
        lbProductType.setModel(new ListModelArray(productTypeList));

    }

    private void fillFileListbox(Long fileId) {
        CosmeticAttachDAO rDaoHe = new CosmeticAttachDAO();
        List<VFileRtAttach> lstCosmeticAttach = rDaoHe.findCosmeticAttach(fileId);
        this.fileListbox.setModel(new ListModelArray(lstCosmeticAttach));
    }

    private void fillAssemblerListbox(Long vFileCosfileId) {
        CosAssemblerDAO assemblerDAO = new CosAssemblerDAO();
        List<CosAssembler> listModel = assemblerDAO.findByCosFileId(vFileCosfileId);
        lbAssemblerList.setModel(new ListModelArray(listModel));
    }

    private void fillManufactureListbox(Long vFileCosfileId) {
        CosManufacturerDAO manufactureDAO = new CosManufacturerDAO();
        List<CosAssembler> listModel = manufactureDAO.findByCosFileId(vFileCosfileId);
        lbManufactureList.setModel(new ListModelArray(listModel));
    }

    @Listen("onClose = #windowView")
    public void onClose() {
        windowView.onClose();
        Events.sendEvent("onVisible", windowParent, null);
    }

    @Listen("onVisible = #windowView")
    public void onVisible(Event event) {
        windowView.setVisible(true);
    }

    // Hien thi cac action tren thanh toolbar top va bottom
    public void loadActionsToToolbar(int menuType,
            final VFileCosfile vFileCosfile,
            final Process currentProcess, final Window currentWindow) {

        for (Component comp : listTopActionComp) {
            divToolbarTop.appendChild(comp);
        }
        for (Component comp : listBottomActionComp) {
            divToolbarBottom.appendChild(comp);
        }

    }

    private void addButtonToToolbar(Button button) {
        button.setSclass("btnAction");
        divToolbarTop.appendChild(button);
        listTopActionComp.add(button);
        Button btnClone = (Button) button.clone();
        divToolbarBottom.appendChild(btnClone);
        listBottomActionComp.add(btnClone);
    }

    // Load danh sach cac file dinh kem cua van ban den
    // va hien thi tren giao dien
    @SuppressWarnings({"unchecked", "rawtypes"})
    private void loadFileAttach() {
//		AttachDAOHE attachsDAOHE = new AttachDAOHE();
//		List<Attachs> listFile = attachsDAOHE.getByObjectId(vFileCosfile.getRapidTestId());
//		if (listFile != null && !listFile.isEmpty()) {
//			ListModelList modelList = new ListModelList(listFile);
//			lbAttach.setModel(modelList);
//			lbAttach.setVisible(true);
//			divAttach.setVisible(true);
//		} else {
//			lbAttach.setVisible(false);
//			divAttach.setVisible(false);
//		}
    }

    @Listen("onViewAttachFile = #attachListBox")
    public void onViewAttachFile(Event event) {
        // try {
        Attachs attach = (Attachs) event.getData();
        java.io.File file = new java.io.File(attach.getAttachPath()
                + attach.getAttachId());
        if (file.exists()) {
            Filedownload.save(attach.getAttachPath() + attach.getAttachId(),
                    null, attach.getAttachName());
        } else {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.WARNING);
        }

    }

    @Listen("onAfterSendProcess = #windowView")
    public void onAfterSendProcess(Event event) {
    }

    @Listen("onAfterRetrieving = #windowView")
    public void onAfterRetrieving(Event event) {
        onAfterSendProcess(event);
    }

    @Listen("onAfterReturning = #windowView")
    public void onAfterReturning(Event event) {
        onAfterSendProcess(event);
    }

    @SuppressWarnings("unchecked")
    @Listen("onPuttingInBook = #windowView")
    public void onAfterPuttingInBook(Event event) throws Exception {
        Map<String, Object> arguments = (Map<String, Object>) event.getData();
        vFileCosfile.setCosFileId((Long) arguments.get("id"));
        windowView.setVisible(true);
        onAfterSendProcess(event);
        reloadView();
    }

    // Sau khi tạo hồ sơ
    @Listen("onAfterCreatingFile = #windowView")
    public void onAfterCreatingFile() {
        windowView.setVisible(true);
    }

    private void reloadView() throws Exception {
        // load lai tat ca thong tin cua van ban hien thi tren form View
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    private void createReturnButton(List<String> listActionName,
            final NodeToNode ntn) {
        NodeDeptUserDAOHE nduDAOHE = new NodeDeptUserDAOHE();
        List<NodeDeptUser> listNDU = nduDAOHE.getDetailedNodeDeptUser(
                ntn.getNextId(), getDeptId());

        final List<Long> listProcessIdToReturn = new ArrayList<>();
        Process process = processCurrent;
        boolean hasBtnReturn = false;

        ProcessDAOHE processDAOHE = new ProcessDAOHE();
        while (process != null) {
            listProcessIdToReturn.add(process.getProcessId());

            for (NodeDeptUser ndu : listNDU) {
                if (Objects.equals(ndu.getDeptId(), process.getSendGroupId())
                        && Objects.equals(ndu.getUserId(),
                                process.getSendUserId())) {
                    // load action tra lai
                    Button btnTop = new Button("Trả lại");
                    final NodeDeptUser targetToReturn = ndu;
                    final Long rootProcessId = process.getParentId();

                    EventListener<Event> event = new EventListener<Event>() {

                        @Override
                        public void onEvent(Event t) throws Exception {
                            Map<String, Object> data = new ConcurrentHashMap<>();
                            data.put("id",
                                    vFileCosfile.getCosFileId());
                            data.put("listProcessIdToReturn",
                                    listProcessIdToReturn);
                            data.put("rootProcessId", rootProcessId);
                            data.put("targetToReturn", targetToReturn);
                            data.put("parentWindow", windowView);
                            data.put("composer", new DocInReturnController());
                            createWindow(
                                    "windowComment",
                                    "/Pages/document/docIn/subForm/sendProcess.zul",
                                    data, Window.MODAL);
                        }
                    };

                    btnTop.addEventListener(Events.ON_CLICK, event);
                    addButtonToToolbar(btnTop);
                    if (listActionName != null) {
                        listActionName.add(ntn.getAction());
                    }
                    hasBtnReturn = true;
                    break;
                }
            }

            if (process.getParentId() != null) {
                process = processDAOHE.findById(process.getParentId());
            } else {
                process = null;
            }

            if (hasBtnReturn || process == null) {
                break;
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private List<NodeToNode> getListAction(Process process) {
        FlowDAOHE flowDAOHE = new FlowDAOHE();
        List<NodeToNode> listActions = null;
        if (process.getNodeId() == null) {
            // Lay luong don vi
            List listDeptFlow = flowDAOHE.getDeptFlow(getDeptId(),
                    Constants.CATEGORY_TYPE.DOCUMENT_RECEIVE, null);
            if (listDeptFlow != null) {
                Long firstNodeId = (Long) listDeptFlow.get(1);
                process.setNodeId(firstNodeId);
                listActions = flowDAOHE.getNextAction(firstNodeId,
                        Constants.NODE_ASSOCIATE_TYPE.NORMAL);
            }
        } else {
            listActions = flowDAOHE.getNextAction(process.getNodeId(),
                    Constants.NODE_ASSOCIATE_TYPE.NORMAL);
        }
        return listActions;
    }

    /*
     * Set đã đọc cho văn bản
     */
    private void setReadDocument() {
        switch (menuType) {
            case Constants.DOCUMENT_MENU.ALL:
            case Constants.DOCUMENT_MENU.WAITING_PROCESS:
            case Constants.DOCUMENT_MENU.RECEIVE_TO_KNOW:
                if (Constants.PROCESS_STATUS.NEW.equals(processCurrent.getStatus())) {
                    processCurrent.setStatus(Constants.PROCESS_STATUS.READ);
                    ProcessDAOHE processDAOHE = new ProcessDAOHE();
                    processDAOHE.saveOrUpdate(processCurrent);
                }
                break;
        }
    }

    public VFileCosfile getvFileCosfile() {
        return vFileCosfile;
    }

    public void setvFileCosfile(VFileCosfile vFileCosfile) {
        this.vFileCosfile = vFileCosfile;
    }

    @Listen("onViewFlow = #windowView")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        Long docId = vFileCosfile.getFileId();
        Long docType = vFileCosfile.getFileType();
        args.put("objectId", docId);
        args.put("objectType", docType);
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    // Mở popup xử lý nghiệp vụ
    // Trường hợp khởi tạo luồng
    @Listen("onActiveFlow = #windowView")
    public void onActiveFlow() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("docId", vFileCosfile.getCosFileId());
        arguments.put("docType", vFileCosfile.getDocumentTypeCode());
        Long status = vFileCosfile.getStatus();
        if (status == null) {
            status = 0l;
        }
        arguments.put("docStatus", status);

        // form nghiep vu tuong ung voi action
        String formName;
        if (status == 3 || status == 0) {
            formName = "/Pages/module/rapidtest/index.zul";
        } else if (status == 4) {
            formName = "/Pages/module/rapidtest/approveDoc.zul";
        } else {
            formName = "/Pages/module/rapidtest/processDoc.zul";
        }
        createWindow("windowActiveFlow", formName, arguments, Window.MODAL);
    }

    private Map<String, Object> setArgument() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("CRUDMode", "CREATE");
        arguments.put("fileId", vFileCosfile.getFileId());
        arguments.put("parentWindow", windowView);
        return arguments;
    }

    /**
     * Đinh kem ho so goc
     */
    @Listen("onCreateAttachFileFinal = #windowView")
    public void onCreateAttachFileFinal() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdAttachFinalFileCRUD",
                "/Pages/rapidTest/include/attachFinalFileCRUD.zul", arguments,
                Window.POPUP);
    }

    /**
     * Download ho so goc
     *
     * @throws java.io.FileNotFoundException
     */
    @Listen("onDownloadAttachFileFinal = #windowView")
    public void onDownloadAttachFileFinal() throws FileNotFoundException {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        List<Attachs> listAttach = attachDAOHE.getByObjectIdAndType(vFileCosfile.getCosFileId(), Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        if (listAttach != null && listAttach.size() > 0) {
            Attachs att = listAttach.get(0);
            String path = att.getAttachPath() + File.separator + att.getAttachId();
            File f = new File(path);
            if (f.exists()) {
                File tempFile = FileUtil.createTempFile(f, att.getAttachName());
                Filedownload.save(tempFile, null);
            } else {
                showNotification("File không còn tồn tại trên hệ thống");
            }
        }
    }

    /**
     * Thanh toan ho so Hien thi danh sach cac loai phi can thanh toan
     */
    @Listen("onPayment = #windowView")
    public void onPayment() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdPayment",
                "/Pages/rapidTest/include/paymentAll.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    /**
     * Ke toan xac nhan thanh toan
     */
    @Listen("onPaymentConfirm = #windowView")
    public void onPaymentConfirm() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdPaymentConfirm",
                "/Pages/rapidTest/include/paymentConfirm.zul", arguments,
                Window.POPUP);
    }

    /**
     * Van thu tiep nhan(vao so)
     */
    @Listen("onPutInBookOk = #windowView")
    public void onPutInBookOk() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdPutInBook",
                "/Pages/rapidTest/include/putInBookOk.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onPutInBookNok = #windowView")
    public void onPutInBookNok() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdPutInBook",
                "/Pages/rapidTest/include/putInBookNok.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onEvaluation = #windowView")
    public void onEvaluation() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdEvaluation",
                "/Pages/rapidTest/include/evaluationCV.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onEvaluationCv = #windowView")
    public void onEvaluationCv() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdEvaluation",
                "/Pages/rapidTest/include/evaluationCv.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onEvaluationTP = #windowView")
    public void onEvaluationTP() {
        Map<String, Object> arguments = setArgument();
        arguments.put("userEvaluationType", Constants.EVALUTION.USER_EVALUATION_TYPE.TP);
        Window window = createWindow("wdEvaluationTP",
                "/Pages/rapidTest/include/evaluationTP.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onApproveEvaluation = #windowView")
    public void onApproveEvaluation() {
        Map<String, Object> arguments = setArgument();
        arguments.put("userEvaluationType", Constants.EVALUTION.USER_EVALUATION_TYPE.CT);
        Window window = createWindow("wdEvaluationCT",
                "/Pages/rapidTest/include/evaluationCT.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onProvideNumberDispatch = #windowView")
    public void onProvideNumberDispatch() {
        Map<String, Object> arguments = setArgument();
        arguments.put("userEvaluationType", Constants.EVALUTION.USER_EVALUATION_TYPE.VT);
        Window window = createWindow("wdEvaluationVT",
                "/Pages/rapidTest/include/evaluationVT.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onProvideNumberPermit = #windowView")
    public void onProvideNumberPermit() {
        Map<String, Object> arguments = setArgument();
        arguments.put("userEvaluationType", Constants.EVALUTION.USER_EVALUATION_TYPE.VT);
        Window window = createWindow("wdEvaluationVT",
                "/Pages/rapidTest/include/evaluationVT.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    /**
     * Cho y kien xu ly
     */
    @Listen("onSendComent = #windowView")
    public void onSendComent() {
        Map<String, Object> arguments = setArgument();

        arguments.put("documentReceiveId", vFileCosfile.getCosFileId());
        //arguments.put("process", currentProcess);
        arguments.put("actionName", "Xin ý kiến");
        arguments.put("actionType",
                Constants.NODE_ASSOCIATE_TYPE.NORMAL);
        arguments.put("parentWindow", windowView);
        arguments.put("composer",
                new RapidTestProcessController());
        arguments.put("processType",
                Constants.PROCESS_TYPE.COMMENT);
        Window window = (Window) Executions.createComponents(
                "/Pages/document/docIn/subForm/sendProcess.zul",
                null, arguments);
        window.doModal();
//            createWindow("wdSendComment",
//                    "/Pages/vFileCosfile/include/sendComment.zul", arguments,
//                    Window.POPUP);
    }

    /**
     * Gui xu ly
     */
    @Listen("onSendProcess = #windowView")
    public void onSendProcess() {

        Map<String, Object> data = new ConcurrentHashMap<>();
        data.put("documentReceiveId", vFileCosfile.getCosFileId());
        //data.put("process", currentProcess);
        data.put("actionName", "Gửi xử lý");
        data.put("actionType", 1L); //linhdx action type = 1 la gui xu ly
        data.put("parentWindow", windowView);
        data.put("composer", new RapidTestProcessController());
        Window window = createWindow(
                "windowComment",
                "/Pages/rapidTest/include/sendProcess.zul",
                data, Window.MODAL);
        window.doModal();

    }

    /**
     * Đong y phe duyet
     */
    @Listen("onApprove = #windowView")
    public void onApprove() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdApprove",
                "/Pages/rapidTest/include/approve.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    /**
     * Ky duyet Voi lanh dao phong la ky nhay Voi lanh dao cuc la ky duyet
     */
    @Listen("onSignCA = #windowView")
    public void onSignCA() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdSignCA",
                "/Pages/rapidTest/include/approve.zul", arguments,
                Window.POPUP);
    }

    /**
     * Tu choi
     */
    @Listen("onReject = #windowView")
    public void onReject() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdReject",
                "/Pages/rapidTest/include/approve.zul", arguments,
                Window.POPUP);
    }

    /**
     * Gui doanh nghiep yeu cau bo sung ho so
     */
    @Listen("onSendCommentForAddition = #windowView")
    public void onSendCommentForAddition() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdSendCommentForAddition",
                "/Pages/rapidTest/include/sendCommentForAddition.zul", arguments,
                Window.POPUP);
    }

    /**
     * Doanh nghiep gui yeu cau kiem tra
     */
    @Listen("onSendRequestToCheck = #windowView")
    public void onSendRequestToCheck() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdSendRequestToCheck",
                "/Pages/rapidTest/include/sendRequestToCheck.zul", arguments,
                Window.POPUP);
    }

    @Listen("onExportFile = #windowView")
    public void onExportFile() {
        FilesModel model = new FilesModel(cosFile.getFileId());
        ExportFileDAO exp = new ExportFileDAO();
        exp.exportCosmeticAnnouncement(model, true);
    }
//    @Listen("onExportSDBS = #windowView")
//    public void onExportSDBS() {
//        Map<String, Object> arguments = setArgument();
//        int typeExport = Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP;
//        ExpocosFileDAO export = new ExpocosFileDAO();
//        export.exportDataCvSdbs(typeExport);
//        
//        
//    }

    private void addAllNextActions() {
        Long docStatus = vFileCosfile.getStatus();
        Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;
        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(
                docType, docStatus, deptId);

        for (NodeToNode action : actions) {
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
    }

    // viethd3: create new button for a processing action
    private Button createButtonForAction(NodeToNode action) {
        // check if action is RETURN_PREVIOUS but parent process came from another node
        if (processCurrent != null
                && action.getType() == Constants.NODE_ASSOCIATE_TYPE.RETURN_PREVIOUS
                && action.getNextId() != processCurrent.getPreviousNodeId()) {
            return null;
        }

        // check if receiverId is diferent to current user id
        // find all process having current status of document
        List<Process> listCurrentProcess = WorkflowAPI.getInstance().findAllCurrentProcess(vFileCosfile.getFileId(), vFileCosfile.getFileType(),
                vFileCosfile.getStatus());
        Long userId = getUserId();
        Long creatorId = vFileCosfile.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return null;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                for (Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                if (!needToProcess) {
                    return null;
                }
            }
        }

        Button btn = new Button(action.getAction());

        final String actionName = action.getAction();
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action
        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = vFileCosfile.getStatus();
        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId,false));

        EventListener<Event> event = new EventListener<Event>() {

            @Override
            public void onEvent(Event t) throws Exception {
                Map<String, Object> data = new ConcurrentHashMap<>();
                data.put("fileId", vFileCosfile.getFileId());
                data.put("docId", vFileCosfile.getFileId());
                data.put("docType", vFileCosfile.getFileType());
                data.put("docStatus", status);
                data.put("actionId", actionId);
                data.put("actionName", actionName);
                data.put("actionType", actionType);
                data.put("nextId", nextId);
                data.put("previousId", prevId);
                if (processCurrent != null) {
                    data.put("process", processCurrent);
                }
                data.put("lstAvailableNDU", lstNDUs);
                data.put("windowParent", windowView);
                createWindow("windowComment", formName, data, Window.MODAL);
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    public int checkDispathSDBS() {
        return checkDispathSDBS(vFileCosfile.getFileId());
    }

    public int checkBooked() {
        return checkBooked(vFileCosfile.getFileId(), vFileCosfile.getFileType());
    }

    public int checkViewProcess() {
        return checkViewProcess(vFileCosfile.getFileId());
    }

    public int checkEvaluation() {
        return checkEvaluation(vFileCosfile.getFileId());
    }

    public int checkPermit() {
        return checkPermit(vFileCosfile.getFileId());
    }

    public void onDownloadPermit() throws FileNotFoundException {
        CosPermitDAO cosPermitDAO = new CosPermitDAO();
        CosPermit permit = cosPermitDAO.findLastByFileId(fileId);

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> lstAttach = attDAOHE.getByObjectIdAndType(permit.getPermitId(), Constants.OBJECT_TYPE.COSMETIC_PERMIT);
        Attachs att = null;
        if (lstAttach != null && lstAttach.size() > 0) {
            att = lstAttach.get(0);

        }
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    public CosFile getCosFile() {
        return cosFile;
    }

    public void setCosFile(CosFile cosFile) {
        this.cosFile = cosFile;
    }

    public Listbox getLbListEvaluation() {
        return lbListEvaluation;
    }

    public void setLbListEvaluation(Listbox lbListEvaluation) {
        this.lbListEvaluation = lbListEvaluation;
    }

    public CosAdditionalRequest getAdditionalRequest() {
        return additionalRequest;
    }

    public void setAdditionalRequest(CosAdditionalRequest additionalRequest) {
        this.additionalRequest = additionalRequest;
    }

    public BookDocument getBookDocument() {
        return bookDocument;
    }

    public void setBookDocument(BookDocument bookDocument) {
        this.bookDocument = bookDocument;
    }

    public CosEvaluationRecord getEvaluationRecord() {
        return evaluationRecord;
    }

    public void setEvaluationRecord(CosEvaluationRecord evaluationRecord) {
        this.evaluationRecord = evaluationRecord;
    }

    public CosPermit getPermit() {
        return permit;
    }

    public void setPermit(CosPermit permit) {
        this.permit = permit;
    }

}
