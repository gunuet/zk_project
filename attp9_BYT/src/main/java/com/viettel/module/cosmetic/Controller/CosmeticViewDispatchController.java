package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.*;
import com.viettel.module.cosmetic.DAO.*;
import com.viettel.module.cosmetic.DAO.CosAdditionalRequestDAO;
import com.viettel.utils.Constants;
import java.util.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

/**
 *
 * @author ChucHV
 */
public class CosmeticViewDispatchController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 8261140945077364743L;
    @Wire
    private Window windowView;
    private Window windowParent;
    private CosAdditionalRequest additionalRequest;
    @Wire
    private Div divDispath,main;
    @SuppressWarnings("unchecked")
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map<String, Object>) Executions.getCurrent().getArg();

        Long fileId = (Long) arguments.get("id");
        windowParent = (Window) arguments.get("parentWindow");
        CosAdditionalRequestDAO cosAdditionalRequestDAO = new CosAdditionalRequestDAO();
        List<CosAdditionalRequest> lstAddition = cosAdditionalRequestDAO.findAllActiveByFileId(fileId);
        if (lstAddition != null && lstAddition.size() > 0) {
            additionalRequest = lstAddition.get(0);
        }

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
         Map<String, Object> arguments = (Map<String, Object>) Executions.getCurrent().getArg();
         divDispath = (Div) Executions.createComponents(
                    "/Pages/module/cosmetic/viewDispatchInclude.zul", main, arguments);

    }

    public CosAdditionalRequest getAdditionalRequest() {
        return additionalRequest;
    }

    public void setAdditionalRequest(CosAdditionalRequest additionalRequest) {
        this.additionalRequest = additionalRequest;
    }
}
