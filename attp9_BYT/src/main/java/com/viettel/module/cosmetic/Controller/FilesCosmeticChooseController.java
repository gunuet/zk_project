/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.utils.Constants;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.WorkflowAPI;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class FilesCosmeticChooseController extends BaseComposer {

    @Wire
    Window filesCosmeticChooseWnd;
    private Window parentWindow;
    Long deptId, procedureId;

    // label for validating data
    @Wire
    private Label lbTopWarning, lbBottomWarning;

    public FilesCosmeticChooseController() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        loadInfoToForm();

    }

    public void loadInfoToForm() {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        deptId = (Long) arguments.get("deptId");
        procedureId = (Long) arguments.get("procedureId");
    }

    public void createNew() throws IOException {
        Map args = new ConcurrentHashMap();
        args.put("documentTypeCode", Constants.COSMETIC.DOCUMENT_TYPE_CODE_TAOMOI);
        setFowardPage(args);
    }
    
    public void changeRequest() throws IOException {
        Map args = new ConcurrentHashMap();
        args.put("documentTypeCode", Constants.COSMETIC.DOCUMENT_TYPE_CODE_BOSUNG);
        setFowardPage(args);
    }
    
    public void extendRequest() throws IOException {
        Map args = new ConcurrentHashMap();
        args.put("documentTypeCode", Constants.COSMETIC.DOCUMENT_TYPE_CODE_GIAHAN);
        setFowardPage(args);
    }
    public void setFowardPage(Map args){
        WorkflowAPI w = new WorkflowAPI();
        procedureId = w.getProcedureTypeIdByCode(Constants.CATEGORY_TYPE.COSMETIC_OBJECT);
        deptId = 3006L;//Cục ATP
        args.put("procedureId", procedureId);
        
        args.put("deptId", deptId);
        args.put("CRUDMode", "CREATE");
        args.put("parentWindow", filesCosmeticChooseWnd);

        createWindow("windowCRUDCosmetic", "/Pages/module/cosmetic/cosmeticCRUD.zul", args, Window.EMBEDDED);
        filesCosmeticChooseWnd.setVisible(false);
    }
    

    @Listen("onVisible = #filesCosmeticChooseWnd")
    public void onVisible() {
        filesCosmeticChooseWnd.setVisible(true);
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

}
