/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.utils.Constants;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.DAO.FilesDAOHE;

/**
 * 
 * @author DUC
 */
public class HomepageDAO extends BaseComposer {

	@Wire
	private Label lblSumFiles;
	@Wire
	private Label lblWaitProcess;
	@Wire
	private Label lblProcess;
	@Wire
	private Label lblFinish;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
                 this.getPage().setTitle(ResourceBundleUtil.getString("title"));
                 
//                 FileUtil f = new FileUtil();
//                 String name = f.getSafeFileNameAll("1 2 3.pdf");
		// Display count homepage
                 
                 //LogUtils.addLogDB("test 555");
		/*
		 * CosmeticDAO objDAOHE = new CosmeticDAO();
		 * lblSumFiles.setValue(String.
		 * valueOf(objDAOHE.countHomePage(Constants.COUNT_HOME_TYPE.TOTAL)));
		 * lblWaitProcess
		 * .setValue(String.valueOf(objDAOHE.countHomePage(Constants
		 * .COUNT_HOME_TYPE.WAIT_PROCESS)));
		 * lblProcess.setValue(String.valueOf(objDAOHE
		 * .countHomePage(Constants.COUNT_HOME_TYPE.PROCESS)));
		 * lblFinish.setValue
		 * (String.valueOf(objDAOHE.countHomePage(Constants.COUNT_HOME_TYPE
		 * .FINISH)));
		 */
		FilesDAOHE objDAOHE = new FilesDAOHE();
		Long sumFiles = objDAOHE.countHomePage(Constants.COUNT_HOME_TYPE.TOTAL);
		Long process = objDAOHE
				.countHomePage(Constants.COUNT_HOME_TYPE.PROCESS);
		Long finish = objDAOHE.countHomePage(Constants.COUNT_HOME_TYPE.FINISH);

		Long waitProcess = sumFiles - process - finish;

		lblSumFiles.setValue(String.valueOf(sumFiles));
		lblWaitProcess.setValue(String.valueOf(process));
		lblProcess.setValue(String.valueOf(waitProcess));
		lblFinish.setValue(String.valueOf(finish));

	}

}
