/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.TmpBill;
import com.viettel.module.cosmetic.DAO.TmpBillDAO;
import com.viettel.module.cosmetic.DAO.VFileCosfileDAO;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Window;

/**
 *
 * @author binhnt53
 */
public class ImpTmpBillToBillController extends BaseComposer {

    //Control tim kiem
    //Danh sach
    @Wire
    Datebox dbFromDate, dbToDate;
    @Wire
    Listbox lbTmpBill, lbStatus, lbIsLast;
    Window showDeptDlg;
    @Wire
    Paging userPagingBottom;
    TmpBill searchForm;
    BillDAO billDao = new BillDAO();

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        TmpBillDAO cdhe = new TmpBillDAO();
        List lstProcedure = cdhe.findAllTmpBill();
        ListModelArray lstModelProcedure = new ListModelArray(lstProcedure);
        lbTmpBill.setModel(lstModelProcedure);
        searchForm = new TmpBill();
        onSearch();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0);
        fillDataToList();
    }

    private void fillDataToList() {
        searchForm.setStatus(Long.parseLong((String) lbStatus.getSelectedItem().getValue()));
        searchForm.setIsLastImp(Long.parseLong((String) lbIsLast.getSelectedItem().getValue()));
        TmpBillDAO objhe = new TmpBillDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(searchForm, dbFromDate.getValue(), dbToDate.getValue(), start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbTmpBill.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }

    @Listen("onClick=#btnImport")
    public void onImport() throws IOException {
        String message = String.format(Constants.Notification.IMP_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.TMP_BILL);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    if (doImport()) {
                                        showNotification(String.format(Constants.Notification.IMP_SUCCESS, Constants.DOCUMENT_TYPE_NAME.TMP_BILL), Constants.Notification.INFO);
                                    } else {
                                        showNotification(String.format(Constants.Notification.IMP_ERROR, Constants.DOCUMENT_TYPE_NAME.TMP_BILL), Constants.Notification.ERROR);
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    @Listen("onClick=#btnImportDb")
    public void onImportDb() throws IOException {
        String message = String.format(Constants.Notification.IMP_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.TMP_BILL);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    if (doImportDb()) {
                                        showNotification(String.format(Constants.Notification.IMP_SUCCESS, Constants.DOCUMENT_TYPE_NAME.TMP_BILL), Constants.Notification.INFO);
                                    } else {
                                        showNotification(String.format(Constants.Notification.IMP_ERROR, Constants.DOCUMENT_TYPE_NAME.TMP_BILL), Constants.Notification.ERROR);
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    public boolean doImportDb() {//bat dau import du lieu vao bang bill
        boolean check = true;
        TmpBillDAO tmpBillDao = new TmpBillDAO();
        List<TmpBill> lstTmpBillGroup = new ArrayList<>();//danh sach group theo bien lai
        List<TmpBill> lstTmpBill = tmpBillDao.findTmpBill(2L, 1L);//danh sach vua import
        List<String> lstBillNo = new ArrayList<>();//danh sanh ma bien lai
        if (!lstTmpBill.isEmpty()) { //danh sach import k rong
            for (TmpBill lstTmpBill1 : lstTmpBill) {//lay danh sach list ma hoa don co the import                
                String billNo = "";
                try {
                    billNo = lstTmpBill1.getBillNo();
                    if (lstBillNo.isEmpty()) {
                        lstBillNo.add(billNo);
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    continue;
                }
                if (!lstBillNo.contains(billNo)) {
                    lstBillNo.add(billNo);
                }
            }
            if (!lstBillNo.isEmpty()) {
                for (String lstBillNo1 : lstBillNo) {
                    //insert bill
                    Bill billBo = new Bill();
                    billDao.saveOrUpdate(billBo);
                    for (TmpBill lstTmpBill1 : lstTmpBill) {//insert payment info
                        if (lstTmpBill1.getBillNo().equals(lstBillNo1)) {
                            lstTmpBillGroup.add(lstTmpBill1);
                            if (billDao.checkExistBill(lstTmpBill1)) {//insert to bill

                            } else {
                                check = false;
                            }
                        }
                    }
                }
            }
        }
        return check;
    }

    public boolean doImport() {
        TmpBillDAO tmpBillDao = new TmpBillDAO();
        List<TmpBill> lstTmpBill = tmpBillDao.findTmpBillLastImport();//danh sach vua import
        List<TmpBill> lstTmpBillGroup = new ArrayList<>();//danh sach group theo bien lai
        List<String> lstBillNo = new ArrayList<>();//danh sanh ma bien lai
        if (!lstTmpBill.isEmpty()) { //neu danh sach import co
            for (TmpBill lstTmpBill1 : lstTmpBill) {
                String billNo = "";
                try {
                    billNo = lstTmpBill1.getBillNo();
                    if (lstBillNo.isEmpty()) {
                        lstBillNo.add(billNo);
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    continue;
                }
                if (billNo != null && !lstBillNo.contains(billNo)) {
                    lstBillNo.add(billNo);
                }
            }
            if (!lstBillNo.isEmpty()) {
                for (String lstBillNo1 : lstBillNo) {
                    for (TmpBill lstTmpBill1 : lstTmpBill) {
                        String currentNo = "";
                        try {
                            currentNo = lstTmpBill1.getBillNo();
                        } catch (Exception ex) {
                            LogUtils.addLogDB(ex);
                            continue;
                        }
                        if (currentNo.equals(lstBillNo1)) {
                            lstTmpBillGroup.add(lstTmpBill1);
                        }
                    }
                    if (!lstTmpBillGroup.isEmpty()) {
                        String nameBus = lstTmpBillGroup.get(0).getBusinessName();
                        String price = lstTmpBillGroup.get(0).getPrice();
                        for (int j = 1; j < lstTmpBillGroup.size(); j++) {
                            if (!lstTmpBillGroup.get(j).getBusinessName().equals(nameBus)) {
                                lstTmpBillGroup.clear();
                                break;
                            } else {
                                if (lstTmpBillGroup.get(j).getPrice() != null && lstTmpBillGroup.get(j).getPrice().length() > 1) {
                                    price = lstTmpBillGroup.get(j).getPrice();
                                }
                            }
                        }
                        if (!lstTmpBillGroup.isEmpty()) {
                            Long lprice = 0L;
                            try {
                                lprice = Long.parseLong(price);
                                if ((lstTmpBillGroup.size() * 500000L) == lprice) {
                                    for (TmpBill lstTmpBillGroup1 : lstTmpBillGroup) {
                                        VFileCosfileDAO vfcfdao = new VFileCosfileDAO();
                                        if (vfcfdao.findAllByNswFileCode(lstTmpBillGroup1.getProductCode()) != null) {
                                            lstTmpBillGroup1.setStatus(2L);
                                            tmpBillDao.saveOrUpdate(lstTmpBillGroup1);
                                        } else {
                                            lstTmpBillGroup1.setStatus(-2L);
                                            tmpBillDao.saveOrUpdate(lstTmpBillGroup1);
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                LogUtils.addLogDB(ex);
                            }
                        }
                        lstTmpBillGroup.clear();
                    }
                }
            }
        }
        lbStatus.setSelectedIndex(3);
        lbIsLast.setSelectedIndex(1);
        onSearch();
        return !lstTmpBill.isEmpty();
    }
}
