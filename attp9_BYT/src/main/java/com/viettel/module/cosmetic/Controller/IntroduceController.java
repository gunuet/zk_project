/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.rapidtest.DAO.RegisterController;
import com.viettel.utils.ResourceBundleUtil;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;

/**
 *
 * @author binhnt53
 */
public class IntroduceController extends BaseComposer {

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        this.getPage().setTitle(ResourceBundleUtil.getString("title"));
    }

}
