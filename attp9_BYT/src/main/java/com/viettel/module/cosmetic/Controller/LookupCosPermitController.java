/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.zkforge.bwcaptcha.Captcha;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.zkoss.zul.Filedownload;

/**
 *
 * @author binhnt53
 */
public class LookupCosPermitController extends BaseComposer {

    @Wire
    Textbox txtBusinessName, txtFileCode, txtTaxCode, txtReceiveNo, txtCaptchaId;
    @Wire
    Listbox lbFiles, lbFileType, lbPermitType;
    @Wire
    Paging userPagingBottom;
    Files searchForm;
    @Wire
    Captcha capcha;
    @Wire
    Vbox vboxCaptchaId;
    @Wire
    private Label lbTopWarning;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        this.getPage().setTitle(ResourceBundleUtil.getString("title"));
        Session ss = Sessions.getCurrent();
        if (ss.getAttribute("countCaptcha1") == null) {
            ss.setAttribute("countCaptcha1", 0);
        }
        capcha.setBgColor(16777215);
        capcha.setCaptchars(Constants.getCaptchars());
        if (Integer.valueOf(ss.getAttribute("countCaptcha1").toString()) >= 2) {
            vboxCaptchaId.setVisible(true);
            ss.setAttribute("isVisibleCaptcha", true);
        }
        searchForm = new Files();
        fillDataToList();
        onSearch();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        userPagingBottom.setActivePage(0);
        search(0);
    }

    private void search(int actp) {
        int take = userPagingBottom.getPageSize();
        int start = actp * userPagingBottom.getPageSize();
        PagingListModel plm;
        searchForm.setFileCode(txtFileCode.getValue());
        searchForm.setTaxCode(txtTaxCode.getValue().trim());
        searchForm.setBusinessName(txtBusinessName.getValue().trim());
        searchForm.setStatus(lbPermitType.getSelectedItem() == null ? null : Long.parseLong(lbPermitType.getSelectedItem().getValue().toString()));
        searchForm.setFileType(lbFileType.getSelectedItem() == null ? null : Long.parseLong(lbFileType.getSelectedItem().getValue().toString()));

        FilesDAOHE fDAO = new FilesDAOHE();
        plm = fDAO.searchFilesPermit(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbFiles.setModel(lstModel);
        lbFiles.setMultiple(true);

    }

    private void fillDataToList() {
        List<Category> lsC = new ArrayList<>();
        lsC.add(0, new Category("-1", "---Chọn---"));
        lsC.add(1, new Category("1950", "Cấp GXN Đạt yêu cầu NK"));
        lsC.add(2, new Category("2050", "Cấp giấy lưu hành bộ XNN"));
        lsC.add(3, new Category("4950", "Cấp phép nhập khẩu trang thiết bị y tế"));
        lbFileType.setModel(new ListModelList<>(lsC));
        lbFileType.renderAll();
    }

    public String getPermit(Long status) {
        String rs;
        switch (status.intValue()) {
            case 21000088:
                rs = "Giấy xác nhận các mặt hàng kiểm tra giảm";
                break;
            case 21000081:
                rs = "Giấy xác nhận các mặt hàng kiểm tra thường/chặt";
                break;
            case 41000067:
                rs = "Công văn không phải bộ xét nghiệm nhanh";
                break;
            case 41000055:
                rs = "Giấy chứng nhận lưu hành bộ xét nghiệm nhanh";
                break;
            case 31000033:
                rs = "Công văn từ chối cấp phép";
                break;
            case 31000063:
                rs = "Công văn cấp phép";
                break;
            default:
                rs = " ";
        }
        return rs;
    }

    public String getPermitNo(Files f) {
        String rs = "";
        PermitDAO pmDAO = new PermitDAO();
        Permit pm = pmDAO.findLastByFileIdAndType(f.getFileId(), null);
        if (pm != null) {
            rs = pm.getReceiveNo();
        }
        return rs;
    }

    public void onSelectFileType() {
        List<Category> lsC = new ArrayList<>();

        lbPermitType.getChildren().clear();
        int k = lbFileType.getSelectedIndex();
        lsC.add(0, new Category("-1", "---Chọn---"));
        if (k == 1) {
            lsC.add(1, new Category("21000019", "Giấy xác nhận các mặt hàng kiểm tra giảm"));
            lsC.add(2, new Category("21000047", "Giấy xác nhận các mặt hàng kiểm tra thường/chặt"));
        } else if (k == 2) {
            lsC.add(1, new Category("41000067", "Công văn không phải bộ xét nghiệm nhanh"));
            lsC.add(2, new Category("41000055", "Giấy chứng nhận lưu hành bộ xét nghiệm nhanh"));

        } else if (k == 3) {
            lsC.add(1, new Category("31000033", "Công văn từ chối cấp phép"));
            lsC.add(2, new Category("31000063", "Công văn cấp phép"));
        }
        lbPermitType.setModel(new ListModelList<>(lsC));
        lbPermitType.renderAll();
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        search(userPagingBottom.getActivePage());
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public String getLabel(String key) {
        try {
            return ResourceBundleUtil.getString(key, "language_XNN_vi");

        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLogDB(ex);
        }
        return "";
    }

    //binhnt add method download qrcode
    @Listen("onDownloadQrCode =#lbFiles")
    public void onDownloadQrCode(Event event) {
        Files obj = (Files) event.getData();
        if (obj.getQrCode() != null) {
            try {
//                InputStream in = new ByteArrayInputStream(obj.getQrCode());
//                BufferedImage bImageFromConvert = ImageIO.read(in);
//                File temp = null;
//                ImageIO.write(bImageFromConvert, "png", temp);
                String fileName = "qrcode_" + obj.getFileId().toString() + ".png";
                ResourceBundle rb = ResourceBundle.getBundle("config");
                String filePath = rb.getString("signTemp");// String filePath =
                FileUtil.mkdirs(filePath);
                File f = new File(filePath + fileName);
                if (f.exists()) {
                } else {
                    f.createNewFile();
                }
                InputStream in = new ByteArrayInputStream(obj.getQrCode());
                BufferedImage bImageFromConvert = ImageIO.read(in);
                ImageIO.write(bImageFromConvert, "png", f);
                OutputStream outputStream = new FileOutputStream(fileName);
                outputStream.write(obj.getQrCode());
                outputStream.flush();
                Filedownload.save(f, "png");
                outputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(LookupCosPermitController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println(obj.getFileId().toString());
    }

    public int CheckHaveQrCode(Long fileid) {
        FilesDAOHE cosDao = new FilesDAOHE();
        int check = cosDao.checkHaveQrCode(fileid);
        return check;
    }
}
