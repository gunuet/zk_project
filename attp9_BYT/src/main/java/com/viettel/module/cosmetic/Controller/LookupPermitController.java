/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.VLookupPermitHomepage;
import com.viettel.module.cosmetic.DAO.VLookupPermitHomepageDAO;
import com.viettel.module.rapidtest.DAO.RegisterController;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

/**
 *
 * @author binhnt53
 */
public class LookupPermitController extends BaseComposer {

    @Wire
    Textbox txtBusinessName, txtProductName, txtBusinessAddress, txtReceiveNo;
    @Wire
    Listbox lbLookupPermitManager, lbSort;
    @Wire
    Datebox dbEffectiveDateFrom, dbEffectiveDateTo;
    @Wire
    Paging userPagingBottom;
    VLookupPermitHomepage searchForm;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        VLookupPermitHomepageDAO permitDaohe = new VLookupPermitHomepageDAO();
        List lstProcedure = permitDaohe.findAll();
        ListModelArray lstModelProcedure = new ListModelArray(lstProcedure);
        lbLookupPermitManager.setModel(lstModelProcedure);
        searchForm = new VLookupPermitHomepage();
        fillDataToList();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 
        if (dbEffectiveDateFrom.getValue() != null && !"".equals(dbEffectiveDateFrom.getValue())) {
            searchForm.setEffectiveDateFrom(dbEffectiveDateFrom.getValue());
        }
        if (dbEffectiveDateTo.getValue() != null && !"".equals(dbEffectiveDateTo.getValue())) {
            searchForm.setEffectiveDateTo(dbEffectiveDateTo.getValue());
        }
        if (!lbSort.getSelectedItem().getValue().equals(Constants.HOLIDAY.COMBOBOX_HEADER_SELECT)) {
            Long arangge = Long.valueOf(lbSort.getSelectedItem().getValue().toString());
            searchForm.setArrange(arangge);
        }
        searchForm.setBusinessAddress(txtBusinessAddress.getValue());
        searchForm.setProductName(txtProductName.getValue());
        searchForm.setBusinessName(txtBusinessName.getValue());
        searchForm.setReceiveNo(txtReceiveNo.getValue());
        fillDataToList();
    }

    private void fillDataToList() {
        VLookupPermitHomepageDAO objhe = new VLookupPermitHomepageDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbLookupPermitManager.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public String getLabel(String key) {
        try {
            return ResourceBundleUtil.getString(key, "language_XNN_vi");

        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLogDB(ex);
        }
        return "";
    }
}
