/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.CosCatManufacturer;
import com.viettel.module.cosmetic.DAO.CosManufacturerDAO;
import com.viettel.voffice.model.AttachCategoryModel;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class ManufacturerListController extends BaseComposer {

    @Wire
    Window ManufacturerList;
    @Wire
    Listbox lstmanufacturer;
    @Wire
    Textbox txtName, txtAddress;
    Window parentWindow;
    private CosCatManufacturer obj;

    public ManufacturerListController() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
         Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        obj = new CosCatManufacturer();
        reloadModel();

    }

    @Listen("onClick=#btnSearch")
    public void Seach() {
        obj.setName(txtName.getText().trim());
        obj.setAddress(txtAddress.getText().trim());
        reloadModel();
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        List drSend = new ArrayList();
        Set<Listitem> ls = lstmanufacturer.getSelectedItems();
        if (ls.size() > 0) {
            for (Listitem item : ls) {
                CosCatManufacturer Process = item.getValue();
                drSend.add(Process);
            }
            Map<String, Object> args = new ConcurrentHashMap<>();
            args.put("documentReceiveProcess", drSend);
            Events.sendEvent(new Event("onChooseManufacturer", parentWindow, args));
            ManufacturerList.onClose();
        }
    }

    private void reloadModel() {
        CosManufacturerDAO cosmanudao = new CosManufacturerDAO();
        List lstFileType = cosmanudao.findAllManufacturerCat(obj);
        ListModelArray lstModel = new ListModelArray(lstFileType);
        lstModel.setMultiple(true);
        lstmanufacturer.setModel(lstModel);
    }

}
