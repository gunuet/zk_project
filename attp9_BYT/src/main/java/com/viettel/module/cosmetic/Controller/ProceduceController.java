/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.utils.ResourceBundleUtil;
import org.zkoss.zk.ui.Component;

/**
 *
 * @author binhnt53
 */
public class ProceduceController extends BaseComposer {

    @Override
    public void doAfterCompose(Component window) throws Exception {
        this.getPage().setTitle(ResourceBundleUtil.getString("title"));
        super.doAfterCompose(window);
    }

}
