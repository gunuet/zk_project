/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.CosProductPresentaton;
import com.viettel.module.cosmetic.DAO.ProductPresentationDAO;
import com.viettel.module.cosmetic.Model.ProductPresentationStatus;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author LONG HOANG GIANG
 */
public class ProductPresentationController extends BaseComposer {

    @Wire
    private Textbox tbNameVi;

    @Wire
    private Textbox tbNameEn;

    @Wire
    private Listbox lbListPresentation, lbIsActive;

    @Wire
    private Window presentationWnd;

    private ListModelList presentationsList;

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            fillData();
            doSearch(true);
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            Clients.clearBusy();
        }
    }

    public void fillData() {
        List<ProductPresentationStatus> modelData = new ArrayList<>();
        modelData.add(new ProductPresentationStatus(getLabelName("presentation_list_status_enable"), 1));
        modelData.add(new ProductPresentationStatus(getLabelName("presentation_list_status_disable"), 0));
        ListModelList lml = new ListModelList(modelData);
        lbIsActive.setModel(lml);
        lbIsActive.renderAll();
        lbIsActive.setSelectedIndex(0);
    }

    @Listen("onClick=#btnSearch")
    public void doSearch() {
        Clients.showBusy("");
        try {
            doSearch(false);
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    public void doSearch(boolean initialize) {
        CosProductPresentaton bean = new CosProductPresentaton();
        bean.setNameEn(tbNameEn.getValue());
        bean.setNameVi(tbNameVi.getValue());
        if (initialize) {
            bean.setIsActive(1L);
        } else {
            bean.setIsActive(Long.parseLong(lbIsActive.getSelectedItem().getValue().toString()));
        }
        ProductPresentationDAO productPresentationDAOHE = new ProductPresentationDAO();
        presentationsList = new ListModelList(productPresentationDAOHE.search(bean));
        presentationsList.setMultiple(true);
        lbListPresentation.setModel(presentationsList);
        lbListPresentation.renderAll();
    }

    @Listen("onClick=#btnAddNew")
    public void doAddNew() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("DATA", null);
        arguments.put("parentWindow", presentationWnd);
        Window window = createWindow("windowCRUD",
                "/Pages/module/cosmetic/product-presentation/presentationCreate.zul", arguments, Window.MODAL);
        window.setMode(Window.MODAL);
//        presentationWnd.setVisible(false);
    }

    @Listen("onEditItem=#lbListPresentation")
    public void doEditItem(ForwardEvent evt) {
        Image image = (Image) evt.getOrigin().getTarget();
        Listitem litem = (Listitem) image.getParent().getParent();
        CosProductPresentaton presentationItem = (CosProductPresentaton) litem.getValue();
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("DATA", presentationItem);
        arguments.put("parentWindow", presentationWnd);
        Window window = createWindow("windowCRUD",
                "/Pages/module/cosmetic/product-presentation/presentationCreate.zul", arguments, Window.MODAL);
        window.setMode(Window.MODAL);
    }

    @Listen("onDeleteItem=#lbListPresentation")
    public void doDeleteItem(ForwardEvent evt) {

        Messagebox.show(getLabelName("presentation_confirm_delete"), getLabelName("common_notification_title"), Messagebox.OK | Messagebox.CANCEL, Messagebox.EXCLAMATION, Messagebox.CANCEL, new EventListener() {

            @Override
            public void onEvent(Event event) throws InterruptedException {
                try {
                    if (Messagebox.ON_OK.equals(event.getName())) {
                        CosProductPresentaton presentationItem = (CosProductPresentaton) lbListPresentation.getSelectedItem().getValue();
                        ProductPresentationDAO daoHE = new ProductPresentationDAO();
                        daoHE.delete(presentationItem);
                        doSearch();
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }
            }
        });
    }

    @Listen("onVisible=#presentationWnd")
    public void onVisible() {
        doSearch();
    }

    public Textbox getTbNameVi() {
        return tbNameVi;
    }

    public void setTbNameVi(Textbox tbNameVi) {
        this.tbNameVi = tbNameVi;
    }

    public Textbox getTbNameEn() {
        return tbNameEn;
    }

    public void setTbNameEn(Textbox tbNameEn) {
        this.tbNameEn = tbNameEn;
    }

    public ListModelList getPresentationsList() {
        return presentationsList;
    }

    public void setPresentationsList(ListModelList presentationsList) {
        this.presentationsList = presentationsList;
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

}
