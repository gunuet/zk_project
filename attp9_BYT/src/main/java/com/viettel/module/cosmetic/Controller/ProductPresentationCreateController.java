/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.CosProductPresentaton;
import com.viettel.module.cosmetic.DAO.ProductPresentationDAO;
import com.viettel.module.cosmetic.Model.ProductPresentationStatus;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author LONG HOANG GIANG
 */
public class ProductPresentationCreateController extends BaseComposer {

    @Wire
    private Listbox lbIsActive;
    
    @Wire
    private Textbox tbNameVi, tbNameEn;
    
    @Wire
    private Window presentationCreateWnd;
    private Window parentWindow;
    
    @Wire
    private Button btnAddOrUpdate;
    
    private CosProductPresentaton productPresentationItem;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        productPresentationItem = (CosProductPresentaton) arguments.get("DATA");
        return super.doBeforeCompose(page, parent, compInfo);
    }
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillData();
    }
    
    private void fillData() {
        
        List<ProductPresentationStatus>modelData = new ArrayList<ProductPresentationStatus>();
        modelData.add(new ProductPresentationStatus(getLabelName("presentation_list_status_enable"), 1));
        modelData.add(new ProductPresentationStatus(getLabelName("presentation_list_status_disable"), 0));
        ListModelList lml = new ListModelList(modelData);
        lbIsActive.setModel(lml);
        lbIsActive.renderAll();
        lbIsActive.setSelectedIndex(0);
        
//        btnAddOrUpdate.setLabel(getLabelName("presentation_create_label_add"));
        if (productPresentationItem != null) {
            tbNameVi.setValue(productPresentationItem.getNameVi());
            tbNameEn.setValue(productPresentationItem.getNameEn());
            lbIsActive.setSelectedIndex(productPresentationItem.getIsActive() == 1L ? 0 : 1);
//            btnAddOrUpdate.setLabel(getLabelName("presentation_create_label_update"));
        }
    }
    
    @Listen("onAddOrUpdate=#presentationCreateWnd")
    public void doAddOrUpdate(ForwardEvent e) {
        Boolean update = productPresentationItem != null;
        Boolean closeWnd = update;
        if (!closeWnd && (e.getData() != null)) {
            closeWnd = (1 == Integer.parseInt(e.getData().toString()));
        }
        String msg = productPresentationItem == null ? getLabelName("common_add_success") : getLabelName("common_update_success");
        if (productPresentationItem == null) {
            productPresentationItem = new CosProductPresentaton();
        }
        if ("".equals(tbNameVi.getValue()) && "".equals(tbNameEn.getValue())) {
            Messagebox.show(getLabelName("presentation_check_name"), getLabelName("common_notification_title"), Messagebox.OK, Messagebox.INFORMATION);
            return;
        }
        productPresentationItem.setNameVi(textBoxGetValue(tbNameVi));
        productPresentationItem.setNameEn(textBoxGetValue(tbNameEn));
        try {
            productPresentationItem.setIsActive(Long.valueOf(lbIsActive.getSelectedItem().getValue().toString()));
        } catch(NumberFormatException ex) {
            LogUtils.addLogDB(ex);
            productPresentationItem.setIsActive(0L);
        }
        ProductPresentationDAO productPresentationDAO = new ProductPresentationDAO();
        productPresentationDAO.saveOrUpdate(productPresentationItem);
        showNotification(msg, Clients.NOTIFICATION_TYPE_INFO);
        // Neu cap nhat thi sau khi nhan cap nhat, reload lai listbox & hidden form.
        if (update || closeWnd) {
            onClose();
        } else {
            productPresentationItem = null;
            tbNameEn.setValue("");
            tbNameVi.setValue("");
        }
                
    }
   
    @Listen("onClose=#presentationCreateWnd")
    public void onClose() {
        Events.sendEvent("onVisible", parentWindow, this);
        presentationCreateWnd.detach();
    }

    public CosProductPresentaton getProductPresentationItem() {
        return productPresentationItem;
    }

    public void setProductPresentationItem(CosProductPresentaton productPresentationItem) {
        this.productPresentationItem = productPresentationItem;
    }
    
    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }
}
