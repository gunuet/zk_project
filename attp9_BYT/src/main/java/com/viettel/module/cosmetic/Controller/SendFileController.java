/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.workflow.BusinessController;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author duv
 */
public class SendFileController extends BusinessController {

    private static final long serialVersionUID = 1L;
    Long docId = null;
    Long docType = null;
    @Wire
    Textbox txtValidate;
    @Wire
    private Label lbWarning;
    private Window parentWindow;
    String sMessage;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("windowParent");
        docId = (Long) arguments.get("docId");
        docType = (Long) arguments.get("docType");
        sMessage = "";
        //lsumMoney = objBill.getTotal();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        CosmeticAttachDAO rDaoHe = new CosmeticAttachDAO();
        List<VFileRtAttach> lstCosmeticAttach = rDaoHe.findCosmeticAttach(docId);
        AttachDAOHE rfinalDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rfinalDaoHe.getByObjectIdAndType(docId, Constants.OBJECT_TYPE.COSMETIC_HO_SO_GOC);
        if ((lstCosmeticAttach.size() <= 0)&&(lstAttach.size() <= 0)) {
            sMessage = "Hồ sơ của bạn thiếu file đính kèm danh sách tài liệu và file đính kèm hồ sơ gốc bạn vui lòng nhập thêm";
            txtValidate.setValue("0");
            lbWarning.setValue(sMessage);
            lbWarning.setVisible(true);
            return;
        } else if (lstCosmeticAttach.size() <= 0) {
            sMessage = "Hồ sơ của bạn thiếu file đính kèm danh sách tài liệu bạn vui lòng nhập thêm";
            txtValidate.setValue("0");
            lbWarning.setValue(sMessage);
            lbWarning.setVisible(true);
            return;
        } 
//        else if (lstAttach.size() <= 0) {
//            sMessage = "Hồ sơ của bạn thiếu file đính kèm hồ sơ gốc bạn vui lòng nhập thêm";
//            txtValidate.setValue("0");
//            lbWarning.setValue(sMessage);
//            lbWarning.setVisible(true);
//            return;
//        } 
        else {
            txtValidate.setValue("1");
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        if ("1".equals(txtValidate.getValue())) {
            showNotification(String.format(Constants.Notification.SENDED_SUCCESS, "hồ sơ"), Constants.Notification.INFO);
            LogUtils.addLog("Override: " + this.getClass().getName() + " clicked OK!");
        }
    }
}
