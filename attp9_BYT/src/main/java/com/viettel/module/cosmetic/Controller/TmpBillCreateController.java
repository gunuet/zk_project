/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.TmpBill;
import com.viettel.module.cosmetic.DAO.TmpBillDAO;
import com.viettel.module.rapidtest.BO.Template;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import org.zkoss.poi.hssf.usermodel.HSSFCell;
import org.zkoss.poi.hssf.usermodel.HSSFRow;
import org.zkoss.poi.hssf.usermodel.HSSFSheet;
import org.zkoss.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.poi.ss.usermodel.DataFormatter;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class TmpBillCreateController extends BaseComposer {

    @Wire
    Image imgDelFile;
    @Wire
    Textbox txtFileName, txtTemplateName, txtDeptName, txtDeptId;
    @Wire
    Listbox lbProcedure, lbTemplateType;
    @Wire
    Window tmpBillCreateWnd;
    private Attachs attachCurrent;
    @Wire
    private Vlayout flist;
    @Wire
    Label lbdeletefile;
    @Wire
    private Label lbWarning;
    private Media media;
    boolean bUploadFile = false, clickDeletefile = false;

    public TmpBillCreateController() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
    }

    @Listen("onClick=#btnSave")
    public void onSave() throws IOException {
        Template rs = new Template();
        if (txtTemplateName.getValue() != null && txtTemplateName.getValue().trim().length() == 0) {
            showNotification("Tên biểu mẫu không thể để trống", Constants.Notification.ERROR);
            txtTemplateName.focus();
            return;
        } else {
            rs.setTemplateName(txtTemplateName.getValue());
        }
        if (lbProcedure.getSelectedItem() != null) {
            int idx = lbProcedure.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setProcedureId((Long) lbProcedure.getSelectedItem().getValue());
                rs.setProcedureName(lbProcedure.getSelectedItem().getLabel());
            } else {
                showNotification("Thủ tục không thể để trống", Constants.Notification.ERROR);
                lbProcedure.focus();
                return;
            }
        }
        if (lbTemplateType.getSelectedItem() != null) {
            int idx = lbTemplateType.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setTemplateTypeId((Long) lbTemplateType.getSelectedItem().getValue());
                rs.setTemplateTypeName(lbTemplateType.getSelectedItem().getLabel());
            } else {
                showNotification("Loại biểu mẫu trong hồ sơ không thể để trống", Constants.Notification.ERROR);
                lbTemplateType.focus();
                return;
            }
        }

        if (txtDeptId.getValue() != null && txtDeptId.getValue().trim().length() == 0) {
            showNotification("Đơn vị không thể để trống", Constants.Notification.ERROR);
            txtDeptId.focus();
            return;
        } else {
            rs.setDeptId(Long.parseLong(txtDeptId.getValue()));
        }

        if (txtDeptName.getValue() != null && txtDeptName.getValue().trim().length() == 0) {
            showNotification("Đơn vị không thể để trống", Constants.Notification.ERROR);
            txtDeptName.focus();
            return;
        } else {
            rs.setDeptName(txtDeptName.getValue());
        }

        rs.setIsActive(1L);
        TemplateDAOHE rdhe = new TemplateDAOHE();
        if (rdhe.hasDuplicate(rs)) {
            showNotification("Trùng dữ liệu đã tạo trên hệ thống");
            return;
        }
        rdhe.saveOrUpdate(rs);
        showNotification("Lưu thành công", Constants.Notification.INFO);

        Window parentWnd = (Window) Path.getComponent("/templateManageWnd");
        Events.sendEvent(new Event("onReload", parentWnd, null));
    }

    @Listen("onUpload = #btnUpload")
    public void onUpload(UploadEvent evt) throws IOException {
        media = evt.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            String sExt = ResourceBundleUtil.getString("extend_file", "config");
            showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                    Constants.Notification.WARNING);
            return;
        }
        InputStream is = media.getStreamData();
        HSSFWorkbook workbook = new HSSFWorkbook(is);
        //Get first sheet from the workbook
        HSSFSheet sheet = workbook.getSheetAt(0);
        for (int i = 10; i < sheet.getPhysicalNumberOfRows(); i++) {
            HSSFRow row = sheet.getRow(i);
            HSSFCell cell0 = row.getCell((short) 0);
            HSSFCell cell1 = row.getCell((short) 1);
            HSSFCell cell2 = row.getCell((short) 2);
            HSSFCell cell3 = row.getCell((short) 3);
            HSSFCell cell4 = row.getCell((short) 4);
            HSSFCell cell5 = row.getCell((short) 5);
            HSSFCell cell6 = row.getCell((short) 7);
            if (cell0 == null || cell0.getCellType() == cell0.CELL_TYPE_BLANK) {
                // This cell is empty
                break;
            }
            TmpBillDAO tmpbillDao = new TmpBillDAO();
            TmpBill tmpBillBo = new TmpBill();
            Date today = cell0.getDateCellValue();
//            String reportDate = df.format(today);
            tmpBillBo.setBillDate(today);
            tmpBillBo.setBillNo(convertCellExel(cell1));
            tmpBillBo.setType(convertCellExel(cell2));
            tmpBillBo.setCode(convertCellExel(cell3));
            tmpBillBo.setProductCode(convertCellExel(cell4));
            tmpBillBo.setBusinessName(convertCellExel(cell5));
            DataFormatter dfs = new DataFormatter();
            String asItLooksInExcel = dfs.formatCellValue(cell6);
            tmpBillBo.setPrice(asItLooksInExcel);
            if ("Mỹ phẩm".equals(tmpBillBo.getType())) {
                tmpbillDao.saveOrUpdate(tmpBillBo);
            }
        }
        showNotification("Lưu thành công", Constants.Notification.INFO);
    }

    public Attachs getAttachCurrent() {
        return attachCurrent;
    }

    public void setAttachCurrent(Attachs attachCurrent) {
        this.attachCurrent = attachCurrent;
    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
    }

    private String convertCellExel(HSSFCell cell) {
        if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
            return String.valueOf(cell.getNumericCellValue()).trim();
        } else if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
            return cell.getStringCellValue().trim();
        } else if (cell.getCellType() == HSSFCell.CELL_TYPE_ERROR) {
            return "";//cell.getErrorCellValue();        
        } else if (cell.getCellType() == HSSFCell.CELL_TYPE_FORMULA) {
            try {
                return cell.getStringCellValue().trim();
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
                return "";
            }
        } else {
            return cell.getStringCellValue().trim();
        }
    }
}
