/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.rapidtest.BO.Template;

import java.io.IOException;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class TmpBillManageController extends BaseComposer {

    //Control tim kiem
    Window showDeptDlg;
    @Wire
    Paging userPagingBottom;
    Template searchForm;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
    }

    @Listen("onClick=#btnImport")
    public void onCreate() throws IOException {
        Window window = (Window) Executions.createComponents(
                "/Pages/admin/utility/tmpBillCreate.zul", null, null);
        window.doModal();
    }
}
