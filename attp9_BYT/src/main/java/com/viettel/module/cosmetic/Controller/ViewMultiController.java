/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.utils.Constants;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class ViewMultiController extends BaseComposer {

    @Wire
    private Div topToolbar;
    private List<Component> listTopActionComp;

    private List<VFileCosfile> lstCosmetic;
    private VFileCosfile vFileCosfile;
    private long FILE_TYPE, DEPT_ID;
    private com.viettel.core.workflow.BO.Process processCurrent;// process dang duoc xu li
    private List<Process> listProcessCurrent;
    private List<Long> listDocId, listDocType;

    public ViewMultiController() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        lstCosmetic = (List<VFileCosfile>) arguments.get("lstCosmetic");
        initThread();
    }

    private void initThread() {
//        List<Component> listTopButton = topToolbar.getChildren();
        Components.removeAllChildren(topToolbar);
        listTopActionComp = new ArrayList<>();
//        listProcessCurrent = new ArrayList<>();
//        listDocId = new ArrayList<>();
//        listDocType = new ArrayList<>();
        if (lstCosmetic.size() > 0) {
            vFileCosfile = lstCosmetic.get(0);
            processCurrent = WorkflowAPI.getInstance().
                    getCurrentProcess(vFileCosfile.getFileId(), vFileCosfile.getFileType(),
                            vFileCosfile.getStatus(), getUserId());
            FILE_TYPE = vFileCosfile.getFileType();
            DEPT_ID = Constants.CUC_QLD_ID;

            addAllNextActions();
            loadActionsToToolbar();
        }
    }

    private void addAllNextActions() {
        Long docStatus = vFileCosfile.getStatus();
        Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;
        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(
                docType, docStatus, deptId);

        for (NodeToNode action : actions) {
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
    }

    private Button createButtonForAction(NodeToNode action) {
        if (validateUserRight(action, processCurrent, vFileCosfile)) {
            return null;
        }

        Button btn = new Button(action.getAction());

        final String actionName = action.getAction();
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action
        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = vFileCosfile.getStatus();
        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId,false));

        org.zkoss.zk.ui.event.EventListener<Event> event = new org.zkoss.zk.ui.event.EventListener<Event>() {
            @Override
            public void onEvent(Event t) throws Exception {
                Map<String, Object> data = new ConcurrentHashMap<>();
                data.put("fileId", vFileCosfile.getFileId());
                data.put("docId", vFileCosfile.getFileId());
                data.put("docType", vFileCosfile.getFileType());
                data.put("docStatus", status);
                data.put("actionId", actionId);
                data.put("actionName", actionName);
                data.put("actionType", actionType);
                data.put("nextId", nextId);
                data.put("previousId", prevId);
                if (processCurrent != null) {
                    data.put("process", processCurrent);
                }
                data.put("lstAvailableNDU", lstNDUs);
                // data.put("parentWindow", windowView);
                createWindow("windowComment", formName, data, Window.MODAL);
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    public boolean validateUserRight(NodeToNode action, com.viettel.core.workflow.BO.Process processCurrent,
            VFileCosfile file) {
        // check if action is RETURN_PREVIOUS but parent process came from another node
        if (processCurrent != null
                && action.getType() == Constants.NODE_ASSOCIATE_TYPE.RETURN_PREVIOUS
                && action.getNextId() != processCurrent.getPreviousNodeId()) {
            return true;
        }
        // check if receiverId is diferent to current user id
        // find all process having current status of document
        List<com.viettel.core.workflow.BO.Process> listCurrentProcess = WorkflowAPI.getInstance().findAllCurrentProcess(file.getFileId(), file.getFileType(),
                file.getStatus());
        Long userId = getUserId();
        Long creatorId = file.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return true;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                for (com.viettel.core.workflow.BO.Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                if (!needToProcess) {
                    return true;
                }
            }
        }
        return false;
    }

    // Hien thi cac action tren thanh toolbar top va bottom
    public void loadActionsToToolbar() {
        for (Component comp : listTopActionComp) {
            topToolbar.appendChild(comp);
        }
    }

}
