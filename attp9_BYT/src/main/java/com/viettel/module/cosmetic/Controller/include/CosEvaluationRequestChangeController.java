package com.viettel.module.cosmetic.Controller.include;

import com.viettel.core.workflow.BusinessController;
import com.viettel.module.cosmetic.BO.CosAdditionalRequest;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.cosmetic.DAO.CosmeticDAO;
import com.viettel.module.cosmetic.DAO.CosAdditionalRequestDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class CosEvaluationRequestChangeController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox mainContent;
    @Wire
    Textbox txtValidate;
    private VFileCosfile vFileCosfile;
    private CosEvaluationRecord obj = new CosEvaluationRecord();

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        CosEvaluationRecordDAO objDAO = new CosEvaluationRecordDAO();
        CosEvaluationRecord object = objDAO.getLastEvaluation(fileId);
        if (object != null) {
            obj.copy(object);
        }

        CosmeticDAO cosmeticDAO = new CosmeticDAO();
        vFileCosfile = cosmeticDAO.findViewByFileId(fileId);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
    String message;
        if ("".equals(mainContent.getText().trim())) {
            message = "Không được để trống nội dung yêu cầu bổ sung";
            mainContent.focus();
            showWarningMessage(message);
            return false;
        }
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;

        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
            onSignDispatchTP();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @throws java.lang.Exception
     */
    public void onSignDispatchTP() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        
        CosAdditionalRequestDAO objDAO = new CosAdditionalRequestDAO();
        CosAdditionalRequest additionalRequest = new CosAdditionalRequest();
        createAdditionalrequest(additionalRequest);
        objDAO.saveOrUpdate(additionalRequest);
        showNotification(String.format(
                Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                Constants.Notification.INFO);
         txtValidate.setValue("1");

    }

    private void createAdditionalrequest(CosAdditionalRequest objRequest) throws Exception {
        Date dateNow = new Date();
        objRequest.setCreateDate(dateNow);
        objRequest.setCreateDeptId(getDeptId());
        objRequest.setCreateDeptName(getDeptName());
        objRequest.setCreatorId(getUserId());
        objRequest.setCreatorName(getUserFullName());
        //objRequest.setStatus(Long.valueOf((String) rbStatus.getSelectedItem().getValue()));
        objRequest.setFileId(fileId);
        objRequest.setContent(mainContent.getValue().trim());

        objRequest.setIsActive(Constants.Status.ACTIVE);
    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    public VFileCosfile getvFileCosfile() {
        return vFileCosfile;
    }

    public void setvFileCosfile(VFileCosfile vFileCosfile) {
        this.vFileCosfile = vFileCosfile;
    }
}
