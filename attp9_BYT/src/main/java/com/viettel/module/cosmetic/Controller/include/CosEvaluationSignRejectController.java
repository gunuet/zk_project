package com.viettel.module.cosmetic.Controller.include;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.BO.CosReject;
import com.viettel.module.cosmetic.DAO.CosmeticDAO;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.cosmetic.DAO.CosRejectDAO;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.cosmetic.Model.CosExportModel;
import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.*;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

/**
 *
 * @author nghiepnc
 */
public class CosEvaluationSignRejectController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private Long fileId;
    private Long docType;
    private String bCode;
    private List listBook;
    @Wire
    private Textbox mainContent;
    @Wire
    Textbox txtValidate;
    private VFileCosfile vFileCosfile;
    private CosEvaluationRecord obj = new CosEvaluationRecord();
    private CosReject cosreject;
    private String fileSignOut = "";
    @Wire
    private Button btnExport;
    @Wire
    Textbox txtCertSerial, txtBase64Hash;

    /**
     * nghiepnc Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_REJECT;
        docType = (Long) arguments.get("docType");

        CosEvaluationRecordDAO objDAOHE = new CosEvaluationRecordDAO();
        CosEvaluationRecord object = objDAOHE.getLastEvaluation(fileId);
        if (object != null) {
            obj.copy(object);
            //obj.setEvaluationRecordId(null);
        }
        CosmeticDAO cosDao = new CosmeticDAO();

        //nghiepnc set code
        vFileCosfile = cosDao.findViewByFileId(fileId);
        return super.doBeforeCompose(page, parent, compInfo);
    }

    /**
     * nghiepnc Ham thuc hien sau khi load form xong
     */
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        String message;
        if ("".equals(mainContent.getText().trim())) {
            message = "Không được để trống nội dung từ chối";
            mainContent.focus();
            showWarningMessage(message);
            return false;
        }
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                btnSubmit();
                break;
        }
    }

    @Listen("onClick=#btnExport")
    public void btnExport(Event event) {
        try {
            clearWarningMessage();
//            CosRejectDAO rejectDAOHE = new CosRejectDAO();
//            List<CosReject> lstReject = rejectDAOHE.findAllActiveByFileId(fileId);
//
//            if (lstReject.size() > 0) {
//                //Co co cong van thi lay ban cu
//                cosreject = lstReject.get(0);
//            } else {
//                cosreject = new CosReject();
//                createReject();
//            }
//            rejectDAOHE.saveOrUpdate(cosreject);
//            Long bookNumber = putInBook(cosreject);//Vao so
//            cosreject.setReceiveNo(String.valueOf(bookNumber));
//
//            rejectDAOHE.saveOrUpdate(cosreject);

            //lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi luu vao attach)
//            CosmeticDAO cosmeticDAO = new CosmeticDAO();
//            VFileCosfile vFileCosfile2 = cosmeticDAO.findViewByFileId(fileId);
//            String pathTemplate = getPathTemplate(fileId);
//            CosExportModel model = setModelObject(cosreject, vFileCosfile2, pathTemplate);
            ExportFileDAO exportFileDAO = new ExportFileDAO();
            exportFileDAO.exportRejectDocumentNosign(getUserId(), getUserFullName(), fileId, null, true);
//            showNotification(String.format(
//                    Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
//                    Constants.Notification.INFO);
            txtValidate.setValue("1");
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    //@Override
    @Listen("onClick=#btnSubmit")
    public void btnSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
            onRejectFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public void onRejectFile() throws Exception {
        if (!isValidatedData()) {
            txtValidate.setValue("0");
            return;
        }

//        CosEvaluationRecordDAO objDAOHE = new CosEvaluationRecordDAO();
//        obj = createApproveObject();
//        objDAOHE.saveOrUpdate(obj);
        onSignReject();

    }

    public void onSignReject() throws Exception {
        clearWarningMessage();
        CosRejectDAO rejectDAOHE = new CosRejectDAO();
        List<CosReject> lstReject = rejectDAOHE.findAllActiveByFileId(fileId);

        if (lstReject.size() > 0) {
            //Co co cong van thi lay ban cu
            cosreject = lstReject.get(0);
        } else {
            cosreject = new CosReject();
            createReject();
        }
        rejectDAOHE.saveOrUpdate(cosreject);
        Long bookNumber = putInBook(cosreject);//Vao so
        cosreject.setReceiveNo(String.valueOf(bookNumber));

        rejectDAOHE.saveOrUpdate(cosreject);
        ExportFileDAO exportFileDAO = new ExportFileDAO();
        exportFileDAO.exportRejectDocument(getUserId(), getUserFullName(), fileId, cosreject, true);
        showNotification(String.format(
                Constants.Notification.REJECT_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                Constants.Notification.INFO);
        base.showNotify("Từ chối hồ sơ thành công!");
        txtValidate.setValue("1");
    }

    public void onSignRejectUpdateDb(String fileName) throws Exception {
        ExportFileDAO exp = new ExportFileDAO();
        exp.updateAttachSignFileSDBS(cosreject, fileName);
        showNotification("Ký số thành công!");
    }

    public String onSignRejectSign() throws Exception {
        clearWarningMessage();
        CosRejectDAO rejectDAOHE = new CosRejectDAO();
        List<CosReject> lstReject = rejectDAOHE.findAllActiveByFileId(fileId);

        if (lstReject.size() > 0) {
            //Co co cong van thi lay ban cu
            cosreject = lstReject.get(0);
        } else {
            cosreject = new CosReject();
            createReject();
        }
        rejectDAOHE.saveOrUpdate(cosreject);
        Long bookNumber = putInBook(cosreject);//Vao so
        cosreject.setReceiveNo(String.valueOf(bookNumber));
        rejectDAOHE.saveOrUpdate(cosreject);

        //lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi luu vao attach)
//        CosmeticDAO cosmeticDAO = new CosmeticDAO();
//        VFileCosfile vFileCosfile2 = cosmeticDAO.findViewByFileId(fileId);
//        String pathTemplate = getPathTemplate(fileId);
//        CosExportModel model = setModelObject(cosreject, vFileCosfile2, pathTemplate);
        ExportFileDAO exportFileDAO = new ExportFileDAO();
        return exportFileDAO.exportRejectDocumentSign(getUserId(), getUserFullName(), fileId, cosreject, false);
    }

    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        actionSignCA(event, actionPrepareSign());
    }

    public String actionPrepareSign() {
        String fileToSign = "";
        try {
            fileToSign = onSignRejectSign();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        SignPdfFile pdfSig = new SignPdfFile();//pdfSig = new SignPdfFile();        
        String base64Hash;
        String certSerial = x509Cert.getSerialNumber().toString(16);
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");
        FileUtil.mkdirs(filePath);
        String outputFileFinalName = "_" + (new Date()).getTime() + ".pdf";
        String outPutFileFinal = filePath + outputFileFinalName;
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");//not use binhnt53 u230315
            String linkImageSign = folderPath + separator + caur.get(0).getSignature();
            String linkImageStamp = folderPath + separator + caur.get(0).getStamper();
            //fileSignOut = outPutFileFinal;
            Pdf pdfProcess = new Pdf();
            try {//chen chu ki
                pdfProcess.insertImageAll(fileToSign, outPutFileFinal, linkImageSign, linkImageStamp, null);
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
            if (pdfProcess.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }

            base64Hash = pdfSig.createHash(outPutFileFinal, new Certificate[]{x509Cert});

            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64Hash.setValue(base64Hash);
            txtCertSerial.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmit();");
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    /**
     *
     * @param event
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) {
        String data = event.getData().toString();
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
//        String inputFileName = "_" + (new Date()).getTime() + ".pdf";//NOT ƯSE BINHNT53 230315
        String outputFileName = "_signed_" + (new Date()).getTime() + ".pdf";
//        String inputFile = filePath + inputFileName;//NOT ƯSE BINHNT53 230315
        String outputFile = filePath + outputFileName;
        fileSignOut = outputFile;
        String signature = data;
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");

        try {
            pdfSig.insertSignature(signature, outputFile);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        try {
            onSignRejectUpdateDb(fileSignOut);
            fileSignOut = "";
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
    }

    private String getPathTemplate(Long fileId) {
        //Get Template
        WorkflowAPI wAPI = new WorkflowAPI();
        Flow flow = wAPI.getFlowByFileId(fileId);
        Long deptId = flow.getDeptId();
        Long procedureId = flow.getObjectId();

        TemplateDAOHE the = new TemplateDAOHE();
        String pathTemplate = the.findPathTemplate(deptId, procedureId,
                Constants.PROCEDURE_TEMPLATE_TYPE.PERMIT);
        return pathTemplate;
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(CosReject cosreject) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);//Lay so dau tien
        BookDocument bookDocument = createBookDocument(cosreject.getRejectId(), book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

//    private CosEvaluationRecord createApproveObject() throws Exception {
//        Date dateNow = new Date();
//        FilesModel model = new FilesModel(fileId);
//        obj.setBusinessAddress(model.getBusinessAddress());
//        obj.setBusinessName(model.getBusinessName());
//        obj.setProductName(model.getCosFile().getProductName());
//        obj.setCreateDate(dateNow);
//        obj.setStatus(Constants_Cos.EVALUTION.FILE_NOK);
//        obj.setFileId(fileId);
//        obj.setMainContent(mainContent.getValue());
//        obj.setIsActive(Constants.Status.ACTIVE);
//        obj.setUserId(getUserId());
//        obj.setUserName(getUserFullName());
//        return obj;
//    }
    private CosReject createReject() throws Exception {
        cosreject.setFileId(fileId);
        cosreject.setIsActive(Constants.Status.ACTIVE);
        cosreject.setStatus(Constants.PERMIT_STATUS.SIGNED);
        cosreject.setReceiveDate(new Date());
        cosreject.setSignDate(new Date());
        return cosreject;

    }

    private CosExportModel setModelObject(CosReject cosreject, VFileCosfile vFileCosfile, String pathTemplate) throws Exception {
        CosExportModel model = new CosExportModel();
        //obj.setBusinessName(additionalRequest.geadditionalRequesttb);
        model.setBusinessName(vFileCosfile.getBusinessName());
        model.setLeaderSinged(getUserFullName());
        model.setObjectId(cosreject.getRejectId());
        model.setObjectType(Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH);
        model.setRolesigner("Lãnh đạo");
        model.setSignDate(new Date());
        model.setSigner(getUserFullName());
//        model.setRapidTestName(vFileCosfile.getRapidTestName());
//        model.setPlaceOfManufacture(vFileCosfile.getPlaceOfManufacture());
        model.setBusinessAddress(vFileCosfile.getBusinessAddress());
        model.setSendNo(cosreject.getReceiveNo());
        model.setPathTemplate(pathTemplate);
        model.setTypeExport(Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP);

        return model;

    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    public VFileCosfile getvFileCosfile() {
        return vFileCosfile;
    }

    public void setvFileCosfile(VFileCosfile vFileCosfile) {
        this.vFileCosfile = vFileCosfile;
    }
}
