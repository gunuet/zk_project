package com.viettel.module.cosmetic.Controller.include;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.DAO.CosAdditionalRequestDAO;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;

/**
 *
 * @author Linhdx
 */
public class DispatchIncludeController extends BaseComposer {

    Long fileId;
    @Wire
    private Listbox lbDispatch;

    private List lstDispatch;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("id");
        CosAdditionalRequestDAO cosAdditionalRequestDAO = new CosAdditionalRequestDAO();
        lstDispatch = cosAdditionalRequestDAO.findAllActiveByFileId(fileId);
        lbDispatch.setModel(new ListModelArray(lstDispatch));

    }

}
