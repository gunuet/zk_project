package com.viettel.module.cosmetic.Controller.include;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.cosmetic.BO.CosReject;
import com.viettel.module.cosmetic.DAO.CosRejectDAO;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;

/**
 *
 * @author Linhdx
 */
public class DispatchRejectIncludeController extends BaseComposer {

    Long fileId;
    private CosReject reject;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("id");
        CosRejectDAO cosRejectDAO = new CosRejectDAO();
            List<CosReject> lstReject = cosRejectDAO.findAllActiveByFileId(fileId);
            if (lstReject != null && lstReject.size() > 0) {
                reject = lstReject.get(0);
            }

    }
    
    public void onDownloadReject() throws FileNotFoundException {
        CosRejectDAO cosRejectDAO = new CosRejectDAO();
        reject = cosRejectDAO.findLastByFileId(fileId);

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> lstAttach = attDAOHE.getByObjectIdAndType(reject.getRejectId(), Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH);
        Attachs att = null;
        if (lstAttach != null && lstAttach.size() > 0) {
            att = lstAttach.get(0);

        }
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }
    
    

}
