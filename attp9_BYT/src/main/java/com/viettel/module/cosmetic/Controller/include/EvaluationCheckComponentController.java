package com.viettel.module.cosmetic.Controller.include;

import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.cosmetic.BO.CosCosfileIngre;
import com.viettel.module.cosmetic.DAO.AnnexeDAO;
import com.viettel.module.cosmetic.DAO.CosCosfileIngreDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class EvaluationCheckComponentController extends BusinessController {

    @Wire("#incList #lbCosfileIngreCheck")
    private Listbox lbCosfileIngreCheck;

    @Wire("#incList #lbAnnexe")
    private Listbox lbAnnexe;
    @Wire("#incList #txtSubstance")
    Textbox txtSubstance;
    @Wire("#incList #userPagingBottom")
    private Paging userPagingBottom;

    // private List<CosCosfileIngre> lstCosfileIngre = new ArrayList();
    private Long fileId;
    private Long cosFileId;
    private List ingredientListCheck, lstAnnexe;
    private static final long serialVersionUID = 1L;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        CategoryDAOHE catDao = new CategoryDAOHE();
        cosFileId = Long.parseLong(catDao.LayTruongcosFileId(fileId));
        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        CosCosfileIngreDAO ingredientDAO = new CosCosfileIngreDAO();
        List<CosCosfileIngre> ingredientList = ingredientDAO.findByCosFileId(cosFileId);
        ingredientListCheck = ingredientDAO.findListIngreAnnexe(cosFileId);
        lbCosfileIngreCheck.setModel(new ListModelArray(ingredientList));
        if (ingredientList.size() > 0) {
            reloadModel(ingredientList.get(0).getIngredientName());
            txtSubstance.setText(ingredientList.get(0).getIngredientName());
        } else {
            reloadModel("**********************");
        }
    }

    @Listen("onOpenView = #incList #lbCosfileIngreCheck")
    public void onOpenView(Event event) {
        CosCosfileIngre cosfileingre = (CosCosfileIngre) event.getData();
        reloadModel(cosfileingre.getIngredientName());
        txtSubstance.setText(cosfileingre.getIngredientName());
    }

    @Listen("onClick=  #incList #btnSearch")
    public void onClickbtnSearch() {
        userPagingBottom.setActivePage(0); 
        reloadModel(txtSubstance.getText());
    }

    @Listen("onPaging = #incList #userPagingBottom")
    public void onPaging(Event event) {

        reloadModel(txtSubstance.getText());
    }

    public void reloadModel(String text) {
        AnnexeDAO annexeDAO = new AnnexeDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm;
        plm = annexeDAO.findBySubstance(text, take, start);
        userPagingBottom.setTotalSize(plm.getCount());
        lstAnnexe = plm.getLstReturn();
        lbAnnexe.setModel(new ListModelArray(lstAnnexe));
    }

    public Boolean Checkingredient(CosCosfileIngre cosCosfileIngre, String hidden) {
        for (int i = 0; i < ingredientListCheck.size(); i++) {
            Long b = Long.parseLong(cosCosfileIngre.getCosfileIngreId().toString());
            Long a = Long.parseLong(ingredientListCheck.get(i).toString());
            if (a.equals(b)) {
                if ("1".equals(hidden)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        if ("1".equals(hidden)) {
            return false;
        } else {
            return true;
        }
    }

}
