package com.viettel.module.cosmetic.Controller.include;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zul.Div;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class EvaluationController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, labelLegal, labelCriteria,lbEvalType,
            labelMechanism, labelLegalRequi, labelCriteriaRequi, labelMechanismRequi, labelProductNameRequi, labelProductName;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    @Wire
    private Window windowEvaluation;
    private Long fileId;
    private Long userEvaluationType;
    @Wire
    private Textbox userEvaluationTypeH, txtValidate;
    @Wire
    private Radiogroup rbStatus;
    @Wire
    private Textbox mainContent, legalContent, mechanismContent, ProductNameContent, criteriaContent, lbStatus;
    @Wire
    private Listbox lbEffective, lbLegal, lbMechanism, lbCriteria, lbProductName;
    @Wire
    private Button btnApproveFile, btnApproveDispatch, btnPreviewDispatch;
    @Wire
    private Button btnApproveFile2, btnApproveDispatch2, btnPreviewDispatch2;
    private VFileRtfile vFileRtfile;
    private String sLegal, sMechanism, sCriteria, sNameProduct;
    private CosEvaluationRecord obj = new CosEvaluationRecord();
    @Wire
    private Div gbLegal,divParent,divChild;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");

        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("id", fileId);
        divChild =(Div) Executions.createComponents(
                "/Pages/module/cosmetic/cosmeticViewCV.zul", divParent, arguments);
    }

    @Listen("onClick=#btnSubmit")
    public void btnSubmit() {
        try {
            onApproveFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public boolean isValidate() {
        // tu choi
        if (Long.valueOf((String) lbStatus.getValue()).equals(Constants.EVALUTION.FILE_NOK)) {
            String message;
            if ("".equals(mainContent.getText().trim())) {
                message = "Lý do từ chối không được bỏ trống";
                mainContent.focus();
                setWarningMessage(message);
                return false;
            }
        } //Bo sung
        else if (Long.valueOf((String) lbStatus.getValue()).equals(Constants.EVALUTION.FILE_NEED_ADD)) {
            String message;
            if ("".equals(mainContent.getText().trim())) {
                message = "Lý do bổ sung không được bỏ trống";
                mainContent.focus();
                setWarningMessage(message);
                return false;
            }
        }

        if ("0".equals(lbLegal.getSelectedItem().getValue()) || "2".equals(lbLegal.getSelectedItem().getValue())) {
            String message;
            if ("".equals(legalContent.getText().trim())) {
                message = "Lý do không cấp hoặc yêu cầu bổ sung không được bỏ trống";
                legalContent.focus();
                setWarningMessage(message);
                return false;
            }
        }
        if ("0".equals(lbCriteria.getSelectedItem().getValue()) || "2".equals(lbCriteria.getSelectedItem().getValue())) {
            String message;
            if ("".equals(criteriaContent.getText().trim())) {
                message = "Lý do không cấp hoặc yêu cầu bổ sung không được bỏ trống";
                criteriaContent.focus();
                setWarningMessage(message);
                return false;
            }
        }
        if ("0".equals(lbMechanism.getSelectedItem().getValue()) || "2".equals(lbMechanism.getSelectedItem().getValue())) {
            String message;
            if ("".equals(mechanismContent.getText().trim())) {
                message = "Lý do không cấp hoặc yêu cầu bổ sung không được bỏ trống";
                mechanismContent.focus();
                setWarningMessage(message);
                return false;
            }
        }
        if ("0".equals(lbProductName.getSelectedItem().getValue()) || "2".equals(lbProductName.getSelectedItem().getValue())) {
            String message;
            if ("".equals(ProductNameContent.getText().trim())) {
                message = "Lý do không cấp hoặc yêu cầu bổ sung không được bỏ trống";
                ProductNameContent.focus();
                setWarningMessage(message);
                return false;
            }
        }
        return true;
    }

    public void onApproveFile() throws Exception {
        if (!isValidate()) {
            return;
        }
        CosEvaluationRecordDAO objDAOHE = new CosEvaluationRecordDAO();
        obj = createApproveObject();
        objDAOHE.saveOrUpdate(obj);
        txtValidate.setValue("1");
        //Tu choi
        if (Long.valueOf((String) lbStatus.getValue()).equals(Constants.EVALUTION.FILE_NOK)) {
            base.showNotify("Từ chối hồ sơ thành công!");
        } //Bo sung
        else if (Long.valueOf((String) lbStatus.getValue()).equals(Constants.EVALUTION.FILE_NEED_ADD)) {
            base.showNotify("Yêu cầu bổ sung hồ sơ thành công!");
        } else {
            base.showNotify("Thẩm định đạt hồ sơ thành công!");
        }

    }

    private CosEvaluationRecord createApproveObject() throws Exception {
        Date dateNow = new Date();
        obj.setCreateDate(dateNow);
        obj.setFileId(fileId);
        obj.setLegal(Long.valueOf((String) lbLegal.getSelectedItem().getValue()));

        obj.setLegalContent(textBoxGetValue(legalContent));
        obj.setStatus(Long.valueOf((String) lbStatus.getValue()));
        obj.setMainContent(textBoxGetValue(mainContent));
        obj.setCriteria(Long.valueOf((String) lbCriteria.getSelectedItem().getValue()));
        obj.setCriteriaContent(textBoxGetValue(criteriaContent));
        obj.setMechanism(Long.valueOf((String) lbMechanism.getSelectedItem().getValue()));
        obj.setMechanismContent(textBoxGetValue(mechanismContent));

        obj.setNameproduct(Long.valueOf((String) lbProductName.getSelectedItem().getValue()));
        obj.setNameproductContent(textBoxGetValue(ProductNameContent));
        obj.setIsActive(Constants.Status.ACTIVE);
        obj.setUserId(getUserId());
        obj.setUserName(getUserFullName());
        return obj;
    }

    @Listen("onChange=#legalContent")
    public void txtlegalContent() {
        SetTextMainContent();
    }

    @Listen("onChange=#ProductNameContent")
    public void txtProductNameContent() {
        SetTextMainContent();
    }

    @Listen("onChange=#criteriaContent")
    public void txtcriteriaContent() {
        SetTextMainContent();
    }

    @Listen("onChange=#mechanismContent")
    public void txtmechanismContent() {
        SetTextMainContent();
    }

    @Listen("onClick=#btnCheck")
    public void onClickbtnCheck() {
        Map<String, Object> agrs = new ConcurrentHashMap<String, Object>();
        agrs.put("parentWindow", windowEvaluation);
        agrs.put("fileId", fileId);
        createWindow("windowCheckComponent ", "/Pages/module/cosmetic/include/evaluation/evaluationCheckComponent.zul", agrs, Window.OVERLAPPED);
    }

    public void onChangelbLegal() {
        if ("0".equals(lbLegal.getSelectedItem().getValue()) || "2".equals(lbLegal.getSelectedItem().getValue())) {
            legalContent.setVisible(true);
            labelLegal.setVisible(true);
            labelLegalRequi.setVisible(true);
        } else {
            legalContent.setVisible(false);
            labelLegal.setVisible(false);
            labelLegalRequi.setVisible(false);
        }
    }

    public void onChangelbCriteria() {
        if ("0".equals(lbCriteria.getSelectedItem().getValue()) || "2".equals(lbCriteria.getSelectedItem().getValue())) {
            criteriaContent.setVisible(true);
            labelCriteria.setVisible(true);
            labelCriteriaRequi.setVisible(true);
        } else {
            labelCriteria.setVisible(false);
            criteriaContent.setVisible(false);
            labelCriteriaRequi.setVisible(false);
        }
    }

    public void onChangelbMechanism() {
        if ("0".equals(lbMechanism.getSelectedItem().getValue()) || "2".equals(lbMechanism.getSelectedItem().getValue())) {
            mechanismContent.setVisible(true);
            labelMechanism.setVisible(true);
            labelMechanismRequi.setVisible(true);
        } else {
            mechanismContent.setVisible(false);
            labelMechanism.setVisible(false);
            labelMechanismRequi.setVisible(false);
        }
    }

    public void onChangelbProductName() {
        if ("0".equals(lbProductName.getSelectedItem().getValue()) || "2".equals(lbProductName.getSelectedItem().getValue())) {
            ProductNameContent.setVisible(true);
            labelProductName.setVisible(true);
            labelProductNameRequi.setVisible(true);
        } else {
            ProductNameContent.setVisible(false);
            labelProductName.setVisible(false);
            labelProductNameRequi.setVisible(false);
        }
    }

    public void SetTextMainContent() {
        sLegal = "Pháp chế: " + legalContent.getValue() + ".\n";
        sNameProduct = "Tên sản phẩm: " + ProductNameContent.getValue() + ".\n";
        sCriteria = "Chỉ tiêu yêu cầu của Hiệp định mỹ phẩm ASEAN và các phụ lục: " + criteriaContent.getValue() + ".\n";
        sMechanism = "Mục đích sử dụng: " + mechanismContent.getValue();

        String sTxt = "";
        if (legalContent.getValue() != "" && legalContent.getValue() != null) {
            sTxt += sLegal;
        }
        if (ProductNameContent.getValue() != "" && ProductNameContent.getValue() != null) {
            sTxt += sNameProduct;
        }
        if (criteriaContent.getValue() != "" && criteriaContent.getValue() != null) {
            sTxt += sCriteria;
        }
        if (mechanismContent.getValue() != "" && mechanismContent.getValue() != null) {
            sTxt += sMechanism;
        }
        mainContent.setValue(sTxt);
    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    private void setWarningMessage(String message) {

        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);

    }
}
