package com.viettel.module.cosmetic.Controller.include;

import com.viettel.module.rapidtest.DAO.include.*;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.payment.BO.VRtPaymentInfo;
import com.viettel.module.payment.DAO.VRtPaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.module.payment.BO.PaymentInfo;

import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class PaymentAllController extends BusinessController {

    /**
     *
     */
    protected static final long serialVersionUID = 1L;
    @Wire
    protected Label lbTopWarning, lbBottomWarning;
    // Ý kiến lãnh đạo
    @Wire
    protected Window businessWindow;
    protected Window parentWindow;
    protected ListModel model;

    List<PaymentInfo> lstFeePaymentInfo;

    @Wire("#lboxFeePaymentInfo")
    protected Listbox lboxFeePaymentInfo;

    protected Long fileId;
    protected Long phase;
    protected Long fee;
    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */



    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {

        return super.doBeforeCompose(page, parent, compInfo);
    }


    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        fileId = (Long) arguments.get("fileId");
        phase = Long.valueOf((String) arguments.get("phase"));
        reload(fileId);
    }

    private void reload(Long fileId) {
        PaymentInfoDAO paymentInfoDAOHE = new PaymentInfoDAO();
        lstFeePaymentInfo = paymentInfoDAOHE.getListPayment(fileId, phase);
        lboxFeePaymentInfo.setModel(new ListModelList(lstFeePaymentInfo));
    }

    @Listen("onPayByMoney = #lboxFeePaymentInfo")
    public void onPayByMoney(Event event) {
        PaymentInfo paymentInfo = (PaymentInfo) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("paymentInfo", paymentInfo);
        arguments.put("parentWindow", businessWindow);
        arguments.put("feePaymentType", Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT);
//        Window window = createWindow("wdPaymentByMoney", "/Pages/rapidTest/include/paymentByMoneyAndBankCRUD.zul",null,arguments);
//        window.doModal();
        Window window = (Window) Executions.createComponents(
                "/Pages/rapidTest/include/paymentByMoneyAndBankCRUD.zul", null, arguments);
        window.doModal();
    }

    @Listen("onPayAll = #lboxFeePaymentInfo")
    public void onPayAll(Event event) {
         PaymentInfo paymentInfo = (PaymentInfo) event.getData();
            List<VRtPaymentInfo> listVRtPaymentInfo;
             VRtPaymentInfoDAO objDAOHE = new VRtPaymentInfoDAO();
        listVRtPaymentInfo = objDAOHE.getListRequestPaymentInfoId(getDeptId(),paymentInfo.getPaymentInfoId()); 
        //listVRtPaymentInfo = new ArrayList<VRtPaymentInfo>();
            //listVRtPaymentInfo.add(paymentInfo);
       Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("listVRtPaymentInfo", listVRtPaymentInfo);
        arguments.put("menuType", Constants.DOCUMENT_MENU.ALL);
        arguments.put("feePaymentType", Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_MOT_HO_SO);
        arguments.put("parentWindow", parentWindow);
        Window window = createWindow("ConfirmPaymentInfoAll", "/Pages/rapidTest/include/detailpaymentConfirm.zul",
                arguments, Window.MODAL);
        window.doModal();
        
    }
    
    
    @Listen("onPayByBank = #lboxFeePaymentInfo")
    public void onPayByBank(Event event) {
        PaymentInfo paymentInfo = (PaymentInfo) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("paymentInfo", paymentInfo);
        arguments.put("parentWindow", businessWindow);
        arguments.put("feePaymentType", Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN);
        Window window = createWindow("wdPaymentByMoney", "/Pages/rapidTest/include/paymentByMoneyAndBankCRUD.zul",
                arguments, Window.POPUP);
        window.doModal();
    }

    /**
     * Dong cua so khi o dang popup
     */
    @Listen("onClose = #businessWindow")
    public void onClose() {
        businessWindow.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onVisible = #businessWindow")
    public void onVisible() {
        reload(fileId);
        lbTopWarning.setValue("Thanh toán thành công");

    }

    public ListModel getModel() {
        return model;
    }

    public void setModel(ListModel model) {
        this.model = model;
    }
    
        @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

}
