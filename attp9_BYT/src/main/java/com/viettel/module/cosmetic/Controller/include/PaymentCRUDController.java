package com.viettel.module.cosmetic.Controller.include;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.utils.ResourceBundleUtil;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class PaymentCRUDController extends BaseComposer {

    @Wire
    private Vlayout flist;

    @Wire
    private Textbox paymentPerson, cost, billCode, paymentCode;

    @Wire
    private Datebox paymentDate;

    @Wire
    private Listbox lboxFeePaymentType;
    private Window parentWindow;

    @Wire
    private Window windowPaymentCRUD;

    @Wire
    Button btnAttach;
    List<Media> listMedia;
    private PaymentInfo paymentInfo;

    @SuppressWarnings("rawtypes")
    private Boolean isPaymentByBank;
    Long feePaymentType;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        paymentInfo = (PaymentInfo) arguments.get("paymentInfo");
        feePaymentType = (Long) arguments.get("feePaymentType");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        listMedia = new ArrayList();
        loadCombobox();
    }

    private void loadCombobox() {
        if (feePaymentType != null) {
            if (Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN.equals(feePaymentType)) {
                lboxFeePaymentType.setSelectedIndex(0);
            } else if (Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT.equals(feePaymentType)) {
                lboxFeePaymentType.setSelectedIndex(1);
            }

        }

    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        flist.getChildren().clear();
        // final Media media = event.getMedia();
        final Media media = event.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            	String sExt = ResourceBundleUtil.getString("extend_file", "config");
	                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
        }

        // luu file vao danh sach file
        listMedia.clear();
        listMedia.add(media);

        // layout hien thi ten file va nut "Xóa";
        final Hlayout hl = new Hlayout();
        hl.setSpacing("6px");
        hl.setClass("newFile");
        hl.appendChild(new Label(media.getName()));
//        A rm = new A("Xóa");
//        rm.addEventListener(Events.ON_CLICK,
//                new org.zkoss.zk.ui.event.EventListener() {
//                    @Override
//                    public void onEvent(Event event) throws Exception {
//                        hl.detach();
//                        // xoa file khoi danh sach file
//                        listMedia.remove(media);
//                    }
//                });
//        hl.appendChild(rm);
        // flist= new Vlayout();
        flist.appendChild(hl);
    }

    public Boolean isPaymentByBank() {
        return Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN.equals(feePaymentType);
    }

    @Listen("onClick = #btnCreate")
    public void onCreate() {
        paymentInfo = createObject();
        PaymentInfoDAO objDAOHE = new PaymentInfoDAO();
        objDAOHE.saveOrUpdate(paymentInfo);
        windowPaymentCRUD.onClose();
        Events.sendEvent("onVisible", parentWindow, null);

    }

    private PaymentInfo createObject() {
        paymentInfo.setPaymentActionUserId(getUserId());
        paymentInfo.setPaymentActionUser(getUserFullName());
        paymentInfo.setStatus(Constants.RAPID_TEST.PAYMENT.PAY_ALREADY);
        paymentInfo.setPaymentPerson(paymentPerson.getValue());
        paymentInfo.setPaymentDate(paymentDate.getValue());
        paymentInfo.setPaymentTypeId(Long.valueOf((String) lboxFeePaymentType.getSelectedItem().getValue()));
//        Long costL = null;
//        if (cost.getValue() != null) {
//            try {
//                costL = Long.valueOf(cost.getValue());
//            } catch (WrongValueException | NumberFormatException ex) {
//            }
//        }
//        paymentInfo.setCost(costL);
        if (billCode != null) {
            paymentInfo.setBillCode(billCode.getValue());
        }
        if (paymentCode != null) {
            paymentInfo.setPaymentCode(paymentCode.getValue());
        }

        return paymentInfo;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

}
