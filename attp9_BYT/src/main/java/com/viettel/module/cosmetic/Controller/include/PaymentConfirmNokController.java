package com.viettel.module.cosmetic.Controller.include;

import com.viettel.module.rapidtest.DAO.include.*;
import com.viettel.utils.LogUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.select.annotation.Listen;

/**
 *
 * @author Linhdx
 */
public class PaymentConfirmNokController extends PaymentConfirmController {

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tu choi Tiep nhan ho so:" + fileId);
            onReject();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

}
