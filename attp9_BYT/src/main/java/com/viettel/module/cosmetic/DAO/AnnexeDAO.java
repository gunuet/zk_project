/*
 * To change this template, choose Tools | CosCosfileIngres
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.Annexe;
import com.viettel.module.cosmetic.BO.CosCosfileIngre;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class AnnexeDAO extends GenericDAOHibernate<Annexe, Long> {

    public AnnexeDAO() {
        super(Annexe.class);
    }

    public void delete(Long id) {
        Annexe obj = findById(id);
        obj.setIsActive(-1L);
        update(obj);
    }

    @Override
    public void saveOrUpdate(Annexe cos) {
        if (cos != null) {
            super.saveOrUpdate(cos);
        }

        getSession().flush();

    }

    public List getAllActive() {
        List<CosCosfileIngre> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosCosfileIngre a ");
            stringBuilder.append("  where a.isActive = 1 "
                    + " order by cosfileIngreId) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findAllIdByCosFileId(Long cosFileId) {
        List<CosCosfileIngre> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" select a.cosfileIngreId from CosCosfileIngre a ");
            stringBuilder.append("  where a.isActive = 1  and a.cosFileId = ? "
                    + " order by cosfileIngreId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, cosFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public PagingListModel findBySubstance(String substance, int take, int start) {
        List lst = null;
        PagingListModel model;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from Annexe a ");
            stringBuilder.append("  where lower(a.substance) LIKE  ? ");

            StringBuilder stringBuilderCount = new StringBuilder(" SELECT COUNT(a) from Annexe a ");
            stringBuilderCount.append("  where lower(a.substance) LIKE  ? ");

            Query query = getSession().createQuery(stringBuilder.toString());
            Query queryCount = getSession().createQuery(stringBuilderCount.toString());
            query.setParameter(0, StringUtils.toLikeString(substance));
            queryCount.setParameter(0, StringUtils.toLikeString(substance));
            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }
            lst = query.list();
            Long count = (Long) queryCount.uniqueResult();
            model = new PagingListModel(lst, count);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new PagingListModel(lst, Long.MIN_VALUE);
        }

        return model;
    }

    public PagingListModel search(Annexe searchForm, int start, int take) {
        List listParam = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder("select a from Annexe a where a.isActive <> -1 ");
            StringBuilder strCountBuf = new StringBuilder("select count(a) from Annexe a where a.isActive <> -1 ");
            StringBuilder hql = new StringBuilder();
            if (searchForm != null) {
                if (searchForm.getAnnexeTypeName() != null && !"".equals(searchForm.getAnnexeTypeName().trim())) {
                    hql.append(" and lower(a.annexeTypeName) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getAnnexeTypeName()));
                }
                if (searchForm.getAnnexeType() != null) {
                    hql.append(" and a.annexeType=? ");
                    listParam.add(searchForm.getAnnexeType());
                }
                if (searchForm.getIsActive() != null) {
                    hql.append(" and a.isActive=? ");
                    listParam.add(searchForm.getIsActive());
                }
                if (searchForm.getCasNumber() != null && !"".equals(searchForm.getCasNumber().trim())) {
                    hql.append(" and lower(a.casNumber) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getCasNumber()));
                }
                if (searchForm.getConditionsOfUse() != null && !"".equals(searchForm.getConditionsOfUse().trim())) {
                    hql.append(" and lower(a.conditionsOfUse) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getConditionsOfUse()));
                }
                if (searchForm.getFieldUse() != null && !"".equals(searchForm.getFieldUse().trim())) {
                    hql.append(" and lower(a.fieldUse) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getFieldUse()));
                }
                if (searchForm.getLimitationRequirements() != null && !"".equals(searchForm.getLimitationRequirements().trim())) {
                    hql.append(" and lower(a.limitationRequirements) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getLimitationRequirements()));
                }
                if (searchForm.getMaximumAuthorized() != null && !"".equals(searchForm.getMaximumAuthorized().trim())) {
                    hql.append(" and lower(a.maximumAuthorized) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getMaximumAuthorized()));
                }
                if (searchForm.getNote() != null && !"".equals(searchForm.getNote().trim())) {
                    hql.append(" and lower(a.note) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getNote()));
                }
                if (searchForm.getRefNo() != null && !"".equals(searchForm.getRefNo().trim())) {
                    hql.append(" and lower(a.refNo) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getRefNo()));
                }
                if (searchForm.getSubstance() != null && !"".equals(searchForm.getSubstance().trim())) {
                    hql.append(" and lower(a.substance) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getSubstance()));
                }
                if (searchForm.getAllowedUntil() != null) {
                    hql.append(" and a.allowedUntil=? ");
                    listParam.add(searchForm.getAllowedUntil());
                }
            }
            hql.append(" ORDER BY a.annexeTypeName ");
            strBuf.append(hql);
            strCountBuf.append(hql);

            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public boolean hasDuplicate(Annexe rs) {
        StringBuilder hql = new StringBuilder("select count(r) from Annexe r where r.isActive <> -1 ");
        List lstParams = new ArrayList();
        if (rs.getAnnexeId() != null && rs.getAnnexeId() > 0L) {
            hql.append(" and r.annexeId <> ?");
            lstParams.add(rs.getAnnexeId());
        } else {
            return false;
        }
        Query query = session.createQuery(hql.toString());
        for (int i = 0; i < lstParams.size(); i++) {
            query.setParameter(i, lstParams.get(i));
        }
        Long count = (Long) query.uniqueResult();
        return count > 1L;
    }
}
