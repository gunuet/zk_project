/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Query;
import org.zkoss.util.media.Media;

/**
 *
 * @author giangnh20
 */
public class CaUserDAO extends GenericDAOHibernate<CaUser, Long> {

    public CaUserDAO() {
        super(CaUser.class);
    }

    @Override
    public void delete(CaUser o) {
        if (o == null) {
            return;
        }
        if (o.getIsActive() == 0L) {
            super.delete(o);
        } else {
            o.setIsActive(0L);
            update(o);
        }
        getSession().flush();
    }

    @Override
    public CaUser findById(Long id) {
        Query query = getSession().getNamedQuery("CaUser.findByCaId");
        query.setParameter("caId", id);
        List result = query.list();
        return result.isEmpty() ? null : (CaUser) result.get(0);
    }

    public List<CaUser> findCaBySerial(String serial, Long isActive, Long userId) {
        List<CaUser> lst;
        List params = new ArrayList();
        StringBuilder sb = new StringBuilder(" from CaUser c WHERE 1=1 ");
        if (serial != null && !"".equals(serial)) {
            sb.append(" and c.caSerial like ? ESCAPE '/'");
            params.add(StringUtils.toLikeString(serial));
        }
        sb.append(" and c.isActive = ?");
        params.add(isActive);
        if (userId != null) {
            sb.append(" and c.userId = ?");
            params.add(userId);
        }
        Query query = getSession().createQuery(sb.toString());
        for (int i = 0; i < params.size(); i++) {
            query.setParameter(i, params.get(i));
        }
        lst = query.list();
        return lst;
    }

    public List<CaUser> findCaByUserId(Long isActive, Long userId) {
        List<CaUser> lst;
        List params = new ArrayList();
        StringBuilder sb = new StringBuilder(" from CaUser c WHERE 1=1 ");
        sb.append(" and c.isActive = ?");
        params.add(isActive);
        if (userId != null) {
            sb.append(" and c.userId = ?");
            params.add(userId);
        }
        Query query = getSession().createQuery(sb.toString());
        for (int i = 0; i < params.size(); i++) {
            query.setParameter(i, params.get(i));
        }
        lst = query.list();
        return lst;
    }

//    @Override
//    public void saveOrUpdate(CaUser ca) {
//        if (ca != null) {
//            super.saveOrUpdate(ca);
//            getSession().getTransaction().commit();
//        }
//    }
    /**
     * Ham kiem tra xem ca da co trong database & dang active hay khong Table
     * CaUser can dat index(caSerial, isActive) de tang toc do truy van
     *
     * @author giangnh20
     * @param serial
     * @return
     */
    public boolean isExistCa(String serial) {
        if (serial != null && !"".equals(serial)) {
            Query query = getSession().createQuery(" from CaUser c WHERE c.caSerial = :caSerial AND c.isActive = 1");
            query.setParameter("caSerial", serial);
            List<CaUser> result = query.list();
            return result.size() > 0;
        }
        return false;
    }

    public String saveAttach(Media media, Long caId) throws IOException {
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }
        String relationPath = "CA" + separator + String.format("%d%s%d%s%d", Calendar.getInstance().get(Calendar.YEAR), separator, Calendar.getInstance().get(Calendar.MONTH) + 1, separator, Calendar.getInstance().get(Calendar.DATE)) + separator + String.valueOf(caId);
        InputStream inputStream;
        OutputStream outputStream;
        try {
            File f = new File(folderPath + separator + relationPath);
            if (!f.exists()) {
                f.mkdirs();
            }
            relationPath += separator + DigestUtils.md5Hex(media.getName()) + String.format(".%s", media.getFormat());
            String filePath = folderPath + separator + relationPath;
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(filePath);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }

                inputStream.close();


                outputStream.close();

            return relationPath;
        } catch (FileNotFoundException ex) {
            LogUtils.addLogDB(ex);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        } finally {

        }
        return "";
    }

}
