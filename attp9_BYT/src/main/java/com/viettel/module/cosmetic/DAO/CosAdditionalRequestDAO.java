/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosAdditionalRequest;
import com.viettel.utils.Constants;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class CosAdditionalRequestDAO extends
        GenericDAOHibernate<CosAdditionalRequest, Long> {

    public CosAdditionalRequestDAO() {
        super(CosAdditionalRequest.class);
    }

    @Override
    public void saveOrUpdate(CosAdditionalRequest obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }

        getSession().flush();
    }

    @Override
    public CosAdditionalRequest findById(Long id) {
        Query query = getSession().getNamedQuery(
                "CosAdditionalRequest.findByAdditionalRequestId");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CosAdditionalRequest) result.get(0);
        }
    }

    @Override
    public void delete(CosAdditionalRequest obj) {
//        obj.setIsActive(Constants.Status.INACTIVE);
//        getSession().saveOrUpdate(obj);
    }
    
    public List<CosAdditionalRequest> findAllActiveByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from CosAdditionalRequest a "
                + "where a.fileId = :fileId "
                + "and a.isActive = :isActive "
                + "order by additionalRequestId desc ");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        return result;
    }

}
