/*
 * To change this template, choose Tools | CosAssemblers
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosAssembler;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class CosAssemblerDAO extends GenericDAOHibernate<CosAssembler, Long> {

    public CosAssemblerDAO() {
        super(CosAssembler.class);
    }

    public void delete(Long id) {
        CosAssembler obj = findById(id);
        obj.setIsActive(0l); 
        update(obj);
    }
    
     @Override
    public void saveOrUpdate(CosAssembler cos) {
        if (cos != null) {
            super.saveOrUpdate(cos);
        }

        getSession().flush();

    }

    public List getAllActive() {
        List<CosAssembler> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosAssembler a ");
            stringBuilder.append("  where a.isActive = 1 "
                    + " order by assemblerId) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }
    
    public List findAllIdByCosFileId(Long cosFileId) {
        List<CosAssembler> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" select a.assemblerId from CosAssembler a ");
            stringBuilder.append("  where a.isActive = 1  and a.cosFileId = ? "
                    + " order by assemblerId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, cosFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

   public List findByCosFileId(Long cosFileId) {
        List<CosAssembler> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosAssembler a ");
            stringBuilder.append("  where a.isActive = 1  and a.cosFileId = ? "
                    + " order by assemblerId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, cosFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

}
