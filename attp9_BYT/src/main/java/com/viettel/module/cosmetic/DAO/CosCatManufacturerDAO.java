/*
 * To change this template, choose Tools | CosManufacturers
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosCatManufacturer;
import com.viettel.module.cosmetic.BO.CosManufacturer;
import com.viettel.utils.Constants;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class CosCatManufacturerDAO extends GenericDAOHibernate<CosCatManufacturer, Long> {

    public CosCatManufacturerDAO() {
        super(CosCatManufacturer.class);
    }

    @Override
    public void saveOrUpdate(CosCatManufacturer cos) {
        if (cos != null) {
            super.saveOrUpdate(cos);
        }
        getSession().flush();
    }
}
