/*
 * To change this template, choose Tools | CosCosfileIngres
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosCosfileIngre;
import com.viettel.module.cosmetic.BO.CosCosfileIngre;
import com.viettel.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class CosCosfileIngreDAO extends GenericDAOHibernate<CosCosfileIngre, Long> {

    public CosCosfileIngreDAO() {
        super(CosCosfileIngre.class);
    }

    public void delete(Long id) {
        CosCosfileIngre obj = findById(id);
        obj.setIsActive(0l);
        update(obj);
    }

    @Override
    public void saveOrUpdate(CosCosfileIngre cos) {
        if (cos != null) {
            super.saveOrUpdate(cos);
        }

        getSession().flush();

    }

    public List getAllActive() {
        List<CosCosfileIngre> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosCosfileIngre a ");
            stringBuilder.append("  where a.isActive = 1 "
                    + " order by cosfileIngreId) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findAllIdByCosFileId(Long cosFileId) {
        List<CosCosfileIngre> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" select a.cosfileIngreId from CosCosfileIngre a ");
            stringBuilder.append("  where a.isActive = 1  and a.cosFileId = ? "
                    + " order by cosfileIngreId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, cosFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findByCosFileId(Long cosFileId) {
        List<CosCosfileIngre> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosCosfileIngre a ");
            stringBuilder.append("  where a.isActive = 1  and a.cosFileId = ? "
                    + " order by cosfileIngreId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, cosFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findListIngreAnnexe(Long cosFileId) {
        List<CosCosfileIngre> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" SELECT DISTINCT(a.cosfileIngreId) from CosCosfileIngre a,Annexe b");
            stringBuilder.append("  where a.isActive = ?  and a.cosFileId = ?  AND lower(b.substance) like '%' || lower(TRIM(a.ingredientName)) ||'%'"
                    + " order by a.cosfileIngreId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, Constants.Status.ACTIVE);
            query.setParameter(1, cosFileId);
//            query.setParameter(2, Constants.Status.ACTIVE);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

}
