/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.utils.Constants;
import java.util.ArrayList;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class CosEvaluationRecordDAO extends
        GenericDAOHibernate<CosEvaluationRecord, Long> {

    public CosEvaluationRecordDAO() {
        super(CosEvaluationRecord.class);
    }

    @Override
    public void saveOrUpdate(CosEvaluationRecord obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }

        getSession().flush();
    }

    @Override
    public CosEvaluationRecord findById(Long id) {
        Query query = getSession().getNamedQuery(
                "CosEvaluationRecord.findByEvaluationRecordId");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CosEvaluationRecord) result.get(0);
        }
    }

    @Override
    public void delete(CosEvaluationRecord obj) {
        obj.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(obj);
    }
    
    public CosEvaluationRecord getLastEvaluation(Long fileId){
         Query query = getSession().createQuery("select a from CosEvaluationRecord a where a.fileId = :fileId "
                 + "order by a.evaluationRecordId desc");
         query.setParameter("fileId", fileId);
         List<CosEvaluationRecord> lst = query.list();
         if(lst.size()>0){
             return lst.get(0);
         }else{
             return null;
         }
    }
    
    public List<CosEvaluationRecord> getAllEvaluation(Long fileId){
         Query query = getSession().createQuery("select a from CosEvaluationRecord a where a.fileId = :fileId "
                 + "order by a.evaluationRecordId desc");
         query.setParameter("fileId", fileId);
         List<CosEvaluationRecord> lst = query.list();
         return lst;
//         if(lst.size()>0){
//             return lst;
//         }else{
//             return new ArrayList<>();
//         }
    }
    
    public int checkEvaluation(Long fileId){
        CosEvaluationRecord obj = getLastEvaluation(fileId);
        if(obj==null){
           return Constants.CHECK_VIEW.NOT_VIEW;
        }else{
            return Constants.CHECK_VIEW.VIEW;
        }
    }

}
