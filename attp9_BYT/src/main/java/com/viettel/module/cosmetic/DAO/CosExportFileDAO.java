package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.VRtPaymentInfo;
import com.viettel.module.payment.DAO.VRtPaymentInfoDAO;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.module.cosmetic.Model.CosExportModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.WordExportUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;

/**
 *
 * @author ChucHV
 */
public class CosExportFileDAO extends BaseComposer {

    private Long fileId;
    private String content;
    private String bieumaubancongbo = "/WEB-INF/template/bancongbomypham.doc";//xuat giay cong bo cho doanh nghiep 03
    private String bieumauthongbaosuadoibosung = "/WEB-INF/template/bieumauthongbaosuadoibosung.docx";//xuat giay cong bo cho doanh nghiep 03
    private String bieumaubienlaithutienphilephi = "/WEB-INF/template/thutienphilephi.docx";//bien lai thu tien le phi
    private String bieumaugiayphep = "/WEB-INF/template/cos_permit_sign.docx";//xuat giay cong bo cho doanh nghiep 03

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    private WordprocessingMLPackage prepareDataToExportCvSdbs(CosExportModel model) {
        try {
            String sendNo = "0";
            if (model.getSendNo() != null) {
                sendNo = model.getSendNo();
            }
            String businessName = model.getBusinessName();
            Date signedDate = model.getSignDate();
            String signedDateStr = "Hà Nội, ngày " + " tháng " + " năm ";
            if (signedDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(signedDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                signedDateStr = "Hà Nội, ngày " + days + " tháng " + months + " năm " + years;
            }

            String signer = model.getSigner();
            String rolesigner = model.getRolesigner();
            String leaderSinged = model.getLeaderSinged();

            content = model.getContent();
            WordExportUtils wU = new WordExportUtils();

            if (model.getPathTemplate() != null) {
                bieumauthongbaosuadoibosung = model.getPathTemplate();
            }

            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(new FileInputStream(new File(bieumauthongbaosuadoibosung)));

            wU.replacePlaceholder(wmp, "BỘ Y TẾ", "${deptParent}");
            wU.replacePlaceholder(wmp, "CỤC QUẢN LÝ DƯỢC", "${receiptDeptName}");
            wU.replacePlaceholder(wmp, "Cục Quản lý dược ", "${receiptDeptNames}");
            wU.replacePlaceholder(wmp, sendNo, "${sendNo}");
            wU.replacePlaceholder(wmp, signedDateStr, "${signDateStr}");
            wU.replacePlaceholder(wmp, businessName, "${businessName}");
            wU.replacePlaceholder(wmp, signer, "${signer}");
            wU.replacePlaceholder(wmp, rolesigner, "${roleSigner}");
            wU.replacePlaceholder(wmp, leaderSinged, "${LeaderSigned}");

            wU.replacePlaceholder(wmp, content, "${contentDispatch}");
            ConcurrentHashMap map = new ConcurrentHashMap();


            wU.replacePlaceholder(wmp, map);
            return wmp;
        } catch (Docx4JException | IOException ex) {
            LogUtils.addLogDB(ex);
        }
        return null;

    }

    private WordprocessingMLPackage prepareDataToWord_BLTPLP(Bill mBill) {
        try {


            String businessName = mBill.getConfirmUserName();
            Date signedDate = mBill.getCreateDate();
            String signedDateStr = "Hà Nội, ngày " + " tháng " + " năm ";
            if (signedDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(signedDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                signedDateStr = "Hà Nội, ngày " + days + " tháng " + months + " năm " + years;
            }

            String signer = mBill.getConfirmUserName();
            String rolesigner = mBill.getConfirmUserName();
            String leaderSinged = mBill.getConfirmUserName();
            WordExportUtils wU = new WordExportUtils();

//            if (model.getPathTemplate() != null) {
//                bieumauthongbaosuadoibosung = model.getPathTemplate();
//            }

            //WordprocessingMLPackage wmp = WordprocessingMLPackage.load(new FileInputStream(new File(bieumaubienlaithutienphilephi)));
            String pathTemplate = getPathTemplate(mBill.getBillId());
            WordprocessingMLPackage wmp = null;
            if (pathTemplate != null) {
                File mTempFile = new File(pathTemplate);
                wmp = WordprocessingMLPackage.load(mTempFile);
                wU.replacePlaceholder(wmp, signedDateStr, "${businessName}");
                wU.replacePlaceholder(wmp, businessName, "${businessAddress}");
                wU.replacePlaceholder(wmp, signer, "${noMonney}");
                wU.replacePlaceholder(wmp, rolesigner, "${textMonney}");
                wU.replacePlaceholder(wmp, leaderSinged, "${typePayment}");
                ConcurrentHashMap map = new ConcurrentHashMap();
                wU.replacePlaceholder(wmp, map);
            }
            return wmp;
        } catch (Docx4JException ex) {
            LogUtils.addLogDB(ex);
        }
        return null;

    }

//    public void ExportPDF(ExportModel model) throws Docx4JException {
//        WordprocessingMLPackage temp = prepareDataToExportCvSdbs(model);
//        // 2) Prepare Pdf settings
//        try {
//            PdfSettings pdfSettings = new PdfSettings();
//
//            // 3) Convert WordprocessingMLPackage to Pdf
//            OutputStream out;
//
//            out = new FileOutputStream(new File(
//                    "E:/HelloWorld.pdf"));
//            PdfConversion converter = new org.docx4j.convert.out.pdf.viaXSLFO.Conversion(
//                    temp);
//            converter.output(out, pdfSettings);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ExportFileDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    private WordprocessingMLPackage prepareDataToExportPermit(CosExportModel model) {
        try {
            String sendNo = "0";
            if (model.getSendNo() != null) {
                sendNo = model.getSendNo();
            }
            Date signedDate = model.getSignDate();
            String signedDateStr = "Hà Nội, ngày " + " tháng " + " năm ";
            if (signedDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(signedDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                signedDateStr = "Hà Nội, ngày " + days + " tháng " + months + " năm " + years;
            }

            String signer = model.getSigner();
            String rolesigner = model.getRolesigner();
            String leaderSinged = model.getLeaderSinged();

            String rapidTestName = model.getRapidTestName();
            String placeOfManufacture = model.getPlaceOfManufacture();
            String businessName = model.getBusinessName();
            String businessAddress = model.getBusinessAddress();

            WordExportUtils wU = new WordExportUtils();

            if (model.getPathTemplate() != null) {
                bieumaugiayphep = model.getPathTemplate();
            }

            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(
                    new FileInputStream(new File(bieumaugiayphep)));

            wU.replacePlaceholder(wmp, "BỘ Y TẾ", "${deptParent}");

            wU.replacePlaceholder(wmp, "CỤC QUẢN LÝ DƯỢC", "${receiptDeptName}");
            wU.replacePlaceholder(wmp, "Cục Quản lý dược ", "${receiptDeptNames}");

            wU.replacePlaceholder(wmp, sendNo, "${sendNo}");

            wU.replacePlaceholder(wmp, signedDateStr, "${signDateStr}");
            wU.replacePlaceholder(wmp, businessName, "${businessName}");
            wU.replacePlaceholder(wmp, businessAddress, "${businessAddress}");
            wU.replacePlaceholder(wmp, signer, "${signer}");
            wU.replacePlaceholder(wmp, rolesigner, "${roleSigner}");
            wU.replacePlaceholder(wmp, leaderSinged, "${LeaderSigned}");

            wU.replacePlaceholder(wmp, placeOfManufacture, "${placeOfManufacture}");
            wU.replacePlaceholder(wmp, rapidTestName, "${rapidTestName}");

            ConcurrentHashMap map = new ConcurrentHashMap();

            wU.replacePlaceholder(wmp, map);

            return wmp;
        } catch (Docx4JException | IOException ex) {
            LogUtils.addLogDB(ex);

        }
        return null;

    }

    private WordprocessingMLPackage prepareDataToExportPermit_Cos(CosExportModel model) {
        try {
            String sendNo = "0";
            if (model.getSendNo() != null) {
                sendNo = model.getSendNo();
            }
            Date signedDate = model.getSignDate();
            String signedDateStr = "Hà Nội, ngày " + " tháng " + " năm ";
            if (signedDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(signedDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                signedDateStr = "Hà Nội, ngày " + days + " tháng " + months + " năm " + years;
            }

            String signer = model.getSigner();
            String rolesigner = model.getRolesigner();
            String leaderSinged = model.getLeaderSinged();

            String rapidTestName = model.getRapidTestName();
            String placeOfManufacture = model.getPlaceOfManufacture();
            String businessName = model.getBusinessName();
            String businessAddress = model.getBusinessAddress();

            WordExportUtils wU = new WordExportUtils();

            if (model.getPathTemplate() != null) {
                // bieumaugiayphep = model.getPathTemplate();
            }
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            bieumaugiayphep = request.getRealPath(bieumaugiayphep);
            FileInputStream file1 = new FileInputStream(new File(bieumaugiayphep));
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(file1);

            wU.replacePlaceholder(wmp, "BỘ Y TẾ", "${deptParent}");

            wU.replacePlaceholder(wmp, "CỤC QUẢN LÝ DƯỢC", "${receiptDeptName}");
            wU.replacePlaceholder(wmp, "Cục Quản lý dược ", "${receiptDeptNames}");

            wU.replacePlaceholder(wmp, sendNo, "${sendNo}");

            wU.replacePlaceholder(wmp, signedDateStr, "${signDateStr}");
            wU.replacePlaceholder(wmp, businessName, "${businessName}");
            wU.replacePlaceholder(wmp, businessAddress, "${businessAddress}");
            wU.replacePlaceholder(wmp, signer, "${signer}");
            wU.replacePlaceholder(wmp, rolesigner, "${roleSigner}");
            wU.replacePlaceholder(wmp, leaderSinged, "${LeaderSigned}");

            wU.replacePlaceholder(wmp, placeOfManufacture, "${placeOfManufacture}");
            wU.replacePlaceholder(wmp, rapidTestName, "${rapidTestName}");

            ConcurrentHashMap map = new ConcurrentHashMap();

            wU.replacePlaceholder(wmp, map);

            return wmp;
        } catch (Docx4JException | IOException ex) {
            LogUtils.addLogDB(ex);

        }
        return null;

    }

    public void exportTempCvSdbs(CosExportModel model) throws IOException {
//        String fileName = "CongvanSDBS_" + (new Date()).getTime() + ".docx";
          prepareDataToExportCvSdbs(model);
//        BaseGenericForwardComposer base = new BaseGenericForwardComposer();
        //base.downloadTemFile(wmp,fileName); 
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public Boolean exportDataCvSdbs(CosExportModel model) {
        try {

            int typeExport = model.getTypeExport();
            String fileName = "CongvanSDBS_" + (new Date()).getTime() + ".docx";
            Long objectId = model.getObjectId();
            Long objectType = model.getObjectType();
            WordprocessingMLPackage wmp = prepareDataToExportCvSdbs(model);
            switch (typeExport) {
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP:
                    AttachDAO base = new AttachDAO();
                    List<Attachs> lstAttach = getListSDBS(objectId, objectType);
                    base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    if (lstAttach.isEmpty()) {
                        base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    } else {
                        base.downloadFileAttach(lstAttach.get(0));
                    }

                    return true;
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_SIGN:
                    break;
            }
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }
        return false;
    }

    private List<Attachs> getListSDBS(Long objectId, Long objectType) {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        List<Attachs> lst = attachDAOHE.getAllByObjectIdAndType(objectId, objectType);
        if (lst != null && lst.size() > 0) {
            return lst;
        }
        return new ArrayList();
    }

    public Boolean exportPermit(CosExportModel model) {
        try {

            int typeExport = model.getTypeExport();
            String fileName = "Giayphep_" + (new Date()).getTime() + ".docx";
            Long objectId = model.getObjectId();
            Long objectType = model.getObjectType();
            WordprocessingMLPackage wmp = prepareDataToExportPermit(model);
            switch (typeExport) {
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP:
                    AttachDAO base = new AttachDAO();
                    List<Attachs> lstAttach = getListAttach(objectId, objectType);
                    //test
                    base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    if (lstAttach.isEmpty()) {
                        base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    } else {
                        base.downloadFileAttach(lstAttach.get(0));
                    }
                    return true;
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_SIGN:
                    break;
            }
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }
        return false;
    }

    public Boolean exportBillPDF(Bill mBill) {
        try {


            String fileName = "thu_tien_phi_le_phi" + (new Date()).getTime() + ".docx";
            Long objectId = mBill.getBillId();
            WordprocessingMLPackage wmp = prepareDataToWord_BLTPLP(mBill);
            AttachDAO base = new AttachDAO();
            List<Attachs> lstAttach = getListAttach(objectId, Constants.OBJECT_TYPE.PAYMENT_EXPORT_PDF);
            //test
            base.saveFileAttach(wmp, fileName, objectId, Constants.OBJECT_TYPE.PAYMENT_EXPORT_PDF, null);
            if (lstAttach.isEmpty()) {
                base.saveFileAttach(wmp, fileName, objectId, Constants.OBJECT_TYPE.PAYMENT_EXPORT_PDF, null);
            } else {
                base.downloadFileAttach(lstAttach.get(0));
            }
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }
        return false;
    }

    private String getPathTemplate(Long billId) {
        List<VRtPaymentInfo> listFile;
        VRtPaymentInfoDAO objDAOHE = new VRtPaymentInfoDAO();
        listFile = objDAOHE.getListRequestPaymentBillId(getDeptId(), billId);
        if (listFile.size() > 0) {
//            Long fileId = listFile.get(0).getFileId();
//            //Get Template

            TemplateDAOHE the = new TemplateDAOHE();
            String pathTemplate = the.findPathTemplate(Constants.PROCEDURE_TEMPLATE_TYPE.PHILEPHI);
            return pathTemplate;
        }
        return null;
    }

    public Boolean exportPermit_Cos(CosExportModel model) {
        try {

            int typeExport;
            typeExport = 1;
            String fileName = "PhieuCongBoSanPhamMyPham_" + (new Date()).getTime() + ".docx";
            Long objectId = model.getObjectId();
            Long objectType = model.getObjectType();
            WordprocessingMLPackage wmp = prepareDataToExportPermit_Cos(model);
            switch (typeExport) {
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP:
                    AttachDAO base = new AttachDAO();
//                    List<Attachs> lstAttach = getListAttach(objectId, objectType);
                    //test
//                    if (lstAttach.isEmpty()) {
//                        base.saveFileAttach(wmp, fileName, objectId, objectType, null);
//                    } else {
//                        base.downloadFileAttach(lstAttach.get(0));
//                    }
                    base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    return true;
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_SIGN:
                    break;
            }
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }
        return false;
    }
    
    
      public Boolean exportReject_Cos(CosExportModel model) {
        try {

            int typeExport;
            typeExport = 1;
            String fileName = "PhieuCongBoSanPhamMyPham_" + (new Date()).getTime() + ".docx";
            Long objectId = model.getObjectId();
            Long objectType = model.getObjectType();
            WordprocessingMLPackage wmp = prepareDataToExportPermit_Cos(model);
            switch (typeExport) {
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP:
                    AttachDAO base = new AttachDAO();
//                    List<Attachs> lstAttach = getListAttach(objectId, objectType);
                    //test
//                    if (lstAttach.isEmpty()) {
//                        base.saveFileAttach(wmp, fileName, objectId, objectType, null);
//                    } else {
//                        base.downloadFileAttach(lstAttach.get(0));
//                    }
                    base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    return true;
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_SIGN:
                    break;
            }
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }
        return false;
    }

    private List<Attachs> getListAttach(Long objectId, Long objectType) {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        List<Attachs> lst = attachDAOHE.getAllByObjectIdAndType(objectId, objectType);
        if (lst != null && lst.size() > 0) {
            return lst;
        }
        return new ArrayList();
    }

    public void exportCosmeticAnnouncement(FilesModel fileModel) {
        try {
            WordExportUtils wU = new WordExportUtils();

            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            String path = request.getRealPath(bieumaubancongbo);
            FileInputStream fileTemplate = new FileInputStream(new File(path));
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(fileTemplate);
            ConcurrentHashMap map = new ConcurrentHashMap();
            map.put("createForm", fileModel);
            /*
            CosManufacturer manufacturer;
            if(fileModel.getLstManufacturer() != null && !fileModel.getLstManufacturer().isEmpty()){
                manufacturer = (CosManufacturer) fileModel.getLstManufacturer().get(0);
            } else {
                manufacturer = new CosManufacturer();
            }
            wU.replacePlaceholder(wmp, true, "");
            wU.replacePlaceholder(wmp, manufacturer.getName(), "${manufacturerName}");
            wU.replacePlaceholder(wmp, manufacturer.getAddress(), "${manufacturerAddress}");
            wU.replacePlaceholder(wmp, manufacturer.getPhone(), "${manufacturerPhone}");
            wU.replacePlaceholder(wmp, manufacturer.getFax(), "${manufacturerFax}");
            
            CosAssembler assembler;
            if(fileModel.getLstAssembler()!= null && !fileModel.getLstAssembler().isEmpty()){
                assembler = (CosAssembler) fileModel.getLstAssembler().get(0);
            } else {
                assembler = new CosAssembler();
            }
            wU.replacePlaceholder(wmp, true, "");
            wU.replacePlaceholder(wmp, assembler.getName(), "${assemblerName}");
            wU.replacePlaceholder(wmp, assembler.getAddress(), "${assemblerMain}");
            wU.replacePlaceholder(wmp, assembler.getPhone(), "${assemblerSub}");
            wU.replacePlaceholder(wmp, assembler.getFax(), "${manufacturerFax}");
            */
            wU.replacePlaceholder(wmp, map);
            wU.replaceTable(wmp, 0, fileModel.getLstProductTypes());
            wU.replaceTable(wmp, 1, fileModel.getLstPresentations());
            wU.replaceTable(wmp, 2, fileModel.getLstManufacturer());
            wU.replaceTable(wmp, 3, fileModel.getLstAssembler());
            wU.replaceTable(wmp, 7, fileModel.getLstIngredient());
            wU.writePDFToStream(wmp, true);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }
}
