/*
 * To change this template, choose Tools | CosManufacturers
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosCatManufacturer;
import com.viettel.module.cosmetic.BO.CosManufacturer;
import com.viettel.utils.Constants;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class CosManufacturerDAO extends GenericDAOHibernate<CosManufacturer, Long> {

    public CosManufacturerDAO() {
        super(CosManufacturer.class);
    }

    public void delete(Long id) {
        CosManufacturer obj = findById(id);
        obj.setIsActive(0l);
        update(obj);
    }

    @Override
    public void saveOrUpdate(CosManufacturer cos) {
        if (cos != null) {
            super.saveOrUpdate(cos);
        }

        getSession().flush();

    }

//    public void delete(List<Long> id) {
//        try {
//            StringBuilder stringBuilder = new StringBuilder(" update CosManufacturer a ");
//            stringBuilder.append("  set a.isActive = 0 "
//                    + " where a.manufacturerId  ");
//            Query query = getSession().createQuery(stringBuilder.toString());
//            lst = query.list();
//        } catch (Exception ex) {
//            String msg = ex.getMessage();
//            LogUtils.addLogDB(msg);
//            return new ArrayList<>();
//        }
//    }
    public List getAllActive() {
        List<CosManufacturer> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosManufacturer a ");
            stringBuilder.append("  where a.isActive = 1 "
                    + " order by manufacturerId ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findAllIdByCosFileId(Long cosFileId) {
        List<CosManufacturer> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" select a.manufacturerId from CosManufacturer a ");
            stringBuilder.append("  where a.isActive = 1  and a.cosFileId = ? "
                    + " order by manufacturerId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, cosFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findByCosFileId(Long cosFileId) {
        List<CosManufacturer> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosManufacturer a ");
            stringBuilder.append("  where a.isActive = 1  and a.cosFileId = ? "
                    + " order by manufacturerId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, cosFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findAllManufacturerCat(CosCatManufacturer obj) {
        List lst = null;
        List listParam = new ArrayList();
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosCatManufacturer a ");
            stringBuilder.append("  where a.isActive = ? ");
            listParam.add(Constants.Status.ACTIVE);
            if (obj != null) {
                if (obj.getName() != null && !"".equals(obj.getName().trim())) {
                    stringBuilder.append(" and lower(a.name) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(obj.getName()));
                }
                if (obj.getAddress() != null && !"".equals(obj.getAddress().trim())) {
                    stringBuilder.append(" and lower(a.address) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(obj.getAddress()));
                }
            }
            stringBuilder.append(" order by name asc ");
            Query query = getSession().createQuery(stringBuilder.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
            }
            query.setMaxResults(100);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lst;
    }
    public Long CheckManufacturerName(String name)
    {
        String HQL="SELECT COUNT(*) FROM CosCatManufacturer a  WHERE a.isActive=1"
                + " AND nameValid=?";
         Query countQuery = session.createQuery(HQL.toString());
         countQuery.setParameter(0, name);
         Long count = (Long) countQuery.uniqueResult();
         return count;
                
    }

}
