/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosPermit;
import com.viettel.utils.Constants;

import java.util.List;
import org.hibernate.Query;



/**
 *
 * @author Linhdx
 */
public class CosPermitDAO extends
        GenericDAOHibernate<CosPermit, Long> {

    public CosPermitDAO() {
        super(CosPermit.class);
    }

    @Override
    public void saveOrUpdate(CosPermit obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }
    }

    
    public CosPermit findById(Long id) {
        Query query = getSession().getNamedQuery(
                "Permit.findByPermitId");
        query.setParameter("permitId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CosPermit) result.get(0);
        }
    }

    public List<CosPermit> findAllActiveByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from CosPermit a where a.fileId = :fileId "
                + "and a.isActive = :isActive");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        return result;
    }
    
    public CosPermit findLastByFileId(Long fileId) {
        CosPermit obj = new CosPermit();
        Query query = getSession().createQuery("select a from CosPermit a where a.fileId = :fileId "
                + "and a.isActive = :isActive ORDER BY a.permitId desc");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        if(result !=null && result.size() > 0){
            obj = (CosPermit)result.get(0);
        }
        return obj;
    }
    
    public int checkPermit(Long fileId){
        List<CosPermit> lstPermit = findAllActiveByFileId(fileId);
        if(lstPermit==null || lstPermit.isEmpty()){
           return Constants.CHECK_VIEW.NOT_VIEW;
        }else{
            return Constants.CHECK_VIEW.VIEW;
        }
    }

//    public String LayTruong(String sTenBang, String sTruongKhoa, String sGiaTriKhoa, String sTenTruong) {
//        String SQL = new String();
//        SQL = String.format("SELECT r.%s FROM %s r WHERE r.%s='%s'", sTenTruong, sTenBang, sTruongKhoa, sGiaTriKhoa);
//        Query fileQuery = session.createQuery(SQL.toString());
//        return (String) fileQuery.list().get(0).toString();
//
//    }

}
