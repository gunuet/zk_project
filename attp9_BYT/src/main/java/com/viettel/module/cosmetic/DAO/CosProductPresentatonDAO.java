/*
 * To change this template, choose Tools | CosProductPresentatons
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosProductPresentaton;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class CosProductPresentatonDAO extends GenericDAOHibernate<CosProductPresentaton, Long> {

    public CosProductPresentatonDAO() {
        super(CosProductPresentaton.class);
    }

    public void delete(Long id) {
        CosProductPresentaton obj = findById(id);
        obj.setIsActive(0l);
        update(obj);
    }

    public List getAllActive() {
        List<CosProductPresentaton> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosProductPresentaton a ");
            stringBuilder.append("  where a.isActive = 1 "
                    + " order by nlssort(lower(ltrim(a.nameVi)),'nls_sort = Vietnamese') ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List getAllActivePlusDifferent() {
        List<CosProductPresentaton> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosProductPresentaton a ");
            stringBuilder.append("  where a.isActive = 1 "
                    + " order by nlssort(lower(ltrim(a.nameVi)),'nls_sort = Vietnamese') ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            lst = new ArrayList<>();
        }
        CosProductPresentaton diffPresentation = new CosProductPresentaton();
        diffPresentation.setProductPresentationId(-1L);
        diffPresentation.setNameVi("Các dạng khác (Đề nghị ghi rõ)");
        diffPresentation.setNameEn("Others (please specify))");
        diffPresentation.setIsDifferent(1L);
        diffPresentation.setIsActive(1L);
        lst.add(diffPresentation);
        return lst;
    }

    public List findByPresentationStringList(String presentationList) {

        if (presentationList == null) {
            return new ArrayList();
        }
        presentationList = StringUtils.strip(presentationList.trim(), ";");
        String[] presentationIds = StringUtils.split(presentationList, ";");
        Long[] ids = new Long[presentationIds.length];
        for (int i = 0; i < presentationIds.length; i++) {
            ids[i] = Long.parseLong(presentationIds[i]);
        }
        List<CosProductPresentaton> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosProductPresentaton a ");
            stringBuilder.append("  where a.isActive = 1 AND a.productPresentationId IN :presentationIds"
                    + " order by nlssort(lower(ltrim(a.nameVi)),'nls_sort = Vietnamese') ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameterList("presentationIds", ids);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }
}
