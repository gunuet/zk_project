/*
 * To change this template, choose Tools | CosProductTypes
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosProductPresentaton;
import com.viettel.module.cosmetic.BO.CosProductType;
import com.viettel.module.cosmetic.BO.CosProductTypeSub;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class CosProductTypeDAO extends GenericDAOHibernate<CosProductType, Long> {

    public CosProductTypeDAO() {
        super(CosProductType.class);
    }

    public void delete(Long id) {
        CosProductType obj = findById(id);
        obj.setIsActive(0l);
        update(obj);
    }

    public List getAllActive() {
        List<CosProductType> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosProductType a ");
            stringBuilder.append("  where a.isActive = 1 "
                    + " order by nlssort(lower(ltrim(a.name)),'nls_sort = Vietnamese') ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findByProductTypeStringList(String productTypeList) {
        List<CosProductType> lst = null;
        Long[] ids = splitToLong(productTypeList, ";");
        if (ids != null) {
            try {
                StringBuilder stringBuilder = new StringBuilder(" from CosProductType a ");
                stringBuilder.append("  where a.isActive = 1 AND a.productTypeId IN :productTypeIds"
                        + " order by nlssort(lower(ltrim(a.nameVi)),'nls_sort = Vietnamese') ");
                Query query = getSession().createQuery(stringBuilder.toString());
                query.setParameterList("productTypeIds", ids);
                lst = query.list();
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
                return new ArrayList<>();
            }
        }
        return lst;
    }
    
    public List findProductTypeAndSub(String productTypeList) {
        Long[] ids = splitToLong(productTypeList, ";");
        if (ids != null) {
            StringBuilder hql = new StringBuilder("SELECT p, s FROM CosProductType p, CosProductTypeSub s WHERE p.productTypeId = s.productTypeId AND p.productTypeId IN :productTypeId");
            hql.append(" AND p.isActive > 0");
            hql.append(" ORDER BY p.ord");
            Query query = getSession().createQuery(hql.toString());
            query.setParameterList("productTypeId", ids);
            List result = query.list();
            return result;
        }
        return null;
    }
   

}
