/*
 * To change this template, choose Tools | CosProductTypeSubs
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosProductType;
import com.viettel.module.cosmetic.BO.CosProductTypeSub;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author linhdx
 */
public class CosProductTypeSubDAO extends GenericDAOHibernate<CosProductTypeSub, Long> {

    public CosProductTypeSubDAO() {
        super(CosProductTypeSub.class);
    }

    public void delete(Long id) {
        CosProductTypeSub obj = findById(id);
        obj.setIsActive(0l);
        update(obj);
    }

    public List getAllActive() {
        List<CosProductTypeSub> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from CosProductTypeSub a ");
//            stringBuilder.append("  where a.isActive = 1 "
//                    + " order by nlssort(lower(ltrim(a.nameVi)),'nls_sort = Vietnamese') ");
            stringBuilder.append(" where a.isActive = 1 order by a.productTypeId ASC, a.ord ASC ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List<CosProductTypeSub> getAllActiveOrderByProductType() {
        List<CosProductTypeSub> lst;
        try {
            StringBuilder stringBuilder = new StringBuilder(" select a from CosProductTypeSub a, CosProductType b ");
            stringBuilder.append(" where a.productTypeId = b.productTypeId and a.isActive = 1 "
                    + " order by a.productTypeId asc, a.ord asc ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List<CosProductType> getProductTypeFullContent() {
        List<CosProductTypeSub> lstSub = getAllActive();
        List<CosProductType> lstType = new ArrayList();
        boolean bAdd;
        for (CosProductTypeSub sub : lstSub) {
            if (lstType.isEmpty()) {
                CosProductType type = new CosProductType();
                type.setProductTypeId(sub.getProductTypeId());
                type.setName(String.format("%s (%s)", sub.getNameVi(), sub.getNameEn()));
                lstType.add(type);
            } else {
                bAdd = true;
                for (CosProductType type : lstType) {
                    if (type.getProductTypeId().equals(sub.getProductTypeId())) {
                        type.setName(String.format("%s \r\n - %s (%s)", type.getName(), sub.getNameVi(), sub.getNameEn()));
                        bAdd = false;
                        break;
                    }
                }
                if (bAdd) {
                    CosProductType type = new CosProductType();
                    type.setProductTypeId(sub.getProductTypeId());
                    type.setName(String.format("%s (%s)", sub.getNameVi(), sub.getNameEn()));
                    lstType.add(type);
                }
            }
        }
        return lstType;
    }
}
