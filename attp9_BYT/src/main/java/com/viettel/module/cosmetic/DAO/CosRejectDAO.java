/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosReject;
import com.viettel.utils.Constants;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class CosRejectDAO extends
        GenericDAOHibernate<CosReject, Long> {

    public CosRejectDAO() {
        super(CosReject.class);
    }

    @Override
    public void saveOrUpdate(CosReject obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }
    }

    
    public CosReject findById(Long id) {
        Query query = getSession().getNamedQuery(
                "Reject.findByRejectId");
        query.setParameter("rejectId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CosReject) result.get(0);
        }
    }

    public List<CosReject> findAllActiveByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from CosReject a where a.fileId = :fileId "
                + "and a.isActive = :isActive");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        return result;
    }
    
     public CosReject findLastByFileId(Long fileId) {
        CosReject obj = new CosReject();
        Query query = getSession().createQuery("select a from CosReject a where a.fileId = :fileId "
                + "and a.isActive = :isActive ORDER BY a.rejectId desc");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        if(result !=null && result.size() > 0){
            obj = (CosReject)result.get(0);
        }
        return obj;
    }



}
