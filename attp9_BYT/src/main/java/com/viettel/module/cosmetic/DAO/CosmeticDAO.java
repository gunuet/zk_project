/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;

import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;

import com.viettel.voffice.model.SearchModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class CosmeticDAO extends GenericDAOHibernate<CosFile, Long> {

    public CosmeticDAO() {
        super(CosFile.class);
    }

    @Override
    public void saveOrUpdate(CosFile cosmetic) {
        if (cosmetic != null) {
            super.saveOrUpdate(cosmetic);
        }

        getSession().flush();

    }

    @Override
    public CosFile findById(Long id) {
        Query query = getSession().getNamedQuery(
                "CosFile.findByCosFileId");
        query.setParameter("cosFileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CosFile) result.get(0);
        }
    }
    
    public Files getFileById(Long fileId){
        String hql = "select f from Files f where f.fileId = ?";
        Query query = session.createQuery(hql);
        query.setParameter(0, fileId);
        List<Files> lstFile = query.list();
        if(lstFile != null && lstFile.size()>0){
            return lstFile.get(0);
        }
        return null;
    }
    
    public Long countCosfile() {
        Query query = getSession().createQuery("select count(a) from CosFile a");
        Long count = (Long) query.uniqueResult();
        return count;
    }

    public VFileCosfile findViewByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from VFileCosfile a where a.fileId = :fileId");
        query.setParameter("fileId", fileId);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFileCosfile) result.get(0);
        }
    }

    public CosFile findByFileId(Long id) {
        Query query = getSession().getNamedQuery(
                "CosFile.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CosFile) result.get(0);
        }
    }

    public void delete(VFileCosfile vCosFile) {
//        Files findByFileId(vCosFile.getFileId());
//        cosmetic.setIsActive(Constants.Status.INACTIVE);
//        getSession().saveOrUpdate(cosmetic);
    }

    public List getList(Long deptId,
            SearchModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder hql;

        if (getSize) {
            hql = new StringBuilder(
                    "SELECT COUNT(d) FROM VFileCosfile d WHERE isActive = 1 ");
        } else {
            hql = new StringBuilder(
                    "SELECT d FROM VFileCosfile d WHERE isActive = 1 ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                hql.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                hql.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(d.nswFileCode) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(d.cosmeticNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatus() != null) {
                hql.append(" AND d.status = ?");
                docParams.add(model.getStatus());
            }
            if (model.getDocumentTypeCode() != null) {
                hql.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }
        }

        hql.append(" order by fileId desc");

        Query fileQuery = session.createQuery(hql.toString());
        List<CosFile> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

//    public List<VFileCosfile> findFilesByReceiverAndDeptId(SearchModel model, Long receiverId, Long receiveDeptId, int start, int take) {
//        String hql = "SELECT distinct(n) FROM VFileCosfile n, Process p WHERE "
//                + " n.fileId = p.objectId AND "
//                + " n.fileType = p.objectType AND "
//                + " n.status = p.status AND "
//                + " (p.receiveUserId = :receiverId OR "
//                + " (p.receiveUserId is null AND "
//                + " p.receiveGroupId = :receiveDeptId)) "
//                + "order by n.fileId desc";
//        Query query = getSession().createQuery(hql);
//        query.setParameter("receiverId", receiverId);
//        query.setParameter("receiveDeptId", receiveDeptId);
//        List<VFileCosfile> listFiles = query.list();
//        return listFiles;
//    }
    public PagingListModel findFilesByReceiverAndDeptId(SearchModel searchModel,
            Long receiverId, Long receiveDeptId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) ");
        StringBuilder strCountBuf = new StringBuilder("select count(distinct n.cosFileId) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                            + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                            + " AND no.type = ? "
                            + " ) AND "
                            //+ " 1 = 1 ");
                            + " p.receiveUserId = ? ");
                    listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " (p.receiveUserId is null AND "
                            + " p.receiveGroupId = ? ) ");
                    listParam.add(receiveDeptId);
                    break;
                default:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }

        //listParam.add(receiverId);
        //listParam.add(receiveDeptId);
        if (searchModel != null) {

            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getModifyDateFrom() != null) {
                hql.append(" AND n.modifyDate >= ? ");
                listParam.add(searchModel.getModifyDateFrom());
            }
            if (searchModel.getModifyDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getModifyDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(n.cosmeticNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }
            if (searchModel.getDocumentTypeCode() != null) {
                hql.append(" AND n.documentTypeCode = ?");
                listParam.add(searchModel.getDocumentTypeCode());
            }

            if (searchModel instanceof CosmeticFileModel) {
                CosmeticFileModel cosmeticFileModel = (CosmeticFileModel) searchModel;
                if (cosmeticFileModel.getBrandName() != null && cosmeticFileModel.getBrandName().trim().length() > 0) {
                    hql.append(" AND lower(n.brandName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBrandName()));
                }

                if (cosmeticFileModel.getProductName() != null && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getProductName()));
                }

                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null && cosmeticFileModel.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessAddress()));
                }

                if (cosmeticFileModel.getDistributePersonName() != null && cosmeticFileModel.getDistributePersonName().trim().length() > 0) {
                    hql.append(" AND lower(n.distributePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getDistributePersonName()));
                }

                if (cosmeticFileModel.getIntendedUse() != null && cosmeticFileModel.getIntendedUse().trim().length() > 0) {
                    hql.append(" AND lower(n.intendedUse) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getIntendedUse()));
                }
                if (cosmeticFileModel.getTaxCode()!= null && cosmeticFileModel.getTaxCode().trim().length() > 0) {
                    hql.append(" AND lower(n.taxCode) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getTaxCode()));
                }

            }

        }

        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
                default:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
            }
        } else {
            hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
        }

        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    public Long countHomePage(String countType) {
        if (countType.isEmpty()) {
            return 0L;
        }
        List listParam = new ArrayList();
        StringBuilder strCountBuf = new StringBuilder("select count(n) ");
        StringBuilder hql = new StringBuilder();
        hql.append(" FROM VFileCosfile n "
                + "WHERE n.isActive = 1 "
                + "AND (n.isTemp is null OR n.isTemp = 0) ");
        Long count = 0L;
        try {
            switch (countType) {
                case Constants.COUNT_HOME_TYPE.TOTAL:
                    hql.append("AND ((n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?)) "
                    );
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP);
                    listParam.add(Constants.FILE_STATUS_CODE.FINISH);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_DATHAMDINH);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DAXEMXET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_VT_DATIEPNHAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DATHANHTOAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DAPHANCONG);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DATHAMXET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_DAKYCONGVANSDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_DAPHEDUYET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DATHONGBAOSDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_KT_DAXACNHANTHANHTOAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DASDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TRAYEUCAUKIEMTRALAI);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_VT_DAGUIPHIEUBAOTHU);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CHOPHANCONG);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_TUCHOIDUYET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHDAT);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHKHONGDAT);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_KT_TUCHOIXACNHANPHI);
                    break;
                case Constants.COUNT_HOME_TYPE.WAIT_PROCESS:
                    hql.append("AND (n.status = ?) ");
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP);
                    break;
                case Constants.COUNT_HOME_TYPE.PROCESS:
                    hql.append("AND ((n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?) "
                            + "OR (n.status = ?)) "
                    );
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_DATHAMDINH);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DAXEMXET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_VT_DATIEPNHAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DATHANHTOAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DAPHANCONG);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DATHAMXET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_DAKYCONGVANSDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_DAPHEDUYET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DATHONGBAOSDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_KT_DAXACNHANTHANHTOAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DASDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TRAYEUCAUKIEMTRALAI);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_VT_DAGUIPHIEUBAOTHU);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CHOPHANCONG);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_TUCHOIDUYET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHDAT);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHKHONGDAT);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_KT_TUCHOIXACNHANPHI);
                    break;
                case Constants.COUNT_HOME_TYPE.FINISH:
                    hql.append("AND (n.status = ?) ");
                    listParam.add(Constants.FILE_STATUS_CODE.FINISH);
                    break;
                default:
                    break;
            }

            strCountBuf.append(hql);
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                countQuery.setParameter(i, listParam.get(i));
            }

            count = (Long) countQuery.uniqueResult();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return count;
    }

    public PagingListModel findFilesByCreatorId(SearchModel searchModel, Long creatorId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) FROM VFileCosfile n WHERE n.isActive = 1  ");
        StringBuilder strCountBuf = new StringBuilder("select count(distinct n.cosFileId) FROM VFileCosfile n WHERE n.isActive = 1  ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);

        if (searchModel != null) {
            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getModifyDateFrom() != null) {
                hql.append(" AND n.modifyDate >= ? ");
                listParam.add(searchModel.getModifyDateFrom());
            }
            if (searchModel.getModifyDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getModifyDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(n.cosmeticNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }

            if (searchModel instanceof CosmeticFileModel) {
                CosmeticFileModel cosmeticFileModel = (CosmeticFileModel) searchModel;
                if (cosmeticFileModel.getBrandName() != null && cosmeticFileModel.getBrandName().trim().length() > 0) {
                    hql.append(" AND lower(n.brandName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBrandName()));
                }

                if (cosmeticFileModel.getProductName() != null && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getProductName()));
                }

                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null && cosmeticFileModel.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessAddress()));
                }

                if (cosmeticFileModel.getDistributePersonName() != null && cosmeticFileModel.getDistributePersonName().trim().length() > 0) {
                    hql.append(" AND lower(n.distributePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getDistributePersonName()));
                }

                if (cosmeticFileModel.getIntendedUse() != null && cosmeticFileModel.getIntendedUse().trim().length() > 0) {
                    hql.append(" AND lower(n.intendedUse) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getIntendedUse()));
                }

            }

        }

        hql.append(" order by n.modifyDate desc,n.nswFileCode desc");

        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    /**
     * ?
     * Ham kiem tra giay phep 0:Giay phep, 1: sua doi bo sung -1: Chua xuat giay
     *
     * @param fileId
     * @param userID
     * @return
     */
    public int CheckGiayPhep(Long fileId, Long userID) {
        String HQL = " SELECT count(r) FROM CosPermit r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        //Check PERMIT_STATUS  da dong dau
        //query.setParameter("fileId", Constants_Cos.EVALUTION.PERMIT_STATUS.PROVIDED_NUMBER);
        Long count = (Long) query.uniqueResult();
        //neu co du lieu bang permit
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }

        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    public int checkDispathSDBS(Long fileId, Long userID) {

        String HQL = " SELECT count(r) FROM CosAdditionalRequest r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }
    
    public int checkDispathReject(Long fileId, Long userID) {

        String HQL = " SELECT count(r) FROM CosReject r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    public List findListStatusByReceiverAndDeptId(SearchModel searchModel, Long receiverId, Long receiveDeptId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                            + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                            + " AND no.type = ? "
                            + " ) AND "
                            //+ " 1 = 1 ");
                            + " p.receiveUserId = ? ");
                    listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " (p.receiveUserId is null AND "
                            + " p.receiveGroupId = ? ) ");
                    listParam.add(receiveDeptId);
                    break;
                default:
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }
        //  hql.append(" order by n.fileId desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }

    public List findListStatusByCreatorId(Long creatorId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) FROM VFileCosfile n WHERE n.isActive = 1 AND n.status is not null ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);
        hql.append(" order by n.status desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }
}
