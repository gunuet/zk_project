/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.CosProductPresentaton;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author LONG HOANG GIANG
 */
public class ProductPresentationDAO extends GenericDAOHibernate<CosProductPresentaton, Long> {

    public ProductPresentationDAO() {
        super(CosProductPresentaton.class);
    }

    @Override
    public CosProductPresentaton findById(Long id) {
        Query query = getSession().getNamedQuery("CosProductPresentaton.findByProductPresentationId");
        query.setParameter("productPresentationId", id);
        List result = query.list();
        return result.isEmpty() ? null : (CosProductPresentaton) result.get(0);
    }

    @Override
    public void saveOrUpdate(CosProductPresentaton presentation) {
        if (presentation != null) {
            super.saveOrUpdate(presentation);
            getSession().getTransaction().commit();
        }
    }

    @Override
    public void delete(CosProductPresentaton bean) {
        if (bean != null) {
            if (bean.getIsActive() == 0L) {
                super.delete(bean);
            } else {
                bean.setIsActive(0L);
                update(bean);
            }
            getSession().flush();
        }
    }
    
    public List<CosProductPresentaton> search(CosProductPresentaton bean) {
//        String sql = "SELECT p FROM CosProductPresentaton p WHERE 1=1 AND p.isActive = 1";
        String sql = "SELECT p FROM CosProductPresentaton p WHERE 1=1";
        List params = new ArrayList();
        if (bean != null) {
            
            sql += " AND p.isActive = ? ";
//            params.add(1L);
            params.add(bean.getIsActive());
            
            if (bean.getNameVi() != null && !"".equals(bean.getNameVi())) {
                sql += " AND lower(p.nameVi) LIKE ? ESCAPE '/' ";
                params.add(StringUtils.toLikeString(bean.getNameVi()));
            }
            
            if (bean.getNameEn() != null && !"".equals(bean.getNameEn())) {
                sql += " AND lower(p.nameEn) LIKE ? ESCAPE '/' ";
                params.add(StringUtils.toLikeString(bean.getNameEn()));
            }
        }
        
        Query query = getSession().createQuery(sql);
        for (int i=0;i<params.size();i++) {
            query.setParameter(i, params.get(i));
        }
        
        List result = query.list();
        return result;
        
    }
    
}
