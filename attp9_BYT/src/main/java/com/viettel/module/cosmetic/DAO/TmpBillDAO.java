/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.TmpBill;
import com.viettel.utils.Constants;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author GPCP_BINHNT53
 */
public class TmpBillDAO extends GenericDAOHibernate<TmpBill, Long> {

    public TmpBillDAO() {
        super(TmpBill.class);
    }

    public void delete(Long id) {
//        TmpBill obj = findById(id);
    }

    @Override
    public void saveOrUpdate(TmpBill cos) {
        if (cos != null) {
            super.saveOrUpdate(cos);
        }

        getSession().flush();

    }

    public List findAllTmpBill() {
        List<TmpBill> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from TmpBill a ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lst;
    }

    public List findTmpBillLastImport() {
        List<TmpBill> lst = null;
        List listParam = new ArrayList();
        try {
            StringBuilder stringBuilder = new StringBuilder(" from TmpBill a where a.isLastImp = ?");
            listParam.add(Constants.Status.ACTIVE);
            Query query = getSession().createQuery(stringBuilder.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
            }
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lst;
    }

    public List findTmpBill(Long status, Long isLastImport) {
        List<TmpBill> lst = null;
        List listParam = new ArrayList();
        try {
            StringBuilder stringBuilder = new StringBuilder(" from TmpBill a where a.status = ? and a.isLastImp = ?");
            listParam.add(status);
            listParam.add(isLastImport);
            Query query = getSession().createQuery(stringBuilder.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
            }
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lst;
    }

    public PagingListModel search(TmpBill searchForm, Date startDate, Date endDate, int start, int take) {
        try {
            StringBuilder hql = new StringBuilder(" from TmpBill r where 1 = 1 ");
            List lstParam = new ArrayList();
            if (startDate != null) {
                hql.append(" and r.billDate >= ?");
                startDate = DateTimeUtils.setStartTimeOfDate(startDate);
                lstParam.add(startDate);
            }
            if (endDate != null) {
                hql.append(" and r.billDate <= ?");
                endDate = DateTimeUtils.setStartTimeOfDate(endDate);
                lstParam.add(endDate);
            }
            if (searchForm != null) {
                if (searchForm.getStatus() != null && searchForm.getStatus() > Constants.COMBOBOX_HEADER_VALUE) {
                    hql.append(" and r.status = ?");
                    lstParam.add(searchForm.getStatus());
                }
                if (searchForm.getIsLastImp() != null && searchForm.getIsLastImp() > Constants.COMBOBOX_HEADER_VALUE) {
                    hql.append(" and r.isLastImp = ?");
                    lstParam.add(searchForm.getIsLastImp());
                }
            }
            StringBuilder selectHql = new StringBuilder("select r ").append(hql).append(" order by r.billDate desc");
            StringBuilder countHql = new StringBuilder("select count(r) ").append(hql);
            Query query = session.createQuery(selectHql.toString());
            Query countQuery = session.createQuery(countHql.toString());
            for (int i = 0; i < lstParam.size(); i++) {
                query.setParameter(i, lstParam.get(i));
                countQuery.setParameter(i, lstParam.get(i));
            }
            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }
            Long count = (Long) countQuery.uniqueResult();
            query.setFirstResult(start);
            query.setMaxResults(take);
            List lst = query.list();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
}
