/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.Model.CosmeticSearchModel;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author GPCP_BINHNT53
 */
public class VFileCosfileDAO extends GenericDAOHibernate<VFileCosfile, Long> {

    public VFileCosfileDAO() {
        super(VFileCosfile.class);
    }

    public void delete(Long id) {
//        VFileCosfile obj = findById(id);
    }

    @Override
    public void saveOrUpdate(VFileCosfile bo) {
        if (bo != null) {
            super.saveOrUpdate(bo);
        }

        getSession().flush();

    }

    public List findAllTmpBill() {
        List<VFileCosfile> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from VFileCosfile a ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lst;
    }

    public VFileCosfile findAllByFileId(Long fileId) {
        List<VFileCosfile> lst = null;
        List listParam = new ArrayList();
        try {
            StringBuilder stringBuilder = new StringBuilder(" from VFileCosfile a where fileId = ?");
            listParam.add(fileId);
            Query query = getSession().createQuery(stringBuilder.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
            }
            lst = query.list();
            if (!lst.isEmpty()) {
                return lst.get(0);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
        return null;
    }

    public VFileCosfile findAllByNswFileCode(String nswFileCode) {
        List<VFileCosfile> lst = null;
        List listParam = new ArrayList();
        try {
            StringBuilder stringBuilder = new StringBuilder(" from VFileCosfile a where a.nswFileCode = ?");
            listParam.add(nswFileCode);
            Query query = getSession().createQuery(stringBuilder.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
            }
            lst = query.list();
            if (!lst.isEmpty()) {
                return lst.get(0);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
        return null;
    }
    
    public List searchPermit(CosmeticSearchModel searchModel) {
        
        StringBuilder hql = new StringBuilder(" from VFileCosfile v, CosPermit p WHERE v.fileId = p.fileId ");
        List params = new ArrayList();
        List result = new ArrayList();
        if (searchModel != null) {
            if (searchModel.getBusinessName() != null && !"".equals(searchModel.getBusinessName())) {
                hql.append(" AND lower(v.businessName) LIKE ? ESCAPE '/' ");
                params.add(StringUtils.toLikeString(searchModel.getBusinessName()));
            }
            if (searchModel.getBusinessAddress() != null && !"".equals(searchModel.getBusinessAddress())) {
                hql.append(" AND lower(v.businessAddress) LIKE ? ESCAPE '/' ");
                params.add(StringUtils.toLikeString(searchModel.getBusinessAddress()));
            }
            if (searchModel.getProductName() != null && !"".equals(searchModel.getProductName())) {
                hql.append(" AND lower(v.productName) LIKE ? ESCAPE '/' ");
                params.add(StringUtils.toLikeString(searchModel.getProductName()));
            }
            if (searchModel.getReceiveNo() != null && !"".equals(searchModel.getReceiveNo())) {
                hql.append(" AND lower(p.receiveNo) LIKE ? ESCAPE '/' ");
                params.add(StringUtils.toLikeString(searchModel.getReceiveNo()));
            }
            if (searchModel.getReceiveDateFrom() != null) {
                hql.append(" AND p.receiveDate >= ? ");
                params.add(DateTimeUtils.setStartTimeOfDate(searchModel.getReceiveDateFrom()));
            }
            if (searchModel.getReceiveDateTo() != null) {
                hql.append(" AND p.receiveDate <= ? ");
                params.add(DateTimeUtils.setEndTimeOfDate(searchModel.getReceiveDateTo()));
            }
            if (searchModel.getArrange() != null) {
                if (searchModel.getArrange() == 1L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.businessName)), 'nls_sort = Vietnamese') ASC ");
                } else if (searchModel.getArrange() == 2L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.businessName)), 'nls_sort = Vietnamese') DESC ");
                } else if (searchModel.getArrange() == 3L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.productName)), 'nls_sort = Vietnamese') ASC ");
                } else if (searchModel.getArrange() == 4L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.productName)), 'nls_sort = Vietnamese') DESC ");
                } else if (searchModel.getArrange() == 5L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.businessAddress)), 'nls_sort = Vietnamese') ASC ");
                } else if (searchModel.getArrange() == 6L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.businessAddress)), 'nls_sort = Vietnamese') DESC ");
                } else if (searchModel.getArrange() == 7L) {
                    hql.append(" ORDER BY nlssort(lower(trim(p.receiveNo)), 'nls_sort = Vietnamese') ASC ");
                } else if (searchModel.getArrange() == 8L) {
                    hql.append(" ORDER BY nlssort(lower(trim(p.receiveNo)), 'nls_sort = Vietnamese') DESC ");
                }
            }
            Query query = getSession().createQuery(hql.toString());
            for (int i=0;i<params.size();i++) {
                query.setParameter(i, params.get(i));
            }
            result = query.list();
        }
        return result;
    }
    
    public List searchReject(CosmeticSearchModel searchModel) {
        
        StringBuilder hql = new StringBuilder(" from VFileCosfile v, CosReject p WHERE v.fileId = p.fileId ");
        List params = new ArrayList();
        List result = new ArrayList();
        if (searchModel != null) {
            if (searchModel.getBusinessName() != null && !"".equals(searchModel.getBusinessName())) {
                hql.append(" AND lower(v.businessName) LIKE ? ESCAPE '/' ");
                params.add(StringUtils.toLikeString(searchModel.getBusinessName()));
            }
            if (searchModel.getBusinessAddress() != null && !"".equals(searchModel.getBusinessAddress())) {
                hql.append(" AND lower(v.businessAddress) LIKE ? ESCAPE '/' ");
                params.add(StringUtils.toLikeString(searchModel.getBusinessAddress()));
            }
            if (searchModel.getProductName() != null && !"".equals(searchModel.getProductName())) {
                hql.append(" AND lower(v.productName) LIKE ? ESCAPE '/' ");
                params.add(StringUtils.toLikeString(searchModel.getProductName()));
            }
            if (searchModel.getReceiveNo() != null && !"".equals(searchModel.getReceiveNo())) {
                hql.append(" AND lower(p.receiveNo) LIKE ? ESCAPE '/' ");
                params.add(StringUtils.toLikeString(searchModel.getReceiveNo()));
            }
            if (searchModel.getReceiveDateFrom() != null) {
                hql.append(" AND p.receiveDate >= ? ");
                params.add(DateTimeUtils.setStartTimeOfDate(searchModel.getReceiveDateFrom()));
            }
            if (searchModel.getReceiveDateTo() != null) {
                hql.append(" AND p.receiveDate <= ? ");
                params.add(DateTimeUtils.setEndTimeOfDate(searchModel.getReceiveDateTo()));
            }
            if (searchModel.getArrange() != null) {
                if (searchModel.getArrange() == 1L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.businessName)), 'nls_sort = Vietnamese') ASC ");
                } else if (searchModel.getArrange() == 2L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.businessName)), 'nls_sort = Vietnamese') DESC ");
                } else if (searchModel.getArrange() == 3L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.productName)), 'nls_sort = Vietnamese') ASC ");
                } else if (searchModel.getArrange() == 4L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.productName)), 'nls_sort = Vietnamese') DESC ");
                } else if (searchModel.getArrange() == 5L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.businessAddress)), 'nls_sort = Vietnamese') ASC ");
                } else if (searchModel.getArrange() == 6L) {
                    hql.append(" ORDER BY nlssort(lower(trim(v.businessAddress)), 'nls_sort = Vietnamese') DESC ");
                } else if (searchModel.getArrange() == 7L) {
                    hql.append(" ORDER BY nlssort(lower(trim(p.receiveNo)), 'nls_sort = Vietnamese') ASC ");
                } else if (searchModel.getArrange() == 8L) {
                    hql.append(" ORDER BY nlssort(lower(trim(p.receiveNo)), 'nls_sort = Vietnamese') DESC ");
                }
            }
            Query query = getSession().createQuery(hql.toString());
            for (int i=0;i<params.size();i++) {
                query.setParameter(i, params.get(i));
            }
            result = query.list();
        }
        return result;
    }
}
