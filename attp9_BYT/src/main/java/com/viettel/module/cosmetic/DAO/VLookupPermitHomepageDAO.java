/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.VLookupPermitHomepage;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author GPCP_BINHNT53
 */
public class VLookupPermitHomepageDAO extends GenericDAOHibernate<VLookupPermitHomepage, Long> {

    public VLookupPermitHomepageDAO() {
        super(VLookupPermitHomepage.class);
    }

    public void delete(Long id) {
//        VLookupPermitHomepage obj = findById(id);
    }

    @Override
    public void saveOrUpdate(VLookupPermitHomepage cos) {
        if (cos != null) {
            super.saveOrUpdate(cos);
        }

        getSession().flush();

    }

    public List findAll() {
        List<VLookupPermitHomepage> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from VLookupPermitHomepage a ");
            Query query = getSession().createQuery(stringBuilder.toString());
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return lst;
    }

    public PagingListModel search(VLookupPermitHomepage searchForm, int start, int take) {
        try {
            StringBuilder hql = new StringBuilder(" from VLookupPermitHomepage v where 1 = 1 ");
            List lstParam = new ArrayList();

            if (searchForm != null) {
                if (searchForm.getBusinessName() != null && searchForm.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(v.businessName) like ? escape '/'");
                    lstParam.add(StringUtils.toLikeString(searchForm.getBusinessName()));
                }
                if (searchForm.getBusinessAddress() != null && searchForm.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(v.businessAddress) like ? escape '/'");
                    lstParam.add(StringUtils.toLikeString(searchForm.getBusinessAddress()));
                }
                if (searchForm.getProductName() != null && searchForm.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(v.productName) like ? escape '/'");
                    lstParam.add(StringUtils.toLikeString(searchForm.getProductName()));
                }
                if (searchForm.getReceiveNo() != null && searchForm.getReceiveNo().trim().length() > 0) {
                    hql.append(" AND lower(v.receiveNo) like ? escape '/'");
                    lstParam.add(StringUtils.toLikeString(searchForm.getReceiveNo()));
                }
                if (searchForm.getEffectiveDateFrom() != null) {
                    hql.append(" and v.effectiveDate >= ?");
                    searchForm.setEffectiveDate(DateTimeUtils.setStartTimeOfDate(searchForm.getEffectiveDateFrom()));
                    lstParam.add(searchForm.getEffectiveDateFrom());
                }
                if (searchForm.getEffectiveDateTo() != null) {
                    hql.append(" and v.effectiveDate <= ?");
                    searchForm.setEffectiveDate(DateTimeUtils.setStartTimeOfDate(searchForm.getEffectiveDateTo()));
                    lstParam.add(searchForm.getEffectiveDateTo());
                }

            }
            if (searchForm.getArrange() != null) {
                if (searchForm.getArrange() == 1) {
                    hql.append(" order by nlssort(lower(trim(v.businessName)),'nls_sort = Vietnamese') asc ");
                } else if (searchForm.getArrange() == 2) {
                    hql.append(" order by nlssort(lower(trim(v.businessName)),'nls_sort = Vietnamese') desc ");
                } else if (searchForm.getArrange() == 3) {
                    hql.append(" order by nlssort(lower(trim(v.productName)),'nls_sort = Vietnamese') asc ");
                } else if (searchForm.getArrange() == 4) {
                    hql.append(" order by nlssort(lower(trim(v.productName)),'nls_sort = Vietnamese') desc ");
                } else if (searchForm.getArrange() == 5) {
                    hql.append(" order by nlssort(lower(trim(v.businessAddress)),'nls_sort = Vietnamese') asc ");
                } else if (searchForm.getArrange() == 6) {
                    hql.append(" order by nlssort(lower(trim(v.businessAddress)),'nls_sort = Vietnamese') desc ");
                } else if (searchForm.getArrange() == 7) {
                    hql.append(" order by nlssort(lower(trim(v.receiveNo)),'nls_sort = Vietnamese') asc ");
                } else if (searchForm.getArrange() == 8) {
                    hql.append(" order by nlssort(lower(trim(v.receiveNo)),'nls_sort = Vietnamese') desc ");
                }
            }
            StringBuilder selectHql = new StringBuilder("select v ").append(hql);
            StringBuilder countHql = new StringBuilder("select count(v) ").append(hql);
            Query query = session.createQuery(selectHql.toString());
            Query countQuery = session.createQuery(countHql.toString());
            for (int i = 0; i < lstParam.size(); i++) {
                query.setParameter(i, lstParam.get(i));
                countQuery.setParameter(i, lstParam.get(i));
            }
            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }
            Long count = (Long) countQuery.uniqueResult();
            query.setFirstResult(start);
            query.setMaxResults(take);
            List lst = query.list();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
}
