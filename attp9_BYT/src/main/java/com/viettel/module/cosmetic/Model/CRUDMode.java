/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.cosmetic.Model;

/**
 *
 * @author giangnh20
 */
public enum CRUDMode {
    
    CREATE(0), EDIT(1), DELETE(2), COPY(3);
    
    private final int value;
    
    private CRUDMode(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return value;
    }
}
