/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Model;

import com.viettel.module.rapidtest.model.*;
import java.util.Date;

/**
 *
 * @author ChucHV
 */
public class CosExportModel {

    private int typeExport;
    private String content;
    private Date signDate;
    private String sendNo;
    private String businessName;
    private String businessAddress;
    private String signer;
    private String rolesigner;
    private String leaderSinged;

    private Long objectId;
    private Long objectType;

    private String rapidTestName;
    private String placeOfManufacture;

    private String pathTemplate;

    public CosExportModel() {
    }

    public int getTypeExport() {
        return typeExport;
    }

    public void setTypeExport(int typeExport) {
        this.typeExport = typeExport;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSendNo() {
        return sendNo;
    }

    public void setSendNo(String sendNo) {
        this.sendNo = sendNo;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getSigner() {
        return signer;
    }

    public void setSigner(String signer) {
        this.signer = signer;
    }

    public String getRolesigner() {
        return rolesigner;
    }

    public void setRolesigner(String rolesigner) {
        this.rolesigner = rolesigner;
    }

    public String getLeaderSinged() {
        return leaderSinged;
    }

    public void setLeaderSinged(String leaderSinged) {
        this.leaderSinged = leaderSinged;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getObjectType() {
        return objectType;
    }

    public void setObjectType(Long objectType) {
        this.objectType = objectType;
    }

    public String getRapidTestName() {
        return rapidTestName;
    }

    public void setRapidTestName(String rapidTestName) {
        this.rapidTestName = rapidTestName;
    }

    public String getPlaceOfManufacture() {
        return placeOfManufacture;
    }

    public void setPlaceOfManufacture(String placeOfManufacture) {
        this.placeOfManufacture = placeOfManufacture;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getPathTemplate() {
        return pathTemplate;
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

}
