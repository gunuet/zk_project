/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Model;

import com.viettel.module.cosmetic.BO.CosProductTypeSub;
import java.util.List;


/**
 *
 * @author linhdx
 */
public class CosProductTypeSubModel {
    private Long productTypeSubId;
    private Long productTypeId;
    private String nameVi;
    private String nameEn;
    private String nameLink;//Ten đe xuong dong
    private List<CosProductTypeSub> lstCosProductTypeSub;
    private Long isActive;
    private Long isDifferent;
    private Long ord;

    public Long getProductTypeSubId() {
        return productTypeSubId;
    }

    public void setProductTypeSubId(Long productTypeSubId) {
        this.productTypeSubId = productTypeSubId;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getNameVi() {
        return nameVi;
    }

    public void setNameVi(String nameVi) {
        this.nameVi = nameVi;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameLink() {
        return nameLink;
    }

    public void setNameLink(String nameLink) {
        this.nameLink = nameLink;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getOrd() {
        return ord;
    }

    public void setOrd(Long ord) {
        this.ord = ord;
    }

    public Long getIsDifferent() {
        return isDifferent;
    }

    public void setIsDifferent(Long isDifferent) {
        this.isDifferent = isDifferent;
    }
    
    

    public List<CosProductTypeSub> getLstCosProductTypeSub() {
        return lstCosProductTypeSub;
    }

    public void setLstCosProductTypeSub(List<CosProductTypeSub> lstCosProductTypeSub) {
        this.lstCosProductTypeSub = lstCosProductTypeSub;
    }
    
    
    
    
    
    

    
}
