/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Model;

import com.viettel.voffice.model.SearchModel;
import java.util.Date;
import java.util.List;

/**
 *
 * @author giangnh20
 */
public class CosmeticFileModel extends SearchModel {

    private Long fileId;
    private String nswFileCode;
    private Long cosFileId;
    private Long extensionNo;
    private String cosmeticNo;
    private String brandName;
    private String productName;
    private String productType;
    private String otherProductType;
    private String intendedUse;
    private String productPresentation;
    private String otherProductPresentation;
    private String distributorName;
    private String distributorAddress;
    private String distributorPhone;
    private String distributorFax;
    private String distributorRegisNumber;
    private String distributePersonName;
    private String distributePersonPhone;
    private String distributePersonEmail;
    private String distributePersonPosition;
    private String importerName;
    private String importerAddress;
    private String importerPhone;
    private String importerFax;
    private Long tested;
    private Long roled;
    private String circulatingNo;
    private String signPlace;
    private Date signDate;
    private String signName;
    private Date dateIssue;
    private String dateEffect;
    private String contents;
    private String attachmentInfos;
    private Long fileType;
    private String fileTypeName;
    private String fileCode;
    private String fileName;
    private String taxCode;
    private Long businessId;
    private String businessName;
    private String businessAddress;
    private String businessPhone;
    private String businessFax;
    private Date createDate;
    private Date modifyDate;
    private Long creatorId;
    private String creatorName;
    private Long createDeptId;
    private String createDeptName;
    private Long isActive;
    private Long version;
    private Long parentFileId;
    private Long isTemp;
    

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getExtensionNo() {
        return extensionNo;
    }

    public void setExtensionNo(Long extensionNo) {
        this.extensionNo = extensionNo;
    }

    public String getCosmeticNo() {
        return cosmeticNo;
    }

    public void setCosmeticNo(String cosmeticNo) {
        this.cosmeticNo = cosmeticNo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getOtherProductType() {
        return otherProductType;
    }

    public void setOtherProductType(String otherProductType) {
        this.otherProductType = otherProductType;
    }

    public String getIntendedUse() {
        return intendedUse;
    }

    public void setIntendedUse(String intendedUse) {
        this.intendedUse = intendedUse;
    }

    public String getProductPresentation() {
        return productPresentation;
    }

    public void setProductPresentation(String productPresentation) {
        this.productPresentation = productPresentation;
    }

    public String getOtherProductPresentation() {
        return otherProductPresentation;
    }

    public void setOtherProductPresentation(String otherProductPresentation) {
        this.otherProductPresentation = otherProductPresentation;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorPhone() {
        return distributorPhone;
    }

    public void setDistributorPhone(String distributorPhone) {
        this.distributorPhone = distributorPhone;
    }

    public String getDistributorFax() {
        return distributorFax;
    }

    public void setDistributorFax(String distributorFax) {
        this.distributorFax = distributorFax;
    }

    public String getDistributorRegisNumber() {
        return distributorRegisNumber;
    }

    public void setDistributorRegisNumber(String distributorRegisNumber) {
        this.distributorRegisNumber = distributorRegisNumber;
    }

    public String getDistributePersonName() {
        return distributePersonName;
    }

    public void setDistributePersonName(String distributePersonName) {
        this.distributePersonName = distributePersonName;
    }

    public String getDistributePersonPhone() {
        return distributePersonPhone;
    }

    public void setDistributePersonPhone(String distributePersonPhone) {
        this.distributePersonPhone = distributePersonPhone;
    }

    public String getDistributePersonEmail() {
        return distributePersonEmail;
    }

    public void setDistributePersonEmail(String distributePersonEmail) {
        this.distributePersonEmail = distributePersonEmail;
    }

    public String getDistributePersonPosition() {
        return distributePersonPosition;
    }

    public void setDistributePersonPosition(String distributePersonPosition) {
        this.distributePersonPosition = distributePersonPosition;
    }

    public String getImporterName() {
        return importerName;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public void setImporterAddress(String importerAddress) {
        this.importerAddress = importerAddress;
    }

    public String getImporterPhone() {
        return importerPhone;
    }

    public void setImporterPhone(String importerPhone) {
        this.importerPhone = importerPhone;
    }

    public String getImporterFax() {
        return importerFax;
    }

    public void setImporterFax(String importerFax) {
        this.importerFax = importerFax;
    }

    public Long getTested() {
        return tested;
    }

    public void setTested(Long tested) {
        this.tested = tested;
    }

    public Long getRoled() {
        return roled;
    }

    public void setRoled(Long roled) {
        this.roled = roled;
    }

    public String getCirculatingNo() {
        return circulatingNo;
    }

    public void setCirculatingNo(String circulatingNo) {
        this.circulatingNo = circulatingNo;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public Date getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    public String getDateEffect() {
        return dateEffect;
    }

    public void setDateEffect(String dateEffect) {
        this.dateEffect = dateEffect;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getAttachmentInfos() {
        return attachmentInfos;
    }

    public void setAttachmentInfos(String attachmentInfos) {
        this.attachmentInfos = attachmentInfos;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getParentFileId() {
        return parentFileId;
    }

    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public Long getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(Long isTemp) {
        this.isTemp = isTemp;
    }


}