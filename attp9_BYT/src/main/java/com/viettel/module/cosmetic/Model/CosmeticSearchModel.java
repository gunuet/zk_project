/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.cosmetic.Model;

import java.util.Date;

/**
 * 
 * @author giangnh20
 */
public class CosmeticSearchModel {
    
    private String businessName;
    private String businessAddress;
    private String productName;
    private String receiveNo;
    private Date receiveDateFrom;
    private Date receiveDateTo;
    private Long arrange;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = trimString(businessName);
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = trimString(businessAddress);
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = trimString(productName);
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = trimString(receiveNo);
    }

    public Date getReceiveDateFrom() {
        return receiveDateFrom;
    }

    public void setReceiveDateFrom(Date receiveDateFrom) {
        this.receiveDateFrom = receiveDateFrom;
    }

    public Date getReceiveDateTo() {
        return receiveDateTo;
    }

    public void setReceiveDateTo(Date receiveDateTo) {
        this.receiveDateTo = receiveDateTo;
    }

    public Long getArrange() {
        return arrange;
    }

    public void setArrange(Long arrange) {
        this.arrange = arrange;
    }
    
    public String trimString(String input) {
        if (input != null) {
            return input.trim();
        }
        return null;
    }
    
}
