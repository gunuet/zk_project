/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Model;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosAssembler;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.cosmetic.BO.CosProductPresentaton;
import com.viettel.module.cosmetic.BO.CosProductType;
import com.viettel.module.cosmetic.BO.CosProductTypeSub;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.BO.VProductType;
import com.viettel.module.cosmetic.DAO.CosAssemblerDAO;
import com.viettel.module.cosmetic.DAO.CosCosfileIngreDAO;
import com.viettel.module.cosmetic.DAO.CosManufacturerDAO;
import com.viettel.module.cosmetic.DAO.CosProductPresentatonDAO;
import com.viettel.module.cosmetic.DAO.CosProductTypeDAO;
import com.viettel.module.cosmetic.DAO.CosProductTypeSubDAO;
import com.viettel.module.cosmetic.DAO.CosmeticDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.bcel.classfile.Constant;

/**
 *
 * @author Vu Manh Ha
 */
public class FilesModel {
    //file info

    CosFile cosFile;
    List lstManufacturer;
    List lstAssembler;
    List lstIngredient;
    List lstAttachs;
    List lstPresentations;
    List lstProductTypes;
    //More info
    private int typeExport;
    private String content;
    private Date signDate;
    private String sendNo;
    private String businessName;
    private String businessAddress;
    private String signer;
    private String rolesigner;
    private String leaderSinged;
    private String pathTemplate;
    private Long cosmeticPermitId;
    private Long cosmeticPermitType;
    private Long cosmeticRejectId;
    private Long cosmeticRejectType;
    private Long cosmeticAdditionalId;
    private Long cosmeticAdditionalType;

    public FilesModel(Long fileId) {
        CosmeticDAO cosmeticDAO = new CosmeticDAO();
        cosFile = cosmeticDAO.findByFileId(fileId);
        Files file = cosmeticDAO.getFileById(fileId);
        businessName = file.getBusinessName();
        businessAddress = file.getBusinessAddress();

        cosmeticRejectType = Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH;
        cosmeticPermitType = Constants.OBJECT_TYPE.COSMETIC_PERMIT;//Giay phep cong bo
        cosmeticAdditionalType = Constants.OBJECT_TYPE.COSMETIC_SDBS_DISPATH;//Giay phep sua doi bo sung
        CosAssemblerDAO assemblerDAO = new CosAssemblerDAO();
        lstAssembler = assemblerDAO.findByCosFileId(cosFile.getCosFileId());
        if (lstAssembler != null) {
            for (Object ob : lstAssembler) {
                CosAssembler item = (CosAssembler) ob;
                if (item.getAssemblerMain() != null && item.getAssemblerMain().equals(1l)) {
                    item.setMainAssembler(Boolean.TRUE);
                } else {
                    item.setMainAssembler(Boolean.FALSE);
                }

                if (item.getAssemblerSub() != null && item.getAssemblerSub().equals(1l)) {
                    item.setSubAssembler(Boolean.TRUE);
                } else {
                    item.setSubAssembler(Boolean.FALSE);
                }
            }
        }

        CosManufacturerDAO manufactureDAO = new CosManufacturerDAO();
        lstManufacturer = manufactureDAO.findByCosFileId(cosFile.getCosFileId());

        CosCosfileIngreDAO ingredientDAO = new CosCosfileIngreDAO();
        lstIngredient = ingredientDAO.findByCosFileId(cosFile.getCosFileId());

        CosProductPresentatonDAO presentationDAO = new CosProductPresentatonDAO();
        lstPresentations = presentationDAO.getAllActive();
        if (cosFile.getProductPresentation() != null) {
            if (lstPresentations != null && lstPresentations.size() > 0) {
                for (Object ob : lstPresentations) {
                    CosProductPresentaton cos = (CosProductPresentaton) ob;
                    cos.setNameFull(String.format("%s (%s)", cos.getNameVi(), cos.getNameEn()));
                }
            }
            Long[] lstSelectedProduct = GenericDAOHibernate.splitToLong(cosFile.getProductPresentation(), ";");
            if (lstSelectedProduct != null && lstSelectedProduct.length > 0) {
                for (Long id : lstSelectedProduct) {
                    for (Object ob : lstPresentations) {
                        CosProductPresentaton cos = (CosProductPresentaton) ob;
                        if (cos.getProductPresentationId().equals(id)) {
                            cos.setCheck(Boolean.TRUE);
                            break;
                        }
                    }
                }
            }
        }

        CosProductTypeSubDAO productTypeDAO = new CosProductTypeSubDAO();
        lstProductTypes = productTypeDAO.getProductTypeFullContent();
        if (cosFile.getProductType() != null) {
            Long[] lstSelectedProduct = GenericDAOHibernate.splitToLong(cosFile.getProductType(), ";");
            if (lstSelectedProduct != null && lstSelectedProduct.length > 0) {
                for (Long id : lstSelectedProduct) {
                    for (Object ob : lstProductTypes) {
                        CosProductType cos = (CosProductType) ob;
                        if (cos.getProductTypeId().equals(id)) {
                            cos.setCheck(Boolean.TRUE);
                            break;
                        }
                    }
                }
            }
        }


        //linhdx them duong dan file template
        pathTemplate = null;
        //pathTemplate = getPathTemplate(fileId);

    }

    public CosFile getCosFile() {
        return cosFile;
    }

    public void setCosFile(CosFile cosFile) {
        this.cosFile = cosFile;
    }

    public List getLstManufacturer() {
        return lstManufacturer;
    }

    public void setLstManufacturer(List lstManufacturer) {
        this.lstManufacturer = lstManufacturer;
    }

    public List getLstAssembler() {
        return lstAssembler;
    }

    public void setLstAssembler(List lstAssembler) {
        this.lstAssembler = lstAssembler;
    }

    public List getLstIngredient() {
        return lstIngredient;
    }

    public void setLstIngredient(List lstIngredient) {
        this.lstIngredient = lstIngredient;
    }

    public List getLstAttachs() {
        return lstAttachs;
    }

    public void setLstAttachs(List lstAttachs) {
        this.lstAttachs = lstAttachs;
    }

    public List getLstPresentations() {
        return lstPresentations;
    }

    public void setLstPresentations(List lstPresentations) {
        this.lstPresentations = lstPresentations;
    }

    public List getLstProductTypes() {
        return lstProductTypes;
    }

    public void setLstProductTypes(List lstProductTypes) {
        this.lstProductTypes = lstProductTypes;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLeaderSinged() {
        return leaderSinged;
    }

    public void setLeaderSinged(String leaderSinged) {
        this.leaderSinged = leaderSinged;
    }

    public String getPathTemplate() {
        return pathTemplate;
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

    public String getRolesigner() {
        return rolesigner;
    }

    public void setRolesigner(String rolesigner) {
        this.rolesigner = rolesigner;
    }

    public String getSendNo() {
        return sendNo;
    }

    public void setSendNo(String sendNo) {
        this.sendNo = sendNo;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSigner() {
        return signer;
    }

    public void setSigner(String signer) {
        this.signer = signer;
    }

    public int getTypeExport() {
        return typeExport;
    }

    public void setTypeExport(int typeExport) {
        this.typeExport = typeExport;
    }

    public Long getCosmeticPermitId() {
        return cosmeticPermitId;
    }

    public void setCosmeticPermitId(Long cosmeticPermitId) {
        this.cosmeticPermitId = cosmeticPermitId;
    }

    public Long getCosmeticPermitType() {
        return cosmeticPermitType;
    }

    public void setCosmeticPermitType(Long cosmeticPermitType) {
        this.cosmeticPermitType = cosmeticPermitType;
    }

    public Long getCosmeticRejectId() {
        return cosmeticRejectId;
    }

    public void setCosmeticRejectId(Long cosmeticRejectId) {
        this.cosmeticRejectId = cosmeticRejectId;
    }

    public Long getCosmeticRejectType() {
        return cosmeticRejectType;
    }

    public void setCosmeticRejectType(Long cosmeticRejectType) {
        this.cosmeticRejectType = cosmeticRejectType;
    }

    public Long getCosmeticAdditionalId() {
        return cosmeticAdditionalId;
    }

    public void setCosmeticAdditionalId(Long cosmeticAdditionalId) {
        this.cosmeticAdditionalId = cosmeticAdditionalId;
    }

    public Long getCosmeticAdditionalType() {
        return cosmeticAdditionalType;
    }

    public void setCosmeticAdditionalType(Long cosmeticAdditionalType) {
        this.cosmeticAdditionalType = cosmeticAdditionalType;
    }

    private String getPathTemplate(Long fileId) {
        //Get Template
        WorkflowAPI wAPI = new WorkflowAPI();
        Flow flow = wAPI.getFlowByFileId(fileId);
        Long deptId = flow.getDeptId();
        Long procedureId = flow.getObjectId();

        TemplateDAOHE the = new TemplateDAOHE();
        String pathTemplate = the.findPathTemplate(deptId, procedureId,
                Constants.PROCEDURE_TEMPLATE_TYPE.PERMIT);
        return pathTemplate;
    }

    public String toXML() throws JAXBException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(FilesModel.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            StringWriter builder = new StringWriter();
            jaxbMarshaller.marshal(this, builder);
            return builder.toString();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }

    }
}
