/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.cosmetic.Model;

import java.util.ArrayList;
import java.util.List;
import org.zkoss.zul.ListModelList;

/**
 *
 * @author LONG HOANG GIANG
 */
public class ProductPresentationStatus {
    
    private String name;
    private Integer value;

    public ProductPresentationStatus() {
    }
    
    public ProductPresentationStatus(String name, Integer value) {
        setName(name);
        setValue(value);
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
    
}
