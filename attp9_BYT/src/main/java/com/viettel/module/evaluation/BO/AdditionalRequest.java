/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.evaluation.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MrBi
 */
@Entity
@Table(name = "ADDITIONAL_REQUEST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdditionalRequest.findAll", query = "SELECT a FROM AdditionalRequest a"),
    @NamedQuery(name = "AdditionalRequest.findByAdditionalRequestId", query = "SELECT a FROM AdditionalRequest a WHERE a.additionalRequestId = :additionalRequestId"),
    @NamedQuery(name = "AdditionalRequest.findByFileId", query = "SELECT a FROM AdditionalRequest a WHERE a.fileId = :fileId"),
    @NamedQuery(name = "AdditionalRequest.findByCreatorId", query = "SELECT a FROM AdditionalRequest a WHERE a.creatorId = :creatorId"),
    @NamedQuery(name = "AdditionalRequest.findByCreatorName", query = "SELECT a FROM AdditionalRequest a WHERE a.creatorName = :creatorName"),
    @NamedQuery(name = "AdditionalRequest.findByCreateDate", query = "SELECT a FROM AdditionalRequest a WHERE a.createDate = :createDate"),
    @NamedQuery(name = "AdditionalRequest.findByCreateDeptId", query = "SELECT a FROM AdditionalRequest a WHERE a.createDeptId = :createDeptId"),
    @NamedQuery(name = "AdditionalRequest.findByCreateDeptName", query = "SELECT a FROM AdditionalRequest a WHERE a.createDeptName = :createDeptName"),
    @NamedQuery(name = "AdditionalRequest.findByVersion", query = "SELECT a FROM AdditionalRequest a WHERE a.version = :version"),
    @NamedQuery(name = "AdditionalRequest.findByContentType", query = "SELECT a FROM AdditionalRequest a WHERE a.contentType = :contentType"),
    @NamedQuery(name = "AdditionalRequest.findByStatus", query = "SELECT a FROM AdditionalRequest a WHERE a.status = :status"),
    @NamedQuery(name = "AdditionalRequest.findByAttachId", query = "SELECT a FROM AdditionalRequest a WHERE a.attachId = :attachId"),
    @NamedQuery(name = "AdditionalRequest.findByIsActive", query = "SELECT a FROM AdditionalRequest a WHERE a.isActive = :isActive"),
    @NamedQuery(name = "AdditionalRequest.findByRequestType", query = "SELECT a FROM AdditionalRequest a WHERE a.requestType = :requestType")})
public class AdditionalRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    @SequenceGenerator(name = "ADDITIONAL_REQUEST_SEQ", sequenceName = "ADDITIONAL_REQUEST_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ADDITIONAL_REQUEST_SEQ")
    @Column(name = "ADDITIONAL_REQUEST_ID")
    private Long additionalRequestId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 500)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Column(name = "VERSION")
    private Long version;
//    @Lob
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "CONTENT_TYPE")
    private Long contentType;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "ATTACH_ID")
    private Long attachId;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "REQUEST_TYPE")
    private Long requestType;

    public AdditionalRequest() {
    }

    public AdditionalRequest(Long additionalRequestId) {
        this.additionalRequestId = additionalRequestId;
    }

    public Long getAdditionalRequestId() {
        return additionalRequestId;
    }

    public void setAdditionalRequestId(Long additionalRequestId) {
        this.additionalRequestId = additionalRequestId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getContentType() {
        return contentType;
    }

    public void setContentType(Long contentType) {
        this.contentType = contentType;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getAttachId() {
        return attachId;
    }

    public void setAttachId(Long attachId) {
        this.attachId = attachId;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getRequestType() {
        return requestType;
    }

    public void setRequestType(Long requestType) {
        this.requestType = requestType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (additionalRequestId != null ? additionalRequestId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdditionalRequest)) {
            return false;
        }
        AdditionalRequest other = (AdditionalRequest) object;
        if ((this.additionalRequestId == null && other.additionalRequestId != null) || (this.additionalRequestId != null && !this.additionalRequestId.equals(other.additionalRequestId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.evaluation.BO.AdditionalRequest[ additionalRequestId=" + additionalRequestId + " ]";
    }
    
}
