/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.evaluation.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Van Trung
 */
@Entity
@Table(name = "EVALUATION_RECORD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EvaluationRecord.findAll", query = "SELECT e FROM EvaluationRecord e"),
    @NamedQuery(name = "EvaluationRecord.findByEvaluationRecordId", query = "SELECT e FROM EvaluationRecord e WHERE e.evaluationRecordId = :evaluationRecordId"),
    @NamedQuery(name = "EvaluationRecord.findByProcessId", query = "SELECT e FROM EvaluationRecord e WHERE e.processId = :processId"),
    @NamedQuery(name = "EvaluationRecord.findByMainContent", query = "SELECT e FROM EvaluationRecord e WHERE e.mainContent = :mainContent"),
    @NamedQuery(name = "EvaluationRecord.findByFormContent", query = "SELECT e FROM EvaluationRecord e WHERE e.formContent = :formContent"),
    @NamedQuery(name = "EvaluationRecord.findByEvalType", query = "SELECT e FROM EvaluationRecord e WHERE e.evalType = :evalType"),
    @NamedQuery(name = "EvaluationRecord.findByFileId", query = "SELECT e FROM EvaluationRecord e WHERE e.fileId = :fileId"),
    @NamedQuery(name = "EvaluationRecord.findByFileType", query = "SELECT e FROM EvaluationRecord e WHERE e.fileType = :fileType"),
    @NamedQuery(name = "EvaluationRecord.findByCreatorId", query = "SELECT e FROM EvaluationRecord e WHERE e.creatorId = :creatorId"),
    @NamedQuery(name = "EvaluationRecord.findByCreatorName", query = "SELECT e FROM EvaluationRecord e WHERE e.creatorName = :creatorName"),
    @NamedQuery(name = "EvaluationRecord.findByCreateDate", query = "SELECT e FROM EvaluationRecord e WHERE e.createDate = :createDate"),
    @NamedQuery(name = "EvaluationRecord.findByStatus", query = "SELECT e FROM EvaluationRecord e WHERE e.status = :status"),
    @NamedQuery(name = "EvaluationRecord.findByIsActive", query = "SELECT e FROM EvaluationRecord e WHERE e.isActive = :isActive"),
    @NamedQuery(name = "EvaluationRecord.findByAttachId", query = "SELECT e FROM EvaluationRecord e WHERE e.attachId = :attachId")})
public class EvaluationRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @SequenceGenerator(name = "EVALUATION_RECORD_SEQ", sequenceName = "EVALUATION_RECORD_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_RECORD_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVALUATION_RECORD_ID")
    private Long evaluationRecordId;
    @Column(name = "PROCESS_ID")
    private Long processId;
    @Size(max = 2000)
    @Column(name = "MAIN_CONTENT")
    private String mainContent;
    @Size(max = 2000)
    @Column(name = "FORM_CONTENT")
    private String formContent;
    @Column(name = "EVAL_TYPE")
    private Long evalType;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 250)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "ATTACH_ID")
    private Long attachId;

    public EvaluationRecord() {
    }

    public EvaluationRecord(Long evaluationRecordId) {
        this.evaluationRecordId = evaluationRecordId;
    }

    public Long getEvaluationRecordId() {
        return evaluationRecordId;
    }

    public void setEvaluationRecordId(Long evaluationRecordId) {
        this.evaluationRecordId = evaluationRecordId;
    }

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    public String getMainContent() {
        return mainContent;
    }

    public void setMainContent(String mainContent) {
        this.mainContent = mainContent;
    }

    public String getFormContent() {
        return formContent;
    }

    public void setFormContent(String formContent) {
        this.formContent = formContent;
    }

    public Long getEvalType() {
        return evalType;
    }

    public void setEvalType(Long evalType) {
        this.evalType = evalType;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getAttachId() {
        return attachId;
    }

    public void setAttachId(Long attachId) {
        this.attachId = attachId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaluationRecordId != null ? evaluationRecordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EvaluationRecord)) {
            return false;
        }
        EvaluationRecord other = (EvaluationRecord) object;
        if ((this.evaluationRecordId == null && other.evaluationRecordId != null) || (this.evaluationRecordId != null && !this.evaluationRecordId.equals(other.evaluationRecordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.evaluation.EvaluationRecord[ evaluationRecordId=" + evaluationRecordId + " ]";
    }
    
}
