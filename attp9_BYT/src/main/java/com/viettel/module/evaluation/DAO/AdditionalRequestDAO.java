/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.evaluation.DAO;

import com.viettel.module.cosmetic.DAO.*;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.utils.Constants;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author ThanhTM
 */
public class AdditionalRequestDAO extends
        GenericDAOHibernate<AdditionalRequest, Long> {

    public AdditionalRequestDAO() {
        super(AdditionalRequest.class);
    }

    @Override
    public void saveOrUpdate(AdditionalRequest obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }

        getSession().flush();
    }

    @Override
    public AdditionalRequest findById(Long id) {
        Query query = getSession().getNamedQuery(
                "AdditionalRequest.findByAdditionalRequestId");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (AdditionalRequest) result.get(0);
        }
    }

    @Override
    public void delete(AdditionalRequest obj) {
//        obj.setIsActive(Constants.Status.INACTIVE);
//        getSession().saveOrUpdate(obj);
    }

    public List<AdditionalRequest> findAllActiveByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from AdditionalRequest a "
                + "where a.fileId = :fileId "
                + "and a.isActive = :isActive "
                + "order by createDate desc ");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        return result;
    }

    public AdditionalRequest findLastActiveByFileId(Long fileId,Long userId) {
        Query query = getSession().createQuery("select a from AdditionalRequest a "
                + "where a.fileId = :fileId "
                + "and a.creatorId = :userId "
                + "and a.isActive = :isActive "
                + "order by a.additionalRequestId desc ");
        query.setParameter("fileId", fileId);
        query.setParameter("userId", userId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (AdditionalRequest) result.get(0);
        }
    }
}
