/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.evaluation.DAO;

import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.DAO.*;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.utils.Constants;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author ThanhTM
 */
public class EvaluationRecordDAO extends
        GenericDAOHibernate<EvaluationRecord, Long> {

    public EvaluationRecordDAO() {
        super(EvaluationRecord.class);
    }

    @Override
    public void saveOrUpdate(EvaluationRecord obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }

        getSession().flush();
    }

    public List getListFileId(Long fileID) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT e FROM EvaluationRecord e WHERE e.isActive = 1   ");

        docHQL.append(" AND e.fileId = ? ");

        docHQL.append(" order by e.createDate desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, fileID);
        List<EvaluationRecord> listObj;

        listObj = fileQuery.list();

        return listObj;
    }

    // MinhNV - GetListByFileIdAndEvalType 29/04/2015
    public List getListByFileIdAndEvalType(Long fileID, Long evalType) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT e FROM EvaluationRecord e WHERE e.isActive = 1   ");

        docHQL.append(" AND e.fileId = ? ");
        docHQL.append(" AND e.evalType = ? ");
        docHQL.append(" order by e.createDate desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, fileID);
        fileQuery.setParameter(1, evalType);
        List<EvaluationRecord> listObj;

        listObj = fileQuery.list();

        return listObj;
    }

    public EvaluationRecord findLastActiveByFileId(Long fileID, Long userId, Long type) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT e FROM EvaluationRecord e WHERE ( e.isActive = 1  or e.isActive is null ) ");

        docHQL.append(" AND e.fileId = ? ");
        docHQL.append(" AND e.creatorId = ? ");
        if (type != null) {
            docHQL.append(" AND e.evalType = ? ");
        }
        docHQL.append(" order by e.createDate desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, fileID);
        fileQuery.setParameter(1, userId);
        if (type != null) {
            fileQuery.setParameter(2, type);
        }
        List result = fileQuery.list();
        if (result.isEmpty()) {
            return new EvaluationRecord();
        } else {
            return (EvaluationRecord) result.get(0);
        }
    }

    public EvaluationRecord findByFileIdAndEvalTypeMax(Long fileId) {
        String sql = "SELECT e FROM EvaluationRecord e  WHERE e.evaluationRecordId = (SELECT max(e2.evaluationRecordId) FROM EvaluationRecord e2 WHERE e2.fileId = ?) AND e.fileId = ?";
        Query query = session.createQuery(sql);

        query.setParameter(0, fileId);
        query.setParameter(1, fileId);

        List<EvaluationRecord> evaluationRecordList = query.list();
        if (evaluationRecordList.size() > 0) {
            return evaluationRecordList.get(0);
        } else {
            return null;
        }
    }

    public EvaluationRecord getLastEvaluation(Long fileId) {
        Query query = getSession().createQuery("select a from EvaluationRecord a where a.fileId = :fileId "
                + "order by a.evaluationRecordId desc");
        query.setParameter("fileId", fileId);
        List<EvaluationRecord> lst = query.list();
        if (lst.size() > 0) {
            return lst.get(0);
        } else {
            return null;
        }
    }

    public List<EvaluationRecord> getAllEvaluation(Long fileId) {
        Query query = getSession().createQuery("select a from EvaluationRecord a where a.fileId = :fileId "
                + "order by a.evaluationRecordId desc");
        query.setParameter("fileId", fileId);
        List<EvaluationRecord> lst = query.list();
        return lst;
//        if(lst.size()>0){
//            return lst;
//        }else{
//            return new ArrayList<>();
//        }
    }

    public EvaluationRecord findMaxByFileIdAndEvalType(Long fileId, Long evalType) {
        String sql = "SELECT e FROM EvaluationRecord e  WHERE e.evaluationRecordId = (SELECT max(e2.evaluationRecordId) FROM EvaluationRecord e2 WHERE e2.fileId = e.fileId and e2.evalType = e.evalType) AND e.fileId = ? AND e.evalType = ?";
        Query query = session.createQuery(sql);

        query.setParameter(0, fileId);
        query.setParameter(1, evalType);

        List<EvaluationRecord> evaluationRecordList = query.list();
        if (evaluationRecordList.size() > 0) {
            return evaluationRecordList.get(0);
        } else {
            return null;
        }
    }

    public EvaluationRecord findContentByFileIdAndEvalType(Long fileId, Long evalType) {
        String sql = "SELECT e FROM EvaluationRecord e  WHERE e.fileId = ? AND e.evalType = ? ORDER BY createDate DESC";
        Query query = session.createQuery(sql);
        query.setParameter(0, fileId);
        query.setParameter(1, evalType);

        List<EvaluationRecord> evaluationRecordList = query.list();
        if (evaluationRecordList.size() > 0) {
            return evaluationRecordList.get(0);
        } else {
            return null;
        }
    }

    public EvaluationRecord getLastEvaluationByEvalType(Long evalType) {
        Query query = getSession().createQuery("select a from EvaluationRecord a where a.evalType = :evalType "
                + "order by a.evaluationRecordId desc");
        query.setParameter("evalType", evalType);
        List<EvaluationRecord> lst = query.list();
        if (lst.size() > 0) {
            return lst.get(0);
        } else {
            return null;
        }
    }
}
