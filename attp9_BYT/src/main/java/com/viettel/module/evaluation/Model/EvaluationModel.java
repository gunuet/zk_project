/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.evaluation.Model;

import java.util.Date;

import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.module.rapidtest.BO.RtCouncil;
import com.viettel.core.workflow.BO.Process;
import org.zkoss.zul.ListModelList;

/**
 *
 * @author Linhdx
 */
public class EvaluationModel {

    //bat buoc
    private Long userId;
    private String userName;
    private Long deptId;
    private String deptName;
    //co quan kiem tra Department
    private Long checkDeptId;
    private String checkDeptName;

    //Noi dung y kien tham dinh
    private String content;
    private Long resultEvaluation;//Ket qua tham dinh Id
    private String resultEvaluationStr;//Ket qua tham dinh Str
    private Long viceDeptId;

//Ngiep vu XNN
    //Chuc vu nguoi tao
    private Long positionId;
    private String positionName;
    //Ngay het han trong cong van SDBS 
    private Date expirationDate;
    private String resonRequest;
    private Long resultAppraisal;

//Ngiep vu Dat yeu cau nhap khau
//Nghiep vu Trang thiet bi    
    // neu typeProfessional = 1: chuyen vien chinh
    // neu typeProfessional = 2: chuyen vien kiem tra cheo
    private Long typeProfessional;

    //XNN chi dinh co so kiem nghiem
    private ListModelList<RtAssay> evaluationBasicAssay;
    private ListModelList<RtCouncil> evaluationRtCouncil;

    //Ngay hen tra ho so
    private Date returnDate;

    //Ngay hop hoi dong
    private Date meetingDate;

    private Long nextUserId;//Nguoi xu ly tiep theo
    private String nextUserName;//Nguoi xu ly tiep theo

    private Process process;

    public Long getTypeProfessional() {
        return typeProfessional;
    }

    public void setTypeProfessional(Long typeProfessional) {
        this.typeProfessional = typeProfessional;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getResultEvaluation() {
        return resultEvaluation;
    }

    public void setResultEvaluation(Long resultEvaluation) {
        this.resultEvaluation = resultEvaluation;
    }

    public String getResultEvaluationStr() {
        return resultEvaluationStr;
    }

    public void setResultEvaluationStr(String resultEvaluationStr) {
        this.resultEvaluationStr = resultEvaluationStr;
    }

    public ListModelList<RtAssay> getEvaluationBasicAssay() {
        return evaluationBasicAssay;
    }

    public void setEvaluationBasicAssay(ListModelList<RtAssay> evaluationBasicAssay) {
        this.evaluationBasicAssay = evaluationBasicAssay;
    }

    public Long getCheckDeptId() {
        return checkDeptId;
    }

    public void setCheckDeptId(Long checkDeptId) {
        this.checkDeptId = checkDeptId;
    }

    public String getCheckDeptName() {
        return checkDeptName;
    }

    public void setCheckDeptName(String checkDeptName) {
        this.checkDeptName = checkDeptName;
    }

    public Long getViceDeptId() {
        return viceDeptId;
    }

    public void setViceDeptId(Long viceDeptId) {
        this.viceDeptId = viceDeptId;
    }

    public ListModelList<RtCouncil> getEvaluationRtCouncil() {
        return evaluationRtCouncil;
    }

    public void setEvaluationRtCouncil(ListModelList<RtCouncil> evaluationRtCouncil) {
        this.evaluationRtCouncil = evaluationRtCouncil;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Date getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(Date meetingDate) {
        this.meetingDate = meetingDate;
    }

    /**
     * @return the resonRequest
     */
    public String getResonRequest() {
        return resonRequest;
    }

    /**
     * @param resonRequest the resonRequest to set
     */
    public void setResonRequest(String resonRequest) {
        this.resonRequest = resonRequest;
    }

    /**
     * @return the resultAppraisal
     */
    public Long getResultAppraisal() {
        return resultAppraisal;
    }

    /**
     * @param resultAppraisal the resultAppraisal to set
     */
    public void setResultAppraisal(Long resultAppraisal) {
        this.resultAppraisal = resultAppraisal;
    }

    public Long getNextUserId() {
        return nextUserId;
    }

    public void setNextUserId(Long nextUserId) {
        this.nextUserId = nextUserId;
    }

    public String getNextUserName() {
        return nextUserName;
    }

    public void setNextUserName(String nextUserName) {
        this.nextUserName = nextUserName;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

}
