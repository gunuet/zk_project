/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.evaluation.Model;

/**
 *
 * @author Linhdx
 */
public class MessageModel {

    //bat buoc
    private String functionName;
    private Long fileId;
    private Boolean feeUpdate;
    private Long phase;
    private String code;


    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Boolean isFeeUpdate() {
        return feeUpdate;
    }

    public void setFeeUpdate(Boolean feeUpdate) {
        this.feeUpdate = feeUpdate;
    }

    public Long getPhase() {
        return phase;
    }

    public void setPhase(Long phase) {
        this.phase = phase;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
