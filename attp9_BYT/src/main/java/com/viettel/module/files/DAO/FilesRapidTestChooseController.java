/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.files.DAO;

import com.viettel.utils.Constants;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Department;
import com.viettel.core.workflow.DAO.FlowDAOHE;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class FilesRapidTestChooseController extends BaseComposer {

    @Wire
    Window filesRapidTestChooseWnd;
    private Window parentWindow;
    Long deptId, procedureId;

    // label for validating data
    @Wire
    private Label lbTopWarning, lbBottomWarning;

    public FilesRapidTestChooseController() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        loadInfoToForm();

    }

    public void loadInfoToForm() {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        deptId = (Long) arguments.get("deptId");
        procedureId = (Long) arguments.get("procedureId");
    }

    public void createNew() throws IOException {
        Map args = new ConcurrentHashMap();
        args.put("documentTypeCode", Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI);
        setFowardPage(args);
    }
    
    public void changeRequest() throws IOException {
        Map args = new ConcurrentHashMap();
        args.put("documentTypeCode", Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG);
        setFowardPage(args);
    }
    
    public void extendRequest() throws IOException {
        Map args = new ConcurrentHashMap();
        args.put("documentTypeCode", Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN);
        setFowardPage(args);
    }
    public void setFowardPage(Map args){
        args.put("procedureId", procedureId);
        args.put("deptId", deptId);
        args.put("CRUDMode", "CREATE");
        args.put("parentWindow", filesRapidTestChooseWnd);

        createWindow("windowCRUDRapidTest", "/Pages/rapidTest/rapidTestCRUD.zul", args, Window.EMBEDDED);
        filesRapidTestChooseWnd.setVisible(false);
    }
    

    @Listen("onVisible = #filesRapidTestChooseWnd")
    public void onVisible() {
        filesRapidTestChooseWnd.setVisible(true);
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

}
