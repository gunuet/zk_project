/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "APPLIESFEE")
@XmlRootElement
public class AppliesFee implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "APPLIESFEE_SEQ", sequenceName = "APPLIESFEE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APPLIESFEE_SEQ")
    @Column(name = "APPLIESFEEID")
    private Long id;
    @Size(max = 2000)
    @Column(name = "NOTE")
    private String note;   
    @Size(max = 255)
    @Column(name = "AMOUNTFEE")
    private String amountFee; 
    @Column(name = "IS_ACTIVE")
    private Long is_Active;  
    @Column(name = "IMPORTDEVICEFILEID")
    private Long importDeviceFileId;  
    @Column(name = "FEETYPE")
    private String feeType; 
    @Column(name = "IS_DELETED")
    private Long isDeleted; 
    @Override
    public int hashCode() {
        int hash = 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AppliesFee)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getIs_Active() {
        return is_Active;
    }

    public void setIs_Active(Long is_Active) {
        this.is_Active = is_Active;
    }

    public Long getImportDeviceFileId() {
        return importDeviceFileId;
    }

    public void setImportDeviceFileId(Long importDeviceFileId) {
        this.importDeviceFileId = importDeviceFileId;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public Long getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Long isDeleted) {
        this.isDeleted = isDeleted;
    }


    public String getAmountFee() {
        return amountFee;
    }

    public void setAmountFee(String amountFee) {
        this.amountFee = amountFee;
    }

    
    /**
     * @return the position
     */
    
    
}
