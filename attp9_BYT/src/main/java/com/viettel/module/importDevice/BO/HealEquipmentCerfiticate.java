/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "ATTACHHEALEQUIPMENTCERFITICATE")
@XmlRootElement
public class HealEquipmentCerfiticate implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "HEALEQUIPMENTCERFITICATE_SEQ", sequenceName = "HEALEQUIPMENTCERFITICATE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HEALEQUIPMENTCERFITICATE_SEQ")
    @Column(name = "ATTACHEQUIPMENTID")
    private Long id;
    @Size(max = 2000)
    @Column(name = "NOTE")
    private String note;
    @Column(name = "CREATEDATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;    

    @Column(name = "IS_ACTIVE")
    private Long isActive;  
    @Column(name = "CERFITICATENUMBER")
    private String cerfiticateNumber;  
    @Column(name = "HEALEQUIPMENTNAME")
    private String healEquimentName;  
    @Column(name = "ATTACHNAME")
    private String attachName;  
    @Column(name = "PATH")
    private String path;  
    @Override
    public int hashCode() {
        int hash = 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HealEquipmentCerfiticate)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long is_Active) {
        this.isActive = is_Active;
    }

    public String getCerfiticateNumber() {
        return cerfiticateNumber;
    }

    public void setCerfiticateNumber(String cerfiticateNumber) {
        this.cerfiticateNumber = cerfiticateNumber;
    }

    public String getHealEquimentName() {
        return healEquimentName;
    }

    public void setHealEquimentName(String healEquimentName) {
        this.healEquimentName = healEquimentName;
    }

    public String getAttachName() {
        return attachName;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    
    
}
