/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "ID_MEETING")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IdMeeting.findAll", query = "SELECT i FROM IdMeeting i"),
    @NamedQuery(name = "IdMeeting.findById", query = "SELECT i FROM IdMeeting i WHERE i.id = :id"),
    @NamedQuery(name = "IdMeeting.findByPeople", query = "SELECT i FROM IdMeeting i WHERE i.people = :people"),
    @NamedQuery(name = "IdMeeting.findByDay", query = "SELECT i FROM IdMeeting i WHERE i.day = :day"),
    @NamedQuery(name = "IdMeeting.findByStatus", query = "SELECT i FROM IdMeeting i WHERE i.status = :status"),
    @NamedQuery(name = "IdMeeting.findByBooks", query = "SELECT i FROM IdMeeting i WHERE i.books = :books")})
public class IdMeeting implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "ID_MEETING_SEQ", sequenceName = "ID_MEETING_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_MEETING_SEQ")
    @Column(name = "ID")
    private Long id;
    @Size(max = 4000)
    @Column(name = "PEOPLE")
    private String people;
    @Column(name = "DAY")
    @Temporal(TemporalType.DATE)
    private Date day;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "BOOKS")
    private Long books;
    @Column(name = "POSITION")
    private String position;
    @Column(name = "FILE_ID")
    private String fileId;
    @Column(name = "PLACE")
    private String place;
    @Column(name = "RESULT")
    private String result;
    @Column(name = "SORT_ORDER")
    private Long sortOrder;
    @Column(name = "STATUSDOCNAME")
    private String statusDocName;

    public IdMeeting() {
    }

    public IdMeeting(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getBooks() {
        return books;
    }

    public void setBooks(Long books) {
        this.books = books;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IdMeeting)) {
            return false;
        }
        IdMeeting other = (IdMeeting) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importDevice.BO.IdMeeting[ id=" + getId() + " ]";
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the place
     */
    public String getPlace() {
        return place;
    }

    /**
     * @param place the place to set
     */
    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }

    public String getStatusDocName() {
        return statusDocName;
    }

    public void setStatusDocName(String statusDocName) {
        this.statusDocName = statusDocName;
    }

    
    
}
