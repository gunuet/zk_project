/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MrBi
 */
@Entity
@Table(name = "IMPORT_DEVICE_FILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImportDeviceFile.findAll", query = "SELECT i FROM ImportDeviceFile i"),
    @NamedQuery(name = "ImportDeviceFile.findByImportDeviceFileId", query = "SELECT i FROM ImportDeviceFile i WHERE i.importDeviceFileId = :importDeviceFileId"),
    @NamedQuery(name = "ImportDeviceFile.findByFileId", query = "SELECT i FROM ImportDeviceFile i WHERE i.fileId = :fileId"),
    @NamedQuery(name = "ImportDeviceFile.findByNswFileCode", query = "SELECT i FROM ImportDeviceFile i WHERE i.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "ImportDeviceFile.findByDocumentTypeCode", query = "SELECT i FROM ImportDeviceFile i WHERE i.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "ImportDeviceFile.findByImporterName", query = "SELECT i FROM ImportDeviceFile i WHERE i.importerName = :importerName"),
    @NamedQuery(name = "ImportDeviceFile.findByImporterAddress", query = "SELECT i FROM ImportDeviceFile i WHERE i.importerAddress = :importerAddress"),
    @NamedQuery(name = "ImportDeviceFile.findByImporterPhone", query = "SELECT i FROM ImportDeviceFile i WHERE i.importerPhone = :importerPhone"),
    @NamedQuery(name = "ImportDeviceFile.findByImporterFax", query = "SELECT i FROM ImportDeviceFile i WHERE i.importerFax = :importerFax"),
    @NamedQuery(name = "ImportDeviceFile.findByImporterEmail", query = "SELECT i FROM ImportDeviceFile i WHERE i.importerEmail = :importerEmail"),
    @NamedQuery(name = "ImportDeviceFile.findByDirector", query = "SELECT i FROM ImportDeviceFile i WHERE i.director = :director"),
    @NamedQuery(name = "ImportDeviceFile.findByDirectorPhone", query = "SELECT i FROM ImportDeviceFile i WHERE i.directorPhone = :directorPhone"),
    @NamedQuery(name = "ImportDeviceFile.findByDirectorMobile", query = "SELECT i FROM ImportDeviceFile i WHERE i.directorMobile = :directorMobile"),
    @NamedQuery(name = "ImportDeviceFile.findByResponsiblePersonName", query = "SELECT i FROM ImportDeviceFile i WHERE i.responsiblePersonName = :responsiblePersonName"),
    @NamedQuery(name = "ImportDeviceFile.findByResponsiblePersonPhone", query = "SELECT i FROM ImportDeviceFile i WHERE i.responsiblePersonPhone = :responsiblePersonPhone"),
    @NamedQuery(name = "ImportDeviceFile.findByResponsiblePersonMobile", query = "SELECT i FROM ImportDeviceFile i WHERE i.responsiblePersonMobile = :responsiblePersonMobile"),
    @NamedQuery(name = "ImportDeviceFile.findByImportPurpose", query = "SELECT i FROM ImportDeviceFile i WHERE i.importPurpose = :importPurpose"),
    @NamedQuery(name = "ImportDeviceFile.findBySignPlace", query = "SELECT i FROM ImportDeviceFile i WHERE i.signPlace = :signPlace"),
    @NamedQuery(name = "ImportDeviceFile.findBySignDate", query = "SELECT i FROM ImportDeviceFile i WHERE i.signDate = :signDate"),
    @NamedQuery(name = "ImportDeviceFile.findBySignName", query = "SELECT i FROM ImportDeviceFile i WHERE i.signName = :signName"),
    @NamedQuery(name = "ImportDeviceFile.findBySignedData", query = "SELECT i FROM ImportDeviceFile i WHERE i.signedData = :signedData"),
    @NamedQuery(name = "ImportDeviceFile.findByImportType", query = "SELECT i FROM ImportDeviceFile i WHERE i.importType = :importType"),
    @NamedQuery(name = "ImportDeviceFile.findByEquipmentNo", query = "SELECT i FROM ImportDeviceFile i WHERE i.equipmentNo = :equipmentNo"),
    @NamedQuery(name = "ImportDeviceFile.findByPrice", query = "SELECT i FROM ImportDeviceFile i WHERE i.price = :price"),
    @NamedQuery(name = "ImportDeviceFile.findByBookNumber", query = "SELECT i FROM ImportDeviceFile i WHERE i.bookNumber = :bookNumber")})
public class ImportDeviceFile implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "IMPORT_DEVICE_FILE_SEQ", sequenceName = "IMPORT_DEVICE_FILE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMPORT_DEVICE_FILE_SEQ")
    @Column(name = "IMPORT_DEVICE_FILE_ID")
    private Long importDeviceFileId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 62)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Column(name = "TYPE_CREATE")
    private Long typeCreate;//0: Moi 1: Dieu chinh 2 : Gia han
    @Column(name = "IS_CHANGE")
    private Long isChange; //0: Ten hang nuoc SX, 1: Ten to chuc nhap khau
    @Column(name = "OLD_NSW_FILE_CODE")
    private String oldNswFileCode;
    @Column(name = "SEND_BOOK_NUMBER")
    private String sendBookNumber;
    @Size(max = 510)
    @Column(name = "IMPORTER_NAME")
    private String importerName;
    @Size(max = 510)
    @Column(name = "IMPORTER_ADDRESS")
    private String importerAddress;
    @Size(max = 510)
    @Column(name = "IMPORTER_PHONE")
    private String importerPhone;
    @Size(max = 40)
    @Column(name = "IMPORTER_FAX")
    private String importerFax;
    @Size(max = 62)
    @Column(name = "IMPORTER_EMAIL")
    private String importerEmail;
    @Size(max = 510)
    @Column(name = "DIRECTOR")
    private String director;
    @Size(max = 62)
    @Column(name = "DIRECTOR_PHONE")
    private String directorPhone;
    @Size(max = 62)
    @Column(name = "DIRECTOR_MOBILE")
    private String directorMobile;
    @Size(max = 510)
    @Column(name = "RESPONSIBLE_PERSON_NAME")
    private String responsiblePersonName;
    @Size(max = 62)
    @Column(name = "RESPONSIBLE_PERSON_PHONE")
    private String responsiblePersonPhone;
    @Size(max = 62)
    @Column(name = "RESPONSIBLE_PERSON_MOBILE")
    private String responsiblePersonMobile;
    @Size(max = 510)
    @Column(name = "IMPORT_PURPOSE")
    private String importPurpose;
    @Size(max = 510)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.DATE)
    private Date signDate;
    @Size(max = 510)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Size(max = 4000)
    @Column(name = "SIGNED_DATA")
    private String signedData;
    @Column(name = "IMPORT_TYPE")
    private Long importType;
    @Size(max = 510)
    @Column(name = "EQUIPMENT_NO")
    private String equipmentNo;
    @Column(name = "PRICE")
    private Long price;
    @Column(name = "BOOK_NUMBER")
    private Long bookNumber;
    @Column(name = "RECEIVE_ID")
    private Long receiveId;
    @Column(name = "SEND_ID")
    private Long sendId;
    @Column(name = "BOOK_SEND")
    private Long bookSend;
    @Column(name = "BOOK_MEETING")
    private Long bookMeeting;
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Column(name = "GROUP_PRODUCT_ID")
    private String groupProductId;
    @Size(max = 510)
    @Column(name = "GROUP_PRODUCT_NAME")
    private String groupProductName;

    @Column(name = "CFS_DATE")
    @Temporal(TemporalType.DATE)
    private Date cfsDate;

    @Column(name = "ISO_DATE")
    @Temporal(TemporalType.DATE)
    private Date isoDate;

    @Column(name = "UQ_DATE")
    @Temporal(TemporalType.DATE)
    private Date uqDate;

    @Size(max = 2000)
    @Column(name = "COMMENT_CV")
    private String commentCV;

    @Column(name = "PERMIT_NUMBER")
    private String permitNumber;
    @Column(name = "PERMIT_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date permitDate;

    public ImportDeviceFile() {
    }

    public ImportDeviceFile(Long importDeviceFileId) {
        this.importDeviceFileId = importDeviceFileId;
    }

    public Long getImportDeviceFileId() {
        return importDeviceFileId;
    }

    public void setImportDeviceFileId(Long importDeviceFileId) {
        this.importDeviceFileId = importDeviceFileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getImporterName() {
        return importerName;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public void setImporterAddress(String importerAddress) {
        this.importerAddress = importerAddress;
    }

    public String getImporterPhone() {
        return importerPhone;
    }

    public void setImporterPhone(String importerPhone) {
        this.importerPhone = importerPhone;
    }

    public String getImporterFax() {
        return importerFax;
    }

    public void setImporterFax(String importerFax) {
        this.importerFax = importerFax;
    }

    public String getImporterEmail() {
        return importerEmail;
    }

    public void setImporterEmail(String importerEmail) {
        this.importerEmail = importerEmail;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDirectorPhone() {
        return directorPhone;
    }

    public void setDirectorPhone(String directorPhone) {
        this.directorPhone = directorPhone;
    }

    public String getDirectorMobile() {
        return directorMobile;
    }

    public void setDirectorMobile(String directorMobile) {
        this.directorMobile = directorMobile;
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public String getResponsiblePersonPhone() {
        return responsiblePersonPhone;
    }

    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
        this.responsiblePersonPhone = responsiblePersonPhone;
    }

    public String getResponsiblePersonMobile() {
        return responsiblePersonMobile;
    }

    public void setResponsiblePersonMobile(String responsiblePersonMobile) {
        this.responsiblePersonMobile = responsiblePersonMobile;
    }

    public String getImportPurpose() {
        return importPurpose;
    }

    public void setImportPurpose(String importPurpose) {
        this.importPurpose = importPurpose;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignedData() {
        return signedData;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public Long getImportType() {
        return importType;
    }

    public void setImportType(Long importType) {
        this.importType = importType;
    }

    public String getEquipmentNo() {
        return equipmentNo;
    }

    public void setEquipmentNo(String equipmentNo) {
        this.equipmentNo = equipmentNo;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(Long bookNumber) {
        this.bookNumber = bookNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getImportDeviceFileId() != null ? getImportDeviceFileId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImportDeviceFile)) {
            return false;
        }
        ImportDeviceFile other = (ImportDeviceFile) object;
        if ((this.getImportDeviceFileId() == null && other.getImportDeviceFileId() != null) || (this.getImportDeviceFileId() != null && !this.importDeviceFileId.equals(other.importDeviceFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importDevice.BO.ImportDeviceFile[ importDeviceFileId=" + getImportDeviceFileId() + " ]";
    }

    /**
     * @return the receiveId
     */
    public Long getReceiveId() {
        return receiveId;
    }

    /**
     * @param receiveId the receiveId to set
     */
    public void setReceiveId(Long receiveId) {
        this.receiveId = receiveId;
    }

    /**
     * @return the sendId
     */
    public Long getSendId() {
        return sendId;
    }

    /**
     * @return the bookSend
     */
    public Long getBookSend() {
        return bookSend;
    }

    /**
     * @param sendId the sendId to set
     */
    public void setSendId(Long sendId) {
        this.sendId = sendId;
    }

    /**
     * @param bookSend the bookSend to set
     */
    public void setBookSend(Long bookSend) {
        this.bookSend = bookSend;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the groupProductId
     */
    public String getGroupProductId() {
        return groupProductId;
    }

    /**
     * @param groupProductId the groupProductId to set
     */
    public void setGroupProductId(String groupProductId) {
        this.groupProductId = groupProductId;
    }

    /**
     * @return the groupProductName
     */
    public String getGroupProductName() {
        return groupProductName;
    }

    /**
     * @param groupProductName the groupProductName to set
     */
    public void setGroupProductName(String groupProductName) {
        this.groupProductName = groupProductName;
    }

    /**
     * @return the bookMeeting
     */
    public Long getBookMeeting() {
        return bookMeeting;
    }

    /**
     * @param bookMeeting the bookMeeting to set
     */
    public void setBookMeeting(Long bookMeeting) {
        this.bookMeeting = bookMeeting;
    }

    public Date getCfsDate() {
        return cfsDate;
    }

    public void setCfsDate(Date cfsDate) {
        this.cfsDate = cfsDate;
    }

    public Date getIsoDate() {
        return isoDate;
    }

    public void setIsoDate(Date isoDate) {
        this.isoDate = isoDate;
    }

    public Date getUqDate() {
        return uqDate;
    }

    public void setUqDate(Date uqDate) {
        this.uqDate = uqDate;
    }

    public String getCommentCV() {
        return commentCV;
    }

    public void setCommentCV(String commentCV) {
        this.commentCV = commentCV;
    }

    public String getPermitNumber() {
        return permitNumber;
    }

    public void setPermitNumber(String permitNumber) {
        this.permitNumber = permitNumber;
    }

    public Date getPermitDate() {
        return permitDate;
    }

    public void setPermitDate(Date permitDate) {
        this.permitDate = permitDate;
    }

    public Long getTypeCreate() {
        return typeCreate;
    }

    public void setTypeCreate(Long typeCreate) {
        this.typeCreate = typeCreate;
    }

    public Long getIsChange() {
        return isChange;
    }

    public void setIsChange(Long isChange) {
        this.isChange = isChange;
    }

    public String getOldNswFileCode() {
        return oldNswFileCode;
    }

    public void setOldNswFileCode(String oldNswFileCode) {
        this.oldNswFileCode = oldNswFileCode;
    }

    public String getSendBookNumber() {
        return sendBookNumber;
    }

    public void setSendBookNumber(String sendBookNumber) {
        this.sendBookNumber = sendBookNumber;
    }

}
