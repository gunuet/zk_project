/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MrBi
 */
@Entity
@Table(name = "IMPORT_DEVICE_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImportDeviceProduct.findAll", query = "SELECT i FROM ImportDeviceProduct i"),
    @NamedQuery(name = "ImportDeviceProduct.findByProductId", query = "SELECT i FROM ImportDeviceProduct i WHERE i.productId = :productId"),
    @NamedQuery(name = "ImportDeviceProduct.findByFileId", query = "SELECT i FROM ImportDeviceProduct i WHERE i.fileId = :fileId"),
    @NamedQuery(name = "ImportDeviceProduct.findByProductName", query = "SELECT i FROM ImportDeviceProduct i WHERE i.productName = :productName"),
    @NamedQuery(name = "ImportDeviceProduct.findByModel", query = "SELECT i FROM ImportDeviceProduct i WHERE i.model = :model"),
    @NamedQuery(name = "ImportDeviceProduct.findByManufacturer", query = "SELECT i FROM ImportDeviceProduct i WHERE i.manufacturer = :manufacturer"),
    @NamedQuery(name = "ImportDeviceProduct.findByNationalCode", query = "SELECT i FROM ImportDeviceProduct i WHERE i.nationalCode = :nationalCode"),
    @NamedQuery(name = "ImportDeviceProduct.findByNationalName", query = "SELECT i FROM ImportDeviceProduct i WHERE i.nationalName = :nationalName"),
    @NamedQuery(name = "ImportDeviceProduct.findByProduceYear", query = "SELECT i FROM ImportDeviceProduct i WHERE i.produceYear = :produceYear"),
    @NamedQuery(name = "ImportDeviceProduct.findBySendProductPlace", query = "SELECT i FROM ImportDeviceProduct i WHERE i.sendProductPlace = :sendProductPlace"),
    @NamedQuery(name = "ImportDeviceProduct.findBySendProductCode", query = "SELECT i FROM ImportDeviceProduct i WHERE i.sendProductCode = :sendProductCode"),
    @NamedQuery(name = "ImportDeviceProduct.findByGroupProductId", query = "SELECT i FROM ImportDeviceProduct i WHERE i.groupProductId = :groupProductId"),
    @NamedQuery(name = "ImportDeviceProduct.findByGroupProductName", query = "SELECT i FROM ImportDeviceProduct i WHERE i.groupProductName = :groupProductName"),
    @NamedQuery(name = "ImportDeviceProduct.findByDistributor", query = "SELECT i FROM ImportDeviceProduct i WHERE i.distributor = :distributor"),
    @NamedQuery(name = "ImportDeviceProduct.findByNationalDistributorCode", query = "SELECT i FROM ImportDeviceProduct i WHERE i.nationalDistributorCode = :nationalDistributorCode"),
    @NamedQuery(name = "ImportDeviceProduct.findByNationalDistributorName", query = "SELECT i FROM ImportDeviceProduct i WHERE i.nationalDistributorName = :nationalDistributorName")})
public class ImportDeviceProduct implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "IMPORT_DEVICE_PRODUCT_SEQ", sequenceName = "IMPORT_DEVICE_PRODUCT_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMPORT_DEVICE_PRODUCT_SEQ")
    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 510)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 500)
    @Column(name = "MODEL")
    private String model;
    @Size(max = 510)
    @Column(name = "MANUFACTURER")
    private String manufacturer;
    @Size(max = 200)
    @Column(name = "NATIONAL_CODE")
    private String nationalCode;
    @Size(max = 2000)
    @Column(name = "NATIONAL_NAME")
    private String nationalName;
    @Size(max = 500)
    @Column(name = "PRODUCE_YEAR")
    private String produceYear;
    @Size(max = 510)
    @Column(name = "SEND_PRODUCT_PLACE")
    private String sendProductPlace;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_PRODUCT_CODE")
    private Long sendProductCode;
    @Column(name = "GROUP_PRODUCT_ID")
    private String groupProductId;
    @Size(max = 510)
    @Column(name = "GROUP_PRODUCT_NAME")
    private String groupProductName;
    @Size(max = 1020)
    @Column(name = "DISTRIBUTOR")
    private String distributor;
    @Size(max = 400)
    @Column(name = "NATIONAL_DISTRIBUTOR_CODE")
    private String nationalDistributorCode;
    @Size(max = 4000)
    @Column(name = "NATIONAL_DISTRIBUTOR_NAME")
    private String nationalDistributorName;
    @Column(name = "NATIONAL_PRODUCT")
    private String nationalProduct;
    @Column(name = "NATIONAL_PRODUCT_CODE")
    private String nationalProductCode;
    @Column(name = "IS_CATEGORY")
    private Long isCategory;
    @Column(name = "QUANTITY")
    private String quantity;
    @Column(name = "PRODUCT_MANUFACTURE")
    private String productManufacture;
 
    public ImportDeviceProduct() {
    }

    public ImportDeviceProduct(Long productId) {
        this.productId = productId;
    }

    public ImportDeviceProduct(Long productId, Long sendProductCode) {
        this.productId = productId;
        this.sendProductCode = sendProductCode;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getProductName() {
        return removeHtml(productName);
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getModel() {
        return removeHtml(model);
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return removeHtml(manufacturer);
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getNationalCode() {
        if(model !=null && model.contains("phụ lục đính kèm")){
            return null;
        }
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getNationalName() {
        if(model !=null && model.contains("phụ lục đính kèm")){
            return null;
        }
        return nationalName;
        
                        
    }

    public void setNationalName(String nationalName) {
        this.nationalName = nationalName;
    }

    public String getProduceYear() {
        return produceYear;
    }

    public void setProduceYear(String produceYear) {
        this.produceYear = produceYear;
    }

    public String getSendProductPlace() {
        return sendProductPlace;
    }

    public void setSendProductPlace(String sendProductPlace) {
        this.sendProductPlace = sendProductPlace;
    }

    public Long getSendProductCode() {
        return sendProductCode;
    }

    public void setSendProductCode(Long sendProductCode) {
        this.sendProductCode = sendProductCode;
    }

    public String getGroupProductId() {
        return groupProductId;
    }

    public void setGroupProductId(String groupProductId) {
        this.groupProductId = groupProductId;
    }

    public String getGroupProductName() {
        return groupProductName;
    }

    public void setGroupProductName(String groupProductName) {
        this.groupProductName = groupProductName;
    }

    public String getDistributor() {
        return removeHtml(distributor);
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getNationalDistributorCode() {
        if(distributor == null || "".equals(String.valueOf(distributor))){
            return null;
        }
        if(model !=null && model.contains("phụ lục đính kèm")){
            return null;
        }
        return nationalDistributorCode;
    }

    public void setNationalDistributorCode(String nationalDistributorCode) {
        this.nationalDistributorCode = nationalDistributorCode;
    }

    public String getNationalDistributorName() {
        if(distributor == null || "".equals(String.valueOf(distributor))){
            return null;
        }
        if(model !=null && model.contains("phụ lục đính kèm")){
            return null;
        }
        return nationalDistributorName;
    }

    public void setNationalDistributorName(String nationalDistributorName) {
        this.nationalDistributorName = nationalDistributorName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImportDeviceProduct)) {
            return false;
        }
        ImportDeviceProduct other = (ImportDeviceProduct) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importDevice.BO.ImportDeviceProduct[ productId=" + productId + " ]";
    }

    /**
     * @return the nationalProduct
     */
    public String getNationalProduct() {
        if(model != null && model.contains("phụ lục đính kèm")){
            return null;
        }
        if(productManufacture == null || "".equals(productManufacture)){
            return null;
        }
        return nationalProduct;
    }

    /**
     * @param nationalProduct the nationalProduct to set
     */
    public void setNationalProduct(String nationalProduct) {
        this.nationalProduct = nationalProduct;
    }

    /**
     * @return the nationalProductCode
     */
    public String getNationalProductCode() {
        if(model != null && model.contains("phụ lục đính kèm")){
            return null;
        }
        return nationalProductCode;
    }

    /**
     * @param nationalProductCode the nationalProductCode to set
     */
    public void setNationalProductCode(String nationalProductCode) {
        this.nationalProductCode = nationalProductCode;
    }

    public Long getIsCategory() {
        return isCategory;
    }

    public void setIsCategory(Long isCategory) {
        this.isCategory = isCategory;
    }
    public String getProductManufacture() {
        return removeHtml(productManufacture);
    }

    public void setProductManufacture(String productManufacture) {
        this.productManufacture = productManufacture;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    
    private String removeHtml(String htmltext){
        if(htmltext==null){
            return null;
        }
        htmltext = htmltext.replaceAll("\\<.*?>"," ");
        htmltext = htmltext.replaceAll("\\&.*?\\;", " ");
        return htmltext;
    }
}
