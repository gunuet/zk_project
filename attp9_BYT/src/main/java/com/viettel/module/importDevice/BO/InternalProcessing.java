/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "INTERNALPROCESSING")
@XmlRootElement
public class InternalProcessing implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "INTERNALPROCESSING_SEQ", sequenceName = "INTERNALPROCESSING_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INTERNALPROCESSING_SEQ")
    @Column(name = "ID")
    private Long id;
    @Size(max = 2000)
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "CREATEDATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;    

    @Column(name = "IS_ACTIVE")
    private Long is_Active;  
    @Column(name = "MEETINGID")
    private Long meetingId;  
    @Override
    public int hashCode() {
        int hash = 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InternalProcessing)) {
            return false;
        }
        return true;
    }

    /**
     * @return the position
     */
    public String getContent() {
        return content;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    public Date getCreateDate() {
        return createDate;
    }    
    public void setIsActive(Long isActive){
        this.is_Active = isActive;
    }
    public Long getIsActive(){
        return this.is_Active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(Long meetingId) {
        this.meetingId = meetingId;
    }
    
}
