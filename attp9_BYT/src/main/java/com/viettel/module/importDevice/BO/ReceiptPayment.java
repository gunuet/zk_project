/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "RECEIPTPAYMENT")
@XmlRootElement
public class ReceiptPayment implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "RECEIPTPAYMENT_SEQ", sequenceName = "RECEIPTPAYMENT_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RECEIPTPAYMENT_SEQ")
    @Column(name = "RECEIPTPAYMENTID")
    private Long id;
    @Size(max = 255)
    @Column(name = "COMPANYNAME")
    private String companyName;   
    @Column(name = "COMPANYADDRESS")
    private String companyAddress;  
    @Size(max = 20)
    @Column(name = "TAXCODE")
    private String taxCode; 
    @Column(name = "IS_ACTIVE")
    private Long isActive;  
    @Column(name = "BILLID")
    private Long billId;  
    @Column(name = "RECEIPTPAYMENTNUMBER")
    private String receiptPaymentNumber; 
    @Column(name = "CREATEBY")
    private String createBy; 
    @Column(name = "CREATEDATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createDate; 
    @Override
    public int hashCode() {
        int hash = 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReceiptPayment)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }


    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getReceiptPaymentNumber() {
        return receiptPaymentNumber;
    }

    public void setReceiptPaymentNumber(String receiptPaymentNumber) {
        this.receiptPaymentNumber = receiptPaymentNumber;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    
    
}
