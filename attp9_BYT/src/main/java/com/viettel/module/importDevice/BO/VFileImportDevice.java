/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MrBi
 */
@Entity
@Table(name = "V_FILE_IMPORT_DEVICE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VFileImportDevice.findAll", query = "SELECT v FROM VFileImportDevice v"),
    @NamedQuery(name = "VFileImportDevice.findByImportDeviceFileId", query = "SELECT v FROM VFileImportDevice v WHERE v.importDeviceFileId = :importDeviceFileId"),
    @NamedQuery(name = "VFileImportDevice.findByFileId", query = "SELECT v FROM VFileImportDevice v WHERE v.fileId = :fileId"),
    @NamedQuery(name = "VFileImportDevice.findByNswFileCode", query = "SELECT v FROM VFileImportDevice v WHERE v.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "VFileImportDevice.findByDocumentTypeCode", query = "SELECT v FROM VFileImportDevice v WHERE v.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "VFileImportDevice.findByImporterName", query = "SELECT v FROM VFileImportDevice v WHERE v.importerName = :importerName"),
    @NamedQuery(name = "VFileImportDevice.findByImporterAddress", query = "SELECT v FROM VFileImportDevice v WHERE v.importerAddress = :importerAddress"),
    @NamedQuery(name = "VFileImportDevice.findByImporterPhone", query = "SELECT v FROM VFileImportDevice v WHERE v.importerPhone = :importerPhone"),
    @NamedQuery(name = "VFileImportDevice.findByImporterFax", query = "SELECT v FROM VFileImportDevice v WHERE v.importerFax = :importerFax"),
    @NamedQuery(name = "VFileImportDevice.findByImporterEmail", query = "SELECT v FROM VFileImportDevice v WHERE v.importerEmail = :importerEmail"),
    @NamedQuery(name = "VFileImportDevice.findByDirector", query = "SELECT v FROM VFileImportDevice v WHERE v.director = :director"),
    @NamedQuery(name = "VFileImportDevice.findByDirectorPhone", query = "SELECT v FROM VFileImportDevice v WHERE v.directorPhone = :directorPhone"),
    @NamedQuery(name = "VFileImportDevice.findByDirectorMobile", query = "SELECT v FROM VFileImportDevice v WHERE v.directorMobile = :directorMobile"),
    @NamedQuery(name = "VFileImportDevice.findByResponsiblePersonName", query = "SELECT v FROM VFileImportDevice v WHERE v.responsiblePersonName = :responsiblePersonName"),
    @NamedQuery(name = "VFileImportDevice.findByResponsiblePersonPhone", query = "SELECT v FROM VFileImportDevice v WHERE v.responsiblePersonPhone = :responsiblePersonPhone"),
    @NamedQuery(name = "VFileImportDevice.findByResponsiblePersonMobile", query = "SELECT v FROM VFileImportDevice v WHERE v.responsiblePersonMobile = :responsiblePersonMobile"),
    @NamedQuery(name = "VFileImportDevice.findByImportPurpose", query = "SELECT v FROM VFileImportDevice v WHERE v.importPurpose = :importPurpose"),
    @NamedQuery(name = "VFileImportDevice.findBySignPlace", query = "SELECT v FROM VFileImportDevice v WHERE v.signPlace = :signPlace"),
    @NamedQuery(name = "VFileImportDevice.findBySignDate", query = "SELECT v FROM VFileImportDevice v WHERE v.signDate = :signDate"),
    @NamedQuery(name = "VFileImportDevice.findBySignName", query = "SELECT v FROM VFileImportDevice v WHERE v.signName = :signName"),
    @NamedQuery(name = "VFileImportDevice.findBySignedData", query = "SELECT v FROM VFileImportDevice v WHERE v.signedData = :signedData"),
    @NamedQuery(name = "VFileImportDevice.findByImportType", query = "SELECT v FROM VFileImportDevice v WHERE v.importType = :importType"),
    @NamedQuery(name = "VFileImportDevice.findByEquipmentNo", query = "SELECT v FROM VFileImportDevice v WHERE v.equipmentNo = :equipmentNo"),
    @NamedQuery(name = "VFileImportDevice.findByPrice", query = "SELECT v FROM VFileImportDevice v WHERE v.price = :price"),
    @NamedQuery(name = "VFileImportDevice.findByFileType", query = "SELECT v FROM VFileImportDevice v WHERE v.fileType = :fileType"),
    @NamedQuery(name = "VFileImportDevice.findByFileTypeName", query = "SELECT v FROM VFileImportDevice v WHERE v.fileTypeName = :fileTypeName"),
    @NamedQuery(name = "VFileImportDevice.findByFileCode", query = "SELECT v FROM VFileImportDevice v WHERE v.fileCode = :fileCode"),
    @NamedQuery(name = "VFileImportDevice.findByFileName", query = "SELECT v FROM VFileImportDevice v WHERE v.fileName = :fileName"),
    @NamedQuery(name = "VFileImportDevice.findByStatus", query = "SELECT v FROM VFileImportDevice v WHERE v.status = :status"),
    @NamedQuery(name = "VFileImportDevice.findByTaxCode", query = "SELECT v FROM VFileImportDevice v WHERE v.taxCode = :taxCode"),
    @NamedQuery(name = "VFileImportDevice.findByBusinessId", query = "SELECT v FROM VFileImportDevice v WHERE v.businessId = :businessId"),
    @NamedQuery(name = "VFileImportDevice.findByBusinessName", query = "SELECT v FROM VFileImportDevice v WHERE v.businessName = :businessName"),
    @NamedQuery(name = "VFileImportDevice.findByBusinessAddress", query = "SELECT v FROM VFileImportDevice v WHERE v.businessAddress = :businessAddress"),
    @NamedQuery(name = "VFileImportDevice.findByBusinessPhone", query = "SELECT v FROM VFileImportDevice v WHERE v.businessPhone = :businessPhone"),
    @NamedQuery(name = "VFileImportDevice.findByBusinessFax", query = "SELECT v FROM VFileImportDevice v WHERE v.businessFax = :businessFax"),
    @NamedQuery(name = "VFileImportDevice.findByCreateDate", query = "SELECT v FROM VFileImportDevice v WHERE v.createDate = :createDate"),
    @NamedQuery(name = "VFileImportDevice.findByModifyDate", query = "SELECT v FROM VFileImportDevice v WHERE v.modifyDate = :modifyDate"),
    @NamedQuery(name = "VFileImportDevice.findByCreatorId", query = "SELECT v FROM VFileImportDevice v WHERE v.creatorId = :creatorId"),
    @NamedQuery(name = "VFileImportDevice.findByCreatorName", query = "SELECT v FROM VFileImportDevice v WHERE v.creatorName = :creatorName"),
    @NamedQuery(name = "VFileImportDevice.findByCreateDeptId", query = "SELECT v FROM VFileImportDevice v WHERE v.createDeptId = :createDeptId"),
    @NamedQuery(name = "VFileImportDevice.findByCreateDeptName", query = "SELECT v FROM VFileImportDevice v WHERE v.createDeptName = :createDeptName"),
    @NamedQuery(name = "VFileImportDevice.findByIsActive", query = "SELECT v FROM VFileImportDevice v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VFileImportDevice.findByVersion", query = "SELECT v FROM VFileImportDevice v WHERE v.version = :version"),
    @NamedQuery(name = "VFileImportDevice.findByParentFileId", query = "SELECT v FROM VFileImportDevice v WHERE v.parentFileId = :parentFileId"),
    @NamedQuery(name = "VFileImportDevice.findByIsTemp", query = "SELECT v FROM VFileImportDevice v WHERE v.isTemp = :isTemp"),
    @NamedQuery(name = "VFileImportDevice.findByStartDate", query = "SELECT v FROM VFileImportDevice v WHERE v.startDate = :startDate"),
    @NamedQuery(name = "VFileImportDevice.findByNumDayProcess", query = "SELECT v FROM VFileImportDevice v WHERE v.numDayProcess = :numDayProcess"),
    @NamedQuery(name = "VFileImportDevice.findByDeadline", query = "SELECT v FROM VFileImportDevice v WHERE v.deadline = :deadline"),
    @NamedQuery(name = "VFileImportDevice.findByBookNumber", query = "SELECT v FROM VFileImportDevice v WHERE v.bookNumber = :bookNumber")})
public class VFileImportDevice implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @Column(name = "IMPORT_DEVICE_FILE_ID")
    private Long importDeviceFileId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 62)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Size(max = 510)
    @Column(name = "IMPORTER_NAME")
    private String importerName;
    @Size(max = 510)
    @Column(name = "IMPORTER_ADDRESS")
    private String importerAddress;
    @Size(max = 510)
    @Column(name = "IMPORTER_PHONE")
    private String importerPhone;
    @Size(max = 40)
    @Column(name = "IMPORTER_FAX")
    private String importerFax;
    @Size(max = 62)
    @Column(name = "IMPORTER_EMAIL")
    private String importerEmail;
    @Size(max = 510)
    @Column(name = "DIRECTOR")
    private String director;
    @Size(max = 62)
    @Column(name = "DIRECTOR_PHONE")
    private String directorPhone;
    @Size(max = 62)
    @Column(name = "DIRECTOR_MOBILE")
    private String directorMobile;
    @Size(max = 510)
    @Column(name = "RESPONSIBLE_PERSON_NAME")
    private String responsiblePersonName;
    @Size(max = 62)
    @Column(name = "RESPONSIBLE_PERSON_PHONE")
    private String responsiblePersonPhone;
    @Size(max = 62)
    @Column(name = "RESPONSIBLE_PERSON_MOBILE")
    private String responsiblePersonMobile;
    @Size(max = 510)
    @Column(name = "IMPORT_PURPOSE")
    private String importPurpose;
    @Size(max = 510)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.DATE)
    private Date signDate;
    @Size(max = 510)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Size(max = 4000)
    @Column(name = "SIGNED_DATA")
    private String signedData;
    @Column(name = "IMPORT_TYPE")
    private Long importType;
    @Size(max = 510)
    @Column(name = "EQUIPMENT_NO")
    private String equipmentNo;
    @Column(name = "PRICE")
    private Long price;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Size(max = 510)
    @Column(name = "FILE_TYPE_NAME")
    private String fileTypeName;
    @Size(max = 62)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 510)
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "STATUS")
    private Long status;
    @Size(max = 62)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Column(name = "BUSINESS_ID")
    private Long businessId;
    @Size(max = 510)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 1000)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 62)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 62)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 510)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 510)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "VERSION")
    private Long version;
    @Column(name = "PARENT_FILE_ID")
    private Long parentFileId;
    @Column(name = "IS_TEMP")
    private Long isTemp;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "NUM_DAY_PROCESS")
    private Long numDayProcess;
    @Column(name = "DEADLINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;
    @Column(name = "FINISH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;
    @Column(name = "BOOK_NUMBER")
    private Long bookNumber;
    @Column(name = "BOOK_MEETING")
    private Long bookMeeting;
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Column(name = "GROUP_PRODUCT_ID")
    private String groupProductId;
    @Size(max = 510)
    @Column(name = "GROUP_PRODUCT_NAME")
    private String groupProductName;

    @Column(name = "PERMIT_NUMBER")
    private String permitNumber;
    @Column(name = "PERMIT_DATE")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date permitDate;

    @Column(name = "TYPE_CREATE")
    private Long typeCreate;

    public VFileImportDevice() {
    }

    public Long getImportDeviceFileId() {
        return importDeviceFileId;
    }

    public void setImportDeviceFileId(Long importDeviceFileId) {
        this.importDeviceFileId = importDeviceFileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getImporterName() {
        return importerName;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public void setImporterAddress(String importerAddress) {
        this.importerAddress = importerAddress;
    }

    public String getImporterPhone() {
        return importerPhone;
    }

    public void setImporterPhone(String importerPhone) {
        this.importerPhone = importerPhone;
    }

    public String getImporterFax() {
        return importerFax;
    }

    public void setImporterFax(String importerFax) {
        this.importerFax = importerFax;
    }

    public String getImporterEmail() {
        return importerEmail;
    }

    public void setImporterEmail(String importerEmail) {
        this.importerEmail = importerEmail;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDirectorPhone() {
        return directorPhone;
    }

    public void setDirectorPhone(String directorPhone) {
        this.directorPhone = directorPhone;
    }

    public String getDirectorMobile() {
        return directorMobile;
    }

    public void setDirectorMobile(String directorMobile) {
        this.directorMobile = directorMobile;
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public String getResponsiblePersonPhone() {
        return responsiblePersonPhone;
    }

    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
        this.responsiblePersonPhone = responsiblePersonPhone;
    }

    public String getResponsiblePersonMobile() {
        return responsiblePersonMobile;
    }

    public void setResponsiblePersonMobile(String responsiblePersonMobile) {
        this.responsiblePersonMobile = responsiblePersonMobile;
    }

    public String getImportPurpose() {
        return importPurpose;
    }

    public void setImportPurpose(String importPurpose) {
        this.importPurpose = importPurpose;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignedData() {
        return signedData;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public Long getImportType() {
        return importType;
    }

    public void setImportType(Long importType) {
        this.importType = importType;
    }

    public String getEquipmentNo() {
        return equipmentNo;
    }

    public void setEquipmentNo(String equipmentNo) {
        this.equipmentNo = equipmentNo;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getParentFileId() {
        return parentFileId;
    }

    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public Long getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(Long isTemp) {
        this.isTemp = isTemp;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Long getNumDayProcess() {
        return numDayProcess;
    }

    public void setNumDayProcess(Long numDayProcess) {
        this.numDayProcess = numDayProcess;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Long getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(Long bookNumber) {
        this.bookNumber = bookNumber;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the finishDate
     */
    public Date getFinishDate() {
        return finishDate;
    }

    /**
     * @param finishDate the finishDate to set
     */
    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    /**
     * @return the groupProductId
     */
    public String getGroupProductId() {
        return groupProductId;
    }

    /**
     * @param groupProductId the groupProductId to set
     */
    public void setGroupProductId(String groupProductId) {
        this.groupProductId = groupProductId;
    }

    /**
     * @return the groupProductName
     */
    public String getGroupProductName() {
        return groupProductName;
    }

    /**
     * @param groupProductName the groupProductName to set
     */
    public void setGroupProductName(String groupProductName) {
        this.groupProductName = groupProductName;
    }

    /**
     * @return the bookMeeting
     */
    public Long getBookMeeting() {
        return bookMeeting;
    }

    /**
     * @param bookMeeting the bookMeeting to set
     */
    public void setBookMeeting(Long bookMeeting) {
        this.bookMeeting = bookMeeting;
    }

    public String getPermitNumber() {
        return permitNumber;
    }

    public void setPermitNumber(String permitNumber) {
        this.permitNumber = permitNumber;
    }

    public Date getPermitDate() {
        return permitDate;
    }

    public void setPermitDate(Date permitDate) {
        this.permitDate = permitDate;
    }

    public Long getTypeCreate() {
        return typeCreate;
    }

    public void setTypeCreate(Long typeCreate) {
        this.typeCreate = typeCreate;
    }

}
