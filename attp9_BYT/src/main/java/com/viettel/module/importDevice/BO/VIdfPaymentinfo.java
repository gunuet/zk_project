/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.importDevice.BO;

import java.io.Serializable;
import java.lang.Long;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SONY VAIO
 */
@Entity
@Table(name = "V_IDF_PAYMENTINFO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VIdfPaymentinfo.findAll", query = "SELECT v FROM VIdfPaymentinfo v"),
    @NamedQuery(name = "VIdfPaymentinfo.findByFileId", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.fileId = :fileId"),
    @NamedQuery(name = "VIdfPaymentinfo.findByFileType", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.fileType = :fileType"),
    @NamedQuery(name = "VIdfPaymentinfo.findByCreateDate", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.createDate = :createDate"),
    @NamedQuery(name = "VIdfPaymentinfo.findByModifyDate", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.modifyDate = :modifyDate"),
    @NamedQuery(name = "VIdfPaymentinfo.findByTaxCode", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.taxCode = :taxCode"),
    @NamedQuery(name = "VIdfPaymentinfo.findByBusinessName", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.businessName = :businessName"),
    @NamedQuery(name = "VIdfPaymentinfo.findByBusinessAddress", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.businessAddress = :businessAddress"),
    @NamedQuery(name = "VIdfPaymentinfo.findByBusinessPhone", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.businessPhone = :businessPhone"),
    @NamedQuery(name = "VIdfPaymentinfo.findByBusinessFax", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.businessFax = :businessFax"),
    @NamedQuery(name = "VIdfPaymentinfo.findByIsActive", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VIdfPaymentinfo.findByCreatorId", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.creatorId = :creatorId"),
    @NamedQuery(name = "VIdfPaymentinfo.findByCreatorName", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.creatorName = :creatorName"),
    @NamedQuery(name = "VIdfPaymentinfo.findByCreateDeptId", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.createDeptId = :createDeptId"),
    @NamedQuery(name = "VIdfPaymentinfo.findByCreateDeptName", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.createDeptName = :createDeptName"),
    @NamedQuery(name = "VIdfPaymentinfo.findByNswFileCode", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "VIdfPaymentinfo.findByImportDeviceFileId", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.importDeviceFileId = :importDeviceFileId"),
    @NamedQuery(name = "VIdfPaymentinfo.findByImporterName", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.importerName = :importerName"),
    @NamedQuery(name = "VIdfPaymentinfo.findByDocumentTypeCode", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "VIdfPaymentinfo.findByPaymentInfoId", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.paymentInfoId = :paymentInfoId"),
    @NamedQuery(name = "VIdfPaymentinfo.findByStatusfile", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.statusfile = :statusfile"),
    @NamedQuery(name = "VIdfPaymentinfo.findByStatuspayment", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.statuspayment = :statuspayment"),
    @NamedQuery(name = "VIdfPaymentinfo.findByPaymentPerson", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.paymentPerson = :paymentPerson"),
    @NamedQuery(name = "VIdfPaymentinfo.findByPaymentDate", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.paymentDate = :paymentDate"),
    @NamedQuery(name = "VIdfPaymentinfo.findByFeeId", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.feeId = :feeId"),
    @NamedQuery(name = "VIdfPaymentinfo.findByPaymentTypeId", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.paymentTypeId = :paymentTypeId"),
    @NamedQuery(name = "VIdfPaymentinfo.findByCost", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.cost = :cost"),
    @NamedQuery(name = "VIdfPaymentinfo.findByPaymentCode", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.paymentCode = :paymentCode"),
    @NamedQuery(name = "VIdfPaymentinfo.findByPaymentConfirm", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.paymentConfirm = :paymentConfirm"),
    @NamedQuery(name = "VIdfPaymentinfo.findByPaymentActionUser", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.paymentActionUser = :paymentActionUser"),
    @NamedQuery(name = "VIdfPaymentinfo.findByBillId", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.billId = :billId"),
    @NamedQuery(name = "VIdfPaymentinfo.findByBillCode", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.billCode = :billCode"),
    @NamedQuery(name = "VIdfPaymentinfo.findByDateConfirm", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.dateConfirm = :dateConfirm"),
    @NamedQuery(name = "VIdfPaymentinfo.findByNote", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.note = :note"),
    @NamedQuery(name = "VIdfPaymentinfo.findByFeeName", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.feeName = :feeName"),
    @NamedQuery(name = "VIdfPaymentinfo.findByPhase", query = "SELECT v FROM VIdfPaymentinfo v WHERE v.phase = :phase")})
public class VIdfPaymentinfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_ID")
    @Id
    private Long fileId;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.DATE)
    private Date modifyDate;
    @Size(max = 62)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Size(max = 510)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 1000)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 62)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 62)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 510)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 510)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Size(max = 62)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMPORT_DEVICE_FILE_ID")
    private Long importDeviceFileId;
    @Size(max = 510)
    @Column(name = "IMPORTER_NAME")
    private String importerName;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PAYMENT_INFO_ID")
    private Long paymentInfoId;
    @Column(name = "STATUSFILE")
    private Long statusfile;
    @Column(name = "STATUSPAYMENT")
    private Long statuspayment;
    @Size(max = 510)
    @Column(name = "PAYMENT_PERSON")
    private String paymentPerson;
    @Column(name = "PAYMENT_DATE")
    @Temporal(TemporalType.DATE)
    private Date paymentDate;
    @Column(name = "FEE_ID")
    private Long feeId;
    @Column(name = "PAYMENT_TYPE_ID")
    private Long paymentTypeId;
    @Column(name = "COST")
    private Long cost;
    @Size(max = 510)
    @Column(name = "PAYMENT_CODE")
    private String paymentCode;
    @Size(max = 510)
    @Column(name = "PAYMENT_CONFIRM")
    private String paymentConfirm;
    @Size(max = 255)
    @Column(name = "PAYMENT_ACTION_USER")
    private String paymentActionUser;
    @Column(name = "BILL_ID")
    private Long billId;
    @Size(max = 510)
    @Column(name = "BILL_CODE")
    private String billCode;
    @Column(name = "DATE_CONFIRM")
    @Temporal(TemporalType.DATE)
    private Date dateConfirm;
    @Size(max = 1000)
    @Column(name = "NOTE")
    private String note;
    @Size(max = 510)
    @Column(name = "FEE_NAME")
    private String feeName;
    @Column(name = "PHASE")
    private Long phase;

    public VIdfPaymentinfo() {
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getImportDeviceFileId() {
        return importDeviceFileId;
    }

    public void setImportDeviceFileId(Long importDeviceFileId) {
        this.importDeviceFileId = importDeviceFileId;
    }

    public String getImporterName() {
        return importerName;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public Long getPaymentInfoId() {
        return paymentInfoId;
    }

    public void setPaymentInfoId(Long paymentInfoId) {
        this.paymentInfoId = paymentInfoId;
    }

    public Long getStatusfile() {
        return statusfile;
    }

    public void setStatusfile(Long statusfile) {
        this.statusfile = statusfile;
    }

    public Long getStatuspayment() {
        return statuspayment;
    }

    public void setStatuspayment(Long statuspayment) {
        this.statuspayment = statuspayment;
    }

    public String getPaymentPerson() {
        return paymentPerson;
    }

    public void setPaymentPerson(String paymentPerson) {
        this.paymentPerson = paymentPerson;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Long getFeeId() {
        return feeId;
    }

    public void setFeeId(Long feeId) {
        this.feeId = feeId;
    }

    public Long getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getPaymentConfirm() {
        return paymentConfirm;
    }

    public void setPaymentConfirm(String paymentConfirm) {
        this.paymentConfirm = paymentConfirm;
    }

    public String getPaymentActionUser() {
        return paymentActionUser;
    }

    public void setPaymentActionUser(String paymentActionUser) {
        this.paymentActionUser = paymentActionUser;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public Date getDateConfirm() {
        return dateConfirm;
    }

    public void setDateConfirm(Date dateConfirm) {
        this.dateConfirm = dateConfirm;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public Long getPhase() {
        return phase;
    }

    public void setPhase(Long phase) {
        this.phase = phase;
    }
    
}
