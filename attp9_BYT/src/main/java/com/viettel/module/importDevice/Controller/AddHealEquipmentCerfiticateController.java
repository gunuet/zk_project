/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importDevice.BO.HealEquipmentCerfiticate;
import com.viettel.module.importDevice.DAO.HealEquipmentCerfiticateDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author hieuld5
 */
public class AddHealEquipmentCerfiticateController extends BaseComposer {

    @Wire
    Textbox tbCerfiticateNumber, tbHealEquimentName, tbNote;
    @Wire
    Window healEquipmentCefiticateCreate;
    @Wire
    private Vlayout flist;
    private List<Media> listMedia;
    Window windowParent;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        windowParent = (Window) arguments.get("parentWindow");
        listMedia = new ArrayList<>();
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        if (listMedia.isEmpty()) {
            final Media[] medias = event.getMedias();
            for (final Media media : medias) {
                String extFile = media.getName().replace("\"", "");
                if (!FileUtil.validFileType(extFile)) {
                    String sExt = ResourceBundleUtil.getString("extend_file", "config");
                    showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                            Constants.Notification.WARNING);
                    return;
                }

                // luu file vao danh sach file
                listMedia.add(media);

                // layout hien thi ten file va nut "Xóa";
                final Hlayout hl = new Hlayout();
                hl.setSpacing("6px");
                hl.setClass("newFile");
                hl.appendChild(new Label(media.getName()));
                A rm = new A("Xóa");
                rm.addEventListener(Events.ON_CLICK,
                        new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        hl.detach();
                        // xoa file khoi danh sach file
                        listMedia.remove(media);
                    }
                });
                hl.appendChild(rm);
                flist.appendChild(hl);
            }
        } else {
            showNotification("Xóa file đính kèm cũ trước khi upload file đính kèm mới!",
                    Constants.Notification.ERROR);
        }
    }

    @Listen("onClick=#btnClose")
    public void onClose(Event event) {
        healEquipmentCefiticateCreate.detach();
    }

    @Listen("onClick = #btnAdd")
    public void saveHealEquipmentCerfiticate(Event event) {
        try {
            if (validate()) {
                HealEquipmentCerfiticate heal = new HealEquipmentCerfiticate();
                heal.setCerfiticateNumber(tbCerfiticateNumber.getValue());
                heal.setHealEquimentName(tbHealEquimentName.getValue());
                if (tbNote.getValue() != null && !"".equals(tbNote.getValue())) {
                    heal.setNote(tbNote.getValue());
                }
                heal.setAttachName(FileUtil.getSafeFileNameAll(listMedia.get(0).getName()));
                heal.setPath(null);
                heal.setCreateDate(new Date());
                heal.setIsActive(Constants.Status.ACTIVE);
                InputStream inputStream;
                OutputStream outputStream;
                String fileName = heal.getAttachName();
                String folderPath = ResourceBundleUtil.getString("dir_upload");
                FileUtil.mkdirs(folderPath);
                String separator = ResourceBundleUtil.getString("separator");
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                folderPath += separator + dateFormat.format(date);
                File fd = new File(folderPath);
                if (!fd.exists()) {
                    // tạo forlder
                    fd.mkdirs();
                }
                File f = new File(folderPath + separator + fileName);
                if (f.exists()) {
                } else {
                    f.createNewFile();
                }
                // save to hard disk and database
                inputStream = listMedia.get(0).getStreamData();
                outputStream = new FileOutputStream(f);

                byte[] buffer = new byte[1024];
                int len;
                while ((len = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, len);
                }
                outputStream.close();
                inputStream.close();

                HealEquipmentCerfiticateDAO idDao = new HealEquipmentCerfiticateDAO();
                idDao.saveOrUpdate(heal);
                closeView();
            }
        } catch (WrongValueException | IOException ex) {
            LogUtils.addLogDB(ex);
        }
    }

    protected boolean validate() {
        if (tbCerfiticateNumber.getValue() == null || "".equals(tbCerfiticateNumber.getValue())) {
            showNotification("Thông tin số giấy phép chưa được nhập", Constants.Notification.INFO);
            tbCerfiticateNumber.focus();
            return false;
        }
        if (tbHealEquimentName.getValue() == null || "".equals(tbHealEquimentName.getValue())) {
            showNotification("Thông tin Tên trang thiết bị chưa được nhập", Constants.Notification.INFO);
            tbHealEquimentName.focus();
            return false;
        }
        if (listMedia.isEmpty()) {
            showNotification("Chưa có tệp tải lên", Constants.Notification.INFO);
            return false;
        }
        return true;
    }

    protected void closeView() {
        if (windowParent != null) {
            Events.sendEvent("onReload", windowParent, null);
            healEquipmentCefiticateCreate.detach();
        }
    }

}
