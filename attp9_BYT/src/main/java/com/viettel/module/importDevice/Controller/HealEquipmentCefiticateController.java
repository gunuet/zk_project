/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.HealEquipmentCerfiticate;
import com.viettel.module.importDevice.DAO.HealEquipmentCerfiticateDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author hieuld5
 */
public class HealEquipmentCefiticateController extends BaseComposer {

    @Wire
    Textbox tbCerfiticateNumber, tbHealEquimentName, tbNote;
    @Wire
    Listbox fileListbox;
    @Wire
    Paging userPagingBottom;
    @Wire
    Window healEquipmentCefiticate;

    @Override
    public void doAfterCompose(Component window) {
        try {
            super.doAfterCompose(window);
            onSearch(0);
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    private void onSearch(int page) {
        Clients.showBusy("");
        try {
            userPagingBottom.setActivePage(page);
            HealEquipmentCerfiticateDAO idDAO = new HealEquipmentCerfiticateDAO();
            int take = userPagingBottom.getPageSize();
            int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
            PagingListModel plm = idDAO.getReceiptPaymentList(start, take, tbCerfiticateNumber.getValue(), tbHealEquimentName.getValue(), tbNote.getValue());
            userPagingBottom.setTotalSize(plm.getCount());
            if (plm.getCount() == 0) {
                userPagingBottom.setVisible(false);
            } else {
                userPagingBottom.setVisible(true);
            }

            ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
            fileListbox.setModel(lstModel);
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        Long id = Long.parseLong(event.getData().toString());
        HealEquipmentCerfiticateDAO attDAOHE = new HealEquipmentCerfiticateDAO();

        HealEquipmentCerfiticate att = attDAOHE.findHealEquipmentCerfiticateById(id);
        if (att == null) {
            showNotification("Không có biên bản họp !!!");
            return;
        }
        String path = att.getPath();
        File f = new File(path);
        if (f.exists()) {
            Filedownload.save(f, att.getPath());
        } else {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
        }

    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        onSearch(userPagingBottom.getActivePage());
    }

    @Listen("onReload=#healEquipmentCefiticate")
    public void onReload() {
        showNotification("Thao tác thành công!", Constants.Notification.INFO);
        onSearch(userPagingBottom.getActivePage());
    }

    @Listen("onClick=#btnSearch")
    public void search() {
        onSearch(0);
    }

    @Listen("onClick = #btnAdd")
    public void addAttach(Event event) {
        Map args = new ConcurrentHashMap();
        args.put("parentWindow", healEquipmentCefiticate);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/healEquipmentCerfiticate_create.zul", null, args);
        window.doModal();
    }

    @Listen("onDelete = #fileListbox")
    public void onDelete(Event event) {
        if (fileListbox.getSelectedItems() != null && !fileListbox.getSelectedItems().isEmpty()) {
            final Long idDell = Long.parseLong(event.getData().toString());
            EventListener eventListener = new EventListener() {
                @Override
                public void onEvent(Event evt) throws InterruptedException {
                    if (Messagebox.ON_OK.equals(evt.getName())) {
                        HealEquipmentCerfiticateDAO attDAOHE = new HealEquipmentCerfiticateDAO();
                        HealEquipmentCerfiticate att = attDAOHE.findHealEquipmentCerfiticateById(idDell);
                        att.setIsActive(Constants.Status.INACTIVE);
                        attDAOHE.update(att);
                        onSearch(0);
                    }
                }
            };
            Messagebox.show("Xác nhận xóa?", "Thông báo",
                    Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, Messagebox.CANCEL,
                    eventListener);
        } else {
            showNotification("Bạn chưa chọn bản ghi nào!", Constants.Notification.ERROR);
        }
    }
}
