/*
L * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;

import com.sun.crypto.provider.SunJCE;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BusinessController;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.RSAUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.ws.BO.Attachment;
import com.viettel.ws.BO.Body;
import com.viettel.ws.BO.Content;
import com.viettel.ws.BO.Envelope;
import com.viettel.ws.Helper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.spec.KeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.viettel.utils.StringUtils;

/**
 *
 * @author linhdx
 */
public class IdAttachFromNSWController extends BusinessController {

    @Wire
    private Vlayout flist;
    private List<Media> listMedia;
    @Wire
    private Window parentWindow;
    @Wire
    private Button btnAttach, btnCreate;

    private Window win;
    //receive tu popup
    private String AttachmentTypeName;
    private String AttachmentTypeCode;
    private String NswFileCode;
    private String NswProductFileCode;
    private String NswFileType;
    private String Step;
    //tra lai service
    private String AttachmentName;
    private String AttachmentId;
    private String enc;
    private String time;
    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd-hh:mm:ss");

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        listMedia = new ArrayList();

        Execution execution = Executions.getCurrent();
        String nswConfigFileUploadEncrypt = ResourceBundleUtil.getString("nsw_config_file_upload_encrypt", "config");
        if ("true".equals(nswConfigFileUploadEncrypt)) {
            enc = (String) execution.getParameter("enc");
            RSAUtils util = new RSAUtils();
            String param = util.decrypt(enc);
            if (param != null && param.length() > 0) {
                String[] lst = param.split("&");
                for (String obj : lst) {
                    String[] str = obj.split("=");
                    if (str != null && str.length > 0) {
                        if ("AN".equals(str[0])) {
                            AttachmentTypeName = str[1];
                        }
                        if ("AC".equals(str[0])) {
                            AttachmentTypeCode = str[1];
                        }
                        if ("NC".equals(str[0])) {
                            NswFileCode = str[1];
                        }
                        if ("NP".equals(str[0])) {
                            NswProductFileCode = str[1];
                        }
                        if ("DT".equals(str[0])) {
                            time = str[1];
                        }
                        if ("NT".equals(str[0])) {
                            NswFileType = str[1];
                        }
                        if ("ST".equals(str[0])) {
                            Step = str[1];
                        }

                    }
                }
            }
            if (time != null) {
                int timeUpload = Integer.valueOf(ResourceBundleUtil.getString("time_file_upload_encrypt", "config"));
                Date sendDate = formatterDateTime.parse(time);
                Calendar cal = Calendar.getInstance();
                cal.setTime(sendDate);
                cal.add(Calendar.MINUTE, timeUpload);
                Date now = new Date();
                if (now.after(cal.getTime())) {
                    showNotification("Quá thời gian upload!");
                    AttachmentTypeName = null;
                    AttachmentTypeCode = null;
                    NswFileCode = null;
                    NswProductFileCode = null;
                    NswFileType = null;
                    Step = null;
                }
            }

        } else {
            AttachmentTypeName = (String) execution.getParameter("AttachmentTypeName");
            AttachmentTypeCode = (String) execution.getParameter("AttachmentTypeCode");
            NswFileCode = (String) execution.getParameter("NswFileCode");
            NswProductFileCode = (String) execution.getParameter("NswProductFileCode");
            NswFileType = (String) execution.getParameter("NswFileType");
            Step = (String) execution.getParameter("Step");
        }

    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        final Media[] medias = event.getMedias();
        String warning = "";
        // listMedia.clear();
        Long limitSize = Long.parseLong(getConfigValue(
                "limitSizeAttachPerDraft", "config", "209715200"));
        String limit = getConfigValue("limitSizeStr", "config", "30M");
        for (final Media media : medias) {
            Long mediaSize = Integer.valueOf(media.getByteData().length)
                    .longValue();
            if (mediaSize > limitSize) {
                showNotification("Dung lượng file đính kèm không được vượt quá " + limit, Clients.NOTIFICATION_TYPE_INFO);
                return;
            }
            String fileName = media.getName();
            if(fileName !=null && fileName.length() > 50){
                String content = "Độ dài quy định là 50 kí tự, file hiện tại là "+fileName.length()+ " kí tự. Đề nghị DN chỉnh sửa lại";
                showNotification(content, Clients.NOTIFICATION_TYPE_INFO);
                return;
            }
            String extFile = media.getName().replace("\"", "");
            if ("194".equals(AttachmentTypeCode)) {
                //Neu la file phu luc thi chi cho phep pdf
                if (!FileUtil.validFileTypePdf(extFile)) {
                    showNotification("File không đúng định dạng Pdf!" + warning,
                            Constants.Notification.WARNING);
                    return;
                }
            } else {
                if (!FileUtil.validFileTypeHq(extFile)) {
                    showNotification("File không đúng định dạng!" + warning,
                            Constants.Notification.WARNING);
                    return;
                }
            }

            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listMedia.remove(media);
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }

    }

    @Listen("onClick = #btnCreate")
    public void onCreate() throws IOException, Exception {
        Long attachType = !StringUtils.validString(AttachmentTypeCode) ? null : Long.parseLong(AttachmentTypeCode);
        if (listMedia != null && listMedia.size() > 0) {
            Media media = listMedia.get(0);
            Attachs attach;
            if(NswFileType == null){
            	return;
            }
            switch (NswFileType) {
                case "0":
                    //Xac nhan dat yeu cau nhap khau
                    attach = saveImportFileAttach(media, -1L, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE, attachType, AttachmentTypeName);
                    break;
                case "1":
                    //Xet nghiem nhanh
                    attach = saveImportFileAttach(media, -1L, Constants.OBJECT_TYPE.IMPORT_FILE_TYPE, attachType, AttachmentTypeName);
                    break;
                case "2":
                    //trang thiet bi
                    if (NswProductFileCode == null || NswProductFileCode.length() == 0) {
                        attach = saveImportFileAttach(media, -1L, Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE, attachType, AttachmentTypeName);
                    } else {
                        attach = saveImportFileAttach(media, -1L, Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL, attachType, AttachmentTypeName);
                    }
                    break;
                default:
                    attach = new Attachs();

            }
            Helper hp = new Helper();
            Envelope evl;
            if (attach.getAttachId() != null) {
                evl = hp.makeEnvelopeSend(Constants.NSW_TYPE(70L), Constants.NSW_FUNCTION(70L), NswFileCode, NswFileCode, attach.getAttachId().toString(), "TTB");

            } else {
                evl = new Envelope();
            }

            Body bd = new Body();
            Content ct = new Content();
            Attachment attSend = new Attachment();

            attSend.setAttachmentTypeName(AttachmentTypeName);
            attSend.setAttachmentTypeCode(AttachmentTypeCode);
            attSend.setNswFileCode(NswFileCode);
            attSend.setNswProductFileCode(NswProductFileCode);
            attSend.setNswFileType(NswFileType);
            attSend.setStep(Step);
            attSend.setAttachmentName(attach.getAttachName());
            attSend.setAttachmentId(attach.getAttachId().toString());
            ct.setAttachment(attSend);
            bd.setContent(ct);
            evl.setBody(bd);
            Boolean isSend = hp.sendMs(evl);
            if (isSend) {
                showSuccessNotification("Gửi thành công. Hãy đóng của sổ upload");
                btnCreate.setDisabled(true);
            } else {
                showNotification("Gửi thất bại!");
            }
        }
        // Clients.evalJavaScript("closePopup();");
    }

    public Attachs saveImportFileAttach(Media media, Long objectId, Long objectType, 
    		Long attachType, String attachTypeName) throws IOException {
        if (media == null) {
            return null;
        }
        Attachs attach = new Attachs();
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)

        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");
        if (!"/".equals(separator) && !"\\".equals(separator)) {
            separator = "/";
        }
        String fileName = media.getName();
        fileName = FileUtil.getSafeFileNameAll(fileName);
        InputStream inputStream;
        OutputStream outputStream;
        try {

            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(objectId);
            attach.setCreatorId(-1L);
            attach.setCreatorName("Doanh nghiep upload truc tiep");
            attach.setDateCreate(new Date());
            attach.setModifierId(-1L);
            attach.setDateModify(new Date());
            attach.setAttachCat(objectType);//van ban den
            attach.setAttachType(attachType);
            attach.setAttachTypeName(attachTypeName);

            attachDAOHE.saveOrUpdate(attach);
            String subPathDate = String.format("%d%s%d%s%d", Calendar.getInstance().get(Calendar.YEAR), separator, Calendar.getInstance().get(Calendar.MONTH) + 1, separator, Calendar.getInstance().get(Calendar.DATE));
            folderPath += separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_STR + separator + subPathDate + separator + Constants.OBJECT_TYPE_STR.COSMETIC_DOCUMENT_TYPE_CA + separator + objectId;
            folderPath += separator + attach.getAttachId();
            attach.setAttachPath(folderPath);
            attachDAOHE.saveOrUpdate(attach);
            File f = new File(attach.getFullPathFile());
            if (!f.exists()) {
                // tao folder
                if (!f.getParentFile().exists()) {
                    f.getParentFile().mkdirs();
                }
                f.createNewFile();
            }
            //save to hard disk and database
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }

                outputStream.close();


                inputStream.close();


        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        } finally {

        }
        return attach;
    }

    /**
     * @return the parentWindow
     */
    public Window getParentWindow() {
        return parentWindow;
    }

    /**
     * @param parentWindow the parentWindow to set
     */
    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

}
