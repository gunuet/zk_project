/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.importDevice.BO.IdMeeting;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.DAO.ListImportDeviceDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Window;

/**
 *
 * @author hieuld5
 */
public class IdMeetingController extends BusinessController {

    @Wire
    Label ldbookNum, lbday, lbplace;
    @Wire
    Paging userPagingBottom;
    @Wire
    Window idMeeting;
    @Wire
    Listbox lbPeople, lbBBH, lbDeviceFile;
    private Map<String, Object> arguments;
    String[] result = null;
    private Window parentWindow;
    private IdMeeting idm;
    List<VFileImportDevice> lsfile = new ArrayList<>();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        arguments = (Map) Executions.getCurrent().getArg();
        setParentWindow((Window) arguments.get("parentWindow"));
        idm = (IdMeeting) arguments.get("IdMeeting");
        loadData();
    }

    private void loadData() {
        if (idm == null || getParentWindow() == null) {
            return;
        }

        SimpleDateFormat fm = new SimpleDateFormat("dd-MM-yyyy");
        ldbookNum.setValue(idm.getBooks().toString());
        lbday.setValue(fm.format(idm.getDay()));
        lbplace.setValue(idm.getPlace());
        loadPeople();
        loadBBH();
        loadDeviceFile();
    }

    private void loadPeople() {
        List<IdMeeting> lsPeople = new ArrayList<>();

        if (StringUtils.validString(idm.getPeople())) {
            String[] names = idm.getPeople().split(";");
            String[] pos = idm.getPosition().split(";");
            for (int i = 0; i < names.length; i++) {
                IdMeeting idm = new IdMeeting();
                idm.setPeople(names[i]);
                if (i < pos.length) {
                    idm.setPosition(pos[i]);
                }
                lsPeople.add(idm);
            }
        }
        lbPeople.setModel(new ListModelList<>(lsPeople));
    }

    private void loadBBH() {
        AttachDAOHE attDAO = new AttachDAOHE();
        List<Attachs> lsa = attDAO.getByObjectIdAndAttachCatAndAttachType(idm.getId(), Constants.OBJECT_TYPE.ID_MEETING_ATT, Constants.OBJECT_TYPE.ID_MEETING_ATT);
        lbBBH.setModel(new ListModelList<>(lsa));
        lbBBH.renderAll();
    }

    private void loadDeviceFile() {
        ListImportDeviceDAO idDAO = new ListImportDeviceDAO();
        if (StringUtils.validString(idm.getFileId())) {
            String[] id = idm.getFileId().split(";");
            if (StringUtils.validString(idm.getResult())) {
                result = idm.getResult().split(";");
            }
            if (id.length > 0) {
                for (int i = 0; i < id.length; i++) {
                    if (StringUtils.validString(id[i])) {
                        VFileImportDevice f = idDAO.findByFileId(Long.parseLong(id[i]));
//                        f.setSignedData(null);
//                        if (result != null && i < result.length) {
//                            f.setSignedData(result[i]);
//                        }
                        lsfile.add(f);
                    }
                }
            }
        }
        loadFile(0);
//        lbDeviceFile.setModel(new ListModelList<>(lsfile));
//        lbDeviceFile.renderAll();
    }

    public String getResult(int idx) {
        try {
            if (result != null && idx < result.length) {
                return result[idx];
            } else {
                return "";
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return "";
        }
    }

    @Listen("onClose=#idMeeting")
    public void onCloseWindow() {
        if (idMeeting != null) {
            idMeeting.detach();
        }
    }

    private void loadFile(int page) {
        int size = userPagingBottom.getPageSize();
        int start = page * size;

        List lsdv = new ArrayList<>();
        for (int i = start; i < start + size; i++) {
            if (lsfile.size() > i) {
                lsdv.add(lsfile.get(i));
            }
        }
        PagingListModel plm = new PagingListModel(lsdv, new Long(lsfile.size()));
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbDeviceFile.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        loadFile(userPagingBottom.getActivePage());
    }

    @Listen("onViewAtt = #lbBBH")
    public void onViewAtt(Event event) throws Exception {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);
    }

    /**
     * @return the parentWindow
     */
    public Window getParentWindow() {
        return parentWindow;
    }

    /**
     * @param parentWindow the parentWindow to set
     */
    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }
}
