package com.viettel.module.importDevice.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class ImportDeviceAttachFinalFileCRUDController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Window windowAttachFinalFileCRUD;
    private Window parentWindow;
    private Attachs attach;
    // Tep noi dung
    @Wire
    private Vlayout flist1;
    List<Media> listMediaFinalFile;
    private Long fileId;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        fileId = (Long) arguments.get("fileId");
        listMediaFinalFile = new ArrayList();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @param typeSave
     */
    @Listen("onClick = #btnCreate")
    public void onSave() throws IOException {
//        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        AttachDAO base = new AttachDAO();
        if ((listMediaFinalFile != null && listMediaFinalFile.size() > 0)) {
            for (Media media : listMediaFinalFile) {
                base.saveFileAttach(media, fileId, Constants.OBJECT_TYPE.COSMETIC_HO_SO_GOC, null);
            }
        } else {
            showNotification("Phải chọn tệp đính kèm!", Constants.Notification.ERROR);
            return;
        }

        showNotification("Lưu thành công",
                Constants.Notification.INFO);
        windowAttachFinalFileCRUD.onClose();
        Events.sendEvent("onLoadFinalFile", parentWindow, null);
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        if (listMediaFinalFile.isEmpty()) {
            //showWarningMessage("Chưa chọn file đính kèm");
        }
        return true;
    }

    /**
     * Dong cua so khi o dang popup
     */
    @Listen("onClose = #windowAttachFinalFileCRUD")
    public void onClose() {
        windowAttachFinalFileCRUD.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onUpload = #btnAttachFinalFile")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        flist1.getChildren().clear();
        // final Media media = event.getMedia();
        final Media media = event.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            String sExt = ResourceBundleUtil.getString("extend_file", "config");
            showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                    Constants.Notification.WARNING);
        } else {
            // luu file vao danh sach file
            listMediaFinalFile.clear();
            listMediaFinalFile.add(media);

            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            flist1.appendChild(hl);
        }
    }

    public Attachs getAttach() {
        return attach;
    }

    public void setAttach(Attachs attach) {
        this.attach = attach;
    }
}
