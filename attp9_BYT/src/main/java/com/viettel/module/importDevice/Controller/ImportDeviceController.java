/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.zkoss.poi.xssf.usermodel.XSSFSheet;
import org.zkoss.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.poi.hssf.usermodel.HSSFSheet;
import org.zkoss.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.BO.Place;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.BO.CosAdditionalRequest;
import com.viettel.module.cosmetic.BO.CosAssembler;
import com.viettel.module.cosmetic.BO.CosCosfileIngre;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.cosmetic.BO.CosManufacturer;
import com.viettel.module.cosmetic.BO.CosPermit;
import com.viettel.module.cosmetic.Controller.CosmeticAttachDAO;
import com.viettel.module.cosmetic.Controller.CosmeticBaseController;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.cosmetic.Model.CRUDMode;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.DAO.ExportDeviceDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceProductDAO;
import com.viettel.module.importDevice.Model.ExportDeviceModel;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.utils.ValidatorUtil;
import com.viettel.voffice.BO.Business;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.RapidTest.VImportFileRtAttach;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BusinessDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.voffice.model.AttachCategoryModel;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Objects;
import org.zkoss.poi.xssf.usermodel.XSSFCell;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;

/**
 *
 * @author vunt
 */
public class ImportDeviceController extends CosmeticBaseController {

    private final int SAVE = 1;
    private final int SAVE_CLOSE = 2;
    private final int SAVE_COPY = 3;
    private final int RAPID_TEST_FILE_TYPE = 1;
    private long FILE_TYPE;
    private String FILE_TYPE_NAME;
    private long DEPT_ID;

    // Control tim kiem
    @Wire
    Listbox lbProcedure, lbProduct, lbChoosePriceProfile, lbGroupProduct,
            lbImportCategoryProduct, lbTypeCreate, lbIsChange;
    @Wire
    Textbox tbOfficersName, tbOfficersPhone, tbHealthEquipmentNo,
            tbOfficersMobile, tbIntendedUse, tbSignPlace, tbPrice, tbOldNswFile;
    @Wire
    Label tbUnitName, tbUnitAdress, tbUnitPhone, tbUnitFax, tbDirectorName,
            tbDirectorPhone, tbDirectorMobile;
    @Wire
    Textbox tbProductName, tbModel, tbManufactory, tbProceduceYear, tbProductManufacture,
            tbProductId, tbProductIndex, tbSendPortCode, tbSendPortName,
            tbNationalCode, tbNationalName, tbDistributor, tbQuantity,
            tbNationalDistributorCode, tbNationalDistributorName, tbNationalProductCode, tbNationalProduct;
    @Wire
    Checkbox ckModel;
    Set<Listitem> lstPlace;
    Set<Listitem> lstPlaceDistributor;
    Set<Listitem> lstPlaceProduct;
    @Wire
    Datebox dbSignDate, dbUqDate; //dbCfsDate,dbIsoDate,
    @Wire
    Label lbProductWarning, lbTopWarning, lbBottomWarning,
            lbImportExcelWarning, lblTaxCode;
    private List<Category> listImportFileType;
    private List<Media> listMedia, listFileExcel;
    @Wire
    private Vlayout flist, fListImportExcel;

    // Danh sach
    @Wire
    Listbox lbTemplate, finalFileListbox;
    @Wire
    private Listbox lbImportFileType, fileListbox;
    private List<ImportDeviceProduct> lstImportProduct = new ArrayList();

    @Wire
    Window windowCRUDCosmetic;
    @Wire
    private Window parentWindow;
    @Wire
    Button btnAttachPL;
    @Wire
    private Tabbox tb;
    @Wire
    private Tabpanel mainpanel;

    @Wire
    Paging userPagingBottom;
    private String crudMode;
    private String isSave;
    private ListModel model;
    private CosFile cosFile;
    private ImportDeviceFile importDeviceFile;
    private ImportDeviceProduct importDeviceProduct;
    private Files files;
    private Long documentTypeCode;
    private Long typeCreate;
    private Long isChange;

    @Wire
    Textbox txtCertSerial, txtBase64Hash;
    private CosAdditionalRequest additionalRequest;
    private BookDocument bookDocument = new BookDocument();
    private CosEvaluationRecord evaluationRecord = new CosEvaluationRecord();
    private CosPermit permit = new CosPermit();
    private PaymentInfo paymentInfo = new PaymentInfo();
    private String nswFileCode;
    // luu filesId trong truong hop copy
    private Long originalFilesId;
    private Long originalImportFileId;
    private Long originalFileType;
    private List<CosManufacturer> originalLstManufacturer = new ArrayList<>();
    private List<CosAssembler> originalLstAssembler = new ArrayList<>();
    private List<CosCosfileIngre> originalLstCosCosfileIngre = new ArrayList<>();
    private List<ImportDeviceProduct> originalLstImportProduct = new ArrayList<>();
    private String fileSignOut = "";
    private Users user;
    private Business business;
    Map<String, Object> arguments;
    private List<Category> lstCategory;
    List<String> listNationName;
    List<String> listNationCode;
    private String modelstr = "";

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {

        arguments = (Map) Executions.getCurrent().getArg();

        parentWindow = (Window) arguments.get("parentWindow");
        crudMode = (String) arguments.get("CRUDMode");
        listMedia = new ArrayList();
        Long id;
        FilesDAOHE fileDAOHE;
        ImportDeviceFiletDAO idpDAO;
        listNationName = new ArrayList<String>();
        listNationCode = new ArrayList<String>();
        // CosmeticDAO objDAOHE;
        switch (crudMode) {
            case "CREATE":
                files = new Files();
                importDeviceFile = new ImportDeviceFile();
                importDeviceFile.setNswFileCode(null);
                loadBusinessInfo();
                nswFileCode = getAutoNswFileCode(Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI);
                break;
            case "UPDATE":
                id = (Long) arguments.get("id");
                fileDAOHE = new FilesDAOHE();
                files = fileDAOHE.findById(id);
                idpDAO = new ImportDeviceFiletDAO();
                importDeviceFile = idpDAO.findByFileId(id);

                nswFileCode = importDeviceFile.getNswFileCode();
                originalFilesId = files.getFileId();
                originalFileType = files.getFileType();
                originalImportFileId = importDeviceFile.getImportDeviceFileId();
                break;
            case "COPY":
                nswFileCode = getAutoNswFileCode(Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI);
                id = (Long) arguments.get("id");
                fileDAOHE = new FilesDAOHE();
                Files filesOld = fileDAOHE.findById(id);
                files = copy(filesOld);
                idpDAO = new ImportDeviceFiletDAO();
                ImportDeviceFile impdfOld = idpDAO.findByFileId(id);
                importDeviceFile = copy(impdfOld);

                // nswFileCode = importDeviceFile.getNswFileCode();
                originalFilesId = filesOld.getFileId();
                originalFileType = filesOld.getFileType();
                originalImportFileId = importDeviceFile.getImportDeviceFileId();
                break;
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        dbSignDate.setValue(new Date());
        btnAttachPL.setVisible(false);
        listImportFileType = new CategoryDAOHE()
                .findAllCategory(Constants.IMPORT_TYPE.IMPORT_PRODUCT_CATEGORY);
        ListModelArray lstCategoryModel = new ListModelArray(listImportFileType);
        lbImportCategoryProduct.setModel(lstCategoryModel);
        lbImportCategoryProduct.renderAll();
        lbImportCategoryProduct.setSelectedIndex(0);
        loadItemForEdit();
        if ("UPDATE".equals(crudMode) || "COPY".equals(crudMode)) {
            if ("UPDATE".equals(crudMode)) {
                fillFileListbox(originalFilesId);
                fillFinalFileListbox(originalFilesId);
            }
            tbProductId.setDisabled(false);
            ImportDeviceProductDAO idpDAO = new ImportDeviceProductDAO();
            lstImportProduct = idpDAO.findByImportFileId(originalFilesId);
            ListModelArray lstModel = new ListModelArray(lstImportProduct);
            lbProduct.setModel(lstModel);
            if (lstImportProduct.size() > 0) {
                for (ImportDeviceProduct obj : lstImportProduct) {
                    ImportDeviceProduct idp = new ImportDeviceProduct();
                    idp.setProductId(obj.getProductId());
                    idp.setModel(obj.getModel());
                    idp.setManufacturer(obj.getManufacturer());
                    idp.setNationalName(obj.getNationalName());
                    idp.setNationalCode(obj.getNationalCode());
                    idp.setProduceYear(obj.getProduceYear());
                    idp.setSendProductCode(obj.getSendProductCode());
                    idp.setSendProductPlace(obj.getSendProductPlace());
                    idp.setDistributor(obj.getDistributor());
                    idp.setNationalDistributorCode(obj
                            .getNationalDistributorCode());
                    idp.setNationalDistributorName(obj
                            .getNationalDistributorName());
                    idp.setNationalProduct(obj.getNationalProduct());
                    idp.setNationalProductCode(obj.getNationalProductCode());
                    originalLstImportProduct.add(idp);
                }
            }

        }
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Listen("onLoadFinalFile = #windowCRUDCosmetic")
    public void onLoadFinalFile() {
        fillFinalFileListbox(files.getFileId());
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachNew = new ArrayList<Attachs>();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.COSMETIC_HO_SO_GOC);
        if (lstAttach.size() > 0) {
            lstAttachNew.add(lstAttach.get(0));
        }
        this.finalFileListbox.setModel(new ListModelArray(lstAttachNew));
    }

    @Listen("onCheck = #ckModel")
    public void onCheck(Event event) {
        if (ckModel.isChecked()) {
            tbModel.setValue("Theo phụ lục file đính kèm");
            tbModel.setDisabled(true);
            btnAttachPL.setVisible(true);
        } else {
            tbModel.setValue(modelstr);
            tbModel.setDisabled(false);
            btnAttachPL.setVisible(false);
        }
    }

    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    private void loadItemForEdit() {
        WorkflowAPI w = new WorkflowAPI();
        FILE_TYPE = w.getProcedureTypeIdByCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
        FILE_TYPE_NAME = w.getProcedureNameByCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
        if (importDeviceFile.getDocumentTypeCode() != null) {
            typeCreate = importDeviceFile.getTypeCreate();
            if (Objects.equals(typeCreate, Constants.IMPORT.TYPE_CREATE_MOI)) {
                lbTypeCreate.setSelectedIndex(1);
            } else if (Objects.equals(typeCreate, Constants.IMPORT.TYPE_CREATE_DIEUCHINH)) {
                lbTypeCreate.setSelectedIndex(2);
            } else if (Objects.equals(typeCreate, Constants.IMPORT.TYPE_CREATE_GIAHAN)) {
                lbTypeCreate.setSelectedIndex(3);
            }
        }
        if (importDeviceFile.getIsChange() != null) {
            isChange = importDeviceFile.getIsChange();
            if (Objects.equals(isChange, Constants.IMPORT.IS_CHANGE_TENTTB)) {
                lbIsChange.setSelectedIndex(1);
            } else if (Objects.equals(isChange, Constants.IMPORT.IS_CHANGE_HANGNUOC)) {
                lbIsChange.setSelectedIndex(2);
            }
        }
        ImportDeviceFiletDAO idfDAO = new ImportDeviceFiletDAO();
        importDeviceFile = idfDAO.findByFileId(originalFilesId);
        if (importDeviceFile != null) {
            tbProductName.setValue(importDeviceFile.getProductName());
            tbHealthEquipmentNo.setValue(importDeviceFile.getEquipmentNo());
            lblTaxCode.setValue(files.getTaxCode());
            tbUnitName.setValue(importDeviceFile.getImporterName());
            tbUnitAdress.setValue(importDeviceFile.getImporterAddress());
            tbUnitPhone.setValue(importDeviceFile.getImporterPhone());
            tbUnitFax.setValue(importDeviceFile.getImporterFax());
            tbDirectorName.setValue(importDeviceFile.getDirector());
            tbDirectorPhone.setValue(importDeviceFile.getDirectorPhone());
            tbDirectorMobile.setValue(importDeviceFile.getDirectorMobile());
            tbOfficersName
                    .setValue(importDeviceFile.getResponsiblePersonName());
            tbOfficersPhone.setValue(importDeviceFile
                    .getResponsiblePersonPhone());
            tbOfficersMobile.setValue(importDeviceFile
                    .getResponsiblePersonMobile());
            tbIntendedUse.setValue(importDeviceFile.getImportPurpose());
            tbSignPlace.setValue(importDeviceFile.getSignPlace());
            tbPrice.setValue(String.valueOf(importDeviceFile.getPrice()));
            dbUqDate.setValue(importDeviceFile.getUqDate());
            tbOldNswFile.setValue(importDeviceFile.getSendBookNumber());
            if (!"".equals(importDeviceFile.getSignDate())) {
                dbSignDate.setValue(importDeviceFile.getSignDate());
            }
            if (listImportFileType != null && !listImportFileType.isEmpty()) {
                for (int i = 0; i < listImportFileType.size(); i++) {
                    if (importDeviceFile.getGroupProductId().equals(listImportFileType.get(i).getCode()) && importDeviceFile.getGroupProductName().equals(listImportFileType.get(i).getName())) {
                        lbImportCategoryProduct.setSelectedIndex(i);
                        break;
                    }
                }
            }
            if (importDeviceFile.getPrice() == 500000L) {
                lbChoosePriceProfile.setSelectedIndex(0);
            } else if (importDeviceFile.getPrice() == 1000000L) {
                lbChoosePriceProfile.setSelectedIndex(1);
            } else if (importDeviceFile.getPrice() == 3000000L) {
                lbChoosePriceProfile.setSelectedIndex(2);
            } else {
                lbChoosePriceProfile.setSelectedIndex(3);
            }
        }
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException, Exception {
        if (flist.getChildren() == null || flist.getChildren().isEmpty()) {
            showWarningMessage("Chưa chọn file đính kèm");
            return;
        }
        if ("-1".equals(lbImportFileType.getSelectedItem().getValue()
                .toString())) {
            showWarningMessage("Chọn loại tệp đính kèm");
            return;
        }
        if (saveObject() == false) {
            return;
        }
        if (files.getFileId() != null) {

            Long importFileFileType = Long.valueOf(lbImportFileType
                    .getSelectedItem().getValue().toString());
            AttachDAO base = new AttachDAO();

            for (Media media : listMedia) {
                base.saveImportFileAttach(media, files.getFileId(),
                        Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE,
                        importFileFileType);
            }
            fillFileListbox(files.getFileId());
            listMedia.clear();
            flist.getChildren().clear();
            lbTopWarning.setValue("");
            lbBottomWarning.setValue("");
            // showWarningMessage(flist.getChildren().size()+"");
        } else {
            FilesDAOHE filesDAOHE = new FilesDAOHE();
            files = createFile();
            filesDAOHE.saveOrUpdate(files);

            Long importFileFileType = Long.valueOf((String) lbImportFileType
                    .getSelectedItem().getValue());
            AttachDAO base = new AttachDAO();

            for (Media media : listMedia) {
                base.saveImportFileAttach(media, files.getFileId(),
                        Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE,
                        importFileFileType);
            }
            fillFileListbox(files.getFileId());
            listMedia.clear();
            flist.getChildren().clear();
            lbTopWarning.setValue("");
            lbBottomWarning.setValue("");
        }
    }

    @Listen("onClick = #btnOpenAttachFileFinal")
    public void onCreateAttachFileFinal() throws IOException, Exception {

        if (saveObject() == false) {
            return;
        }
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("CRUDMode", "CREATE");
        arguments.put("fileId", files.getFileId());
        arguments.put("importDeviceFileId", importDeviceFile.getImportDeviceFileId());
        arguments.put("parentWindow", windowCRUDCosmetic);
        Window window = createWindow("wdAttachFinalFileCRUD",
                "/Pages/module/importreq/attachFinalDeviceFileCRUD.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    public boolean saveObject() throws IOException, Exception {
        boolean returnValue = true;
        // neu chua co fileID thi them moi
        if (files.getFileId() == null) {
            // Luu thong tin ho so truoc khi tao
            if (isValidatedData()) {
                // createObject();
                FilesDAOHE filesDAOHE = new FilesDAOHE();
                files = createFile();
                filesDAOHE.saveOrUpdate(files);
                return true;
            } else {
                return false;
            }
        }

        return returnValue;
    }

    private void fillFileListbox(Long fileId) {
        CosmeticAttachDAO rDaoHe = new CosmeticAttachDAO();
        List<VImportFileRtAttach> lstImportAttach = rDaoHe
                .findImportAttach(fileId);
        this.fileListbox.setModel(new ListModelArray(lstImportAttach));

    }

    @Listen("onClick=#btnCreateProFile")
    public void onCreateProFile() throws IOException, Exception {
        if (lbImportFileType.getSelectedItem() == null || "-1".equals(lbImportFileType.getSelectedItem().getValue().toString())) {
            showNotification("Chọn loại tệp trước khi thêm từ hồ sơ dùng chung !!!");
            return;
        }
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", windowCRUDCosmetic);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/publicFileDeviceManageListImport.zul", null,
                arguments);
        window.doModal();
    }

    @Listen("onClick=#btnChooseNation")
    public void onChooseNation() throws IOException, Exception {
        createNationalWindow(1);
    }

    @Listen("onClick=#btnChooseNationalDistributor")
    public void onChooseNationDistributor() throws IOException, Exception {
        createNationalWindow(2);
    }

    @Listen("onClick=#btnChooseNationalProduct")
    public void onChooseNationProduct() throws IOException, Exception {
        createNationalWindow(3);
    }

    private void createNationalWindow(int mode) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", windowCRUDCosmetic);
        arguments.put("crudMode", CRUDMode.CREATE);
//        if (mode == 1) {
//            arguments.put("currentItem", null);
//        } else if (mode == 2) {
//            arguments.put("currentItem", null);
//        } else {
//            arguments.put("currentItem", null);
//        }
        arguments.put("Mode", mode);
        createWindow("caCreateWindow",
                "/Pages/admin/place/nation/popupNation.zul", arguments,
                Window.MODAL);

    }

    @Listen("onEdit=#lbTemplate")
    public void onUpdate(Event ev) throws IOException {
        // TblNhanvien nv = (TblNhanvien)
        // lbTemplate.getSelectedItem().getValue();
        // Map args = new ConcurrentHashMap();
        // args.put("id", nv.getNhanvienId());
        // Window window = (Window)
        // Executions.createComponents("/Pages/qlnv/nhanvien/nvCreate.zul",
        // null, args);
        // window.doModal();
    }

    @Listen("onDelete=#lbTemplate")
    public void onDelete(Event ev) throws IOException {
        // TblNhanvien nv = (TblNhanvien)
        // lbTemplate.getSelectedItem().getValue();
        // TblNhanvienDAO nvDAO = new TblNhanvienDAO();
        // nvDAO.delete(nv.getNhanvienId());
        // onSearch();
    }

    // su kien chon tu ho so dung chung
    @Listen("onChooseProFile = #windowCRUDCosmetic")
    public void onChooseProFile(Event event) throws IOException, Exception {
        if (files.getFileId() != null) {
            Map<String, Object> args = (Map<String, Object>) event.getData();
            List list = (List) args.get("documentReceiveProcess");
            if (list == null || list.isEmpty()) {
                return;
            }
            AttachDAOHE attachDAOHE = new AttachDAOHE();
            for (Object list1 : list) {
                AttachCategoryModel Process = (AttachCategoryModel) list1;
                Attachs attach = new Attachs();
                attach.setAttachPath(Process.getAttach().getAttachPath());
                String fileName = (Process.getAttach().getAttachName());
                attach.setAttachName(fileName);
                attach.setIsActive(Constants.Status.ACTIVE);
                attach.setObjectId(files.getFileId());
                attach.setCreatorId(getUserId());
                attach.setCreatorName(Process.getAttach().getCreatorName());
                attach.setDateCreate(new Date());
                attach.setModifierId(getUserId());
                attach.setDateModify(new Date());
                attach.setAttachCat(Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE);
                attach.setAttachType(Long.parseLong(lbImportFileType.getSelectedItem().getValue().toString()));
                attach.setAttachTypeName(lbImportFileType.getSelectedItem().getLabel());
                attach.setAttachDes(Process.getAttach().getAttachDes());
                attachDAOHE.saveOrUpdate(attach);
            }
            fillFileListbox(files.getFileId());
        } else {
            FilesDAOHE filesDAOHE = new FilesDAOHE();
            files = createFile();
            filesDAOHE.saveOrUpdate(files);

            Map<String, Object> args = (Map<String, Object>) event.getData();
            List list = (List) args.get("documentReceiveProcess");

            AttachDAOHE attachDAOHE = new AttachDAOHE();
            for (int idx = 0; idx < list.size(); idx++) {
                AttachCategoryModel Process = (AttachCategoryModel) list
                        .get(idx);
                Attachs attach = new Attachs();
                attach.setAttachPath(Process.getAttach().getAttachPath());
                String fileName = (Process.getAttach().getAttachName());
                attach.setAttachName(fileName);
                attach.setIsActive(Constants.Status.ACTIVE);
                attach.setObjectId(files.getFileId());
                attach.setCreatorId(getUserId());
                attach.setCreatorName(Process.getAttach().getCreatorName());
                attach.setDateCreate(new Date());
                attach.setModifierId(getUserId());
                attach.setDateModify(new Date());
                attach.setAttachCat(Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE);
                attach.setAttachType(Process.getAttach().getAttachType());
                attach.setAttachDes(Process.getAttach().getAttachDes());
                attachDAOHE.saveOrUpdate(attach);
            }
            fillFileListbox(files.getFileId());
        }
        if (saveObject() == false) {
            showWarningMessage("Nhập thông tin hồ sơ trước khi lưu");
        }

    }

    /**
     * Vunt Hàm add dữ liệu product vào list
     *
     */
    private void addProduct(boolean reload) throws Exception {
        modelstr = "";
        if (!validateProduct()) {
            return;
        }
//        if (!"UPDATE".equals(crudMode)) {
//            onSave(SAVE);
//            return;
//        }
        if (!"UPDATE".equals(crudMode)) {
            showNotification("Phải lưu hồ sơ trước !!!");
            return;
        }
        ImportDeviceProduct idp;
        if (tbProductIndex.getValue() != null
                && !tbProductIndex.getValue().isEmpty()
                && lstImportProduct.size() > 0) {
            int index = Integer.parseInt(tbProductIndex.getValue());
            idp = lstImportProduct.get(index);
        } else {
            idp = new ImportDeviceProduct();
        }
        idp.setModel(tbModel.getValue());
        idp.setIsCategory(ckModel.isChecked() ? 1L : 0L);
        idp.setManufacturer(tbManufactory.getValue());
        idp.setNationalName(tbNationalName.getValue());
        idp.setNationalCode(tbNationalCode.getValue());
        idp.setNationalProduct(tbNationalProduct.getValue());
        idp.setNationalProductCode(tbNationalProductCode.getValue());
        idp.setProduceYear(tbProceduceYear.getValue());
        idp.setQuantity(tbQuantity.getValue());
        // idp.setSendProductPlace(tbSendPortName.getValue());
        idp.setDistributor(tbDistributor.getValue());
        idp.setProductManufacture(tbProductManufacture.getValue());
        // idp.setNationalDistributorCode(tbNationalDistributorCode.getValue());
        // idp.setNationalDistributorCode("setNationalDistributorCode");
        idp.setNationalDistributorName(tbNationalDistributorName.getValue());
        idp.setSendProductCode(0L);
        ImportDeviceProductDAO idDAO = new ImportDeviceProductDAO();
        idDAO.saveOrUpdate(idp);
        if (reload) {

            idp.setFileId(files.getFileId());

            tbProductId.setValue(idp.getProductId().toString());
            lstImportProduct.add(idp);
            Integer k = lstImportProduct.size() - 1;
            tbProductIndex.setValue(k.toString());
        } else {
            if (tbProductIndex.getValue() != null
                    && !tbProductIndex.getValue().isEmpty()) {
                // donothing
            } else {
                lstImportProduct.add(idp);
            }

            ListModelArray lstModelManufature = new ListModelArray(lstImportProduct);
            lbProduct.setModel(lstModelManufature);
            lbProductWarning.setValue("");
            resetProduct();
        }
    }

    @Listen("onClick = #btnAddNewProduct")
    public void onAddProduct() throws Exception {
        addProduct(false);
    }

    private boolean validateProduct() {
        if (lbImportCategoryProduct.getSelectedIndex() == 0) {
            lbProductWarning.setValue("Nhóm thiết bị phải được chọn");
            lbImportCategoryProduct.focus();
            return false;
        }

        if (tbModel.getValue() == null
                || tbModel.getValue().trim().length() == 0) {
            lbProductWarning.setValue("Chủng loại không thể để trống");
            tbModel.focus();
            return false;
        }

        if (tbManufactory.getValue() == null
                || tbManufactory.getValue().trim().length() == 0) {
            lbProductWarning.setValue("Hãng sản xuất không thể để trống");
            tbManufactory.focus();
            return false;
        }

        if (tbNationalName.getValue() == null
                || tbNationalName.getValue().trim().length() == 0) {
            lbProductWarning.setValue("Nước sản xuất không thể để trống");
            tbNationalName.focus();
            return false;
        }

        if (tbProceduceYear.getValue() == null
                || tbProceduceYear.getValue().trim().length() == 0) {
            lbProductWarning.setValue("Năm sản xuất không thể để trống");
            tbProceduceYear.focus();
            return false;
        }
        if (tbProductName.getValue() == null
                || tbProductName.getValue().trim().length() == 0) {
            lbProductWarning.setValue("Tên thiết bị không thể để trống");
            tbProductName.focus();
            return false;
        }
        return true;
    }

    private boolean checkProduct() {
        if (lbImportCategoryProduct.getSelectedIndex() == 0) {
            return false;
        }
        if (tbProductName.getValue() == null
                || tbProductName.getValue().trim().length() == 0) {
            return false;
        }
        if (tbModel.getValue() == null
                || tbModel.getValue().trim().length() == 0) {
            return false;
        }

        if (tbManufactory.getValue() == null
                || tbManufactory.getValue().trim().length() == 0) {
            return false;
        }

        if (tbNationalName.getValue() == null
                || tbNationalName.getValue().trim().length() == 0) {
            return false;
        }
        if (tbQuantity.getValue() == null
                || tbQuantity.getValue().trim().length() == 0) {
            return false;
        }
        if (tbProceduceYear.getValue() == null
                || tbProceduceYear.getValue().trim().length() == 0) {
            return false;
        }
        return true;
    }

    private void resetProduct() {
        tbProductId.setValue("");
        tbProductIndex.setValue("");
        tbModel.setValue("");
        ckModel.setChecked(false);
        tbManufactory.setValue("");
        tbNationalName.setValue("");
        tbProceduceYear.setValue("");
        tbQuantity.setValue("");
        tbSendPortName.setValue("");
        tbNationalDistributorCode.setValue("");
        tbNationalDistributorName.setValue("");
        tbDistributor.setValue("");
        tbProductManufacture.setValue("");
        tbNationalProduct.setValue("");
        tbNationalProductCode.setValue("");
        tbNationalCode.setValue("");
        onCheck(null);
    }

    public String getMode(ImportDeviceProduct imd) {
        // String s=imd.getIsCategory()==1L?"-Chi tiết xem file đính kèm":"";
        return imd.getModel();
    }

    @Listen("onEdit=#lbProduct")
    public void onEditAssembler(Event ev) throws IOException {
        if (!"UPDATE".equals(crudMode)) {
            showNotification("Phải lưu hồ sơ trước khi chỉnh sửa !!!");
            return;
        }
        ImportDeviceProduct idp = (ImportDeviceProduct) lbProduct
                .getSelectedItem().getValue();
        tbProductId.setValue(String.valueOf(idp.getProductId()));
        ckModel.setChecked(idp.getIsCategory() == 1L);
        if (idp.getIsCategory() != 1L) {
            modelstr = idp.getModel();
        }
        onCheck(ev);
        tbModel.setValue(idp.getModel());
        tbManufactory.setValue(idp.getManufacturer());
        tbNationalName.setValue(idp.getNationalName());
        tbNationalCode.setValue(idp.getNationalCode());
        tbProceduceYear.setValue(String.valueOf(idp.getProduceYear()));
        tbQuantity.setValue(idp.getQuantity());
        // tbSendPortName.setValue(idp.getSendProductPlace());
        tbProductIndex.setValue(String.valueOf(lbProduct.getSelectedIndex()));
        // tbSendPortCode.setValue(String.valueOf(idp.getSendProductCode()));
        tbDistributor.setValue(idp.getDistributor());
        tbProductManufacture.setValue(idp.getProductManufacture());
        tbNationalDistributorCode.setValue(idp.getNationalDistributorCode());
        tbNationalDistributorName.setValue(idp.getNationalDistributorName());
        tbNationalProduct.setValue(idp.getNationalProduct());
        tbNationalProductCode.setValue(idp.getNationalProductCode());
    }

    @Listen("onDelete=#lbProduct")
    public void onDeleteAssembler(Event ev) throws IOException {
        final ImportDeviceProduct idp = (ImportDeviceProduct) lbProduct
                .getSelectedItem().getValue();
        modelstr = "";
        String message = "Bạn có chắc chắn muốn xóa thiết bị y tế ?";
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

            @Override
            public void onEvent(Event event) {
                if (null != event.getName()) {
                    switch (event.getName()) {
                        case Messagebox.ON_OK:
                            // OK is clicked
                            try {
                                // idp = (ImportDeviceProduct)
                                // lbProduct.getSelectedItem().getValue();
                                lstImportProduct.remove(idp);
                                ListModelArray lstModelAssembler = new ListModelArray(
                                        lstImportProduct);
                                lbProduct.setModel(lstModelAssembler);
                                resetProduct();
                            } catch (Exception ex) {
                                LogUtils.addLogDB(ex);
                                showNotification(
                                        String.format(
                                                Constants.Notification.DELETE_ERROR,
                                                ""),
                                        Constants.Notification.ERROR);
                            }
                            break;
                        case Messagebox.ON_NO:
                            break;
                    }
                }
            }
        });
        //

    }

    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case RAPID_TEST_FILE_TYPE:
                categoryDAOHE = new CategoryDAOHE();
                listImportFileType = categoryDAOHE
                        .findAllCategory(Constants.IMPORT_TYPE.IMPORT_FILE_TYPE);
                lstModel = new ListModelList(listImportFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    public ListModelList getListCategoryProduct() {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        categoryDAOHE = new CategoryDAOHE();
        listImportFileType = categoryDAOHE
                .findAllCategory(Constants.IMPORT_TYPE.IMPORT_PRODUCT_CATEGORY);
        lstModel = new ListModelList(listImportFileType);
        return lstModel;

    }

    public int getSelectedIndexInModel() {
        int selectedItem = 0;
        return selectedItem;
    }

    private void loadBusinessInfo() {
        Long userId = getUserId();
        BusinessDAOHE bhe = new BusinessDAOHE();
        business = bhe.findByUserId(userId);
        UserDAOHE uhe = new UserDAOHE();
        if (business != null) {

            user = uhe.getUserInBusiness(business.getBusinessId());

            files.setBusinessId(business.getBusinessId());
            files.setBusinessName(business.getBusinessName());
            files.setBusinessAddress(business.getBusinessAddress());
            files.setBusinessPhone(business.getBusinessTelephone());
            files.setBusinessFax(business.getBusinessFax());
            files.setTaxCode(business.getBusinessTaxCode());

            importDeviceFile.setDirector(user.getFullName());
            importDeviceFile.setDirectorMobile(user.getTelephone());
            importDeviceFile.setDirectorPhone(user.getTelephone());
            importDeviceFile.setImporterName(business.getBusinessName());
            importDeviceFile.setImporterAddress(business.getBusinessAddress());
            importDeviceFile.setImporterPhone(business.getBusinessTelephone());
            importDeviceFile.setImporterFax(business.getBusinessFax());
        }
    }

    public String getAutoNswFileCode(Long documentTypeCode) {
        //
        String autoNumber;

        ImportDeviceFiletDAO importDeviceFileDAO = new ImportDeviceFiletDAO();
        Long autoNumberL = importDeviceFileDAO.countImportfile();
        autoNumberL += 1L;// Tránh số 0
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

    private Files copy(Files old) {
        Files newObj = new Files();
        newObj.setBusinessAddress(old.getBusinessAddress());
        newObj.setBusinessFax(old.getBusinessFax());
        newObj.setBusinessId(old.getBusinessId());
        newObj.setBusinessName(old.getBusinessName());
        newObj.setBusinessPhone(old.getBusinessPhone());
        newObj.setCreateDate(new Date());
        newObj.setCreateDeptId(old.getCreateDeptId());
        newObj.setCreateDeptName(old.getCreateDeptName());
        newObj.setCreatorId(old.getCreatorId());
        newObj.setCreatorName(old.getCreatorName());
        newObj.setFileCode(old.getFileCode());
        newObj.setFileName(old.getFileName());
        newObj.setFileType(old.getFileType());
        newObj.setFileTypeName(old.getFileTypeName());
        newObj.setFlowId(old.getFlowId());
        newObj.setIsActive(old.getIsActive());
        newObj.setIsTemp(old.getIsTemp());
        newObj.setTaxCode(old.getTaxCode());
        return newObj;
    }

    private ImportDeviceFile copy(ImportDeviceFile old) {
        ImportDeviceFile newObj = new ImportDeviceFile();
        UserToken us = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        UserDAOHE usDAO = new UserDAOHE();
        Users u = usDAO.findById(us.getUserId());
        // newObj.setImportDeviceFileId(old.getImportDeviceFileId());
        newObj.setDirector(us.getUserFullName());
        newObj.setDirectorMobile(u.getTelephone());
        newObj.setDirectorPhone(u.getTelephone());
        newObj.setImporterName(old.getImporterName());
        newObj.setImporterAddress(old.getImporterAddress());
        newObj.setImporterPhone(old.getImporterPhone());
        newObj.setImporterFax(old.getImporterFax());
        newObj.setResponsiblePersonName(old.getResponsiblePersonName());
        newObj.setResponsiblePersonPhone(old.getResponsiblePersonPhone());
        newObj.setResponsiblePersonMobile(old.getResponsiblePersonMobile());
        newObj.setImportPurpose(old.getImportPurpose());
        newObj.setSignPlace(old.getSignPlace());
        newObj.setSignDate(old.getSignDate());
        newObj.setPrice(old.getPrice());
        newObj.setProductName(old.getProductName());
        newObj.setGroupProductId(old.getGroupProductId());
        newObj.setGroupProductName(old.getGroupProductName());
        return newObj;
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();
        String warning = "";
        // listMedia.clear();
        Long limitSize = Long.parseLong(getConfigValue(
                "limitSizeAttachPerDraft", "config", "20971520"));
        String limit = getConfigValue("limitSizeStr", "config", "3M");

        for (final Media media : medias) {
            Long mediaSize = Integer.valueOf(media.getByteData().length)
                    .longValue();
            if (mediaSize > limitSize) {
                showNotification("Dung lượng file đính kèm không được vượt quá " + limit, Clients.NOTIFICATION_TYPE_INFO);
                return;
            }

//            String extFile = media.getFormat().replace("\"", "");
////			String sExt = ResourceBundleUtil.getString("extend_file", "config");
//
//            if (!listatt.contains(extFile)) {
//                warning += "</br>" + media.getName();
//                tag = 1;
//                continue;
//            }
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }
            listMedia.add(media);
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    @Listen("onClick = #btnAttachATT")
    public void onClickATT(Event event) throws Exception {
        if ("COPY".equals(crudMode)) {
            showNotification("Phải lưu hồ sơ trước khi thêm tài liệu !!!");
            return;
        }
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        if (!StringUtils.validString(tbProductId.getValue())) {
            addProduct(true);
        }
        if (!StringUtils.validString(tbProductId.getValue())) {
            return;
        }
        arguments.put("id", Long.parseLong(tbProductId.getValue()));
        arguments.put("parentWindow", windowCRUDCosmetic);
        arguments.put("MODEL", "ATT");
        createWindow("windowAttCRUD",
                "/Pages/module/importreq/product/productAttachs.zul", arguments,
                Window.MODAL);
        lbProduct.setModel(new ListModelList<>(lstImportProduct));
    }

    @Listen("onClick = #btnAttachPL")
    public void onClickPL(Event event) throws Exception {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        if ("COPY".equals(crudMode)) {
            showNotification("Phải lưu hồ sơ trước khi thêm tài liệu !!!");
            return;
        }
        if (!StringUtils.validString(tbProductId.getValue())) {
            addProduct(true);
        }
        if (!StringUtils.validString(tbProductId.getValue())) {
            return;
        }
        arguments.put("id", Long.parseLong(tbProductId.getValue()));
        arguments.put("parentWindow", windowCRUDCosmetic);
        arguments.put("MODEL", "PL");
        createWindow("windowAttCRUD",
                "/Pages/module/importreq/product/productAttachs.zul", arguments,
                Window.MODAL);
        lbProduct.setModel(new ListModelList<>(lstImportProduct));
    }

    public Long getAttachmentSize() {
        if (files != null && files.getFileId() != null) {

            AttachDAOHE daoHE = new AttachDAOHE();
            Long currentSize = 0L;
            List<Attachs> attachs = daoHE.findByObjectId(files.getFileId());
            for (Attachs item : attachs) {
                try {
                    File f = new File(item.getFullPathFile());
                    if (f.exists()) {
                        currentSize += f.length();
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }
            }
            return currentSize;
        }
        return 0L;
    }

    private boolean isValidatedData() {
        if (tbOfficersName.getText().matches("\\s*")) {
            showWarningMessage("Tên cán bộ phụ trách không thể để trống");
            tb.setSelectedPanel(mainpanel);
            tbOfficersName.focus();
            return false;
        }
        if (tbProductName.getText().matches("\\s*")) {
            showWarningMessage("Tên trang thiết bị nhập khẩu không được để trống");
            tbProductName.focus();
            return false;
        }
        if (lbImportCategoryProduct.getSelectedItem() == null) {
            showWarningMessage("Chưa chọn nhóm trang thiết bị");
            lbImportCategoryProduct.focus();
            return false;
        }
        if (tbOfficersPhone.getText().matches("\\s*")) {
            showWarningMessage("Điện thoại liên hệ không thể để trống");
            tb.setSelectedPanel(mainpanel);
            tbOfficersPhone.focus();
            return false;
        }
        if ((ValidatorUtil.validateTextbox(tbOfficersPhone, false, 15,
                ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null
                || tbOfficersPhone.getText().length() <= 9) {
            showWarningMessage("Điện thoại phải là số từ 10 đến 15 ký tự");
            tb.setSelectedPanel(mainpanel);
            tbOfficersPhone.focus();
            return false;
        }
        if (tbOfficersMobile.getText().matches("\\s*")) {
            showWarningMessage("Điện thoại di động không thể để trống");
            tb.setSelectedPanel(mainpanel);
            tbOfficersMobile.focus();
            return false;
        }

        if ((ValidatorUtil.validateTextbox(tbOfficersMobile, false, 15,
                ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null
                || tbOfficersMobile.getText().length() <= 9) {
            showWarningMessage("Điện thoại di động phải là số từ 10 đến 15 ký tự");
            tb.setSelectedPanel(mainpanel);
            tbOfficersMobile.focus();
            return false;
        }
        if (tbHealthEquipmentNo.getText().matches("\\s*")) {
            showWarningMessage("Số công văn của doanh nghiệp không thể để trống");
            tb.setSelectedPanel(mainpanel);
            tbHealthEquipmentNo.focus();
            return false;
        }

        if ((ValidatorUtil.validateTextbox(tbPrice, false, 30,
                ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            showWarningMessage("Giá hồ sơ nhập không đúng");
            tb.setSelectedPanel(mainpanel);
            tbPrice.focus();
            return false;
        }
        if (lbTypeCreate.getSelectedIndex() == 0) {
            showWarningMessage("Chưa chọn loại hồ sơ");
            tb.setSelectedPanel(mainpanel);
            lbTypeCreate.focus();
            return false;
        }
        if (lbTypeCreate.getSelectedIndex() == 2) {
            if (lbIsChange.getSelectedIndex() == 0) {
                showWarningMessage("Chưa chọn loại thay đổi");
                tb.setSelectedPanel(mainpanel);
                lbIsChange.focus();
                return false;
            }
        }

        if (tbIntendedUse.getText().matches("\\s*")) {
            showWarningMessage("Mục đích nhập khẩu không thể để trống");
            tb.setSelectedPanel(mainpanel);
            tbIntendedUse.focus();
            return false;
        }

        if ((ValidatorUtil.validateTextbox(tbIntendedUse, false, 255)) != null) {
            showWarningMessage("Mục đích nhập khẩu nhập không đúng");
            tb.setSelectedPanel(mainpanel);
            tbPrice.focus();
            return false;
        }
        return true;
    }

    protected void showWarningMessage(String message) {
        lbProductWarning.setValue("");
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSave(SAVE);
                break;
            case KeyEvent.F7:
                onSave(SAVE_CLOSE);
                break;
            case KeyEvent.F8:
                onSave(SAVE_COPY);
                break;
        }
    }

    @Listen("onClick = #btnSave, .saveClose")
    public void onSave(int typeSave) throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        try {
            switch (crudMode) {
                case "CREATE": {
                    // nếu có sản phẩm nhưng chưa thêm vào trong list
                    if (tbModel.getValue() != null
                            || tbManufactory.getValue() != null
                            || tbNationalName.getValue() != null
                            || tbProceduceYear.getValue() != null
                            || tbQuantity.getValue() != null) {
                        if (checkProduct()) {
                            String message = String.format(
                                    Constants.Notification.SAVE_COMPLETE_CONFIRM,
                                    Constants.DOCUMENT_TYPE_NAME.IMPORT);
                            Messagebox.show(message, "Xác nhận", Messagebox.YES
                                    | Messagebox.NO, Messagebox.QUESTION,
                                    new org.zkoss.zk.ui.event.EventListener() {
                                @Override
                                public void onEvent(Event event) {
                                    if (null != event.getName()) {
                                        if (event.getName() == null ? Messagebox.ON_YES == null : event.getName().equals(Messagebox.ON_YES)) {
                                            try {
                                                // OK is clicked
                                                addProduct(false);
                                            } catch (Exception ex) {
                                                LogUtils.addLogDB(ex);
                                            }
                                        }
                                    }
                                }

                            });
                        }
                    }

                    String message = String.format(
                            Constants.Notification.SAVE_CONFIRM,
                            Constants.DOCUMENT_TYPE_NAME.FILE);
                    Messagebox.show(message, "Xác nhận", Messagebox.OK
                            | Messagebox.CANCEL, Messagebox.QUESTION,
                            new org.zkoss.zk.ui.event.EventListener() {
                        @Override
                        public void onEvent(Event event) {
                            if (null != event.getName()) {
                                switch (event.getName()) {
                                    case Messagebox.ON_OK:
                                        // OK is clicked
                                        try {
                                            importDeviceFile = new ImportDeviceFile();
                                            createObject();
                                            //onViewListCreate();
                                            crudMode = "UPDATE";
                                            showSuccessNotification("Lưu hồ sơ thành công");
                                        } catch (Exception ex) {
                                            LogUtils.addLogDB(ex);
                                            showNotification(
                                                    String.format(
                                                            Constants.Notification.SAVE_ERROR,
                                                            Constants.DOCUMENT_TYPE_NAME.FILE),
                                                    Constants.Notification.ERROR);
                                        } finally {
                                        }
                                        break;
                                    case Messagebox.ON_NO:
                                        break;
                                }
                            }
                        }
                    });
                    crudMode = "UPDATE";
                    break;
                }
                case "UPDATE": {
                    String message = String.format(
                            Constants.Notification.SAVE_CONFIRM,
                            Constants.DOCUMENT_TYPE_NAME.FILE);
                    Messagebox.show(message, "Xác nhận", Messagebox.OK
                            | Messagebox.CANCEL, Messagebox.QUESTION,
                            new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) {
                            if (null != event.getName()) {
                                switch (event.getName()) {
                                    case Messagebox.ON_OK:
                                        // OK is clicked
                                        try {
                                            //createOldVersion();
                                            createObject();
                                            createFileUpdate();
                                            // onViewListCreate();

                                            showNotification(
                                                    String.format(
                                                            Constants.Notification.UPDATE_SUCCESS,
                                                            Constants.DOCUMENT_TYPE_NAME.FILE),
                                                    Constants.Notification.INFO);

                                        } catch (Exception ex) {
                                            LogUtils.addLogDB(ex);
                                            showNotification(
                                                    String.format(
                                                            Constants.Notification.UPDATE_ERROR,
                                                            Constants.DOCUMENT_TYPE_NAME.FILE),
                                                    Constants.Notification.ERROR);
                                        } finally {
                                        }
                                        break;
                                    case Messagebox.ON_NO:
                                        break;
                                }
                            }
                        }
                    });

                    break;
                }
                case "COPY":
                    // - Gán lại trạng thái update
                    createObject();
                    if ("COPY".equals(crudMode)) {
                        crudMode = "UPDATE";
                    }
                    // Reset lai cac gia tri moi
                    originalFilesId = files.getFileId();
                    originalFileType = files.getFileType();
                    originalImportFileId = importDeviceFile.getImportDeviceFileId();
                    // onViewListCreate();
                    showNotification(String.format(
                            Constants.Notification.UPDATE_SUCCESS,
                            Constants.DOCUMENT_TYPE_NAME.FILE),
                            Constants.Notification.INFO);
                    break;

            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public void onViewListCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", windowCRUDCosmetic);
        arguments.put("deptid", "3409");
        arguments.put("filetype", "HS_IMPORT_DEVICE_KIT");
        arguments.put("menuType", "create");
        onCloseWindow();
        createWindow("cosmeticAll", "/Pages/module/importreq/listImport.zul",
                arguments, Window.EMBEDDED);
    }

    @Listen("onClose=#windowCRUDCosmetic")
    public void onCloseWindow() {
        if (windowCRUDCosmetic != null) {
            windowCRUDCosmetic.detach();
        }
    }

    private void createObject() throws Exception {
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        files = createFile();
        filesDAOHE.saveOrUpdate(files);

        ImportDeviceFiletDAO importDeviceDAO = new ImportDeviceFiletDAO();
        // importDeviceFile = new ImportDeviceFile();
        importDeviceFile = createImportDeviceFile();
        importDeviceDAO.saveOrUpdate(importDeviceFile);

        saveProduct(importDeviceFile.getFileId());
    }

    private Files createFile() throws Exception {
        Date dateNow = new Date();
        files.setCreateDate(dateNow);
        files.setModifyDate(dateNow);
        files.setCreatorId(getUserId());
        files.setCreatorName(getUserFullName());
        files.setCreateDeptId(getDeptId());
        files.setCreateDeptName(getDeptName());
        files.setIsActive(Constants.Status.ACTIVE);
        files.setIsTemp(0L);
        if (files.getNswFileCode() == null) {
            nswFileCode = getAutoNswFileCode(Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI);
            files.setNswFileCode(nswFileCode);
        }
        // set status of file is INIT
        // vunt neu la them moi thi status Bat dau
        // Neu bo sung thi khong cap nhat trang thai
        if (files.getStatus() == null) {
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
        }

        files.setFileType(FILE_TYPE);
        files.setFileTypeName(FILE_TYPE_NAME);
        return files;
    }

    private void createOldVersion() {
        // Tao 1 file verion cu isActive = 0
        Files fileVerion = (files);
        fileVerion.setIsActive(Constants.Status.INACTIVE);
        if (fileVerion.getStatus() == null) {
            fileVerion.setStatus(Constants.PROCESS_STATUS.INITIAL);
        }
        fileVerion.setModifyDate(new Date());
        fileVerion.setParentFileId(files.getFileId());
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        filesDAOHE.saveOrUpdate(fileVerion);
        // Tao cosfile cu co fileId bang file Id vua tao
        ImportDeviceFile idf = copy(importDeviceFile);
        idf.setFileId(fileVerion.getFileId());
        idf.setNswFileCode(importDeviceFile.getNswFileCode());
        ImportDeviceFiletDAO idfDAO = new ImportDeviceFiletDAO();
        idfDAO.saveOrUpdate(idf);
        saveVersionProduct(idf.getFileId());
    }

    private Files createFileUpdate() throws Exception {
        Date dateNow = new Date();
        files.setModifyDate(dateNow);
        return files;
    }

    /**
     * Tao phien ban Nhap product
     *
     * @param ImportFileId
     */
    private void saveVersionProduct(Long versionImportfileId) {
        ImportDeviceProductDAO idpDAO = new ImportDeviceProductDAO();
        for (ImportDeviceProduct obj : originalLstImportProduct) {
            ImportDeviceProduct idp = new ImportDeviceProduct();
            idp.setFileId(versionImportfileId);
            idp.setModel(obj.getModel());
            idp.setManufacturer(obj.getManufacturer());
            idp.setNationalName(obj.getNationalName());
            idp.setNationalCode(obj.getNationalCode());
            idp.setProduceYear(obj.getProduceYear());
            idp.setSendProductCode(obj.getSendProductCode());
            idp.setSendProductPlace(obj.getSendProductPlace());
            idp.setDistributor(obj.getDistributor());
            idp.setNationalDistributorCode(obj.getNationalDistributorCode());
            idp.setNationalDistributorName(obj.getNationalDistributorName());
            idp.setIsCategory(obj.getIsCategory());
            idp.setQuantity(obj.getQuantity());
            idp.setNationalProduct(obj.getNationalProduct());
            idp.setNationalProductCode(obj.getNationalProductCode());
            idp.setProductManufacture(obj.getProductManufacture());
            idpDAO.saveOrUpdate(idp);
        }

    }

    /**
     * vunt Ham tao doi tuong tu form de luu lai
     *
     * @return
     */
    private ImportDeviceFile createImportDeviceFile() {
        if ("COPY".equals(crudMode)) {
            importDeviceFile.setFileId(null);
            importDeviceFile.setNswFileCode(null);
            importDeviceFile.setImportDeviceFileId(null);
            importDeviceFile.setBookNumber(null);
            importDeviceFile.setReceiveId(null);
            importDeviceFile.setSendId(null);
            importDeviceFile.setBookSend(null);
        }
        importDeviceFile.setFileId(files.getFileId());
        documentTypeCode = Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI;
        importDeviceFile.setDocumentTypeCode(documentTypeCode);

        if (importDeviceFile.getNswFileCode() == null) {
            nswFileCode = getAutoNswFileCode(Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI);
            importDeviceFile.setNswFileCode(nswFileCode);
        }
        importDeviceFile.setTypeCreate(Long.valueOf(lbTypeCreate.getSelectedItem().getValue().toString()));
        importDeviceFile.setIsChange(Long.valueOf(lbIsChange.getSelectedItem().getValue().toString()));

        String sendBookNumber = tbOldNswFile.getValue();
        PermitDAO pDAO = new PermitDAO();
        Permit permitOld = pDAO.findTtbCreateNewByReveiveNo(sendBookNumber);
        if (permitOld != null) {
            ImportDeviceFiletDAO dao = new ImportDeviceFiletDAO();
            ImportDeviceFile idOld = dao.findByFileId(permitOld.getFileId());
            importDeviceFile.setOldNswFileCode(idOld.getNswFileCode());
        }

        importDeviceFile.setSendBookNumber(tbOldNswFile.getValue());
        importDeviceFile.setEquipmentNo(tbHealthEquipmentNo.getValue());
        importDeviceFile.setImporterName(tbUnitName.getValue());
        importDeviceFile.setImporterAddress(tbUnitAdress.getValue());
        importDeviceFile.setImporterPhone(tbUnitPhone.getValue());
        importDeviceFile.setImporterFax(tbUnitFax.getValue());
        importDeviceFile.setProductName(tbProductName.getValue());
        importDeviceFile.setGroupProductId(lbImportCategoryProduct.getSelectedItem().getValue().toString());
        importDeviceFile.setGroupProductName(lbImportCategoryProduct.getSelectedItem().getLabel());
        importDeviceFile.setDirector(tbDirectorName.getValue());
        importDeviceFile.setDirectorPhone(tbDirectorPhone.getValue());
        importDeviceFile.setDirectorMobile(tbDirectorMobile.getValue());

        importDeviceFile
                .setResponsiblePersonName(textBoxGetValue(tbOfficersName));
        importDeviceFile
                .setResponsiblePersonPhone(textBoxGetValue(tbOfficersPhone));
        importDeviceFile
                .setResponsiblePersonMobile(textBoxGetValue(tbOfficersMobile));

        importDeviceFile.setImportPurpose(textBoxGetValue(tbIntendedUse));
        importDeviceFile.setSignPlace(textBoxGetValue(tbSignPlace));
        importDeviceFile.setSignDate(dbSignDate.getValue());
//            importDeviceFile.setIsoDate(dbIsoDate.getValue());
//            importDeviceFile.setCfsDate(dbCfsDate.getValue());
        importDeviceFile.setUqDate(dbUqDate.getValue());
        long value = Long.valueOf(lbChoosePriceProfile.getSelectedItem()
                .getValue().toString());
        if (value == 1) {
            importDeviceFile.setPrice(500000L);
        } else if (value == 2) {
            importDeviceFile.setPrice(1000000L);
        } else if (value == 3) {
            importDeviceFile.setPrice(3000000L);
        } else {
            importDeviceFile.setPrice(200000L);
        }

        return importDeviceFile;
    }

    // Import device product
    private void saveProduct(Long importFileId) {
        ImportDeviceProductDAO idpDAO = new ImportDeviceProductDAO();
        List<Long> lstProductIdFromDB = idpDAO.findAllIdByFileId(importFileId);
        for (Long id : lstProductIdFromDB) {
            if (!isExistProduct(id, lstImportProduct)) {
                idpDAO.delete(id);
            }
        }
        List<Long> lstId = new ArrayList<>();
        for (ImportDeviceProduct obj : lstImportProduct) {
            obj.setFileId(importFileId);
            if ("COPY".equals(crudMode)) {
                obj.setProductId(null);
            }
            idpDAO.saveOrUpdate(obj);
            lstId.add(obj.getProductId());
        }

    }

    private Boolean isExistProduct(Long id, List<ImportDeviceProduct> lst) {
        for (ImportDeviceProduct itemInList : lst) {

            if (id != null && id.equals(itemInList.getProductId())) {
                return true;
            }

        }
        return false;
    }

    // Vunt: Donwnload file mẫu của thiết bị y tế
    @Listen("onDownloadFile = #lbDownloadExcelTemplate")
    public void onDownloadFile() throws FileNotFoundException {
        String folderPath = Executions.getCurrent().getDesktop().getWebApp()
                .getRealPath(Constants.UPLOAD.ATTACH_PATH);
        String path = folderPath + "\\BM_ThietBi_YTe.xls";
        File f = new File(path);
//        if (f != null) {
        if (f.exists()) {
            File tempFile = FileUtil.createTempFile(f, f.getName());
            Filedownload.save(tempFile, path);
        } else {
            Clients.showNotification(
                    "File không còn tồn tại trên hệ thống!",
                    Constants.Notification.INFO, null, "middle_center",
                    1500);
        }
//        } else {
//            Clients.showNotification("File không còn tồn tại trên hệ thống!",
//                    Constants.Notification.INFO, null, "middle_center", 1500);
//        }
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VImportFileRtAttach obj = (VImportFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(final Event eventDelete) {
        try {
            VImportFileRtAttach obj = (VImportFileRtAttach) eventDelete
                    .getData();
            Long fileId = obj.getObjectId();
            CosmeticAttachDAO rDAOHE = new CosmeticAttachDAO();
            rDAOHE.delete(obj.getAttachId());
            fillFileListbox(fileId);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification(
                    String.format(
                            Constants.Notification.DELETE_ERROR,
                            Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    /**
     * Xu ly su kien upload file
     *
     * @param event
     */
    @Listen("onUpload = #btnImportExcel")
    public void onUploadExcel(UploadEvent event)
            throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        listFileExcel = new ArrayList<Media>();
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getFormat();
            if (!("xlsx".equals(extFile.toLowerCase()) || "xls".equals(extFile
                    .toLowerCase()))) {
                showNotification("Định dạng file không được phép tải lên",
                        Constants.Notification.WARNING);
                continue;
            }

            // luu file vao danh sach file
            listFileExcel.clear();
            listFileExcel.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listFileExcel.remove(media);
                }
            });
            hl.appendChild(rm);
            fListImportExcel.getChildren().clear();
            fListImportExcel.appendChild(hl);
        }
    }

    // Tao file excel theo bieu mau co san
    @Listen("onClick=#btnCreateFromImport")
    public void onCreateFromImportExcel(Event event) throws IOException,
            Exception {
        if (listFileExcel == null || listFileExcel.isEmpty()) {
            lbImportExcelWarning.setValue("Bạn vui lòng chọn tệp đính kèm!");
            return;
        }

        if (listFileExcel.size() > 1) {
            lbImportExcelWarning
                    .setValue("Chỉ chọn một file đính kèm đúng định dạng!");
            return;
        }
        lbImportExcelWarning.setValue(null);
        for (Media media : listFileExcel) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String fileName = media.getName();
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            folderPath += separator + dateFormat.format(date);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                // tạo forlder
                fd.mkdirs();
            }
            File f = new File(folderPath + separator + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            // save to hard disk
            InputStream inputStream;
            OutputStream outputStream;
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }

            inputStream.close();
            outputStream.close();

            if ("xls".equals(media.getFormat().toLowerCase())) {
                // file xls
                HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(f));
                if (wb.getSheetAt(2) == null
                        || !"THÔNG TIN VỀ CÁN BỘ PHỤ TRÁCH".equals(
                                wb.getSheetName(0))
                        || !"THÔNG TIN TRANG THIẾT BỊ Y TẾ".equals(
                                wb.getSheetName(1))) {
                    lbImportExcelWarning
                            .setValue("File tải lên không đúng định dạng!");
                    return;
                }
                setfrom_XLS_file(wb);
            } else {
                // file xlsx
                XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(f));
                if (wb.getSheetAt(2) == null
                        || !"THÔNG TIN VỀ CÁN BỘ PHỤ TRÁCH".equals(
                                wb.getSheetName(0))
                        || !"THÔNG TIN TRANG THIẾT BỊ Y TẾ".equals(
                                wb.getSheetName(1))) {
                    lbImportExcelWarning
                            .setValue("File tải lên không đúng định dạng!");
                    return;
                }
                setfrom_XLSX_file(wb);
            }

            tb.setSelectedPanel(mainpanel);

        }
    }

    // fle xlsx
    private void setfrom_XLSX_file(XSSFWorkbook wb) {

        // thong tin ve can bo phu trach
        XSSFSheet sheet = wb.getSheetAt(0);

        setCanBoPhuTrachNhapKhau(sheet.getRow(1).getCell(1));
        setSoDienThoai(sheet.getRow(2).getCell(1));
        setSoDiDong(sheet.getRow(2).getCell(8));
        setSoCongvanDoanhnghiep(sheet.getRow(3).getCell(1));

        setSignPlace(sheet.getRow(4).getCell(1));
        setSignDate(sheet.getRow(4).getCell(8));

        //nhom trang thiet bi
        setNhomTrangThietBi(sheet.getRow(5).getCell(1));
        setTenTrangThietBi(sheet.getRow(5).getCell(8));
        setChonLoaiHoSo(sheet.getRow(6).getCell(1));
        setChonLoaiDieuChinh(sheet.getRow(7).getCell(1));
        setSoGiayPhepCu(sheet.getRow(8).getCell(1));
        setGiaTriUocTinh(sheet.getRow(9).getCell(1));
        setThoiHanUyQuyen(sheet.getRow(10).getCell(1));
        setMucDichNhapKhau(sheet.getRow(11).getCell(1));
        // thong tin ve thiet bi y te
        XSSFSheet sheet1 = wb.getSheetAt(1);
        int j = 2;
        lstImportProduct.clear();

//        listImportFileType = new CategoryDAOHE()
//                .findAllCategory(Constants.IMPORT_TYPE.IMPORT_PRODUCT_CATEGORY);
//        ListModelArray lstCategoryModel = new ListModelArray(listImportFileType);
//        lbImportCategoryProduct.setModel(lstCategoryModel);
//        lbImportCategoryProduct.renderAll();
//        lbImportCategoryProduct.setSelectedIndex(0);
        while (sheet1.getRow(j) != null && sheet1.getRow(j).getCell(0) != null
                && !"".equals(sheet1.getRow(j).getCell(0).toString())) {
            ImportDeviceProduct idp = new ImportDeviceProduct();
//
//            if (sheet1.getRow(j).getCell(0) != null
//                    || sheet1.getRow(j).getCell(0).toString() != "") {
//                for (int i = 0; i < lbImportCategoryProduct.getListModel()
//                        .getSize(); i++) {
//                    Category ct = (Category) lbImportCategoryProduct
//                            .getListModel().getElementAt(i);
//                    if (sheet1.getRow(j).getCell(0).toString()
//                            .equals(ct.getName())) {
//                        lbImportCategoryProduct.setSelectedIndex(i);
//                        break;
//                    }
//                }
//            }
            if (sheet1.getRow(j).getCell(0) != null) {
                idp.setQuantity(sheet1.getRow(j).getCell(0).toString());
            }
            if (sheet1.getRow(j).getCell(1) != null) {
                idp.setModel(sheet1.getRow(j).getCell(1).toString());
            }

            if (sheet1.getRow(j).getCell(2) != null
                    || !"".equals(sheet1.getRow(j).getCell(2).toString())) {
                idp.setManufacturer(sheet1.getRow(j).getCell(2).toString());
            }

            if (sheet1.getRow(j).getCell(3) != null) {
                idp.setNationalName(sheet1.getRow(j).getCell(3).toString());
            }

            if (sheet1.getRow(j).getCell(4) != null) {
                idp.setProduceYear(sheet1.getRow(j).getCell(4).toString());
            }
            if (sheet1.getRow(j).getCell(5) != null) {
                idp.setDistributor(sheet1.getRow(j).getCell(5).toString());
            }
            if (sheet1.getRow(j).getCell(6) != null) {
                idp.setNationalDistributorName(sheet1.getRow(j).getCell(6)
                        .toString());
            }
            if (sheet1.getRow(j).getCell(7) != null) {
                idp.setProductManufacture(sheet1.getRow(j).getCell(7).toString());
            }
            if (sheet1.getRow(j).getCell(8) != null) {
                idp.setNationalProduct(sheet1.getRow(j).getCell(8)
                        .toString());
            }
            idp.setSendProductCode(0L);

            lstImportProduct.add(idp);
            j++;
        }
        ListModelArray lstModelManufacturer = new ListModelArray(
                lstImportProduct);
        lbProduct.setModel(lstModelManufacturer);
        // if (lbProcedure.getItemCount() == 0) {

    }

    // file xls
    private void setfrom_XLS_file(HSSFWorkbook wb) {

        // thong tin ve can bo phu trach
        HSSFSheet sheet = wb.getSheetAt(0);
        if (sheet.getRow(1).getCell(1) != null) {
            tbHealthEquipmentNo.setValue(sheet.getRow(1).getCell(1).toString());
        } else {
            tbHealthEquipmentNo.setValue("");
        }

        if (sheet.getRow(1).getCell(8) != null) {
            tbOfficersName.setValue(sheet.getRow(1).getCell(8).toString());
        } else {
            tbOfficersName.setValue("");
        }

        if (sheet.getRow(2).getCell(1) != null) {
            tbOfficersPhone.setValue(sheet.getRow(2).getCell(1).toString());
        } else {
            tbOfficersPhone.setValue("");
        }

        if (sheet.getRow(2).getCell(8) != null) {
            tbOfficersMobile.setValue(sheet.getRow(2).getCell(8).toString());
        } else {
            tbOfficersMobile.setValue("");
        }

        if (sheet.getRow(3).getCell(1) != null) {
            tbIntendedUse.setValue(sheet.getRow(3).getCell(1).toString());
        } else {
            tbIntendedUse.setValue("");
        }

        if (sheet.getRow(3).getCell(8) != null) {
            if ("thiết bị y tế nhập khẩu trị giá dưới 1 tỷ đồng".equals(sheet.getRow(3).getCell(8).toString().toLowerCase())) {
                lbChoosePriceProfile.setSelectedIndex(0);
            } else if ("thiết bị y tế nhập khẩu trị giá từ 1 tỷ đến 3 tỷ đồng".equals(sheet
                    .getRow(3)
                    .getCell(8)
                    .toString()
                    .toLowerCase())) {
                lbChoosePriceProfile.setSelectedIndex(1);
            } else if ("thiết bị y tế nhập khẩu trị giá trên 3 tỷ đồng".equals(sheet.getRow(3).getCell(8).toString().toLowerCase())) {
                lbChoosePriceProfile.setSelectedIndex(2);
            } else {
                lbChoosePriceProfile.setSelectedIndex(3);
            }
        } else {
            lbChoosePriceProfile.setSelectedIndex(0);
        }

        if (sheet.getRow(4).getCell(1) != null) {
            tbSignPlace.setValue(sheet.getRow(4).getCell(1).toString());
        } else {
            tbSignPlace.setValue("");
        }

        if (!"".equals(sheet.getRow(4).getCell(8).toString())) {
            dbSignDate.setValue(sheet.getRow(4).getCell(8).getDateCellValue());
        }

        // thong tin ve thiet bi y te
        HSSFSheet sheet1 = wb.getSheetAt(1);
        int j = 2;
        lstImportProduct.clear();

        listImportFileType = new CategoryDAOHE()
                .findAllCategory(Constants.IMPORT_TYPE.IMPORT_PRODUCT_CATEGORY);
        ListModelArray lstCategoryModel = new ListModelArray(listImportFileType);
        lbImportCategoryProduct.setModel(lstCategoryModel);
        lbImportCategoryProduct.renderAll();
        lbImportCategoryProduct.setSelectedIndex(0);
        while (sheet1.getRow(j) != null && sheet1.getRow(j).getCell(0) != null
                && !"".equals(sheet1.getRow(j).getCell(0).toString())) {
            ImportDeviceProduct idp = new ImportDeviceProduct();

            if (sheet1.getRow(j).getCell(0) != null
                    || sheet1.getRow(j).getCell(0).toString() != "") {
                for (int i = 0; i < lbImportCategoryProduct.getListModel()
                        .getSize(); i++) {
                    Category ct = (Category) lbImportCategoryProduct
                            .getListModel().getElementAt(i);
                    if (sheet1.getRow(j).getCell(0).toString()
                            .equals(ct.getName())) {
                        lbImportCategoryProduct.setSelectedIndex(i);
                        break;
                    }
                }
            }

            if (sheet1.getRow(j).getCell(2) != null) {
                idp.setModel(sheet1.getRow(j).getCell(2).toString());
            }

            if (sheet1.getRow(j).getCell(3) != null
                    || !"".equals(sheet1.getRow(j).getCell(3).toString())) {
                idp.setManufacturer(sheet1.getRow(j).getCell(3).toString());
            }

            if (sheet1.getRow(j).getCell(4) != null) {
                idp.setNationalName(sheet1.getRow(j).getCell(4).toString());
            }

            if (sheet1.getRow(j).getCell(5) != null) {
                idp.setProduceYear(sheet1.getRow(j).getCell(5).toString());
            }
            if (sheet1.getRow(j).getCell(6) != null) {
                idp.setDistributor(sheet1.getRow(j).getCell(6).toString());
            }
            if (sheet1.getRow(j).getCell(7) != null) {
                idp.setNationalDistributorName(sheet1.getRow(j).getCell(7)
                        .toString());
            }
            idp.setSendProductCode(0L);

            lstImportProduct.add(idp);
            j++;
        }
        ListModelArray lstModelManufacturer = new ListModelArray(
                lstImportProduct);
        lbProduct.setModel(lstModelManufacturer);

    }

    // Thuc hien chuc nang ky so doanh nghiep
    @Listen("onUploadCert = #windowCRUDCosmetic")
    public void onUploadCert(Event event) {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        actionSignCA(event, actionPrepareSign());
    }

    public String actionPrepareSign() {
        String fileToSign = "";
        try {
            fileToSign = createFileToSign();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    public String createFileToSign() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return "fail";
        }

        // FilesModel fileModel = new FilesModel(files.getFileId());
        ExportDeviceModel exportModel = new ExportDeviceModel(files.getFileId());
        ExportFileDAO exp = new ExportFileDAO();
        return exp.exportFinalDeviceFileNoSign(exportModel, false);
    }

    public void actionSignCA(Event event, String fileToSign) {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        SignPdfFile pdfSig = new SignPdfFile();// pdfSig = new SignPdfFile();
        String base64Hash;
        try {
            String certSerial = x509Cert.getSerialNumber().toString(16);

            ResourceBundle rb = ResourceBundle.getBundle("config");
            String filePath = rb.getString("signTemp");
            FileUtil.mkdirs(filePath);
            String outputFileFinalName = "_" + (new Date()).getTime() + ".pdf";
            String outPutFileFinal = filePath + outputFileFinalName;
            // String linkImage = rb.getString("signImage");
            // String linkImageSign = linkImage + "232.png";
            // String linkImageStamp = linkImage + "attpStamp.png";
            CaUserDAO ca = new CaUserDAO();
            List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
            if (caur != null && !caur.isEmpty()) {
                String folderPath = ResourceBundleUtil.getString("dir_upload");
                FileUtil.mkdirs(folderPath);
                String separator = ResourceBundleUtil.getString("separator");// not
                // use
                // binhnt53
                // u230315
                String linkImageSign = folderPath + separator
                        + caur.get(0).getSignature();
                String linkImageStamp = folderPath + separator
                        + caur.get(0).getStamper();
                // fileSignOut = outPutFileFinal;
                Pdf pdfProcess = new Pdf();
                try {// chen chu ki
                    String sToFind = Constants.REPLATE_CHARACTER_WHEN_FIND_LOCATION_TO_SIGN.LOCATION_BUSINESS;
                    pdfProcess.insertImageAll(fileToSign, outPutFileFinal,
                            linkImageSign, linkImageStamp, sToFind);
                } catch (IOException ex) {
                    LogUtils.addLogDB(ex);
                }

                base64Hash = pdfSig.createHash(outPutFileFinal,
                        new Certificate[]{x509Cert});

                Session session = Sessions.getCurrent();
                session.setAttribute("certSerial", certSerial);
                session.setAttribute("base64Hash", base64Hash);
                txtBase64Hash.setValue(base64Hash);
                txtCertSerial.setValue(certSerial);
                session.setAttribute("PDFSignature", pdfSig);
                Clients.evalJavaScript("signAndSubmitFinalFile();");
            } else {
                showNotification("Chữ ký số chưa được đăng ký !!!");
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công !!!");
        }

    }

    @Listen("onSign = #windowCRUDCosmetic")
    public void onSign(Event event) {
        String data = event.getData().toString();

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
        // String inputFileName = "_" + (new Date()).getTime() + ".pdf";//NOT
        // ƯSE BINHNT53 230315
        String outputFileName = "_signed_DonXinNhapKhau_"
                + (new Date()).getTime() + ".pdf";
        // String inputFile = filePath + inputFileName;//NOT ƯSE BINHNT53 230315
        String outputFile = filePath + outputFileName;
        fileSignOut = outputFile;
        String signature = data;
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");

        try {
            pdfSig.insertSignature(signature, outputFile);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        LogUtils.addLog("Signed file: " + outputFile);
        try {
            // onSignFinalFile();
            onSignPermit();
            fileSignOut = "";
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
    }

    public void onSignPermit() throws Exception {
        ExportDeviceDAO edD = new ExportDeviceDAO();
        ExportDeviceModel edm = new ExportDeviceModel(files.getFileId());
        edm.setSendNo(permit.getReceiveNo());
        edm.setSignedDate(permit.getSignDate());
        edm.setCosmeticPermitId(files.getFileId());
        edD.updateAttachSignDeviceFile(edm, fileSignOut);
        showNotification("Ký số thành công!", Constants.Notification.INFO);
        fillFinalFileListbox(files.getFileId());
    }

    /**
     * Dong cua so khi o dang popup
     */
    @Listen("onClose = #windowCRUDCosmetic")
    public void onClose() {
        try {
            if (windowCRUDCosmetic != null) {
                windowCRUDCosmetic.detach();
            }
            Events.sendEvent("onVisible", parentWindow, null);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        // windowCRUDCosmetic.onClose();
        // Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onVisible = #windowCRUDCosmetic")
    public void onVisible() {
        windowCRUDCosmetic.setVisible(true);
    }

    @Listen("onChildWindowClosed=#windowCRUDCosmetic")
    public void onChildWindowClosed(Event event) {
        String nations = "";
        String nationsCode = "";
        listNationCode = new ArrayList();
        listNationName = new ArrayList();
        lstPlace = (Set<Listitem>) event.getData();
        if (lstPlace != null) {
            for (Listitem listItem : lstPlace) {
                Place place = listItem.getValue();
                listNationName.add(place.getName());
                listNationCode.add(String.valueOf(place.getCode()));
            }
            if (listNationName.size() != 0) {
                nations = org.apache.commons.lang.StringUtils.join(
                        listNationName, ", ");
            }

            if (listNationCode.size() != 0) {
                nationsCode = org.apache.commons.lang.StringUtils.join(
                        listNationCode, ", ");
            }
        }

        if (nations != null) {
            tbNationalName.setValue(nations);
        } else {
            tbNationalName.setValue("");
        }

        if (nationsCode != null) {
            tbNationalCode.setValue(nationsCode);
        } else {
            tbNationalCode.setValue("");
        }

    }

    @Listen("onChildWindowClosedDistributor=#windowCRUDCosmetic")
    public void onChildWindowClosedDistributor(Event event) {
        String nations = "";
        String nationsCode = "";
        listNationCode = new ArrayList();
        listNationName = new ArrayList();
        lstPlaceDistributor = (Set<Listitem>) event.getData();
        if (lstPlaceDistributor != null) {
            for (Listitem listItem : lstPlaceDistributor) {
                Place place = listItem.getValue();
                listNationName.add(place.getName());
                listNationCode.add(String.valueOf(place.getCode()));
            }
            if (listNationName.size() != 0) {
                nations = org.apache.commons.lang.StringUtils.join(
                        listNationName, ", ");
            }

            if (listNationCode.size() != 0) {
                nationsCode = org.apache.commons.lang.StringUtils.join(
                        listNationCode, ", ");
            }
        }

        if (nations != null) {
            tbNationalDistributorName.setValue(nations);
        } else {
            tbNationalDistributorName.setValue("");
        }

        if (nationsCode != null) {
            tbNationalDistributorCode.setValue(nationsCode);
        } else {
            tbNationalDistributorCode.setValue("");
        }
    }

    @Listen("onChildWindowClosedProduct=#windowCRUDCosmetic")
    public void onChildWindowClosedProduct(Event event) {
        String nations = "";
        String nationsCode = "";
        listNationCode = new ArrayList();
        listNationName = new ArrayList();
        lstPlaceProduct = (Set<Listitem>) event.getData();
        if (lstPlaceProduct != null) {
            for (Listitem listItem : lstPlaceProduct) {
                Place place = listItem.getValue();
                listNationName.add(place.getName());
                listNationCode.add(String.valueOf(place.getCode()));
            }
            if (!listNationName.isEmpty()) {
                nations = org.apache.commons.lang.StringUtils.join(
                        listNationName, ", ");
            }

            if (!listNationCode.isEmpty()) {
                nationsCode = org.apache.commons.lang.StringUtils.join(
                        listNationCode, ", ");
            }
        }

        if (nations != null) {
            tbNationalProduct.setValue(nations);
        } else {
            tbNationalProduct.setValue("");
        }

        if (nationsCode != null) {
            tbNationalProductCode.setValue(nationsCode);
        } else {
            tbNationalProductCode.setValue("");
        }
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////////
    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    public int checkDispathSDBS() {
        return checkDispathSDBS(files.getFileId());
    }

    public int checkBooked() {
        return checkBooked(files.getFileId(), files.getFileType());
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public ImportDeviceFile getImportDeviceFile() {
        return importDeviceFile;
    }

    public void setImportDeviceFile(ImportDeviceFile importDeviceFile) {
        this.importDeviceFile = importDeviceFile;
    }

    private static Object cloneObject(Object obj) {
        try {
            Object clone = obj.getClass().newInstance();
            for (Field field : obj.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.get(obj) == null || Modifier.isFinal(field.getModifiers())) {
                    continue;
                }
                if (field.getType().isPrimitive() || field.getType().equals(String.class)
                        || field.getType().getSuperclass().equals(Number.class)
                        || field.getType().equals(Boolean.class)) {
                    field.set(clone, field.get(obj));
                } else {
                    Object childObj = field.get(obj);
                    if (childObj == obj) {
                        field.set(clone, clone);
                    } else {
                        field.set(clone, cloneObject(field.get(obj)));
                    }
                }
            }
            return clone;
        } catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    private void setCanBoPhuTrachNhapKhau(XSSFCell cell) {
        if (cell != null) {
            tbOfficersName.setValue(cell.toString());
        } else {
            tbOfficersName.setValue("");
        }

    }

    private void setSoDienThoai(XSSFCell cell) {
        if (cell != null) {
            tbOfficersPhone.setValue(cell.toString());
        } else {
            tbOfficersPhone.setValue("");
        }
    }

    private void setSoDiDong(XSSFCell cell) {
        if (cell != null) {
            tbOfficersMobile.setValue(cell.toString());
        } else {
            tbOfficersMobile.setValue("");
        }
    }

    private void setSignPlace(XSSFCell cell) {
        if (cell != null) {
            tbSignPlace.setValue(cell.toString());
        } else {
            tbSignPlace.setValue("");
        }

    }

    private void setSignDate(XSSFCell cell) {
        if (!"".equals(cell.toString())) {
            dbSignDate.setValue(cell.getDateCellValue());
        }

    }

    private void setGiaTriUocTinh(XSSFCell cell) {
        if (cell != null) {
            if ("thiết bị y tế nhập khẩu trị giá dưới 1 tỷ đồng".equals(cell.toString().toLowerCase())) {
                lbChoosePriceProfile.setSelectedIndex(0);
            } else if ("thiết bị y tế nhập khẩu trị giá từ 1 tỷ đến 3 tỷ đồng".equals(cell.toString().toLowerCase())) {
                lbChoosePriceProfile.setSelectedIndex(1);
            } else if ("thiết bị y tế nhập khẩu trị giá trên 3 tỷ đồng".equals(cell.toString().toLowerCase())) {
                lbChoosePriceProfile.setSelectedIndex(2);
            } else {
                lbChoosePriceProfile.setSelectedIndex(3);
            }
        } else {
            lbChoosePriceProfile.setSelectedIndex(0);
        }

    }

    private void setMucDichNhapKhau(XSSFCell cell) {
        if (cell != null) {
            tbIntendedUse.setValue(cell.toString());
        } else {
            tbIntendedUse.setValue("");
        }

    }

    private void setThoiHanUyQuyen(XSSFCell cell) {

        if (!"".equals(cell.toString())) {
            dbUqDate.setValue(cell.getDateCellValue());
        }
    }

    private void setSoCongvanDoanhnghiep(XSSFCell cell) {
        if (cell != null) {
            tbHealthEquipmentNo.setValue(cell.toString());
        } else {
            tbHealthEquipmentNo.setValue("");
        }
    }

    private void setSoGiayPhepCu(XSSFCell cell) {
        if (cell != null) {
            tbOldNswFile.setValue(cell.toString());
        } else {
            tbOldNswFile.setValue("");
        }
    }

    private void setNhomTrangThietBi(XSSFCell cell) {
        if (cell != null) {
            switch (cell.toString().toUpperCase()) {
                case "BƠM TRUYỀN DỊCH, BƠM TIÊM ĐIỆN":
                    lbImportCategoryProduct.setSelectedIndex(1);
                    break;
                case "BUỒNG ÔXY CAO ÁP":
                    lbImportCategoryProduct.setSelectedIndex(2);
                    break;
                case "CÁC LOẠI THIẾT BỊ, VẬT LIỆU CAN THIỆP VÀO CƠ THỂ THUỘC CHUYÊN KHOA TIM MẠCH, THẦN KINH SỌ NÃO":
                    lbImportCategoryProduct.setSelectedIndex(3);
                    break;
                case "CÁC LOẠI THIẾT BỊ, VẬT LIỆU CẤY GHÉP LÂU DÀI (TRÊN 30 NGÀY) VÀO CƠ THỂ":
                    lbImportCategoryProduct.setSelectedIndex(4);
                    break;
                case "CÁC THIẾT BỊ CHẨN ĐOÁN HÌNH ẢNH DÙNG TIA X":
                    lbImportCategoryProduct.setSelectedIndex(5);
                    break;
                case "CÁC THIẾT BỊ ĐIỀU TRỊ DÙNG TIA X":
                    lbImportCategoryProduct.setSelectedIndex(6);
                    break;
                case "CÁC THIẾT BỊ XẠ TRỊ (MÁY COBAN ĐIỀU TRỊ UNG THƯ, MÁY GIA TỐC TUYẾN TÍNH ĐIỀU TRỊ UNG THƯ, DAO MỔ GAMMA CÁC LOẠI, THIẾT BỊ XẠ TRỊ ÁP SÁT CÁC LOẠI)":
                    lbImportCategoryProduct.setSelectedIndex(7);
                    break;
                case "DAO MỔ (ĐIỆN CAO TẦN, LASER, SIÊU ÂM)":
                    lbImportCategoryProduct.setSelectedIndex(8);
                    break;
                case "HỆ THỐNG CỘNG HƯỞNG TỪ":
                    lbImportCategoryProduct.setSelectedIndex(9);
                    break;
                case "HỆ THỐNG CYCLOTRON":
                    lbImportCategoryProduct.setSelectedIndex(10);
                    break;
                case "HỆ THỐNG KHÍ Y TẾ TRUNG TÂM":
                    lbImportCategoryProduct.setSelectedIndex(11);
                    break;
                case "HỆ THỐNG NỘI SOI CHẨN ĐOÁNHỆ THỐNG NỘI SOI CHẨN ĐOÁN":
                    lbImportCategoryProduct.setSelectedIndex(12);
                    break;
                case "HỆ THỐNG PHẪU THUẬT CHUYÊN NGÀNH NHÃN KHOA (LASER EXCIMER, PHEMTOSECOND LASER, PHACO, MÁY CẮT DỊCH KÍNH, MÁY CẮT VẠT GIÁC MẠC)":
                    lbImportCategoryProduct.setSelectedIndex(13);
                    break;
                case "HỆ THỐNG PHẪU THUẬT NỘI SOI":
                    lbImportCategoryProduct.setSelectedIndex(14);
                    break;
                case "HỆ THỐNG TÁN SỎI NGOÀI CƠ THỂ/TÁN SỎI NỘI SOI":
                    lbImportCategoryProduct.setSelectedIndex(15);
                    break;
                case "HỆ THỐNG THIẾT BỊ PHẪU THUẬT TIỀN LIỆT TUYẾN":
                    lbImportCategoryProduct.setSelectedIndex(16);
                    break;
                case "HỆ THỐNG THIẾT BỊ SIÊU ÂM CƯỜNG ĐỘ CAO ĐIỀU TRỊ KHỐI U":
                    lbImportCategoryProduct.setSelectedIndex(17);
                    break;
                case "HỆ THỐNG XÉT NGHIỆM ELISA":
                    lbImportCategoryProduct.setSelectedIndex(18);
                    break;
                case "KÍNH ÁP TRÒNG (CẬN, VIỄN, LOẠN THỊ) VÀ DUNG DỊCH BẢO QUẢN KÍNH ÁP TRÒNG":
                    lbImportCategoryProduct.setSelectedIndex(19);
                    break;
                case "KÍNH HIỂN VI PHẪU THUẬT":
                    lbImportCategoryProduct.setSelectedIndex(20);
                    break;
                case "LỒNG ẤP TRẺ SƠ SINH, MÁY SƯỞI ẤM TRẺ SƠ SINH":
                    lbImportCategoryProduct.setSelectedIndex(21);
                    break;
                case "MÁY CHIẾT TÁCH TẾ BÀO":
                    lbImportCategoryProduct.setSelectedIndex(22);
                    break;
                case "MÁY CHỤP CẮT LỚP ĐÁY MẮT/ MÁY CHỤP HUỲNH QUANG ĐÁY MẮT":
                    lbImportCategoryProduct.setSelectedIndex(23);
                    break;
                case "MÁY ĐỊNH DANH VI KHUẨN, VIRÚT":
                    lbImportCategoryProduct.setSelectedIndex(24);
                    break;
                case "MÁY ĐO ĐIỆN SINH LÝ (MÁY ĐIỆN NÃO, MÁY ĐIỆN TIM, MÁY ĐIỆN CƠ)":
                    lbImportCategoryProduct.setSelectedIndex(25);
                    break;
                case "MÁY ĐO ĐIỆN VÕNG MẠC":
                    lbImportCategoryProduct.setSelectedIndex(26);
                    break;
                case "MÁY ĐO ĐỘ LOÃNG XƯƠNG":
                    lbImportCategoryProduct.setSelectedIndex(27);
                    break;
                case "MÁY ĐO ĐÔNG MÁU":
                    lbImportCategoryProduct.setSelectedIndex(28);
                    break;
                case "MÁY ĐO KHÚC XẠ, GIÁC MẠC TỰ ĐỘNG":
                    lbImportCategoryProduct.setSelectedIndex(29);
                    break;
                case "MÁY ĐO NGƯNG TẬP VÀ PHÂN TÍCH CHỨC NĂNG TIỂU CẦU":
                    lbImportCategoryProduct.setSelectedIndex(30);
                    break;
                case "MÁY ĐO NHỊP TIM THAI BẰNG SIÊU ÂM":
                    lbImportCategoryProduct.setSelectedIndex(31);
                    break;
                case "MÁY ĐO TỐC ĐỘ MÁU LẮNG":
                    lbImportCategoryProduct.setSelectedIndex(32);
                    break;
                case "MÁY ĐO/PHÂN TÍCH CHỨC NĂNG HÔ HẤP":
                    lbImportCategoryProduct.setSelectedIndex(33);
                    break;
                case "MÁY GÂY MÊ/GÂY MÊ KÈM THỞ":
                    lbImportCategoryProduct.setSelectedIndex(34);
                    break;
                case "MÁY GIÚP THỞ":
                    lbImportCategoryProduct.setSelectedIndex(35);
                    break;
                case "MÁY LASER ĐIỀU TRỊ DÙNG TRONG NHÃN KHOA":
                    lbImportCategoryProduct.setSelectedIndex(36);
                    break;
                case "MÁY PHÁ RUNG TIM, TẠO NHỊP":
                    lbImportCategoryProduct.setSelectedIndex(37);
                    break;
                case "MÁY PHÂN TÍCH ĐIỆN GIẢI, KHÍ MÁU":
                    lbImportCategoryProduct.setSelectedIndex(38);
                    break;
                case "MÁY PHÂN TÍCH HUYẾT HỌC":
                    lbImportCategoryProduct.setSelectedIndex(39);
                    break;
                case "MÁY PHÂN TÍCH MIỄN DỊCH":
                    lbImportCategoryProduct.setSelectedIndex(40);
                    break;
                case "MÁY PHÂN TÍCH NHÓM MÁU":
                    lbImportCategoryProduct.setSelectedIndex(41);
                    break;
                case "MÁY PHÂN TÍCH SINH HÓA":
                    lbImportCategoryProduct.setSelectedIndex(42);
                    break;
                case "MÁY SIÊU ÂM CHẨN ĐOÁN":
                    lbImportCategoryProduct.setSelectedIndex(43);
                    break;
                case "MÁY TIM PHỔI NHÂN TẠO":
                    lbImportCategoryProduct.setSelectedIndex(44);
                    break;
                case "MÁY THEO DÕI BỆNH NHÂN":
                    lbImportCategoryProduct.setSelectedIndex(45);
                    break;
                case "THIẾT BỊ CHẨN ĐOÁN BẰNG ĐỒNG VỊ PHÓNG XẠ (HỆ THỐNG PET, PET/CT, SPECT, SPECT/CT, THIẾT BỊ ĐO ĐỘ TẬP TRUNG IỐT I130) ":
                    lbImportCategoryProduct.setSelectedIndex(46);
                    break;
                case "THIẾT BỊ ĐỊNH VỊ TRONG PHẪU THUẬT":
                    lbImportCategoryProduct.setSelectedIndex(47);
                    break;
                case "THIẾT BỊ LỌC MÁU":
                    lbImportCategoryProduct.setSelectedIndex(48);
                    break;
                case "THIẾT BỊ PHẪU THUẬT LẠNH":
                    lbImportCategoryProduct.setSelectedIndex(49);
                    break;
                case "XE CỨU THƯƠNG/ XE CHUYÊN DỤNG KHÁC DÙNG TRONG Y TẾ":
                    lbImportCategoryProduct.setSelectedIndex(50);
                    break;

                default:
                    lbImportCategoryProduct.setSelectedIndex(0);
                    break;
            }
        }
    }

    private void setTenTrangThietBi(XSSFCell cell) {
        if (cell != null) {
            tbProductName.setValue(cell.toString());
        } else {
            tbProductName.setValue("");
        }
    }

    private void setChonLoaiHoSo(XSSFCell cell) {
//        Cấp mới,Điều chỉnh, Gia hạn
        if (cell != null) {
            switch (cell.toString().toLowerCase()) {
                case "cấp mới":
                    lbTypeCreate.setSelectedIndex(1);
                    break;
                case "điều chỉnh":
                    lbTypeCreate.setSelectedIndex(2);
                    break;
                case "gia hạn":
                    lbTypeCreate.setSelectedIndex(3);
                    break;
            }
        } else {
            lbTypeCreate.setSelectedIndex(0);
        }
    }

    private void setChonLoaiDieuChinh(XSSFCell cell) {
        if (cell != null) {
            switch (cell.toString().toLowerCase()) {
                case "hãng nước sản xuất":
                    lbIsChange.setSelectedIndex(1);
                    break;
                case "tên tổ chức nhập khẩu - tên trang thiết bị":
                    lbIsChange.setSelectedIndex(2);
                    break;
            }
        } else {
            lbIsChange.setSelectedIndex(0);
        }
    }

}
