/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.module.cosmetic.Controller.CosmeticBaseController;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;

import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import org.apache.bcel.classfile.Constant;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

/**
 *
 * @author vunt
 */
public class ImportDeviceProductController extends CosmeticBaseController {

    @Wire
    Listbox modelFileListbox, attFileListbox;
    @Wire
    Checkbox ckModel;
    @Wire
    private Window parentWindow;
    private ImportDeviceProduct importDeviceProduct;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        Boolean k = importDeviceProduct.getIsCategory() != null && importDeviceProduct.getIsCategory() == 1L;

        AttachDAOHE atDAO = new AttachDAOHE();
        if (k) {
            List<Attachs> ls1 = atDAO.findByObjectId(importDeviceProduct.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL);
            modelFileListbox.setModel(new ListModelList<>(ls1));
            ckModel.setChecked(k);
            ckModel.setDisabled(true);
            ckModel.setVisible(true);
        }
        List<Attachs> ls2 = atDAO.findByObjectId(importDeviceProduct.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_ATT);
        attFileListbox.setModel(new ListModelList<>(ls2));
    }

    @Listen("onDownloadModelFile = #modelFileListbox")
    public void onDownloadModellFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Listen("onDownloadAttFile = #attFileListbox")
    public void onDownloadAttFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {

        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        importDeviceProduct = (ImportDeviceProduct) arguments.get("product");
        setParentWindow((Window) arguments.get("parentWindow"));
        return super.doBeforeCompose(page, parent, compInfo);
    }

    /**
     * @return the importDeviceProduct
     */
    public ImportDeviceProduct getImportDeviceProduct() {
        return importDeviceProduct;
    }

    /**
     * @param importDeviceProduct the importDeviceProduct to set
     */
    public void setImportDeviceProduct(ImportDeviceProduct importDeviceProduct) {
        this.importDeviceProduct = importDeviceProduct;
    }

    /**
     * @return the parentWindow
     */
    public Window getParentWindow() {
        return parentWindow;
    }

    /**
     * @param parentWindow the parentWindow to set
     */
    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

}
