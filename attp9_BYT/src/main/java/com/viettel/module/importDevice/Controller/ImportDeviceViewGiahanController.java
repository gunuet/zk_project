/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.user.BO.Roles;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.module.common.BO.FileMessage;
import com.viettel.module.common.DAO.FileMessageDAO;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.cosmetic.BO.CosReject;
import com.viettel.module.cosmetic.Controller.CosmeticAttachDAO;
import com.viettel.module.cosmetic.Controller.CosmeticBaseController;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.importDevice.DAO.ExportDeviceDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.Model.ExportDeviceModel;
import com.viettel.module.importDevice.Model.VFileImportDeviceModel;
import com.viettel.module.importDevice.Model.YCBSModel;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Business;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.RapidTest.VImportFileRtAttach;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;

/**
 *
 * @author linhdx
 */
public class ImportDeviceViewGiahanController extends CosmeticBaseController {


    private long FILE_TYPE;
    private String FILE_TYPE_NAME;
    private long DEPT_ID;
    @Wire
    Listbox lbProcedure, finalFileMessage;
    @Wire
    private Tabpanel tabpanel1;
    @Wire
    Label tbOfficersName, tbOfficersPhone, tbOfficersMobile;
    @Wire
    Label tbUnitName, tbUnitAdress, tbUnitPhone, tbUnitFax, tbDirectorName,
            tbDirectorPhone, tbDirectorMobile, lbUqDate, tbOldNswFileCode;
    @Wire
    Label lbTopWarning, lbBottomWarning, lbImportExcelWarning, lblTaxCode,
            lbBookNumber;
    @Wire("#incListCousin #lbcvchinh")
    Label lbcvchinh;
    @Wire("#incListCousin #lbcvcheo")
    Label lbcvcheo;
    @Wire("#incListCousin #lbcousin")
    Label lbcousin;
    @Wire("#incListCousin #lbhhd")
    Label lbhhd;
    //cuongvv
    @Wire("#incListCousin #lbListYCBS")
    private Listbox lbListYCBS;
    private List<Component> listTopActionComp;
    private List<Component> listBottomActionComp;
    private List ingredientListCheck, lstAnnexe;
    @Wire
    private Vlayout fListImportExcel;
    @Wire
    private Div divToolbarTop;
    @Wire
    private Div divToolbarBottom;
    @Wire
    private Listbox finalFileListboxPro;
    @Wire
    private Listbox finalFileListboxProReject;
    @Wire
    private Listbox finalFileListboxProSDBS;
    private Integer menuType;
    private com.viettel.core.workflow.BO.Process processCurrent;// process dang
    // duoc xu li
    private CosReject reject = new CosReject();
    @Wire
    private Div divDispath;
    // Danh sach
    @Wire
    Listbox lbTemplate;
    @Wire
    private Listbox fileListbox;
    @Wire
    Window windowCRUDCosmetic;
    @Wire
    private Window windowViewGiahan;
    @Wire
    private Window parentWindow;
    @Wire
    private Tabbox tb;
    @Wire
    private Tabpanel mainpanel;
    @Wire("#incListEvaluation #lbListEvaluation")
    private Listbox lbListEvaluation;
    @Wire
    Paging userPagingBottom;
    private String crudMode;
    private String isSave;
    private ListModel model;
    private CosFile cosFile;
    private ImportDeviceFile importDeviceFile;
    private ImportDeviceProduct importDeviceProduct;
    private Files files;
    Long documentTypeCode;
    @Wire
    Textbox txtCertSerial, txtBase64Hash;
    private AdditionalRequest additionalRequest;
    private BookDocument bookDocument = new BookDocument();
    private EvaluationRecord evaluationRecord = new EvaluationRecord();
    private Permit permit = new Permit();
    private PaymentInfo paymentInfo = new PaymentInfo();
    private String nswFileCode;
    private Long originalFilesId;
    private Long originalFileType;
    private Users user;
    private Business business;
    private AdditionalRequest addRequest;
    private Long fileId;
    List<NodeToNode> lstNextAction;

    @Wire
    Listbox attFileListbox;
    @Wire
    Div divAttachOfEachModel;

    private Long isDisplayDivCommentCV = 1L;
    @Wire
    Textbox tbCommentCV;
    @Wire("#flow_process #lbProcessImportDevice")
    Listbox lbProcessImportDevice;

    @Wire
    private Textbox tbStatus;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();

        FilesDAOHE fileDAOHE = new FilesDAOHE();
        ImportDeviceFiletDAO idpDAO = new ImportDeviceFiletDAO();
        fileId = (Long) arguments.get("id");
        files = fileDAOHE.findById(fileId);
        importDeviceFile = idpDAO.findByFileId(fileId);
        parentWindow = (Window) arguments.get("parentWindow");
        crudMode = (String) arguments.get("CRUDMode");
        menuType = (Integer) arguments.get("menuType");
        processCurrent = WorkflowAPI.getInstance().getCurrentProcess(
                files.getFileId(), files.getFileType(), files.getStatus(),
                getUserId());
        DEPT_ID = Constants.VU_TBYT_ID;// Vụ trang thiết bị y tế
        FILE_TYPE = files.getFileType();
        FILE_TYPE_NAME = files.getFileTypeName();
        documentTypeCode = importDeviceFile.getDocumentTypeCode();
        nswFileCode = importDeviceFile.getNswFileCode();
        originalFilesId = files.getFileId();
        originalFileType = files.getFileType();
        setProcessingView(originalFilesId, originalFileType);
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        loaData();
    }

    private void loaData() throws UnsupportedEncodingException {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        loadItemForEdit();
        fillFileListbox(originalFilesId);
        if (checkListOK() == 1) {
            fillFinalFileListbox2(originalFilesId);
        }
        if (checkListReject() == 1) {
            fillFinalFileListboxReject(originalFilesId);
        }
        if (checkListSDBS() == 1) {
            fillFinalFileListboxSDBS(originalFilesId);
        }
        listTopActionComp = new ArrayList<>();
        listBottomActionComp = new ArrayList<>();
        setListEvaluation();
        if (checkDispathSDBS() == 1) {
            divDispath = (Div) Executions.createComponents(
                    "/Pages/module/cosmetic/viewDispatchInclude.zul",
                    tabpanel1, arguments);
        }
        addAllNextActions();
        if (menuType != null) {
            loadActionsToToolbar(menuType, files, processCurrent, windowViewGiahan);
        }
        fillFinalFileMessage(fileId);
        getCouncil();
        getEvaluation();
        displayFlow(files.getNswFileCode(), files.getFileType());
    }

    public void setListEvaluation() {
        // lay danh sach cac tham dinh
        if (checkEvaluation() == 1) {
            EvaluationRecordDAO dao = new EvaluationRecordDAO();
            List<EvaluationRecord> lstEvaluationRecord = dao.getAllEvaluation(files.getFileId());
            if (lbListEvaluation != null) {
                lbListEvaluation.setModel(new ListModelArray(
                        lstEvaluationRecord));
            }

        }
    }

    private void fillFinalFileListbox2(Long fileId) throws UnsupportedEncodingException {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachNew = new ArrayList<>();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachType(
                fileId,
                Constants.OBJECT_TYPE.COSMETIC_PERMIT,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDB);
        if (lstAttach.size() > 0) {
            lstAttachNew.add(lstAttach.get(0));
        }

        List<Attachs> lstAttachVTB = rDaoHe.getByObjectIdAndAttachCatAndAttachType(
                fileId,
                Constants.OBJECT_TYPE.COSMETIC_PERMIT,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDB_VTDK);

        if (lstAttachVTB.size() > 0) {
            lstAttachNew.add(lstAttachVTB.get(0));
        }
        this.finalFileListboxPro.setModel(new ListModelArray(lstAttachNew));

    }

    private void fillFinalFileListboxReject(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachNew = new ArrayList<>();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachType(
                fileId,
                Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV);
        if (lstAttach.size() > 0) {
            lstAttachNew.add(lstAttach.get(0));
        }
        this.finalFileListboxProReject.setModel(new ListModelArray(lstAttachNew));
    }

    private void fillFinalFileListboxSDBS(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachNew = new ArrayList<>();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_SDBS_DISPATH);
        if (lstAttach.size() > 0) {
            lstAttachNew.add(lstAttach.get(0));
        }
        this.finalFileListboxProSDBS.setModel(new ListModelArray(lstAttachNew));
    }

    // Hien thi cac action tren thanh toolbar top va bottom
    public void loadActionsToToolbar(int menuType, final Files files,
            final com.viettel.core.workflow.BO.Process currentProcess,
            final Window currentWindow) {

        for (Component comp : listTopActionComp) {
            divToolbarTop.appendChild(comp);
        }

        for (Component comp : listBottomActionComp) {
            divToolbarBottom.appendChild(comp);
        }

    }

    private void addAllNextActions() throws UnsupportedEncodingException {
        // linhdx
        //Long docStatus = files.getStatus();
        Long docId = files.getFileId();
        Long userId = getUserId();
        Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;
        List<Process> lstProcess = WorkflowAPI.getInstance().getProcess(docId,
                docType, userId);
        List<Process> lstAllProcess = WorkflowAPI.getInstance().getAllProcessNotFinish(docId, docType);
        if ((lstAllProcess == null || lstAllProcess.isEmpty()) && files.getStatus() != Constants.FILE_STATUS_CODE.STATUS_MOITAO) {
            return;
        }
        List<Long> lstStatus = new ArrayList();
        // linhdx tim nhung trang thai chua hoan thanh xu ly de tim action tiep
        // theo
        for (Process obj : lstProcess) {
            if (obj.getFinishDate() == null) {
                lstStatus.add(obj.getStatus());
            }
        }

        // linhdx Co nguoi xu ly roi (Doanh nghiep da nop ho so) ma khong co
        // trang thai nao nguoi do can xu ly thi them flag NO_NEED_PROCESS
        // de bao khong can xu ly
        if (lstStatus.isEmpty() && lstAllProcess != null && !lstAllProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }

        // linhdx da co ho so gui toi nguoi do ma khong co ho so nao o trang
        // thai
        // can xu ly thi add flag NO_NEED_PROCESS de bao la khong phai xu ly nua
        if (lstStatus.isEmpty() && !lstProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }

        // Luu y: Neu de lstStatus == null thi ham findAvaiableNextActions se
        // coi nhu la
        // chua co xu ly va se tim node dau tien de gui
        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(docType, lstStatus, deptId);
        lstNextAction = actions;
        // linhdx
        if (actions != null && actions.size() > 0) {
            NodeToNode action = actions.get(0);
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
    }

    // viethd3: create new button for a processing action
    private Button createButtonForAction(NodeToNode action) throws UnsupportedEncodingException {
        //Cuongvv

        String receiveUserStr = ResourceBundleUtil.getString("USER_ID_VTB", "config");
        Long receiveUserID = Long.valueOf(receiveUserStr);
        if (!getUserId().equals(receiveUserID)) {
            if (validateUserRight(action, processCurrent, files)) {
                return null;
            }
        }

        // Button btn = new Button(action.getAction());
        //
        // final String actionName = action.getAction();
        Button btn = new Button("Xử lý hồ sơ");
        final String actionName = "Xử lý hồ sơ";

        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action
        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = files.getStatus();
        final List<NodeToNode> lstNextAction1 = lstNextAction;

        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, false));

        EventListener<Event> event = new EventListener<Event>() {

            @Override
            public void onEvent(Event t) throws Exception {
                Map<String, Object> data = new ConcurrentHashMap<>();
                data.put("fileId", files.getFileId());
                data.put("docId", files.getFileId());
                data.put("docType", files.getFileType());
                data.put("docStatus", status);
                data.put("actionId", actionId);
                data.put("actionName", actionName);
                data.put("actionType", actionType);
                data.put("nextId", nextId);
                data.put("previousId", prevId);
                if (processCurrent != null) {
                    data.put("process", processCurrent);
                }
                data.put("lstAvailableNDU", lstNDUs);
                data.put("parentWindow", windowViewGiahan);
                data.put("lstNextAction", lstNextAction1);
                createWindow("windowComment", formName, data, Window.MODAL);
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    @Listen("onViewFlow = #windowViewGiahan")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        Long docId = files.getFileId();
        Long docType = files.getFileType();
        args.put("objectId", docId);
        args.put("objectType", docType);
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    public void displayFlow(String nswFileCode, Long objectType) {
        ProcessDAOHE pdhe = new ProcessDAOHE();
        List lstProcess = pdhe.getProcessProcess(nswFileCode, objectType);
        if (lstProcess == null || lstProcess.isEmpty()) {
            return;
        }
        ListModelList lstModel = new ListModelList(lstProcess);
        lbProcessImportDevice.setModel(lstModel);

    }

    public String getMode(ImportDeviceProduct imd) {
        return imd.getModel();
    }

    public boolean validateUserRight(NodeToNode action,
            com.viettel.core.workflow.BO.Process processCurrent, Files file) {
        List<com.viettel.core.workflow.BO.Process> listCurrentProcess = WorkflowAPI.getInstance().findAllCurrentProcess(file.getFileId(),
                file.getFileType(), file.getStatus());
        Long userId = getUserId();
        Long creatorId = file.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return true;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                try {
                    for (com.viettel.core.workflow.BO.Process p : listCurrentProcess) {
                        if (p.getReceiveUserId().equals(userId)) {
                            needToProcess = true;
                        }
                    }
                    if (!needToProcess) {
                        return true;
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }

            }
        }
        return false;
    }

    private void fillDataToList() {
    }

    private void loadItemForEdit() {
        ImportDeviceFiletDAO idfDAO = new ImportDeviceFiletDAO();
        importDeviceFile = idfDAO.findByFileId(originalFilesId);
        if (importDeviceFile != null) {
            lblTaxCode.setValue(files.getTaxCode());
            tbUnitName.setValue(importDeviceFile.getImporterName());
            tbUnitAdress.setValue(importDeviceFile.getImporterAddress());
            tbUnitPhone.setValue(importDeviceFile.getImporterPhone());
            tbUnitFax.setValue(importDeviceFile.getImporterFax());
            tbDirectorName.setValue(importDeviceFile.getDirector());
            tbDirectorPhone.setValue(importDeviceFile.getDirectorPhone());
            tbDirectorMobile.setValue(importDeviceFile.getDirectorMobile());
            tbOfficersName.setValue(importDeviceFile.getResponsiblePersonName());
            tbOfficersPhone.setValue(importDeviceFile.getResponsiblePersonPhone());
            tbOfficersMobile.setValue(importDeviceFile.getResponsiblePersonMobile());
            tbOldNswFileCode.setValue(importDeviceFile.getOldNswFileCode());
            if (importDeviceFile.getUqDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                lbUqDate.setValue(sdf.format(importDeviceFile.getUqDate()));
            }

        }
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }

    private void fillFileListbox(Long fileId) {
        CosmeticAttachDAO rDaoHe = new CosmeticAttachDAO();
        List<VImportFileRtAttach> lstImportAttach = rDaoHe.findImportAttach(fileId);
        this.fileListbox.setModel(new ListModelArray(lstImportAttach));

    }

    public void setProcessingView(Long fileId, Long fileType) {
        if (fileId != null && fileType != null) {

            // 13032015 Load danh sach yeu cau sdbs
            BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
            BookDocument bookDocument2 = bookDocumentDAOHE.getBookInFromDocumentId(fileId, fileType);
            if (bookDocument2 != null) {
                bookDocument = bookDocument2;
            }

            // 13032015 Load danh sach yeu cau sdbs
            AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
            List<AdditionalRequest> lstAddition = additionalRequestDAO.findAllActiveByFileId(fileId);
            if (lstAddition != null && lstAddition.size() > 0) {
                additionalRequest = lstAddition.get(0);
            }

            EvaluationRecordDAO dao = new EvaluationRecordDAO();
            evaluationRecord = dao.getLastEvaluation(fileId);

            PermitDAO permitDAO = new PermitDAO();
            List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);
            if (lstPermit != null && lstPermit.size() > 0) {
                permit = lstPermit.get(0);
            }

            PaymentInfoDAO paymentInfoDAO = new PaymentInfoDAO();
            List<PaymentInfo> lstPayment = paymentInfoDAO.getListPayment(
                    fileId, Constants.PAYMENT.PHASE.EVALUATION);
            if (lstPayment != null && lstPayment.size() > 0) {
                paymentInfo = lstPayment.get(0);
            }

        }

    }

    /**
     * Tinh tong dung luong file attach da upload, da luu vao table ATTACHS & co
     * objectId
     *
     * @author
     * @return
     */
    public Long getAttachmentSize() {
        if (files != null && files.getFileId() != null) {

            AttachDAOHE daoHE = new AttachDAOHE();
            Long currentSize = 0L;
            List<Attachs> attachs = daoHE.findByObjectId(files.getFileId());
            for (Attachs item : attachs) {
                try {
                    File f = new File(item.getFullPathFile());
                    if (f.exists()) {
                        currentSize += f.length();
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }
            }
            return currentSize;
        }
        return 0L;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Clear canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    @Listen("onClose = #windowViewGiahan")
    public void onClose() {
        windowViewGiahan.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
    }

    // Vunt: Donwnload file mẫu của thiết bị y tế
    @Listen("onDownloadFile = #lbDownloadExcelTemplate")
    public void onDownloadFile() throws FileNotFoundException {
        String folderPath = Executions.getCurrent().getDesktop().getWebApp().getRealPath(Constants.UPLOAD.ATTACH_PATH);
        String path = folderPath + "\\BM_ThietBi_YTe.xlsx";
        File f = new File(path);

        if (f.exists()) {
            File tempFile = FileUtil.createTempFile(f, f.getName());
            Filedownload.save(tempFile, path);
        } else {
            Clients.showNotification(
                    "File không còn tồn tại trên hệ thống!",
                    Constants.Notification.INFO, null, "middle_center",
                    1500);
        }

    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VImportFileRtAttach obj = (VImportFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    @Listen("onDownloadFile = #fileViewListbox")
    public void onDownloadFileView(Event event) throws FileNotFoundException {
        VImportFileRtAttach obj = (VImportFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDownloadFinalFile = #finalFileListboxPro, #finalFileListbox, #finalFileListboxProReject, #finalFileListboxProSDBS")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long fid = obj.getObjectId();
        CosmeticAttachDAO rDAOHE = new CosmeticAttachDAO();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(fid);
    }

    @Listen("onVisible = #windowViewGiahan")
    public void onVisible() {
        windowViewGiahan.setVisible(true);
    }

    // Minhnv - Export pdf
    @Listen("onExportFile = #windowViewGiahan")
    public void onExportFile() {
        ExportDeviceModel exportModel = new ExportDeviceModel(
                importDeviceFile.getFileId());
        ExportDeviceDAO exp = new ExportDeviceDAO();
        exp.exportIDF(exportModel, true);
    }

    @Listen("onRefresh=#windowViewGiahan")
    public void onRefresh() {
        windowViewGiahan.detach();

        ImportDeviceFiletDAO viewCosFileDAO = new ImportDeviceFiletDAO();
        VFileImportDevice vFileCosfile = viewCosFileDAO.findViewByFileId(fileId);
        Events.sendEvent("onRefresh", parentWindow, vFileCosfile);
        LogUtils.addLog("on refresh view window!");
    }

    // /////////////////////////////////////////////////////////////////////////////////////////////////
    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    public int checkDispathSDBS() {
        return checkDispathSDBS(files.getFileId());
    }

    public int checkBooked() {
        return checkBooked(files.getFileId(), files.getFileType());
    }

    public int checkPayment() {
        return checkPayment(files.getFileId());
    }

    public int checkEvaluation() {
        return checkEvaluation(files.getFileId());
    }

    public int checkPermit() {
        return checkPermit(files.getFileId());
    }

    public int checkDispathReject() {
        return checkDispathReject(files.getFileId());
    }

    public int checkListOK() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachType(
                originalFilesId,
                Constants.OBJECT_TYPE.COSMETIC_PERMIT,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDB);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    public int checkListSDBS() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(originalFilesId,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_SDBS_DISPATH);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    public int checkListReject() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachType(
                originalFilesId,
                Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    public BookDocument getBookDocument() {
        return bookDocument;
    }

    public void setBookDocument(BookDocument bookDocument) {
        this.bookDocument = bookDocument;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public AdditionalRequest getAdditionalRequest() {
        return additionalRequest;
    }

    public void setAdditionalRequest(AdditionalRequest additionalRequest) {
        this.additionalRequest = additionalRequest;
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public Listbox getLbListEvaluation() {
        return lbListEvaluation;
    }

    public void setLbListEvaluation(Listbox lbListEvaluation) {
        this.lbListEvaluation = lbListEvaluation;
    }

    public EvaluationRecord getEvaluationRecord() {
        return evaluationRecord;
    }

    public void setEvaluationRecord(EvaluationRecord evaluationRecord) {
        this.evaluationRecord = evaluationRecord;
    }

    public Permit getPermit() {
        return permit;
    }

    public void setPermit(Permit permit) {
        this.permit = permit;
    }

    public CosReject getReject() {
        return reject;
    }

    public void setReject(CosReject reject) {
        this.reject = reject;
    }

    @Listen("onReload=#windowViewGiahan")
    public void onReload() {
        fillFinalFileMessage(fileId);
    }

    // Trao doi thong tin chuyen vien va DN
    public void fillFinalFileMessage(Long fileId2) {
        FileMessageDAO a = new FileMessageDAO();
        List<FileMessage> lsFileMessage1 = a.getListFileMessage(fileId2);
        this.finalFileMessage.setModel(new ListModelList(lsFileMessage1));
    }

    @Listen("onDeleteFileMessage = #finalFileMessage")
    public void onDeleteFileMessage(Event event) {
        FileMessage obj = (FileMessage) event.getData();
        Long fid = obj.getFileMessageId();
        FileMessageDAO a = new FileMessageDAO();
        a.deleteAttach(obj);
        fillFinalFileMessage(fid);
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("fileId", fileId);
        arguments.put("CRUDMode", "CREATE");
        createWindow("windowCRUDCosmetic",
                "/Pages/module/importreq/createUpdateFileMessage.zul",
                arguments, Window.MODAL);
    }

    @Listen("onView=#lbProduct")
    public void onView(Event ev) {
        ImportDeviceProduct ip = (ImportDeviceProduct) ev.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("product", ip);
        arguments.put("parentWindow", windowViewGiahan);
        createWindow("windowProduct",
                "/Pages/module/importreq/product/importdeviceproduct.zul", arguments,
                Window.MODAL);
    }

    @Listen("onViewPhuluc=#lbProduct")
    public void onViewPhuluc(Event ev) throws FileNotFoundException {
        ImportDeviceProduct ip = (ImportDeviceProduct) ev.getData();
        AttachDAOHE atDAO = new AttachDAOHE();
        List<Attachs> ls1 = atDAO.findByObjectId(ip.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL);
        if (ls1 != null && !ls1.isEmpty()) {
            Attachs obj = ls1.get(0);
            AttachDAO attDAO = new AttachDAO();
            attDAO.downloadFileAttach(obj);
        }

    }

    @Listen("onViewTailieudinhkem=#lbProduct")
    public void onViewTailieudinhkem(Event ev) throws FileNotFoundException {
        ImportDeviceProduct ip = (ImportDeviceProduct) ev.getData();
        AttachDAOHE atDAO = new AttachDAOHE();
        List<Attachs> ls2 = atDAO.findByObjectId(ip.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_ATT);
        if (ls2 != null && ls2.size() > 0) {
            attFileListbox.setModel(new ListModelList<>(ls2));
            divAttachOfEachModel.setVisible(true);
        }

    }

    @Listen("onDownloadAttFile = #attFileListbox")
    public void onDownloadAttFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    public boolean isAbleToModify(FileMessage obj) {
        Long userId = getUserId();
        Long creatorId = obj.getCreatorId();
        return userId.equals(creatorId);
    }

    public boolean isAbleToDelete(FileMessage obj) {
        Long userId = getUserId();
        Long creatorId = obj.getCreatorId();
        return userId.equals(creatorId);
    }

    public Long isDisplayDivCommentCV() {
        Roles role = new Roles();
        role.setRoleCode(ResourceBundleUtil.getString("Role_TTB_CV"));
        UserDAOHE uhe = new UserDAOHE();
        Boolean check = uhe.checkRolesOfUser(getUserId(), role);
        if (check) {
            return 1L;
        } else {
            return 0L;
        }
    }

    @Listen("onUpdateFileMessage = #finalFileMessage")
    public void onUpdateFileMessage(Event event) {
        FileMessage obj = (FileMessage) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileMessageId());
        arguments.put("CRUDMode", "UPDATE");
        // setParam(arguments);
        createWindow("windowCRUDCosmetic",
                "/Pages/module/importreq/createUpdateFileMessage.zul",
                arguments, Window.MODAL);
        // cosmeticAll.setVisible(false);
    }

    public void getCouncil() {
        VFileImportDeviceModel deviceModel = new VFileImportDeviceModel();

        lbcvchinh.setValue(deviceModel.getContent(fileId,
                Constants.EVAL_TYPE.ROLE_CVC));
        lbcvcheo.setValue(deviceModel.getContent(fileId,
                Constants.EVAL_TYPE.ROLE_CVKTC));
        lbcousin.setValue(deviceModel.getContent(fileId,
                Constants.EVAL_TYPE.ROLE_TVHD));
        lbhhd.setValue(deviceModel.getContent(fileId,
                Constants.FILE_STATUS_CODE.STATUS_HHD));
    }

    //Cuongvv
    public void getEvaluation() {
        List<YCBSModel> listYCBS = new ArrayList<>();

        EvaluationRecordDAO eDAO = new EvaluationRecordDAO();
        List<EvaluationRecord> lstevaluationRecord = eDAO.getListByFileIdAndEvalType(fileId, 19L);
        if (lstevaluationRecord != null) {
            for (EvaluationRecord lstevaluationRecord1 : lstevaluationRecord) {
                YCBSModel ycbs = new YCBSModel();
                ycbs.setCreateName(lstevaluationRecord1.getCreatorName());
                ycbs.setSendDate(ycbs.formatDate(lstevaluationRecord1.getCreateDate()));
                ycbs.setContent(lstevaluationRecord1.getMainContent());
                listYCBS.add(ycbs);
            }
        }
        lbListYCBS.setModel(new ListModelArray(listYCBS));
    }

    @Listen("onClick=#btnUpdateCommentCV")
    public void onUpdateCommentCV() throws IOException {
        importDeviceFile.setCommentCV(tbCommentCV.getText());
        ImportDeviceFiletDAO importDeviceDAO = new ImportDeviceFiletDAO();
        importDeviceDAO.saveOrUpdate(importDeviceFile);
        showNotification("Cập nhật ý kiến thành công", Constants.Notification.INFO);
    }

    public boolean isAbleToModifyStatus() {
        boolean result = false;
        if ("admin".equals(getUserName())) {
            result = true;
        }
        return result;
    }

    public void updateStatus() {
        String status = tbStatus.getValue();
        if (status != null && Long.valueOf(status) > 0) {
            List<Long> lstStatus = new ArrayList();
            lstStatus.add(Long.valueOf(status));
            ProcessDAOHE pHe = new ProcessDAOHE();
            List<Process> lstProcess = pHe.getDescProcessListStatus(files.getFileId(), Constants.FILE_TYPE_TTB, lstStatus);
            if (lstProcess.size() > 0) {
                files.setStatus(Long.valueOf(status));
                FilesDAOHE fHe = new FilesDAOHE();
                fHe.SaveAndCommit(files);
                fHe.commit();
                showNotification("Cập nhật thành công");
            } else {
                showNotification("Không đúng trạng thái");
            }

        }
    }

    @Listen("onClick=#tbOldNswFileCode")
    public void onOpenOldFile() throws IOException {
//        String receiveNo = importDeviceFile.getOldNswFileCode();
//        PermitDAO pDAO = new PermitDAO();
//        Permit p = pDAO.findTtbCreateNewByReveiveNo(receiveNo);
//        Files file = null;
//        if(p!=null){
//            FilesDAOHE dao = new FilesDAOHE();
//            file = dao.findById(p.getFileId());
//        }
//        
//        if(file ==null){
//            showNotification("Không tồn tại hồ sơ");
//            return;
//        }
        String oldNswFileCode = importDeviceFile.getOldNswFileCode();
        FilesDAOHE dao = new FilesDAOHE();
        Files file = dao.findByNswCodeAndDoctype(oldNswFileCode, Constants.FILE_TYPE_TTB);
        if(file == null){
            showNotification("Không tồn tại hồ sơ");
            return;
        }
        
        createWindowView(file.getFileId(), Constants.DOCUMENT_MENU.ALL,
                    windowViewGiahan);
        windowViewGiahan.setVisible(false);
    }
    private void createWindowView(Long id, int menuType, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        String fileTypeStr = Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT;
        arguments.put("filetype", fileTypeStr); 
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView2",
                "/Pages/module/importreq/importdeviceView.zul", arguments,
                Window.MODAL);
    }
    
    
}
