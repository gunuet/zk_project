package com.viettel.module.importDevice.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.RoleUserDeptDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BO.ViewMultiForm;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.DAO.ViewMultiFormDAO;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.DAO.ListImportDeviceDAO;
import com.viettel.module.importDevice.Model.SearchDeviceModel;
import com.viettel.module.importDevice.Model.VFileImportDeviceModel;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.model.exporter.ExcelExporter;
import com.viettel.voffice.model.exporter.Interceptor;
import com.viettel.voffice.model.exporter.RowRenderer;
import com.viettel.ws.Helper_VofficeMoh;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBException;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.zkoss.poi.ss.usermodel.Cell;
import org.zkoss.poi.ss.usermodel.CellStyle;
import org.zkoss.poi.ss.usermodel.IndexedColors;
import org.zkoss.poi.ss.usermodel.Row;
import org.zkoss.poi.ss.util.CellRangeAddress;
import org.zkoss.poi.xssf.usermodel.XSSFFont;
import org.zkoss.poi.xssf.usermodel.XSSFSheet;
import org.zkoss.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;

/**
 *
 * @author Linhdx
 */
public class ListImportControler extends BaseComposer {

    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #dbFromDayModify")
    private Datebox dbFromDayModify;
    @Wire("#incSearchFullForm #dbToDayModify")
    private Datebox dbToDayModify;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #tbCosmeticBrand")
    private Textbox tbCosmeticBrand;
    @Wire("#incSearchFullForm #tbCosmeticProductName")
    private Textbox tbCosmeticProductName;
    @Wire("#incSearchFullForm #tbCompanyName")
    private Textbox tbCompanyName;
    @Wire("#incSearchFullForm #tbCompanyAddress")
    private Textbox tbCompanyAddress;
    @Wire("#incSearchFullForm #tbResponsiblePersonName")
    private Textbox tbResponsiblePersonName;
    @Wire("#incSearchFullForm #tbBusinessTaxCode")
    private Textbox tbBusinessTaxCode;
    @Wire("#incSearchFullForm #tbFileId")
    private Textbox tbFileId;
    @Wire("#incSearchFullForm #lboxStatus")
    private Listbox lboxStatus;
    @Wire("#incSearchFullForm #lboxDonvi")
    private Listbox lboxDonvi;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #tbImportName")
    private Textbox tbImportName;
    @Wire("#incSearchFullForm #tbEquipmentNo")
    private Textbox tbEquipmentNo;

    @Wire("#incSearchFullForm #lbDocumentTypeCode")
    private Listbox lbDocumentTypeCode;

    @Wire("#incSearchFullForm #lbImportCategoryProduct")
    private Listbox lbImportCategoryProduct;
    @Wire("#incSearchFullForm #tbProductName")
    private Textbox tbProductName;
    @Wire("#incSearchFullForm #tbPermitNumber")
    private Textbox tbPermitNumber;

    @Wire("#incSearchFullForm #dbFromDayPermit")
    private Datebox dbFromDayPermit;

    @Wire("#incSearchFullForm #dbToDayPermit")
    private Datebox dbToDayPermit;

    @Wire("#incSearchFullForm #tbModel")
    private Textbox tbModel;

    @Wire("#incSearchFullForm #tbManufacturer")
    private Textbox tbManufacturer;

    @Wire("#incSearchFullForm #tbNationName")
    private Textbox tbNationName;

    @Wire("#incSearchFullForm #tbDistributor")
    private Textbox tbDistributor;

    @Wire("#incSearchFullForm #tbNationalDistributorName")
    private Textbox tbNationalDistributorName;

    @Wire("#incSearchFullForm #tbProductManufacture")
    private Textbox tbProductManufacture;

    @Wire("#incSearchFullForm #tbNationalProduct")
    private Textbox tbNationalProduct;

    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire("#tentrang")
    private Label tentrang;
    // Cuongvv
    @Wire
    private Window windowView;
    List<NodeToNode> lstNextAction;
    private com.viettel.core.workflow.BO.Process processCurrent;
    // ---
    @Wire
    private Window cosmeticAll;
    private VFileImportDeviceModel lastSearchModel;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;
    Long UserId = getUserId();
    CategoryDAOHE dao = new CategoryDAOHE();
    ;
	private Users user;
    private String sPositionType;
    private Constants constants = new Constants();
    String sBoSung = Constants.PROCESS_STATUS.SENT_RETURN.toString();
    String statusStr;
    // list cac checkbox đã check trong list
    List<VFileImportDevice> itemsCheckedList, lstCosmetic;
    // list cac checkbox đã check trong 1 trang
    List<Listitem> itemsChecksedPage;
    String menuTypeStr;
    // luong
    private VFileImportDevice vFileCosfile;
    private Files files;
    private SearchDeviceModel sm;
    private List<Category> lstStatusAll = new ArrayList<>();

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            List<Category> lstDonvi = new ArrayList();
            Category obj = new Category();
            obj.setCategoryId(-1L);
            obj.setName("---Chọn---");
            lstDonvi.add(obj);
            Category obj2 = new Category();
            obj2.setCategoryId(1L);
            obj2.setName("Vụ trang thiết bị");
            lstDonvi.add(obj2);
            ListModelArray lstModelDonvi = new ListModelArray(lstDonvi);
            if (lboxDonvi != null) {
                lboxDonvi.setModel(lstModelDonvi);
            }

            itemsCheckedList = new ArrayList<VFileImportDevice>();
            lastSearchModel = new VFileImportDeviceModel();
            fullSearchGbx.setVisible(false);
            Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
            FILE_TYPE_STR = (String) arguments.get("filetype");
            String deptIdStr = (String) arguments.get("deptid");
            menuTypeStr = (String) arguments.get("menuType");
            if (tentrang != null) {
                if (Constants.MENU_TYPE.WAITPROCESS_STR.equals(menuTypeStr)) {

                    tentrang.setValue("Hồ sơ chờ xử lý");
                } else if (Constants.MENU_TYPE.PROCESSING_STR.equals(menuTypeStr)) {
                    tentrang.setValue("Hồ sơ đã tham gia xử lý");
                } else if (Constants.MENU_TYPE.PROCESSED_STR.equals(menuTypeStr)) {
                    tentrang.setValue("Hồ sơ kết thúc xử lý");
                }
            }

            sPositionType = (String) arguments.get("PositionType");
            statusStr = (String) arguments.get("status");
            Long statusL = null;
            try {
                if (statusStr != null) {
                    statusL = Long.parseLong(statusStr);
                }
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
            // if (statusL != null) {
            // lastSearchModel.setStatus(statusL);
            // }
            //
            lastSearchModel.setMenuTypeStr(menuTypeStr);
            try {
                DEPT_ID = Long.parseLong(deptIdStr);
                DEPT_ID = 3409L;
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
                DEPT_ID = -1l;
            }
            FILE_TYPE = WorkflowAPI.getInstance().getProcedureTypeIdByCode(
                    FILE_TYPE_STR);

            Long userId = getUserId();
            UserDAOHE userDAO = new UserDAOHE();
            user = userDAO.findById(userId);
            CategoryDAOHE cdhe = new CategoryDAOHE();
            ListImportDeviceDAO objDAOHE = new ListImportDeviceDAO();
            // List lstStatusAll = cdhe.getSelectCategoryByType(
            // Constants.CATEGORY_TYPE.FILE_STATUS, "name");
            // linhdx 20160506
            if (lbImportCategoryProduct != null) {
                List<Category> listImportFileType = new CategoryDAOHE()
                        .findAllCategory(Constants.IMPORT_TYPE.IMPORT_PRODUCT_CATEGORY);
                ListModelArray lstCategoryModel = new ListModelArray(
                        listImportFileType);
                lbImportCategoryProduct.setModel(lstCategoryModel);
                lbImportCategoryProduct.renderAll();
                lbImportCategoryProduct.setSelectedIndex(0);
            }

            List lstStatus;
            if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
                lstStatus = objDAOHE.findListStatusByCreatorId(getUserId());
                dbFromDay.setValue(null);
                lastSearchModel.setCreateDateFrom(null);
                dbToDay.setValue(new Date());
                lastSearchModel.setCreateDateTo(new Date());
            } else {
                // Cuongvv neu la van thu bo
                Long receiveUserID;
                String receiveUserStr = ResourceBundleUtil.getString("USER_ID_VTB",
                        "config");
                if (receiveUserStr != null) {
                    receiveUserID = Long.valueOf(receiveUserStr);
                } else {
                    receiveUserID = -1L;
                }
                if (getUserId().equals(receiveUserID)) {
                    lstStatus = objDAOHE.findListStatusByDept(lastSearchModel,
                            receiveUserID);
                    dbFromDayModify.setValue(null);
                    lastSearchModel.setModifyDateFrom(null);
                    dbToDayModify.setValue(new Date());
                    lastSearchModel.setModifyDateTo(new Date());
                } else {
                    lstStatus = objDAOHE.findListStatusByReceiverAndDeptId(
                            lastSearchModel, getUserId(), getDeptId());
                    dbFromDayModify.setValue(null);
                    lastSearchModel.setModifyDateFrom(null);
                    dbToDayModify.setValue(new Date());
                    lastSearchModel.setModifyDateTo(new Date());
                }
            }
            if (statusL != null) {
                lstStatus = new ArrayList<>();
                lstStatus.add(statusL);
                if (statusL.equals(Constants.IDF_PROCESS_STATUS.VT_RETURN)) {
                    lstStatus.add(Constants.IDF_PROCESS_STATUS.SENT_RETURN);
                    lstStatus.add(Constants.IDF_PROCESS_STATUS.TKHD_RETURN);
                }
                lastSearchModel.setLsStatusStr(StringUtils.join(
                        lstStatus.toArray(), ",", true));
                lastSearchModel.setStatusList(lstStatus);
            }
            if (lstStatus != null && !lstStatus.isEmpty()) {
                lstStatusAll = cdhe.findStatusByVal(lstStatus.toArray());

                ListModelArray lstModelProcedure = new ListModelArray(lstStatusAll);
                lboxStatus.setModel(lstModelProcedure);
                onSearch();
            }
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() throws UnsupportedEncodingException {
        Clients.showBusy("");
        try {
            userPagingBottom.setActivePage(0);
            userPagingTop.setActivePage(0);
		// validate search
            // neu an nut search reset lại itemsCheckedList
            itemsCheckedList = new ArrayList<VFileImportDevice>();
            if (dbFromDay != null && dbToDay != null
                    && dbFromDay.getValue() != null && dbToDay.getValue() != null) {
                if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                    showNotification(
                            "Thời gian từ ngày không được lớn hơn đến ngày",
                            Constants.Notification.WARNING, 2000);
                    return;
                }
            }
            if (dbFromDayModify != null && dbToDayModify != null
                    && dbFromDayModify.getValue() != null
                    && dbToDayModify.getValue() != null) {
                if (dbFromDayModify.getValue().compareTo(dbToDayModify.getValue()) > 0) {
                    showNotification(
                            "Thời gian từ ngày không được lớn hơn đến ngày",
                            Constants.Notification.WARNING, 2000);
                    return;
                }
            }

            if (dbFromDayPermit != null && dbToDayPermit != null
                    && dbFromDayPermit.getValue() != null
                    && dbToDayPermit.getValue() != null) {
                if (dbFromDayPermit.getValue().compareTo(dbToDayPermit.getValue()) > 0) {
                    showNotification(
                            "Ngày cấp phép : từ ngày không được lớn hơn đến ngày",
                            Constants.Notification.WARNING, 2000);
                    dbFromDayPermit.focus();
                    return;
                }
            }

            lastSearchModel.setBusinessName(tbCompanyName == null ? null
                    : tbCompanyName.getValue());
            lastSearchModel.setBusinessAddress(tbCompanyAddress == null ? null
                    : tbCompanyAddress.getValue());
            lastSearchModel
                    .setResponsiblePersonName(tbResponsiblePersonName == null ? null
                                    : tbResponsiblePersonName.getValue());
            lastSearchModel.setTaxCode(tbBusinessTaxCode == null ? null
                    : tbBusinessTaxCode.getValue());

            lastSearchModel.setCreateDateFrom(dbFromDay == null ? null : dbFromDay
                    .getValue());
            lastSearchModel.setCreateDateTo(dbToDay == null ? null : dbToDay
                    .getValue());
            lastSearchModel.setModifyDateFrom(dbFromDayModify == null ? null
                    : dbFromDayModify.getValue());
            lastSearchModel.setModifyDateTo(dbToDayModify == null ? null
                    : dbToDayModify.getValue());

            lastSearchModel.setImporterName(tbImportName.getValue());
            lastSearchModel.setEquipmentNo(tbEquipmentNo.getValue());
            lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());

            // linhdx 2160506 them cac truong tim kiem
            Long documentTypeCode = Long.valueOf(lbDocumentTypeCode
                    .getSelectedItem().getValue().toString());
            lastSearchModel.setDocumentTypeCode(documentTypeCode);
            String groupProductId = null;
            if (lbImportCategoryProduct.getSelectedIndex() > 0) {
                groupProductId = lbImportCategoryProduct.getSelectedItem()
                        .getValue().toString();
            }
            lastSearchModel.setGroupProductId(groupProductId);
            lastSearchModel.setProductName(tbProductName.getValue());
            lastSearchModel.setPermitNumber(tbPermitNumber.getValue());
            lastSearchModel.setPermitDateFrom(dbFromDayPermit == null ? null
                    : dbFromDayPermit.getValue());
            lastSearchModel.setPermitDateTo(dbToDayPermit == null ? null
                    : dbToDayPermit.getValue());

            // linhdx 20160517, them tim kiem theo san pham
            if (tbModel != null) {
                lastSearchModel.setModel(tbModel.getValue());
            }
            if (tbManufacturer != null) {
                lastSearchModel.setManufacturer(tbManufacturer.getValue());
            }

            if (tbNationName != null) {
                lastSearchModel.setNationalName(tbNationName.getValue());
            }

            if (tbDistributor != null) {
                lastSearchModel.setDistributor(tbDistributor.getValue());
            }

            if (tbNationalDistributorName != null) {
                lastSearchModel
                        .setNationalDistributorName(tbNationalDistributorName
                                .getValue());
            }

            if (tbProductManufacture != null) {
                lastSearchModel.setProductManufacture(tbProductManufacture
                        .getValue());
            }

            if (tbNationalProduct != null) {
                lastSearchModel.setNationalProduct(tbNationalProduct.getValue());
            }

            if (lboxStatus.getSelectedItem() != null) {
                try {
                    Object value = lboxStatus.getSelectedItem().getValue();
                    if (value != null && value instanceof String) {
                        Long valueL = Long.valueOf((String) value);
                        if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                            lastSearchModel.setStatus(valueL);
                        }
                    } else {
                        lastSearchModel.setStatus(null);
                    }
                } catch (NumberFormatException ex) {
                    LogUtils.addLogDB(ex);
                }

            }

            if (statusStr != null) {
                if (statusStr.equals(sBoSung)) {
                    lastSearchModel.setStatus(Long.parseLong(sBoSung));
                }
            }

            userPagingBottom.setActivePage(0);
            userPagingTop.setActivePage(0);

            reloadModel(lastSearchModel);
        } catch (WrongValueException | NumberFormatException | UnsupportedEncodingException ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    private void reloadModel(SearchDeviceModel searchModel)
            throws UnsupportedEncodingException {
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage()
                * userPagingBottom.getPageSize();
        PagingListModel plm;
        ListImportDeviceDAO objDAOHE = new ListImportDeviceDAO();
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(),
                    start, take);
        } else {
            // Cuongvv
            Long receiveUserID;
            String receiveUserStr = ResourceBundleUtil.getString("USER_ID_VTB",
                    "config");
            if (receiveUserStr != null) {
                receiveUserID = Long.valueOf(receiveUserStr);
            } else {
                receiveUserID = -1L;
            }
            if (getUserId().equals(receiveUserID)) {
                plm = objDAOHE.findFilesByDeptMis(searchModel, Long
                        .valueOf(ResourceBundleUtil.getString("USER_ID_VTB",
                                        "config")), start, take);
            } else {
                plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel,
                        getUserId(), getDeptId(), start, take);
                sm = searchModel;
            }
        }
        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstCosmetic = plm.getLstReturn();
        lstModel.setMultiple(true);

        // itemsCheckedList set selected
        lbList.setModel(lstModel);
        lbList.renderAll();
        List<Listitem> listItem = lbList.getItems();
        Set<Listitem> itemselected = new HashSet();
        for (Listitem itemlb : listItem) {
            for (int i = 0; i < itemsCheckedList.size(); i++) {
                if (itemsCheckedList
                        .get(i)
                        .getFileId()
                        .equals(((VFileImportDevice) itemlb.getValue())
                                .getFileId())) {
                    itemselected.add(itemlb);
                    break;
                }
            }
        }
        lbList.setSelectedItems(itemselected);
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) throws UnsupportedEncodingException {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #cosmeticAll")
    public void onSearchFullText(Event event)
            throws UnsupportedEncodingException {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch=#cosmeticAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            // Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    // Show popup khi an nut thao tac
    @Listen("onShowActionMulti=#cosmeticAll")
    public void onShowActionMulti() {
        if (lastSearchModel == null
                || !lastSearchModel.getMenuTypeStr().equals(
                        Constants.MENU_TYPE.WAITPROCESS_STR)) {
            return;
        }
        ViewMultiForm vmf = null;
        if (itemsCheckedList == null || itemsCheckedList.isEmpty()) {
            showNotification("Chưa chọn hồ sơ !!!");
            return;
        } else {
            String form = null;
            // ProcessDAOHE psDAO = new ProcessDAOHE();

            ViewMultiFormDAO vfDAO = new ViewMultiFormDAO();
            boolean checkProcessMutiPayment = false;
            for (VFileImportDevice vf : itemsCheckedList) {
                if (Objects.equals(vf.getStatus(),
                        Constants.IDF_PROCESS_STATUS.DN_PAYMENT)) {
                    checkProcessMutiPayment = true;
                }
                vmf = vfDAO
                        .getByStatusAndType(vf.getStatus(), vf.getFileType());
                if (vmf == null && !checkProcessMutiPayment) {
                    showNotification("Không thể thao tác xử lý nhiều hồ sơ với các hồ sơ đã chọn !!!");
                    return;
                }
                if (!checkProcessMutiPayment) {
                    if (form == null) {
                        form = vmf.getFormName();
                    } else {
                        if (!form.equals(vmf.getFormName())) {
                            showNotification("Chỉ được chọn các hồ sơ cùng trạng thái xử lý!!!");
                            return;
                        }
                    }
                }
            }
            if (checkProcessMutiPayment) {

                Long docId = itemsCheckedList.get(0).getFileId();
                Long userId = getUserId();
                Long docType = FILE_TYPE;
                Long deptId = DEPT_ID;
                List<com.viettel.core.workflow.BO.Process> lstProcess = WorkflowAPI
                        .getInstance().getProcess(docId, docType, userId);
                List<Long> lstStatus = new ArrayList();
                // linhdx tim nhung trang thai chua hoan thanh xu ly de tim
                // action tiep
                // theo
                for (com.viettel.core.workflow.BO.Process obj : lstProcess) {
                    if (obj.getFinishDate() == null) {
                        lstStatus.add(obj.getStatus());
                    }
                }
                List<NodeToNode> actions = WorkflowAPI.getInstance()
                        .findAvaiableNextActions(docType, lstStatus, deptId);
                lstNextAction = actions;
                final Long nextId = actions.get(0).getNextId();

                final List<NodeDeptUser> lstNDUs = new ArrayList<>();
                lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(
                        nextId, false));

                Map<String, Object> data = new ConcurrentHashMap();
                data.put("fileId", itemsCheckedList.get(0).getFileId());
                data.put("docId", itemsCheckedList.get(0).getFileId());
                data.put("docType", itemsCheckedList.get(0).getFileType());
                data.put("docStatus", itemsCheckedList.get(0).getStatus());
                data.put("actionId", actions.get(0).getId());
                data.put("actionName", "Xử lý hồ sơ");
                data.put("actionType", actions.get(0).getType());
                data.put("nextId", nextId);
                data.put("previousId", actions.get(0).getPreviousId());
                if (processCurrent != null) {
                    data.put("process", processCurrent);
                }
                data.put("lstAvailableNDU", lstNDUs);
                data.put("parentWindow", cosmeticAll);
                data.put("lstNextAction", lstNextAction);
                data.put("lstDevice", itemsCheckedList);
                createWindow(
                        "MultiProcess",
                        "/Pages/module/importreq/include/payment/processingMutiPaymentPage.zul",
                        data, Window.MODAL);
            } else {
                Map<String, Object> arguments = new ConcurrentHashMap();
                arguments.put("lstDevice", itemsCheckedList);
                arguments.put("form", vmf);
                arguments.put("windowParent", cosmeticAll);
                createWindow("MultiProcess",
                        "/Pages/module/importreq/viewMultiProcess.zul",
                        arguments, Window.MODAL);
            }

        }

    }

    // //////////////////
    @Listen("onChangeTime=#cosmeticAll")
    public void onChangeTime(Event e) throws UnsupportedEncodingException {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            if (dbFromDay != null) {
                dbFromDay.setValue(model.getFromDate());
            }
            if (dbToDay != null) {
                dbToDay.setValue(model.getToDate());
            }
            if (dbFromDayModify != null) {
                dbFromDayModify.setValue(model.getFromDate());
            }
            if (dbToDayModify != null) {
                dbToDayModify.setValue(model.getToDate());
            }

            onSearch();
        }
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", cosmeticAll);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("procedureId", FILE_TYPE);

        arguments.put("deptId", DEPT_ID);
        return arguments;

    }

    @Listen("onOpenCreate=#cosmeticAll")
    public void onOpenCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("CRUDMode", "CREATE");
        setParam(arguments);
        Window window = createWindow("windowCRUD",
                "/Pages/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
        window.setMode(Window.Mode.EMBEDDED);
        // cosmeticAll.setVisible(false);
    }

    @Listen("onSelect =#incList #lbList")
    public void onSelect(Event event) {
        Listbox lb = (Listbox) event.getTarget();
        List<Listitem> li = lb.getItems();
        Set<Listitem> liselect = lb.getSelectedItems();
        // loai bo nhưng item trong lb co trong itemsCheckedList
        for (Listitem itemlb : li) {
            for (VFileImportDevice items : itemsCheckedList) {
                if (((VFileImportDevice) itemlb.getValue()).getFileId().equals(
                        items.getFileId())) {
                    itemsCheckedList.remove(items);
                    break;
                }
            }
        }
        // add item check vao itemsCheckedList
        for (Listitem item : liselect) {
            if (item.getValue() instanceof VFileImportDevice) {
                itemsCheckedList.add((VFileImportDevice) item.getValue());
            }

        }
    }

    @Listen("onOpenView =#incList #lbList")
    public void onOpenView(Event event) throws UnsupportedEncodingException {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        if (obj == null) {
            itemsCheckedList = new ArrayList<>();
            reloadModel(sm);
            return;
        }
        ImportDeviceFiletDAO idDao = new ImportDeviceFiletDAO();
        ImportDeviceFile importDeviceFile = idDao.findById(obj
                .getImportDeviceFileId());
        if (Objects.equals(importDeviceFile.getTypeCreate(),
                Constants.IMPORT.TYPE_CREATE_GIAHAN)) {
            createWindowViewGiahan(obj.getFileId(),
                    Constants.DOCUMENT_MENU.ALL, cosmeticAll);
        } else {
            createWindowView(obj.getFileId(), Constants.DOCUMENT_MENU.ALL,
                    cosmeticAll);
        }

        cosmeticAll.setVisible(false);
    }

    // Cuongvv
    @Listen("onOpenClick =#incList #lbList")
    public void onOpenClick(Event event) throws UnsupportedEncodingException {
        onSelected(event);
    }

    @Listen("onCheck =#incList #lbList")
    public void onSelected(Event event) throws UnsupportedEncodingException {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        if (obj == null) {
            itemsCheckedList = new ArrayList<>();
            reloadModel(sm);
            return;
        }

        FILE_TYPE = obj.getFileType();
        Long docType = FILE_TYPE;

        DEPT_ID = Constants.VU_TBYT_ID;

        Long deptId = DEPT_ID;

        Long userId = getUserId();
        Long docId = obj.getFileId();

        List<com.viettel.core.workflow.BO.Process> lstProcess = WorkflowAPI
                .getInstance().getProcess(docId, docType, userId);
        List<com.viettel.core.workflow.BO.Process> lstAllProcess = WorkflowAPI
                .getInstance().getAllProcessNotFinish(docId, docType);
        if ((lstAllProcess == null || lstAllProcess.isEmpty())
                && obj.getStatus() != Constants.FILE_STATUS_CODE.STATUS_MOITAO) {
            return;
        }
        List<Long> lstStatus = new ArrayList();

        if (obj.getFinishDate() == null) {
            lstStatus.add(obj.getStatus());
        }
        if (lstStatus.isEmpty() && !lstProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }
        if (lstStatus.isEmpty() && lstAllProcess != null
                && !lstAllProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }
        //linhdx - Neu khong co status nao thi dat trang thai khoi tao
        if (lstStatus.isEmpty()) {
            Long statusNewObj = Constants.FILE_STATUS_CODE.STATUS_DN_DANOP_TTB;
            lstStatus.add(statusNewObj);
        }

        List<NodeToNode> actions = WorkflowAPI.getInstance()
                .findAvaiableNextActions(docType, lstStatus, deptId);
        NodeToNode action = actions.get(0);
        lstNextAction = actions;

        final String actionName = "Xử lý hồ sơ";
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action
        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = obj.getStatus();
        final List<NodeToNode> lstNextAction1 = lstNextAction;

        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance()
                .findNDUsByNodeId(nextId, false));
        Map<String, Object> data = new ConcurrentHashMap<>();

        data.put("fileId", obj.getFileId());
        data.put("docId", obj.getFileId());
        data.put("docType", obj.getFileType());
        data.put("docStatus", status);
        data.put("actionId", actionId);
        data.put("actionName", actionName);
        data.put("actionType", actionType);
        data.put("nextId", nextId);
        data.put("previousId", prevId);
        if (processCurrent != null) {
            data.put("process", processCurrent);
        }
        data.put("lstAvailableNDU", lstNDUs);
        // linhdx sua loi cuong vv
        data.put("parentWindow", cosmeticAll);
        // Cuongvv
        // if (obj.getStatus().equals(statusNewObj)) {
        // data.put("parentWindow", cosmeticAll);
        // } else {
        // data.put("parentWindow", windowView);
        // }
        data.put("lstNextAction", lstNextAction1);
        createWindow("windowComment", formName, data, Window.MODAL);
    }

    // ------------------
    @Listen("onLock =#incList #lbList")
    public void onLock(Event event) throws UnsupportedEncodingException {
        final VFileImportDevice obj = (VFileImportDevice) event.getData();
        if (obj == null) {
            itemsCheckedList = new ArrayList<>();
            reloadModel(sm);
            return;
        }

        Messagebox.show(getLabelName("presentation_confirm_lock"),
                getLabelName("common_notification_title"), Messagebox.OK
                | Messagebox.CANCEL, Messagebox.EXCLAMATION,
                new EventListener() {

                    @Override
                    public void onEvent(Event event)
                    throws InterruptedException {
                        try {
                            if (Messagebox.ON_OK.equals(event.getName())) {
                                deleteFile(obj);
                            }
                        } catch (Exception ex) {
                            LogUtils.addLogDB(ex);
                        }
                    }
                });
        // Map<String, Object> arguments = new ConcurrentHashMap<>();
        // if (obj.getStatus().equals(31000064L)) {
        // arguments.put("model", "view");
        // } else {
        // arguments.put("model", "delete");
        // }
        // arguments.put("vfile", obj);
        // arguments.put("cosmeticAll", cosmeticAll);
        // createWindow("businessWindow",
        // "/Pages/module/importreq/comonListImport/deletePermit.zul",
        // arguments,
        // Window.MODAL);
    }

    @Listen("onCreateComment =#incList #lbList")
    public void onCreateComment(Event event) {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        Map args = new ConcurrentHashMap();
        args.put("object", obj);
        createWindow(
                "flowConfigDlg",
                "/Pages/module/importreq/include/memberboard/memberBoardCreateComment.zul",
                args, Window.MODAL);

    }

    @Listen("onOpenComment =#incList #lbList")
    public void onOpenComment(Event event) {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        Map args = new ConcurrentHashMap();
        args.put("object", obj);
        createWindow(
                "flowConfigDlg",
                "/Pages/module/importreq/include/memberboard/memberBoardViewComment.zul",
                args, Window.MODAL);

    }

    @Listen("onRefresh =#cosmeticAll")
    public void onRefresh(Event event) throws UnsupportedEncodingException {
        onOpenView(event);
    }

    @Listen("onSelectedProcess = #cosmeticAll")
    public void onSelectedProcess(Event event) {
        cosmeticAll.setVisible(false);

    }

    private void createWindowView(Long id, int menuType, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView",
                "/Pages/module/importreq/importdeviceView.zul", arguments,
                Window.EMBEDDED);
    }

    private void createWindowViewGiahan(Long id, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView",
                "/Pages/module/importreq/importdeviceViewGiahan.zul",
                arguments, Window.EMBEDDED);
    }

    @Listen("onSave = #cosmeticAll")
    public void onSave(Event event) {
        cosmeticAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onVisible =  #cosmeticAll")
    public void onVisible() throws UnsupportedEncodingException {
        reloadModel(lastSearchModel);
        cosmeticAll.setVisible(true);
    }

    @Listen("onOpenUpdate = #incList #lbList")
    public void onOpenUpdate(Event event) {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowCRUDRapidTest",
                "/Pages/module/importreq/importdevice.zul", arguments,
                Window.EMBEDDED);
        cosmeticAll.setVisible(false);
    }

    @Listen("onDownLoadGiayPhep = #incList #lbList")
    public void onDownLoadGiayPhep(Event event) throws FileNotFoundException,
            IOException {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        Long fileid = obj.getFileId();
        ListImportDeviceDAO cosDao = new ListImportDeviceDAO();
        CategoryDAOHE catDaohe = new CategoryDAOHE();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        // co giay phep
        if (check == 1) {
            AttachDAOHE attachDaoHe = new AttachDAOHE();
            Long cospermit = Long.parseLong(catDaohe.LayTruongPermitId(fileid));

            Attachs attachs = attachDaoHe.getByObjectIdAndAttachCat(cospermit,
                    Constants.OBJECT_TYPE.COSMETIC_PERMIT);
            // Nếu có file
            if (attachs != null) {
                // attDAO.downloadFileAttach(attachs);

                String path = attachs.getFullPathFile();
                File f = new File(path);
                // neu ton tai
                if (f.exists()) {
                    File tempFile = FileUtil.createTempFile(f,
                            attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                } // Khong se tao file
                else {
                    String folderPath = ResourceBundleUtil
                            .getString("dir_upload");
                    FileUtil.mkdirs(folderPath);
                    String separator = ResourceBundleUtil
                            .getString("separator");
                    folderPath += separator
                            + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
                    File fd = new File(folderPath);
                    if (!fd.exists()) {
                        fd.mkdirs();
                    }
                    f = new File(attachs.getFullPathFile());
                    if (f.exists()) {
                    } else {
                        f.createNewFile();
                    }
                    File tempFile = FileUtil.createTempFile(f,
                            attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                }
            } // chua có file sẽ tạo trên hệ thông
            else {
                showNotification("File không còn tồn tại trên hệ thống!");
            }
        }
    }

    @Listen("onViewFlow = #lbList")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    @Listen("onDelete = #incList #lbList")
    public void onDelete(Event event) {
        final VFileImportDevice obj = (VFileImportDevice) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {
                                        if (isAbleToDelete(obj)) {
                                            FilesDAOHE objDAOHE = new FilesDAOHE();
                                            objDAOHE.delete(obj.getFileId());
                                            onSearch();
                                            showNotification(
                                                    String.format(
                                                            Constants.Notification.DELETE_SUCCESS,
                                                            Constants.DOCUMENT_TYPE_NAME.FILE),
                                                    Constants.Notification.INFO);
                                        } else {
                                            showNotification("Bạn không có quyền xóa");
                                        }

                                    } catch (Exception ex) {
                                        showNotification(
                                                String.format(
                                                        Constants.Notification.DELETE_ERROR,
                                                        Constants.DOCUMENT_TYPE_NAME.FILE),
                                                Constants.Notification.ERROR);
                                        LogUtils.addLogDB(ex);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    @Listen("onClick=#incSearchFullForm #btnReset")
    public void doResetForm() throws UnsupportedEncodingException {
        tbEquipmentNo.setValue("");
        tbImportName.setValue("");

        tbNSWFileCode.setValue("");
        if (dbFromDay != null) {
            dbFromDay.setValue(null);
        }
        if (dbToDay != null) {
            dbToDay.setValue(new Date());
        }
        if (dbFromDayModify != null) {
            dbFromDayModify.setValue(null);
        }
        if (dbToDayModify != null) {
            dbToDayModify.setValue(new Date());
        }
        lboxStatus.setSelectedIndex(0);

        onSearch();
    }

    @Listen("onCopy = #incList #lbList")
    public void onCopy(Event event) {

        VFileImportDevice obj = (VFileImportDevice) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "COPY");
        setParam(arguments);
        createWindow("windowCRUDCosmetic",
                "/Pages/module/importreq/importdevice.zul", arguments,
                Window.EMBEDDED);
        cosmeticAll.setVisible(false);

    }

    public String getSendDate(VFileImportDevice f) {
        SimpleDateFormat formatterDateTime = new SimpleDateFormat("dd-MM-yyyy");
        String rs = f.getStartDate() == null ? formatterDateTime.format(f
                .getCreateDate()) : formatterDateTime.format(f.getStartDate());
        if (sm != null) {
            ProcessDAOHE pDAO = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process p = pDAO
                    .getLast(f.getFileId());
            if (p != null) {
                rs = formatterDateTime.format(p.getSendDate());
            }
        }
        return rs;
    }

    public String getStartDate(VFileImportDevice f) {
        SimpleDateFormat formatterDateTime = new SimpleDateFormat("dd-MM-yyyy");
        String rs = f.getStartDate() == null ? formatterDateTime.format(f
                .getCreateDate()) : formatterDateTime.format(f.getStartDate());
        return rs;
    }

    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(VFileImportDevice obj) {
        if (user.getUserId() != null
                && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(),
                    Constants.IDF_PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(),
                            Constants.IDF_PROCESS_STATUS.VT_RETURN))) {
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean isAbleToLock(VFileImportDevice obj)
            throws UnsupportedEncodingException {
        FilesDAOHE fDAO = new FilesDAOHE();
        Files f = fDAO.findById(obj.getFileId());
        Long userId = getUserId();
        Long deptId = getDeptId();
        RoleUserDeptDAOHE r = new RoleUserDeptDAOHE();
        Boolean check = r.checkRoleCanDeleteFile(userId, deptId);
        return (check && f.getFinishDate() != null && f.getStatus().equals(
                31000063L));
    }

    public boolean isAbleToLock_2(VFileImportDevice obj) {
        FilesDAOHE fDAO = new FilesDAOHE();
        Files f = fDAO.findById(obj.getFileId());
        return (f.getFinishDate() != null && f.getStatus().equals(31000064L));
    }

    public boolean isAbleToModify(VFileImportDevice obj) {
        if (user.getUserId() != null
                && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(),
                    Constants.IDF_PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(),
                            Constants.IDF_PROCESS_STATUS.SENT_DISPATCH))
                    || (Objects.equals(obj.getStatus(),
                            Constants.IDF_PROCESS_STATUS.CV_SENT_RETURN))
                    || (Objects.equals(obj.getStatus(),
                            Constants.IDF_PROCESS_STATUS.SENT_RETURN))
                    || (Objects.equals(obj.getStatus(),
                            Constants.IDF_PROCESS_STATUS.VT_RETURN))
                    || (Objects.equals(obj.getStatus(),
                            Constants.IDF_PROCESS_STATUS.TKHD_RETURN))) {
                return true;
            }
            return false;
        }
        return false;
    }

    public String getStatus(Long status) {
        for (Category c : lstStatusAll) {
            if (status.toString().equals(c.getValue())) {
                return c.getName();
            }
        }
        return WorkflowAPI.getStatusName(status);
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public int CheckGiayPhep(Long fileid) {
        ListImportDeviceDAO cosDao = new ListImportDeviceDAO();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        return check;
    }

    public int checkDispathSDBS(Long fileid) {
        ListImportDeviceDAO cosDao = new ListImportDeviceDAO();
        int check = cosDao.checkDispathSDBS(fileid, getUserId());
        return check;
    }

    public int checkUserShowExport() {
        int checkView = 0;
        UserDAOHE dao = new UserDAOHE();
        Users user = dao.getUserById(getUserId());
        if (Constants.ROLES.CHUCVU_THUKYHOIDDONG.equals(user.getPosId())) {
            checkView = 1;
        }

        return checkView;
    }

    public int checkUserShowCommentTVHD() {
        int checkView = 0;
        UserDAOHE dao = new UserDAOHE();
        Users user = dao.getUserById(getUserId());
        if (user.getPosId().equals(Constants.ROLES.CHUCVU_THANHVIENHOIDDONG)) {
            checkView = 1;
        }

        return checkView;
    }

    @Listen("onViewSDBS = #incList #lbList")
    public void onViewSDBS(Event event) throws FileNotFoundException,
            IOException {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowViewSDBS",
                "/Pages/module/importreq/flow/viewDispatch.zul", arguments,
                Window.MODAL);

    }

    public Constants getConstants() {
        return constants;
    }

    public void setConstants(Constants constants) {
        this.constants = constants;
    }

    public Long getOldDeadlineWarning(VFileImportDevice f) {
        if (f.getFinishDate() != null) {
            return Constants.WARNING.DEADLINE_ON;
        }
        Long value = Constants.WARNING.DEADLINE_ON;
        ProcessDAOHE pDAO = new ProcessDAOHE();
        com.viettel.core.workflow.BO.Process p = pDAO.getLastByUserId(
                f.getFileId(), UserId);
        if (p != null) {
            Date sendDate = p.getSendDate();

            Date exceptedDate = new Date();

            if (sendDate != null
                    && menuTypeStr.equals(Constants.MENU_TYPE.WAITPROCESS_STR)) {
                SimpleDateFormat formatterDateTime = new SimpleDateFormat(
                        "yyyyMMdd");
                if (Long.valueOf(formatterDateTime.format(exceptedDate)) > Long
                        .valueOf(formatterDateTime.format(sendDate)) + 4) {//Lon hon 4 ngay
                    value = Constants.WARNING.DEADLINE_MISS;
                }

            }
        }

        return value;
    }

    public String getDocumentTypeCode(Long documentTypeCode) {
        if (documentTypeCode != null) {
            if (Objects.equals(documentTypeCode,
                    Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI)) {
                return Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI_STR;
            }
        }
        return "";
    }

    public String getTypeCreate(Long typeCreate) {
        String typeCreateStr = Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI_STR;
        if (typeCreate != null) {
            if (Objects.equals(typeCreate, Constants.IMPORT.TYPE_CREATE_MOI)) {
                typeCreateStr = Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI_STR;
            } else if (Objects.equals(typeCreate,
                    Constants.IMPORT.TYPE_CREATE_DIEUCHINH)) {
                typeCreateStr = Constants.IMPORT.DOCUMENT_TYPE_CODE_DIEUCHINH_STR;
            } else if (Objects.equals(typeCreate,
                    Constants.IMPORT.TYPE_CREATE_GIAHAN)) {
                typeCreateStr = Constants.IMPORT.DOCUMENT_TYPE_GIAHAN_STR;
            }
        }
        return typeCreateStr;
    }

    public void deleteFile(VFileImportDevice obj)
            throws UnsupportedEncodingException, Exception {
        FilesDAOHE fileDAOHE = new FilesDAOHE();
        Files file = fileDAOHE.findById(obj.getFileId());
        CategoryDAOHE catDAOHE = new CategoryDAOHE();
        Category cat = catDAOHE.findCategory(
                ResourceBundleUtil.getString("STATUS_ID_DEL_PERMIT", "config"),
                Constants.CATEGORY_TYPE.FILE_STATUS);
        file.setStatus(Long.valueOf(cat.getValue()));

        Helper_VofficeMoh hpMoh = new Helper_VofficeMoh();
        Boolean isOK = hpMoh.SendMs_45(obj, "Hủy giấy phép");
        if (isOK) {
            fileDAOHE.saveOrUpdate(file);
            showNotification("Hủy thành công", Constants.Notification.WARNING,
                    2000);
        } else {
            showNotification("Hủy không thành công",
                    Constants.Notification.WARNING, 2000);
        }
    }

    /*
     * Xuat danh sach ho so linhdx 18/05/2016
     */
    @Listen("onClick = #incSearchFullForm #btnExportDS")
    public void onExportDS() throws Docx4JException, FileNotFoundException,
            IOException, JAXBException {
        ListImportDeviceDAO objDAOHE = new ListImportDeviceDAO();
        PagingListModel plm = objDAOHE
                .findFilesByReceiverAndDeptId(lastSearchModel, getUserId(),
                        getDeptId(), 0, Integer.MAX_VALUE);
        List<VFileImportDevice> lst = plm.getLstReturn();
        List<VFileImportDeviceModel> lstModel = new ArrayList();
        if (lst != null && lst.size() > 0) {
            for (int i = 0; i < lst.size(); i++) {
                VFileImportDevice obj = lst.get(i);
                String sendDate = getStartDate(obj);
                String status = getStatus(obj.getStatus());
                String documentTypeCodeStr = getDocumentTypeCode(obj
                        .getDocumentTypeCode());
                VFileImportDeviceModel model = new VFileImportDeviceModel();
                model.setvFileImportDevice(lst.get(i));
                model.setSendDate(sendDate);
                model.setStatusStr(status);
                model.setDocumentTypeCodeStr(documentTypeCodeStr);
                model.setRowNum(i);
                lstModel.add(model);
            }
        }
        final List data = lstModel;

        final ExcelExporter exporter = new ExcelExporter(8);
        exporter.setDataSheetName("Sheet1");
        exporter.setInterceptor(new Interceptor<XSSFWorkbook>() {
            @Override
            public void beforeRendering(XSSFWorkbook book) {
                ExcelExporter.ExportContext context = exporter
                        .getExportContext();
                XSSFSheet sheet = context.getSheet();

                // Font tiêu đề
                XSSFFont fontTitle = book.createFont();
                fontTitle.setFontHeightInPoints((short) 15);
                fontTitle.setFontName("Times New Roman");
                fontTitle.setColor(IndexedColors.BLACK.getIndex());
                fontTitle.setBold(true);
                fontTitle.setItalic(false);

                Row row = exporter.getOrCreateRow(context.moveToNextRow(),
                        sheet);
                Cell cell = exporter.getOrCreateCell(context.moveToNextCell(),
                        sheet);
                cell.setCellValue("Danh sách hồ sơ Nhập khẩu trang thiết bị y tế");
                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontTitle);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 7));
                // Font cho thời gian
                XSSFFont fontTime = book.createFont();
                fontTime.setFontHeightInPoints((short) 13);
                fontTime.setFontName("Times New Roman");
                fontTime.setColor(IndexedColors.BLACK.getIndex());
                fontTime.setBold(false);
                fontTime.setItalic(true);

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                row = exporter.getOrCreateRow(context.moveToNextRow(), sheet);
                cell = exporter.getOrCreateCell(context.moveToNextCell(), sheet);
                String value = "Thời gian từ ngày %s đến ngày %s";
                String fromDay = "";
                if (dbFromDayModify.getValue() != null) {
                    fromDay = format.format(dbFromDayModify.getValue());
                }
                String toDay = "";
                if (dbToDayModify.getValue() != null) {
                    toDay = format.format(dbToDayModify.getValue());
                }
                cell.setCellValue(String.format(value, fromDay, toDay));
                cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontTime);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 7));

                context.setRowHeaderIndex(2);

                // Row heading
                exporter.getOrCreateRow(context.moveToNextRow(), sheet);
                // Font cho header
                XSSFFont fontHeader = book.createFont();
                fontHeader.setFontHeightInPoints((short) 12);
                fontHeader.setFontName("Times New Roman");
                fontHeader.setColor(IndexedColors.BLACK.getIndex());
                fontHeader.setBold(true);
                fontHeader.setItalic(false);
                // Font cho nội dung
                XSSFFont fontContent = book.createFont();
                fontContent.setFontHeightInPoints((short) 11);
                fontContent.setFontName("Times New Roman");
                fontContent.setColor(IndexedColors.BLACK.getIndex());
                fontContent.setBold(false);
                fontContent.setItalic(false);
                // CellStyle cho headers
                cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontHeader);

                String[] headerNames = {"STT", "Mã HS", "Tên Doanh nghiệp",
                    "Tên thiết bị", "Loại hồ sơ", "Ngày đến",
                    "Ngày cập nhật", "Trạng thái"};
                for (int i = 0; i < headerNames.length; i++) {
                    cell = exporter.getOrCreateCell(context.moveToNextCell(),
                            context.getSheet());
                    cell.setCellValue(headerNames[i]);
                    cell.setCellStyle(cellStyle);
                }
                // Set CellStyle cho các cột
                CellStyle csCenter = sheet.getWorkbook().createCellStyle();
                csCenter.setAlignment(CellStyle.ALIGN_CENTER);
                csCenter.setFont(fontContent);
                exporter.setCellstyle(0, csCenter);
                exporter.setCellstyle(1, csCenter);
                CellStyle csLeft = sheet.getWorkbook().createCellStyle();
                csLeft.setAlignment(CellStyle.ALIGN_LEFT);
                csLeft.setFont(fontContent);
                exporter.setCellstyle(2, csLeft);
                exporter.setCellstyle(3, csLeft);
                exporter.setCellstyle(4, csLeft);
                exporter.setCellstyle(5, csLeft);
                exporter.setCellstyle(6, csLeft);
                exporter.setCellstyle(7, csLeft);
            }

            @Override
            public void afterRendering(XSSFWorkbook book) {
                ExcelExporter.ExportContext context = exporter
                        .getExportContext();
                XSSFSheet sheet = context.getSheet();

                XSSFFont fontFooter = book.createFont();
                fontFooter.setFontHeightInPoints((short) 11);
                fontFooter.setFontName("Times New Roman");
                fontFooter.setColor(IndexedColors.BLACK.getIndex());
                fontFooter.setBold(true);
                fontFooter.setItalic(false);

                Row row = exporter.getOrCreateRow(context.moveToNextRow(),
                        sheet);
                Cell cell = exporter.getOrCreateCell(context.moveToNextCell(),
                        sheet);
                cell.setCellValue("Có tổng số: " + data.size() + " hồ sơ");
                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
                cellStyle.setFont(fontFooter);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 7));
            }
        });

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            exporter.export(8, data,
                    new RowRenderer<Row, VFileImportDeviceModel>() {

                        @Override
                        public void render(Row row,
                                VFileImportDeviceModel vFileImportOrderModel,
                                boolean oddRow) {
                            final ExcelExporter.ExportContext context = exporter
                            .getExportContext();
                            final XSSFSheet sheet = context.getSheet();

                            Cell cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(row.getRowNum()
                                    - context.getRowHeaderIndex());
                            cell.setCellStyle(exporter.getCellstyle(0));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getvFileImportDevice().getNswFileCode());
                            cell.setCellStyle(exporter.getCellstyle(1));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getvFileImportDevice().getBusinessName());
                            cell.setCellStyle(exporter.getCellstyle(2));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getvFileImportDevice().getProductName());
                            cell.setCellStyle(exporter.getCellstyle(3));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getDocumentTypeCodeStr());
                            cell.setCellStyle(exporter.getCellstyle(4));
							// String startDateStr="";

                            // if(vFileImportOrderModel.getvFileImportDevice()!=null
                            // &&
                            // vFileImportOrderModel.getvFileImportDevice().getStartDate()
                            // !=null){
                            // startDateStr =
                            // DateTimeUtils.convertDateToString(vFileImportOrderModel.getvFileImportDevice().getStartDate())
                            // ;
                            //
                            // }
                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getSendDate());
                            cell.setCellStyle(exporter.getCellstyle(5));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            String modifyDateStr = "";
                            if (vFileImportOrderModel.getvFileImportDevice() != null
                            && vFileImportOrderModel
                            .getvFileImportDevice()
                            .getModifyDate() != null) {
                                modifyDateStr = DateTimeUtils
                                .convertDateToString(vFileImportOrderModel
                                        .getvFileImportDevice()
                                        .getModifyDate());
                            }
                            cell.setCellValue(modifyDateStr);
                            cell.setCellStyle(exporter.getCellstyle(6));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getStatusStr());
                            cell.setCellStyle(exporter.getCellstyle(7));
                        }

                    }, out);
            Calendar now = Calendar.getInstance();
            int day = now.get(Calendar.DAY_OF_MONTH);
            int month = now.get(Calendar.MONTH);
            int year = now.get(Calendar.YEAR);
            String name = "Bao cao-" + day + "-" + (month + 1) + "-" + year
                    + ".xlsx";
            AMedia amedia = new AMedia(name, "xls", "application/file",
                    out.toByteArray());
            Filedownload.save(amedia);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }

    }

    /*
     * Xuat danh sach san pham linhdx 18/05/2016
     */
    @Listen("onClick = #incSearchFullForm #btnExportProductDS")
    public void btnExportProductDS() throws Docx4JException,
            FileNotFoundException, IOException, JAXBException {
        ListImportDeviceDAO objDAOHE = new ListImportDeviceDAO();
        PagingListModel plm = objDAOHE
                .findFilesByReceiverAndDeptId(lastSearchModel, getUserId(),
                        getDeptId(), 0, Integer.MAX_VALUE);
        List<VFileImportDeviceModel> lst = plm.getLstReturn();
        List<VFileImportDeviceModel> lstModel = new ArrayList();
        if (lst != null && lst.size() > 0) {
            for (int i = 0; i < lst.size(); i++) {
                VFileImportDeviceModel obj = lst.get(i);
                String startDateStr = getStartDate(obj.getvFileImportDevice());
                String sendDate = getSendDate(obj.getvFileImportDevice());
                String status = getStatus(obj.getvFileImportDevice()
                        .getStatus());
                String documentTypeCodeStr = getDocumentTypeCode(obj
                        .getvFileImportDevice().getDocumentTypeCode());
                VFileImportDeviceModel model = new VFileImportDeviceModel();
                model.setvFileImportDevice(lst.get(i).getvFileImportDevice());
                model.setImportDeviceProduct(lst.get(i)
                        .getImportDeviceProduct());
                model.setStartDateStr(startDateStr);
                model.setSendDate(sendDate);
                model.setStatusStr(status);
                model.setDocumentTypeCodeStr(documentTypeCodeStr);
                model.setRowNum(i);

                String c4 = "";
                String c5 = "";
                String c6 = "";

                ImportDeviceProduct product = model.getImportDeviceProduct();
                c4 += (product.getManufacturer() == null ? "" : product
                        .getManufacturer());
                if (!c4.contains("phụ lục đính kèm") && !"".equals(c4)) {
                    c4 += (product.getNationalName() == null ? " " : ", "
                            + product.getNationalName())
                            + "\r\n";
                }

                c5 += (product.getDistributor() == null ? "" : product
                        .getDistributor());
                if (!c5.contains("phụ lục đính kèm")) {
                    c5 += (product.getNationalDistributorName() == null ? " "
                            : ", " + product.getNationalDistributorName())
                            + "\r\n";
                }
                c6 += (product.getProductManufacture() == null ? "" : product
                        .getProductManufacture());
                if (!c6.contains("phụ lục đính kèm")) {
                    c6 += (product.getNationalProduct() == null ? " " : ", "
                            + product.getNationalProduct())
                            + "\r\n";
                }
                model.setC4(c4);
                model.setC5(c5);
                model.setC6(c6);
                lstModel.add(model);
            }
        }
        final List data = lstModel;

        final ExcelExporter exporter = new ExcelExporter(12);
        exporter.setDataSheetName("Sheet1");
        exporter.setInterceptor(new Interceptor<XSSFWorkbook>() {
            @Override
            public void beforeRendering(XSSFWorkbook book) {
                ExcelExporter.ExportContext context = exporter
                        .getExportContext();
                XSSFSheet sheet = context.getSheet();

                // Font tiêu đề
                XSSFFont fontTitle = book.createFont();
                fontTitle.setFontHeightInPoints((short) 15);
                fontTitle.setFontName("Times New Roman");
                fontTitle.setColor(IndexedColors.BLACK.getIndex());
                fontTitle.setBold(true);
                fontTitle.setItalic(false);

                Row row = exporter.getOrCreateRow(context.moveToNextRow(),
                        sheet);
                Cell cell = exporter.getOrCreateCell(context.moveToNextCell(),
                        sheet);
                cell.setCellValue("Danh sách Thiết bị Nhập khẩu");
                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontTitle);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 11));
                // Font cho thời gian
                XSSFFont fontTime = book.createFont();
                fontTime.setFontHeightInPoints((short) 13);
                fontTime.setFontName("Times New Roman");
                fontTime.setColor(IndexedColors.BLACK.getIndex());
                fontTime.setBold(false);
                fontTime.setItalic(true);

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                row = exporter.getOrCreateRow(context.moveToNextRow(), sheet);
                cell = exporter.getOrCreateCell(context.moveToNextCell(), sheet);
                String value = "Thời gian từ ngày %s đến ngày %s";
                String fromDay = "";
                if (dbFromDayModify.getValue() != null) {
                    fromDay = format.format(dbFromDayModify.getValue());
                }
                String toDay = "";
                if (dbToDayModify.getValue() != null) {
                    toDay = format.format(dbToDayModify.getValue());
                }
                cell.setCellValue(String.format(value, fromDay, toDay));
                cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontTime);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 11));

                context.setRowHeaderIndex(2);

                // Row heading
                exporter.getOrCreateRow(context.moveToNextRow(), sheet);
                // Font cho header
                XSSFFont fontHeader = book.createFont();
                fontHeader.setFontHeightInPoints((short) 12);
                fontHeader.setFontName("Times New Roman");
                fontHeader.setColor(IndexedColors.BLACK.getIndex());
                fontHeader.setBold(true);
                fontHeader.setItalic(false);
                // Font cho nội dung
                XSSFFont fontContent = book.createFont();
                fontContent.setFontHeightInPoints((short) 11);
                fontContent.setFontName("Times New Roman");
                fontContent.setColor(IndexedColors.BLACK.getIndex());
                fontContent.setBold(false);
                fontContent.setItalic(false);
                // CellStyle cho headers
                cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontHeader);

                String[] headerNames = {"STT", "Mã HS", "Tên Doanh nghiệp",
                    "Tên thiết bị", "Model", "Hãng, nước sản xuất",
                    "Hãng nước chủ sở hữu", "Hãng nước phân phối",
                    "Loại hồ sơ", "Ngày đến", "Ngày cập nhật", "Trạng thái"};
                for (int i = 0; i < headerNames.length; i++) {
                    cell = exporter.getOrCreateCell(context.moveToNextCell(),
                            context.getSheet());
                    cell.setCellValue(headerNames[i]);
                    cell.setCellStyle(cellStyle);
                }
                // Set CellStyle cho các cột
                CellStyle csCenter = sheet.getWorkbook().createCellStyle();
                csCenter.setAlignment(CellStyle.ALIGN_CENTER);
                csCenter.setFont(fontContent);
                exporter.setCellstyle(0, csCenter);
                exporter.setCellstyle(1, csCenter);
                CellStyle csLeft = sheet.getWorkbook().createCellStyle();
                csLeft.setAlignment(CellStyle.ALIGN_LEFT);
                csLeft.setFont(fontContent);
                exporter.setCellstyle(2, csLeft);
                exporter.setCellstyle(3, csLeft);
                exporter.setCellstyle(4, csLeft);
                exporter.setCellstyle(5, csLeft);
                exporter.setCellstyle(6, csLeft);
                exporter.setCellstyle(7, csLeft);
                exporter.setCellstyle(8, csLeft);
                exporter.setCellstyle(9, csLeft);
                exporter.setCellstyle(10, csLeft);
                exporter.setCellstyle(11, csLeft);
            }

            @Override
            public void afterRendering(XSSFWorkbook book) {
                ExcelExporter.ExportContext context = exporter
                        .getExportContext();
                XSSFSheet sheet = context.getSheet();

                XSSFFont fontFooter = book.createFont();
                fontFooter.setFontHeightInPoints((short) 11);
                fontFooter.setFontName("Times New Roman");
                fontFooter.setColor(IndexedColors.BLACK.getIndex());
                fontFooter.setBold(true);
                fontFooter.setItalic(false);

                Row row = exporter.getOrCreateRow(context.moveToNextRow(),
                        sheet);
                Cell cell = exporter.getOrCreateCell(context.moveToNextCell(),
                        sheet);
                cell.setCellValue("Có tổng số: " + data.size() + " thiết bị");
                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
                cellStyle.setFont(fontFooter);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 7));
            }
        });

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            exporter.export(11, data,
                    new RowRenderer<Row, VFileImportDeviceModel>() {

                        @Override
                        public void render(Row row,
                                VFileImportDeviceModel vFileImportOrderModel,
                                boolean oddRow) {
                            final ExcelExporter.ExportContext context = exporter
                            .getExportContext();
                            final XSSFSheet sheet = context.getSheet();

                            Cell cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(row.getRowNum()
                                    - context.getRowHeaderIndex());
                            cell.setCellStyle(exporter.getCellstyle(0));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getvFileImportDevice().getNswFileCode());
                            cell.setCellStyle(exporter.getCellstyle(1));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getvFileImportDevice().getBusinessName());
                            cell.setCellStyle(exporter.getCellstyle(2));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getvFileImportDevice().getProductName());
                            cell.setCellStyle(exporter.getCellstyle(3));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getImportDeviceProduct().getModel());
                            cell.setCellStyle(exporter.getCellstyle(4));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel.getC4());
                            cell.setCellStyle(exporter.getCellstyle(5));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel.getC5());
                            cell.setCellStyle(exporter.getCellstyle(6));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel.getC6());
                            cell.setCellStyle(exporter.getCellstyle(7));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getDocumentTypeCodeStr());
                            cell.setCellStyle(exporter.getCellstyle(8));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getStartDateStr());
                            cell.setCellStyle(exporter.getCellstyle(9));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            String modifyDateStr = "";
                            if (vFileImportOrderModel.getvFileImportDevice() != null
                            && vFileImportOrderModel
                            .getvFileImportDevice()
                            .getModifyDate() != null) {
                                modifyDateStr = DateTimeUtils
                                .convertDateToString(vFileImportOrderModel
                                        .getvFileImportDevice()
                                        .getModifyDate());
                            }
                            cell.setCellValue(modifyDateStr);
                            // cell.setCellValue(vFileImportOrderModel.getSendDate());
                            cell.setCellStyle(exporter.getCellstyle(10));

                            cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(vFileImportOrderModel
                                    .getStatusStr());
                            cell.setCellStyle(exporter.getCellstyle(11));
                        }

                    }, out);
            Calendar now = Calendar.getInstance();
            int day = now.get(Calendar.DAY_OF_MONTH);
            int month = now.get(Calendar.MONTH);
            int year = now.get(Calendar.YEAR);
            String name = "Bao cao-" + day + "-" + (month + 1) + "-" + year
                    + ".xlsx";
            AMedia amedia = new AMedia(name, "xls", "application/file",
                    out.toByteArray());
            Filedownload.save(amedia);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }

    }

    public boolean isAbleToModifyStatus() {
        boolean result = false;
        if ("admin".equals(getUserName())) {
            result = true;
        }
        return result;
    }

    @Listen("onUpdateSendMessage =#incList #lbList")
    public void onUpdateSendMessage(Event event)
            throws UnsupportedEncodingException, Exception {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        if (obj == null) {
            return;
        }
        updateSendMessage(obj.getFileId());
    }

    public void updateSendMessage(Long fileId) throws Exception {
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!sendMS(fileId)) {
                showNotification("Lỗi gửi kết quả xử lý sang NSW");
            } else {
                FilesDAOHE fhe = new FilesDAOHE();
                fhe.commit();
                showSuccessNotification("Gui thanh cong");
            }

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    private Boolean sendMS(Long fileId) throws Exception {
        Helper_VofficeMoh hp = new Helper_VofficeMoh();
        return hp.SendMs_39(fileId, fileId, 1L);
    }

    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case 13:
                onSearch();
                break;

        }
    }
}
