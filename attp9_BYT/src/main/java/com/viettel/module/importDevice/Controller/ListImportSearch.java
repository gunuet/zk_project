/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author MrBi
 */
public class ListImportSearch extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    private Datebox dbFromDay, dbToDay;

    @Wire
    private Textbox tbNSWFileCode, tbRapidTestNo;
    @Wire
    private Listbox lboxStatus,lboxDocumentTypeCode,lboxOrder;
    
    @Wire
    private Groupbox fullSearchGbx;


    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
//        dbFromDay.setConstraint(new DateConstraint(dbToDay, "before"));
//        dbToDay.setConstraint(new DateConstraint(dbFromDay, "after"));
    }

//    @Listen("onChange = #dbFromDay, #dbToDay")
//    public void onChangeDate(InputEvent event) {
//        LocalDate fromDate = LocalDate.fromDateFields(dbFromDay.getValue());
//        LocalDate toDate = LocalDate.fromDateFields(dbToDay.getValue());
//        if (toDate.isBefore(fromDate)) {
//            if (event.getTarget().equals(dbFromDay)) {
//                throw new WrongValueException(event.getTarget(),
//                        "Giá trị trong trường Từ ngày không được sau trường Đến ngày");
//            } else if (event.getTarget().equals(dbToDay)) {
//                throw new WrongValueException(event.getTarget(),
//                        "Giá trị trong trường Đến ngày không được trước trường Từ ngày");
//            }
//        }
//    }
}
