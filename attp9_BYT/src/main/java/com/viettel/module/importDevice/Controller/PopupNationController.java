package com.viettel.module.importDevice.Controller;

import java.awt.ItemSelectable;
import java.io.Console;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.jpamodelgen.util.StringUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.google.common.base.Joiner;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.Place;
import com.viettel.core.sys.DAO.PlaceDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.StringUtils;

import org.zkoss.bind.converter.sys.ListboxSelectedItemsConverter;
import org.zkoss.poi.ss.formula.functions.T;
import org.zkoss.poi.ss.usermodel.PivotField;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelSet;
import org.zkoss.zul.Row;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author MrBi
 */
public class PopupNationController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 8261140945077364743L;
    @Wire
    private Window popupNationWindow;
    @Wire
    private Listbox lbNation;
    Place searchForm;
    private Window parentWindow;

    private Set<Listitem> lstNation;
    private int mode;
    private Set<Listitem> lstNationget = new ListModelSet<Listitem>();
    @Wire
    private Paging userPagingBottom;

    @SuppressWarnings("unchecked")
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> args = (Map<String, Object>) Executions
                .getCurrent().getArg();
        parentWindow = (Window) args.get("parentWindow");
        lstNation = (Set<Listitem>) args.get("currentItem");
        if (lstNation != null) {
            lstNationget.addAll(lstNation);
        }
        mode = (int) args.get("Mode");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillDataToList();
    }

    private void fillDataToList() {
        PlaceDAOHE objhe = new PlaceDAOHE();
        searchForm = new Place();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage()
                * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(searchForm, start, take,
                Constants.PLACE.NATION);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        ListModelArray<Listitem> lstModel = new ListModelArray(
                plm.getLstReturn());
        lstModel.setMultiple(true);
        lbNation.setModel(lstModel);
        lbNation.renderAll();
        lbNation.setMultiple(true);

        selectitem();
		// for (int j = 0; j < lstNation.length; j++) {
        //
        // for (int i = 0; i < lbNation.getItemCount(); i++) {
        // Place p = (Place) lstModel.get(i);
        // if (p.getPlaceId().equals(Long.valueOf(lstNation[j]))) {

        // lbNation.addItemToSelection(lbNation.getItems().get(i));
        // lbNation.selectItem(lbNation.getItemAtIndex(i));
        //
        // List<Component> listrowcell = item.getChildren();
        // Component c = listrowcell.get(0);
        // c.getAttributes();
        // if (c instanceof Checkbox) {
        // Checkbox cell = (Checkbox) listrowcell.get(0);
        // cell.setChecked(true);
        // }
        // }
        // }
        // }
        // }
    }

    private void selectitem() {
        if (lstNationget != null) {

            for (Listitem i : lbNation.getItems()) {
                i.setSelected(false);
            }
            for (Listitem itemList : lbNation.getItems()) {
                Place p2;
                p2 = (Place) itemList.getValue();
                for (Listitem item : lstNationget) {
                    Place p;

                    p = item.getValue();

                    if (p.getPlaceId().equals(p2.getPlaceId())) {
                        itemList.setSelected(true);
                    }
                }
            }

        }
    }

    @Listen("onClose=#popupNationWindow")
    public void onCloseWindow() {
        // Set<Listitem> lstPlace = null;
        // Events.sendEvent("onChildWindowClosed", parentWindow, lstPlace);
        if (popupNationWindow != null) {
            popupNationWindow.detach();
        }
    }

    @Listen("#btnClose")
    public void onClose() {
        onCloseWindow();
    }

    @Listen("#btnAdd")
    public void onAdd() {

        lstNationget.addAll(lbNation.getSelectedItems());
        if (mode == 1) {
            Events.sendEvent("onChildWindowClosed", parentWindow, lstNationget);
        } else if (mode == 2) {
            Events.sendEvent("onChildWindowClosedDistributor", parentWindow,
                    lstNationget);
        } else {
            Events.sendEvent("onChildWindowClosedProduct", parentWindow,
                    lstNationget);
        }
        if (popupNationWindow != null) {
            popupNationWindow.detach();
        }
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        lstNationget.addAll(lbNation.getSelectedItems());
        fillDataToList();
    }

}
