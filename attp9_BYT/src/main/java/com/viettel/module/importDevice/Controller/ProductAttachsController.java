/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.module.cosmetic.Controller.CosmeticAttachDAO;
import com.viettel.module.cosmetic.Controller.CosmeticBaseController;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.model.AttachCategoryModel;

/**
 *
 * @author vunt
 */
public class ProductAttachsController extends CosmeticBaseController {

    @Wire
    Label lbProductWarning, lbTopWarning, lbBottomWarning,
            lbImportExcelWarning, lblTaxCode;
    private List<Category> listImportFileType;
    private List<Media> listMedia;
    @Wire
    private Vlayout flist;

    // Danh sach
    @Wire
    Listbox lbTemplate, finalFileListbox;
    @Wire
    private Listbox lbImportFileType, fileListbox;

    @Wire
    Window windowAttCRUD;
    @Wire
    private Window parentWindow;
    @Wire
    private Tabbox tb;
    @Wire
    private Tabpanel mainpanel;
    private String crudMode;
    private Long productID;
    private Map<String, Object> arguments;
    String CategoryCat = Constants.IMPORT_TYPE.TTB_PRODUCT_ATT;
    Long attCat = Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_ATT;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        if ("PL".equals(getCrudMode())) {
            CategoryCat = Constants.IMPORT_TYPE.ID_PRODUCT_PL;
            attCat = Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL;
        }
        listImportFileType = new CategoryDAOHE()
                .findAllCategory(CategoryCat);
        ListModelArray lstCategoryModel = new ListModelArray(listImportFileType);
        lbImportFileType.setModel(lstCategoryModel);
        fillFileListbox();

    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        arguments = (Map) Executions.getCurrent().getArg();

        parentWindow = (Window) arguments.get("parentWindow");
        setCrudMode((String) arguments.get("MODEL"));
        listMedia = new ArrayList();
        productID = (Long) arguments.get("id");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachNew = new ArrayList<Attachs>();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.COSMETIC_HO_SO_GOC);
        if (lstAttach.size() > 0) {
            lstAttachNew.add(lstAttach.get(0));
        }
        this.finalFileListbox.setModel(new ListModelArray(lstAttachNew));
    }

    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    private void fillFileListbox() {
        AttachDAOHE attDAO = new AttachDAOHE();
        List<Attachs> lstImportAttach = attDAO.getByObjectId(productID, attCat);
        this.fileListbox.setModel(new ListModelArray(lstImportAttach));

    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException, Exception {
        if (!isValidatedData()) {
            return;
        }
        Long limitSize = Long.parseLong(getConfigValue(
                "limitSizeAttachPerDraft", "config", "20971520"));
        String limit = getConfigValue("limitSizeStr", "config", "3M");
        for (Media m : listMedia) {
            if (!m.getFormat().endsWith("pdf") && Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL.equals(attCat)) {
                showNotification("Chỉ được nhập file PDF !!!");
                return;
            }
            Long mediaSize = Integer.valueOf(m.getByteData().length)
                    .longValue();
            if (mediaSize > limitSize) {
                showNotification("Dung lượng file không quá " + limit);
                return;
            }
        }
        Long importFileFileType = Long.valueOf(lbImportFileType
                .getSelectedItem().getValue().toString());
        AttachDAO base = new AttachDAO();

        for (Media media : listMedia) {
            base.saveImportFileAttach(media, productID,
                    attCat,
                    importFileFileType);
        }
        fillFileListbox();
        listMedia.clear();
        flist.getChildren().clear();
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();
        if ("PL".equals(getCrudMode())) {
            listMedia = new ArrayList<>();
            flist.getChildren().clear();
        }
        String warning = "";
        // listMedia.clear();
        Long limitSize = Long.parseLong(getConfigValue(
                "limitSizeAttachPerDraft", "config", "20971520"));
        String limit = getConfigValue("limitSizeStr", "config", "3M");
        int tag = 0;
        for (final Media media : medias) {
            Long mediaSize = Integer.valueOf(media.getByteData().length)
                    .longValue();
            if (mediaSize > limitSize) {
                showNotification("Dung lượng file đính kèm không được vượt quá " + limit, Clients.NOTIFICATION_TYPE_INFO);
                return;
            }

            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileTypePdf(extFile)) {
                warning += "</br>" + media.getName();
                tag = 1;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listMedia.remove(media);
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
        if (tag == 1) {
            showNotification("Định dạng file không được phép tải lên, bị loại bỏ" + warning,
                    Constants.Notification.WARNING);

        }
    }

    private boolean isValidatedData() {
        if (lbImportFileType.getSelectedItem() == null || "-1".equals(lbImportFileType.getSelectedItem().getValue().toString())) {
            lbImportFileType.focus();
            showWarningMessage("Chưa chọn loại tệp đính kèm !!!");
            return false;
        }
        if (listMedia == null || listMedia.isEmpty()) {
            showWarningMessage("Chưa upload file đính kèm !!!");
            return false;
        }
        return true;
    }

    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    @Listen("onClose=#windowCRUDCosmetic")
    public void onCloseWindow() {
        if (windowAttCRUD != null) {
            windowAttCRUD.detach();
        }
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);
    }

    @Listen("onClick=#btnCreateProFile")
    public void onCreateProFile() throws IOException, Exception {
        if (lbImportFileType.getSelectedItem() == null || "-1".equals(lbImportFileType.getSelectedItem().getValue().toString())) {
            showNotification("Chọn loại tệp trước khi thêm từ hồ sơ dùng chung !!!");
            return;
        }
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", windowAttCRUD);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/publicFileDeviceManageListImport.zul", null,
                arguments);
        window.doModal();
    }

    @Listen("onChooseProFile = #windowAttCRUD")
    public void onChooseProFile(Event event) throws IOException, Exception {
        if (productID != null) {
            Map<String, Object> args = (Map<String, Object>) event.getData();
            List list = (List) args.get("documentReceiveProcess");
            if (list == null || list.isEmpty()) {
                return;
            }
            String limit = getConfigValue("limitSizeStr", "config", "3M");
            if ("PL".equals(crudMode)) {
                AttachCategoryModel Process = (AttachCategoryModel) list
                        .get(0);
                if (list.size() > 1 || !Process.getAttach().getAttachName().endsWith(".pdf")) {
                    showNotification("Chỉ được chọn 1 file phụ lục duy nhất định dạng *.pdf không quá " + limit);
                    return;
                }
            }
            int idx;
            AttachDAOHE attachDAOHE = new AttachDAOHE();
            for (idx = 0; idx < list.size(); idx++) {
                AttachCategoryModel Process = (AttachCategoryModel) list
                        .get(idx);
                Attachs attach = null;
                if ("PL".equals(crudMode)) {
                    attach = attachDAOHE.getByObjectIdAndAttachCat(productID, attCat);
                }
                if (attach == null) {
                    attach = new Attachs();
                }
                attach.setAttachPath(Process.getAttach().getAttachPath());
                String fileName = (Process.getAttach().getAttachName());
                attach.setAttachName(fileName);
                attach.setIsActive(Constants.Status.ACTIVE);
                attach.setObjectId(productID);
                attach.setCreatorId(getUserId());
                attach.setCreatorName(Process.getAttach().getCreatorName());
                attach.setDateCreate(new Date());
                attach.setModifierId(getUserId());
                attach.setDateModify(new Date());
                attach.setAttachCat(attCat);
                attach.setAttachType(Long.parseLong(lbImportFileType.getSelectedItem().getValue().toString()));
                attach.setAttachTypeName(lbImportFileType.getSelectedItem().getLabel());
                attach.setAttachDes(Process.getAttach().getAttachDes());
                attachDAOHE.saveOrUpdate(attach);
            }
            fillFileListbox();
        }
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(final Event eventDelete) {
        try {
            Attachs obj = (Attachs) eventDelete
                    .getData();
            CosmeticAttachDAO rDAOHE = new CosmeticAttachDAO();
            rDAOHE.delete(obj.getAttachId());
            fillFileListbox();
        } catch (Exception ex) {
            showNotification(
                    String.format(
                            Constants.Notification.DELETE_ERROR,
                            Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onVisible = #windowAttCRUD")
    public void onVisible() {
        windowAttCRUD.setVisible(true);
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    /**
     * @return the crudMode
     */
    public String getCrudMode() {
        return crudMode;
    }

    /**
     * @param crudMode the crudMode to set
     */
    public void setCrudMode(String crudMode) {
        this.crudMode = crudMode;
    }
}
