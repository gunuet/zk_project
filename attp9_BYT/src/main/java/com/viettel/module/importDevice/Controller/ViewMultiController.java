/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.ViewMultiForm;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Include;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class ViewMultiController extends BaseComposer {

    private ViewMultiForm multiform;
    @Wire
    private Vlayout vlayoutMain;
    @Wire
    private Window viewMulti;
    @Wire
    private Include incBusinessPage;
    @Wire
    private Vlayout includeWindow;
    private Map<String, Object> staticArgument;
    @Wire
    private Button btnOK;
    protected Window windowParent;
    private List<VFileImportDevice> lstDevice;

    public ViewMultiController() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        staticArgument = (Map) Executions.getCurrent().getArg();
        lstDevice = (List<VFileImportDevice>) staticArgument.get("lstDevice");
        multiform = (ViewMultiForm) staticArgument.get("form");
        windowParent = (Window) staticArgument.get("windowParent");
        loadForm();
    }

    private void loadForm() {
        addBusinessPage(multiform.getFormName());
    }

    private void addBusinessPage(String formName) {
        try {
            includeWindow.getChildren().clear();
            includeWindow.appendChild(this.createWindow("includeWindow", formName,
                    staticArgument, Window.EMBEDDED));
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onClose=#viewMulti")
    public void onClickClose() {
        refresh();
    }

    @Listen("onClick=#btnOK")
    public void onClickOK() throws IOException {

        Window businessWindow = (Window) includeWindow
                .getFellow("businessWindow");
        Button submit = (Button) businessWindow.getFellow("btnSubmit");
        Events.sendEvent(new Event("onClick", submit, null));
        Textbox txtValidate = null;
        try {
            txtValidate = (Textbox) businessWindow.getFellow("txtValidate");
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        if (txtValidate != null && "1".equals(txtValidate.getValue())) {
            refresh();
        }

    }

    private void refresh() {
        if (windowParent != null) {
            Events.sendEvent("onRefresh", windowParent, null);
        }
        viewMulti.onClose();
    }
}
