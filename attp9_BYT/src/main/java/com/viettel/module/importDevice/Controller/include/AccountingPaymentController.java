package com.viettel.module.importDevice.Controller.include;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.DAO.HolidaysManagementDAOHE;
import com.viettel.module.importDevice.BO.VIdfPaymentinfo;
import com.viettel.module.importDevice.DAO.VIdfPaymentinfoDAO;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.module.payment.controller.ViewAcceptBillController;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.WordExportUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.zul.Button;
import org.zkoss.zul.Filedownload;

/**
 *
 * @author quynhhv1 kế toán xác nhận 1 hồ sơ
 */
public class AccountingPaymentController extends PaymentController {

    @Wire
    private Textbox txtPaymentName;
    @Wire
    private Textbox txtPaymentAddress;
    @Wire
    private Window businessWindow;
    @Wire
    Radiogroup rgTypePayment;
    @Wire
    Radio radioKeypay, radioBanktransfer, radioImmediaacy;
    @Wire
    private Label lbWarning, lbNgayTiepNhan, lbPaymentActionUser;
    @Wire
    private Label lbWarningUnder;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    private Window parentWindow;
    Long lsumMoney = 0L;
    // danh cho upload file
    @Wire
    Textbox txtBillNo;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    private Textbox txtPaymentConfirm;
    @Wire
    Textbox txtValidate;
    @Wire
    private Label lbBookNumber;
    @Wire
    private Label lbMahoso;
    @Wire
    private Label lbTensp;
    @Wire
    Groupbox gbReject;
    @Wire("#lbtextMoney")
    private Label lbtextMoney;
    @Wire
    Button btnManageReceiptPayment,btnExportAppliesFee;
    Bill objBill = null;
    Long typePayment = 0L; // 1 radioKeypay //2 chuyen khoan // 3 truc tiep
    boolean bUploadFile = false, clickDeletefile = false;
    Long docId = null;
    Long docType = null;
    private List<VIdfPaymentinfo> listPaymented;
    VIdfPaymentinfo vPaymentInfo = null;
    BookDocument mBook;
    //private String type;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("windowParent");
        mBook = new BookDocument();
        docId = (Long) arguments.get("docId");
        docType = (Long) arguments.get("docType");
        //lsumMoney = objBill.getTotal();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onAfterRender = #lbListPaymented")
    public void onAfterRender() {
    }

    @Listen("onClick = #btnManageReceiptPayment")
    public void onManageReceiptPayment(Event event){
        Map args = new ConcurrentHashMap();
        args.put("billId", objBill.getBillId());
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/include/payment/manageReceiptPayment.zul", null, args);
        window.doModal();
    }
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    private void reloadModel() throws IOException {

//        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
        VIdfPaymentinfoDAO objDAO = new VIdfPaymentinfoDAO();

        listPaymented = objDAO.getListFileId(docId);
        if (listPaymented.size() > 0) {
            vPaymentInfo = listPaymented.get(0);
            BillDAO objBillDAOHE = new BillDAO();
            objBill = objBillDAOHE.findById(vPaymentInfo.getBillId());
            if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
                rgTypePayment.setSelectedItem(radioKeypay);
            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
                rgTypePayment.setSelectedItem(radioBanktransfer);

            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                rgTypePayment.setSelectedItem(radioImmediaacy);
                
            }
            txtBillNo.setValue(objBill.getCode());
            dbDayPayment.setValue(objBill.getPaymentDate());
            txtPaymentName.setValue(objBill.getCreatetorName());
            txtPaymentAddress.setValue(objBill.getCreatorAddress());
            setTypePayment();
            BookDocumentDAOHE bookDao = new BookDocumentDAOHE();
            if ((docId != null) && (docType != null)) {
                mBook = bookDao.getBookInFromDocumentId(docId, docType);
                if (mBook != null) {
                    lbBookNumber.setValue(mBook.getBookNumber().toString());
                } else {
                    lbBookNumber.setValue("");
                }
            }
            AttachDAOHE attDAOHE = new AttachDAOHE();
            List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
            if ((items != null) && (items.size() > 0)) {
                try {
                    //AMedia amedia = new AMedia(mAttach.getAttachName(), sType, WorkflowAPI.getCtypeFile(sType), is);
                    //iframeFile.setContent(amedia);
                    txtFileName.setValue(items.get(0).getAttachName());
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }
            }
            lsumMoney = vPaymentInfo.getCost();
            String formatedMoneyText = formatNumber(lsumMoney, "###,##0");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            lbSumMoney.setValue("".equals(formatedMoneyText) ? "" : formatedMoneyText + " đồng");
            lbMahoso.setValue(vPaymentInfo.getNswFileCode());
            lbPaymentActionUser.setValue(vPaymentInfo.getPaymentActionUser());
            lbNgayTiepNhan.setValue(sdf.format(vPaymentInfo.getCreateDate()));
//            lbTensp.setValue(VCosPaymentInfo.getProductName());
            //txtPaymentAddress.setValue(vCosPaymentInfo.getBusinessAddress());

            lbtextMoney.setValue(numberToString(lsumMoney));

        }
    }

    @Listen("onClick = #btnExportAppliesFee")
    public void onExportAppliesFee(Event event){
        try{
            String fileName = "Bieumauapphi_" + (new Date()).getTime() + ".docx";
            WordprocessingMLPackage appliesFee = prepareDataToExportAppliesFee();
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            folderPath += separator + dateFormat.format(date);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                // tạo forlder
                fd.mkdirs();
            }
            File f = new File(folderPath + separator + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            appliesFee.save(f);                      
            Filedownload.save(f, folderPath + separator + fileName);
        }catch(IOException | Docx4JException ex){
            LogUtils.addLogDB(ex);
        }
    }
    
    private WordprocessingMLPackage prepareDataToExportAppliesFee(){
        try{
            String bieumaugiayphep = "/WEB-INF/template/bieumauapphi.docx";
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(new FileInputStream(new File(request.getRealPath(bieumaugiayphep))));
            WordExportUtils wU = new WordExportUtils();
            FilesDAOHE filesDAOHE = new FilesDAOHE();
            Files file = filesDAOHE.findById(docId);
            wU.replacePlaceholder(wmp, file.getBusinessName(), "${businessName}");
            wU.replacePlaceholder(wmp, file.getBusinessAddress(), "${businessAddress}");
            wU.replacePlaceholder(wmp, lbtextMoney.getValue(), "${paymentNote}");
            wU.replacePlaceholder(wmp, "Lệ phí cấp phép nhập khẩu trang thiết bị y tế", "${paymentName}");
            wU.replacePlaceholder(wmp, lbSumMoney.getValue() + " đồng", "${paymentValue}");
            Calendar cal = Calendar.getInstance();
            String date = "ngày " + cal.get(Calendar.DAY_OF_MONTH) + " tháng " + (cal.get(Calendar.MONTH) + 1) + " năm " +cal.get(Calendar.YEAR);
            wU.replacePlaceholder(wmp, date, "${date}");            
            wU.replacePlaceholder(wmp, lbPaymentActionUser.getValue(), "${creatorName}");
            return wmp;
        }catch(FileNotFoundException | Docx4JException ex){
            LogUtils.addLogDB(ex);
            return null;
        }
    }
    
    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            String visibleReceiptPayment = ResourceBundleUtil.getString("hopdong");
            String visibleExportAppliesFee = ResourceBundleUtil.getString("hopdong");
            if(visibleReceiptPayment != null && "true".equals(visibleReceiptPayment)){
                btnManageReceiptPayment.setVisible(true);
            }else{
                btnManageReceiptPayment.setVisible(false);
            }
            if(visibleExportAppliesFee != null && "true".equals(visibleExportAppliesFee)){
                btnExportAppliesFee.setVisible(true);
            }else{
                btnExportAppliesFee.setVisible(false);
            }
            reloadModel();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
//        BookDocumentDAOHE bookDao = new BookDocumentDAOHE();
//        if ((docId != null) && (docType != null)) {
//            BookDocument mBook = bookDao.getBookInFromDocumentId(docId, docType);
//            if (mBook != null) {
//                lbBookNumber.setValue(mBook.getBookNumber().toString());
//                lbNgayTiepNhan.setValue(new SimpleDateFormat("dd-MM-yyyy").format(mBook.getCreateDate()));
//                lbPaymentActionUser.setValue(mBook.getCreatorName());
//            } else {
//                lbBookNumber.setValue("");
//            }
//        }
//        lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));
//        txtValidate.setValue("0");
//        txtBillNo.setFocus(true);

    }

    @Listen("onClick = #btnAccept")
    public void onAccept() {
        if (isValidate()) {
            PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
            PaymentInfo paymentInfo;
            int sizeList = listPaymented.size();
            for (int i = 0; i < sizeList; i++) {
                VIdfPaymentinfo app = listPaymented.get(i);
                paymentInfo = objFreePaymentInfo.findById(app.getPaymentInfoId());
                paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED); // trang thai moi tao
                paymentInfo.setDateConfirm(new Date());
                paymentInfo.setPaymentActionUser(getUserFullName());
                paymentInfo.setPaymentActionUserId(getUserId());
                paymentInfo.setNote(txtPaymentConfirm.getText().trim());
                objFreePaymentInfo.saveOrUpdate(paymentInfo);
                //System.out.print("gia tri"+app.getRapidTestNo());
            }
            BillDAO objBillDAOHE = new BillDAO();
            objBill.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED);
            objBill.setConfirmDate(new Date());
            objBill.setConfirmUserId(getUserId());
            objBill.setConfirmUserName(getUserFullName());
            objBill.setCode(txtBillNo.getValue().trim());

            objBillDAOHE.saveOrUpdate(objBill);
            objBillDAOHE.flush();
            showNotification(String.format(Constants.Notification.APROVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.BILL), Constants.Notification.INFO);
            businessWindow.onClose();
            txtValidate.setValue("1");
            FilesDAOHE filesDAOHE = new FilesDAOHE();
            Files files = filesDAOHE.findById(docId);
            files.setStartDate(new Date());
            files.setNumDayProcess(Constants.WARNING.NUM_DAY_PROCESS);
            HolidaysManagementDAOHE holidaysManagementDAOHE = new HolidaysManagementDAOHE();
            Date deadline = holidaysManagementDAOHE.getDeadline(files.getStartDate(), files.getNumDayProcess());
            files.setDeadline(deadline);
            filesDAOHE.saveOrUpdate(files);
        }

    }

    @Listen("onClosePage = #windowsViewAcceptBill")
    public void onClosePage() {
        businessWindow.onClose();
        if (parentWindow != null) {
            Events.sendEvent("onReLoadPage", parentWindow, null);
        }
        //Events.sendEvent("onVisible", parentWindow, null);
    }

    private void setTypePayment() {
        if (rgTypePayment.getSelectedItem() != null) {
            if (null != rgTypePayment.getSelectedItem().getId()) switch (rgTypePayment.getSelectedItem().getId()) {
                case "keypay":
                    //gbTypePayment.setVisible(false);
                    typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
                    break;
                case "immediaacy":
                    typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;
                    break;
                case "banktransfer":
                    typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;
                    break;
            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        setTypePayment();

    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
        lbWarningUnder.setValue(message);
        lbWarning.setVisible(true);
        lbWarningUnder.setVisible(true);
    }

    @Listen("onClick = #btnDownload")
    public void onDownload() throws IOException {

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {

        onAccept();

        if (parentWindow != null) {
            Events.sendEvent("onClosePage", parentWindow, null);
        }

    }

    private boolean isValidate() {
        String message;
//        if ("".equals(txtBillNo.getText().trim())) {
//            message = "Không được để trống số biên lai";
//            txtBillNo.focus();
//            setWarningMessage(message);
//            return false;
//        }
        return true;
    }
}
