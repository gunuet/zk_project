package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.WordExportUtils;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author
 */
public class ApplyFeeControler extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final int BOOK_LISTBOXMODEL = 1;
    private final int STATUS_LISTBOXMODEL = 2;
    @Wire
    private Listbox lbBookIn;
    private List listBookIn;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbMahoso, lbTensp, lbSumMoney, lbtextMoney, lbBookNumber, lbCreate;
//    @Wire
//    private Intbox boxBookNumber;

    private Long fileId, piCost;
    private BookDocument bookDocument;
//    @Wire
//    private Listbox lbStatusReceive;
    @Wire
    private Textbox tbStatusReceive;
//    @Wire
//    private Textbox tbCost;
    @Wire
    private Decimalbox tbCost;
    @Wire
    private Window businessWindow;
    @Wire
    Button btnOpenViewManageFee;
    @Wire
    private Textbox txtValidate, txtMessage;
    private String bCode;
    private Long docType;
    private ImportDeviceFile importDeviceFile;
    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        //bCode = (String) arguments.get("bCode");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_IN;//So tiep nhan
        docType = (Long) arguments.get("docType");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        lbCreate.setValue(getUserFullName());
        ImportDeviceFiletDAO idfDAO = new ImportDeviceFiletDAO();
        importDeviceFile = idfDAO.findByFileId(fileId);
        String visibleAppliesFee = ResourceBundleUtil.getString("hopdong");
        if(visibleAppliesFee != null && "true".equals(visibleAppliesFee)){
            btnOpenViewManageFee.setVisible(true);
        }else{
            btnOpenViewManageFee.setVisible(false);
        }
        if (importDeviceFile != null) {
        	tbCost.setValue(String.valueOf(2000000));
//            tbCost.setValue(String.valueOf(importDeviceFile.getPrice()));
//            if (importDeviceFile.getPrice() == 500000L) {
//                tbCost.setValue(String.valueOf(500000));
//            } else if (importDeviceFile.getPrice() == 1000000L) {
//                tbCost.setValue(String.valueOf(1000000));
//            } else if (importDeviceFile.getPrice() == 3000000L) {
//                tbCost.setValue(String.valueOf(3000000));
//            } else {
//                tbCost.setValue(String.valueOf(200000));
//            }
        }
        txtValidate.setValue("0");
    }

//    void fillHoSo(){
//        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
//        List<VCosPaymentInfo> listPaymented;
//        VCosPaymentInfo vCosPaymentInfo;
//        listPaymented = objDAOHE.getListFileId(fileId);
//         Long lsumMoney = 0L;
//        if (listPaymented.size() > 0) {
//            vCosPaymentInfo = listPaymented.get(0);
//             lsumMoney = vCosPaymentInfo.getCost();
//            lbMahoso.setValue(vCosPaymentInfo.getNswFileCode());
//            lbTensp.setValue(vCosPaymentInfo.getProductName());
//            lbSumMoney.setValue(String.valueOf(lsumMoney));
//            lbtextMoney.setValue(numberToString(lsumMoney));
//            lbCreate.setValue(getUserFullName());
//        }
//    }
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
//            String msg = "";

//            if ((msg = ValidatorUtil.validateTextbox(tbCost, false, 15, ValidatorUtil.PATTERN_CHECK_NUMBER)) != null || tbCost.getValue().trim().length() == 0) {
//                lbBottomWarning.setValue("Lệ phí nhập sai!");
//                tbCost.focus();
//                return;
//            }
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    //thanhvt10 09/11/2016
    @Listen("onClick = #btnOpenViewManageFee")
    public void onViewManageFee(Event event) {
        Map args = new ConcurrentHashMap();
        args.put("windowParent", businessWindow);
        args.put("importDeviceFileId", importDeviceFile.getImportDeviceFileId());
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/manageApplyFee_popup.zul", null, args);
        window.doModal();
    }
    
    @Listen("onClick = #btnExportAppliesFee")
    public void onExportAppliesFee(Event event){
        try{
            String fileName = "Bieumauapphi_" + (new Date()).getTime() + ".docx";
            WordprocessingMLPackage appliesFee = prepareDataToExportAppliesFee();
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            folderPath += separator + dateFormat.format(date);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                // tạo forlder
                fd.mkdirs();
            }
            File f = new File(folderPath + separator + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            appliesFee.save(f);                      
            Filedownload.save(f, folderPath + separator + fileName);
        }catch(IOException | Docx4JException ex){
            LogUtils.addLogDB(ex);
        }
    }
    
    private WordprocessingMLPackage prepareDataToExportAppliesFee(){
        try{
            String bieumaugiayphep = "/WEB-INF/template/bieumauapphi.docx";
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(new FileInputStream(new File(request.getRealPath(bieumaugiayphep))));
            WordExportUtils wU = new WordExportUtils();
            FilesDAOHE filesDAOHE = new FilesDAOHE();
            Files file = filesDAOHE.findById(fileId);
            wU.replacePlaceholder(wmp, file.getBusinessName(), "${businessName}");
            wU.replacePlaceholder(wmp, file.getBusinessAddress(), "${businessAddress}");
            wU.replacePlaceholder(wmp, PaymentController.numberToString(tbCost.getValue().longValue()), "${paymentNote}");
            wU.replacePlaceholder(wmp, "Lệ phí cấp phép nhập khẩu trang thiết bị y tế", "${paymentName}");
            wU.replacePlaceholder(wmp, (tbCost.getValue() != null ? tbCost.getValue().toString() : "0") + " đồng", "${paymentValue}");
            Calendar cal = Calendar.getInstance();
            String date = "ngày " + cal.get(Calendar.DAY_OF_MONTH) + " tháng " + (cal.get(Calendar.MONTH) + 1) + " năm " +cal.get(Calendar.YEAR);
            wU.replacePlaceholder(wmp, date, "${date}");            
            wU.replacePlaceholder(wmp, lbCreate.getValue(), "${creatorName}");
            return wmp;
        }catch(FileNotFoundException | Docx4JException ex){
            LogUtils.addLogDB(ex);
            return null;
        }
    }
    
    @Listen("onAddText = #businessWindow")
    public void onAddText(Event event) {
        String text = event.getData().toString();
        tbCost.setValue(text);
    }
    
    
//    public ListModelList getListBoxModel(int type) {
//        ListModelList model = null;
//        switch (type) {
//            case BOOK_LISTBOXMODEL:
//                BookDAOHE bookDAOHE = new BookDAOHE();
//                listBookIn = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
//                model = new ListModelList(listBookIn);
//                break;
//            case STATUS_LISTBOXMODEL:
//
//                break;
//        }
//
//        return model;
//    }

    /**
     * Lấy id so cu
     *
     * @param type
     * @return
     */
//    public int getSelectedIndexInModel(int type) {
//        int selectedItem = 0;
//        switch (type) {
//            case BOOK_LISTBOXMODEL:
//                if (bookDocument != null) {
//                    for (int i = 0; i < listBookIn.size(); i++) {
//                        if (Objects.equals(bookDocument.getBookId(),
//                                ((Books) listBookIn.get(i)).getBookId())) {
//                            selectedItem = i;
//                            break;
//                        }
//                    }
//                }
//                break;
//        }
//        return selectedItem;
//    }
//    public void onSelectBook() {
//        Books book = (Books) listBookIn.get(lbBookIn.getSelectedIndex());
//        lbBookNumber.setValue(String.valueOf(getMaxBookNumber(book.getBookId())));
//    }
//    @Listen("onAfterRender = #lbBookIn")
//    public void onAfterRenderListBookIn() {
//        int selectedItemIndex = getSelectedIndexInModel(BOOK_LISTBOXMODEL);
//        List<Listitem> listitems = lbBookIn.getItems();
//        if (listitems.isEmpty()) {
//            // Hien thi thong bao loi: don vi chua co so
//            showWarningMessage("Đơn vị " + getDeptName()
//                    + " chưa có sổ hồ sơ.");
//            showNotification("Đơn vị " + getDeptName()
//                    + " chưa có sổ hồ sơ.",
//                    Constants.Notification.WARNING);
//        } else {
//            lbBookIn.setSelectedIndex(selectedItemIndex);
//            Long bookId = lbBookIn.getItemAtIndex(selectedItemIndex).getValue();
//             lbBookNumber.setValue(String.valueOf(getMaxBookNumber(bookId)));
//        }
//    }
//    public Integer getBookNumber(Long fileId, Long fileType) {
//        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
//        BookDocument bd = bookDocumentDAOHE.getBookDocumentFromDocumentId(
//                fileId, fileType);
//        if (bd != null) {
//            return bd.getBookNumber().intValue();
//        }
//        return null;
//    }
//    public int getMaxBookNumber(Long bookId) {
//        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
//        return bookDocumentDAOHE.getMaxBookNumber(bookId).intValue();
//    }
    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbCost.getText().matches("^[0-9.]+$")) {
            showWarningMessage("Lệ phí không được để trống");
            tbCost.focus();
            return false;
        }

        return true;
//    	String msg="";
//    	if ((msg = ValidatorUtil.validateTextbox(tbCost, false, 15, ValidatorUtil.PATTERN_CHECK_NUMBER)) != null) {
//    		showWarningMessage("Mã trang thiết bị y tế chỉ có thể nhập số");
//            tbCost.focus();
//            return false;
//        }
//    	return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_ID.SendMs_43);
        md.setPhase(0l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
//        clearWarningMessage();
    	//linhdx 20170215
        String date_ud = ResourceBundleUtil.getString("TTB_nsw_time_new_fee", "config");
        FilesDAOHE fDAOHE = new FilesDAOHE();
        Files files = fDAOHE.findById(fileId);
        Date d_ud = formatterDateTime.parse(date_ud);
//      //Neu ho so tao sau thoi diem upcode va user thuc hien ap phi la doanquangminh thi se ko thuc hien duoc
//        ProcessDAOHE pHe = new ProcessDAOHE();
//        @SuppressWarnings("unchecked")
//		List<Process> lstProcess = pHe.getProcessProcess(files.getFileId(), files.getFileType());
//        if (d_ud.after(lstProcess.get(0).getSendDate()) && "doanquangminh".equals(getUserName())) {
//        	showNotification("Hồ sơ đã được văn thư áp phí và doanh nghiệp nộp tiền theo thông tư mới, "
//        			+ "Đề nghị chuyển hồ sơ đến văn thư để ban hành",
//                        Constants.Notification.ERROR);
//            return ;
//        }
        
        try {

            PaymentInfoDAO piDAO = new PaymentInfoDAO();
            PaymentInfo pi = new PaymentInfo();
            pi.setFileId(fileId);
            pi.setFeeName("Lệ phí nhập khẩu trang thiết bị y tế");
            pi.setPaymentActionUser(getUserFullName());
            pi.setPaymentActionUserId(getUserId());

            pi.setCreateDate(new Date());
            pi.setIsActive(Constants.Status.ACTIVE);
            pi.setStatus(ConstantsPayment.PAYMENT.PAY_NEW);
            pi.setPhase(ConstantsPayment.PAYMENT.PAY_PHASE);
            String value = String.valueOf(tbCost.getValue());
            Long valueL = 0L;
            try {
                valueL = Long.valueOf(value);
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
            pi.setCost(valueL);

            //linhdx
            pi.setBankNo(ResourceBundleUtil.getString("TTB_TAIKHOAN", "config"));
            pi.setDeptCode("TTB");
            pi.setDeptName("Vụ Trang thiết bị");

            piDAO.saveOrUpdate(pi);

            sendMS();
            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

//    protected BookDocument createBookDocument() {
//        // So van ban *
//        Long bookId = lbBookIn.getSelectedItem().getValue();
//        if (bookDocument == null) {
//            bookDocument = new BookDocument();
//        }
//        if (bookId != -1) {
//            bookDocument.setBookId(bookId);
//        }
//         Books book = (Books) listBookIn.get(lbBookIn.getSelectedIndex());
//       
//        // So den *
//        //bookDocument.setBookNumber(boxBookNumber.getValue().longValue());
//        bookDocument.setBookNumber(Long.valueOf(getMaxBookNumber(book.getBookId())));
//        bookDocument.setDocumentId(fileId);
//        return bookDocument;
//    }
//    public void updateBookCurrentNumber() {
//        if (getMaxBookNumber(bookDocument.getBookId()) <= bookDocument.getBookNumber()) {
//            BookDAOHE bookDAOHE = new BookDAOHE();
//            Books book = bookDAOHE.findById(bookDocument.getBookId());
//            book.setCurrentNumber(bookDocument.getBookNumber());
//            bookDAOHE.saveOrUpdate(book);
//        }
}
