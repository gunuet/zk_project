package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class BoardMemberController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Listbox lbAppraisal;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lblResonRequestCVKTC, lblAppraisalCVKTC, lblResonRequestCVC, lblAppraisalCVC;
    private Long fileId;
//    @Wire
//    private Textbox tbResonRequest;
    private Textbox tbResonRequest = (Textbox)Path.getComponent("/windowProcessing/txtNote");
    @Wire
    private Textbox txtValidate;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHS();
        txtValidate.setValue("0");
    }

    private void fillHS() {
 List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId, Constants.EVAL_TYPE.ROLE_CVC);
        Gson gson=new Gson();
        if (lstEvaluationRecord.size() > 0) {
            obj = lstEvaluationRecord.get(0);
             EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            lblAppraisalCVC.setValue(evModel.getResultEvaluationStr());
            lblResonRequestCVC.setValue(obj.getMainContent());
        }
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId, Constants.EVAL_TYPE.ROLE_CVKTC);
        if (lstEvaluationRecord.size() > 0) {
            obj = lstEvaluationRecord.get(0);
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            lblAppraisalCVKTC.setValue(evModel.getResultEvaluationStr());
            lblResonRequestCVKTC.setValue(obj.getMainContent());
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbResonRequest.getText().matches("\\s*")) {
            showWarningMessage("Ý kiến không thể để trống");
            tbResonRequest.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
//            if (!isValidatedData()) {
//                return;
//            }
            Gson gson = new Gson();
            String reson = tbResonRequest.getValue().trim();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setContent(reson);
            evaluModel.setResultEvaluationStr(lbAppraisal.getSelectedItem().getLabel());
            evaluModel.setResultEvaluation(Long.valueOf((String) lbAppraisal.getSelectedItem().getValue()));
            String jsonEvaluation = gson.toJson(evaluModel);

            EvaluationRecord obj = new EvaluationRecord();
            obj.setMainContent(reson);
            if (Long.valueOf((String) lbAppraisal.getSelectedItem().getValue()) == 1L) {
                obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_TVHD_DUYET);
            } else if (Long.valueOf((String) lbAppraisal.getSelectedItem().getValue()) == 2L) {
                obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_TVHD_SDBS);
            } else {
                obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_TVHD_TUCHOI);
            }
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            obj.setEvalType(Constants.EVAL_TYPE.ROLE_TVHD);
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);
            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

}
