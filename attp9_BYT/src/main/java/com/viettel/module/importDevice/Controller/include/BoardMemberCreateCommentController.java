/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller.include;

import java.util.Date;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Home.Notify;
import com.viettel.voffice.DAOHE.NotifyDAOHE;

/**
 *
 * @author giangpn
 */
public class BoardMemberCreateCommentController extends BaseComposer {

   @Wire 
   Window businessWindow;
   @Wire
   Listbox lstPublicFile;
   @Wire
   Textbox tbAdditionRequest;
   
   VFileImportDevice object;
   
    private Window parentWindow;
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
    	
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
         object = (VFileImportDevice) Executions.getCurrent().getArg().get("object");
      
    }

    @Listen("onClick = #btnSave")
    public void onSaveDoc() {
        Notify notify = new Notify();
        NotifyDAOHE dao = new NotifyDAOHE();
        if (!"".equals(tbAdditionRequest.getText()) && tbAdditionRequest.getText() != null) {
        	notify.setContent(tbAdditionRequest.getText());
        	notify.setUserId(getUserId());
        	notify.setSendUserId(getUserId());
        	notify.setSendUserName(getUserFullName());
        	notify.setObjectId(object.getFileId());
        	notify.setSendTime(new Date());
        	
        	notify.setObjectType(object.getFileType());
        	notify.setStatus(Constants.Status.ACTIVE);
        	notify.setMultiDept(null);
        	
        	dao.saveOrUpdate(notify);
        	showSuccessNotification("Thêm ý kiến thành công!");
        	businessWindow.onClose();
        } else {
            showNotification("Bạn chưa nhập ý kiến");
        }
    }
}
