/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.voffice.DAOHE.NotifyDAOHE;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

/**
 *
 * @author HaVM2
 */
public class BoardMemberViewCommentController extends BaseComposer {
    private static final long serialVersionUID = 1L;

    @Wire
    Window flowConfigDlg;
    @Wire
    Listbox lbProcess;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        VFileImportDevice object = (VFileImportDevice) Executions.getCurrent().getArg().get("object");
        displayFlow(object.getFileId(), object.getFileType());
    }

    public void displayFlow(Long objectId, Long objectType) {
    	NotifyDAOHE dao = new NotifyDAOHE();
        List lstProcess = dao.getNotifyByObject(objectId, objectType);
        if(lstProcess == null || lstProcess.isEmpty()){
            return;
        }
        //
        // Hien thi tren danh sach
        //
        ListModelList lstModel = new ListModelList(lstProcess);
        lbProcess.setModel(lstModel);
    }

    @Listen("onBack=#flowConfigDlg")
    public void onBack() {
        flowConfigDlg.detach();
    }
    
    public String getStatus(Long status){
        return WorkflowAPI.getStatusName(status);
    }
    
}
