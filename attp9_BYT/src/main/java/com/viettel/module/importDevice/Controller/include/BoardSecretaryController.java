package com.viettel.module.importDevice.Controller.include;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import org.zkoss.zk.ui.Path;

/**
 *
 * @author vunt
 */
public class BoardSecretaryController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
//    @Wire
//    private Listbox fileListbox;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lblMember;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Vlayout flist;
    private List<Media> listMedia;
    private Long evalType;
    private EvaluationRecord obj = null;
    private Listbox lbAction = (Listbox) Path.getComponent("/windowProcessing/lbAction");

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override

    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        evalType = Constants.FILE_STATUS_CODE.STATUS_HHD;
        listMedia = new ArrayList();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        txtValidate.setValue("0");
    }

    void fillHoSo() {
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        if (lbAction != null && lbAction.getSelectedItem() != null) {
            lblMember.setValue(lbAction.getSelectedItem().getLabel());
        }
        obj = objDAO.findContentByFileIdAndEvalType(fileId, evalType);
        if (obj != null) {
            tbResonRequest.setValue(obj.getMainContent());
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            if (!isValidatedData()) {
                return;
            }
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbResonRequest.getText().matches("\\s*")) {
            showWarningMessage("Kết luận không được để trống !!!");
            tbResonRequest.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            Gson gson = new Gson();
            String reson = tbResonRequest.getValue().trim();
            com.viettel.module.evaluation.Model.EvaluationModel evaluModel = new com.viettel.module.evaluation.Model.EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setContent(reson);
            //            evaluModel.setTypeProfessional(Constants.TYPE_PROFESSIONAL.TYPE_PROFESSIONAL_MAIN);
            evaluModel.setResultEvaluation(3L);
            evaluModel.setResultEvaluationStr(lblMember.getValue());
            String jsonEvaluation = gson.toJson(evaluModel);
            if (obj == null) {
                obj = new EvaluationRecord();
            }
            obj.setMainContent(reson);
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);

//            if (flist.getChildren() == null || flist.getChildren().isEmpty()) {
//              showWarningMessage("Chưa chọn file đính kèm");
//              return;
//            }
            if (fileId != null) {
                AttachDAO base = new AttachDAO();

                for (Media media : listMedia) {
                    base.saveReportMeetingFileAttach(media, fileId, Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE, Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE);
                }
                listMedia.clear();
                flist.getChildren().clear();
                lbTopWarning.setValue("");
                lbBottomWarning.setValue("");
                // showWarningMessage(flist.getChildren().size()+"");
            }

            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();
        flist.getChildren().clear();
        listMedia.clear();
        Long limitSize = Long.parseLong(getConfigValue("limitSizeAttachPerDraft", "config", "20971520"));
//        String limit=getConfigValue("limitSizeStr", "config", "3M");
        for (final Media media : medias) {
            Long mediaSize = Integer.valueOf(media.getByteData().length).longValue();
            if (mediaSize > limitSize) {
                showNotification("Tổng dung lượng đính kèm không được vượt quá 20M", Clients.NOTIFICATION_TYPE_INFO);
                return;
            }
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }
}
