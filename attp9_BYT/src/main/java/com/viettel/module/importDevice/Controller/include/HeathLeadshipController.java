package com.viettel.module.importDevice.Controller.include;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.BO.CosPermit;
import com.viettel.module.cosmetic.Controller.include.CosEvaluationSignPermitController;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.cosmetic.DAO.CosPermitDAO;
import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.importDevice.DAO.ExportDeviceDAO;
import com.viettel.module.importDevice.Model.ExportDeviceModel;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;

/**
 *
 * @author Linhdx
 */
public class HeathLeadshipController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Listbox finalFileListbox;
    @Wire
    Textbox txtCertSERIALHEATH, txtBase64HASHHEATH;
    private Permit permit;

    private List listBook;
    private Long docType;
    private String bCode;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private String fileSignOut = "";

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_PERMIT;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillFinalFileListbox(fileId);
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tham dinh ho so thiet bi y te:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId, Constants.OBJECT_TYPE.COSMETIC_PERMIT);
        if (lstAttach.size() <= 0) {
            return false;
        }
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                showNotification(String.format(
                        "Hãy thực hiện ký số", Constants.DOCUMENT_TYPE_NAME.FILE),
                        Constants.Notification.ERROR);
                return;
            }
//            onApproveFile();
            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    /**
     * PHE DUYET HO SO, TAO BAN CONG BO
     *
     * @throws Exception
     */
    public void onApproveFile() throws Exception {
//        CosEvaluationRecordDAO objDAOHE = new CosEvaluationRecordDAO();
//        obj = createApproveObject();
//        objDAOHE.saveOrUpdate(obj);
        //onSignPermit();
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);

        if (lstPermit.size() > 0) {//Co co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
            permitDAO.saveOrUpdate(permit);
            Long bookNumber = putInBook(permit);//Vao so
            String receiveNo = getPermitNo(bookNumber);//lay so cong bo
            permit.setReceiveNo(receiveNo);
            permitDAO.saveOrUpdate(permit);
        }

        ExportDeviceModel exportDeviceModel = new ExportDeviceModel(fileId);
        exportDeviceModel.setSendNo(permit.getReceiveNo());
        exportDeviceModel.setSignedDate(permit.getSignDate());
        exportDeviceModel.setCosmeticPermitId(permit.getPermitId());
        ExportDeviceDAO edp = new ExportDeviceDAO();
        edp.exportCosPermit(exportDeviceModel, false);

        base.showNotify("Phê duyệt hồ sơ thành công!");
    }

    /**
     *
     * @param event
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) {
//       String data = event.getData().toString();
//
//       ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = "D:\\vofficeUpload\\signPdf\\";
        FileUtil.mkdirs(filePath);
//       String inputFileName = "_" + (new Date()).getTime() + ".pdf";//NOT ƯSE BINHNT53 230315
        String outputFileName = "_signed_LDBoPheDuyetCongVanNhapKhau_" + (new Date()).getTime() + ".pdf";
//       String inputFile = filePath + inputFileName;//NOT ƯSE BINHNT53 230315
        String outputFile = filePath + outputFileName;
        fileSignOut = outputFile;
        String signature = "eSyhsQSHaOCdx7SJnEn94wdzp+ll6bFPyReoV4djUej6rFQrIMXOfQgJqYdHb4csfnwxPsfnMAXhQh6qIL4N5chn9CQSB4uR6VrMQBVNLWcAkvQU5dzq6xQRx7oxPYmaNK3LG44m9pDoJUMibp7btWAGFT3M27G3YO6GeNywIpk=";
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");

        try {
            pdfSig.insertSignature(signature, outputFile);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        LogUtils.addLog("Signed file: " + outputFile);
        try {
            onSignPermit();
            fileSignOut = "";
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
    }

    /**
     *
     * @throws Exception
     */
    public void onSignPermit() throws Exception {
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);

        if (lstPermit.size() > 0) {//Co co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
            permitDAO.saveOrUpdate(permit);
            Long bookNumber = putInBook(permit);//Vao so
            String receiveNo = getPermitNo(bookNumber);//lay so cong bo
            permit.setReceiveNo(receiveNo);
            permitDAO.saveOrUpdate(permit);
        }

        ExportDeviceDAO edD = new ExportDeviceDAO();
        ExportDeviceModel edm = new ExportDeviceModel(fileId);
        edm.setSendNo(permit.getReceiveNo());
        edm.setSignedDate(permit.getSignDate());
        edm.setCosmeticPermitId(fileId);
        edD.updateAttachSignImportDeviceFile(edm, fileSignOut, Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDB);
        //lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi luu vao attach)

//      CosmeticDAO cosmeticDAO = new CosmeticDAO();
//      VFileCosfile vFileCosfile2 = cosmeticDAO.findViewByFileId(fileId);
//      String pathTemplate = getPathTemplate(fileId);
//      CosExportModel model = setModelObject(cospermit, vFileCosfile2, pathTemplate);
//
//      CosExportFileDAO exportFileDAO = new CosExportFileDAO();
//
//      exportFileDAO.exportPermit_Cos(model);
//     
//     FilesModel fileModel = new FilesModel(fileId);
//     
//     fileModel.setSendNo(cospermit.getReceiveNo());
//     fileModel.setSignDate(cospermit.getSignDate());
//     fileModel.setCosmeticPermitId(cospermit.getPermitId());
//     ExportFileDAO exp = new ExportFileDAO();
//     exp.exportCosmeticAnnouncement(fileModel, false);
//      showNotification(String.format(
//              Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
//              Constants.Notification.INFO);
        showNotification("Ký số thành công!", Constants.Notification.INFO);
        fillFinalFileListbox(fileId);
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        actionSignCA(event, actionPrepareSign());
    }

    public String actionPrepareSign() {
        String fileToSign;
        String path = "";
        String nameFile = "";
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId, Constants.OBJECT_TYPE.COSMETIC_PERMIT);
        if (lstAttach.size() > 0) {
            path = lstAttach.get(0).getAttachPath();
            nameFile = "_" + lstAttach.get(0).getAttachName();
        }

        fileToSign = path + nameFile;//onApproveFileSign();
        return fileToSign;
    }

    /**
     * Phe duyet ho so, kem CKS
     *
     * @return @throws Exception
     */
    public String onApproveFileSign() throws Exception {
//        CosEvaluationRecordDAO objDAOHE = new CosEvaluationRecordDAO();
//        obj = createApproveObject();
//        objDAOHE.saveOrUpdate(obj);
        //onSignPermit();
        clearWarningMessage();
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);

        if (lstPermit.size() > 0) {//Da co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
        }
        permitDAO.saveOrUpdate(permit);
        Long bookNumber = putInBook(permit);//Vao so
        String receiveNo = getPermitNo(bookNumber);
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        ExportDeviceModel exportDeviceModel = new ExportDeviceModel(fileId);
        exportDeviceModel.setSendNo(permit.getReceiveNo());
        exportDeviceModel.setSignedDate(permit.getSignDate());
        exportDeviceModel.setCosmeticPermitId(permit.getPermitId());

        ExportDeviceDAO exp = new ExportDeviceDAO();
        return exp.exportIDFNoSign(exportDeviceModel, false);

        //base.showNotify("Phê duyệt hồ sơ thành công!");
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Permit Permit) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);//Lay so dau tien
        BookDocument bookDocument = createBookDocument(Permit.getPermitId(), book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    private String getPermitNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy"); // Just the year, with 2 digits
        String year = sdf.format(Calendar.getInstance().getTime());
        permitNo += "/" + year;
        permitNo += "/TTBYT";
        return permitNo;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String certSerial = null;//x509Cert.getSerialNumber().toString(16);

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
        String outputFileFinalName = "_signed_LDBoPheDuyetCongVanNhapKhau_" + (new Date()).getTime() + ".pdf";
        String outPutFileFinal = filePath + outputFileFinalName;
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");//not use binhnt53 u230315
            String linkImageSign = folderPath + separator + caur.get(0).getSignature();
            String linkImageStamp = folderPath + separator + caur.get(0).getStamper();
            //fileSignOut = outPutFileFinal;
            Pdf pdfProcess = new Pdf();
            try {//chen chu ki
                pdfProcess.insertImageAll(fileToSign, outPutFileFinal, linkImageSign, linkImageStamp, "SIB");
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
//            if (pdfProcess == null) {
//                showNotification("Ký số không thành công!");
//                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
//                return;
//            }
//            try {//chen CKS
//                base64Hash = pdfSig.createHash(outPutFileFinal, new Certificate[]{x509Cert});
//            } catch (Exception ex) {
//                Logger.getLogger(HeathLeadshipController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//            Session session = Sessions.getCurrent();
//            try {
//            	 session.setAttribute("certSerial", certSerial);
//                 session.setAttribute("base64Hash", base64Hash);
//                 txtCertSERIALHEATH.setValue(certSerial);
//                 txtBase64HASHHEATH.setValue(base64Hash);
//			} catch (Exception e) {
//				LogUtils.addLogDB(e);
//			}
//           
//            session.setAttribute("PDFSignature", pdfSig);
////            Clients.evalJavaScript("signAndSubmitHeathLeadershipFile();");
//            Event e = new Event("");
//            onSign(e);
            try {
                fileSignOut = outPutFileFinal;
                onSignPermit();
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachNew = new ArrayList<Attachs>();
        List<Attachs> lstAttachOld = rDaoHe.getByObjectIdAndType(fileId, Constants.OBJECT_TYPE.COSMETIC_PERMIT);
        if (lstAttachOld.size() > 0) {
            lstAttachNew.add(lstAttachOld.get(0));
        }
        this.finalFileListbox.setModel(new ListModelArray(lstAttachNew));
    }

    private Permit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;

    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

}
