package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.util.media.Media;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;

/**
 *
 * @author Linhdx
 */
public class ID_11_1_BoardSecretarySDBSController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Listbox lbAppraisal;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lblMember;
    private Long fileId, evalType;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    Vlayout flist;
    private List<Media> listMedia;
    @Wire
    private Textbox txtValidate;
    private EvaluationRecord obj = null;
    // private Textbox txtNote = (Textbox)Path.getComponent("/windowProcessing/txtNote");
    private Listbox lbAction = (Listbox) Path.getComponent("/windowProcessing/lbAction");

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        evalType = Constants.FILE_STATUS_CODE.STATUS_HHD;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        txtValidate.setValue("0");
    }

    void fillHoSo() {
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        if (lbAction != null && lbAction.getSelectedItem() != null) {
            lblMember.setValue(lbAction.getSelectedItem().getLabel());
        }
        obj = objDAO.findContentByFileIdAndEvalType(fileId, evalType);
        if (obj != null) {
            tbResonRequest.setValue(obj.getMainContent());
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tham dinh ho so thiet bi y te:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbResonRequest.getText().matches("\\s*")) {
            showWarningMessage("Kết luận không được để trống !!!");
            tbResonRequest.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();
        listMedia = new ArrayList<>();
        flist.getChildren().clear();
        listMedia.clear();
        Long limitSize = Long.parseLong(getConfigValue("limitSizeAttachPerDraft", "config", "20971520"));
        String limit = getConfigValue("limitSizeStr", "config", "3M");
        for (final Media media : medias) {
            Long mediaSize = Integer.valueOf(media.getByteData().length).longValue();
            if (mediaSize > limitSize) {
                showNotification("Dung lượng file đính kèm không được vượt quá " + limit, Clients.NOTIFICATION_TYPE_INFO);
                return;
            }
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            //MinhNV Data Model Gson
            Gson gson = new Gson();
            String reson = tbResonRequest.getValue();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setContent(reson);
            //            evaluModel.setTypeProfessional(Constants.TYPE_PROFESSIONAL.TYPE_PROFESSIONAL_MAIN);
            evaluModel.setResultEvaluation(2L);
            evaluModel.setResultEvaluationStr(lblMember.getValue());
            String jsonEvaluation = gson.toJson(evaluModel);

            if (obj == null) {
                obj = new EvaluationRecord();
            }
            obj.setMainContent(reson);
            obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_HHD);
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            //MinhNV add evalType SĐBS
            obj.setEvalType(Constants.FILE_STATUS_CODE.STATUS_HHD);// Vunt - type SDBS thu ky hoi dong
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);
            //

//            String comment = tbResonRequest.getValue();
//            AdditionalRequest objAR = new AdditionalRequest();
//            objAR.setContent(comment);
//            objAR.setCreatorId(getUserId());
//            objAR.setCreatorName(getUserFullName());
//            objAR.setFileId(fileId);
//            objAR.setCreateDate(new Date());
//            objAR.setIsActive(Constants.Status.ACTIVE);
//            AdditionalRequestDAO objDAOHE = new AdditionalRequestDAO();
//            objDAOHE.saveOrUpdate(objAR);
            if (flist.getChildren() == null || flist.getChildren().isEmpty()) {
                showWarningMessage("Chưa chọn file đính kèm");
                return;
            }
            if (fileId != null) {
                AttachDAO base = new AttachDAO();

                for (Media media : listMedia) {
                    base.saveReportMeetingFileAttach(media, fileId, Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE, Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE);
                }
                listMedia.clear();
                flist.getChildren().clear();
                lbTopWarning.setValue("");
                lbBottomWarning.setValue("");
                // showWarningMessage(flist.getChildren().size()+"");
            }
            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

}
