package com.viettel.module.importDevice.Controller.include;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.Controller.include.CosEvaluationSignPermitController;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.DAO.ExportDeviceDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceProductDAO;
import com.viettel.module.importDevice.Model.ExportDeviceModel;
import com.viettel.newsignature.plugin.SignPdfFile;
import com.viettel.newsignature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.ws.BO.Document;
import com.viettel.ws.BO.Response;
import com.viettel.ws.Helper_VofficeMoh;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

/**
 *
 * @author Linhdx
 */
public class ID_13_2_DepartmentLeadershipController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Listbox finalFileListbox;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    private Permit permit;

    private List listBook;
    private Long docType;
    private String bCode;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private String fileSignOut = "";

    private UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute("userToken");

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_PERMIT;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillFinalFileListbox(fileId);
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tham dinh ho so thiet bi y te:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachType(fileId,
                Constants.OBJECT_TYPE.COSMETIC_PERMIT, Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV);
        if (lstAttach.size() <= 0) {
            return false;
        }
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                showNotification(String.format(
                        "Hãy thực hiện ký số", Constants.DOCUMENT_TYPE_NAME.FILE),
                        Constants.Notification.ERROR);
                return;
            }
//            onApproveFile();
            //    onApproveFileSign();

            updateAttachFileSigned();

            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    /**
     *
     * @param event
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) {
        String data = event.getData().toString();

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
//       String inputFileName = "_" + (new Date()).getTime() + ".pdf";//NOT ƯSE BINHNT53 230315
        String outputFileName = "_signed_PheDuyetCongVanNhapKhau_Ldv_" + (new Date()).getTime() + ".pdf";
//       String inputFile = filePath + inputFileName;//NOT ƯSE BINHNT53 230315
        String outputFile = filePath + outputFileName;
        fileSignOut = outputFile;
        String signature = data;
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");
        String outputFileFinal = session.getAttribute("outputFileFinal").toString();

        try {
            //pdfSig.insertSignature(signature, outputFile);
            pdfSig.insertSignature(signature, outputFileFinal, fileSignOut);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        //LogUtils.addLog("Signed file: " + outputFile);
        try {
            onSignPermit();
            fileSignOut = "";
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
    }

    /**
     *
     * @throws Exception
     */
    public void onSignPermit() throws Exception {
        ExportDeviceDAO edD = new ExportDeviceDAO();
        ExportDeviceModel edm = new ExportDeviceModel(fileId);
        edm.setSendNo(permit.getReceiveNo());
        edm.setSignedDate(permit.getSignDate());
        edm.setCosmeticPermitId(fileId);
        edD.updateAttachSignImportDeviceFile(edm, fileSignOut, Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV);
        //lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi luu vao attach)

//      CosmeticDAO cosmeticDAO = new CosmeticDAO();
//      VFileCosfile vFileCosfile2 = cosmeticDAO.findViewByFileId(fileId);
//      String pathTemplate = getPathTemplate(fileId);
//      CosExportModel model = setModelObject(cospermit, vFileCosfile2, pathTemplate);
//
//      CosExportFileDAO exportFileDAO = new CosExportFileDAO();
//
//      exportFileDAO.exportPermit_Cos(model);
//     
//     FilesModel fileModel = new FilesModel(fileId);
//     
//     fileModel.setSendNo(cospermit.getReceiveNo());
//     fileModel.setSignDate(cospermit.getSignDate());
//     fileModel.setCosmeticPermitId(cospermit.getPermitId());
//     ExportFileDAO exp = new ExportFileDAO();
//     exp.exportCosmeticAnnouncement(fileModel, false);
//      showNotification(String.format(
//              Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
//              Constants.Notification.INFO);
        showNotification("Ký số thành công!", Constants.Notification.INFO);
        fillFinalFileListbox(fileId);
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        actionSignCA(event, actionPrepareSign());
    }

    public String actionPrepareSign() {
        String fileToSign = "";
        try {
            fileToSign = onApproveFileSign();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    /**
     * Phe duyet ho so, kem CKS
     *
     * @return @throws Exception
     */
    public String onApproveFileSign() throws Exception {

        clearWarningMessage();
        PermitDAO permitDAO = new PermitDAO();
        //List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);
        //linhdx 20160506 update permitNumber, PermitDate vao bang import_device_file
        ImportDeviceFiletDAO dao = new ImportDeviceFiletDAO();
        ImportDeviceFile id = dao.findByFileId(fileId);
        //linhdx 20160128 do bi trung ho so TTB va DYCNK nen tao moi giay phep
        permit = new Permit();
        createPermit();
        permitDAO.saveOrUpdate(permit);
        //linhdx 20160701
        //Neu ho so gia han thi se lay noi dung cua ho so cu de ky
        ExportDeviceModel exportDeviceModel = new ExportDeviceModel();
        if (Objects.equals(id.getTypeCreate(), Constants.IMPORT.TYPE_CREATE_GIAHAN)) {
            String sendBookNumber = id.getSendBookNumber();
            PermitDAO pDAO = new PermitDAO();
            Permit permitOld = pDAO.findTtbCreateNewByReveiveNo(sendBookNumber);
            if(permitOld != null){
                ImportDeviceFile idOld = dao.findByFileId(permitOld.getFileId());
                exportDeviceModel = new  ExportDeviceModel(idOld.getFileId());
                permit.setReceiveNo(permitOld.getReceiveNo());//lay so cu
                
                exportDeviceModel.setuQDate(id.getUqDate());//lay han moi
                
            }
            
        } else {
            exportDeviceModel = new ExportDeviceModel(fileId);
            Long bookNumber = putInBook(permit);//Vao so
            String receiveNo = getPermitNo(bookNumber);
            permit.setReceiveNo(receiveNo);
        }
        permitDAO.saveOrUpdate(permit);
        id.setPermitDate(permit.getSignDate());
        id.setPermitNumber(permit.getReceiveNo());
        dao.saveOrUpdate(id);

        exportDeviceModel.setSendNo(permit.getReceiveNo());
        exportDeviceModel.setSignedDate(permit.getSignDate());
        exportDeviceModel.setCosmeticPermitId(permit.getPermitId());
        
        ImportDeviceFile idf = exportDeviceModel.getIdfFile();
        //20161028
        //sua la truong cong van cua DN lay theo ho so moi
        idf.setEquipmentNo(id.getEquipmentNo());
        exportDeviceModel.setIdfFile(idf);
        //Sua truuong ngay ky cua dn
        exportDeviceModel.setCreateDate(id.getSignDate());

        ExportDeviceDAO exp = new ExportDeviceDAO(); 
        return exp.exportIDFNoSign(exportDeviceModel, false, getModelFile());

        //base.showNotify("Phê duyệt hồ sơ thành công!");
    }

    private List<String> getModelFile() throws Exception {
        List<String> rs = new ArrayList<>();
        AttachDAOHE attDAO = new AttachDAOHE();
        ImportDeviceProductDAO idpDAO = new ImportDeviceProductDAO();
        Long fileIdToSearchModel = fileId;
        try{
        	//Neu ho so gia han thi se lay noi dung cua ho so cu de ky
            ImportDeviceFiletDAO dao = new ImportDeviceFiletDAO();
            ImportDeviceFile id = dao.findByFileId(fileId);
            if (Objects.equals(id.getTypeCreate(), Constants.IMPORT.TYPE_CREATE_GIAHAN)) {
                String sendBookNumber = id.getSendBookNumber(); 
                PermitDAO pDAO = new PermitDAO();
                Permit permitOld = pDAO.findTtbCreateNewByReveiveNo(sendBookNumber);
                if(permitOld != null){
                	fileIdToSearchModel = permitOld.getFileId();
                }
            }
        } catch (Exception ex){
        	LogUtils.addLogNoImportant(ex);
        }
        
        
        @SuppressWarnings("unchecked")
		List<ImportDeviceProduct> lsIDP = idpDAO.findByImportFileId(fileIdToSearchModel);
        for (ImportDeviceProduct idp : lsIDP) {
            if (idp.getIsCategory() == 1L) {
                Attachs a = attDAO.getByObjectIdAndAttachCat(idp.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL);
                if (a != null) {
                    rs.add(a.getFullPathFile());
                }

            }
        }
        return rs;
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Permit Permit) throws Exception {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);//Lay so dau tien
        BookDocument bookDocument = createBookDocument(Permit.getPermitId(), book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) throws Exception {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        ImportDeviceFiletDAO imfDao = new ImportDeviceFiletDAO();
        ImportDeviceFile file = imfDao.findByFileId(fileId);
        if (file.getBookSend() == null) {
//            UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
//                    "userToken");
            SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Document d = new Document();
            Helper_VofficeMoh hp = new Helper_VofficeMoh();

            d.setReceiveId(String.valueOf(file.getReceiveId()));
            d.setFrom("Vụ trang thiết bị y tế");
            d.setSignDate(formatterDateTime.format(new Date()));
            d.setUserSign("tuannm.ttbct");
            d.setSignName("Nguyễn Minh Tuấn");
            d.setEditor("tuannm.ttbct");
            d.setEditorName("Nguyễn Minh Tuấn");
//            d.setUserSign(tk.getUserName());
//            d.setSignName(tk.getUserFullName());
//            d.setEditor(tk.getUserName());
//            d.setEditorName(tk.getUserFullName());
            d.setBusinessAddress(file.getImporterAddress());
            d.setBusinessName(file.getImporterName());
            d.setType("1");
            d.setContent("Vụ Trang thiết bị y tế ký công văn cấp phép cho hồ sơ mã: " + file.getNswFileCode()
                    + " của doanh nghiệp: " + file.getImporterName() + ", địa chỉ: " + file.getImporterAddress() + ", điện thoại: " + file.getImporterPhone());
            Response rs = hp.getSendVal(d, file, bookId);
            if (rs == null) {
                showWarningMessage("Lỗi nhận số cho công văn cấp phép từ Voffice Bộ Y tế");
                return null;
            }
            file.setBookSend(Long.parseLong(rs.getResponseNo()));
            file.setSendId(Long.parseLong(rs.getResponseId()));
            imfDao.saveOrUpdate(file);
        }
        Long maxBookNumber = file.getBookSend();

        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    private String getPermitNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy"); // Just the year, with 2 digits
//        String year = sdf.format(Calendar.getInstance().getTime());
        permitNo += "NK";
        //Cuongvv
        permitNo += "/BYT-TB-CT";
        return permitNo;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        if (fileToSign == null) {
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
            return;
        }
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        SignPdfFile pdfSig;//pdfSig = new SignPdfFile();        
        String certSerial = x509Cert.getSerialNumber().toString(16);

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");
        FileUtil.mkdirs(filePath);
        String linkImageSign;
        CaUserDAO ca = new CaUserDAO();
        Long userId = getUserId();
        List<CaUser> caur = ca.findCaByUserId(1L, userId);
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            linkImageSign = request.getRealPath("/Share/img/signature.png");
            //fileSignOut = outPutFileFinal;
            Pdf pdfProcess = new Pdf();

            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String outputFileFinal = resourceBundle.getString("signTemp") + "_" + (new Date()).getTime() + ".pdf";

            String fieldName = " Nguyễn Minh Tuấn(Ldv)";

            pdfSig = pdfProcess.createHash(fileToSign, outputFileFinal, new Certificate[]{x509Cert}, linkImageSign, fieldName); 
            if (pdfSig == null) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }
            Session session = Sessions.getCurrent();
            session.setAttribute("outputFileFinal", outputFileFinal);
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", pdfProcess.getBase64Hash());
            txtBase64HASH.setValue(pdfProcess.getBase64Hash());
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachNew = new ArrayList<Attachs>();
        List<Attachs> lstAttachOld = rDaoHe.getByObjectIdAndAttachCatAndAttachType(fileId,
                Constants.OBJECT_TYPE.COSMETIC_PERMIT, Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV);
        if (lstAttachOld.size() > 0) {
            lstAttachNew.add(lstAttachOld.get(0));
        }
        Attachs bbhd = rDaoHe.getByObjectIdAndAttachCat(fileId, Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE);
        lstAttachNew.add(bbhd);
        this.finalFileListbox.setModel(new ListModelArray(lstAttachNew));
    }

    private Permit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;

    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    public void updateAttachFileSigned() {
        // get file attach
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachType(fileId,
                Constants.OBJECT_TYPE.COSMETIC_PERMIT, Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV);
        if (lstAttach != null && lstAttach.size() > 0) {
            for (Attachs att : lstAttach) {
                Attachs att_new = att;
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att_new);
                }
            }
        }

    }
}
