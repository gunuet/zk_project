package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.DAO.ImportDeviceProductDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.UnsupportedEncodingException;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class ID_1_1_SendFileController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Textbox txtValidate;
    private Textbox txtNote = (Textbox) Path.getComponent("/windowProcessing/txtNote");
    private Long fileId;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
      private boolean isValidatedData() throws UnsupportedEncodingException {
        try {
            AttachDAOHE att = new AttachDAOHE();
            ImportDeviceProductDAO ipDAO = new ImportDeviceProductDAO();
            List<ImportDeviceProduct> lsip = ipDAO.findByImportFileId(fileId);
            if (lsip == null || lsip.isEmpty()) {
                showNotification("Hồ sơ chưa có sản phẩm !!!");
                return false;
            }
            List<Attachs> lst = att.getByObjectIdAndType(fileId, Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE);

            if (lst.isEmpty()) {
                showNotification("Chưa đính kèm đăng ký kinh doanh !!!");
                return false;
            }
            for (ImportDeviceProduct idp : lsip) {
                if (idp.getIsCategory().equals(1L)) {
                    Attachs atm = att.getByObjectIdAndAttachCat(idp.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL);
                    if (atm == null) {
                        showNotification("Sản phẩm sản xuất bởi " + idp.getManufacturer() + " không có phụ lục đính kèm như đã chọn !!!");
                        return false;
                    }
                }
                List<Attachs> lsa = att.getAllByObjectIdAndType(idp.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_ATT);
                if (lsa == null || lsa.isEmpty()) {
                    showNotification("Sản phẩm " + idp.getModel() + " chưa nhập tài liệu đính kèm theo hướng dẫn !!!");
                    return false;
                } else {
                    String[] type = ResourceBundleUtil.getString("TTB_Type").split(";");
                    for (String s : type) {
                        if (StringUtils.validString(s)) {
                            Long k = Long.parseLong(s);
                            Boolean ck = false;
                            for (Attachs a : lsa) {
                                if (a.getAttachType().equals(k)) {
                                    ck = true;
                                    break;
                                }
                            }
                            if (!ck) {
                                showNotification("Sản phẩm " + idp.getModel() + " chưa nhập tài liệu đính kèm theo hướng dẫn !!!");
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }
    private boolean contain(List<Attachs> lst, Long attachType) {
        for (Attachs att : lst) {
            if (att.getAttachType() != null && att.getAttachType().equals(attachType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;

            }

            txtValidate.setValue("1");
            

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

}
