package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.convert.service.PdfDocxFile;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importDevice.DAO.ExportDeviceDAO;
import com.viettel.module.importDevice.Model.ExportDeviceModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.ws.Helper_VofficeMoh;
import java.io.IOException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Linhdx
 */
public class ID_21_1_TK_Reject extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate, txtFinishDate;
    @Wire
    private Listbox finalFileListbox;
    @Wire
    private Listbox finalFileListboxKy;
    @Wire
    Textbox txtCertSERIALHEATH, txtBase64HASHHEATH;
    private Permit permit;

    private List listBook;
    private Long docType;
    private String bCode;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private String fileSignOut = "";

    private List<Attachs> lstAttach;
    private List<Attachs> lstAttachKy;
    private UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute("userToken");
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    
    @Wire
    private Textbox txtMessage;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Thu ky Tu choi:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                showNotification("Lỗi gửi kết quả xử lý sang NSW");
                return;
            }

            txtValidate.setValue("1");
            txtFinishDate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() throws Exception {
        //createDispathAttach();
        approveDispath();
        sendMS();
        return true;
    }

//    /**
//     * Thu ky tao cong van tu choi
//     *
//     * @return
//     */
//    private void createDispathAttach() throws IOException {
//        AttachDAO attDAO = new AttachDAO();
//        String fileName = "_congvantuchoittbytNoSign.docx";
//        attDAO.saveFileAttachReject(fileName, fileId, Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH, Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV);
//    }
    
    
    private void approveDispath() throws Exception{
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);

        if (lstPermit.size() > 0) {//Da co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
        }
        permit.setReceiveNo("1");
        permitDAO.saveOrUpdate(permit);
        
        
        ExportDeviceModel exportDeviceModel = new ExportDeviceModel(fileId);
        exportDeviceModel.setSendNo(permit.getReceiveNo());
        exportDeviceModel.setSignedDate(permit.getSignDate());
        exportDeviceModel.setCosmeticPermitId(permit.getPermitId());
        //exportDeviceModel.setContent("Ly do abc");
        
        ExportDeviceDAO edp = new ExportDeviceDAO();
        PdfDocxFile outputStream = edp.exportIdfNgoaiDM(exportDeviceModel, false);
        AttachDAO attDAO = new AttachDAO();
        String fileName = "congvantuchoittbytNgoaiDM.pdf";
        attDAO.saveFileAttach(outputStream, fileName, fileId, Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH, Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV);
    }
    
    
    private Permit createPermit() throws Exception {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;

    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    private void sendMS() throws Exception {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_ID.SendMs_39);
        md.setPhase(2L);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
        
//        Helper_VofficeMoh hp = new Helper_VofficeMoh();
//        return hp.SendMs_39(fileId, fileId, 2L);
    }
}
