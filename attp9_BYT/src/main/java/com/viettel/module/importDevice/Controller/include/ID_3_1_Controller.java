package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.opensymphony.xwork2.util.TextUtils;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.UnsupportedEncodingException;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.struts2.views.util.TextUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

/**
 *
 * @author MrBi
 */
public class ID_3_1_Controller extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbNote;
    @Wire
    private Listbox lbcvc;
    private Long fileId, evalType;
    @Wire
    private Textbox txtValidate;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillListCVC();
        txtValidate.setValue("0");
    }

    @SuppressWarnings("unchecked")
    private void fillListCVC() throws Exception {
        UserDAOHE uDAO = new UserDAOHE();
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        List<Users> lsu =uDAO.getUserByDeptPosID(tk.getDeptId(), 1L);
        lbcvc.setModel(new ListModelList(lsu));
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Phân công xử lý hồ sơ TTB:" + fileId);
            
            if(onSave()){
                txtValidate.setValue("1");
            };
            
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    private boolean isValidatedData() {

        if (lbcvc.getSelectedItem() == null) {
            showWarningMessage("Chưa chọn chuyên viên chéo");
            lbcvc.focus();
            return false;
        }

        return true;
    }

    public boolean onSave() {
        if (!isValidatedData()) {
            return false;
        }
        FilesDAOHE fDAO = new FilesDAOHE();
        Files f =fDAO.findById(fileId);
        f.setNextUser(Long.parseLong(lbcvc.getSelectedItem().getValue().toString()));
        fDAO.saveOrUpdate(f);
        return true;
    }
}
