package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class ID_4_1_AppraisalMainController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Listbox lbAppraisal;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lblNote, lbNote;
    private Long fileId, evalType;
    //linhdx 20160226 ghep 2 truong thanh 1
//    @Wire
//    private Textbox tbResonRequest;
    private Textbox tbResonRequest = (Textbox)Path.getComponent("/windowProcessing/txtNote");
    @Wire
    private Textbox txtValidate, txtMessage, txtNextUser;
    
    private Files f;
    
    private ImportDeviceFile importDeviceFile;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        ImportDeviceFiletDAO idpDAO = new ImportDeviceFiletDAO();
        importDeviceFile = idpDAO.findByFileId(fileId);
        evalType = Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHDAT;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        FilesDAOHE fDAO = new FilesDAOHE();
        f = fDAO.findById(fileId);
        if (f.getNextUser() != null) {
            txtNextUser.setValue(f.getNextUser() == null ? null : f.getNextUser().toString());
        }
        fillHoSo();
        txtValidate.setValue("0");
    }

    void fillHoSo() {
        Gson gson = new Gson();
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId, evalType);
        tbResonRequest.setValue(importDeviceFile.getCommentCV());
        if (lstEvaluationRecord.size() > 0) {
            obj = lstEvaluationRecord.get(0);
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            List<Listitem> listItem = lbAppraisal.getItems();
            for (Listitem listItem1 : listItem) {
                if (!Objects.equals(Long.valueOf((String) listItem1.getValue()), evModel.getResultEvaluation())) {
                } else {
                    lbAppraisal.setSelectedItem(listItem1);
                    break;
                }
            }
            
            tbResonRequest.setValue(obj.getMainContent());
            lbNote.setValue(evModel.getResultEvaluationStr());
            UserDAOHE uDAO = new UserDAOHE();
            Users u = uDAO.findById(evModel.getUserId());
            lblNote.setValue("Ghi chú của " + evModel.getUserName() + " - " + u.getFullName() + " :");
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
//            if (f.getNextUser() != null) {
//                f.setNextUser(null);
//                FilesDAOHE fDAO = new FilesDAOHE();
//                fDAO.saveOrUpdate(f);
//            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

//        if (tbResonRequest.getText().matches("\\s*")) {
//            showWarningMessage("Ý kiến thẩm định không thể để trống");
//            tbResonRequest.focus();
//            return false;
//        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     * @throws java.lang.Exception
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            Gson gson = new Gson();
            String reson = tbResonRequest.getValue().trim();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setContent(reson);
            evaluModel.setResultEvaluationStr(lbAppraisal.getSelectedItem().getLabel());
            evaluModel.setResultEvaluation(Long.valueOf((String) lbAppraisal.getSelectedItem().getValue()));
            evaluModel.setTypeProfessional(Constants.TYPE_PROFESSIONAL.TYPE_PROFESSIONAL_MAIN); // chuyen vien chinh
            String jsonEvaluation = gson.toJson(evaluModel);
            txtMessage.setValue(jsonEvaluation);

            EvaluationRecord obj = new EvaluationRecord();
            obj.setMainContent(reson);
            if (Long.valueOf((String) lbAppraisal.getSelectedItem().getValue()) == 1L) {
                obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHDAT);
            } else {
                obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHKHONGDAT);
            }

            //MinhNV add evalType 
            obj.setEvalType(Constants.EVAL_TYPE.ROLE_CVC);
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);
            txtValidate.setValue("1");
            

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

}
