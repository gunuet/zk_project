package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class ID_4_3_Controller extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Listbox lbAppraisal;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbNote, lblNote;
    private Long fileId, evalType;
    
    private Textbox  tbNote;
    @Wire
    private Textbox txtValidate, txtMessage;
    private Textbox txtNote = (Textbox) Path.getComponent("/windowProcessing/txtNote");
    
    private ImportDeviceFile importDeviceFile;
    
    //linhdx 20160226 ghep 2 truong ghi chu
//    @Wire
//    private Textbox tbResonRequest;
    private Textbox tbResonRequest = (Textbox)Path.getComponent("/windowProcessing/txtNote");

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        ImportDeviceFiletDAO idpDAO = new ImportDeviceFiletDAO();
        importDeviceFile = idpDAO.findByFileId(fileId);
        evalType = Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        txtValidate.setValue("0");
    }

    void fillHoSo() {
        Gson gson = new Gson();
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId,
                evalType);
        tbResonRequest.setValue(importDeviceFile.getCommentCV());
        if (lstEvaluationRecord.size() > 0) {
            obj = lstEvaluationRecord.get(0);
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(),
                    EvaluationModel.class);
            lbNote.setValue(evModel.getResultEvaluationStr());
            UserDAOHE uDAO = new UserDAOHE();
            Users u = uDAO.findById(evModel.getUserId());
             lblNote.setValue("Ghi chú của "+evModel.getUserName()+" - "+u.getFullName()+" :");
            tbResonRequest.setValue(obj.getMainContent());
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tham dinh ho so thiet bi y te:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbResonRequest.getText().matches("\\s*")) {
            showWarningMessage("Nội dung yêu cầu bổ sung không thể để trống");
            tbResonRequest.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            //MinhNV Data Model Gson
            Gson gson = new Gson();
            String reson = tbResonRequest.getValue().trim();
            String note = txtNote.getValue();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setResultEvaluationStr(note);
            evaluModel.setContent(reson);
            evaluModel.setTypeProfessional(Constants.TYPE_PROFESSIONAL.TYPE_PROFESSIONAL_MAIN);
            evaluModel.setResultEvaluation(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
            String jsonEvaluation = gson.toJson(evaluModel);
            //MinhNV -- Save Data
            EvaluationRecord obj = new EvaluationRecord();
            obj.setMainContent(reson);
            obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            //MinhNV add evalType SĐBS
            obj.setEvalType(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);// MinhNV - type SDBS chuyen vien chinh
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);
            
            
            
            AdditionalRequest objAR = new AdditionalRequest();
            objAR.setContent(reson);
            objAR.setCreatorId(getUserId());
            objAR.setCreatorName(getUserFullName());
            objAR.setFileId(fileId);
            objAR.setCreateDate(new Date());
            objAR.setIsActive(Constants.Status.ACTIVE);
            AdditionalRequestDAO objDAOHE = new AdditionalRequestDAO();
            objDAOHE.saveOrUpdate(objAR);

            sendMS();
            txtValidate.setValue("1");


        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }
    
    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_ID.SendMs_42);
        md.setPhase(1l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

}
