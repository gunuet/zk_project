/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller.include;

import java.util.Date;
import java.util.Map;

import com.google.gson.Gson;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

/**
 *
 * @author duv
 */
public class ID_5_2_AppraisalCrossNotOkController extends BusinessController {

       private static final long serialVersionUID = 1L;
//    @Wire
//    private Listbox lbResultCheck;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lblAppraisal, lblResonRequest, lbResultCheck;
    private Long fileId, evalType;
//    @Wire
//    private Textbox tbOptionCheck;
    private Textbox tbOptionCheck = (Textbox)Path.getComponent("/windowProcessing/txtNote");
    
    private ImportDeviceFile importDeviceFile;
    @Wire
    private Textbox txtValidate;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        ImportDeviceFiletDAO idpDAO = new ImportDeviceFiletDAO();
        importDeviceFile = idpDAO.findByFileId(fileId);
        evalType = Constants.EVAL_TYPE.ROLE_CVC;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        txtValidate.setValue("0");
    }

    void fillHoSo() {
        Gson gson = new Gson();
        tbOptionCheck.setValue(importDeviceFile.getCommentCV());
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId, evalType);
        if (lstEvaluationRecord.size() > 0) {
            obj = lstEvaluationRecord.get(0);
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            if (evModel.getResultEvaluation() == 1) {
                lblAppraisal.setValue("Đạt yêu cầu");               
            } else if (evModel.getResultEvaluation() == 4) {
                lblAppraisal.setValue("Ngoài danh mục");               
            } else {
                lbResultCheck.setValue("Không đạt");
            }
            lbResultCheck.setValue("Không đạt");
            lblResonRequest.setValue(obj.getMainContent());
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

//        if (tbOptionCheck.getText().matches("\\s*")) {
//            showWarningMessage("Ý kiến thẩm tra không được để trống");
//            tbOptionCheck.focus();
//            return false;
//        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            Gson gson = new Gson();
            String reson = tbOptionCheck.getValue().trim();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setContent(reson);
            evaluModel.setResultEvaluationStr(lbResultCheck.getValue());
            evaluModel.setResultEvaluation(2L);
            evaluModel.setTypeProfessional(Constants.TYPE_PROFESSIONAL.TYPE_PROFESSIONAL_CROSS); // chuyen vien kiem tra cheo
            String jsonEvaluation = gson.toJson(evaluModel);

            EvaluationRecord obj = new EvaluationRecord();
            obj.setMainContent(reson);
            obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHKHONGDAT);
            //MinhNV add evalType 
            obj.setEvalType(Constants.EVAL_TYPE.ROLE_CVKTC);
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);
            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

}
