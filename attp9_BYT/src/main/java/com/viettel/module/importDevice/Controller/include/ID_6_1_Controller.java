package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author MrBi
 */
public class ID_6_1_Controller extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning,lbNote,lblNote;
//    lblAppraisal, lblResonRequest, lblResultCheck, lblOptionCheck;
    @Wire
    private Listbox lbResult;
    private Long fileId,evalType;
    @Wire
    private Textbox txtValidate,tbResonRequest,tbNote;
    private Gson gson = new Gson();
    private List<EvaluationRecord> lstEvaluationRecord ;
    private Textbox txtNote = (Textbox)Path.getComponent("/windowProcessing/txtNote");
    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        evalType = Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        txtValidate.setValue("0");
    }

    @SuppressWarnings("unchecked")
	void fillHoSo(){
        Gson gson = new Gson();
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId, evalType);
        if (lstEvaluationRecord.size() > 0) {
            obj = lstEvaluationRecord.get(0);
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            lbNote.setValue(evModel.getResultEvaluationStr());
            UserDAOHE uDAO = new UserDAOHE();
            Users u = uDAO.findById(evModel.getUserId());
             lblNote.setValue("Ghi chú của "+evModel.getUserName()+" - "+u.getFullName()+" :");
            tbResonRequest.setValue(obj.getMainContent());
        }
    }
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Xem xet ho so thiet bi y te:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }


    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }
    
    public String convertJsonFromContenttoString(String jsonFormContent){
    	String status = "";
    	if(!"".equals(jsonFormContent) && jsonFormContent != null  ) {
	    	EvaluationModel evModel = gson.fromJson(jsonFormContent, EvaluationModel.class);
			status=evModel.getResultEvaluationStr();
    	}
		return status;
    	
    }
    private boolean isValidatedData() {

        if (tbResonRequest.getText().matches("\\s*")) {
            showWarningMessage("Nội dung yêu cầu bổ sung không thể để trống");
            tbResonRequest.focus();
            return false;
        }
        

        return true;
    }
    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        try {
            
             if (!isValidatedData()) {
                return;
            }
            //MinhNV Data Model Gson
            Gson gson = new Gson();
            String reson = tbResonRequest.getValue();
            String note = txtNote.getValue();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setResultEvaluationStr(note);
            evaluModel.setContent(reson);
//            evaluModel.setTypeProfessional(Constants.TYPE_PROFESSIONAL.TYPE_PROFESSIONAL_MAIN);
            evaluModel.setResultEvaluation(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
            String jsonEvaluation = gson.toJson(evaluModel);
            //MinhNV -- Save Data
            EvaluationRecord obj = new EvaluationRecord();
            obj.setMainContent(reson);
            obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
          //MinhNV add evalType SĐBS
            obj.setEvalType(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);// MinhNV - type SDBS chuyen vien chinh
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);          

            txtValidate.setValue("1");


        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }


}
