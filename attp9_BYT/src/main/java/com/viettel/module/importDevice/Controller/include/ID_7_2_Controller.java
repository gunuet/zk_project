package com.viettel.module.importDevice.Controller.include;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importDevice.DAO.ExportDeviceDAO;
import com.viettel.module.importDevice.Model.ExportDeviceModel;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.core.workflow.BO.Process;

/**
 *
 * @author Linhdx
 */
public class ID_7_2_Controller extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbNote, lblNote;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate, txtMessage;
    @Wire
    private Listbox finalFileListbox;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    private Permit permit;

    private List listBook;
    private Long docType, evalType;
    private String bCode;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private String fileSignOut = "";
    private Textbox txtNote = (Textbox) Path
            .getComponent("/windowProcessing/txtNote");

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_ADD_REQUEST;
        evalType = Constants.FILE_STATUS_CODE.STATUS_HHD;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillFinalFileListbox(fileId);
        fillHoSo();
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tham dinh ho so thiet bi y te:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_SDBS_DISPATH);
        if (lstAttach.size() <= 0) {
            return false;
        }
        if (tbResonRequest.getText().matches("\\s*")) {
            showWarningMessage("Nội dung yêu cầu bổ sung không thể để trống");
            tbResonRequest.focus();
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    void fillHoSo() {
        Gson gson = new Gson();
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId, evalType);
        if (lstEvaluationRecord != null && !lstEvaluationRecord.isEmpty()) {
            obj = lstEvaluationRecord.get(0);
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(),
                    EvaluationModel.class);
            Process p = evModel.getProcess();
            if (p != null) {
                lbNote.setValue(p.getActionName());
                UserDAOHE uDAO = new UserDAOHE();
                Users u = uDAO.findById(p.getSendUserId());
                lblNote.setValue(p.getSendUser() + " - " + u.getFullName() + " :");
            } else {
                String result = evModel.getResultEvaluationStr();
                lbNote.setValue(result);
                Long userId = evModel.getUserId();
                UserDAOHE uDAO = new UserDAOHE();
                Users u = uDAO.findById(userId);
                lblNote.setValue(u.getFullName() + " :");
            }

            tbResonRequest.setValue(obj.getMainContent());
        }
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                showNotification(String.format("Hãy thực hiện ký số",
                        Constants.DOCUMENT_TYPE_NAME.FILE),
                        Constants.Notification.ERROR);
                return;
            }
            // onApproveFile();
            // onApproveFileSign();
            // MinhNV - Add to EvaluationRecord
            Gson gson = new Gson();
            String reson = tbResonRequest.getValue();
            String note = txtNote.getValue();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setResultEvaluationStr(note);
            evaluModel.setContent(reson);
            // evaluModel.setTypeProfessional(Constants.TYPE_PROFESSIONAL.TYPE_PROFESSIONAL_MAIN);
            evaluModel
                    .setResultEvaluation(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
            String jsonEvaluation = gson.toJson(evaluModel);
            // MinhNV -- Save Data
            EvaluationRecord obj = new EvaluationRecord();
            obj.setMainContent(reson);
            obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            // MinhNV add evalType SĐBS
            obj.setEvalType(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);// MinhNV
            // -
            // type
            // SDBS
            // chuyen
            // vien
            // chinh
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);

            // MinhNV Add ND SDBS
            // Long evalType =
            // Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS;
            // EvaluationRecordDAO erDAO = new EvaluationRecordDAO();
            // EvaluationRecord record =
            // erDAO.findMaxByFileIdAndEvalType(fileId, evalType);
            AdditionalRequest objAR = new AdditionalRequest();
            objAR.setContent(reson);
            objAR.setCreatorId(getUserId());
            objAR.setCreatorName(getUserFullName());
            objAR.setFileId(fileId);
            objAR.setCreateDate(new Date());
            objAR.setIsActive(Constants.Status.ACTIVE);
            AdditionalRequestDAO objDAOHE = new AdditionalRequestDAO();
            objDAOHE.saveOrUpdate(objAR);

            updateAttachFileSigned();
            sendMS();
            txtValidate.setValue("1");
            showNotification(String.format(Constants.Notification.SAVE_SUCCESS,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.INFO);

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    public void updateAttachFileSigned() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_SDBS_DISPATH);
        if (lstAttach != null) {
            for (Attachs att : lstAttach) {
                //Attachs att_new = att;
                if (att.getIsSent() == null || att.getIsSent() == 0) {
                    att.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att);
                }
            }
        }

    }

    /**
     *
     * @param event
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) {
        String data = event.getData().toString();

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
        // String inputFileName = "_" + (new Date()).getTime() + ".pdf";//NOT
        // ƯSE BINHNT53 230315
        String outputFileName = "_signed_PheDuyetYeuCauSuaDoiBoSung_"
                + (new Date()).getTime() + ".pdf";
        // String inputFile = filePath + inputFileName;//NOT ƯSE BINHNT53 230315
        String outputFile = filePath + outputFileName;
        fileSignOut = outputFile;
        String signature = data;
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");

        try {
            pdfSig.insertSignature(signature, outputFile);
            try {
                onSignPermit();
                fileSignOut = "";
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
                showNotification("Ký số không thành công");
                return;
            }
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        LogUtils.addLog("Signed file: " + outputFile);
        // try {
        // onSignPermit();
        // fileSignOut = "";
        // } catch (Exception ex) {
        // Logger.getLogger(CosEvaluationSignPermitController.class.getName()).log(Level.SEVERE,
        // null, ex);
        // }
    }

    /**
     *
     * @throws Exception
     */
    public void onSignPermit() throws Exception {
        ExportDeviceDAO edD = new ExportDeviceDAO();
        ExportDeviceModel edm = new ExportDeviceModel(fileId);
        edm.setSendNo(permit.getReceiveNo());
        edm.setSignedDate(permit.getSignDate());
        edm.setCosmeticPermitId(fileId);
        edD.updateAttachSignFileSDBS(edm, fileSignOut);
		// lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi
        // luu vao attach)

        // CosmeticDAO cosmeticDAO = new CosmeticDAO();
        // VFileCosfile vFileCosfile2 = cosmeticDAO.findViewByFileId(fileId);
        // String pathTemplate = getPathTemplate(fileId);
        // CosExportModel model = setModelObject(cospermit, vFileCosfile2,
        // pathTemplate);
        //
        // CosExportFileDAO exportFileDAO = new CosExportFileDAO();
        //
        // exportFileDAO.exportPermit_Cos(model);
        //
        // FilesModel fileModel = new FilesModel(fileId);
        //
        // fileModel.setSendNo(cospermit.getReceiveNo());
        // fileModel.setSignDate(cospermit.getSignDate());
        // fileModel.setCosmeticPermitId(cospermit.getPermitId());
        // ExportFileDAO exp = new ExportFileDAO();
        // exp.exportCosmeticAnnouncement(fileModel, false);
        // showNotification(String.format(
        // Constants.Notification.UPDATE_SUCCESS,
        // Constants.DOCUMENT_TYPE_NAME.FILE),
        // Constants.Notification.INFO);
        showNotification("Ký số thành công!", Constants.Notification.INFO);
        fillFinalFileListbox(fileId);
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        actionSignCA(event, actionPrepareSign());
    }

    public String actionPrepareSign() {
        String fileToSign = "";
        try {
            fileToSign = onApproveFileSign();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    /**
     * Phe duyet ho so, kem CKS
     *
     * @return @throws Exception
     */
    public String onApproveFileSign() throws Exception {
        // CosEvaluationRecordDAO objDAOHE = new CosEvaluationRecordDAO();
        // obj = createApproveObject();
        // objDAOHE.saveOrUpdate(obj);
        // onSignPermit();
        clearWarningMessage();
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);

        if (lstPermit.size() > 0) {// Da co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
        }
        permitDAO.saveOrUpdate(permit);
        Long bookNumber = putInBook(permit);// Vao so
        String receiveNo = getPermitNo(bookNumber);
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        ExportDeviceModel exportDeviceModel = new ExportDeviceModel(fileId);
        exportDeviceModel.setSendNo(permit.getReceiveNo());
        exportDeviceModel.setSignedDate(permit.getSignDate());
        exportDeviceModel.setCosmeticPermitId(permit.getPermitId());
        exportDeviceModel.setContent(tbResonRequest.getValue());

        AdditionalRequest ar = new AdditionalRequest();

        ExportDeviceDAO exp = new ExportDeviceDAO();
        return exp.strExportIdfAdditionalRequest(exportDeviceModel,
                getUserId(), fileId, ar, false);

        // base.showNotify("Phê duyệt hồ sơ thành công!");
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Permit Permit) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(Permit.getPermitId(),
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    private String getPermitNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
        // with 4 digits
        //permitNo += "/" + year;
        permitNo += "-TB/BYT-TB-CT";
        return permitNo;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        SignPdfFile pdfSig = new SignPdfFile();// pdfSig = new
        // SignPdfFile();
        String base64Hash;
        String certSerial = x509Cert.getSerialNumber().toString(16);

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");
        FileUtil.mkdirs(filePath);
        String outputFileFinalName = "_" + (new Date()).getTime() + ".pdf";
        String outPutFileFinal = filePath + outputFileFinalName;
        // String linkImage = rb.getString("signImage");
        // String linkImageSign = linkImage + "232.png";
        // String linkImageStamp = linkImage + "attpStamp.png";
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");// not
            // use
            // binhnt53
            // u230315
            String linkImageSign = folderPath + separator
                    + caur.get(0).getSignature();
            String linkImageStamp = folderPath + separator
                    + caur.get(0).getStamper();
            // fileSignOut = outPutFileFinal;
            Pdf pdfProcess = new Pdf();
            try {// chen chu ki
                pdfProcess.insertImageAll(fileToSign, outPutFileFinal,
                        linkImageSign, linkImageStamp, null);
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
            if (pdfProcess.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }

            base64Hash = pdfSig.createHash(outPutFileFinal,
                    new Certificate[]{x509Cert});

            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64HASH.setValue(base64Hash);
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachNew = new ArrayList<Attachs>();
        List<Attachs> lstAttachOld = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_SDBS_DISPATH);
        if (lstAttachOld.size() > 0) {
            lstAttachNew.add(lstAttachOld.get(0));
        }
        Attachs bbhd = rDaoHe.getByObjectIdAndAttachCat(fileId, Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE);
        if(bbhd != null){
            lstAttachNew.add(bbhd);
        }        
        this.finalFileListbox.setModel(new ListModelArray(lstAttachNew));
    }

    private Permit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;

    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_ID.SendMs_42);
        md.setPhase(1l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }
}
