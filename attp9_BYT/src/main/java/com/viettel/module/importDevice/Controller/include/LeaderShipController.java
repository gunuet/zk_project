package com.viettel.module.importDevice.Controller.include;

import com.google.gson.Gson;
import com.opensymphony.xwork2.util.TextUtils;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.struts2.views.util.TextUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

/**
 *
 * @author vunt
 */
public class LeaderShipController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
//    lblAppraisal, lblResonRequest, lblResultCheck, lblOptionCheck;
//    @Wire
//    private Listbox lbResult;
    private Long fileId,evalTypeCVC, evalTypeCVKTC;
    @Wire
    private Textbox txtValidate;
    private Gson gson = new Gson();
    private List<EvaluationRecord> lstEvaluationRecordCVC, lstEvaluationRecordCVKTC ;
    
    @Wire
    private Label tbCVCName, tbCVCStatus, tbCVCContent, tbCVCDate, 
    tbCVKTCName, tbCVKTCtatus, tbCVKTCContent, tbCVKTCDate;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        evalTypeCVC = Constants.EVAL_TYPE.ROLE_CVC;
        evalTypeCVKTC = Constants.EVAL_TYPE.ROLE_CVKTC;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        txtValidate.setValue("0");
    }

    @SuppressWarnings("unchecked")
	void fillHoSo(){
    	lstEvaluationRecordCVC = new ArrayList<EvaluationRecord>();
    	lstEvaluationRecordCVKTC = new ArrayList<EvaluationRecord>();
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecordCVC = objDAO.getListByFileIdAndEvalType(fileId,evalTypeCVC);
        lstEvaluationRecordCVKTC = objDAO.getListByFileIdAndEvalType(fileId,evalTypeCVKTC);
        
        if(lstEvaluationRecordCVC.size() > 0){
        	tbCVCName.setValue(lstEvaluationRecordCVC.get(0).getCreatorName());
        	tbCVCStatus.setValue(convertJsonFromContenttoString(lstEvaluationRecordCVC.get(0).getFormContent()));
        	tbCVCContent.setValue(lstEvaluationRecordCVC.get(0).getMainContent());
        	tbCVCDate.setValue(convertDateTimeToString(lstEvaluationRecordCVC.get(0).getCreateDate()));
        }
        
        if(lstEvaluationRecordCVKTC.size() > 0){
        	tbCVKTCName.setValue(lstEvaluationRecordCVKTC.get(0).getCreatorName());
        	tbCVKTCtatus.setValue(convertJsonFromContenttoString(lstEvaluationRecordCVKTC.get(0).getFormContent()));
        	tbCVKTCContent.setValue(lstEvaluationRecordCVKTC.get(0).getMainContent());
        	tbCVKTCDate.setValue(convertDateTimeToString(lstEvaluationRecordCVKTC.get(0).getCreateDate()));
        }
        
//        ListModelArray lstModelResult = new ListModelArray(lstEvaluationRecordCVC);
//        lbResult.setModel(lstModelResult);
    }
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Xem xet ho so thiet bi y te:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }


    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }
    
    public String convertJsonFromContenttoString(String jsonFormContent){
    	String status = "";
    	if(!"".equals(jsonFormContent) && jsonFormContent != null) {
	    	EvaluationModel evModel = gson.fromJson(jsonFormContent, EvaluationModel.class);
		status=evModel.getResultEvaluationStr();
    	}
		return status;
    	
    }
    
    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        try {
            txtValidate.setValue("1");


        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }


}
