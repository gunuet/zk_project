package com.viettel.module.importDevice.Controller.include;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Controller.CosmeticAttachDAO;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.RapidTest.VImportFileRtAttach;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;

/**
 *
 * @author vunt
 */
public class LeaderShipRegisterLicensesController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Label lblResonRequest, lblAttachName, lbDownloadFile;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Vlayout flist;
    private Files files;
    private Long attachId = 0L;
    private String attachName = "";

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        CosmeticAttachDAO rDaoHe = new CosmeticAttachDAO();
        List<Attachs> lstImportAttach = rDaoHe.findReportAttach(fileId);
        for (Attachs attachs : lstImportAttach) {
        	attachId = attachs.getAttachId();
        	attachName = attachs.getAttachName();
		}
        
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        txtValidate.setValue("0");
    }
    
    @SuppressWarnings("unchecked")
   	void fillHoSo(){
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListFileId(fileId);
        if (lstEvaluationRecord.size() > 0) {
            obj = lstEvaluationRecord.get(0);
            lblResonRequest.setValue(obj.getMainContent());
        }
        
        lblAttachName.setValue(attachName);
      }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Chu tich hoi dong ky so:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }
    
    @Listen("onDownloadFile = #lbDownloadFile")
    public void onDownloadFile() throws FileNotFoundException {
    	AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }


    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            txtValidate.setValue("1");


        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

}
