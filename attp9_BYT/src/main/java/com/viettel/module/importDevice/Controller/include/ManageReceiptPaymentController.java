package com.viettel.module.importDevice.Controller.include;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.ReceiptPayment;
import com.viettel.module.importDevice.DAO.ReceiptPaymentDAO;
import com.viettel.utils.Constants;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class ManageReceiptPaymentController extends BaseComposer {

    @Wire
    Datebox dbdateInternal;
    @Wire
    Listbox lbListInternalProcess;
    @Wire
    Textbox dbCompanyName, tbTaxCode, dbReceiptPaymentNumber, tbCreateBy;
    @Wire
    Paging pagingBottomInternalProcess;
    @Wire
    protected Window windowReceiptPayment;
    Long billId;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        billId = (Long) arguments.get("billId");
        fillDataToList();
    }

    private void fillDataToList() {
        int take = pagingBottomInternalProcess.getPageSize();
        int start = pagingBottomInternalProcess.getActivePage() * pagingBottomInternalProcess.getPageSize();
        ReceiptPaymentDAO idDAO = new ReceiptPaymentDAO();
        PagingListModel lstReceiptPayment = idDAO.getReceiptPaymentList(start, take, dbCompanyName.getValue(), tbTaxCode.getValue(), dbReceiptPaymentNumber.getValue(), tbCreateBy.getValue(), billId);
        if (lstReceiptPayment != null) {
            pagingBottomInternalProcess.setTotalSize(lstReceiptPayment.getCount());
            if (lstReceiptPayment.getCount() == 0) {
                pagingBottomInternalProcess.setVisible(false);
            } else {
                pagingBottomInternalProcess.setVisible(true);
            }
            ListModelArray lstRlst = new ListModelArray(lstReceiptPayment.getLstReturn());
            lbListInternalProcess.setModel(lstRlst);
        }
    }

    @Listen("onPaging = #pagingBottomInternalProcess")
    public void onPagingList(Event event) {
        fillDataToList();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Listen("onClick=#btnSearchPayment")
    public void onSearch() throws IOException {
        fillDataToList();
    }

    @Listen("onClick = #btnCreatePayment")
    public void onOpenCreatePayment(Event event) {
        Map args = new ConcurrentHashMap();
        args.put("billId", billId);
        args.put("receiptPaymentId", 0L);
        args.put("parentWindow", windowReceiptPayment);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/include/payment/receiptPayment_create.zul", null, args);
        window.doModal();
    }

    @Listen("onEdit = #lbListInternalProcess")
    public void onEdit(Event event) throws Exception {
        Map args = new ConcurrentHashMap();
        args.put("billId", billId);
        Long receiptPaymentId = Long.parseLong(event.getData().toString());
        args.put("receiptPaymentId", receiptPaymentId);
        args.put("mode", "Edit");
        args.put("parentWindow", windowReceiptPayment);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/include/payment/receiptPayment_create.zul", null, args);
        window.doModal();
    }
    
    @Listen("onViewDetail = #lbListInternalProcess")
    public void onViewDetail(Event event) throws Exception {
        Map args = new ConcurrentHashMap();
        args.put("billId", billId);
        Long receiptPaymentId = Long.parseLong(event.getData().toString());
        args.put("receiptPaymentId", receiptPaymentId);
        args.put("mode", "View");
        args.put("parentWindow", windowReceiptPayment);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/include/payment/receiptPayment_create.zul", null, args);
        window.doModal();
    }

    @Listen("onDelete = #lbListInternalProcess")
    public void onDelete(Event event) throws Exception {
        final Long receiptPaymentId = Long.parseLong(event.getData().toString());
        EventListener eventListener = new EventListener() {
            @Override
            public void onEvent(Event evt) throws InterruptedException {
                if (Messagebox.ON_OK.equals(evt.getName())) {
                    ReceiptPaymentDAO idDAO = new ReceiptPaymentDAO();
                    ReceiptPayment inter = idDAO.findReceiptPaymentById(receiptPaymentId);
                    inter.setIsActive(0L);
                    idDAO.saveOrUpdate(inter);
                    fillDataToList();
                }
            }
        };
        Messagebox.show("Bạn có đồng ý xoá bản ghi này?", "Thông báo",
                Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                eventListener);

    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    @Listen("onRefreshList = #windowReceiptPayment")
    public void onRefreshList(Event event) {
        showNotification("Thao tác thành công!", Constants.Notification.INFO);
        fillDataToList();
    }
}
