/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller.include;

import com.viettel.core.workflow.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Include;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.HolidaysManagementDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.DAO.NodeToNodeDAOHE;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.importDevice.BO.VIdfPaymentinfo;
import com.viettel.module.importDevice.DAO.VIdfPaymentinfoDAO;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Objects;
import org.zkoss.zk.ui.ComponentNotFoundException;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;

/**
 *
 * @author duv
 */
public class ProcessingMutiPaymentController extends BaseComposer {
    
    private static final long serialVersionUID = 1L;
    public static final String TREE_USER_SELECTOR_PAGE = "/Pages/core/workflow/treeUserSelector.zul";
    @Wire
    private Vlayout includeWindow;
    @Wire
    private Textbox txtNote;
    @Wire
    private Button btnOK;
    @Wire
    private Window windowProcessing;
    @Wire
    protected Include incSelectObjectsToSendProcess;
    //Cuongvv
    @Wire
    private Listbox lbcvxlc;
    @Wire("#incUserSelector #wdListObjectsToSend #lbNodeDeptUser")
    protected Listbox lbNodeDeptUser;
    @Wire("#incUserSelector #wdListObjectsToSend #lbNDU")
    protected Textbox lbNDU;
    @Wire("#incUserSelector #wdListObjectsToSend #cbListNDU")
    protected Caption cbListNDU;
    @Wire("#incUserSelector #wdListObjectsToSend #grbListNDU")
    protected Groupbox grbListNDU;
    @Wire("#incUserSelector #wdListObjectsToSend #lhDelete")
    protected Listheader lhDelete;
    @Wire("#incUserSelector #wdListObjectsToSend")
    protected Window wdListObjectsToSend;
    protected Window windowParent;
    protected Process process;
    protected Long docType;
    protected Long actionId;
    protected String actionName;
    protected Long actionType;
    protected Long nextId;
    protected int typeProcess = 0;
    protected List<NodeToNode> listNodeToNode;
    protected List<NodeDeptUser> listAvailableNDU = new ArrayList();
    protected List<Long> listUserToSend;
    protected List<Long> listDeptToSend;
    private List<Process> listProcessCurrent;
    private List<Long> listDocId, listDocType;
    private List<NodeToNode> lstNextAction;
    private List<VFileImportDevice> lstDevice;
    @Wire
    Listbox lbAction, lstPaymentPay;
    Map<String, Object> arguments;
    Map<String, Object> argumentsListLDU;
    // linhdx thong diep trao doi
    private MessageModel message;
    private final Boolean curentDept = false;
    private Files files;
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
        loadDataAfterCompose();
        loadForm();        
    }
    
    private void loadForm() {
        listAvailableNDU = new ArrayList<>();
        listAvailableNDU.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(
                nextId, curentDept));
        lbNodeDeptUser.setModel(new ListModelList(listAvailableNDU));
        argumentsListLDU.put("listNDU", listAvailableNDU);
    }
    
    public void loadDataAfterCompose() throws Exception {
        Map<String, Object> arguments = (Map<String, Object>) Executions.getCurrent().getArg();
        if (arguments.get("typeProcess") != null) {
            typeProcess = (int) arguments.get("typeProcess");
        }
        
        windowParent = (Window) arguments.get("parentWindow");
        actionId = (Long) arguments.get("actionId");
        actionName = (String) arguments.get("actionName");
        actionType = (Long) arguments.get("actionType");
        
        nextId = (Long) arguments.get("nextId");
        process = (Process) arguments.get("process");
        docType = (Long) arguments.get("docType");
        lstDevice = (List<VFileImportDevice>) arguments.get("lstDevice");
        argumentsListLDU = (Map<String, Object>) Executions.getCurrent().getArg();
        lstNextAction = (List<NodeToNode>) arguments.get("lstNextAction");
        if (lstNextAction != null && lstNextAction.size() > 1) {
            NodeToNode a = new NodeToNode();
            a.setId(Constants.COMBOBOX_HEADER_VALUE);
            a.setAction(Constants.COMBOBOX_HEADER_PROCESS_SELECT);
            lstNextAction.add(0, a);
        }
        
        ListModelArray lstModelNextAction = new ListModelArray(lstNextAction);
        lbAction.clearSelection();
        lbAction.setModel(lstModelNextAction);
        lstPaymentPay.setModel(new ListModelArray(lstDevice));
        lbAction.renderAll();
        
        lbAction.setSelectedIndex(0);
        lbAction.renderAll();
        btnOK.setLabel(actionName);
        listUserToSend = new ArrayList<>();
        listDeptToSend = new ArrayList<>();
        
    }
    
    public void onDeleteListitem(int index) {
        
        listAvailableNDU.remove(index);
        lbNodeDeptUser.setModel(new ListModelList(listAvailableNDU));
        argumentsListLDU.put("listNDU", listAvailableNDU);
    }    
    
    @Listen("onDelete = #lstPaymentPay")
    public void onDeletePaymentDoc(Event event) {
        Long id = Long.parseLong(event.getData().toString());
        for (VFileImportDevice fi : lstDevice) {
            if (fi.getImportDeviceFileId().equals(id)) {
                lstDevice.remove(fi);
                break;
            }
        }
        lstPaymentPay.setModel(new ListModelArray(lstDevice));
    }
    
    public void processSingle(Long docId) throws UnsupportedEncodingException {
        List<NodeDeptUser> lstChoosenUser = getListChoosedNDU(docId);
        NodeToNodeDAOHE ntnDAOHE = new NodeToNodeDAOHE();
        NodeToNode action = ntnDAOHE.getActionById(actionId);
        Long processParentId = null;
        if (process != null) {
            processParentId = process.getProcessId();
        }
        String note = txtNote.getText();
        Boolean re = false;
        // Thêm re tránh gửi nhiều lần sang NSW
        // Thêm check fix luồng sau khi phê duyệt đơn đăng ký
        Long check = 3L;

        //linhdx 20160320
        Boolean checkIsXNDYCNK = false;
        WorkflowAPI w = new WorkflowAPI();
        if (docType.equals(w.getProcedureTypeByCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT).getCategoryId())) {
            checkIsXNDYCNK = true;
            if ("Phê duyệt đơn đăng ký".equals(action.getAction())) {
                check = checkActionPDDDK(docId);
            }
        }
        Boolean fn = false;
        try {
            Textbox txtFinishDate;
            Window businessWindow = (Window) includeWindow.getFellow("businessWindow");
            txtFinishDate = (Textbox) businessWindow.getFellow("txtFinishDate");
            if (txtFinishDate != null && "1".equals(txtFinishDate.getValue())) {
                fn = true;
                FilesDAOHE fDAO = new FilesDAOHE();
                Files f = fDAO.findById(docId);
                f.setFinishDate(new Date());
                fDAO.saveOrUpdate(f);
                
            }
        } catch (ComponentNotFoundException | WrongValueException ex) {
            LogUtils.addLogNoImportant(ex);
        }
        Boolean l = WorkflowAPI.getInstance().
                sendDocToListNDUs(docId, docType, action, note, processParentId, lstChoosenUser, message, re, check != 1L, fn);
        if (!l) {
            showNotification("Lỗi kết nối hệ thống NSW");
            return;
        }
        if (check == 2L || (check == 3L && !fn)) {
            re = true;
        }
        Boolean m;
        
        if (checkIsXNDYCNK) {
            m = WorkflowAPI.getInstance().
                    sendTransparentXNDYCNK(docId, docType, action, note, processParentId, lstChoosenUser, message, re, check != 2L, fn);
        } else {
            m = WorkflowAPI.getInstance().
                    sendTransparent(docId, docType, action, note, processParentId, lstChoosenUser, message, re, check != 2L, fn);
            
        }
        
        if (!m) {
            showNotification("Lỗi kết nối hệ thống NSW");
            return;
        }
        windowProcessing.onClose();
        showNotification(String.format(Constants.Notification.SENDED_SUCCESS, "hồ sơ"), Constants.Notification.INFO);
        // windowParent.invalidate();
    }
    
    private Long checkActionPDDDK(Long fileId) {
        ImportOrderProductDAO impDAO = new ImportOrderProductDAO();
        List<ImportOrderProduct> lsImP = impDAO.findAllIdByFileId(fileId);
        if (lsImP != null && !lsImP.isEmpty()) {
            for (ImportOrderProduct p : lsImP) {
                if ("CHAT".equals(p.getCheckMethodCode()) || "THUONG".equals(p.getCheckMethodCode())) {
                    return 2L;
                }
            }
        }
        return 1L;
    }
    
    public void processMulti(Long docId) throws UnsupportedEncodingException {
        List<NodeDeptUser> lstChoosenUser = getListChoosedNDU(docId);
        NodeToNode action = (new NodeToNodeDAOHE()).getActionById(actionId);
        Long processParentId = null;
        int sizeProcess = listProcessCurrent.size();
        Boolean fn = false;
        try {
            Textbox txtFinishDate;
            
            Window businessWindow = (Window) includeWindow.getFellow("businessWindow");
            
            txtFinishDate = (Textbox) businessWindow.getFellow("txtFinishDate");
            if (txtFinishDate != null && "1".equals(txtFinishDate.getValue())) {
                fn = true;
                FilesDAOHE fDAO = new FilesDAOHE();
                Files f = fDAO.findById(docId);
                f.setFinishDate(new Date());
                fDAO.saveOrUpdate(f);
            }
        } catch (ComponentNotFoundException | WrongValueException ex) {
            LogUtils.addLogNoImportant(ex);
        }
        Process tempProcess;
        for (int i = 0; i < sizeProcess; i++) {
            tempProcess = listProcessCurrent.get(i);
            if (tempProcess != null) {
                processParentId = tempProcess.getProcessId();
            }
            String note = txtNote.getText();
            Boolean re = false;
            Boolean k = WorkflowAPI.getInstance().sendDocToListNDUs(listDocId.get(i),
                    listDocType.get(i), action, note, processParentId,
                    lstChoosenUser, message, re, !fn, fn);
            if (!k) {
                showNotification("Lỗi kết nối hệ thống NSW");
                return;
            }
        }
        windowProcessing.onClose();
    }        
    
    @Listen("onClose=#windowProcessing")
    public void onClickClose() {
        refresh();
    }
    
    @Listen("onClick=#btnOK")
    public void onClickOK() {
        if ((Long) lbAction.getSelectedItem().getValue() == -1L) {
            showNotification("Phải chọn thao tác xử lý");
            return;
        }
        
        VIdfPaymentinfoDAO objDAO = new VIdfPaymentinfoDAO();
        PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
        for (VFileImportDevice fi : lstDevice) {
            Long fileId = fi.getFileId();
            List<VIdfPaymentinfo> listPaymented = objDAO.getListFileId(fileId);
            for (VIdfPaymentinfo app : listPaymented) {
                PaymentInfo paymentInfo;
                paymentInfo = objFreePaymentInfo.findById(app.getPaymentInfoId());
                paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED); // trang thai moi tao
                paymentInfo.setDateConfirm(new Date());
                paymentInfo.setPaymentActionUser(getUserFullName());
                paymentInfo.setPaymentActionUserId(getUserId());
                paymentInfo.setNote("");
                objFreePaymentInfo.saveOrUpdate(paymentInfo);
            }
            BillDAO objBillDAOHE = new BillDAO();
            Bill objBill = objBillDAOHE.findById(listPaymented.get(0).getBillId());
            objBill.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED);
            objBill.setConfirmDate(new Date());
            objBill.setConfirmUserId(getUserId());
            objBill.setConfirmUserName(getUserFullName());
            
            objBillDAOHE.saveOrUpdate(objBill);
            
            FilesDAOHE filesDAOHE = new FilesDAOHE();
            Files fileLocal = filesDAOHE.findById(fi.getFileId());
            fileLocal.setStartDate(new Date());
            fileLocal.setNumDayProcess(Constants.WARNING.NUM_DAY_PROCESS);
            HolidaysManagementDAOHE holidaysManagementDAOHE = new HolidaysManagementDAOHE();
            Date deadline = holidaysManagementDAOHE.getDeadline(fileLocal.getStartDate(), fileLocal.getNumDayProcess());
            fileLocal.setDeadline(deadline);
            filesDAOHE.saveOrUpdate(fileLocal);
            try {
                processSingle(fileId);
                refresh();
                closeView();
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
        }        
    }
    
    private void refresh() {
        if (windowParent != null) {
            Events.sendEvent("onRefresh", windowParent, null);
            
        }
    }
    
    private void closeView() {
        //linhdx 20160301 dong cua so view
        try {
            Window windowview = (Window) Path.getComponent("/windowView");
            if (windowview != null) {
                Events.sendEvent("onClose", windowview, null);
            }
        } catch (Exception ex) {
            LogUtils.addLog(ex);
        }
    }
    
    public List<NodeDeptUser> getListChoosedNDU(Long docId) {
        /**
         * Cuongvv
         */
        List list = new ArrayList<>();
        Long statusTPPC = 31000002L;
        Long statusTPPC2 = 31000038L;
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        Files file = filesDAOHE.findById(docId);
        if ((file.getStatus().equals(statusTPPC) || file.getStatus().equals(statusTPPC2)) && getDeptId() == 3411) {
            UserDAOHE uDAO = new UserDAOHE();
            UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                    "userToken");
            List<Users> lsu = uDAO.getUserByDeptPosID(tk.getDeptId(), 23L);
            String username = "";
            if (lbcvxlc.getSelectedItem() != null) {
                for (Users lsu1 : lsu) {
                    if (Objects.equals(lsu1.getUserId(), lbcvxlc.getSelectedItem().getValue())) {
                        username = lsu1.getUserName();
                    }
                }
                NodeDeptUser deptUser = new NodeDeptUser(Long.parseLong(lbcvxlc.getSelectedItem().getValue().toString()), username);
                
                Users us = uDAO.getUserById(deptUser.getUserId());
                deptUser.setUserName(us.getFullName());
                list.add(deptUser);
                
            } else {
                showNotification("Bạn chưa chọn người xử lý tiếp theo!");
            }
        }
        
        if (lbNodeDeptUser != null) {
            for (Listitem item : lbNodeDeptUser.getItems()) {
                if (!item.isDisabled()) {
                    list.add((NodeDeptUser) item.getValue());
                }
            }
        }
        return list;
    }
    
    @Listen("onSelect = #lbAction")
    public void selectAction() {
        Listitem item = lbAction.getSelectedItem();
        if ((Long) lbAction.getSelectedItem().getValue() == -1L) {
            showNotification("Phải chọn thao tác xử lý");
            btnOK.setLabel("Xử lý hồ sơ");
            lbAction.setFocus(true);
            return;
        }
        actionId = (Long) item.getValue();
        NodeToNode action = (new NodeToNodeDAOHE()).findById(actionId);
        actionName = (String) action.getAction();
        actionType = action.getType();
        nextId = action.getNextId();
        btnOK.setLabel(actionName);
        listUserToSend = new ArrayList<>();
        listDeptToSend = new ArrayList<>();
        
        loadForm();
        
    }
}
