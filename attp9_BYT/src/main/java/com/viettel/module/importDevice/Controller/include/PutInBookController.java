package com.viettel.module.importDevice.Controller.include;

import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;

import static com.viettel.module.payment.parent.PaymentController.numberToString;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.ws.BO.Document;
import com.viettel.ws.BO.Response;
import com.viettel.ws.Helper_VofficeMoh;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

/**
 *
 * @author
 */
public class PutInBookController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final int BOOK_LISTBOXMODEL = 1;
    private final int STATUS_LISTBOXMODEL = 2;
    @Wire
    private Listbox lbBookIn;
    private List listBookIn;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbMahoso, lbTensp, lbSumMoney, lbtextMoney, lbBookNumber;
//    @Wire
//    private Intbox boxBookNumber;

    private Long fileId, importDeviceFileId;
    private BookDocument bookDocument;
//    @Wire
//    private Listbox lbStatusReceive;
    @Wire
    private Textbox tbStatusReceive;
    @Wire
    private Textbox tbAdditionRequest;
    @Wire
    private Textbox txtValidate;
    private String bCode;
    private Long docType;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        //bCode = (String) arguments.get("bCode");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_IN;//So tiep nhan
        docType = (Long) arguments.get("docType");

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        if (lbBookNumber != null) {
            lbBookNumber.setVisible(false);
        }
        txtValidate.setValue("0");
    }

    void fillHoSo() {
        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
        List<VCosPaymentInfo> listPaymented;
        VCosPaymentInfo vCosPaymentInfo;
        listPaymented = objDAOHE.getListFileId(fileId);
        Long lsumMoney;
        if (listPaymented.size() > 0) {
            vCosPaymentInfo = listPaymented.get(0);
            lsumMoney = vCosPaymentInfo.getCost();
            lbMahoso.setValue(vCosPaymentInfo.getNswFileCode());
            lbTensp.setValue(vCosPaymentInfo.getProductName());
            lbSumMoney.setValue(String.valueOf(lsumMoney));
            lbtextMoney.setValue(numberToString(lsumMoney));
        }
        BookDAOHE bookDAOHE = new BookDAOHE();
        //linhdx Tim so van ban chung voi Ho so them moi
        WorkflowAPI w = new WorkflowAPI();
        Long docType_New = w.getProcedureTypeIdByCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
        listBookIn = bookDAOHE.getBookByTypeAndPrefix(docType_New, bCode);
        lbBookIn.setModel(new ListModelList<>(listBookIn));
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
            onSave();
//            onSaveBookNumber();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onAfterRender = #lbBookIn")
    public void onAfterRenderListBookIn() {
        List<Listitem> listitems = lbBookIn.getItems();
        if (listitems.isEmpty()) {
            // Hien thi thong bao loi: don vi chua co so
            showWarningMessage("Đơn vị " + getDeptName()
                    + " chưa có sổ hồ sơ.");
            showNotification("Đơn vị " + getDeptName()
                    + " chưa có sổ hồ sơ.",
                    Constants.Notification.WARNING);
        } else {
            if (listBookIn != null && !listBookIn.isEmpty()) {
                lbBookIn.setSelectedIndex(0);
            }
            // lbBookIn.setSelectedIndex(selectedItemIndex);
            // Long bookId = lbBookIn.getItemAtIndex(selectedItemIndex).getValue();
            // lbBookNumber.setValue(String.valueOf(getMaxBookNumber(bookId)));
        }
    }

    public Integer getBookNumber(Long fileId, Long fileType) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bd = bookDocumentDAOHE.getBookDocumentFromDocumentId(
                fileId, fileType);
        if (bd != null) {
            return bd.getBookNumber().intValue();
        }
        return null;
    }

    public int getMaxBookNumber(Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        return bookDocumentDAOHE.getMaxBookNumber(bookId).intValue();
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbAdditionRequest != null && tbAdditionRequest.getText().matches("\\s*")) {
            showWarningMessage("Yêu cầu bổ sung không thể để trống");
            tbAdditionRequest.focus();
            return false;
        }
        if (lbBookIn.getSelectedItem() == null) {
            showWarningMessage("Chưa chọn sổ");
            lbBookIn.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {

        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        try {
            //Neu chon dong y thi luu so
            if (Objects.equals(Long.valueOf((String) tbStatusReceive.getValue()), Constants.Status.ACTIVE)) {

                ImportDeviceFiletDAO fDAO = new ImportDeviceFiletDAO();
                ImportDeviceFile f = fDAO.findByFileId(fileId);
                if (f.getBookNumber() == null) {
                    UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                            "userToken");
                    SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    Document d = new Document();
                    Helper_VofficeMoh hp = new Helper_VofficeMoh();
                    d.setFrom("Vụ trang thiết bị y tế");
                    d.setSignDate(formatterDateTime.format(new Date()));
                    d.setUserSign(tk.getUserName());
                    d.setSignName(tk.getUserFullName());
                    d.setBusinessAddress(f.getImporterAddress());
                    d.setBusinessName(f.getImporterName());
                    d.setEquipmentNo(f.getEquipmentNo());
                    d.setContent("Vụ Trang thiết bị y tế tiếp nhận hồ sơ lấy số công văn đến cho hồ sơ mã: " + f.getNswFileCode()
                            + " của doanh nghiệp: " + f.getImporterName() + ", địa chỉ: " + f.getImporterAddress() + ", điện thoại: " + f.getImporterPhone());
                    Response rs = hp.getReceiveVal(d, f, Long.parseLong(lbBookIn.getSelectedItem().getValue().toString()));
                    if (rs == null) {
                        showWarningMessage("Lỗi nhận số công văn đến từ Voffice Bộ Y tế");
                        return;
                    }
                    f.setImportDeviceFileId(f.getImportDeviceFileId());
                    f.setBookNumber(Long.parseLong(rs.getResponseNo()));
                    f.setReceiveId(Long.parseLong(rs.getResponseId()));
                    fDAO.saveOrUpdate(f);
                }
                lbBookNumber.setValue(f.getBookNumber().toString());
                BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
                bookDocument = createBookDocument();
                bookDocument.setStatus(Constants.Status.ACTIVE);
                bookDocument.setCreatorId(getUserId());
                bookDocument.setCreatorName(getUserFullName());
                bookDocument.setCreateDate(new Date());
                bookDocumentDAOHE.saveOrUpdate(bookDocument);
                updateBookCurrentNumber();
                String hopdong = ResourceBundleUtil.getString("hopdong", "config");
                if ("true".equals(hopdong)) {
                    Helper_VofficeMoh helperMoh = new Helper_VofficeMoh();
                    helperMoh.SendMs_73(f, String.valueOf(f.getBookNumber()));
                }
                txtValidate.setValue("1");

            } else if (Objects.equals(Long.valueOf((String) tbStatusReceive.getValue()), Constants.Status.INACTIVE)) {
                //Neu khong dong y thi yeu cau nhap comment va luu comment
                String comment = tbAdditionRequest.getValue().trim();
                AdditionalRequest obj = new AdditionalRequest();
                obj.setContent(comment);
                obj.setCreatorId(getUserId());
                obj.setCreatorName(getUserFullName());
                obj.setFileId(fileId);
                obj.setCreateDate(new Date());
                obj.setIsActive(Constants.Status.ACTIVE);
                AdditionalRequestDAO objDAOHE = new AdditionalRequestDAO();
                objDAOHE.saveOrUpdate(obj);
                txtValidate.setValue("1");
            }
            showNotification(String.format(
                    Constants.Notification.SAVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.INFO);

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    protected BookDocument createBookDocument() {
        // So van ban *
        Long bookId = lbBookIn.getSelectedItem().getValue();
        if (bookDocument == null) {
            bookDocument = new BookDocument();
        }
        if (bookId != -1) {
            bookDocument.setBookId(bookId);
        }
//        Books book = (Books) listBookIn.get(lbBookIn.getSelectedIndex());
        // So den *
        //bookDocument.setBookNumber(boxBookNumber.getValue().longValue());
        // bookDocument.setBookNumber(Long.valueOf(getMaxBookNumber(book.getBookId())));
        bookDocument.setBookNumber(Long.parseLong(lbBookNumber.getValue()));
        bookDocument.setDocumentId(fileId);
        return bookDocument;
    }

    public void updateBookCurrentNumber() {
        if (getMaxBookNumber(bookDocument.getBookId()) <= bookDocument.getBookNumber()) {
            BookDAOHE bookDAOHE = new BookDAOHE();
            Books book = bookDAOHE.findById(bookDocument.getBookId());
            book.setCurrentNumber(bookDocument.getBookNumber());
            bookDAOHE.saveOrUpdate(book);
        }
    }
}
