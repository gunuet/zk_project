package com.viettel.module.importDevice.Controller.include;

import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importDevice.BO.ReceiptPayment;
import com.viettel.module.importDevice.DAO.ReceiptPaymentDAO;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.WordExportUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.poi.ss.usermodel.Cell;
import org.zkoss.poi.ss.usermodel.CellStyle;
import org.zkoss.poi.ss.usermodel.Font;
import org.zkoss.poi.ss.usermodel.Row;
import org.zkoss.poi.ss.usermodel.Sheet;
import org.zkoss.poi.ss.usermodel.Workbook;
import org.zkoss.poi.ss.util.CellRangeAddress;
import org.zkoss.poi.xssf.usermodel.XSSFFont;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class ReceiptPaymentController extends BaseComposer {

    @Wire
    Button btnSaveIntenal;
    @Wire
    Textbox dbCompanyName, tbTaxCode, dbCompanyAddress, dbReceiptPaymentNumber, tbCreateBy;
    @Wire
    Label lbTopWarning;
    @Wire
    Window createInternalProcessPopup;
    @Wire
    Div divExport, divEdit;
    Long billId;
    Long receiptPaymentId;
    protected Window windowParent;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        billId = (Long) arguments.get("billId");
        receiptPaymentId = (Long) arguments.get("receiptPaymentId");
        windowParent = (Window) arguments.get("parentWindow");
        if (receiptPaymentId != null && receiptPaymentId > 0) {
            String mode = (String) arguments.get("mode");
            ReceiptPaymentDAO idDAO = new ReceiptPaymentDAO();
            ReceiptPayment inter = idDAO.findReceiptPaymentById(receiptPaymentId);
            dbCompanyName.setValue(inter.getCompanyName());
            tbTaxCode.setValue(inter.getTaxCode());
            dbCompanyAddress.setValue(inter.getCompanyAddress());
            dbReceiptPaymentNumber.setValue(inter.getReceiptPaymentNumber());
            tbCreateBy.setValue(inter.getCreateBy());
            if (mode != null && "View".equals(mode)) {
                divEdit.setVisible(false);
                divExport.setVisible(true);
                dbCompanyName.setReadonly(true);
                tbTaxCode.setReadonly(true);
                dbCompanyAddress.setReadonly(true);
                dbReceiptPaymentNumber.setReadonly(true);
                tbCreateBy.setReadonly(true);
            } else {
                btnSaveIntenal.setLabel("Cập nhật");
                divExport.setVisible(false);
                divEdit.setVisible(true);
            }
        }
    }

    @Listen("onClick = #btnSaveIntenal")
    public void onSave(Event event) {
        try {
            ReceiptPaymentDAO idDAO = new ReceiptPaymentDAO();
            if (checkValidate()) {
                ReceiptPayment inter = new ReceiptPayment();
                if (receiptPaymentId != null && receiptPaymentId > 0) {

                    inter = idDAO.findReceiptPaymentById(receiptPaymentId);
                } else {
                    inter.setIsActive(1L);
                    inter.setBillId(billId);
                }
                if (dbCompanyAddress.getValue() != null) {
                    inter.setCompanyAddress(dbCompanyAddress.getValue());
                }
                inter.setCompanyName(dbCompanyName.getValue());
                inter.setTaxCode(tbTaxCode.getValue());
                inter.setReceiptPaymentNumber(dbReceiptPaymentNumber.getValue());
                if (tbCreateBy.getValue() != null) {
                    inter.setCreateBy(tbCreateBy.getValue());
                }
                idDAO.saveOrUpdate(inter);
                closeView();
                createInternalProcessPopup.detach();

            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onClick = #btnCancelInternalProcess")
    public void onClosePopup(Event event) {
        createInternalProcessPopup.detach();
    }

    private void closeView() {
        //linhdx 20160301 dong cua so view
        try {
//            Window windowview = (Window) Path.getComponent("/" + windowParent);
            if (windowParent != null) {
                Events.sendEvent("onRefreshList", windowParent, null);
            }
        } catch (Exception ex) {
            LogUtils.addLog(ex);
        }
    }

    private boolean checkValidate() {
        if (dbCompanyName.getValue() == null || "".equals(dbCompanyName.getValue())) {
            lbTopWarning.setValue("Bạn chưa nhập mục Tên công ty!");
            return false;
        }
        if (dbCompanyAddress.getValue() == null || "".equals(dbCompanyAddress.getValue())) {
            lbTopWarning.setValue("Bạn chưa nhập mục Địa chỉ!");
            return false;
        }
        if (tbTaxCode.getValue() == null || "".equals(tbTaxCode.getValue())) {
            lbTopWarning.setValue("Bạn chưa nhập mục Mã số thuế!");
            return false;
        }
        if (dbReceiptPaymentNumber.getValue() == null || "".equals(dbReceiptPaymentNumber.getValue())) {
            lbTopWarning.setValue("Bạn chưa nhập mục Số biên lai!");
            return false;
        }
        return true;
    }

    @Listen("onClick = #btnExportDoc")
    public void onExportReceiptPaymentDoc(Event event) {
        try {
            String fileName = "Thongtinbienlai_" + (new Date()).getTime() + ".docx";
            WordprocessingMLPackage receiptPayment = prepareDataToExportReceiptPayment();
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            folderPath += separator + dateFormat.format(date);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                // tạo forlder
                fd.mkdirs();
            }
            File f = new File(folderPath + separator + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            receiptPayment.save(f);
            Filedownload.save(f, folderPath + separator + fileName);
        } catch (IOException | Docx4JException ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onClick = #btnExportPDF")
    public void onExportReceiptPaymentPDF(Event event) {
        try {
            prepareDataToExportReceiptPaymentPDF();
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private PdfDocxFile prepareDataToExportReceiptPaymentPDF() throws IOException {
        PdfDocxFile outputStreamPdf;
        try {
            String bienlaithanhtoanDoc = "/WEB-INF/template/bienlaithanhtoan.docx";
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(new FileInputStream(new File(request.getRealPath(bienlaithanhtoanDoc))));
            WordExportUtils wU = new WordExportUtils();
            ReceiptPaymentDAO idDAO = new ReceiptPaymentDAO();
            ReceiptPayment inter = idDAO.findReceiptPaymentById(receiptPaymentId);
            wU.replacePlaceholder(wmp, inter.getCompanyName(), "${companyName}");
            wU.replacePlaceholder(wmp, inter.getCompanyAddress(), "${companyAddress}");
            wU.replacePlaceholder(wmp, inter.getTaxCode(), "${taxCode}");
            wU.replacePlaceholder(wmp, inter.getReceiptPaymentNumber(), "${receiptPaymentNumber}");
            Calendar cal = Calendar.getInstance();
            String date = "ngày " + cal.get(Calendar.DAY_OF_MONTH) + " tháng " + (cal.get(Calendar.MONTH) + 1) + " năm " + cal.get(Calendar.YEAR);
            wU.replacePlaceholder(wmp, date, "${date}");
            wU.replacePlaceholder(wmp, inter.getCreateBy(), "${creatorName}");
            outputStreamPdf = wU.writePDFToStream(wmp, true);
            return outputStreamPdf;
        } catch (FileNotFoundException | Docx4JException ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    private WordprocessingMLPackage prepareDataToExportReceiptPayment() {
        try {
            String bienlaithanhtoanDoc = "/WEB-INF/template/bienlaithanhtoan.docx";
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(new FileInputStream(new File(request.getRealPath(bienlaithanhtoanDoc))));
            WordExportUtils wU = new WordExportUtils();
            ReceiptPaymentDAO idDAO = new ReceiptPaymentDAO();
            ReceiptPayment inter = idDAO.findReceiptPaymentById(receiptPaymentId);
            wU.replacePlaceholder(wmp, inter.getCompanyName(), "${companyName}");
            wU.replacePlaceholder(wmp, inter.getCompanyAddress(), "${companyAddress}");
            wU.replacePlaceholder(wmp, inter.getTaxCode(), "${taxCode}");
            wU.replacePlaceholder(wmp, inter.getReceiptPaymentNumber(), "${receiptPaymentNumber}");
            Calendar cal = Calendar.getInstance();
            String date = "ngày " + cal.get(Calendar.DAY_OF_MONTH) + " tháng " + (cal.get(Calendar.MONTH) + 1) + " năm " + cal.get(Calendar.YEAR);
            wU.replacePlaceholder(wmp, date, "${date}");
            wU.replacePlaceholder(wmp, inter.getCreateBy(), "${creatorName}");
            return wmp;
        } catch (FileNotFoundException | Docx4JException ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }


    @Listen("onClick = #btnExportExcel")
    public void onExportExcelPayment() {
        try {
            Workbook wb = new HSSFWorkbook();
            Sheet personSheet = wb.createSheet("payment");

            ReceiptPaymentDAO idDAO = new ReceiptPaymentDAO();
            ReceiptPayment inter = idDAO.findReceiptPaymentById(receiptPaymentId);

            Font fontBoldTitle;
            fontBoldTitle = wb.createFont();
            fontBoldTitle.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
            fontBoldTitle.setFontHeightInPoints((short)14);
            fontBoldTitle.setFontName("Times New Roman");
            CellStyle styleBold = wb.createCellStyle();
            styleBold.setFont((Font) fontBoldTitle);
            styleBold.setAlignment(CellStyle.ALIGN_CENTER);
            int row = 0;
            Row dataRow0 = personSheet.createRow(row);
            dataRow0.createCell(0).setCellValue("");            
            row++;
            Row dataRow1 = personSheet.createRow(row++ );
            Cell cellheader = dataRow1.createCell(0);
            cellheader.setCellValue("CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
            cellheader.setCellStyle(styleBold);
            CellRangeAddress region = CellRangeAddress.valueOf("A" + 2 + ":J" + 2);
            personSheet.addMergedRegion(region);
            
            Row dataRow2 = personSheet.createRow(row++);
            Cell cellheader2 = dataRow2.createCell(0);
            cellheader2.setCellValue("Độc lập - Tự do - Hạnh phúc");
            fontBoldTitle.setFontHeightInPoints((short)12);
            cellheader2.setCellStyle(styleBold);
            CellRangeAddress region2 = CellRangeAddress.valueOf("A" + 3 + ":J" + 3);
            personSheet.addMergedRegion(region2);
            
            Row dataRow3 = personSheet.createRow(row++);
            dataRow3.createCell(0).setCellValue("");
            
            Row dataRow4 = personSheet.createRow(row++);
            Cell cellheader4 = dataRow4.createCell(0);
            cellheader4.setCellValue("BIÊN LAI LỆ PHÍ");
            fontBoldTitle.setFontHeightInPoints((short)14);
            cellheader4.setCellStyle(styleBold);
            CellRangeAddress region4 = CellRangeAddress.valueOf("A" + 5 + ":J" + 5);
            personSheet.addMergedRegion(region4);
            
            Row dataRow5 = personSheet.createRow(row++);
            dataRow5.createCell(0).setCellValue("");
            
            Row dataRow6 = personSheet.createRow(row++);
            Cell cellheader6 = dataRow6.createCell(0);
            cellheader6.setCellValue("Công ty:");
            fontBoldTitle.setFontHeightInPoints((short)12);
            cellheader6.setCellStyle(styleBold);
            dataRow6.createCell(1).setCellValue(inter.getCompanyName());
            
            Row dataRow7 = personSheet.createRow(row++);
            Cell cellheader7 = dataRow7.createCell(0);
            cellheader7.setCellValue("Địa chỉ:");
            cellheader7.setCellStyle(styleBold);           
            dataRow7.createCell(1).setCellValue(inter.getCompanyAddress());
            
            Row dataRow8 = personSheet.createRow(row++);
            Cell cellheader8 = dataRow8.createCell(0);
            cellheader8.setCellValue("Mã số thuế:");
            cellheader8.setCellStyle(styleBold);            
            dataRow8.createCell(1).setCellValue(inter.getTaxCode());
            
            Row dataRow9 = personSheet.createRow(row++);
            Cell cellheader9 = dataRow9.createCell(0);
            cellheader9.setCellValue("Số biên lai:");
            cellheader9.setCellStyle(styleBold);             
            dataRow9.createCell(1).setCellValue(inter.getReceiptPaymentNumber());
            
            Row dataRow10 = personSheet.createRow(row++);
            dataRow10.createCell(0).setCellValue("");
            
            Row dataRow11 = personSheet.createRow(row++);
            dataRow11.createCell(0).setCellValue("");
            
            Row dataRow12 = personSheet.createRow(row++);
            dataRow12.createCell(0).setCellValue("");
            dataRow12.createCell(1).setCellValue("");
            dataRow12.createCell(2).setCellValue("");
            dataRow12.createCell(3).setCellValue("");
            dataRow12.createCell(4).setCellValue("");
            dataRow12.createCell(5).setCellValue("");
            
            Calendar cal = Calendar.getInstance();
            String date = "ngày " + cal.get(Calendar.DAY_OF_MONTH) + " tháng " + (cal.get(Calendar.MONTH) + 1) + " năm " + cal.get(Calendar.YEAR);
            dataRow12.createCell(6).setCellValue("Hà Nội," + date);
            
            Row dataRow13 = personSheet.createRow(row++);
            dataRow13.createCell(0).setCellValue("");
            dataRow13.createCell(1).setCellValue("");
            dataRow13.createCell(2).setCellValue("");
            dataRow13.createCell(3).setCellValue("");
            dataRow13.createCell(4).setCellValue("");
            dataRow13.createCell(5).setCellValue("");
            dataRow13.createCell(6).setCellValue("Người tạo biên lai");

            
            Row dataRow14 = personSheet.createRow(row++);
            dataRow14.createCell(0).setCellValue("");
            dataRow14.createCell(1).setCellValue("");
            dataRow14.createCell(2).setCellValue("");
            dataRow14.createCell(3).setCellValue("");
            dataRow14.createCell(4).setCellValue("");
            dataRow14.createCell(5).setCellValue("");
            dataRow14.createCell(6).setCellValue("(Ký ghi rõ họ tên)");
            
            Row dataRow16 = personSheet.createRow(row++);
            dataRow16.createCell(0).setCellValue("");
            
            Row dataRow15 = personSheet.createRow(row++);
            dataRow15.createCell(0).setCellValue("");
            dataRow15.createCell(1).setCellValue("");
            dataRow15.createCell(2).setCellValue("");
            dataRow15.createCell(3).setCellValue("");
            dataRow15.createCell(4).setCellValue("");
            dataRow15.createCell(5).setCellValue("");
            dataRow15.createCell(6).setCellValue(inter.getCreateBy());
            
            String fileName = "Thongtinbienlai_" + (new Date()).getTime() + ".xls";
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            folderPath += separator + dateFormat.format(date);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                // tạo forlder
                fd.mkdirs();
            }
            File f = new File(folderPath + separator + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            try (FileOutputStream fileOut = new FileOutputStream(folderPath + separator + fileName)) {
                wb.write(fileOut);
                fileOut.close();
            }
            Filedownload.save(f, folderPath + separator + fileName);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }
}
