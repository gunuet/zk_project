/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.module.importDevice.BO.AppliesFee;
import com.viettel.module.importDevice.BO.IdMeeting;
import com.viettel.module.importDevice.DAO.AppliesFeeDAO;
import com.viettel.module.importDevice.DAO.IdMeetingDAO;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.model.AttachCategoryModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author hieuld5
 */
public class listIdMeetingController extends BaseComposer {

    @Wire
    Datebox dbDay;
    @Wire
    Textbox tbPlace;
    @Wire
    Listbox fileListbox;
    @Wire
    Paging userPagingBottom;
    IdMeeting searchForm;
    @Wire
    Window idMettingAll;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        searchForm = new IdMeeting();
        searchForm.setDay(dbDay.getValue());
        searchForm.setPlace(tbPlace.getValue());
        onSearch(0);
    }

    private void onSearch(int page) {
        userPagingBottom.setActivePage(page);
        IdMeetingDAO idDAO = new IdMeetingDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = idDAO.searchIdMeeting(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstModel.setMultiple(true);
        fileListbox.setModel(lstModel);
        fileListbox.renderAll();
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        IdMeeting id = (IdMeeting) event.getData();
        AttachDAOHE attDAOHE = new AttachDAOHE();

        Attachs att = attDAOHE.getByObjectIdAndAttachCat(id.getId(), Constants.OBJECT_TYPE.ID_MEETING_ATT);
        if (att == null) {
            showNotification("Không có biên bản họp !!!");
            return;
        }
        String path = att.getFullPathFile();
        File f = new File(path);
        if (f.exists()) {
            Filedownload.save(f, att.getAttachName());
        } else {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
        }

    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        onSearch(userPagingBottom.getActivePage());
    }

    @Listen("onReload=#publicFileManage")
    public void onReload() {
        onSearch(userPagingBottom.getActivePage());
    }

    @Listen("onClick=#btnSearch")
    public void search() {
        searchForm.setDay(dbDay.getValue());
        searchForm.setPlace(tbPlace.getValue());
        onSearch(0);
    }

    @Listen("onClick = #btnApproval")
    public void approvalDoc(Event event){
        if(fileListbox.getSelectedItems() != null && !fileListbox.getSelectedItems().isEmpty()){
        EventListener eventListener = new EventListener() {
            @Override
            public void onEvent(Event evt) throws InterruptedException {
                if (Messagebox.ON_OK.equals(evt.getName())) {
                    Set<Listitem> liselect = fileListbox.getSelectedItems();
                                IdMeetingDAO idDAO = new IdMeetingDAO();
                                for (Listitem item : liselect) {
                                    IdMeeting idmeet = (IdMeeting) item.getValue();
                                    idDAO.updateToStatusName(idmeet.getId(), "Đã phê duyệt");
                                }
                                onSearch(0);
                }
            }
        };
        Messagebox.show("Xác nhận phê duyệt?", "Thông báo",
                Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                eventListener);            
        }else{
            showNotification("Bạn chưa chọn bản ghi nào!", Constants.Notification.ERROR);
        }
    }
    
    @Listen("onClick = #btnDigitalSign")
    public void digitalSignDoc(Event event){
        if(fileListbox.getSelectedItems() != null && !fileListbox.getSelectedItems().isEmpty()){
            EventListener eventListener = new EventListener() {
                @Override
                public void onEvent(Event evt) throws InterruptedException {
                    if (Messagebox.ON_OK.equals(evt.getName())) {
                        Set<Listitem> liselect = fileListbox.getSelectedItems();
                        IdMeetingDAO idDAO = new IdMeetingDAO();
                        for (Listitem item : liselect) {
                            IdMeeting idmeet = (IdMeeting) item.getValue();
                            idDAO.updateToStatusName(idmeet.getId(), "Đã ký số");
                        }
                        onSearch(0);
                    }
                }
            };
            Messagebox.show("Xác nhận ký số?", "Thông báo",
                    Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                    eventListener);                
        }else{
            showNotification("Bạn chưa chọn bản ghi nào!", Constants.Notification.ERROR);
        }
    }
    
    public String getDay(Date d) {
        SimpleDateFormat fm = new SimpleDateFormat("dd-MM-yyyy");
        if (d == null) {
            return "";
        } else {
            return fm.format(d);
        }
    }

    @Listen("onView=#fileListbox")
    public void onView(Event event) throws IOException {
        IdMeeting idm = (IdMeeting) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", idMettingAll);
        arguments.put("IdMeeting", idm);
//        createWindow("windowAttCRUD",
//                "/Pages/module/importreq/ID_Meeting.zul", arguments,
//                Window.MODAL);
        
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/ID_Meeting.zul", idMettingAll, arguments);
       
        List<Listitem> list = fileListbox.getItems();
        for(Listitem item : list){
        	item.setDisabled(true);
        }
        window.doModal();
        for(Listitem item : list){
        	item.setDisabled(false);
        }
        
    }
    
     public String getNumberFile(String fileIdStr) {
         int count = 0;
         if(fileIdStr !=null){
             String[] id = fileIdStr.split(";");
             count = id.length;
             
         }
         return String.valueOf(count);
        
        
    }
}
