/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this nation file, choose Tools | Nations
 * and open the nation in the editor.
 */
package com.viettel.module.importDevice.Controller.monitoring;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.Place;
import com.viettel.core.sys.DAO.PlaceDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserBean;
import com.viettel.module.importDevice.DAO.MonitorManageDAO;
import com.viettel.module.importDevice.Model.VFileImportDeviceModel;
import com.viettel.utils.Constants;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author linhdx
 */
public class MonitorManageController extends BaseComposer {

    @Wire("#dbFromDay")
    private Datebox dbFromDay;
    @Wire("#dbToDay")
    private Datebox dbToDay;

    //Danh sach
    @Wire
    Listbox lbUser;
    @Wire
    Label lbTotalFile, lbTotalFileWaitProcessing,lbTotalFileProcessed,
            lbTotalFileWaitOnDeadline, lbTotalFileWaitMissDeadline;

    private VFileImportDeviceModel lastSearchModel = new VFileImportDeviceModel();

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        UserDAOHE phe = new UserDAOHE();
        UserBean usersForm = new UserBean();
        usersForm.setDeptId(Constants.VU_TBYT_ID);
        PagingListModel plm = phe.search(usersForm, 0, 1000);
        List lstUser = plm.getLstReturn();
        ListModelArray lstModelUser = new ListModelArray(lstUser);
        lbUser.setModel(lstModelUser);
        lbUser.renderAll();
       
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        if (dbFromDay != null && dbToDay != null
                && dbFromDay.getValue() != null
                && dbToDay.getValue() != null) {
            if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                showNotification(
                        "Thời gian từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                return;
            }
        }
        lastSearchModel.setCreateDateFrom(dbFromDay == null ? null
                : dbFromDay.getValue());
        lastSearchModel.setCreateDateTo(dbToDay == null ? null
                : dbToDay.getValue());
        Long userId = lbUser.getSelectedItem().getValue();
//        Long userId = -1L;
//        if (userIdStr != null && userIdStr instanceof String) {
//            userId = Long.valueOf(userIdStr);
//        }
        lastSearchModel.setUserId(userId);
        MonitorManageDAO mmDAO= new MonitorManageDAO();
        int totalFile = mmDAO.countImportDeviceFile(lastSearchModel, "totalFile");
        lbTotalFile.setValue(String.valueOf(totalFile));
        int totalFileWaitProcessing = mmDAO.countImportDeviceFile(lastSearchModel, "totalFileWaitProcessing");
        lbTotalFileWaitProcessing.setValue(String.valueOf(totalFileWaitProcessing));
        int totalFileProcessed = totalFile - totalFileWaitProcessing;
        lbTotalFileProcessed.setValue(String.valueOf(totalFileProcessed));
        int totalFileWaitOnDeadline = mmDAO.countImportDeviceFile(lastSearchModel, "totalFileWaitOnDeadline");
        lbTotalFileWaitOnDeadline.setValue(String.valueOf(totalFileWaitOnDeadline));
        int totalFileWaitMissDeadline = mmDAO.countImportDeviceFile(lastSearchModel, "totalFileWaitMissDeadliness");
        lbTotalFileWaitMissDeadline.setValue(String.valueOf(totalFileWaitMissDeadline));
    }

}
