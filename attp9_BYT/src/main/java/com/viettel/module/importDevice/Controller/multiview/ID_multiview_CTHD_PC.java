package com.viettel.module.importDevice.Controller.multiview;

import com.google.gson.Gson;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author Linhdx
 */
public class ID_multiview_CTHD_PC extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    @Wire
    Listbox lbDeviceFile;
    @Wire
    Listbox lbcbxl;
    @Wire
    private Textbox txtComment, txtValidate;
    @Wire
    private Window businessWindow;
    @Wire
    private Label lbnswFileCode, lbproductName, lbCommentCV, lbCommentCVKTC, lbStatusCVKTC,
            lbStatusCV, lbCVKTC, lbCV, lbbusinessName, lbStatus;

    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire
    private Groupbox gbProcess;
    private List<VFileImportDevice> lstDevice;
    private List<com.viettel.core.workflow.BO.Process> lsSendProcess = new ArrayList<com.viettel.core.workflow.BO.Process>();
    private UserToken user;
    private List<com.viettel.core.workflow.BO.Process> lsCurProcess = new ArrayList<com.viettel.core.workflow.BO.Process>();

    List<NodeDeptUser> lsnDu = null;
    int index = -1;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        lstDevice = (List<VFileImportDevice>) arguments.get("lstDevice");
        user = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return super.doBeforeCompose(page, parent, compInfo);

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        WorkflowAPI workflowInstance = WorkflowAPI.getInstance();
        List<NodeToNode> actions;
        ProcessDAOHE pDAOHE = new ProcessDAOHE();
        for (VFileImportDevice files : lstDevice) {
            com.viettel.core.workflow.BO.Process currentP = pDAOHE.getLastByUserId(files.getFileId(), user.getUserId());
            actions = workflowInstance.getNextActionOfNodeId(currentP.getNodeId());
            NodeToNode nTn = actions.get(0);
            if (actions.size() > 0 && actions.get(0).getFormId() == null) {
                nTn = actions.get(1);
            }
            if (lsnDu == null) {
                lsnDu = WorkflowAPI.getInstance().findNDUsByNodeId(nTn.getNextId(), false);
            }
            com.viettel.core.workflow.BO.Process nextP = new com.viettel.core.workflow.BO.Process();
            nextP.setObjectId(files.getFileId());
            nextP.setObjectType(files.getFileType());
            nextP.setActionName(nTn.getAction());
            nextP.setActionType(nTn.getType());
            nextP.setIsActive(1L);
            nextP.setNodeId(nTn.getNextId());
            nextP.setSendUser(currentP.getReceiveUser());
            nextP.setSendUserId(currentP.getReceiveUserId());
            nextP.setSendGroupId(currentP.getReceiveGroupId());
            nextP.setSendGroup(currentP.getReceiveGroup());
            nextP.setStatus(nTn.getStatus());
            nextP.setSendDate(new Date());
            nextP.setPreviousNodeId(nTn.getPreviousId());
            if (lsnDu != null && !lsnDu.isEmpty()) {
                NodeDeptUser nduReceive = lsnDu.get(0);
                nextP.setProcessType(nduReceive.getProcessType());

                nextP.setReceiveGroupId(nduReceive.getDeptId());
                nextP.setReceiveGroup(nduReceive.getDeptName());

            }
            //     p.setReceiveUserId(nduReceive.getUserId());
            //        p.setReceiveUser(nduReceive.getUserName());
            nextP.setOrderProcess(currentP.getOrderProcess() + 1L);
            nextP.setParentId(currentP.getProcessId());
            lsSendProcess.add(nextP);
            lsCurProcess.add(currentP);
        }
        lbcbxl.setModel(new ListModelList<>(lsnDu));
        reload(0);
    }

    public void reload(int pindex) {
        int size = userPagingBottom.getPageSize();
        List<VFileImportDevice> lst = new ArrayList<>();
        for (int i = pindex * size; i < size * (pindex + 1); i++) {
            if (lstDevice.size() > i) {
                lst.add(lstDevice.get(i));
            }
        }
        PagingListModel model = new PagingListModel(lst, new Long(lstDevice.size()));
        userPagingBottom.setTotalSize(model.getCount());
        userPagingTop.setTotalSize(model.getCount());
        if (model.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(model.getLstReturn());
        lstModel.setMultiple(true);
        lbDeviceFile.setModel(lstModel);
        lbDeviceFile.renderAll();
        gbProcess.setVisible(false);
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reload(userPagingBottom.getActivePage());
    }

    public String getComent(int index) {
        return lsSendProcess.get(index + userPagingBottom.getActivePage() * userPagingBottom.getPageSize()).getNote();
    }

    public String getprocessName(int index) {
        return lsSendProcess.get(index + userPagingBottom.getActivePage() * userPagingBottom.getPageSize()).getReceiveUser();
    }

    @Listen("onDeleteFile =  #lbDeviceFile")
    public void onDeleteFile(Event event) {
        int tt = (int) event.getData();
        int idx = userPagingBottom.getActivePage() * userPagingBottom.getPageSize() + tt;
        lstDevice.remove(idx);
        lsSendProcess.remove(idx);
        lsCurProcess.remove(idx);
        if (idx == lstDevice.size() && tt == 0 && userPagingBottom.getActivePage() > 0) {
            reload(userPagingBottom.getActivePage() - 1);
        }
        reload(userPagingBottom.getActivePage());
    }

    @Listen("onShow =  #lbDeviceFile")
    public void onShow(Event event) {
        index = (int) event.getData() + userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        com.viettel.core.workflow.BO.Process saveP = lsSendProcess.get(index);
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        VFileImportDevice files = lstDevice.get(index);
        lbnswFileCode.setValue(files.getNswFileCode());
        lbproductName.setValue(files.getProductName());
        lbbusinessName.setValue(files.getBusinessName());
        lbStatus.setValue(WorkflowAPI.getStatusName(files.getStatus()));
        List<EvaluationRecord> lsEv = objDAO.getListByFileIdAndEvalType(files.getFileId(), Constants.EVAL_TYPE.ROLE_CVC);

        if (lsEv.size() > 0) {
            EvaluationRecord obj = lsEv.get(0);
            lbCommentCV.setValue(obj.getMainContent());
            lbCV.setValue(obj.getCreatorName());
            lbStatusCV.setValue(convertJsonFromContenttoString(obj.getFormContent()));
        }
        List<EvaluationRecord> lsEvc = objDAO.getListByFileIdAndEvalType(files.getFileId(), Constants.EVAL_TYPE.ROLE_CVKTC);

        if (lsEvc.size() > 0) {
            EvaluationRecord obj = lsEvc.get(0);
            lbCommentCVKTC.setValue(obj.getMainContent());
            lbCVKTC.setValue(obj.getCreatorName());
            lbStatusCVKTC.setValue(convertJsonFromContenttoString(obj.getFormContent()));
        }
        int idxcb = -1;
        if (lsnDu != null && !lsnDu.isEmpty()) {
            for (int i = 0; i < lsnDu.size(); i++) {
                NodeDeptUser ndu = lsnDu.get(i);
                if (ndu.getUserId().equals(saveP.getReceiveUserId())) {
                    idxcb = i;
                    break;
                }
            }
        }
        lbcbxl.setSelectedIndex(idxcb);
        txtComment.setValue(saveP.getNote());
        gbProcess.setVisible(true);
    }

    public String convertJsonFromContenttoString(String jsonFormContent) {
        String status = "";
        Gson gson = new Gson();
        if (StringUtils.validString(jsonFormContent)) {
            EvaluationModel evModel = gson.fromJson(jsonFormContent, EvaluationModel.class);
            status = evModel.getResultEvaluationStr();
        }
        return status;

    }

    public ListModel<NodeDeptUser> getUserProcess(int idxl) {
        VFileImportDevice vf = lstDevice.get(idxl);
        ProcessDAOHE psDAO = new ProcessDAOHE();
        List<com.viettel.core.workflow.BO.Process> lsp = psDAO.getByObjectId(vf.getFileId());
        List<NodeDeptUser> lsrender = new ArrayList<>();
        lsrender.add(0, new NodeDeptUser(-1L, "---Chọn---"));
        for (NodeDeptUser us : lsnDu) {
            Boolean k = true;
            for (com.viettel.core.workflow.BO.Process ps : lsp) {
                if (ps.getSendUserId().equals(us.getUserId())) {
                    k = false;
                    break;
                }
            }
            if (k) {
                lsrender.add(us);
            }
        }
        return new ListModelList<>(lsrender);
    }

    @Listen("onSelect = #lbDeviceFile listbox")
    public void onCheckMethod(Event event) {
        Listbox listbox = (Listbox) event.getTarget();
        Listitem listitem = listbox.getSelectedItem();
        int thutu = (int) listbox.getAttribute("thutu");
        com.viettel.core.workflow.BO.Process saveP = lsSendProcess.get(thutu);
        saveP.setReceiveUser(listitem.getLabel());
        saveP.setReceiveUserId(Long.parseLong(listitem.getValue().toString()));
        clearWarningMessage();
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Chủ tịch hội đồng xử lý nhiều hồ sơ");
            onSaveAll();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
            LogUtils.addLogDB(ex);
            Logger.getLogger(ID_multiview_CTHD_PC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void onSaveAll() {
        if (!isValidate()) {
            showNotification("Chưa xử lý cho tất cả hồ sơ !!!");
            return;
        }
        ProcessDAOHE psDAO = new ProcessDAOHE();
        FilesDAOHE fDAO = new FilesDAOHE();
        for (VFileImportDevice f : lstDevice) {
            Files fs = fDAO.findById(f.getFileId());
            fs.setStatus(lsSendProcess.get(0).getStatus());
            fDAO.saveOrUpdate(fs);
        }
        for (com.viettel.core.workflow.BO.Process cp : lsCurProcess) {
            cp.setFinishDate(new Date());
        }
        psDAO.saveOrUpdate_notCommit(lsCurProcess);
        psDAO.saveOrUpdate(lsSendProcess);
        txtValidate.setValue("1");
        
    }

    private boolean isValidate() {
        Boolean rs = true;
        if (lsSendProcess == null || lsSendProcess.isEmpty()) {
            return false;
        }
        for (com.viettel.core.workflow.BO.Process p : lsSendProcess) {
            if (p.getReceiveUserId() == null || p.getReceiveUserId() == -1L) {
                return false;
            }
        }
        return rs;
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        if (lbcbxl.getSelectedItem() == null) {
            showWarningMessage("Chưa chọn người xử lý !!!");
            return;
        }
        com.viettel.core.workflow.BO.Process saveP = lsSendProcess.get(index);
        saveP.setNote(txtComment.getValue());
        saveP.setReceiveUser(lbcbxl.getSelectedItem().getLabel());
        saveP.setReceiveUserId(Long.parseLong(lbcbxl.getSelectedItem().getValue().toString()));
        reload(userPagingBottom.getActivePage());
        clearWarningMessage();
    }

    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    private void clearWarningMessage() {
        lbBottomWarning.setValue("");
    }

}
