package com.viettel.module.importDevice.Controller.multiview;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importDevice.BO.IdMeeting;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.importDevice.DAO.ExportDeviceDAO;
import com.viettel.module.importDevice.DAO.IdMeetingDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceProductDAO;
import com.viettel.module.importDevice.Model.ExportDeviceModel;
import com.viettel.module.importDevice.Model.VFileImportDeviceModel;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.event.PagingEvent;

import com.viettel.core.workflow.BO.Process;
import com.viettel.utils.ResourceBundleUtil;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zul.Button;

/**
 *
 * @author Linhdx
 */
public class ID_multiview_TKHD_XL extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    @Wire
    Listbox lbDeviceFile;
    @Wire
    Listbox lbcbxl, lbaction, lbPeople, lbBBH;
    @Wire
    Button btnInternalProcess;
    @Wire
    private Textbox txtComment, txtValidate, tbplace, tbName, tbPosition;
    @Wire
    private Window businessWindow;
    @Wire
    private Vlayout flist;
    private List<Media> listMedia;
    @Wire
    private Datebox dbdate;
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire
    private Label lbnswFileCode, lbproductName, lbCommentCV, lbCommentCVKTC, lbCommentTVHD,
            lbStatusCVKTC, lbStatusCV, lbCVKTC, lbCV, lbTVHD, lbStatusTVHD, lbbusinessName, ldbookNum;
    @Wire
    private Groupbox gbProcess;
    @Wire
    Window choosePeople;
    private List<VFileImportDevice> lstDevice;
    private List<Process> lsSendProcess = new ArrayList<Process>();
    private UserToken user;
    private List<Process> lsCurProcess = new ArrayList<Process>();
    List<NodeDeptUser> lsnDu = null;
    List<NodeToNode> lsaction = null;
    int index = -1;
    int idxac = -1;
    int idxcb = -1;
    private List listBook;
    private Long evalType;
    private Long bookNumber = null;
    IdMeeting meeting = null;
    private Attachs bbh = null;
    private List<IdMeeting> lsPeople = new ArrayList<>();

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {

        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        lstDevice = (List<VFileImportDevice>) arguments.get("lstDevice");
        try {
            selectionSort(lstDevice, lstDevice.size());
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        user = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        evalType = Constants.FILE_STATUS_CODE.STATUS_HHD;
        return super.doBeforeCompose(page, parent, compInfo);

    }

    private static void selectionSort(List<VFileImportDevice> unsortedArray, int size)
            throws Exception {
        int min;
        boolean ismin;

        for (int i = 0; i < size; i++) {
            min = i;
            ismin = false;
            for (int j = i + 1; j < size; j++) {
                if (unsortedArray.get(j).getNswFileCode().compareTo(unsortedArray.get(min).getNswFileCode()) < 0) {
                    min = j;
                    ismin = true;
                }
            }
            if (ismin == true) {
                VFileImportDevice temp;
                temp = unsortedArray.get(i);
                unsortedArray.set(i, unsortedArray.get(min));
                unsortedArray.set(min, temp);
            }
        }
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        String visibleInternalProcess = ResourceBundleUtil.getString("hopdong");
        if (visibleInternalProcess != null && "true".equals(visibleInternalProcess)) {
            btnInternalProcess.setVisible(true);
        } else {
            btnInternalProcess.setVisible(false);
        }
        IdMeetingDAO idDAO = new IdMeetingDAO();
        meeting = idDAO.getLastActive();
        if (meeting == null) {
            bookNumber = putInBook();
            meeting = new IdMeeting();
            meeting.setDay(new Date());
            meeting.setBooks(bookNumber);
            meeting.setStatus(0L);
            idDAO.saveOrUpdate(meeting);
        } else {
            bookNumber = meeting.getBooks();
        }
        ldbookNum.setValue(bookNumber.toString());
        dbdate.setValue(meeting.getDay());
        tbplace.setValue(meeting.getPlace());
        if (StringUtils.validString(meeting.getPeople())) {
            String[] names = meeting.getPeople().split(";");
            String[] pos = meeting.getPosition().split(";");
            for (int i = 0; i < names.length; i++) {
                IdMeeting idm = new IdMeeting();
                idm.setPeople(names[i]);
                if (i < pos.length) {
                    idm.setPosition(pos[i]);
                }
                lsPeople.add(idm);
            }
        }
        lbPeople.setModel(new ListModelList<>(lsPeople));
        WorkflowAPI workflowInstance = WorkflowAPI.getInstance();
        listMedia = new ArrayList();
        ProcessDAOHE pDAOHE = new ProcessDAOHE();
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        Gson gson = new Gson();
        for (VFileImportDevice files : lstDevice) {
            com.viettel.core.workflow.BO.Process currentP = pDAOHE.getLastByUserId(files.getFileId(), user.getUserId());
            lsaction = workflowInstance.getNextActionOfNodeId(currentP.getNodeId());
            com.viettel.core.workflow.BO.Process nextP = new com.viettel.core.workflow.BO.Process();
            nextP.setObjectId(files.getFileId());
            nextP.setObjectType(files.getFileType());
            nextP.setIsActive(1L);
            nextP.setSendUser(currentP.getReceiveUser());
            nextP.setSendUserId(currentP.getReceiveUserId());
            nextP.setSendGroupId(currentP.getReceiveGroupId());
            nextP.setSendGroup(currentP.getReceiveGroup());
            nextP.setSendDate(new Date());
            nextP.setOrderProcess(currentP.getOrderProcess() + 1L);
            nextP.setParentId(currentP.getProcessId());
            EvaluationRecord obj = objDAO.findContentByFileIdAndEvalType(files.getFileId(), evalType);
            if (obj != null) {
                com.viettel.module.evaluation.Model.EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
                Process process = evModel.getProcess();
                if (process != null) {
                    nextP.setActionName(process.getActionName());
                    nextP.setNote(process.getNote());
                    nextP.setReceiveUserId(process.getReceiveUserId());
                    nextP.setReceiveUser(process.getReceiveUser());
                    nextP.setReceiveGroupId(process.getReceiveGroupId());
                    nextP.setReceiveGroup(process.getReceiveGroup());
                    nextP.setNodeId(process.getNodeId());
                    nextP.setActionType(process.getActionType());
                    nextP.setStatus(process.getStatus());
                }

            }
            lsSendProcess.add(nextP);
            lsCurProcess.add(currentP);
        }
        lbaction.setModel(new ListModelList<>(lsaction));
        reload(0);
    }

    public void reload(int pindex) {
        int size = userPagingBottom.getPageSize();
        List<VFileImportDevice> lst = new ArrayList<>();
        for (int i = pindex * size; i < size * (pindex + 1); i++) {
            if (lstDevice.size() > i) {
                lst.add(lstDevice.get(i));
            }
        }
        PagingListModel model = new PagingListModel(lst, new Long(lstDevice.size()));
        userPagingBottom.setTotalSize(model.getCount());
        userPagingTop.setTotalSize(model.getCount());
        if (model.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(model.getLstReturn());
        lstModel.setMultiple(true);
        lbDeviceFile.setModel(lstModel);
        lbDeviceFile.renderAll();
        loadBBH();
        gbProcess.setVisible(false);
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reload(userPagingBottom.getActivePage());
    }

    public String getResult(int index) {
        return lsSendProcess.get(index + userPagingBottom.getActivePage() * userPagingBottom.getPageSize()).getActionName();
    }

    public String getComent(int index) {
        return lsSendProcess.get(index + userPagingBottom.getActivePage() * userPagingBottom.getPageSize()).getNote();
    }

    public String getprocessName(int index) {
        return lsSendProcess.get(index + userPagingBottom.getActivePage() * userPagingBottom.getPageSize()).getReceiveUser();
    }

    @Listen("onSelect =  #lbaction")
    public void onSelectAction() {
        idxac = lbaction.getSelectedIndex();

        NodeToNode nTn = lsaction.get(idxac);
        lsnDu = WorkflowAPI.getInstance().findNDUsByNodeId(nTn.getNextId(), false);
        lbcbxl.clearSelection();
        lbcbxl.setModel(new ListModelList<>(lsnDu));
        lbcbxl.renderAll();
        lbcbxl.setSelectedIndex(-1);
    }

    @Listen("onSelect =  #lbcbxl")
    public void onSelectCb() {
        idxcb = lbcbxl.getSelectedIndex();
    }

    @Listen("onDeleteFile =  #lbDeviceFile")
    public void onDeleteFile(Event event) {
        int tt = (int) event.getData();
        int idx = userPagingBottom.getActivePage() * userPagingBottom.getPageSize() + tt;
        lstDevice.remove(idx);
        lsSendProcess.remove(idx);
        lsCurProcess.remove(idx);
        if (idx == lstDevice.size() && tt == 0 && userPagingBottom.getActivePage() > 0) {
            reload(userPagingBottom.getActivePage() - 1);
        }
        reload(userPagingBottom.getActivePage());
    }

    @Listen("onShow =  #lbDeviceFile")
    public void onShow(Event event) {
        index = (int) event.getData() + userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        com.viettel.core.workflow.BO.Process saveP = lsSendProcess.get(index);
        VFileImportDevice files = lstDevice.get(index);
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("files", files);
        arguments.put("saveP", saveP);
        arguments.put("numberIndex", (int) event.getData());
        arguments.put("parentWindow", businessWindow);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/multiView/ID_multiView_TKHD_XL_Popup.zul", businessWindow, arguments);
        window.doModal();

//        createWindow("popup",
//                "/Pages/module/importreq/multiView/ID_multiView_TKHD_XL_Popup.zul", arguments, Window.POPUP);
    }

    @Listen("onClick = #btnInternalProcess")
    public void onShowInternalProcess(Event event) {
        Map<String, Object> args = new ConcurrentHashMap<>();
        args.put("parentWindow", businessWindow);
        args.put("meetingId", meeting.getId());
//        createWindow("windowInternalprocess",
//                "/Pages/module/importreq/internalProcess_popup.zul", args, Window.POPUP);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/internalProcess_popup.zul", null, args);
        window.doModal();
    }

    @Listen("onViewHS =  #lbDeviceFile")
    public void onViewHS(Event event) {
        index = (int) event.getData() + userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        VFileImportDevice files = lstDevice.get(index);
        createWindowView(files, Constants.DOCUMENT_MENU.ALL,
                businessWindow);
        //businessWindow.setVisible(false);
    }

    private void createWindowView(VFileImportDevice files, int menuType, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", files.getFileId());
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", files.getFileType());
        arguments.put("deptid", Constants.VU_TBYT_ID);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importreq/importdeviceView2.zul", parentWindow, arguments);
        window.doModal();
//        createWindow("windowView2",
//                "/Pages/module/importreq/importdeviceView2.zul", arguments,
//                Window.POPUP);
    }

    public String convertJsonFromContenttoString(String jsonFormContent) {
        String status = "";
        Gson gson = new Gson();
        if (StringUtils.validString(jsonFormContent)) {
            EvaluationModel evModel = gson.fromJson(jsonFormContent, EvaluationModel.class);
            status = evModel.getResultEvaluationStr();
        }
        return status;

    }

    @Listen("onDeleteName=#lbPeople")
    public void onDeleteName(Event e) {
        int idx = (int) e.getData();
        lsPeople.remove(idx);
        lbPeople.setModel(new ListModelList<>(lsPeople));
        lbPeople.renderAll();
    }

    @Listen("onClick=#btnCreatePeople")
    public void onAddName() {
        if (!StringUtils.validString(tbName.getValue()) || !StringUtils.validString(tbPosition.getValue())) {
            showNotification("Chưa nhập tên hoặc chức danh !!!");
        } else {
            IdMeeting idm = new IdMeeting();
            idm.setPeople(tbName.getValue());
            idm.setPosition(tbPosition.getValue());
            lsPeople.add(idm);
            lbPeople.setModel(new ListModelList<>(lsPeople));
            lbPeople.renderAll();
            tbName.setValue(null);
            tbPosition.setValue(null);
        }
    }

    @Listen("onClick=#btnChoosePeople")
    public void onChoosePeople() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", businessWindow);
        createWindow("windowChoosePeople", "/Pages/module/importreq/multiView/choosePeople.zul",
                arguments, Window.MODAL);
    }

    @Listen("onSelectedPeople =#businessWindow")
    public void onSelectedPeople(Event event) {
        Map<String, Object> arguments = (Map<String, Object>) event.getData();
//        showNotify("Lụa chọn thành công!");
        List<Category> lst = (List<Category>) arguments.get("lstPeopleChoose");
        for (Category a : lst) {
            IdMeeting idm = new IdMeeting();
            idm.setPeople(a.getName());
            idm.setPosition(a.getValue());
            lsPeople.add(idm);
        }

        lbPeople.setModel(new ListModelList<>(lsPeople));
        lbPeople.renderAll();

    }

    private void showNotify(String msg) {
        showNotification(msg, Constants.Notification.INFO);
    }

    private boolean expBBHHd(Boolean addAtt) {
        ExportDeviceModel exportModel = new ExportDeviceModel();
        String sendNo = getSendNo(bookNumber);
        //exportModel.setLstProduct(lstCosmetic);
        exportModel.setSendNo(sendNo);
        exportModel.setPlace(tbplace.getValue());
        exportModel.setSignedDate(dbdate.getValue());
        ExportDeviceDAO exp = new ExportDeviceDAO();
        List<VFileImportDeviceModel> lstModelDY = new ArrayList<>();
        List<VFileImportDeviceModel> lstModelTC = new ArrayList<>();
        List<VFileImportDeviceModel> lstModelBS = new ArrayList<>();
        for (int j = 0; j < lstDevice.size(); j++) {
            VFileImportDevice vFileImport = lstDevice.get(j);
            VFileImportDeviceModel deviceModel = new VFileImportDeviceModel();
            deviceModel.setImporterName(vFileImport.getImporterName());
            String C8 = lsSendProcess.get(j).getActionName();
            if (C8 != null && C8.contains("họp cấp phép")) {
                C8 = "Đồng ý cấp phép";
            }
            //Cuongvv
            if (C8 != null && C8.contains("từ chối cấp phép")) {
                C8 = "Từ chối cấp phép";
                C8 += "\n";
            }

            //deviceModel.setC8(lsSendProcess.get(j).getNote());
            //deviceModel.setC1(vFileImport.getProductName());
            deviceModel.setBookNumberStr("Số đến:"
                    + vFileImport.getBookNumber());
            deviceModel.setImportDeviceFileId(vFileImport.getImportDeviceFileId());
            deviceModel.setImporterName(vFileImport.getImporterName());
            deviceModel.setNswFileCode("Mã HS: "
                    + vFileImport.getNswFileCode());
            ImportDeviceProductDAO idp = new ImportDeviceProductDAO();
            List<ImportDeviceProduct> lstProduct = idp.findByImportFileId(vFileImport.getFileId());
            if (lstProduct.size() > 0) {
                String c2 = "";
                String c3 = "";
                String c4 = "";
                String c5 = "";
                String c6 = "";
                for (int i = 0; i < lstProduct.size(); i++) {
                    ImportDeviceProduct product = lstProduct.get(i);
                    //c2 += product.getProductName() + "\r\n";
                    c3 += product.getModel() + "\r\n";
                    c4 += (product.getManufacturer() == null ? "" : product.getManufacturer());
                    if (!c4.contains("phụ lục đính kèm") && !"".equals(c4)) {
                        c4 += (product.getNationalName() == null ? " " : ", " + product.getNationalName()) + "\r\n";
                    }
                    c6 += (product.getProductManufacture() == null ? "" : product.getProductManufacture());
                    if (!c6.contains("phụ lục đính kèm")) {
                        c6 += (product.getNationalProduct() == null ? " " : ", " + product.getNationalProduct()) + "\r\n";
                    }

                    c5 += (product.getDistributor() == null ? "" : product.getDistributor());
                    if (!c5.contains("phụ lục đính kèm")) {
                        c5 += (product.getNationalDistributorName() == null ? " " : ", " + product.getNationalDistributorName()) + "\r\n";
                    }

//                    c4 += (product.getManufacturer() == null ? "_" : product.getManufacturer()) + (product.getNationalName() == null ? "_" : ", " + product.getNationalName()) + "\r\n";
//                    c6 += (product.getProductManufacture() == null ? "_" : product.getProductManufacture()) + (product.getNationalProduct() == null ? "_" : ", " + product.getNationalProduct()) + "\r\n";
//                    c5 += (product.getDistributor() == null ? "_" : product.getDistributor()) + (product.getNationalDistributorName() == null ? "_" : ", " + product.getNationalDistributorName()) + "\r\n";
                }

                c2 += vFileImport.getProductName();
                deviceModel.setC2(c2);
                deviceModel.setC3(c3);
                deviceModel.setC4(c4);
                deviceModel.setC5(c5);
                deviceModel.setC6(c6);
                deviceModel.setC8(C8);
            }

            lstModelDY.add(deviceModel);

//            if ("Đồng ý cấp phép".equals(C8) || C8.contains("ý cấp phép")) {
//                lstModelDY.add(deviceModel);
//            } else if ("Từ chối cấp phép".equals(C8) || C8.contains("chối cấp phép")) {
//                lstModelTC.add(deviceModel);
//            } else {
//                lstModelBS.add(deviceModel);
//            }
        }
        boolean isOk = exp.exportBienbanhop(exportModel, lstModelDY, lstModelTC, lstModelBS, lsPeople, addAtt ? meeting.getId() : null);
        return isOk;
    }

    @Listen("onClick=#btnAddM")
    public void onAddMeeting() {
        if (lsPeople == null || lsPeople.isEmpty()) {
            showNotification("Chưa chọn thành viên hội đồng !!!");
            return;
        }
        if (lstDevice == null || lstDevice.isEmpty()) {
            showNotification("Chưa chọn hồ sơ cần xử lý !!!");
            return;
        }
        Boolean rs = true;
        if (lsSendProcess == null || lsSendProcess.isEmpty()) {
            showNotification("Chưa xử lý cho tất cả hồ sơ !!!");
            rs = false;
        }
        if (rs) {
            for (com.viettel.core.workflow.BO.Process p : lsSendProcess) {
                if (p.getReceiveUserId() == null) {
                    showNotification("Chưa xử lý cho tất cả hồ sơ !!!");
                    return;
                }
            }
        }
        IdMeetingDAO idDao = new IdMeetingDAO();
        meeting.setDay(dbdate.getValue());
        meeting.setPlace(tbplace.getValue());
        String pp = "";
        String ps = "";
        String id = "";
        for (IdMeeting idm : lsPeople) {
            pp += idm.getPeople() + ";";
            ps += idm.getPosition() + ";";
        }
        ImportDeviceFiletDAO idfDAO = new ImportDeviceFiletDAO();
        for (VFileImportDevice f : lstDevice) {
            id += f.getFileId() + ";";
            f.setBookMeeting(bookNumber);
            ImportDeviceFile idf = idfDAO.findById(f.getImportDeviceFileId());
            idf.setBookMeeting(bookNumber);
            idfDAO.saveOrUpdate(idf);
        }

        pp = "".equals(pp) ? "" : pp.substring(0, pp.length() - 1);
        ps = "".equals(ps) ? "" : ps.substring(0, ps.length() - 1);
        id = "".equals(id) ? "" : id.substring(0, id.length() - 1);
        meeting.setPeople(pp);
        meeting.setPosition(ps);
        meeting.setFileId(id);
        idDao.saveOrUpdate(meeting);
        reload(userPagingBottom.getActivePage());
        boolean isOk = false;
        if (rs) {
            isOk = expBBHHd(rs);
        } else {
            AttachDAOHE attDAO = new AttachDAOHE();
            attDAO.deleteByFileId(meeting.getId(), Constants.OBJECT_TYPE.ID_MEETING_ATT);
        }
        loadBBH();
        if (isOk) {
            showSuccessNotification("Chốt biên bản họp thành công !!!");
        } else {
            showWarningMessage("Lỗi Webservice !!!");
        }

    }

    @Listen("onClick=#btnExpAtt")
    public void onExportFile() {
        if (lstDevice == null || lstDevice.isEmpty()) {
            showWarningMessage("Không có hồ sơ !!!");
            return;
        }
        ExportDeviceModel exportModel = new ExportDeviceModel();
        //Cuongvv
        String sendNo = getSendNo(bookNumber);
        //exportModel.setLstProduct(lstCosmetic);
        exportModel.setSendNo(sendNo);
        ExportDeviceDAO exp = new ExportDeviceDAO();

        List<VFileImportDeviceModel> lstModel = new ArrayList<VFileImportDeviceModel>();
        for (int j = 0; j < lstDevice.size(); j++) {
            VFileImportDevice vFileImport = lstDevice.get(j);
            VFileImportDeviceModel deviceModel = new VFileImportDeviceModel();
            deviceModel.setBookNumberStr("Số đến :"
                    + vFileImport.getBookNumber());
            deviceModel.setImportDeviceFileId(vFileImport.getImportDeviceFileId());
            deviceModel.setImporterName(vFileImport.getImporterName());
            deviceModel.setNswFileCode("Mã HS: "
                    + vFileImport.getNswFileCode());
            deviceModel.setStrStatus(WorkflowAPI.getStatusName(vFileImport.getStatus()));
            deviceModel.setResulCVC(deviceModel.getContent(
                    vFileImport.getFileId(), Constants.EVAL_TYPE.ROLE_CVC));
            deviceModel.setResulCVKTC(deviceModel.getContent(
                    vFileImport.getFileId(), Constants.EVAL_TYPE.ROLE_CVKTC));
            deviceModel.setResulTVHD(deviceModel.getContent(
                    vFileImport.getFileId(), Constants.EVAL_TYPE.ROLE_TVHD));
            deviceModel.setResultAll(deviceModel.getResulCVC() + "\r\n" + deviceModel.getResulCVKTC() + "\r\n"
                    + deviceModel.getResulTVHD());
            //deviceModel.setResulHHD("Kết luận: " + lsSendProcess.get(j).getActionName() + ", " + lsSendProcess.get(j).getNote());
            Long fileId = vFileImport.getFileId();
            ImportDeviceProductDAO idp = new ImportDeviceProductDAO();
            List<ImportDeviceProduct> lstProduct = idp.findByImportFileId(fileId);
            String model = "";
            String nationalName = "";
            String nationalDistributorName = "";
            String nationalProduct = "";
            String procductYear = "";
            //String procductName = "Tên trang thiết bị y tế: ";
            String procductName = "";
            if (lstProduct.size() > 0) {
                for (int i = 0; i < lstProduct.size(); i++) {
                    //linhdx sua
                    ImportDeviceProduct product = lstProduct.get(i);
                    model += product.getModel() + "\r\n";
                    nationalName += (product.getManufacturer() == null ? "" : product.getManufacturer());
                    if (!nationalName.contains("phụ lục đính kèm")) {
                        nationalName += (product.getNationalName() == null ? "" : ", " + product.getNationalName()) + "\r\n";
                    }
                    nationalProduct += (product.getProductManufacture() == null ? " " : product.getProductManufacture());
                    if (!nationalProduct.contains("phụ lục đính kèm") && !" ".equals(nationalProduct)) {
                        nationalProduct += (product.getNationalProduct() == null ? "" : ", " + product.getNationalProduct()) + "\r\n";
                    }
                    nationalDistributorName += (product.getDistributor() == null ? "" : product.getDistributor());
                    if (!nationalDistributorName.contains("phụ lục đính kèm")) {
                        nationalDistributorName += (product.getNationalDistributorName() == null ? "" : ", " + product.getNationalDistributorName()) + "\r\n";
                    }

                    procductYear += product.getProduceYear() + "\r\n";
                }
            }
            deviceModel.setProductName(procductName + vFileImport.getProductName());
            deviceModel.setModel(model);
            deviceModel.setNationalName(nationalName);
            deviceModel.setNationalProduct(nationalProduct);
            deviceModel.setNationalDistributorName(nationalDistributorName);
            deviceModel.setProcductYear(procductYear);

            lstModel.add(deviceModel);
        }
        exp.exportBienbanCBhop(exportModel, lstModel, true);
    }

    @Listen("onClick=#btnExpAtt2")
    public void onExportFile2() {
        onExportFile();
    }

    private Long putInBook() {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Long docType = 4950L;
        String bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_MEETINGS;
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    protected BookDocument createBookDocument(Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(0L);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    private String getSendNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
        permitNo += "/BB-NK";
        return permitNo;
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        if (lbaction.getSelectedIndex() < 0) {
            showWarningMessage("Chưa chọn kết quả xử lý !!!");
            return;
        }
        if (lbcbxl.getSelectedIndex() < 0) {
            showWarningMessage("Chưa chọn người xử lý !!!");
            return;
        }
        com.viettel.core.workflow.BO.Process saveP = lsSendProcess.get(index);
        NodeToNode nTn = lsaction.get(idxac);
        NodeDeptUser nduReceive = lsnDu.get(idxcb);

        saveP.setActionName(nTn.getAction());
        saveP.setActionType(nTn.getType());
        saveP.setNodeId(nTn.getNextId());

        saveP.setStatus(nTn.getStatus());
        saveP.setSendDate(new Date());
        saveP.setPreviousNodeId(nTn.getPreviousId());
        if (!lsnDu.isEmpty()) {
            saveP.setProcessType(nduReceive.getProcessType());

            saveP.setReceiveGroupId(nduReceive.getDeptId());
            saveP.setReceiveGroup(nduReceive.getDeptName());

        }
        saveP.setReceiveUserId(nduReceive.getUserId());
        saveP.setReceiveUser(nduReceive.getUserName());
        saveP.setNote(txtComment.getValue());
        saveEval(saveP.getObjectId());
        reload(userPagingBottom.getActivePage());
        clearWarningMessage();
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();
        listMedia.clear();
        flist.getChildren().clear();
        Long limitSize = Long.parseLong(getConfigValue("limitSizeAttachPerDraft", "config", "20971520"));
        String limit = getConfigValue("limitSizeStr", "config", "3M");
        for (final Media media : medias) {
            Long mediaSize = Integer.valueOf(media.getByteData().length).longValue();
            if (mediaSize > limitSize) {
                showNotification("Dung lượng file đính kèm không được vượt quá " + limit, Clients.NOTIFICATION_TYPE_INFO);
                return;
            }
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Thư ký hội đồng xử lý nhiều hồ sơ");
            onSaveAll();
        } catch (Exception ex) {
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
            LogUtils.addLogDB(ex);
        }
    }

    private void saveEval(Long fileId) {
        Gson gson = new Gson();

        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        EvaluationRecord obj = objDAO.findContentByFileIdAndEvalType(fileId, evalType);
        if (obj == null) {
            obj = new EvaluationRecord();
        }
        com.viettel.module.evaluation.Model.EvaluationModel evaluModel = new com.viettel.module.evaluation.Model.EvaluationModel();
        evaluModel.setUserId(getUserId());
        evaluModel.setUserName(getUserName());
        evaluModel.setDeptId(getDeptId());
        evaluModel.setDeptName(getDeptName());
        evaluModel.setContent(txtComment.getValue());
        evaluModel.setResultEvaluationStr(lbaction.getSelectedItem().getLabel());
        String jsonEvaluation = gson.toJson(evaluModel);
        obj.setMainContent(txtComment.getValue());
        obj.setFormContent(jsonEvaluation);
        obj.setCreatorId(getUserId());
        obj.setCreatorName(getUserFullName());
        obj.setFileId(fileId);
        obj.setCreateDate(new Date());
        obj.setIsActive(Constants.Status.ACTIVE);
        obj.setEvalType(evalType);
        objDAO.saveOrUpdate(obj);
    }

    @Listen("onClick=#btnCreateM")
    public void onAddAttM() throws IOException {
        if (listMedia == null || listMedia.isEmpty()) {
            showNotification("Chưa chọn tệp đính kèm");
            return;
        }
        AttachDAO attDAO = new AttachDAO();
        for (Media med : listMedia) {
            attDAO.saveReportMeetingFileAttach(med, meeting.getId(), Constants.OBJECT_TYPE.ID_MEETING_ATT, Constants.OBJECT_TYPE.ID_MEETING_ATT);
        }
        loadBBH();
    }

    @Listen("onDeleteAtt = #lbBBH")
    public void onDeleteAtt(Event event) {
        Attachs obj = (Attachs) event.getData();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        loadBBH();
    }

    @Listen("onViewAtt = #lbBBH")
    public void onViewAtt(Event event) throws Exception {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);
    }

    private void loadBBH() {
        // lbBBH.getChildren().clear();
        listMedia.clear();
        flist.getChildren().clear();
        AttachDAOHE attDAO = new AttachDAOHE();
        List<Attachs> lsa = attDAO.getByObjectIdAndAttachCatAndAttachType(meeting.getId(), Constants.OBJECT_TYPE.ID_MEETING_ATT, Constants.OBJECT_TYPE.ID_MEETING_ATT);
        lbBBH.setModel(new ListModelList<>(lsa));
        lbBBH.renderAll();
    }

    private Attachs coppyAtatchs(Long fileId) {
        AttachDAOHE attDAO = new AttachDAOHE();
        Attachs a = attDAO.getByObjectIdAndAttachCat(fileId, Constants.OBJECT_TYPE.ID_MEETING_ATT);
        if (a == null) {
            a = new Attachs();
        }
        a.setObjectId(fileId);
        a.setAttachName(bbh.getAttachName());
        a.setAttachPath(bbh.getAttachPath());
        a.setAttachCat(Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE);
        a.setAttachType(Constants.OBJECT_TYPE.BOARD_SECRETARY_FILE_TYPE);
        a.setAttachTypeName(bbh.getAttachTypeName());
        a.setCreatorId(bbh.getCreatorId());
        a.setCreatorName(bbh.getCreatorName());
        a.setDateCreate(bbh.getDateCreate());
        a.setDateModify(bbh.getDateModify());
        a.setIsSent(bbh.getIsSent());
        a.setIsActive(bbh.getIsActive());
        return a;
    }

    private void onSaveAll() throws IOException {
        if (!isValidate()) {
            return;
        }
        // onAddMeeting();
        AttachDAOHE attDAO = new AttachDAOHE();
//        bbh = attDAO.getByObjectIdAndAttachCat(meeting.getId(), Constants.OBJECT_TYPE.ID_MEETING_ATT);
//
//        if (bbh == null) {
//            showNotification("Chưa chốt biên bản họp hội đồng !!!");
//            return;
//        }
        ProcessDAOHE psDAO = new ProcessDAOHE();
        FilesDAOHE fDAO = new FilesDAOHE();
        IdMeetingDAO mDAO = new IdMeetingDAO();
        ImportDeviceFiletDAO idfDAO = new ImportDeviceFiletDAO();
        for (int i = 0; i < lstDevice.size(); i++) {
            VFileImportDevice f = lstDevice.get(i);
            Files fs = fDAO.findById(f.getFileId());
            ImportDeviceFile idf = idfDAO.findById(f.getImportDeviceFileId());
            fs.setStatus(lsSendProcess.get(i).getStatus());
            Attachs aa = coppyAtatchs(f.getFileId());
            attDAO.saveOrUpdate(aa);
            idf.setBookMeeting(bookNumber);
            idfDAO.saveOrUpdate(idf);
            fDAO.saveOrUpdate(fs);
        }
        for (com.viettel.core.workflow.BO.Process cp : lsCurProcess) {
            cp.setFinishDate(new Date());
        }
        psDAO.saveOrUpdate_notCommit(lsCurProcess);
        //Lưu biên bản
        String result = "";
        for (com.viettel.core.workflow.BO.Process ps : lsSendProcess) {
            result += ps.getActionName() + ";";
        }
        result = result == "" ? "" : result.substring(0, result.length() - 1);
        meeting.setResult(result);
        meeting.setStatus(1L);
        mDAO.saveOrUpdate(meeting);
        psDAO.saveOrUpdate(lsSendProcess);
        txtValidate.setValue("1");

    }

    private boolean isValidate() {
        Boolean rs = true;
        if (lsSendProcess == null || lsSendProcess.isEmpty()) {
            showNotification("Chưa xử lý cho tất cả hồ sơ !!!");
            return false;
        }
        for (com.viettel.core.workflow.BO.Process p : lsSendProcess) {
            if (p.getReceiveUserId() == null) {
                showNotification("Chưa xử lý cho tất cả hồ sơ !!!");
                return false;
            }
        }
        AttachDAOHE attDAO = new AttachDAOHE();
        bbh = attDAO.getByObjectIdAndAttachCat(meeting.getId(), Constants.OBJECT_TYPE.ID_MEETING_ATT);

        if (bbh == null) {
            showNotification("Chưa chốt biên bản họp hội đồng !!!");
            return false;
        }
        return rs;
    }

    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    private void clearWarningMessage() {
        lbBottomWarning.setValue("");
    }

    @Listen("onReload =#businessWindow")
    public void onReload(Event event) {
        Map<String, Object> arguments = (Map<String, Object>) event.getData();
        int numberIndex = (Integer) arguments.get("numberIndex");
        Process saveP = (Process) arguments.get("saveP");
        lsSendProcess.set(numberIndex + userPagingBottom.getActivePage() * userPagingBottom.getPageSize(), saveP);
        reload(userPagingBottom.getActivePage());
    }
}
