package com.viettel.module.importDevice.Controller.multiview;

import com.google.gson.Gson;
import com.viettel.core.user.model.UserToken;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importDevice.BO.IdMeeting;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.utils.Constants;
import com.viettel.utils.StringUtils;
import java.util.*;
import org.zkoss.zul.Groupbox;
import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

/**
 *
 * @author Linhdx
 */
public class ID_multiview_TKHD_XL_Popup extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    @Wire
    Listbox lbDeviceFile;
    @Wire
    Listbox lbcbxl, lbaction, lbPeople, lbBBH;
    @Wire
    private Textbox txtComment;

    @Wire
    private Label lbnswFileCode, lbproductName, lbCommentCV, lbCommentCVKTC, lbCommentTVHD,
            lbStatusCVKTC, lbStatusCV, lbCVKTC, lbCV, lbTVHD, lbStatusTVHD, lbbusinessName, ldbookNum;
    @Wire
    private Groupbox gbProcess;
    @Wire
    Window choosePeople;
    List<NodeDeptUser> lsnDu = null;
    List<NodeToNode> lsaction = null;
    int index = -1;
    int idxac = -1;
    int idxcb = -1;
    private Long evalType;
    IdMeeting meeting = null;
    private Process saveP;
    private Window parentWindow;
    private int numberIndex;
    
    @Wire
    Window windowPopup;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {

        return super.doBeforeCompose(page, parent, compInfo);

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map<String, Object>) Executions.getCurrent().getArg();
        saveP = (Process) arguments.get("saveP");
        VFileImportDevice files = (VFileImportDevice) arguments.get("files");
        parentWindow = (Window) arguments.get("parentWindow");
        numberIndex = (Integer) arguments.get("numberIndex");
        evalType = Constants.FILE_STATUS_CODE.STATUS_HHD;
        ProcessDAOHE pDAOHE = new ProcessDAOHE();
        UserToken user = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        WorkflowAPI workflowInstance = WorkflowAPI.getInstance();
        Process currentP = pDAOHE.getLastByUserId(files.getFileId(), user.getUserId());
        lsaction = workflowInstance.getNextActionOfNodeId(currentP.getNodeId());
        lbaction.setModel(new ListModelList<>(lsaction));

        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lbnswFileCode.setValue(files.getNswFileCode());
        lbproductName.setValue(files.getProductName());
        lbbusinessName.setValue(files.getBusinessName());
        List<EvaluationRecord> lsEv = objDAO.getListByFileIdAndEvalType(files.getFileId(), Constants.EVAL_TYPE.ROLE_CVC);

        if (lsEv.size() > 0) {
            EvaluationRecord obj = lsEv.get(0);
            lbCommentCV.setValue(obj.getMainContent());
            lbCV.setValue(obj.getCreatorName());
            lbStatusCV.setValue(convertJsonFromContenttoString(obj.getFormContent()));
        }
        List<EvaluationRecord> lsEvc = objDAO.getListByFileIdAndEvalType(files.getFileId(), Constants.EVAL_TYPE.ROLE_CVKTC);

        if (lsEvc.size() > 0) {
            EvaluationRecord obj = lsEvc.get(0);
            lbCommentCVKTC.setValue(obj.getMainContent());
            lbCVKTC.setValue(obj.getCreatorName());
            lbStatusCVKTC.setValue(convertJsonFromContenttoString(obj.getFormContent()));
        }
        List<EvaluationRecord> lsEvtvhd = objDAO.getListByFileIdAndEvalType(files.getFileId(), Constants.EVAL_TYPE.ROLE_TVHD);

        if (lsEvtvhd.size() > 0) {
            EvaluationRecord obj = lsEvtvhd.get(0);
            lbCommentTVHD.setValue(obj.getMainContent());
            lbTVHD.setValue(obj.getCreatorName());
            lbStatusTVHD.setValue(convertJsonFromContenttoString(obj.getFormContent()));
        }
        idxac = 0;
        if (lsaction != null && !lsaction.isEmpty()) {
            for (int i = 0; i < lsaction.size(); i++) {
                NodeToNode ntn = lsaction.get(i);
                if (ntn.getAction().equals(saveP.getActionName())) {
                    idxac = i;
                    break;
                }
            }
        }
        
        lbaction.renderAll();
        lbaction.setSelectedIndex(idxac);
        idxcb = 0;
        if (idxac >= 0 && lsaction != null) {
            lsnDu = WorkflowAPI.getInstance().findNDUsByNodeId(lsaction.get(idxac).getNextId(), false);
            lbcbxl.clearSelection();
            lbcbxl.setModel(new ListModelList<>(lsnDu));
            if (lsnDu != null && !lsnDu.isEmpty()) {
                for (int i = 0; i < lsnDu.size(); i++) {
                    NodeDeptUser ndu = lsnDu.get(i);
                    if (ndu.getUserId().equals(saveP.getReceiveUserId())) {
                        idxcb = i;
                        break;
                    }
                }
            }
        }
        lbcbxl.renderAll();
        lbcbxl.setSelectedIndex(idxcb);
        txtComment.setValue(saveP.getNote());
        gbProcess.setVisible(true);
    }

    public String convertJsonFromContenttoString(String jsonFormContent) {
        String status = "";
        Gson gson = new Gson();
        if (StringUtils.validString(jsonFormContent)) {
            EvaluationModel evModel = gson.fromJson(jsonFormContent, EvaluationModel.class);
            status = evModel.getResultEvaluationStr();
        }
        return status;

    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        if (lbaction.getSelectedIndex() < 0) {
            showWarningMessage("Chưa chọn kết quả xử lý !!!");
            return;
        }
        if (lbcbxl.getSelectedIndex() < 0) {
            showWarningMessage("Chưa chọn người xử lý !!!");
            return;
        }
        NodeToNode nTn = lsaction.get(idxac);
        NodeDeptUser nduReceive = lsnDu.get(idxcb);
        saveP.setSendUserId(getUserId());
        saveP.setSendUser(getUserName());
        saveP.setSendGroupId(getDeptId());
        saveP.setSendGroup(getDeptName());
        saveP.setActionName(nTn.getAction());
        saveP.setActionType(nTn.getType());
        saveP.setNodeId(nTn.getNextId());

        saveP.setStatus(nTn.getStatus());
        saveP.setSendDate(new Date());
        saveP.setPreviousNodeId(nTn.getPreviousId());
        if (lsnDu != null && !lsnDu.isEmpty()) {
            saveP.setProcessType(nduReceive.getProcessType());
            saveP.setReceiveGroupId(nduReceive.getDeptId());
            saveP.setReceiveGroup(nduReceive.getDeptName());

        }
        saveP.setReceiveUserId(nduReceive.getUserId());
        saveP.setReceiveUser(nduReceive.getUserName());
        saveP.setNote(txtComment.getValue());
        saveEval(saveP);

        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("numberIndex", numberIndex);
        arguments.put("saveP",saveP);
        Events.sendEvent(new Event("onReload", parentWindow, arguments));
        windowPopup.detach();
        //reload(userPagingBottom.getActivePage());
        clearWarningMessage();
    }

    private void saveEval(Process saveP) {
        Long fileId = saveP.getObjectId();
        Gson gson = new Gson();

        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        EvaluationRecord obj = objDAO.findContentByFileIdAndEvalType(fileId, evalType);
        if (obj == null) {
            obj = new EvaluationRecord();
        }
        com.viettel.module.evaluation.Model.EvaluationModel evaluModel = new com.viettel.module.evaluation.Model.EvaluationModel();
        evaluModel.setProcess(saveP);
        evaluModel.setContent(txtComment.getValue());
        evaluModel.setUserId(getUserId());
        evaluModel.setUserName(getUserName());
        evaluModel.setDeptId(getDeptId());
        evaluModel.setDeptName(getDeptName());
        evaluModel.setContent(txtComment.getValue());
        evaluModel.setResultEvaluationStr(lbaction.getSelectedItem().getLabel());
        String jsonEvaluation = gson.toJson(evaluModel);
        obj.setMainContent(txtComment.getValue());
        obj.setFormContent(jsonEvaluation);
        obj.setCreatorId(getUserId());
        obj.setCreatorName(getUserFullName());
        obj.setFileId(fileId);
        obj.setCreateDate(new Date());
        obj.setIsActive(Constants.Status.ACTIVE);
        obj.setEvalType(evalType);
        objDAO.saveOrUpdate(obj);
    }

    @Listen("onSelect =  #lbaction")
    public void onSelectAction() {
        idxac = lbaction.getSelectedIndex();

        NodeToNode nTn = lsaction.get(idxac);
        lsnDu = WorkflowAPI.getInstance().findNDUsByNodeId(nTn.getNextId(), false);
        lbcbxl.clearSelection();
        lbcbxl.setModel(new ListModelList<>(lsnDu));
        lbcbxl.renderAll();
        lbcbxl.setSelectedIndex(0);
    }

    @Listen("onSelect =  #lbcbxl")
    public void onSelectCb() {
        idxcb = lbcbxl.getSelectedIndex();
    }

    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    private void clearWarningMessage() {
        lbBottomWarning.setValue("");
    }
}
