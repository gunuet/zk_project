package com.viettel.module.importDevice.Controller.publicDevice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.event.PagingEvent;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.importDevice.DAO.ListImportDeviceDAO;
import com.viettel.module.importDevice.Model.SearchDeviceModel;
import com.viettel.module.importDevice.Model.VFileImportDeviceModel;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Linhdx
 */
public class ListImportControler extends BaseComposer {

    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #dbFromDayModify")
    private Datebox dbFromDayModify;
    @Wire("#incSearchFullForm #dbToDayModify")
    private Datebox dbToDayModify;
    @Wire("#incSearchFullForm #tbCompanyName")
    private Textbox tbCompanyName;
    @Wire("#incSearchFullForm #tbCompanyAddress")
    private Textbox tbCompanyAddress;
    @Wire("#incSearchFullForm #tbResponsiblePersonName")
    private Textbox tbResponsiblePersonName;
    @Wire("#incSearchFullForm #tbBusinessTaxCode")
    private Textbox tbBusinessTaxCode;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #tbImportName")
    private Textbox tbImportName;
    @Wire("#incSearchFullForm #tbEquipmentNo")
    private Textbox tbEquipmentNo;
    @Wire("#incSearchFullForm #lbDocumentTypeCode")
    private Listbox lbDocumentTypeCode;
    @Wire("#incSearchFullForm #lbImportCategoryProduct")
    private Listbox lbImportCategoryProduct;
    @Wire("#incSearchFullForm #tbProductName")
    private Textbox tbProductName;
    @Wire("#incSearchFullForm #tbPermitNumber")
    private Textbox tbPermitNumber;
    @Wire("#incSearchFullForm #dbFromDayPermit")
    private Datebox dbFromDayPermit;
    @Wire("#incSearchFullForm #dbToDayPermit")
    private Datebox dbToDayPermit;
    @Wire("#incSearchFullForm #tbModel")
    private Textbox tbModel;
    @Wire("#incSearchFullForm #tbManufacturer")
    private Textbox tbManufacturer;
    @Wire("#incSearchFullForm #tbNationName")
    private Textbox tbNationName;
    @Wire("#incSearchFullForm #tbDistributor")
    private Textbox tbDistributor;
    @Wire("#incSearchFullForm #tbNationalDistributorName")
    private Textbox tbNationalDistributorName;
    @Wire("#incSearchFullForm #tbProductManufacture")
    private Textbox tbProductManufacture;
    @Wire("#incSearchFullForm #tbNationalProduct")
    private Textbox tbNationalProduct;
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbList")
    private Listbox lbList;
    List<NodeToNode> lstNextAction;
    private VFileImportDeviceModel lastSearchModel;
    Long UserId;
    CategoryDAOHE dao = new CategoryDAOHE();
    private Constants constants = new Constants();
    String menuTypeStr;
    private SearchDeviceModel sm;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        menuTypeStr = "processing";
        lastSearchModel = new VFileImportDeviceModel();
        lastSearchModel.setMenuTypeStr(menuTypeStr);
        if (lbImportCategoryProduct != null) {
            List<Category> listImportFileType = new CategoryDAOHE().findAllCategory(Constants.IMPORT_TYPE.IMPORT_PRODUCT_CATEGORY);
            ListModelArray lstCategoryModel = new ListModelArray(listImportFileType);
            lbImportCategoryProduct.setModel(lstCategoryModel);
            lbImportCategoryProduct.renderAll();
            lbImportCategoryProduct.setSelectedIndex(0);
        }
        dbFromDayModify.setValue(null);
        lastSearchModel.setModifyDateFrom(null);
        dbToDayModify.setValue(new Date());
        lastSearchModel.setModifyDateTo(new Date());
        onSearch();
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() throws UnsupportedEncodingException {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        if (dbFromDay != null && dbToDay != null
                && dbFromDay.getValue() != null && dbToDay.getValue() != null) {
            if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                showNotification(
                        "Thời gian từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                return;
            }
        }
        if (dbFromDayModify != null && dbToDayModify != null
                && dbFromDayModify.getValue() != null
                && dbToDayModify.getValue() != null) {
            if (dbFromDayModify.getValue().compareTo(dbToDayModify.getValue()) > 0) {
                showNotification(
                        "Thời gian từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                return;
            }
        }
        if (dbFromDayPermit != null && dbToDayPermit != null
                && dbFromDayPermit.getValue() != null
                && dbToDayPermit.getValue() != null) {
            if (dbFromDayPermit.getValue().compareTo(dbToDayPermit.getValue()) > 0) {
                showNotification(
                        "Ngày cấp phép : từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                dbFromDayPermit.focus();
                return;
            }
        }
        lastSearchModel.setBusinessName(tbCompanyName == null ? null
                : tbCompanyName.getValue());
        lastSearchModel.setBusinessAddress(tbCompanyAddress == null ? null
                : tbCompanyAddress.getValue());
        lastSearchModel.setResponsiblePersonName(tbResponsiblePersonName == null ? null
                : tbResponsiblePersonName.getValue());
        lastSearchModel.setTaxCode(tbBusinessTaxCode == null ? null
                : tbBusinessTaxCode.getValue());
        lastSearchModel.setCreateDateFrom(dbFromDay == null ? null : dbFromDay.getValue());
        lastSearchModel.setCreateDateTo(dbToDay == null ? null : dbToDay.getValue());
        lastSearchModel.setModifyDateFrom(dbFromDayModify == null ? null
                : dbFromDayModify.getValue());
        lastSearchModel.setModifyDateTo(dbToDayModify == null ? null
                : dbToDayModify.getValue());
        lastSearchModel.setImporterName(tbImportName.getValue());
        lastSearchModel.setEquipmentNo(tbEquipmentNo.getValue());
        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        Long documentTypeCode = Long.valueOf(lbDocumentTypeCode.getSelectedItem().getValue().toString());
        lastSearchModel.setDocumentTypeCode(documentTypeCode);
        String groupProductId = null;
        if (lbImportCategoryProduct.getSelectedIndex() > 0) {
            groupProductId = lbImportCategoryProduct.getSelectedItem().getValue().toString();
        }
        lastSearchModel.setGroupProductId(groupProductId);
        lastSearchModel.setProductName(tbProductName.getValue());
        lastSearchModel.setPermitNumber(tbPermitNumber.getValue());
        lastSearchModel.setPermitDateFrom(dbFromDayPermit == null ? null
                : dbFromDayPermit.getValue());
        lastSearchModel.setPermitDateTo(dbToDayPermit == null ? null
                : dbToDayPermit.getValue());
        if (tbModel != null) {
            lastSearchModel.setModel(tbModel.getValue());
        }
        if (tbManufacturer != null) {
            lastSearchModel.setManufacturer(tbManufacturer.getValue());
        }
        if (tbNationName != null) {
            lastSearchModel.setNationalName(tbNationName.getValue());
        }
        if (tbDistributor != null) {
            lastSearchModel.setDistributor(tbDistributor.getValue());
        }
        if (tbNationalDistributorName != null) {
            lastSearchModel.setNationalDistributorName(tbNationalDistributorName.getValue());
        }
        if (tbProductManufacture != null) {
            lastSearchModel.setProductManufacture(tbProductManufacture.getValue());
        }
        if (tbNationalProduct != null) {
            lastSearchModel.setNationalProduct(tbNationalProduct.getValue());
        }
        userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        reloadModel(lastSearchModel);
    }

    private void reloadModel(SearchDeviceModel searchModel) throws UnsupportedEncodingException {
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm;
        ListImportDeviceDAO objDAOHE = new ListImportDeviceDAO();
        plm = objDAOHE.findFilesByDeptMis(searchModel, Long.valueOf(ResourceBundleUtil.getString("USER_ID_VTB", "config")),
                start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbList.setModel(lstModel);
        lbList.renderAll();
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) throws UnsupportedEncodingException {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    public String getSendDate(VFileImportDevice f) {
        SimpleDateFormat formatterDateTime = new SimpleDateFormat("dd-MM-yyyy");
        String rs = f.getStartDate() == null ? formatterDateTime.format(f.getCreateDate()) : formatterDateTime.format(f.getStartDate());
        if (sm != null) {
            ProcessDAOHE pDAO = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process p = pDAO.getLast(
                    f.getFileId());
            if (p != null) {
                rs = formatterDateTime.format(p.getSendDate());
            }
        }
        return rs;
    }

    public String getStartDate(VFileImportDevice f) {
        SimpleDateFormat formatterDateTime = new SimpleDateFormat("dd-MM-yyyy");
        String rs = f.getStartDate() == null ? formatterDateTime.format(f.getCreateDate()) : formatterDateTime.format(f.getStartDate());
        return rs;
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public Constants getConstants() {
        return constants;
    }

    public void setConstants(Constants constants) {
        this.constants = constants;
    }

    public String getDocumentTypeCode(Long documentTypeCode) {
        if (documentTypeCode != null) {
            if (Objects.equals(documentTypeCode, Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI)) {
                return Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI_STR;
            }
        }
        return "";
    }
    
    public String getTypeCreate(Long typeCreate) {
        if (typeCreate != null) {
            if (Objects.equals(typeCreate, Constants.IMPORT.TYPE_CREATE_MOI)) {
                return Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI_STR;
            } else if(Objects.equals(typeCreate, Constants.IMPORT.TYPE_CREATE_DIEUCHINH)) {
                return Constants.IMPORT.DOCUMENT_TYPE_CODE_DIEUCHINH_STR;
            } else if(Objects.equals(typeCreate, Constants.IMPORT.TYPE_CREATE_GIAHAN)) {
                return Constants.IMPORT.DOCUMENT_TYPE_GIAHAN_STR;
            }
        }
        return "";
    }

    @Listen("onDownLoadGiayPhep = #incList #lbList")
    public void onDownLoadGiayPhep(Event event) throws FileNotFoundException,
            IOException {
        VFileImportDevice obj = (VFileImportDevice) event.getData();
        Long fileId = obj.getFileId();
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttachVTB = rDaoHe.getByObjectIdAndAttachCatAndAttachType(
                fileId,
                Constants.OBJECT_TYPE.COSMETIC_PERMIT,
                Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_LDB_VTDK);
        if (lstAttachVTB.size() > 0) {
            Attachs attach = lstAttachVTB.get(0);
            String path = attach.getFullPathFile();
            File f = new File(path);
            if (f.exists()) {
                File tempFile = FileUtil.createTempFile(f,
                        attach.getAttachName());
                Filedownload.save(tempFile, path);
            }
        } else {
            showNotification("File không còn tồn tại trên hệ thống!");
        }

    }

}
