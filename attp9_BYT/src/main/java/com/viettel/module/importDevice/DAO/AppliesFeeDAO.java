/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.AppliesFee;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author E5420
 */
public class AppliesFeeDAO
        extends GenericDAOHibernate<AppliesFee, Long> {

    public AppliesFeeDAO() {
        super(AppliesFee.class);
    }

    @Override
    public void saveOrUpdate(AppliesFee imDePro) {
        if (imDePro != null) {
            super.saveOrUpdate(imDePro);
        }

        getSession().flush();
    }

    public PagingListModel getAppliesFeeList(int start, int take,Long isActive, String feeType,String amountFee, Long importDeviceFileId) {
        try {
            List listParam = new ArrayList();
            StringBuilder strQuery = new StringBuilder(" from AppliesFee a where a.isDeleted = 0");

            if(isActive != null){
                strQuery.append(" and a.is_Active = ?");
                listParam.add(isActive);
            }
            if (amountFee != null) {
                strQuery.append(" and a.amountFee = ?");
                listParam.add(amountFee);
            }
            if (feeType != null && !"".equals(feeType)) {
                strQuery.append(" and a.feeType like ?");
                listParam.add("%" + feeType + "%");
            }

            if (importDeviceFileId != null) {
                strQuery.append(" and a.importDeviceFileId = ?");
                listParam.add(importDeviceFileId);
            }
            StringBuilder hqlQuery = new StringBuilder("select a ").append(strQuery);
            StringBuilder hqlCountQuery = new StringBuilder("select count(a) ").append(strQuery);
            Query query = session.createQuery(hqlQuery.toString());
            Query countQuery = session.createQuery(hqlCountQuery.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }
            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }
            List result = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(result, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public AppliesFee findAppliesFeeById(Long id) {
        try {
            StringBuilder strQuery = new StringBuilder(" select a from AppliesFee a where a.isDeleted = 0 and a.id = ?");
            Query query = session.createQuery(strQuery.toString());
            query.setParameter(0, id);
            AppliesFee newInter = new AppliesFee();
            if (query.list() != null  && query.list().size() > 0) {
                newInter = (AppliesFee) query.list().get(0);
            }
            return newInter;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
}
