/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.HealEquipmentCerfiticate;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author E5420
 */
public class HealEquipmentCerfiticateDAO
        extends GenericDAOHibernate<HealEquipmentCerfiticate, Long> {

    public HealEquipmentCerfiticateDAO() {
        super(HealEquipmentCerfiticate.class);
    }

    @Override
    public void saveOrUpdate(HealEquipmentCerfiticate imDePro) {
        if (imDePro != null) {
            super.saveOrUpdate(imDePro);
        }

        getSession().flush();
    }

    public PagingListModel getReceiptPaymentList(int start, int take, String cerfiticateNumber, String healEquimentName,String attachName) {
        try {
            List listParam = new ArrayList();
            StringBuilder strQuery = new StringBuilder(" from HealEquipmentCerfiticate a where a.isActive = 1");

            if (cerfiticateNumber != null && !"".equals(cerfiticateNumber)) {
                strQuery.append(" and a.cerfiticateNumber like ?");
                listParam.add("%" + cerfiticateNumber + "%");
            }
            if (healEquimentName != null && !"".equals(healEquimentName)) {
                strQuery.append(" and a.healEquimentName like ?");
                listParam.add("%" + healEquimentName + "%");
            }
            
            if (attachName != null && !"".equals(attachName)) {
                strQuery.append(" and a.attachName like ?");
                listParam.add("%" + attachName + "%");
            }
            
            StringBuilder hqlQuery = new StringBuilder("select a ").append(strQuery);
            StringBuilder hqlCountQuery = new StringBuilder("select count(a) ").append(strQuery);
            Query query = session.createQuery(hqlQuery.toString());
            Query countQuery = session.createQuery(hqlCountQuery.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }
            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }
            List result = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(result, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public HealEquipmentCerfiticate findHealEquipmentCerfiticateById(Long id) {
        try {
            StringBuilder strQuery = new StringBuilder(" select a from HealEquipmentCerfiticate a where a.isActive = 1 and a.id = ?");
            Query query = session.createQuery(strQuery.toString());
            query.setParameter(0, id);
            HealEquipmentCerfiticate newInter = new HealEquipmentCerfiticate();
            if (query.list() != null  && query.list().size() > 0) {
                newInter = (HealEquipmentCerfiticate) query.list().get(0);
            }
            return newInter;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
}
