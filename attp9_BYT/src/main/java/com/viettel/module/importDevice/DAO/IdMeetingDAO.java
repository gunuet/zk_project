/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.IdMeeting;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author E5420
 */
public class IdMeetingDAO
        extends GenericDAOHibernate<IdMeeting, Long> {

    public IdMeetingDAO() {
        super(IdMeeting.class);
    }

    @Override
    public void saveOrUpdate(IdMeeting imDePro) {
        if (imDePro != null) {
            super.saveOrUpdate(imDePro);
        }

        getSession().flush();
    }

    public IdMeeting getLastActive() {
        Query query = getSession().createQuery("select a from IdMeeting a where a.status = 0 order by a.day desc");
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (IdMeeting) result.get(0);
        }
    }
    

    public PagingListModel searchIdMeeting(IdMeeting searchForm, int start, int take) {
        List listParam = new ArrayList();
        List listParamName = new ArrayList();
        SimpleDateFormat fm = new SimpleDateFormat("dd/MM/yyyy");
        try {
            StringBuilder strBuf = new StringBuilder("Select a from IdMeeting a where a.status = 1 ");
            StringBuilder strCountBuf = new StringBuilder("select count(a) from IdMeeting a where a.status = 1 ");
            StringBuilder hql = new StringBuilder();
            if (searchForm != null) {
                if (searchForm.getDay() != null) {
                    hql.append(" and to_char(a.day,'dd/MM/yyyy')=:day ");
                    listParam.add(fm.format(searchForm.getDay()));
                    listParamName.add("day");
                }
                if (StringUtils.validString(searchForm.getPlace())) {
                    hql.append(" and lower(a.place) LIKE :place escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getPlace()));
                    listParamName.add("place");
                }
            }
            strBuf.append(hql);
            strCountBuf.append(hql);
            strBuf.append(" order by a.books ");
            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(listParamName.get(i).toString(), listParam.get(i));
                countQuery.setParameter(listParamName.get(i).toString(), listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public Long getCountMeetingTtb() {
        String strCountBuf = "select count(a) from IdMeeting a where a.status = 1 ";
        Query countQuery = session.createQuery(strCountBuf);
        Long count = (Long) countQuery.uniqueResult();
        return count;

    }
    
    public void updateToStatusName(Long id, String statusName){
        List listParam = new ArrayList();
        try{
            StringBuilder strBuf = new StringBuilder("update IdMeeting a set a.statusDocName = ? where a.status = 1 and a.id = ? ");
            listParam.add(statusName);
            listParam.add(id);
            Query query = session.createQuery(strBuf.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));                
            }
            query.executeUpdate();
        }catch(Exception ex){
            LogUtils.addLogDB(ex);
        }
    }
}
