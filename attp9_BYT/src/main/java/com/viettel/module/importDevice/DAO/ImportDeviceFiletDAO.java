/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.voffice.BO.Files;

/**
 *
 * @author Linhdx
 */
public class ImportDeviceFiletDAO extends GenericDAOHibernate<ImportDeviceFile, Long> {

    public ImportDeviceFiletDAO() {
        super(ImportDeviceFile.class);
    }

    public void delete(Long id) {
        ImportDeviceFile obj = findById(id);
        delete(obj);
//        obj.setIsActive(0l); 
//        update(obj);
    }

    @Override
    public void saveOrUpdate(ImportDeviceFile imDePro) {
        if (imDePro != null) {
            super.saveOrUpdate(imDePro);
        }

        getSession().flush();

    }

    public VFileImportDevice findViewByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from VFileImportDevice a where a.fileId = :fileId");
        query.setParameter("fileId", fileId);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFileImportDevice) result.get(0);
        }
    }

    public Long countImportfile() {
        Query query = getSession().createQuery("select count(a) from ImportDeviceFile a");
        Long count = (Long) query.uniqueResult();
        return count;
    }

    public Long countEquipmentNoImportfile(String equipmentNo) {
        StringBuilder stringBuilder = new StringBuilder(" select count(a) from ImportDeviceFile a ");
        stringBuilder.append("  where a.equipmentNo = ? ");
        Query query = getSession().createQuery(stringBuilder.toString());
        query.setParameter(0, equipmentNo);
        Long count = (Long) query.uniqueResult();
        return count;
    }

    public ImportDeviceFile findByFileId(Long id) {
        Query query = getSession().getNamedQuery(
                "ImportDeviceFile.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ImportDeviceFile) result.get(0);
        }
    }

    public ImportDeviceFile getFileById(Long id) {
        Query query = getSession().getNamedQuery(
                "ImportDeviceFile.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ImportDeviceFile) result.get(0);
        }
    }

    public Files getFileByFileId(Long fileId) {
        String hql = "select f from Files f where f.fileId = ?";
        Query query = session.createQuery(hql);
        query.setParameter(0, fileId);
        List<Files> lstFile = query.list();
        if (lstFile != null && lstFile.size() > 0) {
            return lstFile.get(0);
        }
        return null;
    }

    public ImportDeviceFile getFileBySendId(Long sendId) {
        String hql = "select f from ImportDeviceFile f where f.sendId = ? order by importDeviceFileId desc";
        Query query = session.createQuery(hql);
        query.setParameter(0, sendId);
        List<ImportDeviceFile> lstFile = query.list();
        if (lstFile != null && lstFile.size() > 0) {
            return lstFile.get(0);
        }
        return null;
    }
}
