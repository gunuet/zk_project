/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import com.viettel.module.cosmetic.DAO.*;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;

import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;

import com.viettel.voffice.model.SearchModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class ImportDeviceProductDAO extends GenericDAOHibernate<ImportDeviceProduct, Long> {

    public ImportDeviceProductDAO() {
        super(ImportDeviceProduct.class);
    }

    @Override
    public void saveOrUpdate(ImportDeviceProduct imDePro) {
        if (imDePro != null) {
            super.saveOrUpdate(imDePro);
        }

        getSession().flush();

    }
    
    public List findAllIdByFileId(Long importFileId) {
        List<ImportDeviceProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" select p.productId from ImportDeviceProduct p ");
            stringBuilder.append("  where p.fileId = ? "
                    + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, importFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }
    public void setIsTempProduct(Long newfileId, Long oldfileId) {
        Query query = getSession()
                .createQuery(
                        "update ImportDeviceProduct set fileId = :newfileId where fileId = :oldfileId");
        query.setParameter("newfileId", newfileId);
        query.setParameter("oldfileId", oldfileId);
        query.executeUpdate();
    }
    public List findByImportFileId(Long importFileId) {
        List<ImportDeviceProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from ImportDeviceProduct a ");
            stringBuilder.append("  where  a.fileId = ? "
                    + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, importFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public void delete(Long id) {
        ImportDeviceProduct obj = findById(id);
        delete(obj);
    }

}
