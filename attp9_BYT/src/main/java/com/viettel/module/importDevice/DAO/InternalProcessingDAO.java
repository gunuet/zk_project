/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.InternalProcessing;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author E5420
 */
public class InternalProcessingDAO
        extends GenericDAOHibernate<InternalProcessing, Long> {

    public InternalProcessingDAO() {
        super(InternalProcessing.class);
    }

    @Override
    public void saveOrUpdate(InternalProcessing imDePro) {
        if (imDePro != null) {
            super.saveOrUpdate(imDePro);
        }

        getSession().flush();
    }

    public PagingListModel getInternalProcessList(int start, int take, String content, Date createDate, Long meetingId) {
        try {
            List listParam = new ArrayList();
            StringBuilder strQuery = new StringBuilder(" from InternalProcessing a where a.is_Active = 1");

            if (content != null && !"".equals(content)) {
                strQuery.append(" and a.content like ?");
                listParam.add(content);
            }
            if (createDate != null) {
                strQuery.append(" and a.createDate = ?");
                listParam.add(createDate);
            }

            if (meetingId != null) {
                strQuery.append(" and a.meetingId = ?");
                listParam.add(meetingId);
            }
            StringBuilder hqlQuery = new StringBuilder("select a ").append(strQuery);
            StringBuilder hqlCountQuery = new StringBuilder("select count(a) ").append(strQuery);
            Query query = session.createQuery(hqlQuery.toString());
            Query countQuery = session.createQuery(hqlCountQuery.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }
            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }
            List result = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(result, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public InternalProcessing findInternalProcessById(Long id) {
        try {
            StringBuilder strQuery = new StringBuilder(" select a from InternalProcessing a where a.is_Active = 1 and a.id = ?");
            Query query = session.createQuery(strQuery.toString());
            query.setParameter(0, id);
            InternalProcessing newInter = new InternalProcessing();
            if (query.list() != null  && query.list().size() > 0) {
                newInter = (InternalProcessing) query.list().get(0);
            }
            return newInter;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
}
