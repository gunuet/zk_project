/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.importDevice.Model.SearchDeviceModel;
import com.viettel.module.importDevice.Model.VFileImportDeviceModel;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;

/**
 *
 * @author Linhdx
 */
public class ListImportDeviceDAO extends GenericDAOHibernate<CosFile, Long> {

    public ListImportDeviceDAO() {
        super(CosFile.class);
    }

    @Override
    public void saveOrUpdate(CosFile cosmetic) {
        if (cosmetic != null) {
            super.saveOrUpdate(cosmetic);
        }

        getSession().flush();

    }

    @Override
    public CosFile findById(Long id) {
        Query query = getSession().getNamedQuery(
                "VFileImportDevice.findByImportDeviceFileId");
        query.setParameter("importDeviceFileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (CosFile) result.get(0);
        }
    }

    public Files getFileById(Long fileId) {
        String hql = "select f from Files f where f.fileId = ?";
        Query query = session.createQuery(hql);
        query.setParameter(0, fileId);
        List<Files> lstFile = query.list();
        if (lstFile != null && lstFile.size() > 0) {
            return lstFile.get(0);
        }
        return null;
    }

    public Long countCosfile() {
        Query query = getSession().createQuery(
                "select count(a) from VFileImportDevice a");
        Long count = (Long) query.uniqueResult();
        return count;
    }

    public VFileImportDevice findViewByFileId(Long fileId) {
        Query query = getSession().createQuery(
                "select a from VFileImportDevice a where a.fileId = :fileId");
        query.setParameter("fileId", fileId);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFileImportDevice) result.get(0);
        }
    }

    public VFileImportDevice findByFileId(Long id) {
        Query query = getSession().createQuery(
                "Select v from VFileImportDevice v where v.fileId = :fileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFileImportDevice) result.get(0);
        }
    }

    public void delete(VFileImportDevice vCosFile) {
        // Files findByFileId(vCosFile.getFileId());
        // cosmetic.setIsActive(Constants.Status.INACTIVE);
        // getSession().saveOrUpdate(cosmetic);
    }

    public List getList(Long deptId, SearchDeviceModel model, int activePage,
            int pageSize, boolean getSize) {
        StringBuilder hql;

        if (getSize) {
            hql = new StringBuilder(
                    "SELECT COUNT(d) FROM VFileImportDevice d WHERE isActive = 1 ");
        } else {
            hql = new StringBuilder(
                    "SELECT d FROM VFileImportDevice d WHERE isActive = 1 ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        if (model != null) {
            // todo:fill query
            if (model.getCreateDateFrom() != null) {
                hql.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                hql.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null
                    && model.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(d.nswFileCode) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getEquipmentNo() != null
                    && model.getEquipmentNo().trim().length() > 0) {
                hql.append(" AND lower(d.equipmentNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getEquipmentNo()));
            }
            if (model.getStatus() != null) {
                hql.append(" AND d.status = ?");
                docParams.add(model.getStatus());
            }
            // if (model.getEquipmentNo()!= null) {
            // hql.append(" AND d.documentTypeCode = ?");
            // docParams.add(model.getEquipmentNo());
            // }
        }

        hql.append(" order by fileId desc");

        Query fileQuery = session.createQuery(hql.toString());
        List<CosFile> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    public PagingListModel findFilesByReceiverAndDeptId(
            SearchDeviceModel searchModel, Long receiverId, Long receiveDeptId,
            int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct n ");
        StringBuilder strCountBuf = new StringBuilder(
                "select count(distinct n.fileId) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    //strBuf = new StringBuilder("SELECT n ");
                    hql.append("  FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.sendUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileImportDevice n WHERE "
                            + "n.finishDate is not null"
                    //  + " n.fileId = p.objectId AND "
                    // + " n.fileType = p.objectType AND "
                    // + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                    // + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                    // + " AND no.type = ? " + " ) AND "
                    // + " 1 = 1 ");
                    //+ " p.sendUserId = ? "
                    );
                    // listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    // listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status "
                    // + " (p.receiveUserId is null AND "
                    //+ " AND p.receiveGroupId = ?  "
                    );
                    // listParam.add(receiveDeptId);
                    break;
                case Constants.MENU_TYPE.DELETE_PERMIT:
                    hql.append(" FROM VFileImportDevice n WHERE"
                            + " n.finishDate is not null AND "
                            + " n.status = 31000063 or n.status = 31000064"
                    );
                    break;
                //linhdx 20160517 them chuc nang theo doi san pham
                case Constants.MENU_TYPE.DEPT_PRODUCT_PROCESS_STR:
                    strCountBuf = new StringBuilder("select count(n.fileId) ");
                    strBuf = new StringBuilder("select new com.viettel.module.importDevice.Model.VFileImportDeviceModel(n,a)");
                    hql.append("  FROM VFileImportDevice n, ImportDeviceProduct a, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " n.fileId = a.fileId "
                    );
                    break;

                default:
                    hql.append("  FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            // + " n.status = p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }
        //linhdx them dieu kien tim kiem is_active = 1 va is_temp=0

        hql.append(" AND n.isActive = 1 and n.isTemp=0  ");
        // listParam.add(receiverId);
        // listParam.add(receiveDeptId);
        if (searchModel != null) {

            // todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel
                        .getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel != null && !Constants.MENU_TYPE.PROCESSED_STR.equals(searchModel.getMenuTypeStr()) && !Constants.MENU_TYPE.DELETE_PERMIT.equals(searchModel.getMenuTypeStr())) {
                if (searchModel.getModifyDateFrom() != null) {
                    hql.append(" AND p.sendDate >= ? ");
                    listParam.add(searchModel.getModifyDateFrom());
                }
                if (searchModel.getModifyDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getModifyDateTo());
                    hql.append(" AND p.sendDate < ? ");
                    listParam.add(toDate);
                }
            } else {
                if (searchModel.getModifyDateFrom() != null) {
                    hql.append(" AND n.createDate >= ? ");
                    listParam.add(searchModel.getModifyDateFrom());
                }
                if (searchModel.getModifyDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getModifyDateTo());
                    hql.append(" AND n.createDate < ? ");
                    listParam.add(toDate);
                }
            }
            if (searchModel.getnSWFileCode() != null
                    && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel
                        .getnSWFileCode()));
            }
            if (searchModel.getEquipmentNo() != null
                    && searchModel.getEquipmentNo().trim().length() > 0) {
                hql.append(" AND lower(n.equipmentNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel
                        .getEquipmentNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }

            if (searchModel instanceof VFileImportDeviceModel) {
                VFileImportDeviceModel cosmeticFileModel = (VFileImportDeviceModel) searchModel;

                if (cosmeticFileModel.getImporterName() != null
                        && cosmeticFileModel.getImporterName().trim().length() > 0) {
                    hql.append(" AND lower(n.importerName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getImporterName()));
                }
                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null
                        && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null
                        && cosmeticFileModel.getBusinessAddress().trim()
                        .length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getBusinessAddress()));
                }

                if (cosmeticFileModel.getTaxCode() != null
                        && cosmeticFileModel.getTaxCode().trim().length() > 0) {
                    hql.append(" AND lower(n.taxCode) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getTaxCode()));
                }

                //linhdx 20160506
                if (cosmeticFileModel.getDocumentTypeCode() != null
                        && cosmeticFileModel.getDocumentTypeCode() != 0) {
                    Long typeTaomoi = 1L;
                    if(typeTaomoi.equals(cosmeticFileModel.getDocumentTypeCode())){
                        hql.append(" AND (n.typeCreate= ? OR n.typeCreate is null) "); 
                    } else{
                       hql.append(" AND n.typeCreate= ? "); 
                    }
                    listParam.add(cosmeticFileModel.getDocumentTypeCode());
                }

                if (cosmeticFileModel.getGroupProductId() != null && !"-1".equals(cosmeticFileModel.getGroupProductId())
                        && cosmeticFileModel.getGroupProductId().trim().length() > 0) {
                    hql.append(" AND n.groupProductId = ? ");
                    listParam.add(cosmeticFileModel
                            .getGroupProductId().trim());
                }

                if (cosmeticFileModel.getProductName() != null
                        && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getProductName()));
                }

                if (cosmeticFileModel.getPermitNumber() != null
                        && cosmeticFileModel.getPermitNumber().trim().length() > 0) {
                    hql.append(" AND lower(n.permitNumber) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getPermitNumber()));
                }

                if (cosmeticFileModel.getPermitDateFrom() != null) {
                    hql.append(" AND n.permitDate >= ? ");
                    listParam.add(cosmeticFileModel.getPermitDateFrom());
                }
                if (cosmeticFileModel.getPermitDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(cosmeticFileModel.getPermitDateTo());
                    hql.append(" AND n.permitDate < ? ");
                    listParam.add(toDate);
                }

                if (cosmeticFileModel.getModel() != null && cosmeticFileModel.getModel().trim().length() > 0) {
                    hql.append(" AND lower(a.model) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getModel()));
                }

                if (cosmeticFileModel.getManufacturer() != null && cosmeticFileModel.getManufacturer().trim().length() > 0) {
                    hql.append(" AND lower(a.manufacturer) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getManufacturer()));
                }
                if (cosmeticFileModel.getNationalName() != null && cosmeticFileModel.getNationalName().trim().length() > 0) {
                    hql.append(" AND lower(a.nationalName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getNationalName()));
                }

                if (cosmeticFileModel.getDistributor() != null && cosmeticFileModel.getDistributor().trim().length() > 0) {
                    hql.append(" AND lower(a.distributor) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getDistributor()));
                }
                if (cosmeticFileModel.getNationalDistributorName() != null && cosmeticFileModel.getNationalDistributorName().trim().length() > 0) {
                    hql.append(" AND lower(a.nationalDistributorName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getNationalDistributorName()));
                }

                if (cosmeticFileModel.getProductManufacture() != null && cosmeticFileModel.getProductManufacture().trim().length() > 0) {
                    hql.append(" AND lower(a.productManufacture) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getProductManufacture()));
                }

                if (cosmeticFileModel.getNationalProduct() != null && cosmeticFileModel.getNationalProduct().trim().length() > 0) {
                    hql.append(" AND lower(a.nationalProduct) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getNationalProduct()));
                }

            }

        }
        strBuf.append(hql);
        strCountBuf.append(hql);

        strBuf.append(" order by n.nswFileCode asc ");

//        if (null != searchModel.getMenuTypeStr() && (searchModel.getMenuTypeStr().equals(Constants.MENU_TYPE.WAITPROCESS_STR))) {
//            strBuf.append(" order by n.modifyDate desc");
//        } else {
//            strBuf.append(" order by n.modifyDate desc");
//        }
        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    //Cuongvv lay thong tin van thu bo
    public PagingListModel findFilesByDeptMis(
            SearchDeviceModel searchModel, Long receiveUserId,
            int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct n ");
        StringBuilder strCountBuf = new StringBuilder(
                "select count(distinct n.fileId) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    // strBuf = new StringBuilder("SELECT n ");
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.receiveUserId = ? AND "
                            + " p.finishDate is null ");
                    listParam.add(receiveUserId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.receiveUserId = ? AND "
                            + " p.finishDate is not null ");
                    listParam.add(receiveUserId);
                    break;
                default:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.receiveUserId = ? AND "
                            + " p.finishDate is not null");
                    listParam.add(receiveUserId);
                    break;
            }
        }

        // listParam.add(receiverId);
        // listParam.add(receiveDeptId);
        if (searchModel != null) {

            // todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel
                        .getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel != null && !Constants.MENU_TYPE.PROCESSED_STR.equals(searchModel.getMenuTypeStr()) && !Constants.MENU_TYPE.DELETE_PERMIT.equals(searchModel.getMenuTypeStr())) {
                if (searchModel.getModifyDateFrom() != null) {
                    hql.append(" AND p.sendDate >= ? ");
                    listParam.add(searchModel.getModifyDateFrom());
                }
                if (searchModel.getModifyDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getModifyDateTo());
                    hql.append(" AND p.sendDate < ? ");
                    listParam.add(toDate);
                }
            } else {
                if (searchModel.getModifyDateFrom() != null) {
                    hql.append(" AND n.createDate >= ? ");
                    listParam.add(searchModel.getModifyDateFrom());
                }
                if (searchModel.getModifyDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getModifyDateTo());
                    hql.append(" AND n.createDate < ? ");
                    listParam.add(toDate);
                }
            }
            if (searchModel.getnSWFileCode() != null
                    && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel
                        .getnSWFileCode()));
            }
            if (searchModel.getEquipmentNo() != null
                    && searchModel.getEquipmentNo().trim().length() > 0) {
                hql.append(" AND lower(n.equipmentNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel
                        .getEquipmentNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }

            if (searchModel instanceof VFileImportDeviceModel) {
                VFileImportDeviceModel cosmeticFileModel = (VFileImportDeviceModel) searchModel;

                if (cosmeticFileModel.getImporterName() != null
                        && cosmeticFileModel.getImporterName().trim().length() > 0) {
                    hql.append(" AND lower(n.importerName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getImporterName()));
                }
                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null
                        && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null
                        && cosmeticFileModel.getBusinessAddress().trim()
                        .length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getBusinessAddress()));
                }

                if (cosmeticFileModel.getTaxCode() != null
                        && cosmeticFileModel.getTaxCode().trim().length() > 0) {
                    hql.append(" AND lower(n.taxCode) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getTaxCode()));
                }

                //linhdx 20160506
                if (cosmeticFileModel.getDocumentTypeCode() != null
                        && cosmeticFileModel.getDocumentTypeCode() != 0) {
                    hql.append(" AND n.documentTypeCode= ? ");
                    listParam.add(cosmeticFileModel.getDocumentTypeCode());
                }

                if (cosmeticFileModel.getGroupProductId() != null && !"-1".equals(cosmeticFileModel.getGroupProductId())
                        && cosmeticFileModel.getGroupProductId().trim().length() > 0) {
                    hql.append(" AND lower(n.groupProductId) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getGroupProductId()));
                }

                if (cosmeticFileModel.getProductName() != null
                        && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getProductName()));
                }

                if (cosmeticFileModel.getPermitNumber() != null
                        && cosmeticFileModel.getPermitNumber().trim().length() > 0) {
                    hql.append(" AND lower(n.permitNumber) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getPermitNumber()));
                }

                if (cosmeticFileModel.getPermitDateFrom() != null) {
                    hql.append(" AND n.permitDate >= ? ");
                    listParam.add(cosmeticFileModel.getPermitDateFrom());
                }
                if (cosmeticFileModel.getPermitDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(cosmeticFileModel.getPermitDateTo());
                    hql.append(" AND n.permitDate < ? ");
                    listParam.add(toDate);
                }

                if (cosmeticFileModel.getModel() != null && cosmeticFileModel.getModel().trim().length() > 0) {
                    hql.append(" AND lower(a.model) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getModel()));
                }

                if (cosmeticFileModel.getManufacturer() != null && cosmeticFileModel.getManufacturer().trim().length() > 0) {
                    hql.append(" AND lower(a.manufacturer) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getManufacturer()));
                }
                if (cosmeticFileModel.getNationalName() != null && cosmeticFileModel.getNationalName().trim().length() > 0) {
                    hql.append(" AND lower(a.nationalName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getNationalName()));
                }

                if (cosmeticFileModel.getDistributor() != null && cosmeticFileModel.getDistributor().trim().length() > 0) {
                    hql.append(" AND lower(a.distributor) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getDistributor()));
                }
                if (cosmeticFileModel.getNationalDistributorName() != null && cosmeticFileModel.getNationalDistributorName().trim().length() > 0) {
                    hql.append(" AND lower(a.nationalDistributorName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getNationalDistributorName()));
                }

                if (cosmeticFileModel.getProductManufacture() != null && cosmeticFileModel.getProductManufacture().trim().length() > 0) {
                    hql.append(" AND lower(a.productManufacture) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getProductManufacture()));
                }

                if (cosmeticFileModel.getNationalProduct() != null && cosmeticFileModel.getNationalProduct().trim().length() > 0) {
                    hql.append(" AND lower(a.nationalProduct) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getNationalProduct()));
                }
            }

        }
        strBuf.append(hql);
        strCountBuf.append(hql);
        if (null != searchModel.getMenuTypeStr() && (searchModel.getMenuTypeStr().equals(Constants.MENU_TYPE.WAITPROCESS_STR) || searchModel.getMenuTypeStr().equals(Constants.MENU_TYPE.DEPT_PROCESS_STR))) {
            //strBuf.append(" order by p.sendDate");
            strBuf.append(" order by n.nswFileCode asc");
        } else {
            strBuf.append(" order by n.nswFileCode asc");
        }
        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }
//Cuongvv

    public List findListStatusByDept(
            SearchDeviceModel searchModel, Long receiveUserId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(p.status) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.receiveUserId = ? AND "
                            + " p.finishDate is null");
                    listParam.add(receiveUserId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.receiveUserId = ? AND "
                            + " p.finishDate is not null");
                    listParam.add(receiveUserId);
                    break;

                default:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.receiveUserId = ? AND "
                            + " p.finishDate is null");
                    listParam.add(receiveUserId);
                    break;
            }
        }
        // hql.append(" order by n.fileId desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }

    public Long countHomePage(String countType) {
        if (countType.isEmpty()) {
            return 0L;
        }
        List listParam = new ArrayList();
        StringBuilder strCountBuf = new StringBuilder("select count(n) ");
        StringBuilder hql = new StringBuilder();
        hql.append(" FROM VFileImportDevice n " + "WHERE n.isActive = 1 "
                + "AND (n.isTemp is null OR n.isTemp = 0) ");
        Long count = 0L;
        try {
            switch (countType) {
                case Constants.COUNT_HOME_TYPE.TOTAL:
                    hql.append("AND ((n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?)) ");
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP);
                    listParam.add(Constants.FILE_STATUS_CODE.FINISH);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_DATHAMDINH);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DAXEMXET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_VT_DATIEPNHAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DATHANHTOAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DAPHANCONG);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DATHAMXET);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_LD_DAKYCONGVANSDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_DAPHEDUYET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DATHONGBAOSDBS);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_KT_DAXACNHANTHANHTOAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DASDBS);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_TRAYEUCAUKIEMTRALAI);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VT_DAGUIPHIEUBAOTHU);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CHOPHANCONG);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_TUCHOIDUYET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHDAT);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHKHONGDAT);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_KT_TUCHOIXACNHANPHI);
                    break;
                case Constants.COUNT_HOME_TYPE.WAIT_PROCESS:
                    hql.append("AND (n.status = ?) ");
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP);
                    break;
                case Constants.COUNT_HOME_TYPE.PROCESS:
                    hql.append("AND ((n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?)) ");
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_DATHAMDINH);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DAXEMXET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_VT_DATIEPNHAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DATHANHTOAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DAPHANCONG);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_TP_DATHAMXET);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_LD_DAKYCONGVANSDBS);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_DAPHEDUYET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DATHONGBAOSDBS);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_KT_DAXACNHANTHANHTOAN);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DASDBS);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_TRAYEUCAUKIEMTRALAI);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VT_DAGUIPHIEUBAOTHU);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CHOPHANCONG);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_LD_TUCHOIDUYET);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHDAT);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHKHONGDAT);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_KT_TUCHOIXACNHANPHI);
                    break;
                case Constants.COUNT_HOME_TYPE.FINISH:
                    hql.append("AND (n.status = ?) ");
                    listParam.add(Constants.FILE_STATUS_CODE.FINISH);
                    break;
                default:
                    break;
            }

            strCountBuf.append(hql);
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                countQuery.setParameter(i, listParam.get(i));
            }

            count = (Long) countQuery.uniqueResult();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return count;
    }

    public PagingListModel findFilesByCreatorId(SearchDeviceModel searchModel,
            Long creatorId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder(
                "SELECT n FROM VFileImportDevice n WHERE n.isActive = 1  ");
        StringBuilder strCountBuf = new StringBuilder(
                "select count(n.importDeviceFileId) FROM VFileImportDevice n WHERE n.isActive = 1  ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);

        if (searchModel != null) {
            // todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel
                        .getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getModifyDateFrom() != null) {
                hql.append(" AND n.modifyDate >= ? ");
                listParam.add(searchModel.getModifyDateFrom());
            }
            if (searchModel.getModifyDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel
                        .getModifyDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null
                    && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel
                        .getnSWFileCode()));
            }
            if (searchModel.getEquipmentNo() != null
                    && searchModel.getEquipmentNo().trim().length() > 0) {
                hql.append(" AND lower(n.equipmentNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel
                        .getEquipmentNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());

            } 
//            else if (searchModel.getLsStatusStr() != null) {
//                hql.append(" AND n.status in :lstStatus ");
//            }
            if (searchModel instanceof VFileImportDeviceModel) {
                VFileImportDeviceModel cosmeticFileModel = (VFileImportDeviceModel) searchModel;

                if (cosmeticFileModel.getImporterName() != null
                        && cosmeticFileModel.getImporterName().trim().length() > 0) {
                    hql.append(" AND n.importerName like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getImporterName()));
                }
                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }
                if (cosmeticFileModel.getBusinessName() != null
                        && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null
                        && cosmeticFileModel.getBusinessAddress().trim()
                        .length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getBusinessAddress()));
                }

                if (cosmeticFileModel.getResponsiblePersonName() != null
                        && cosmeticFileModel.getResponsiblePersonName().trim()
                        .length() > 0) {
                    hql.append(" AND lower(n.responsiblePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel
                            .getResponsiblePersonName()));
                }

            }

        }
        // if (searchModel.getModifyDateFrom() != null) {
        //
        // hql.append(" order by n.createDate desc");
        //
        // } else {
        // if (searchModel.getCreateDateFrom() != null) {
        // hql.append(" order by n.createDate desc");
        // }
        // }
        hql.append(" order by n.modifyDate desc");
        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

//        if (searchModel != null && searchModel.getLsStatusStr() != null) {
//            query.setParameterList("lstStatus", searchModel.getStatusList());
//            countQuery.setParameter("lstStatus", searchModel.getStatusList());
//        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    /**
     * ? Ham kiem tra giay phep 0:Giay phep, 1: sua doi bo sung -1: Chua xuat
     * giay
     *
     * @param fileId
     * @param userID
     * @return
     */
    public int CheckGiayPhep(Long fileId, Long userID) {
        String HQL = " SELECT count(r) FROM CosPermit r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        // Check PERMIT_STATUS da dong dau
        // query.setParameter("fileId",
        // Constants_Cos.EVALUTION.PERMIT_STATUS.PROVIDED_NUMBER);
        Long count = (Long) query.uniqueResult();
        // neu co du lieu bang permit
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }

        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    public int checkDispathSDBS(Long fileId, Long userID) {

        String HQL = " SELECT count(r) FROM AdditionalRequest r WHERE  r.isActive=:isActive AND r.fileId=:fileId ";

        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    public int checkDispathReject(Long fileId, Long userID) {

        String HQL = " SELECT count(r) FROM CosReject r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    public List findListStatusByReceiverAndDeptId(
            SearchDeviceModel searchModel, Long receiverId, Long receiveDeptId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.finishDate is null AND "
                            + " n.status = p.status AND " + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.sendUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileImportDevice n WHERE"
                            + " n.finishDate is not null"
                    //  + " n.fileId = p.objectId AND "
                    //  + " n.fileType = p.objectType AND "
                    //  + " n.status != p.status AND "
                    //    + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                    //     + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                    //     + " AND no.type = ? " + " ) AND "
                    // + " 1 = 1 ");
                    //        + " p.receiveUserId = ? "
                    );
                    //   listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    //   listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status  "
                    //+ " AND p.finishDate is null "
                    //+ " (p.receiveUserId is null AND "
                    // + "AND p.receiveGroupId = ?  "
                    );
                    //listParam.add(receiveDeptId);
                    break;
                case Constants.MENU_TYPE.DELETE_PERMIT:
                    hql.append(" FROM VFileImportDevice n WHERE"
                            + " n.finishDate is not null AND "
                            + " n.status = 31000063 or n.status = 31000064"
                    );
                    break;
                //linhdx 20160517 them chuc nang theo doi san pham
                case Constants.MENU_TYPE.DEPT_PRODUCT_PROCESS_STR:
                    hql.append("  FROM VFileImportDevice n, ImportDeviceProduct a, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " n.fileId = a.fileId "
                    );
                    break;
                default:
                    hql.append(" FROM VFileImportDevice n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND " + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }
        // hql.append(" order by n.fileId desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }

    public List findListStatusByCreatorId(Long creatorId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder(
                "SELECT distinct(n.status) FROM VFileImportDevice n WHERE n.isActive = 1 AND n.status is not null ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);
        hql.append(" order by n.status desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }

    public int getCountGroupTtb(List<Long> lstStatus) {
        int count;
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.fileId) ");
        StringBuilder hql = new StringBuilder();
        hql.append(" FROM VFileImportDevice n, Process p WHERE"
                + " n.fileId = p.objectId AND "
                + " n.fileType = p.objectType AND "
                + " n.status = p.status ");
        hql.append(" AND n.isActive = 1 and n.isTemp=0  ");
        if (lstStatus != null && lstStatus.size() > 0) {
            hql.append(" AND n.status IN :lstStatus");
        }
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        if (lstStatus != null && lstStatus.size() > 0) {
            query.setParameterList("lstStatus", lstStatus);
        }
//        Long count = (Long) query.uniqueResult();
        List lst = query.list();
        count = lst.size();
        return count;
    }

}
