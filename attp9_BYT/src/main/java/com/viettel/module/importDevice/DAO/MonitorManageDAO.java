/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.IdMeeting;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.Model.VFileImportDeviceModel;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author E5420
 */
public class MonitorManageDAO
        extends GenericDAOHibernate<ImportDeviceFile, Long> {

    public MonitorManageDAO() {
        super(ImportDeviceFile.class);
    }

    public int countImportDeviceFile(VFileImportDeviceModel searchModel, String typeSearch) {
        int count = 0;
        StringBuilder strBuf = new StringBuilder("SELECT distinct n.fileId ");
        StringBuilder hql = new StringBuilder();
        List listParam = new ArrayList();
        Query query;
        List lst;
        switch (typeSearch) {
            case "totalFile":
                hql.append("FROM VFileImportDevice n, Process p WHERE"
                        + " n.fileId = p.objectId AND "
                        + " n.fileType = p.objectType AND "
                        + "  p.receiveUserId = ? ");
                listParam.add(searchModel.getUserId());
                if (searchModel.getCreateDateFrom() != null) {
                    hql.append(" AND n.createDate >= ? ");
                    listParam.add(searchModel.getCreateDateFrom());
                }
                if (searchModel.getCreateDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getCreateDateTo());
                    hql.append(" AND n.createDate < ? ");
                    listParam.add(toDate);
                }

                strBuf.append(hql);
                query = session.createQuery(strBuf.toString());
                for (int i = 0; i < listParam.size(); i++) {
                    query.setParameter(i, listParam.get(i));
                }
                lst = query.list();
                count = lst.size();
                return count;

            case "totalFileWaitProcessing":
                hql = new StringBuilder();
                hql.append("FROM VFileImportDevice n, Process p WHERE"
                        + " n.fileId = p.objectId AND "
                        + " n.fileType = p.objectType AND "
                        + " p.finishDate is null AND "
//                        + " n.status = p.status AND "
                        + "  p.receiveUserId = ? ");
                listParam.add(searchModel.getUserId());
                if (searchModel.getCreateDateFrom() != null) {
                    hql.append(" AND n.createDate >= ? ");
                    listParam.add(searchModel.getCreateDateFrom());
                }
                if (searchModel.getCreateDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getCreateDateTo());
                    hql.append(" AND n.createDate < ? ");
                    listParam.add(toDate);
                }

                strBuf.append(hql);
                query = session.createQuery(strBuf.toString());
                for (int i = 0; i < listParam.size(); i++) {
                    query.setParameter(i, listParam.get(i));
                }
                lst = query.list();
                count = lst.size();
                return count;
            case "totalFileProcessed":
                //do
                break;
            case "totalFileWaitOnDeadline":
                hql = new StringBuilder();
                hql.append("FROM VFileImportDevice n, Process p WHERE"
                        + " n.fileId = p.objectId AND "
                        + " n.fileType = p.objectType AND "
                        + " p.finishDate is null AND "
                        + " n.status = p.status AND "
                        + "  p.receiveUserId = ? ");
                listParam.add(searchModel.getUserId());
                if (searchModel.getCreateDateFrom() != null) {
                    hql.append(" AND n.createDate >= ? ");
                    listParam.add(searchModel.getCreateDateFrom());
                }
                if (searchModel.getCreateDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getCreateDateTo());
                    hql.append(" AND n.createDate < ? ");
                    listParam.add(toDate);
                }
                
                Date now = new Date ();
                Date deadline = DateTimeUtils.addDay(now, -3);
                hql.append(" AND p.sendDate >= ? ");
                listParam.add(deadline);
                strBuf.append(hql);
                query = session.createQuery(strBuf.toString());
                for (int i = 0; i < listParam.size(); i++) {
                    query.setParameter(i, listParam.get(i));
                }
                lst = query.list();
                count = lst.size();
                return count;
            case "totalFileWaitMissDeadliness":
                hql = new StringBuilder();
                hql.append("FROM VFileImportDevice n, Process p WHERE"
                        + " n.fileId = p.objectId AND "
                        + " n.fileType = p.objectType AND "
                        + " p.finishDate is null AND "
                        + " n.status = p.status AND "
                        + "  p.receiveUserId = ? ");
                listParam.add(searchModel.getUserId());
                if (searchModel.getCreateDateFrom() != null) {
                    hql.append(" AND n.createDate >= ? ");
                    listParam.add(searchModel.getCreateDateFrom());
                }
                if (searchModel.getCreateDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getCreateDateTo());
                    hql.append(" AND n.createDate < ? ");
                    listParam.add(toDate);
                }
                
                Date now2 = new Date ();
                Date deadline2 = DateTimeUtils.addDay(now2, -3);
                hql.append(" AND p.sendDate < ? ");
                listParam.add(deadline2);
                strBuf.append(hql);
                query = session.createQuery(strBuf.toString());
                for (int i = 0; i < listParam.size(); i++) {
                    query.setParameter(i, listParam.get(i));
                }
                lst = query.list();
                count = lst.size();
                return count;
        }
        return count;

    }
}
