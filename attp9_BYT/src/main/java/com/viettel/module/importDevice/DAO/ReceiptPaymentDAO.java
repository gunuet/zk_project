/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importDevice.BO.AppliesFee;
import com.viettel.module.importDevice.BO.ReceiptPayment;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author E5420
 */
public class ReceiptPaymentDAO
        extends GenericDAOHibernate<ReceiptPayment, Long> {

    public ReceiptPaymentDAO() {
        super(ReceiptPayment.class);
    }

    @Override
    public void saveOrUpdate(ReceiptPayment imDePro) {
        if (imDePro != null) {
            super.saveOrUpdate(imDePro);
        }

        getSession().flush();
    }

    public PagingListModel getReceiptPaymentList(int start, int take, String companyName, String taxCode,String receiptPaymentNumber, String createBy, Long billId) {
        try {
            List listParam = new ArrayList();
            StringBuilder strQuery = new StringBuilder(" from ReceiptPayment a where a.isActive = 1");

            if (companyName != null && !"".equals(companyName)) {
                strQuery.append(" and a.companyName like ?");
                listParam.add("%" + companyName + "%");
            }
            if (taxCode != null && !"".equals(taxCode)) {
                strQuery.append(" and a.taxCode like ?");
                listParam.add("%" + taxCode + "%");
            }
            
            if (createBy != null && !"".equals(createBy)) {
                strQuery.append(" and a.createBy like ?");
                listParam.add("%" + createBy + "%");
            }

            if (receiptPaymentNumber != null && !"".equals(receiptPaymentNumber)) {
                strQuery.append(" and a.receiptPaymentNumber like ?");
                listParam.add("%" + receiptPaymentNumber + "%");
            }
                        
            if (billId != null) {
                strQuery.append(" and a.billId = ?");
                listParam.add(billId);
            }
            StringBuilder hqlQuery = new StringBuilder("select a ").append(strQuery);
            StringBuilder hqlCountQuery = new StringBuilder("select count(a) ").append(strQuery);
            Query query = session.createQuery(hqlQuery.toString());
            Query countQuery = session.createQuery(hqlCountQuery.toString());
            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }
            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }
            List result = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(result, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public ReceiptPayment findReceiptPaymentById(Long id) {
        try {
            StringBuilder strQuery = new StringBuilder(" select a from ReceiptPayment a where a.isActive = 1 and a.id = ?");
            Query query = session.createQuery(strQuery.toString());
            query.setParameter(0, id);
            ReceiptPayment newInter = new ReceiptPayment();
            if (query.list() != null  && query.list().size() > 0) {
                newInter = (ReceiptPayment) query.list().get(0);
            }
            return newInter;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
}
