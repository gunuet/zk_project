/*
 ' * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Model;

import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceProductDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;


/**
 *
 * @author Vu Manh Ha
 */
public class ExportDeviceModel {
    //file info

    ImportDeviceFile importDeviceFile;
    List<ImportDeviceProductModel> lstProduct;
//    List lstAssembler;
//    List lstIngredient;
//    List lstAttachs;
//    List lstPresentations;
//    List lstProductTypes;

    //More info
    private String createName;
    private String content;
    private Date signedDate;
    private Date createDate;
    private String sendNo;
    private String businessName;
    private String businessAddress;
    //Cuongvv
    private Date uQDate;
    private String signer;
    private String rolesigner;
    private String leaderSinged;
    private String pathTemplate;
    private Long cosmeticPermitId;
    private Long cosmeticPermitType;
    private Long cosmeticRejectId;
    private Long cosmeticRejectType;
    private Long cosmeticAdditionalId;
    private Long cosmeticAdditionalType;
    private String place;

    public ExportDeviceModel() {
        // TODO Auto-generated constructor stub
    }

    public ExportDeviceModel(Long id) {
        ImportDeviceFiletDAO deviceDAO = new ImportDeviceFiletDAO();
        importDeviceFile = deviceDAO.findByFileId(id);
        Files file = deviceDAO.getFileByFileId(id);
        //MinhNv - add sendNo
        if (sendNo == null) {
            sendNo = String.valueOf(file.getFileId());
        }
        businessName = file.getBusinessName() == null ? "" : file.getBusinessName();
        businessAddress = file.getBusinessAddress() == null ? "" : file.getBusinessAddress();
        createName = file.getCreatorName() == null ? "" : file.getCreatorName();
        createDate = importDeviceFile.getSignDate();
        uQDate = importDeviceFile.getUqDate();
        cosmeticRejectType = Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH;
        cosmeticPermitType = Constants.OBJECT_TYPE.COSMETIC_PERMIT;//Giay phep cong bo
        cosmeticAdditionalType = Constants.OBJECT_TYPE.IMPORT_DEVICE_SDBS_DISPATH;//Giay phep sua doi bo sung

// MinhNV - Thêm table import Product vào biểu mẫu :
        ImportDeviceProductDAO proDAO = new ImportDeviceProductDAO();

        List<ImportDeviceProduct> lsp = proDAO.findByImportFileId(id);
        if (lsp != null && lsp.size() > 0) {
            lstProduct = new ArrayList<>();
            for (int i = 0; i < lsp.size(); i++) {
                ImportDeviceProduct ip = lsp.get(i);
                ImportDeviceProductModel mode = new ImportDeviceProductModel();
                mode.setC1(importDeviceFile.getProductName());
                mode.setC2(ip.getModel() == null ? "" : ip.getModel());
                mode.setC3(ip.getQuantity() == null ? "" : ip.getQuantity());

                
                //Hang nuoc san xuat
                String c5 = (ip.getManufacturer() == null ? " " : ip.getManufacturer());
                if (!c5.contains("phụ lục đính kèm")) {
                    c5 += (ip.getNationalName() == null ? " " : ", " + ip.getNationalName());
                }
                mode.setC5(c5);
                //Hang nuoc chu so huu
                String c6 = (ip.getDistributor() == null ? " " : ip.getDistributor());
                if (!c6.contains("phụ lục đính kèm")) {
                    c6 += (ip.getNationalDistributorName() == null ? " " : ", " + ip.getNationalDistributorName());
                }
                mode.setC6(c6);
                
                //Hang nuoc phan phoi
                String c4 = (ip.getProductManufacture() == null ? " " : ip.getProductManufacture());
                if (!c4.contains("phụ lục đính kèm")) {
                    c4 += (ip.getNationalProduct() == null ? " " : ", " + ip.getNationalProduct());
                }
                mode.setC4(c4);
                mode.setC7(ip.getProduceYear());
                lstProduct.add(mode);
            }
        }

        // them duong dan file template
        pathTemplate = null;
        //pathTemplate = getPathTemplate(fileId);

    }

    public void setImportDeviceFile(ImportDeviceFile importDeviceFile) {
        this.importDeviceFile = importDeviceFile;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getuQDate() {
        return uQDate;
    }

    public void setuQDate(Date uQDate) {
        this.uQDate = uQDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public ImportDeviceFile getImportDeviceFile() {
        return importDeviceFile;
    }

    public String getContent() {
        return content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public ImportDeviceFile getIdfFile() {
        return importDeviceFile;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public Long getCosmeticPermitId() {
        return cosmeticPermitId;
    }

    public Long getCosmeticPermitType() {
        return cosmeticPermitType;
    }

    public Long getCosmeticRejectType() {
        return cosmeticRejectType;
    }

    public String getPathTemplate() {
        return pathTemplate;
    }

    public void setIdfFile(ImportDeviceFile idfFile) {
        this.importDeviceFile = idfFile;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public void setCosmeticPermitId(Long cosmeticPermitId) {
        this.cosmeticPermitId = cosmeticPermitId;
    }

    public void setCosmeticPermitType(Long cosmeticPermitType) {
        this.cosmeticPermitType = cosmeticPermitType;
    }

    public void setCosmeticRejectType(Long cosmeticRejectType) {
        this.cosmeticRejectType = cosmeticRejectType;
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public void setLstProduct(List<ImportDeviceProductModel> lstProduct) {
        this.lstProduct = lstProduct;
    }

    public List getLstProduct() {
        return lstProduct;
    }

    public Date getSignedDate() {
        return signedDate;
    }

    public void setSignedDate(Date signedDate) {
        this.signedDate = signedDate;
    }

    public String getSendNo() {
        return sendNo;
    }

    public void setSendNo(String sendNo) {
        this.sendNo = sendNo;
    }

    private String getPathTemplate(Long fileId) {
        //Get Template
        WorkflowAPI wAPI = new WorkflowAPI();
        Flow flow = wAPI.getFlowByFileId(fileId);
        Long deptId = flow.getDeptId();
        Long procedureId = flow.getObjectId();

        TemplateDAOHE the = new TemplateDAOHE();
        String pathTemplate = the.findPathTemplate(deptId, procedureId,
                Constants.PROCEDURE_TEMPLATE_TYPE.PERMIT);
        return pathTemplate;
    }

    public String toXML() throws JAXBException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ExportDeviceModel.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            StringWriter builder = new StringWriter();
            jaxbMarshaller.marshal(this, builder);
            return builder.toString();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }

    }

    /**
     * @return the place
     */
    public String getPlace() {
        return place;
    }

    /**
     * @param place the place to set
     */
    public void setPlace(String place) {
        this.place = place;
    }
}
