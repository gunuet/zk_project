/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.importDevice.Model;

/**
 *
 * @author hieuld5
 */
public class ImportDeviceProductModel {
    private String C1;
    private String C2;
    private String C3;
    private String C4;
    private String C5;
    private String C6;
    private String C7;

 
    public String getC1() {
        return C1;
    }

    /**
     * @param C1 the C1 to set
     */
    public void setC1(String C1) {
        this.C1 = C1;
    }

    /**
     * @return the C2
     */
    public String getC2() {
        return C2;
    }

    /**
     * @param C2 the C2 to set
     */
    public void setC2(String C2) {
        this.C2 = C2;
    }

    /**
     * @return the C3
     */
    public String getC3() {
        return C3;
    }

    /**
     * @param C3 the C3 to set
     */
    public void setC3(String C3) {
        this.C3 = C3;
    }

    /**
     * @return the C4
     */
    public String getC4() {
        return C4;
    }

    /**
     * @param C4 the C4 to set
     */
    public void setC4(String C4) {
        this.C4 = C4;
    }

    /**
     * @return the C5
     */
    public String getC5() {
        return C5;
    }

    /**
     * @param C5 the C5 to set
     */
    public void setC5(String C5) {
        this.C5 = C5;
    }

    /**
     * @return the C6
     */
    public String getC6() {
        return C6;
    }

    /**
     * @param C6 the C6 to set
     */
    public void setC6(String C6) {
        this.C6 = C6;
    }

    /**
     * @return the C7
     */
    public String getC7() {
        return C7;
    }

    /**
     * @param C7 the C7 to set
     */
    public void setC7(String C7) {
        this.C7 = C7;
    }
}
