/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author MrBi
 */
public class SearchDeviceModel {

    protected Long bookId;
    protected int menuType;
    protected String menuTypeStr;

    protected String equipmentNo;
    protected Long importType;
    protected String nSWFileCode;
    protected Long status;

    protected Date createDateFrom;
    protected Date createDateTo;
    protected Date modifyDateFrom;
    protected Date modifyDateTo;

    private List<Long> statusList;
    private String lsStatusStr;

    public SearchDeviceModel() {
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public void setMenuType(int menuType) {
        this.menuType = menuType;
    }

    public void setMenuTypeStr(String menuTypeStr) {
        this.menuTypeStr = menuTypeStr;
    }

    public void setEquipmentNo(String equipmentNo) {
        this.equipmentNo = equipmentNo;
    }

    public void setImportType(Long importType) {
        this.importType = importType;
    }

    public void setnSWFileCode(String nSWFileCode) {
        this.nSWFileCode = nSWFileCode;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public void setCreateDateFrom(Date createDateFrom) {
        this.createDateFrom = createDateFrom;
    }

    public void setCreateDateTo(Date createDateTo) {
        this.createDateTo = createDateTo;
    }

    public void setModifyDateFrom(Date modifyDateFrom) {
        this.modifyDateFrom = modifyDateFrom;
    }

    public void setModifyDateTo(Date modifyDateTo) {
        this.modifyDateTo = modifyDateTo;
    }

    public void setStatusList(List<Long> statusList) {
        this.statusList = statusList;
    }

    public Long getBookId() {
        return bookId;
    }

    public int getMenuType() {
        return menuType;
    }

    public String getMenuTypeStr() {
        return menuTypeStr;
    }

    public String getEquipmentNo() {
        return equipmentNo;
    }

    public Long getImportType() {
        return importType;
    }

    public String getnSWFileCode() {
        return nSWFileCode;
    }

    public Long getStatus() {
        return status;
    }

    public Date getCreateDateFrom() {
        return createDateFrom;
    }

    public Date getCreateDateTo() {
        return createDateTo;
    }

    public Date getModifyDateFrom() {
        return modifyDateFrom;
    }

    public Date getModifyDateTo() {
        return modifyDateTo;
    }

    public List<Long> getStatusList() {
        return statusList;
    }

    /**
     * @return the lsStatusStr
     */
    public String getLsStatusStr() {
        return lsStatusStr;
    }

    /**
     * @param lsStatusStr the lsStatusStr to set
     */
    public void setLsStatusStr(String lsStatusStr) {
        this.lsStatusStr = lsStatusStr;
    }

}
