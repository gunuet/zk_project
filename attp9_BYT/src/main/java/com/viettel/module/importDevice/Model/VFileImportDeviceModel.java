/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Model;

import java.util.Date;

import com.google.gson.Gson;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.utils.Constants;

/**
 *
 * @author MrBi
 */
public class VFileImportDeviceModel extends SearchDeviceModel {

    private Long importDeviceFileId;
    private Long fileId;
    private String nswFileCode;
    private Long documentTypeCode;
    private String importerName;
    private String importerAddress;
    private String importerPhone;
    private String importerFax;
    private String importerEmail;
    private String director;
    private String directorPhone;
    private String directorMobile;
    private String responsiblePersonName;
    private String responsiblePersonPhone;
    private String responsiblePersonMobile;
    private String importPurpose;
    private String signPlace;
    private Date signDate;
    private String signName;
    private String signedData;
    private Long bookNumber;
    private String bookNumberStr;
    private String model;
    private String nationalName;//Nuoc san xuat
    private String nationalDistributorName;//Nuoc chu so huu
    private String nationalProduct;//Nuoc phan phoi
    private String procductYear;
    private String productName;
    private Long fileType;
    private String fileTypeName;
    private String fileCode;
    private String fileName;
    private String taxCode;
    private Long businessId;
    private String businessName;
    private String businessAddress;
    private String businessPhone;
    private String businessFax;
    private Date createDate;
    private Date modifyDate;
    private Long creatorId;
    private String creatorName;
    private Long createDeptId;
    private String createDeptName;
    private Long isActive;
    private Long version;
    private Long parentFileId;
    private Long isTemp;
    private Date startDate;
    private Long numDayProcess;
    private Date deadline;
    private String strStatus;
    private String resulCVC;
    private String resulCVKTC;
    private String resulTVHD;
    private String resultAll;
    private String resulHHD;
    private String lsStatusStr;
    private String C1;
    private String C2;
    private String C3;
    private String C4;
    private String C5;
    private String C6;
    private String C7;
    private String C8;

    private Long userId;

    private String groupProductId;//Nhom thiet bi
//    private String productName;//ten trang thiet bi

    private String permitNumber;
    private Date permitDateFrom;
    private Date permitDateTo;

    //private String model;
    private String manufacturer;
    //private String nationalName;
    private String distributor;
    //private String nationalDistributorName;
    private String productManufacture;

    private VFileImportDevice vFileImportDevice;
    private ImportDeviceProduct importDeviceProduct;

    private String sendDate;
    private String statusStr;
    private int rowNum;
    String documentTypeCodeStr;
    private String startDateStr;

    //private String nationalDistributorName;
    public VFileImportDeviceModel() {
    }

    public VFileImportDeviceModel(VFileImportDevice n, ImportDeviceProduct a) {
        this.vFileImportDevice = n;
        this.importDeviceProduct = a;
    }

    public void setImportDeviceFileId(Long importDeviceFileId) {
        this.importDeviceFileId = importDeviceFileId;
    }

    public Long getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(Long bookNumber) {
        this.bookNumber = bookNumber;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public void setImporterName(String importerName) {
        this.importerName = importerName;
    }

    public void setImporterAddress(String importerAddress) {
        this.importerAddress = importerAddress;
    }

    public void setImporterPhone(String importerPhone) {
        this.importerPhone = importerPhone;
    }

    public void setImporterFax(String importerFax) {
        this.importerFax = importerFax;
    }

    public void setImporterEmail(String importerEmail) {
        this.importerEmail = importerEmail;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setDirectorPhone(String directorPhone) {
        this.directorPhone = directorPhone;
    }

    public void setDirectorMobile(String directorMobile) {
        this.directorMobile = directorMobile;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
        this.responsiblePersonPhone = responsiblePersonPhone;
    }

    public void setResponsiblePersonMobile(String responsiblePersonMobile) {
        this.responsiblePersonMobile = responsiblePersonMobile;
    }

    public void setImportPurpose(String importPurpose) {
        this.importPurpose = importPurpose;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public void setIsTemp(Long isTemp) {
        this.isTemp = isTemp;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setNumDayProcess(Long numDayProcess) {
        this.numDayProcess = numDayProcess;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Long getImportDeviceFileId() {
        return importDeviceFileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public String getImporterName() {
        return importerName;
    }

    public String getImporterAddress() {
        return importerAddress;
    }

    public String getImporterPhone() {
        return importerPhone;
    }

    public String getImporterFax() {
        return importerFax;
    }

    public String getImporterEmail() {
        return importerEmail;
    }

    public String getDirector() {
        return director;
    }

    public String getDirectorPhone() {
        return directorPhone;
    }

    public String getDirectorMobile() {
        return directorMobile;
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public String getResponsiblePersonPhone() {
        return responsiblePersonPhone;
    }

    public String getResponsiblePersonMobile() {
        return responsiblePersonMobile;
    }

    public String getImportPurpose() {
        return importPurpose;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public String getSignName() {
        return signName;
    }

    public String getSignedData() {
        return signedData;
    }

    public Long getFileType() {
        return fileType;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public Long getVersion() {
        return version;
    }

    public Long getParentFileId() {
        return parentFileId;
    }

    public Long getIsTemp() {
        return isTemp;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Long getNumDayProcess() {
        return numDayProcess;
    }

    public Date getDeadline() {
        return deadline;
    }

    public String getStrStatus() {
        return strStatus;
    }

    public void setStrStatus(String strStatus) {
        this.strStatus = strStatus;
    }

    public String getResulCVC() {
        return resulCVC;
    }

    public void setResulCVC(String resulCVC) {
        this.resulCVC = resulCVC;
    }

    public String getResulCVKTC() {
        return resulCVKTC;
    }

    public void setResulCVKTC(String resulCVKTC) {
        this.resulCVKTC = resulCVKTC;
    }

    public String getResulTVHD() {
        return resulTVHD;
    }

    public void setResulTVHD(String resulTVHD) {
        this.resulTVHD = resulTVHD;
    }

    public String getBookNumberStr() {
        return bookNumberStr;
    }

    public void setBookNumberStr(String bookNumberStr) {
        this.bookNumberStr = bookNumberStr;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNationalName() {
        return nationalName;
    }

    public void setNationalName(String nationalName) {
        this.nationalName = nationalName;
    }

    public String getNationalDistributorName() {
        return nationalDistributorName;
    }

    public void setNationalDistributorName(String nationalDistributorName) {
        this.nationalDistributorName = nationalDistributorName;
    }

    public String getNationalProduct() {
        return nationalProduct;
    }

    public void setNationalProduct(String nationalProduct) {
        this.nationalProduct = nationalProduct;
    }

//    public String getProcductName() {
//        return procductName;
//    }
//
//    public void setProcductName(String procductName) {
//        this.procductName = procductName;
//    }
    public String getProcductYear() {
        return procductYear;
    }

    public void setProcductYear(String procductYear) {
        this.procductYear = procductYear;
    }

    public String getContent(Long fileId, Long evalType) {
        String content;
        EvaluationRecordDAO dao = new EvaluationRecordDAO();
        EvaluationRecord evalrecord = dao.findContentByFileIdAndEvalType(fileId, evalType);
        if (evalrecord != null) {
            content = evalrecord.getFormContent();
        } else {
            return "";
        }
        if (evalType.equals(Constants.FILE_STATUS_CODE.STATUS_HHD)) {
            Gson gs = new Gson();
            EvaluationModel evModel = gs.fromJson(content, EvaluationModel.class);
            String sComment = evalrecord.getCreatorName() + ": ";
            if (evModel.getResultEvaluationStr() != null) {
                sComment += evModel.getResultEvaluationStr();
            } else {
                if(evModel.getProcess() !=null && evModel.getProcess().getActionName() !=null){
                    sComment += evModel.getProcess().getActionName();
                }
            }
            if (evalrecord.getMainContent() != null) {
                sComment += ", " + evalrecord.getMainContent();
            }
            return sComment;
        }
        return (content == null ? "" : convertJsonFromContenttoString(content)) + (evalrecord.getMainContent() == null ? "" : ", Lý do: " + evalrecord.getMainContent());
    }

    public String convertJsonFromContenttoString(String jsonFormContent) {
        Gson gson = new Gson();
        String status = "";
        if (!"".equals(jsonFormContent) && jsonFormContent !=null) {
            EvaluationModel evModel = gson.fromJson(jsonFormContent, EvaluationModel.class);

            if (evModel != null) {
                Long userId = evModel.getUserId();
                UserDAOHE uhe = new UserDAOHE();
                Users u = uhe.getUserById(userId);
                status = evModel.getResultEvaluationStr();

                status = u.getFullName() + ": " + status;
            } else {
                status = "";
            }
        }

        return status;

    }

    /**
     * @return the resulHHD
     */
    public String getResulHHD() {
        return resulHHD;
    }

    /**
     * @param resulHHD the resulHHD to set
     */
    public void setResulHHD(String resulHHD) {
        this.resulHHD = resulHHD;
    }

    /**
     * @return the lsStatusStr
     */
    public String getLsStatusStr() {
        return lsStatusStr;
    }

    /**
     * @param lsStatusStr the lsStatusStr to set
     */
    public void setLsStatusStr(String lsStatusStr) {
        this.lsStatusStr = lsStatusStr;
    }

    /**
     * @return the C1
     */
    public String getC1() {
        return C1;
    }

    /**
     * @param C1 the C1 to set
     */
    public void setC1(String C1) {
        this.C1 = C1;
    }

    /**
     * @return the C2
     */
    public String getC2() {
        return C2;
    }

    /**
     * @param C2 the C2 to set
     */
    public void setC2(String C2) {
        this.C2 = C2;
    }

    /**
     * @return the C3
     */
    public String getC3() {
        return C3;
    }

    /**
     * @param C3 the C3 to set
     */
    public void setC3(String C3) {
        this.C3 = C3;
    }

    /**
     * @return the C4
     */
    public String getC4() {
        return C4;
    }

    /**
     * @param C4 the C4 to set
     */
    public void setC4(String C4) {
        this.C4 = C4;
    }

    /**
     * @return the C5
     */
    public String getC5() {
        return C5;
    }

    /**
     * @param C5 the C5 to set
     */
    public void setC5(String C5) {
        this.C5 = C5;
    }

    /**
     * @return the C6
     */
    public String getC6() {
        return C6;
    }

    /**
     * @param C6 the C6 to set
     */
    public void setC6(String C6) {
        this.C6 = C6;
    }

    /**
     * @return the C7
     */
    public String getC7() {
        return C7;
    }

    /**
     * @param C7 the C7 to set
     */
    public void setC7(String C7) {
        this.C7 = C7;
    }

    /**
     * @return the C8
     */
    public String getC8() {
        return C8;
    }

    /**
     * @param C8 the C8 to set
     */
    public void setC8(String C8) {
        this.C8 = C8;
    }

    public String getResultAll() {
        return resultAll;
    }

    public void setResultAll(String resultAll) {
        this.resultAll = resultAll;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getGroupProductId() {
        return groupProductId;
    }

    public void setGroupProductId(String groupProductId) {
        this.groupProductId = groupProductId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPermitNumber() {
        return permitNumber;
    }

    public void setPermitNumber(String permitNumber) {
        this.permitNumber = permitNumber;
    }

    public Date getPermitDateFrom() {
        return permitDateFrom;
    }

    public void setPermitDateFrom(Date permitDateFrom) {
        this.permitDateFrom = permitDateFrom;
    }

    public Date getPermitDateTo() {
        return permitDateTo;
    }

    public void setPermitDateTo(Date permitDateTo) {
        this.permitDateTo = permitDateTo;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getProductManufacture() {
        return productManufacture;
    }

    public void setProductManufacture(String productManufacture) {
        this.productManufacture = productManufacture;
    }

    public VFileImportDevice getvFileImportDevice() {
        return vFileImportDevice;
    }

    public void setvFileImportDevice(VFileImportDevice vFileImportDevice) {
        this.vFileImportDevice = vFileImportDevice;
    }

    public ImportDeviceProduct getImportDeviceProduct() {
        return importDeviceProduct;
    }

    public void setImportDeviceProduct(ImportDeviceProduct importDeviceProduct) {
        this.importDeviceProduct = importDeviceProduct;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public String getDocumentTypeCodeStr() {
        return documentTypeCodeStr;
    }

    public void setDocumentTypeCodeStr(String documentTypeCodeStr) {
        this.documentTypeCodeStr = documentTypeCodeStr;
    }

    public String getStartDateStr() {
        return startDateStr;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }

}
