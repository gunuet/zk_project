/*
 ' * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importDevice.Model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Vu Manh Ha
 */
public class YCBSModel {

    private Long fileId;
    private String createName;
    private String content;
    private String sendDate;

    public YCBSModel() {
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendDate() {
        return sendDate;
    }

    public String formatDate(Date date) {
        String formatDate;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        formatDate = dateFormat.format(date);
        return formatDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

}
