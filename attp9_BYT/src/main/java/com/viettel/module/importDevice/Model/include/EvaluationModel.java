/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.importDevice.Model.include;

/**
 *
 * @author VUTUAN
 */
public class EvaluationModel {
    private String resonRequest;
    private Long resultAppraisal;
    
    // neu typeProfessional = 1: chuyen vien chinh
    // neu typeProfessional = 2: chuyen vien kiem tra cheo
    private Long typeProfessional;
    
    public String getResonRequest() {
        return resonRequest;
    }

    public void setResonRequest(String resonRequest) {
        this.resonRequest = resonRequest;
    }
    
    public Long getResultAppraisal() {
        return resultAppraisal;
    }

    public void setResultAppraisal(Long resultAppraisal) {
        this.resultAppraisal = resultAppraisal;
    }
    
    public Long getTypeProfessional() {
        return typeProfessional;
    }

    public void setTypeProfessional(Long typeProfessional) {
        this.typeProfessional = typeProfessional;
    }
    
}
