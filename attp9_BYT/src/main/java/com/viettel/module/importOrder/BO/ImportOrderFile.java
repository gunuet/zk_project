/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import com.viettel.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author THANHDV
 */
@Entity
@Table(name = "IMPORT_ORDER_FILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImportOrderFile.findAll", query = "SELECT i FROM ImportOrderFile i"),
    @NamedQuery(name = "ImportOrderFile.findByImportOrderFileId", query = "SELECT i FROM ImportOrderFile i WHERE i.importOrderFileId = :importOrderFileId"),
    @NamedQuery(name = "ImportOrderFile.findByFileId", query = "SELECT i FROM ImportOrderFile i WHERE i.fileId = :fileId"),
    @NamedQuery(name = "ImportOrderFile.findByNswFileCode", query = "SELECT i FROM ImportOrderFile i WHERE i.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "ImportOrderFile.findByDocumentTypeCode", query = "SELECT i FROM ImportOrderFile i WHERE i.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "ImportOrderFile.findByTransNo", query = "SELECT i FROM ImportOrderFile i WHERE i.transNo = :transNo"),
    @NamedQuery(name = "ImportOrderFile.findByContractNo", query = "SELECT i FROM ImportOrderFile i WHERE i.contractNo = :contractNo"),
    @NamedQuery(name = "ImportOrderFile.findByBillNo", query = "SELECT i FROM ImportOrderFile i WHERE i.billNo = :billNo"),
    @NamedQuery(name = "ImportOrderFile.findByGoodsOwnerName", query = "SELECT i FROM ImportOrderFile i WHERE i.goodsOwnerName = :goodsOwnerName"),
    @NamedQuery(name = "ImportOrderFile.findByGoodsOwnerAddress", query = "SELECT i FROM ImportOrderFile i WHERE i.goodsOwnerAddress = :goodsOwnerAddress"),
    @NamedQuery(name = "ImportOrderFile.findByGoodsOwnerPhone", query = "SELECT i FROM ImportOrderFile i WHERE i.goodsOwnerPhone = :goodsOwnerPhone"),
    @NamedQuery(name = "ImportOrderFile.findByGoodsOwnerFax", query = "SELECT i FROM ImportOrderFile i WHERE i.goodsOwnerFax = :goodsOwnerFax"),
    @NamedQuery(name = "ImportOrderFile.findByGoodsOwnerEmail", query = "SELECT i FROM ImportOrderFile i WHERE i.goodsOwnerEmail = :goodsOwnerEmail"),
    @NamedQuery(name = "ImportOrderFile.findByResponsiblePersonName", query = "SELECT i FROM ImportOrderFile i WHERE i.responsiblePersonName = :responsiblePersonName"),
    @NamedQuery(name = "ImportOrderFile.findByResponsiblePersonAddress", query = "SELECT i FROM ImportOrderFile i WHERE i.responsiblePersonAddress = :responsiblePersonAddress"),
    @NamedQuery(name = "ImportOrderFile.findByResponsiblePersonPhone", query = "SELECT i FROM ImportOrderFile i WHERE i.responsiblePersonPhone = :responsiblePersonPhone"),
    @NamedQuery(name = "ImportOrderFile.findByResponsiblePersonFax", query = "SELECT i FROM ImportOrderFile i WHERE i.responsiblePersonFax = :responsiblePersonFax"),
    @NamedQuery(name = "ImportOrderFile.findByResponsiblePersonEmail", query = "SELECT i FROM ImportOrderFile i WHERE i.responsiblePersonEmail = :responsiblePersonEmail"),
    @NamedQuery(name = "ImportOrderFile.findByExporterName", query = "SELECT i FROM ImportOrderFile i WHERE i.exporterName = :exporterName"),
    @NamedQuery(name = "ImportOrderFile.findByExporterAddress", query = "SELECT i FROM ImportOrderFile i WHERE i.exporterAddress = :exporterAddress"),
    @NamedQuery(name = "ImportOrderFile.findByExporterPhone", query = "SELECT i FROM ImportOrderFile i WHERE i.exporterPhone = :exporterPhone"),
    @NamedQuery(name = "ImportOrderFile.findByExporterFax", query = "SELECT i FROM ImportOrderFile i WHERE i.exporterFax = :exporterFax"),
    @NamedQuery(name = "ImportOrderFile.findByExporterEmail", query = "SELECT i FROM ImportOrderFile i WHERE i.exporterEmail = :exporterEmail"),
    @NamedQuery(name = "ImportOrderFile.findByComingDate", query = "SELECT i FROM ImportOrderFile i WHERE i.comingDate = :comingDate"),
    @NamedQuery(name = "ImportOrderFile.findByExporterGateCode", query = "SELECT i FROM ImportOrderFile i WHERE i.exporterGateCode = :exporterGateCode"),
    @NamedQuery(name = "ImportOrderFile.findByExporterGateName", query = "SELECT i FROM ImportOrderFile i WHERE i.exporterGateName = :exporterGateName"),
    @NamedQuery(name = "ImportOrderFile.findByImporterGateCode", query = "SELECT i FROM ImportOrderFile i WHERE i.importerGateCode = :importerGateCode"),
    @NamedQuery(name = "ImportOrderFile.findByImporterGateName", query = "SELECT i FROM ImportOrderFile i WHERE i.importerGateName = :importerGateName"),
    @NamedQuery(name = "ImportOrderFile.findByCustomsBranchCode", query = "SELECT i FROM ImportOrderFile i WHERE i.customsBranchCode = :customsBranchCode"),
    @NamedQuery(name = "ImportOrderFile.findByCustomsBranchName", query = "SELECT i FROM ImportOrderFile i WHERE i.customsBranchName = :customsBranchName"),
    @NamedQuery(name = "ImportOrderFile.findByCheckTime", query = "SELECT i FROM ImportOrderFile i WHERE i.checkTime = :checkTime"),
    @NamedQuery(name = "ImportOrderFile.findByCheckPlace", query = "SELECT i FROM ImportOrderFile i WHERE i.checkPlace = :checkPlace"),
    @NamedQuery(name = "ImportOrderFile.findByDeptCode", query = "SELECT i FROM ImportOrderFile i WHERE i.deptCode = :deptCode"),
    @NamedQuery(name = "ImportOrderFile.findByDeptName", query = "SELECT i FROM ImportOrderFile i WHERE i.deptName = :deptName"),
    @NamedQuery(name = "ImportOrderFile.findBySignPlace", query = "SELECT i FROM ImportOrderFile i WHERE i.signPlace = :signPlace"),
    @NamedQuery(name = "ImportOrderFile.findBySignDate", query = "SELECT i FROM ImportOrderFile i WHERE i.signDate = :signDate"),
    @NamedQuery(name = "ImportOrderFile.findBySignName", query = "SELECT i FROM ImportOrderFile i WHERE i.signName = :signName"),
    @NamedQuery(name = "ImportOrderFile.findBySignedData", query = "SELECT i FROM ImportOrderFile i WHERE i.signedData = :signedData"),
    @NamedQuery(name = "ImportOrderFile.findByFilesNoReduced", query = "SELECT i FROM ImportOrderFile i WHERE i.filesNoReduced = :filesNoReduced"),
    @NamedQuery(name = "ImportOrderFile.findByFilesNo", query = "SELECT i FROM ImportOrderFile i WHERE i.filesNo = :filesNo")})
public class ImportOrderFile implements Serializable {

    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "IMPORT_ORDER_FILE_SEQ", sequenceName = "IMPORT_ORDER_FILE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMPORT_ORDER_FILE_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMPORT_ORDER_FILE_ID")
    private Long importOrderFileId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 62)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Size(max = 70)
    @Column(name = "TRANS_NO")
    private String transNo;
    @Size(max = 70)
    @Column(name = "CONTRACT_NO")
    private String contractNo;
    @Size(max = 510)
    @Column(name = "BILL_NO")
    private String billNo;
    @Size(max = 510)
    @Column(name = "GOODS_OWNER_NAME")
    private String goodsOwnerName;
    @Size(max = 1020)
    @Column(name = "GOODS_OWNER_ADDRESS")
    private String goodsOwnerAddress;
    @Size(max = 510)
    @Column(name = "GOODS_OWNER_PHONE")
    private String goodsOwnerPhone;
    @Size(max = 40)
    @Column(name = "GOODS_OWNER_FAX")
    private String goodsOwnerFax;
    @Size(max = 62)
    @Column(name = "GOODS_OWNER_EMAIL")
    private String goodsOwnerEmail;
    @Size(max = 510)
    @Column(name = "RESPONSIBLE_PERSON_NAME")
    private String responsiblePersonName;
    @Size(max = 510)
    @Column(name = "RESPONSIBLE_PERSON_ADDRESS")
    private String responsiblePersonAddress;
    @Size(max = 100)
    @Column(name = "RESPONSIBLE_PERSON_PHONE")
    private String responsiblePersonPhone;
    @Size(max = 40)
    @Column(name = "RESPONSIBLE_PERSON_FAX")
    private String responsiblePersonFax;
    @Size(max = 420)
    @Column(name = "RESPONSIBLE_PERSON_EMAIL")
    private String responsiblePersonEmail;
    @Size(max = 510)
    @Column(name = "EXPORTER_NAME")
    private String exporterName;
    @Size(max = 510)
    @Column(name = "EXPORTER_ADDRESS")
    private String exporterAddress;
    @Size(max = 510)
    @Column(name = "EXPORTER_PHONE")
    private String exporterPhone;
    @Size(max = 510)
    @Column(name = "EXPORTER_FAX")
    private String exporterFax;
    @Size(max = 510)
    @Column(name = "EXPORTER_EMAIL")
    private String exporterEmail;
    @Column(name = "COMING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comingDate;
    @Size(max = 12)
    @Column(name = "EXPORTER_GATE_CODE")
    private String exporterGateCode;
    @Size(max = 510)
    @Column(name = "EXPORTER_GATE_NAME")
    private String exporterGateName;
    @Size(max = 12)
    @Column(name = "IMPORTER_GATE_CODE")
    private String importerGateCode;
    @Size(max = 510)
    @Column(name = "IMPORTER_GATE_NAME")
    private String importerGateName;
    @Size(max = 40)
    @Column(name = "CUSTOMS_BRANCH_CODE")
    private String customsBranchCode;
    @Size(max = 510)
    @Column(name = "CUSTOMS_BRANCH_NAME")
    private String customsBranchName;
    @Column(name = "CHECK_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkTime;
    @Size(max = 510)
    @Column(name = "CHECK_PLACE")
    private String checkPlace;
    @Size(max = 24)
    @Column(name = "DEPT_CODE")
    private String deptCode;
    @Size(max = 510)
    @Column(name = "DEPT_NAME")
    private String deptName;
    @Size(max = 510)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.DATE)
    private Date signDate;
    @Size(max = 510)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Size(max = 4000)
    @Column(name = "SIGNED_DATA")
    private String signedData;
    @Size(max = 100)
    @Column(name = "FILES_NO_REDUCED")
    private String filesNoReduced;
    @Size(max = 100)
    @Column(name = "FILES_NO")
    private String filesNo;
    @Column(name = "CHECK_DEPT_ID")
    private Long checkDeptId;
    @Column(name = "CHECK_DEPT_CODE")
    private String checkDeptCode;
    @Column(name = "CHECK_DEPT_NAME")
    private String checkDeptName;
    @Column(name = "REGISTER_NO")
    private Long registerNo;
    @Column(name = "PERMIT_NO")
    private Long permitNo;
    //linhdx y kien xu ly doi voi lo hang khong dat
    @Column(name = "COMMENTS_TYPE")
    private String commentsType;
    @Column(name = "COMMENTS")
    private String comments;

    public ImportOrderFile() {
    }

    public ImportOrderFile(Long importOrderFileId) {
        this.importOrderFileId = importOrderFileId;
    }

    public Long getCheckDeptId() {
        return checkDeptId;
    }

    public void setCheckDeptId(Long checkDeptId) {
        this.checkDeptId = checkDeptId;
    }

    public String getCheckDeptCode() {
        return checkDeptCode;
    }

    public void setCheckDeptCode(String checkDeptCode) {
        this.checkDeptCode = checkDeptCode;
    }

    public String getCheckDeptName() {
        return checkDeptName;
    }

    public void setCheckDeptName(String checkDeptName) {
        this.checkDeptName = checkDeptName;
    }

    public Long getImportOrderFileId() {
        return importOrderFileId;
    }

    public void setImportOrderFileId(Long importOrderFileId) {
        this.importOrderFileId = importOrderFileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {

        return replaceCharacter(goodsOwnerEmail);
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = replaceCharacter(goodsOwnerEmail);
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public String getResponsiblePersonAddress() {
        return responsiblePersonAddress;
    }

    public void setResponsiblePersonAddress(String responsiblePersonAddress) {
        this.responsiblePersonAddress = responsiblePersonAddress;
    }

    public String getResponsiblePersonPhone() {
        return responsiblePersonPhone;
    }

    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
        this.responsiblePersonPhone = responsiblePersonPhone;
    }

    public String getResponsiblePersonFax() {
        return responsiblePersonFax;
    }

    public void setResponsiblePersonFax(String responsiblePersonFax) {
        this.responsiblePersonFax = responsiblePersonFax;
    }

    public String getResponsiblePersonEmail() {
        return replaceCharacter(responsiblePersonEmail);
    }

    public void setResponsiblePersonEmail(String responsiblePersonEmail) {
        this.responsiblePersonEmail = replaceCharacter(responsiblePersonEmail);
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterFax() {
        return exporterFax;
    }

    public void setExporterFax(String exporterFax) {
        this.exporterFax = exporterFax;
    }

    public String getExporterEmail() {
        return replaceCharacter(exporterEmail);
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = replaceCharacter(exporterEmail);
    }

    public Date getComingDate() {
        return comingDate;
    }

    public void setComingDate(Date comingDate) {
        this.comingDate = comingDate;
    }

    public String getExporterGateCode() {
        return exporterGateCode;
    }

    public void setExporterGateCode(String exporterGateCode) {
        this.exporterGateCode = exporterGateCode;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateCode() {
        return importerGateCode;
    }

    public void setImporterGateCode(String importerGateCode) {
        this.importerGateCode = importerGateCode;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public String getCustomsBranchCode() {
        return customsBranchCode;
    }

    public void setCustomsBranchCode(String customsBranchCode) {
        this.customsBranchCode = customsBranchCode;
    }

    public String getCustomsBranchName() {
        return customsBranchName;
    }

    public void setCustomsBranchName(String customsBranchName) {
        this.customsBranchName = customsBranchName;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignedData() {
        return signedData;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public String getFilesNoReduced() {
        return filesNoReduced;
    }

    public void setFilesNoReduced(String filesNoReduced) {
        this.filesNoReduced = filesNoReduced;
    }

    public String getFilesNo() {
        return filesNo;
    }

    public void setFilesNo(String filesNo) {
        this.filesNo = filesNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getImportOrderFileId() != null ? getImportOrderFileId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImportOrderFile)) {
            return false;
        }
        ImportOrderFile other = (ImportOrderFile) object;
        if ((this.getImportOrderFileId() == null && other.getImportOrderFileId() != null) || (this.getImportOrderFileId() != null && !this.importOrderFileId.equals(other.importOrderFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importOrder.BO.ImportOrderFile[ importOrderFileId=" + getImportOrderFileId() + " ]";
    }

    /**
     * @return the registerNo
     */
    public Long getRegisterNo() {
        return registerNo;
    }

    /**
     * @param registerNo the registerNo to set
     */
    public void setRegisterNo(Long registerNo) {
        this.registerNo = registerNo;
    }

    /**
     * @return the permitNo
     */
    public Long getPermitNo() {
        return permitNo;
    }

    /**
     * @param permitNo the permitNo to set
     */
    public void setPermitNo(Long permitNo) {
        this.permitNo = permitNo;
    }

    public String getCommentsType() {
        return commentsType;
    }

    public void setCommentsType(String commentsType) {
        this.commentsType = commentsType;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String replaceCharacter(String sourceStr) {
        return StringUtils.replaceCharacter(sourceStr);
    }

}
