/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Van Trung
 */
@Entity
@Table(name = "IMPORT_ORDER_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImportOrderProduct.findAll", query = "SELECT i FROM ImportOrderProduct i")
    ,
    @NamedQuery(name = "ImportOrderProduct.findByProductId", query = "SELECT i FROM ImportOrderProduct i WHERE i.productId = :productId")
    ,
    @NamedQuery(name = "ImportOrderProduct.findByFileId", query = "SELECT i FROM ImportOrderProduct i WHERE i.fileId = :fileId"),})

public class ImportOrderProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "IMPORT_ORDER_PRODUCT_SEQ", sequenceName = "IMPORT_ORDER_PRODUCT_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IMPORT_ORDER_PRODUCT_SEQ")
    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Size(max = 31)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 250)
    @Column(name = "PRODUCT_DESCRIPTION")
    private String productDescription;
    @Size(max = 12)
    @Column(name = "PRODUCT_CODE")
    private String productCode;
    @Size(max = 35)
    @Column(name = "NATIONAL_CODE")
    private String nationalCode;
    @Size(max = 100)
    @Column(name = "NATIONAL_NAME")
    private String nationalName;
    @Size(max = 50)
    @Column(name = "CONFIRM_ANNOUNCE_NO")
    private String confirmAnnounceNo;
    @Column(name = "TOTAL")
    private Double total;
    @Size(max = 50)
    @Column(name = "TOTAL_UNIT_CODE")
    private String totalUnitCode;
    @Size(max = 20)
    @Column(name = "TOTAL_UNIT_NAME")
    private String totalUnitName;
    @Column(name = "NETWEIGHT")
    private Double netweight;
    @Size(max = 18)
    @Column(name = "NETWEIGHT_UNIT_CODE")
    private String netweightUnitCode;
    @Size(max = 18)
    @Column(name = "NETWEIGHT_UNIT_NAME")
    private String netweightUnitName;
    @Column(name = "BASE_UNIT")
    private Long baseUnit;
    @Column(name = "DATE_OF_MANUFACTURER")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfManufacturer;
    @Column(name = "EXPIRED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiredDate;
    @Size(max = 50)
    @Column(name = "PRODUCT_NUMBER")
    private String productNumber;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "HS_CODE")
    private String hsCode;
    @Column(name = "MANUFACTURER")
    private String manufacTurer;
    @Column(name = "MANUFACTURER_ADDRESS")
    private String manufacturerAddress;
//Bổ sung kết quả
    @Column(name = "CHECK_METHOD_CODE")
    private String checkMethodCode;
    @Column(name = "CHECK_METHOD_NAME")
    private String checkMethodName;
    @Column(name = "PASS")
    private Long pass;
    @Column(name = "REASON")
    private String reason;
    //Bổ sung hồ sơ xử lý không đạt
    @Column(name = "PROCESS_TYPE")
    private Long processType;
    @Column(name = "PROCESS_TYPE_NAME")
    private String processTypeName;
    @Column(name = "RE_PROCESS")
    private Long reProcess;
    @Column(name = "COMMENTS")
    private String comments;
    @Column(name = "RESULTS")
    private String results;
//Bo sung kn lai
    @Column(name = "RE_TEST")
    private Long reTest;
    @Column(name = "QR_CODE")
    private byte[] qrCode;
    @Column(name = "HAVE_QR_CODE")
    private Long haveQrCode;

    public ImportOrderProduct() {
    }

    public ImportOrderProduct(Long productId) {
        this.productId = productId;
    }

    public Long getReTest() {
        return reTest;
    }

    public void setReTest(Long reTest) {
        this.reTest = reTest;
    }

    public String getResult() {
        return results;
    }

    public void setResult(String results) {
        this.results = results;
    }

    public Long getProcessType() {
        return processType;
    }

    public void setProcessType(Long processType) {
        this.processType = processType;
    }

    public String getProcessTypeName() {
        return processTypeName;
    }

    public void setProcessTypeName(String processTypeName) {
        this.processTypeName = processTypeName;
    }

    public Long getReProcess() {
        return reProcess;
    }

    public void setReProcess(Long reProcess) {
        this.reProcess = reProcess;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getPass() {
        return pass;
    }

    public void setPass(Long pass) {
        this.pass = pass;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCheckMethodCode() {
        return checkMethodCode;
    }

    public void setCheckMethodCode(String checkMethodCode) {
        this.checkMethodCode = checkMethodCode;
    }

    public String getCheckMethodName() {
        return checkMethodName;
    }

    public void setCheckMethodName(String checkMethodName) {
        this.checkMethodName = checkMethodName;
    }

    public String getManufacTurer() {
        return manufacTurer;
    }

    public void setManufacTurer(String manufacTurer) {
        this.manufacTurer = manufacTurer;
    }

    public String getManufacturerAddress() {
        return manufacturerAddress;
    }

    public void setManufacturerAddress(String manufacturerAddress) {
        this.manufacturerAddress = manufacturerAddress;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getNationalName() {
        return nationalName;
    }

    public void setNationalName(String nationalName) {
        this.nationalName = nationalName;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getTotalUnitCode() {
        return totalUnitCode;
    }

    public void setTotalUnitCode(String totalUnitCode) {
        this.totalUnitCode = totalUnitCode;
    }

    public String getTotalUnitName() {
        return totalUnitName;
    }

    public void setTotalUnitName(String totalUnitName) {
        this.totalUnitName = totalUnitName;
    }

    public Double getNetweight() {
        return netweight;
    }

    public void setNetweight(Double netweight) {
        this.netweight = netweight;
    }

    public String getNetweightUnitCode() {
        return netweightUnitCode;
    }

    public void setNetweightUnitCode(String netweightUnitCode) {
        this.netweightUnitCode = netweightUnitCode;
    }

    public String getNetweightUnitName() {
        return netweightUnitName;
    }

    public void setNetweightUnitName(String netweightUnitName) {
        this.netweightUnitName = netweightUnitName;
    }

    public Long getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(Long baseUnit) {
        this.baseUnit = baseUnit;
    }

    public Date getDateOfManufacturer() {
        return dateOfManufacturer;
    }

    public void setDateOfManufacturer(Date dateOfManufacturer) {
        this.dateOfManufacturer = dateOfManufacturer;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImportOrderProduct)) {
            return false;
        }
        ImportOrderProduct other = (ImportOrderProduct) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importOrder.BO.ImportOrderProduct[ productId=" + productId + " ]";
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public byte[] getQrCode() {
        return qrCode;
    }

    public void setQrCode(byte[] qrCode) {
        this.qrCode = qrCode;
    }

    public Long getHaveQrCode() {
        return haveQrCode;
    }

    public void setHaveQrCode(Long haveQrCode) {
        this.haveQrCode = haveQrCode;
    }

}
