/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "PROCESS_DOCUMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcessDocument.findAll", query = "SELECT p FROM ProcessDocument p"),
    @NamedQuery(name = "ProcessDocument.findById", query = "SELECT p FROM ProcessDocument p WHERE p.id = :id"),
    @NamedQuery(name = "ProcessDocument.findByFileId", query = "SELECT p FROM ProcessDocument p WHERE p.fileId = :fileId"),
    @NamedQuery(name = "ProcessDocument.findByProcessName", query = "SELECT p FROM ProcessDocument p WHERE p.processName = :processName"),
    @NamedQuery(name = "ProcessDocument.findByCheckDate", query = "SELECT p FROM ProcessDocument p WHERE p.checkDate = :checkDate"),
    @NamedQuery(name = "ProcessDocument.findByCheckPlace", query = "SELECT p FROM ProcessDocument p WHERE p.checkPlace = :checkPlace"),
    @NamedQuery(name = "ProcessDocument.findByProcessPhone", query = "SELECT p FROM ProcessDocument p WHERE p.processPhone = :processPhone"),
    @NamedQuery(name = "ProcessDocument.findByComents", query = "SELECT p FROM ProcessDocument p WHERE p.coments = :coments"),
    @NamedQuery(name = "ProcessDocument.findByIsActive", query = "SELECT p FROM ProcessDocument p WHERE p.isActive = :isActive"),
    @NamedQuery(name = "ProcessDocument.findByCreatedDate", query = "SELECT p FROM ProcessDocument p WHERE p.createdDate = :createdDate"),
    @NamedQuery(name = "ProcessDocument.findByType", query = "SELECT p FROM ProcessDocument p WHERE p.type = :type")})
public class ProcessDocument implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "PROCESS__DOCUMENT_SEQ", sequenceName = "PROCESS__DOCUMENT_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROCESS__DOCUMENT_SEQ")
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 100)
    @Column(name = "PROCESS_NAME")
    private String processName;
    @Column(name = "CHECK_DATE")
//    @Temporal(TemporalType.DATE)
//    private Date checkDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkDate;
    @Size(max = 100)
    @Column(name = "CHECK_PLACE")
    private String checkPlace;
    @Size(max = 40)
    @Column(name = "PROCESS_PHONE")
    private String processPhone;
    @Size(max = 1000)
    @Column(name = "COMENTS")
    private String coments;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    @Column(name = "TYPE")
    private Long type;

    public ProcessDocument() {
    }

    public ProcessDocument(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getProcessPhone() {
        return processPhone;
    }

    public void setProcessPhone(String processPhone) {
        this.processPhone = processPhone;
    }

    public String getComents() {
        return coments;
    }

    public void setComents(String coments) {
        this.coments = coments;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcessDocument)) {
            return false;
        }
        ProcessDocument other = (ProcessDocument) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importOrder.BO.ProcessDocument[ id=" + id + " ]";
    }

}
