/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import java.io.Serializable;
import java.lang.Long;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "PRODUCT_PRODUCT_TARGET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductProductTarget.findAll", query = "SELECT p FROM ProductProductTarget p"),
    @NamedQuery(name = "ProductProductTarget.findById", query = "SELECT p FROM ProductProductTarget p WHERE p.id = :id"),
    @NamedQuery(name = "ProductProductTarget.findByProductId", query = "SELECT p FROM ProductProductTarget p WHERE p.productId = :productId"),
    @NamedQuery(name = "ProductProductTarget.findByProductTargetId", query = "SELECT p FROM ProductProductTarget p WHERE p.productTargetId = :productTargetId"),
    @NamedQuery(name = "ProductProductTarget.findByTestValue", query = "SELECT p FROM ProductProductTarget p WHERE p.testValue = :testValue"),
    @NamedQuery(name = "ProductProductTarget.findByMaxValue", query = "SELECT p FROM ProductProductTarget p WHERE p.maxValue = :maxValue"),
    @NamedQuery(name = "ProductProductTarget.findByComments", query = "SELECT p FROM ProductProductTarget p WHERE p.comments = :comments"),
    @NamedQuery(name = "ProductProductTarget.findByIsActive", query = "SELECT p FROM ProductProductTarget p WHERE p.isActive = :isActive"),
    @NamedQuery(name = "ProductProductTarget.findByCreatedDate", query = "SELECT p FROM ProductProductTarget p WHERE p.createdDate = :createdDate"),
    @NamedQuery(name = "ProductProductTarget.findByCreatedBy", query = "SELECT p FROM ProductProductTarget p WHERE p.createdBy = :createdBy")})
public class ProductProductTarget implements Serializable {

    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Column(name = "PRODUCT_TARGET_ID")
    private Long productTargetId;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "CREATED_BY")
    private Long createdBy;
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation

    @SequenceGenerator(name = "PRODUCT_PRODUCT_TARGET_SEQ", sequenceName = "PRODUCT_PRODUCT_TARGET_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCT_PRODUCT_TARGET_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 60)
    @Column(name = "TEST_VALUE")
    private String testValue;
    @Size(max = 60)
    @Column(name = "MAX_VALUE")
    private String maxValue;
    @Size(max = 500)
    @Column(name = "COMMENTS")
    private String comments;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "COST")
    private Long cost;
    @Column(name = "TYPE")
    private String type;
    @Size(max = 50)
    @Column(name = "TYPE_NAME")
    private String typeName;
    @Size(max = 200)
    @Column(name = "NAME")
    private String name;
    @Size(max = 1000)
    @Column(name = "TEST_METHOD")
    private String testMethod;
    @Column(name = "PASS")
    private Long pass;

    @Size(max = 1000)

    public ProductProductTarget() {
    }

    public ProductProductTarget(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getTestValue() {
        return testValue;
    }

    public void setTestValue(String testValue) {
        this.testValue = testValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getPass() {
        return pass;
    }

    public void setPass(Long pass) {
        this.pass = pass;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductProductTarget)) {
            return false;
        }
        ProductProductTarget other = (ProductProductTarget) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importOrder.BO.ProductProductTarget[ id=" + id + " ]";
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTestMethod() {
        return testMethod;
    }

    public void setTestMethod(String testMethod) {
        this.testMethod = testMethod;
    }

    public Long getProductTargetId() {
        return productTargetId;
    }

    public void setProductTargetId(Long productTargetId) {
        this.productTargetId = productTargetId;
    }
}
