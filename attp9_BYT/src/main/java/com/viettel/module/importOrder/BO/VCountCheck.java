/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tichnv
 */
@Entity
@Table(name = "V_COUNT_CHECK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCountCheck.findAll", query = "SELECT v FROM VCountCheck v"),
    @NamedQuery(name = "VCountCheck.findByProductId", query = "SELECT v FROM VCountCheck v WHERE v.productId = :productId"),
    @NamedQuery(name = "VCountCheck.findByProductName", query = "SELECT v FROM VCountCheck v WHERE v.productName = :productName"),
    @NamedQuery(name = "VCountCheck.findByProductDescription", query = "SELECT v FROM VCountCheck v WHERE v.productDescription = :productDescription"),
    @NamedQuery(name = "VCountCheck.findByProductCode", query = "SELECT v FROM VCountCheck v WHERE v.productCode = :productCode"),
    @NamedQuery(name = "VCountCheck.findByNationalCode", query = "SELECT v FROM VCountCheck v WHERE v.nationalCode = :nationalCode"),
    @NamedQuery(name = "VCountCheck.findByNationalName", query = "SELECT v FROM VCountCheck v WHERE v.nationalName = :nationalName"),
    @NamedQuery(name = "VCountCheck.findByConfirmAnnounceNo", query = "SELECT v FROM VCountCheck v WHERE v.confirmAnnounceNo = :confirmAnnounceNo"),
    @NamedQuery(name = "VCountCheck.findByTotalUnitCode", query = "SELECT v FROM VCountCheck v WHERE v.totalUnitCode = :totalUnitCode"),
    @NamedQuery(name = "VCountCheck.findByTotalUnitName", query = "SELECT v FROM VCountCheck v WHERE v.totalUnitName = :totalUnitName"),
    @NamedQuery(name = "VCountCheck.findByNetweight", query = "SELECT v FROM VCountCheck v WHERE v.netweight = :netweight"),
    @NamedQuery(name = "VCountCheck.findByNetweightUnitCode", query = "SELECT v FROM VCountCheck v WHERE v.netweightUnitCode = :netweightUnitCode"),
    @NamedQuery(name = "VCountCheck.findByNetweightUnitName", query = "SELECT v FROM VCountCheck v WHERE v.netweightUnitName = :netweightUnitName"),
    @NamedQuery(name = "VCountCheck.findByBaseUnit", query = "SELECT v FROM VCountCheck v WHERE v.baseUnit = :baseUnit"),
    @NamedQuery(name = "VCountCheck.findByDateOfManufacturer", query = "SELECT v FROM VCountCheck v WHERE v.dateOfManufacturer = :dateOfManufacturer"),
    @NamedQuery(name = "VCountCheck.findByExpiredDate", query = "SELECT v FROM VCountCheck v WHERE v.expiredDate = :expiredDate"),
    @NamedQuery(name = "VCountCheck.findByProductNumber", query = "SELECT v FROM VCountCheck v WHERE v.productNumber = :productNumber"),
    @NamedQuery(name = "VCountCheck.findByFileId", query = "SELECT v FROM VCountCheck v WHERE v.fileId = :fileId"),
    @NamedQuery(name = "VCountCheck.findByHsCode", query = "SELECT v FROM VCountCheck v WHERE v.hsCode = :hsCode"),
    @NamedQuery(name = "VCountCheck.findByManufacturer", query = "SELECT v FROM VCountCheck v WHERE v.manufacturer = :manufacturer"),
    @NamedQuery(name = "VCountCheck.findByManufacturerAddress", query = "SELECT v FROM VCountCheck v WHERE v.manufacturerAddress = :manufacturerAddress"),
    @NamedQuery(name = "VCountCheck.findByTotal", query = "SELECT v FROM VCountCheck v WHERE v.total = :total"),
    @NamedQuery(name = "VCountCheck.findByCheckMethodCode", query = "SELECT v FROM VCountCheck v WHERE v.checkMethodCode = :checkMethodCode"),
    @NamedQuery(name = "VCountCheck.findByCheckMethodName", query = "SELECT v FROM VCountCheck v WHERE v.checkMethodName = :checkMethodName"),
    @NamedQuery(name = "VCountCheck.findByPass", query = "SELECT v FROM VCountCheck v WHERE v.pass = :pass"),
    @NamedQuery(name = "VCountCheck.findByReason", query = "SELECT v FROM VCountCheck v WHERE v.reason = :reason"),
    @NamedQuery(name = "VCountCheck.findByProcessType", query = "SELECT v FROM VCountCheck v WHERE v.processType = :processType"),
    @NamedQuery(name = "VCountCheck.findByProcessTypeName", query = "SELECT v FROM VCountCheck v WHERE v.processTypeName = :processTypeName"),
    @NamedQuery(name = "VCountCheck.findByReProcess", query = "SELECT v FROM VCountCheck v WHERE v.reProcess = :reProcess"),
    @NamedQuery(name = "VCountCheck.findByComments", query = "SELECT v FROM VCountCheck v WHERE v.comments = :comments"),
    @NamedQuery(name = "VCountCheck.findByResults", query = "SELECT v FROM VCountCheck v WHERE v.results = :results"),
    @NamedQuery(name = "VCountCheck.findByReTest", query = "SELECT v FROM VCountCheck v WHERE v.reTest = :reTest"),
    @NamedQuery(name = "VCountCheck.findByCreatorId", query = "SELECT v FROM VCountCheck v WHERE v.creatorId = :creatorId")})
public class VCountCheck implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Size(max = 250)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 250)
    @Column(name = "PRODUCT_DESCRIPTION")
    private String productDescription;
    @Size(max = 30)
    @Column(name = "PRODUCT_CODE")
    private String productCode;
    @Size(max = 35)
    @Column(name = "NATIONAL_CODE")
    private String nationalCode;
    @Size(max = 100)
    @Column(name = "NATIONAL_NAME")
    private String nationalName;
    @Size(max = 50)
    @Column(name = "CONFIRM_ANNOUNCE_NO")
    private String confirmAnnounceNo;
    @Size(max = 50)
    @Column(name = "TOTAL_UNIT_CODE")
    private String totalUnitCode;
    @Size(max = 20)
    @Column(name = "TOTAL_UNIT_NAME")
    private String totalUnitName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "NETWEIGHT")
    private Long netweight;
    @Size(max = 18)
    @Column(name = "NETWEIGHT_UNIT_CODE")
    private String netweightUnitCode;
    @Size(max = 18)
    @Column(name = "NETWEIGHT_UNIT_NAME")
    private String netweightUnitName;
    @Column(name = "BASE_UNIT")
    private Long baseUnit;
    @Column(name = "DATE_OF_MANUFACTURER")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfManufacturer;
    @Column(name = "EXPIRED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiredDate;
    @Size(max = 50)
    @Column(name = "PRODUCT_NUMBER")
    private String productNumber;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 50)
    @Column(name = "HS_CODE")
    private String hsCode;
    @Size(max = 100)
    @Column(name = "MANUFACTURER")
    private String manufacturer;
    @Size(max = 250)
    @Column(name = "MANUFACTURER_ADDRESS")
    private String manufacturerAddress;
    @Column(name = "TOTAL")
    private Long total;
    @Size(max = 50)
    @Column(name = "CHECK_METHOD_CODE")
    private String checkMethodCode;
    @Size(max = 200)
    @Column(name = "CHECK_METHOD_NAME")
    private String checkMethodName;
    @Column(name = "PASS")
    private Short pass;
    @Size(max = 500)
    @Column(name = "REASON")
    private String reason;
    @Column(name = "PROCESS_TYPE")
    private Short processType;
    @Size(max = 50)
    @Column(name = "PROCESS_TYPE_NAME")
    private String processTypeName;
    @Column(name = "RE_PROCESS")
    private Short reProcess;
    @Size(max = 500)
    @Column(name = "COMMENTS")
    private String comments;
    @Size(max = 500)
    @Column(name = "RESULTS")
    private String results;
    @Column(name = "RE_TEST")
    private Short reTest;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;

    public VCountCheck() {
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getNationalName() {
        return nationalName;
    }

    public void setNationalName(String nationalName) {
        this.nationalName = nationalName;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo;
    }

    public String getTotalUnitCode() {
        return totalUnitCode;
    }

    public void setTotalUnitCode(String totalUnitCode) {
        this.totalUnitCode = totalUnitCode;
    }

    public String getTotalUnitName() {
        return totalUnitName;
    }

    public void setTotalUnitName(String totalUnitName) {
        this.totalUnitName = totalUnitName;
    }

    public Long getNetweight() {
        return netweight;
    }

    public void setNetweight(Long netweight) {
        this.netweight = netweight;
    }

    public String getNetweightUnitCode() {
        return netweightUnitCode;
    }

    public void setNetweightUnitCode(String netweightUnitCode) {
        this.netweightUnitCode = netweightUnitCode;
    }

    public String getNetweightUnitName() {
        return netweightUnitName;
    }

    public void setNetweightUnitName(String netweightUnitName) {
        this.netweightUnitName = netweightUnitName;
    }

    public Long getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(Long baseUnit) {
        this.baseUnit = baseUnit;
    }

    public Date getDateOfManufacturer() {
        return dateOfManufacturer;
    }

    public void setDateOfManufacturer(Date dateOfManufacturer) {
        this.dateOfManufacturer = dateOfManufacturer;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerAddress() {
        return manufacturerAddress;
    }

    public void setManufacturerAddress(String manufacturerAddress) {
        this.manufacturerAddress = manufacturerAddress;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getCheckMethodCode() {
        return checkMethodCode;
    }

    public void setCheckMethodCode(String checkMethodCode) {
        this.checkMethodCode = checkMethodCode;
    }

    public String getCheckMethodName() {
        return checkMethodName;
    }

    public void setCheckMethodName(String checkMethodName) {
        this.checkMethodName = checkMethodName;
    }

    public Short getPass() {
        return pass;
    }

    public void setPass(Short pass) {
        this.pass = pass;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Short getProcessType() {
        return processType;
    }

    public void setProcessType(Short processType) {
        this.processType = processType;
    }

    public String getProcessTypeName() {
        return processTypeName;
    }

    public void setProcessTypeName(String processTypeName) {
        this.processTypeName = processTypeName;
    }

    public Short getReProcess() {
        return reProcess;
    }

    public void setReProcess(Short reProcess) {
        this.reProcess = reProcess;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public Short getReTest() {
        return reTest;
    }

    public void setReTest(Short reTest) {
        this.reTest = reTest;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

}
