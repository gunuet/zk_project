/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tichnv
 */
@Entity
@Table(name = "V_COUNT_STATUS_FILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCountStatusFile.findAll", query = "SELECT v FROM VCountStatusFile v"),
    @NamedQuery(name = "VCountStatusFile.findByDeptId", query = "SELECT v FROM VCountStatusFile v WHERE v.deptId = :deptId"),
    @NamedQuery(name = "VCountStatusFile.findByDeptPath", query = "SELECT v FROM VCountStatusFile v WHERE v.deptPath = :deptPath"),
    @NamedQuery(name = "VCountStatusFile.findByReceiveGroupId", query = "SELECT v FROM VCountStatusFile v WHERE v.receiveGroupId = :receiveGroupId"),
    @NamedQuery(name = "VCountStatusFile.findByStatus", query = "SELECT v FROM VCountStatusFile v WHERE v.status = :status")})
public class VCountStatusFile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "DEPT_ID")
    private Long deptId;
    @Size(max = 4000)
    @Column(name = "DEPT_PATH")
    private String deptPath;
    @Column(name = "RECEIVE_GROUP_ID")
    private Long receiveGroupId;
    @Column(name = "STATUS")
    private Long status;

    public VCountStatusFile() {
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptPath() {
        return deptPath;
    }

    public void setDeptPath(String deptPath) {
        this.deptPath = deptPath;
    }

    public Long getReceiveGroupId() {
        return receiveGroupId;
    }

    public void setReceiveGroupId(Long receiveGroupId) {
        this.receiveGroupId = receiveGroupId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

}
