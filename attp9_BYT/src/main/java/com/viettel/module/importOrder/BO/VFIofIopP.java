/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author binh.ninhthanh
 */
@Entity
@Table(name = "V_F_IOF_IOP_P")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VFIofIopP.findAll", query = "SELECT v FROM VFIofIopP v")
    , @NamedQuery(name = "VFIofIopP.findByCosFileId", query = "SELECT v FROM VFIofIopP v WHERE v.cosFileId = :cosFileId")
    , @NamedQuery(name = "VFIofIopP.findByImportOrderFileId", query = "SELECT v FROM VFIofIopP v WHERE v.importOrderFileId = :importOrderFileId")
    , @NamedQuery(name = "VFIofIopP.findByFileId", query = "SELECT v FROM VFIofIopP v WHERE v.fileId = :fileId")
    , @NamedQuery(name = "VFIofIopP.findByNswFileCode", query = "SELECT v FROM VFIofIopP v WHERE v.nswFileCode = :nswFileCode")
    , @NamedQuery(name = "VFIofIopP.findByDocumentTypeCode", query = "SELECT v FROM VFIofIopP v WHERE v.documentTypeCode = :documentTypeCode")
    , @NamedQuery(name = "VFIofIopP.findByTransNo", query = "SELECT v FROM VFIofIopP v WHERE v.transNo = :transNo")
    , @NamedQuery(name = "VFIofIopP.findByContractNo", query = "SELECT v FROM VFIofIopP v WHERE v.contractNo = :contractNo")
    , @NamedQuery(name = "VFIofIopP.findByBillNo", query = "SELECT v FROM VFIofIopP v WHERE v.billNo = :billNo")
    , @NamedQuery(name = "VFIofIopP.findByGoodsOwnerName", query = "SELECT v FROM VFIofIopP v WHERE v.goodsOwnerName = :goodsOwnerName")
    , @NamedQuery(name = "VFIofIopP.findByGoodsOwnerAddress", query = "SELECT v FROM VFIofIopP v WHERE v.goodsOwnerAddress = :goodsOwnerAddress")
    , @NamedQuery(name = "VFIofIopP.findByGoodsOwnerPhone", query = "SELECT v FROM VFIofIopP v WHERE v.goodsOwnerPhone = :goodsOwnerPhone")
    , @NamedQuery(name = "VFIofIopP.findByGoodsOwnerFax", query = "SELECT v FROM VFIofIopP v WHERE v.goodsOwnerFax = :goodsOwnerFax")
    , @NamedQuery(name = "VFIofIopP.findByGoodsOwnerEmail", query = "SELECT v FROM VFIofIopP v WHERE v.goodsOwnerEmail = :goodsOwnerEmail")
    , @NamedQuery(name = "VFIofIopP.findByResponsiblePersonName", query = "SELECT v FROM VFIofIopP v WHERE v.responsiblePersonName = :responsiblePersonName")
    , @NamedQuery(name = "VFIofIopP.findByResponsiblePersonAddress", query = "SELECT v FROM VFIofIopP v WHERE v.responsiblePersonAddress = :responsiblePersonAddress")
    , @NamedQuery(name = "VFIofIopP.findByResponsiblePersonPhone", query = "SELECT v FROM VFIofIopP v WHERE v.responsiblePersonPhone = :responsiblePersonPhone")
    , @NamedQuery(name = "VFIofIopP.findByResponsiblePersonFax", query = "SELECT v FROM VFIofIopP v WHERE v.responsiblePersonFax = :responsiblePersonFax")
    , @NamedQuery(name = "VFIofIopP.findByResponsiblePersonEmail", query = "SELECT v FROM VFIofIopP v WHERE v.responsiblePersonEmail = :responsiblePersonEmail")
    , @NamedQuery(name = "VFIofIopP.findByExporterName", query = "SELECT v FROM VFIofIopP v WHERE v.exporterName = :exporterName")
    , @NamedQuery(name = "VFIofIopP.findByExporterAddress", query = "SELECT v FROM VFIofIopP v WHERE v.exporterAddress = :exporterAddress")
    , @NamedQuery(name = "VFIofIopP.findByExporterPhone", query = "SELECT v FROM VFIofIopP v WHERE v.exporterPhone = :exporterPhone")
    , @NamedQuery(name = "VFIofIopP.findByExporterFax", query = "SELECT v FROM VFIofIopP v WHERE v.exporterFax = :exporterFax")
    , @NamedQuery(name = "VFIofIopP.findByExporterEmail", query = "SELECT v FROM VFIofIopP v WHERE v.exporterEmail = :exporterEmail")
    , @NamedQuery(name = "VFIofIopP.findByComingDate", query = "SELECT v FROM VFIofIopP v WHERE v.comingDate = :comingDate")
    , @NamedQuery(name = "VFIofIopP.findByExporterGateCode", query = "SELECT v FROM VFIofIopP v WHERE v.exporterGateCode = :exporterGateCode")
    , @NamedQuery(name = "VFIofIopP.findByExporterGateName", query = "SELECT v FROM VFIofIopP v WHERE v.exporterGateName = :exporterGateName")
    , @NamedQuery(name = "VFIofIopP.findByImporterGateCode", query = "SELECT v FROM VFIofIopP v WHERE v.importerGateCode = :importerGateCode")
    , @NamedQuery(name = "VFIofIopP.findByImporterGateName", query = "SELECT v FROM VFIofIopP v WHERE v.importerGateName = :importerGateName")
    , @NamedQuery(name = "VFIofIopP.findByCustomsBranchCode", query = "SELECT v FROM VFIofIopP v WHERE v.customsBranchCode = :customsBranchCode")
    , @NamedQuery(name = "VFIofIopP.findByCustomsBranchName", query = "SELECT v FROM VFIofIopP v WHERE v.customsBranchName = :customsBranchName")
    , @NamedQuery(name = "VFIofIopP.findByCheckTime", query = "SELECT v FROM VFIofIopP v WHERE v.checkTime = :checkTime")
    , @NamedQuery(name = "VFIofIopP.findByCheckPlace", query = "SELECT v FROM VFIofIopP v WHERE v.checkPlace = :checkPlace")
    , @NamedQuery(name = "VFIofIopP.findByDeptCode", query = "SELECT v FROM VFIofIopP v WHERE v.deptCode = :deptCode")
    , @NamedQuery(name = "VFIofIopP.findByDeptName", query = "SELECT v FROM VFIofIopP v WHERE v.deptName = :deptName")
    , @NamedQuery(name = "VFIofIopP.findBySignPlace", query = "SELECT v FROM VFIofIopP v WHERE v.signPlace = :signPlace")
    , @NamedQuery(name = "VFIofIopP.findBySignDate", query = "SELECT v FROM VFIofIopP v WHERE v.signDate = :signDate")
    , @NamedQuery(name = "VFIofIopP.findBySignName", query = "SELECT v FROM VFIofIopP v WHERE v.signName = :signName")
    , @NamedQuery(name = "VFIofIopP.findBySignedData", query = "SELECT v FROM VFIofIopP v WHERE v.signedData = :signedData")
    , @NamedQuery(name = "VFIofIopP.findByFileType", query = "SELECT v FROM VFIofIopP v WHERE v.fileType = :fileType")
    , @NamedQuery(name = "VFIofIopP.findByFileTypeName", query = "SELECT v FROM VFIofIopP v WHERE v.fileTypeName = :fileTypeName")
    , @NamedQuery(name = "VFIofIopP.findByFileCode", query = "SELECT v FROM VFIofIopP v WHERE v.fileCode = :fileCode")
    , @NamedQuery(name = "VFIofIopP.findByFileName", query = "SELECT v FROM VFIofIopP v WHERE v.fileName = :fileName")
    , @NamedQuery(name = "VFIofIopP.findByStatus", query = "SELECT v FROM VFIofIopP v WHERE v.status = :status")
    , @NamedQuery(name = "VFIofIopP.findByTaxCode", query = "SELECT v FROM VFIofIopP v WHERE v.taxCode = :taxCode")
    , @NamedQuery(name = "VFIofIopP.findByBusinessId", query = "SELECT v FROM VFIofIopP v WHERE v.businessId = :businessId")
    , @NamedQuery(name = "VFIofIopP.findByBusinessName", query = "SELECT v FROM VFIofIopP v WHERE v.businessName = :businessName")
    , @NamedQuery(name = "VFIofIopP.findByBusinessAddress", query = "SELECT v FROM VFIofIopP v WHERE v.businessAddress = :businessAddress")
    , @NamedQuery(name = "VFIofIopP.findByBusinessPhone", query = "SELECT v FROM VFIofIopP v WHERE v.businessPhone = :businessPhone")
    , @NamedQuery(name = "VFIofIopP.findByBusinessFax", query = "SELECT v FROM VFIofIopP v WHERE v.businessFax = :businessFax")
    , @NamedQuery(name = "VFIofIopP.findByCreateDate", query = "SELECT v FROM VFIofIopP v WHERE v.createDate = :createDate")
    , @NamedQuery(name = "VFIofIopP.findByModifyDate", query = "SELECT v FROM VFIofIopP v WHERE v.modifyDate = :modifyDate")
    , @NamedQuery(name = "VFIofIopP.findByCreatorId", query = "SELECT v FROM VFIofIopP v WHERE v.creatorId = :creatorId")
    , @NamedQuery(name = "VFIofIopP.findByCreatorName", query = "SELECT v FROM VFIofIopP v WHERE v.creatorName = :creatorName")
    , @NamedQuery(name = "VFIofIopP.findByCreateDeptId", query = "SELECT v FROM VFIofIopP v WHERE v.createDeptId = :createDeptId")
    , @NamedQuery(name = "VFIofIopP.findByCreateDeptName", query = "SELECT v FROM VFIofIopP v WHERE v.createDeptName = :createDeptName")
    , @NamedQuery(name = "VFIofIopP.findByIsActive", query = "SELECT v FROM VFIofIopP v WHERE v.isActive = :isActive")
    , @NamedQuery(name = "VFIofIopP.findByVersion", query = "SELECT v FROM VFIofIopP v WHERE v.version = :version")
    , @NamedQuery(name = "VFIofIopP.findByParentFileId", query = "SELECT v FROM VFIofIopP v WHERE v.parentFileId = :parentFileId")
    , @NamedQuery(name = "VFIofIopP.findByIsTemp", query = "SELECT v FROM VFIofIopP v WHERE v.isTemp = :isTemp")
    , @NamedQuery(name = "VFIofIopP.findByStartDate", query = "SELECT v FROM VFIofIopP v WHERE v.startDate = :startDate")
    , @NamedQuery(name = "VFIofIopP.findByNumDayProcess", query = "SELECT v FROM VFIofIopP v WHERE v.numDayProcess = :numDayProcess")
    , @NamedQuery(name = "VFIofIopP.findByDeadline", query = "SELECT v FROM VFIofIopP v WHERE v.deadline = :deadline")
    , @NamedQuery(name = "VFIofIopP.findByFinishDate", query = "SELECT v FROM VFIofIopP v WHERE v.finishDate = :finishDate")
    , @NamedQuery(name = "VFIofIopP.findByProductName", query = "SELECT v FROM VFIofIopP v WHERE v.productName = :productName")
    , @NamedQuery(name = "VFIofIopP.findByProductDescription", query = "SELECT v FROM VFIofIopP v WHERE v.productDescription = :productDescription")
    , @NamedQuery(name = "VFIofIopP.findByProductCode", query = "SELECT v FROM VFIofIopP v WHERE v.productCode = :productCode")
    , @NamedQuery(name = "VFIofIopP.findByNationalCode", query = "SELECT v FROM VFIofIopP v WHERE v.nationalCode = :nationalCode")
    , @NamedQuery(name = "VFIofIopP.findByNationalName", query = "SELECT v FROM VFIofIopP v WHERE v.nationalName = :nationalName")
    , @NamedQuery(name = "VFIofIopP.findByConfirmAnnounceNo", query = "SELECT v FROM VFIofIopP v WHERE v.confirmAnnounceNo = :confirmAnnounceNo")
    , @NamedQuery(name = "VFIofIopP.findByHaveQrCode", query = "SELECT v FROM VFIofIopP v WHERE v.haveQrCode = :haveQrCode")
    , @NamedQuery(name = "VFIofIopP.findByProductId", query = "SELECT v FROM VFIofIopP v WHERE v.productId = :productId")
    , @NamedQuery(name = "VFIofIopP.findByReceiveNo", query = "SELECT v FROM VFIofIopP v WHERE v.receiveNo = :receiveNo")
    , @NamedQuery(name = "VFIofIopP.findByReceiveDate", query = "SELECT v FROM VFIofIopP v WHERE v.receiveDate = :receiveDate")
    , @NamedQuery(name = "VFIofIopP.findByReceiveDeptName", query = "SELECT v FROM VFIofIopP v WHERE v.receiveDeptName = :receiveDeptName")
    , @NamedQuery(name = "VFIofIopP.findBySignerName", query = "SELECT v FROM VFIofIopP v WHERE v.signerName = :signerName")
    , @NamedQuery(name = "VFIofIopP.findByEffectiveDate", query = "SELECT v FROM VFIofIopP v WHERE v.effectiveDate = :effectiveDate")})
public class VFIofIopP implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMPORT_ORDER_FILE_ID")
    private Long importOrderFileId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 4000)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Size(max = 4000)
    @Column(name = "TRANS_NO")
    private String transNo;
    @Size(max = 4000)
    @Column(name = "CONTRACT_NO")
    private String contractNo;
    @Size(max = 4000)
    @Column(name = "BILL_NO")
    private String billNo;
    @Size(max = 4000)
    @Column(name = "GOODS_OWNER_NAME")
    private String goodsOwnerName;
    @Size(max = 4000)
    @Column(name = "GOODS_OWNER_ADDRESS")
    private String goodsOwnerAddress;
    @Size(max = 4000)
    @Column(name = "GOODS_OWNER_PHONE")
    private String goodsOwnerPhone;
    @Size(max = 4000)
    @Column(name = "GOODS_OWNER_FAX")
    private String goodsOwnerFax;
    @Size(max = 4000)
    @Column(name = "GOODS_OWNER_EMAIL")
    private String goodsOwnerEmail;
    @Size(max = 4000)
    @Column(name = "RESPONSIBLE_PERSON_NAME")
    private String responsiblePersonName;
    @Size(max = 4000)
    @Column(name = "RESPONSIBLE_PERSON_ADDRESS")
    private String responsiblePersonAddress;
    @Size(max = 4000)
    @Column(name = "RESPONSIBLE_PERSON_PHONE")
    private String responsiblePersonPhone;
    @Size(max = 4000)
    @Column(name = "RESPONSIBLE_PERSON_FAX")
    private String responsiblePersonFax;
    @Size(max = 4000)
    @Column(name = "RESPONSIBLE_PERSON_EMAIL")
    private String responsiblePersonEmail;
    @Size(max = 4000)
    @Column(name = "EXPORTER_NAME")
    private String exporterName;
    @Size(max = 4000)
    @Column(name = "EXPORTER_ADDRESS")
    private String exporterAddress;
    @Size(max = 765)
    @Column(name = "EXPORTER_PHONE")
    private String exporterPhone;
    @Size(max = 765)
    @Column(name = "EXPORTER_FAX")
    private String exporterFax;
    @Size(max = 765)
    @Column(name = "EXPORTER_EMAIL")
    private String exporterEmail;
    @Column(name = "COMING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comingDate;
    @Size(max = 1500)
    @Column(name = "EXPORTER_GATE_CODE")
    private String exporterGateCode;
    @Size(max = 4000)
    @Column(name = "EXPORTER_GATE_NAME")
    private String exporterGateName;
    @Size(max = 66)
    @Column(name = "IMPORTER_GATE_CODE")
    private String importerGateCode;
    @Size(max = 4000)
    @Column(name = "IMPORTER_GATE_NAME")
    private String importerGateName;
    @Size(max = 60)
    @Column(name = "CUSTOMS_BRANCH_CODE")
    private String customsBranchCode;
    @Size(max = 4000)
    @Column(name = "CUSTOMS_BRANCH_NAME")
    private String customsBranchName;
    @Column(name = "CHECK_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkTime;
    @Size(max = 4000)
    @Column(name = "CHECK_PLACE")
    private String checkPlace;
    @Size(max = 36)
    @Column(name = "DEPT_CODE")
    private String deptCode;
    @Size(max = 4000)
    @Column(name = "DEPT_NAME")
    private String deptName;
    @Size(max = 4000)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.DATE)
    private Date signDate;
    @Size(max = 4000)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Size(max = 4000)
    @Column(name = "SIGNED_DATA")
    private String signedData;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Size(max = 765)
    @Column(name = "FILE_TYPE_NAME")
    private String fileTypeName;
    @Size(max = 93)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 765)
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "STATUS")
    private Long status;
    @Size(max = 93)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Column(name = "BUSINESS_ID")
    private Long businessId;
    @Size(max = 765)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 1500)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 93)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 93)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 765)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 765)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "VERSION")
    private Long version;
    @Column(name = "PARENT_FILE_ID")
    private Long parentFileId;
    @Column(name = "IS_TEMP")
    private Short isTemp;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "NUM_DAY_PROCESS")
    private Long numDayProcess;
    @Column(name = "DEADLINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;
    @Column(name = "FINISH_DATE")
    @Temporal(TemporalType.DATE)
    private Date finishDate;
    @Size(max = 4000)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 4000)
    @Column(name = "PRODUCT_DESCRIPTION")
    private String productDescription;
    @Size(max = 4000)
    @Column(name = "PRODUCT_CODE")
    private String productCode;
    @Size(max = 105)
    @Column(name = "NATIONAL_CODE")
    private String nationalCode;
    @Size(max = 4000)
    @Column(name = "NATIONAL_NAME")
    private String nationalName;
    @Size(max = 4000)
    @Column(name = "CONFIRM_ANNOUNCE_NO")
    private String confirmAnnounceNo;
    @Column(name = "HAVE_QR_CODE")
    private Long haveQrCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRODUCT_ID")
    @Id
    private Long productId;
    @Column(name = "PASS")
    private Long pass;
    @Size(max = 150)
    @Column(name = "RECEIVE_NO")
    private String receiveNo;
    @Column(name = "RECEIVE_DATE")
    @Temporal(TemporalType.DATE)
    private Date receiveDate;
    @Size(max = 1500)
    @Column(name = "RECEIVE_DEPT_NAME")
    private String receiveDeptName;
    @Size(max = 750)
    @Column(name = "SIGNER_NAME")
    private String signerName;
    @Column(name = "EFFECTIVE_DATE")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;

    public VFIofIopP() {
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getImportOrderFileId() {
        return importOrderFileId;
    }

    public void setImportOrderFileId(Long importOrderFileId) {
        this.importOrderFileId = importOrderFileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return goodsOwnerEmail;
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = goodsOwnerEmail;
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public String getResponsiblePersonAddress() {
        return responsiblePersonAddress;
    }

    public void setResponsiblePersonAddress(String responsiblePersonAddress) {
        this.responsiblePersonAddress = responsiblePersonAddress;
    }

    public String getResponsiblePersonPhone() {
        return responsiblePersonPhone;
    }

    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
        this.responsiblePersonPhone = responsiblePersonPhone;
    }

    public String getResponsiblePersonFax() {
        return responsiblePersonFax;
    }

    public void setResponsiblePersonFax(String responsiblePersonFax) {
        this.responsiblePersonFax = responsiblePersonFax;
    }

    public String getResponsiblePersonEmail() {
        return responsiblePersonEmail;
    }

    public void setResponsiblePersonEmail(String responsiblePersonEmail) {
        this.responsiblePersonEmail = responsiblePersonEmail;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterFax() {
        return exporterFax;
    }

    public void setExporterFax(String exporterFax) {
        this.exporterFax = exporterFax;
    }

    public String getExporterEmail() {
        return exporterEmail;
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = exporterEmail;
    }

    public Date getComingDate() {
        return comingDate;
    }

    public void setComingDate(Date comingDate) {
        this.comingDate = comingDate;
    }

    public String getExporterGateCode() {
        return exporterGateCode;
    }

    public void setExporterGateCode(String exporterGateCode) {
        this.exporterGateCode = exporterGateCode;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateCode() {
        return importerGateCode;
    }

    public void setImporterGateCode(String importerGateCode) {
        this.importerGateCode = importerGateCode;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public String getCustomsBranchCode() {
        return customsBranchCode;
    }

    public void setCustomsBranchCode(String customsBranchCode) {
        this.customsBranchCode = customsBranchCode;
    }

    public String getCustomsBranchName() {
        return customsBranchName;
    }

    public void setCustomsBranchName(String customsBranchName) {
        this.customsBranchName = customsBranchName;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignedData() {
        return signedData;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getParentFileId() {
        return parentFileId;
    }

    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public Short getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(Short isTemp) {
        this.isTemp = isTemp;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Long getNumDayProcess() {
        return numDayProcess;
    }

    public void setNumDayProcess(Long numDayProcess) {
        this.numDayProcess = numDayProcess;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getNationalName() {
        return nationalName;
    }

    public void setNationalName(String nationalName) {
        this.nationalName = nationalName;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo;
    }

    public Long getHaveQrCode() {
        return haveQrCode;
    }

    public void setHaveQrCode(Long haveQrCode) {
        this.haveQrCode = haveQrCode;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getReceiveDeptName() {
        return receiveDeptName;
    }

    public void setReceiveDeptName(String receiveDeptName) {
        this.receiveDeptName = receiveDeptName;
    }

    public String getSignerName() {
        return signerName;
    }

    public void setSignerName(String signerName) {
        this.signerName = signerName;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Long getPass() {
        return pass;
    }

    public void setPass(Long pass) {
        this.pass = pass;
    }

}
