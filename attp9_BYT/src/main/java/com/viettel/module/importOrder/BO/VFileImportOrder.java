/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import com.viettel.utils.Constants;
import com.viettel.utils.Constants_CKS;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tichnv
 */
@Entity
@Table(name = "V_FILE_IMPORT_ORDER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VFileImportOrder.findAll", query = "SELECT v FROM VFileImportOrder v"),
    @NamedQuery(name = "VFileImportOrder.findByCosFileId", query = "SELECT v FROM VFileImportOrder v WHERE v.cosFileId = :cosFileId"),
    @NamedQuery(name = "VFileImportOrder.findByImportOrderFileId", query = "SELECT v FROM VFileImportOrder v WHERE v.importOrderFileId = :importOrderFileId"),
    @NamedQuery(name = "VFileImportOrder.findByFileId", query = "SELECT v FROM VFileImportOrder v WHERE v.fileId = :fileId"),
    @NamedQuery(name = "VFileImportOrder.findByNswFileCode", query = "SELECT v FROM VFileImportOrder v WHERE v.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "VFileImportOrder.findByDocumentTypeCode", query = "SELECT v FROM VFileImportOrder v WHERE v.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "VFileImportOrder.findByTransNo", query = "SELECT v FROM VFileImportOrder v WHERE v.transNo = :transNo"),
    @NamedQuery(name = "VFileImportOrder.findByContractNo", query = "SELECT v FROM VFileImportOrder v WHERE v.contractNo = :contractNo"),
    @NamedQuery(name = "VFileImportOrder.findByBillNo", query = "SELECT v FROM VFileImportOrder v WHERE v.billNo = :billNo"),
    @NamedQuery(name = "VFileImportOrder.findByGoodsOwnerName", query = "SELECT v FROM VFileImportOrder v WHERE v.goodsOwnerName = :goodsOwnerName"),
    @NamedQuery(name = "VFileImportOrder.findByGoodsOwnerAddress", query = "SELECT v FROM VFileImportOrder v WHERE v.goodsOwnerAddress = :goodsOwnerAddress"),
    @NamedQuery(name = "VFileImportOrder.findByGoodsOwnerPhone", query = "SELECT v FROM VFileImportOrder v WHERE v.goodsOwnerPhone = :goodsOwnerPhone"),
    @NamedQuery(name = "VFileImportOrder.findByGoodsOwnerFax", query = "SELECT v FROM VFileImportOrder v WHERE v.goodsOwnerFax = :goodsOwnerFax"),
    @NamedQuery(name = "VFileImportOrder.findByGoodsOwnerEmail", query = "SELECT v FROM VFileImportOrder v WHERE v.goodsOwnerEmail = :goodsOwnerEmail"),
    @NamedQuery(name = "VFileImportOrder.findByResponsiblePersonName", query = "SELECT v FROM VFileImportOrder v WHERE v.responsiblePersonName = :responsiblePersonName"),
    @NamedQuery(name = "VFileImportOrder.findByResponsiblePersonAddress", query = "SELECT v FROM VFileImportOrder v WHERE v.responsiblePersonAddress = :responsiblePersonAddress"),
    @NamedQuery(name = "VFileImportOrder.findByResponsiblePersonPhone", query = "SELECT v FROM VFileImportOrder v WHERE v.responsiblePersonPhone = :responsiblePersonPhone"),
    @NamedQuery(name = "VFileImportOrder.findByResponsiblePersonFax", query = "SELECT v FROM VFileImportOrder v WHERE v.responsiblePersonFax = :responsiblePersonFax"),
    @NamedQuery(name = "VFileImportOrder.findByResponsiblePersonEmail", query = "SELECT v FROM VFileImportOrder v WHERE v.responsiblePersonEmail = :responsiblePersonEmail"),
    @NamedQuery(name = "VFileImportOrder.findByExporterName", query = "SELECT v FROM VFileImportOrder v WHERE v.exporterName = :exporterName"),
    @NamedQuery(name = "VFileImportOrder.findByExporterAddress", query = "SELECT v FROM VFileImportOrder v WHERE v.exporterAddress = :exporterAddress"),
    @NamedQuery(name = "VFileImportOrder.findByExporterPhone", query = "SELECT v FROM VFileImportOrder v WHERE v.exporterPhone = :exporterPhone"),
    @NamedQuery(name = "VFileImportOrder.findByExporterFax", query = "SELECT v FROM VFileImportOrder v WHERE v.exporterFax = :exporterFax"),
    @NamedQuery(name = "VFileImportOrder.findByExporterEmail", query = "SELECT v FROM VFileImportOrder v WHERE v.exporterEmail = :exporterEmail"),
    @NamedQuery(name = "VFileImportOrder.findByComingDate", query = "SELECT v FROM VFileImportOrder v WHERE v.comingDate = :comingDate"),
    @NamedQuery(name = "VFileImportOrder.findByExporterGateCode", query = "SELECT v FROM VFileImportOrder v WHERE v.exporterGateCode = :exporterGateCode"),
    @NamedQuery(name = "VFileImportOrder.findByExporterGateName", query = "SELECT v FROM VFileImportOrder v WHERE v.exporterGateName = :exporterGateName"),
    @NamedQuery(name = "VFileImportOrder.findByImporterGateCode", query = "SELECT v FROM VFileImportOrder v WHERE v.importerGateCode = :importerGateCode"),
    @NamedQuery(name = "VFileImportOrder.findByImporterGateName", query = "SELECT v FROM VFileImportOrder v WHERE v.importerGateName = :importerGateName"),
    @NamedQuery(name = "VFileImportOrder.findByCustomsBranchCode", query = "SELECT v FROM VFileImportOrder v WHERE v.customsBranchCode = :customsBranchCode"),
    @NamedQuery(name = "VFileImportOrder.findByCustomsBranchName", query = "SELECT v FROM VFileImportOrder v WHERE v.customsBranchName = :customsBranchName"),
    @NamedQuery(name = "VFileImportOrder.findByCheckTime", query = "SELECT v FROM VFileImportOrder v WHERE v.checkTime = :checkTime"),
    @NamedQuery(name = "VFileImportOrder.findByCheckPlace", query = "SELECT v FROM VFileImportOrder v WHERE v.checkPlace = :checkPlace"),
    @NamedQuery(name = "VFileImportOrder.findByDeptCode", query = "SELECT v FROM VFileImportOrder v WHERE v.deptCode = :deptCode"),
    @NamedQuery(name = "VFileImportOrder.findByDeptName", query = "SELECT v FROM VFileImportOrder v WHERE v.deptName = :deptName"),
    @NamedQuery(name = "VFileImportOrder.findBySignPlace", query = "SELECT v FROM VFileImportOrder v WHERE v.signPlace = :signPlace"),
    @NamedQuery(name = "VFileImportOrder.findBySignDate", query = "SELECT v FROM VFileImportOrder v WHERE v.signDate = :signDate"),
    @NamedQuery(name = "VFileImportOrder.findBySignName", query = "SELECT v FROM VFileImportOrder v WHERE v.signName = :signName"),
    @NamedQuery(name = "VFileImportOrder.findBySignedData", query = "SELECT v FROM VFileImportOrder v WHERE v.signedData = :signedData"),
    @NamedQuery(name = "VFileImportOrder.findByFileType", query = "SELECT v FROM VFileImportOrder v WHERE v.fileType = :fileType"),
    @NamedQuery(name = "VFileImportOrder.findByFileTypeName", query = "SELECT v FROM VFileImportOrder v WHERE v.fileTypeName = :fileTypeName"),
    @NamedQuery(name = "VFileImportOrder.findByFileCode", query = "SELECT v FROM VFileImportOrder v WHERE v.fileCode = :fileCode"),
    @NamedQuery(name = "VFileImportOrder.findByFileName", query = "SELECT v FROM VFileImportOrder v WHERE v.fileName = :fileName"),
    @NamedQuery(name = "VFileImportOrder.findByStatus", query = "SELECT v FROM VFileImportOrder v WHERE v.status = :status"),
    @NamedQuery(name = "VFileImportOrder.findByTaxCode", query = "SELECT v FROM VFileImportOrder v WHERE v.taxCode = :taxCode"),
    @NamedQuery(name = "VFileImportOrder.findByBusinessId", query = "SELECT v FROM VFileImportOrder v WHERE v.businessId = :businessId"),
    @NamedQuery(name = "VFileImportOrder.findByBusinessName", query = "SELECT v FROM VFileImportOrder v WHERE v.businessName = :businessName"),
    @NamedQuery(name = "VFileImportOrder.findByBusinessAddress", query = "SELECT v FROM VFileImportOrder v WHERE v.businessAddress = :businessAddress"),
    @NamedQuery(name = "VFileImportOrder.findByBusinessPhone", query = "SELECT v FROM VFileImportOrder v WHERE v.businessPhone = :businessPhone"),
    @NamedQuery(name = "VFileImportOrder.findByBusinessFax", query = "SELECT v FROM VFileImportOrder v WHERE v.businessFax = :businessFax"),
    @NamedQuery(name = "VFileImportOrder.findByCreateDate", query = "SELECT v FROM VFileImportOrder v WHERE v.createDate = :createDate"),
    @NamedQuery(name = "VFileImportOrder.findByModifyDate", query = "SELECT v FROM VFileImportOrder v WHERE v.modifyDate = :modifyDate"),
    @NamedQuery(name = "VFileImportOrder.findByCreatorId", query = "SELECT v FROM VFileImportOrder v WHERE v.creatorId = :creatorId"),
    @NamedQuery(name = "VFileImportOrder.findByCreatorName", query = "SELECT v FROM VFileImportOrder v WHERE v.creatorName = :creatorName"),
    @NamedQuery(name = "VFileImportOrder.findByCreateDeptId", query = "SELECT v FROM VFileImportOrder v WHERE v.createDeptId = :createDeptId"),
    @NamedQuery(name = "VFileImportOrder.findByCreateDeptName", query = "SELECT v FROM VFileImportOrder v WHERE v.createDeptName = :createDeptName"),
    @NamedQuery(name = "VFileImportOrder.findByIsActive", query = "SELECT v FROM VFileImportOrder v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VFileImportOrder.findByVersion", query = "SELECT v FROM VFileImportOrder v WHERE v.version = :version"),
    @NamedQuery(name = "VFileImportOrder.findByParentFileId", query = "SELECT v FROM VFileImportOrder v WHERE v.parentFileId = :parentFileId"),
    @NamedQuery(name = "VFileImportOrder.findByIsTemp", query = "SELECT v FROM VFileImportOrder v WHERE v.isTemp = :isTemp"),
    @NamedQuery(name = "VFileImportOrder.findByStartDate", query = "SELECT v FROM VFileImportOrder v WHERE v.startDate = :startDate"),
    @NamedQuery(name = "VFileImportOrder.findByNumDayProcess", query = "SELECT v FROM VFileImportOrder v WHERE v.numDayProcess = :numDayProcess"),
    @NamedQuery(name = "VFileImportOrder.findByDeadline", query = "SELECT v FROM VFileImportOrder v WHERE v.deadline = :deadline")})
public class VFileImportOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMPORT_ORDER_FILE_ID")
    private Long importOrderFileId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 31)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Size(max = 35)
    @Column(name = "TRANS_NO")
    private String transNo;
    @Size(max = 35)
    @Column(name = "CONTRACT_NO")
    private String contractNo;
    @Size(max = 255)
    @Column(name = "BILL_NO")
    private String billNo;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_NAME")
    private String goodsOwnerName;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_ADDRESS")
    private String goodsOwnerAddress;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_PHONE")
    private String goodsOwnerPhone;
    @Size(max = 20)
    @Column(name = "GOODS_OWNER_FAX")
    private String goodsOwnerFax;
    @Size(max = 31)
    @Column(name = "GOODS_OWNER_EMAIL")
    private String goodsOwnerEmail;
    @Size(max = 255)
    @Column(name = "RESPONSIBLE_PERSON_NAME")
    private String responsiblePersonName;
    @Size(max = 255)
    @Column(name = "RESPONSIBLE_PERSON_ADDRESS")
    private String responsiblePersonAddress;
    @Size(max = 50)
    @Column(name = "RESPONSIBLE_PERSON_PHONE")
    private String responsiblePersonPhone;
    @Size(max = 20)
    @Column(name = "RESPONSIBLE_PERSON_FAX")
    private String responsiblePersonFax;
    @Size(max = 210)
    @Column(name = "RESPONSIBLE_PERSON_EMAIL")
    private String responsiblePersonEmail;
    @Size(max = 255)
    @Column(name = "EXPORTER_NAME")
    private String exporterName;
    @Size(max = 255)
    @Column(name = "EXPORTER_ADDRESS")
    private String exporterAddress;
    @Size(max = 255)
    @Column(name = "EXPORTER_PHONE")
    private String exporterPhone;
    @Size(max = 255)
    @Column(name = "EXPORTER_FAX")
    private String exporterFax;
    @Size(max = 255)
    @Column(name = "EXPORTER_EMAIL")
    private String exporterEmail;
    @Column(name = "COMING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comingDate;
    @Size(max = 6)
    @Column(name = "EXPORTER_GATE_CODE")
    private String exporterGateCode;
    @Size(max = 255)
    @Column(name = "EXPORTER_GATE_NAME")
    private String exporterGateName;
    @Size(max = 6)
    @Column(name = "IMPORTER_GATE_CODE")
    private String importerGateCode;
    @Size(max = 255)
    @Column(name = "IMPORTER_GATE_NAME")
    private String importerGateName;
    @Size(max = 20)
    @Column(name = "CUSTOMS_BRANCH_CODE")
    private String customsBranchCode;
    @Size(max = 255)
    @Column(name = "CUSTOMS_BRANCH_NAME")
    private String customsBranchName;
    @Column(name = "CHECK_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkTime;
    @Size(max = 255)
    @Column(name = "CHECK_PLACE")
    private String checkPlace;
    @Size(max = 12)
    @Column(name = "DEPT_CODE")
    private String deptCode;
    @Size(max = 255)
    @Column(name = "DEPT_NAME")
    private String deptName;
    @Size(max = 255)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    @Size(max = 255)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Size(max = 2000)
    @Column(name = "SIGNED_DATA")
    private String signedData;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Size(max = 255)
    @Column(name = "FILE_TYPE_NAME")
    private String fileTypeName;
    @Size(max = 31)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 255)
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "STATUS")
    private Long status;
    @Size(max = 31)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Column(name = "BUSINESS_ID")
    private Long businessId;
    @Size(max = 255)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 500)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 31)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 31)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 255)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 255)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "VERSION")
    private Long version;
    @Column(name = "PARENT_FILE_ID")
    private Long parentFileId;
    @Column(name = "IS_TEMP")
    private Long isTemp;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "NUM_DAY_PROCESS")
    private Long numDayProcess;
    @Column(name = "DEADLINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;
    @Column(name = "FINISH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;
    @Column(name = "HAVE_QR_CODE")
    private Long haveQrCode;

    public VFileImportOrder() {
    }

    public VFileImportOrder(VFileImportOrder obj, Long statusProcess) {
        this.status = statusProcess;
        this.fileId = obj.getFileId();
        this.fileType = obj.getFileType();
        this.fileCode = obj.getFileCode();
        this.fileTypeName = obj.getFileTypeName();
        this.importOrderFileId = obj.getImportOrderFileId();
        this.billNo = obj.getBillNo();
        this.businessAddress = obj.getBusinessAddress();
        this.businessFax = obj.getBusinessFax();
        this.businessId = obj.getBusinessId();
        this.businessName = obj.getBusinessName();
        this.businessPhone = obj.getBusinessPhone();
        this.checkPlace = obj.getCheckPlace();
        this.checkTime = obj.getCheckTime();
        this.comingDate = obj.getComingDate();
        this.contractNo = obj.getContractNo();
        this.cosFileId = obj.getCosFileId();
        this.createDate = obj.getCreateDate();
        this.createDeptId = obj.getCreateDeptId();
        this.createDeptName = obj.getCreateDeptName();
        this.creatorId = obj.getCreatorId();
        this.creatorName = obj.getCreatorName();
        this.customsBranchCode = obj.getCustomsBranchCode();
        this.customsBranchName = obj.getCustomsBranchName();
        this.deadline = obj.getDeadline();
        this.deptCode = obj.getDeptCode();
        this.deptName = obj.getDeptName();
        this.documentTypeCode = obj.getDocumentTypeCode();
        this.exporterAddress = obj.getExporterAddress();
        this.exporterEmail = obj.getExporterEmail();
        this.exporterFax = obj.getExporterFax();
        this.exporterGateCode = obj.getExporterGateCode();
        this.exporterGateName = obj.getExporterGateName();
        this.exporterName = obj.getExporterName();
        this.nswFileCode = obj.getNswFileCode();
        this.businessName = obj.getBusinessName();
        this.fileCode = obj.getFileCode();
        this.transNo = obj.getTransNo();
        this.contractNo = obj.getContractNo();
        this.billNo = obj.getBillNo();
        this.createDate = obj.getCreateDate();
        this.modifyDate = obj.getModifyDate();

    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public Long getImportOrderFileId() {
        return importOrderFileId;
    }

    public void setImportOrderFileId(Long importOrderFileId) {
        this.importOrderFileId = importOrderFileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return goodsOwnerEmail;
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = goodsOwnerEmail;
    }

    public String getResponsiblePersonName() {
        return responsiblePersonName;
    }

    public void setResponsiblePersonName(String responsiblePersonName) {
        this.responsiblePersonName = responsiblePersonName;
    }

    public String getResponsiblePersonAddress() {
        return responsiblePersonAddress;
    }

    public void setResponsiblePersonAddress(String responsiblePersonAddress) {
        this.responsiblePersonAddress = responsiblePersonAddress;
    }

    public String getResponsiblePersonPhone() {
        return responsiblePersonPhone;
    }

    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
        this.responsiblePersonPhone = responsiblePersonPhone;
    }

    public String getResponsiblePersonFax() {
        return responsiblePersonFax;
    }

    public void setResponsiblePersonFax(String responsiblePersonFax) {
        this.responsiblePersonFax = responsiblePersonFax;
    }

    public String getResponsiblePersonEmail() {
        return responsiblePersonEmail;
    }

    public void setResponsiblePersonEmail(String responsiblePersonEmail) {
        this.responsiblePersonEmail = responsiblePersonEmail;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterFax() {
        return exporterFax;
    }

    public void setExporterFax(String exporterFax) {
        this.exporterFax = exporterFax;
    }

    public String getExporterEmail() {
        return exporterEmail;
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = exporterEmail;
    }

    public Date getComingDate() {
        return comingDate;
    }

    public void setComingDate(Date comingDate) {
        this.comingDate = comingDate;
    }

    public String getExporterGateCode() {
        return exporterGateCode;
    }

    public void setExporterGateCode(String exporterGateCode) {
        this.exporterGateCode = exporterGateCode;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateCode() {
        return importerGateCode;
    }

    public void setImporterGateCode(String importerGateCode) {
        this.importerGateCode = importerGateCode;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public String getCustomsBranchCode() {
        return customsBranchCode;
    }

    public void setCustomsBranchCode(String customsBranchCode) {
        this.customsBranchCode = customsBranchCode;
    }

    public String getCustomsBranchName() {
        return customsBranchName;
    }

    public void setCustomsBranchName(String customsBranchName) {
        this.customsBranchName = customsBranchName;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignedData() {
        return signedData;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getParentFileId() {
        return parentFileId;
    }

    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public Long getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(Long isTemp) {
        this.isTemp = isTemp;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Long getNumDayProcess() {
        return numDayProcess;
    }

    public void setNumDayProcess(Long numDayProcess) {
        this.numDayProcess = numDayProcess;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Long getDeadlineWarning() {
        Date currentDate = new Date();
        Long value = Constants.WARNING.DEADLINE_ON;
        if (deadline != null) {
            if (!deadline.after(currentDate)) {
                if (Constants_CKS.FILE_STATUS_CODE.FINISH != status
                        && Constants_CKS.FILE_STATUS_CODE.STATUS_DATHONGBAOSDBS != status) {
                    value = Constants.WARNING.DEADLINE_MISS;
                }

            }

        }

        return value;
    }

    /**
     * @return the finishDate
     */
    public Date getFinishDate() {
        return finishDate;
    }

    /**
     * @param finishDate the finishDate to set
     */
    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Long getHaveQrCode() {
        return haveQrCode;
    }

    public void setHaveQrCode(Long haveQrCode) {
        this.haveQrCode = haveQrCode;
    }
}
