/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.BO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tichnv
 */
@Entity
@Table(name = "V_PRODUCT_TARGET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VProductTarget.findAll", query = "SELECT v FROM VProductTarget v"),
    @NamedQuery(name = "VProductTarget.findById", query = "SELECT v FROM VProductTarget v WHERE v.id = :id"),
    @NamedQuery(name = "VProductTarget.findByProductId", query = "SELECT v FROM VProductTarget v WHERE v.productId = :productId"),
    @NamedQuery(name = "VProductTarget.findByTestValue", query = "SELECT v FROM VProductTarget v WHERE v.testValue = :testValue"),
    @NamedQuery(name = "VProductTarget.findByMaxValue", query = "SELECT v FROM VProductTarget v WHERE v.maxValue = :maxValue"),
    @NamedQuery(name = "VProductTarget.findByComments", query = "SELECT v FROM VProductTarget v WHERE v.comments = :comments"),
    @NamedQuery(name = "VProductTarget.findByIsActive", query = "SELECT v FROM VProductTarget v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VProductTarget.findByCreatedDate", query = "SELECT v FROM VProductTarget v WHERE v.createdDate = :createdDate"),
    @NamedQuery(name = "VProductTarget.findByCreatedBy", query = "SELECT v FROM VProductTarget v WHERE v.createdBy = :createdBy"),
    @NamedQuery(name = "VProductTarget.findByStatus", query = "SELECT v FROM VProductTarget v WHERE v.status = :status"),
    @NamedQuery(name = "VProductTarget.findByName", query = "SELECT v FROM VProductTarget v WHERE v.name = :name"),
    @NamedQuery(name = "VProductTarget.findByType", query = "SELECT v FROM VProductTarget v WHERE v.type = :type"),
    @NamedQuery(name = "VProductTarget.findByTestMethod", query = "SELECT v FROM VProductTarget v WHERE v.testMethod = :testMethod"),
    @NamedQuery(name = "VProductTarget.findByTypeName", query = "SELECT v FROM VProductTarget v WHERE v.typeName = :typeName")})
public class VProductTarget implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Column(name = "PRODUCT_TARGET_ID")
    private Long productTargetId;
    @Column(name = "TEST_VALUE")
    private String testValue;
    @Size(max = 30)
    @Column(name = "MAX_VALUE")
    private String maxValue;
    @Size(max = 250)
    @Column(name = "COMMENTS")
    private String comments;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "CREATED_BY")
    private Long createdBy;
    @Column(name = "STATUS")
    private Long status;
    @Size(max = 100)
    @Column(name = "NAME")
    private String name;
    @Column(name = "COST")
    private Long cost;
    @Size(max = 50)
    @Column(name = "TYPE")
    private String type;
    @Size(max = 1000)
    @Column(name = "TEST_METHOD")
    private String testMethod;
    @Size(max = 200)
    @Column(name = "TYPE_NAME")
    private String typeName;
    @Column(name = "PASS")
    private Long pass;

    public VProductTarget() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getTestValue() {
        return testValue;
    }

    public void setTestValue(String testValue) {
        this.testValue = testValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTestMethod() {
        return testMethod;
    }

    public void setTestMethod(String testMethod) {
        this.testMethod = testMethod;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getProductTargetId() {
        return productTargetId;
    }

    public void setProductTargetId(Long productTargetId) {
        this.productTargetId = productTargetId;
    }

    public Long getPass() {
        return pass;
    }

    public void setPass(Long pass) {
        this.pass = pass;
    }

}
