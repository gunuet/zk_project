package com.viettel.module.importOrder.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.HolidaysManagementDAOHE;
import com.viettel.module.importOrder.DAO.RtAssayDAO;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.TimeHoliday;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class CreateUpdateRtAssayController extends BaseComposer {
//
//    @Wire
//    Textbox tbId, tbName, tbDescription;
    @Wire
    Datebox dbTimeDate;
    @Wire
    Listbox lbDayType;
    @Wire
    Window createUpdate;
    @Wire
    Textbox tbrtAssayName,tbcode,tbrtAddress, tbPhone,tbFax,tbId;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        loadInfoToForm();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Listen("onClick=#btnSave")
    public void onSave() throws IOException {
                        RtAssay timeHoliday = new RtAssay();
                        if (tbId.getValue() != null && !tbId.getValue().isEmpty()) {
                            timeHoliday.setId(Long.parseLong(tbId.getValue()));
                        }

                        if (tbrtAssayName.getValue() != null&& tbrtAssayName.getValue().trim().length() == 0) {
                			showNotification("Phải nhập tên đơn vị kiểm tra ",Constants.Notification.ERROR);
                			tbrtAssayName.focus();
                			return ;
                		}else {
                            timeHoliday.setName(tbrtAssayName.getValue());
                        }

                        if (tbcode.getValue() != null	&& tbcode.getValue().trim().length() == 0) {
                			showNotification("Phải nhập mã đơn vị", Constants.Notification.ERROR);
                			tbcode.focus();
                			return ;
                        } else {
                            timeHoliday.setCode(tbcode.getValue());
                        }
                        if (tbrtAddress.getValue() != null
                				&& tbrtAddress.getValue().trim().length() == 0) {
                			showNotification("Phải nhập địa chỉ đơn vị", Constants.Notification.ERROR);
                			tbrtAddress.focus();
                			return ;
                        } else {
                            timeHoliday.setAddress(tbrtAddress.getValue());
                        } 
                        if (tbPhone.getValue() != null
                				&& tbPhone.getValue().trim().length() == 0) {
                			showNotification("Phải nhập số điện thoại", Constants.Notification.ERROR);
                			tbPhone.focus();
                			return ;
                        } else {
                            timeHoliday.setPhone(tbFax.getValue());
                        }
                        if (tbFax.getValue() != null
                				&& tbFax.getValue().trim().length() == 0) {
                			showNotification("Phải nhập mã fax", Constants.Notification.ERROR);
                			tbFax.focus();
                			return ;
                        } else {
                            timeHoliday.setFax(tbFax.getValue());
                            
                        }

                    
                        timeHoliday.setIsActive(1L);
                        timeHoliday.setCreatedBy(getUserId());
                        //timeHoliday.setCreatedDate(new Date());
                        RtAssayDAO rtAssayDAO = new RtAssayDAO();
                        rtAssayDAO.saveOrUpdate(timeHoliday);
                        if (tbId.getValue() != null && !tbId.getValue().isEmpty()) {
                            //update thì detach window
                        	createUpdate.detach();
                        } else {
                            // them moi thi clear window
                            loadInfoToForm();
                        }
                        showNotification("Lưu thành công", Constants.Notification.INFO);

                        Window parentWnd = (Window) Path.getComponent("/managertAssayWindow");
                        Events.sendEvent(new Event("onReload", parentWnd, null));
                    }
                
    

    public void loadInfoToForm() {
        Long id = (Long) Executions.getCurrent().getArg().get("id");
        if (id != null) {
            RtAssayDAO objhe = new RtAssayDAO();
            RtAssay rs = objhe.findById(id);
            if (rs.getId()!=null) {
				tbId.setValue(rs.getId().toString());
			}
            if (rs.getName() != null) {
                tbrtAssayName.setValue(rs.getName().toString());
            }
            if (rs.getCode() != null) {
                tbcode.setValue(rs.getCode().toString());
            }
            if (rs.getAddress() != null) {
                tbrtAddress.setValue(rs.getAddress().toString());
            }

            if (rs.getPhone() != null) {
                tbPhone.setValue(rs.getPhone().toString());
            }
            if (rs.getFax() != null) {
                tbFax.setValue(rs.getFax().toString());
            }
        } else {
        	tbrtAssayName.setValue("");
            tbcode.setValue("");
            tbrtAddress.setValue("");
            tbPhone.setValue("");
            tbFax.setValue("");
        }
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }
}
