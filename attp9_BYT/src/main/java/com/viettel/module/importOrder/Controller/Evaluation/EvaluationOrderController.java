package com.viettel.module.importOrder.Controller.Evaluation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Listitem;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VCountCheck;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VCountStatusFileDAO;
import com.viettel.module.importOrder.DAO.VProductTargetDAO;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;

import org.zkoss.zul.Combobox;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author Linhdx
 */
public class EvaluationOrderController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, labelCriteria, labelMechanism,
            labelLegalRequi, labelCriteriaRequi, labelMechanismRequi,
            labelProductNameRequi, labelProductName, labelChiTieu, lbtexLegal,
            producIdHidden, lbFileId, lbProductWarning, lbHisstory,
            lbNumberUnprocess, lbTextNumberUnprocess, lbtexhisstory;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    @Wire
    private Window windowEvaluation;
    private Long fileId;
    private Long userEvaluationType;
    private VFileImportOrder cosFile;
    @Wire
    Listbox lbOrderProduct;
    @Wire
    Listbox lbProductTarget;
    @Wire
    Listbox lbxDepartment;
    Long documentTypeCode;
    private Files files;
    private String nswFileCode;
    private List<Category> listImportOderFileType;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;
    private Users user;
    @Wire
    private Textbox userEvaluationTypeH, txtValidate;
    @Wire
    private Radiogroup rbStatus;
    @Wire
    private Textbox mainContent, legalContent, mechanismContent,
            ProductNameContent, criteriaContent, lbStatus;
    @Wire
    private Listbox lbEffective, lbLegal, lbMechanism, lbCriteria,
            lbProductName, labelLegal, lboxLegal, lboxlLegalCT,
            lbProductHisstory;
    @Wire
    private Button btnApproveFile, btnApproveDispatch, btnPreviewDispatch,
            btnAddNew, btnSave;
    @Wire
    private Button btnApproveFile2, btnApproveDispatch2, btnPreviewDispatch2;
    private VFileRtfile vFileRtfile;
    private String sLegal, sMechanism, sCriteria, sNameProduct;
    private CosEvaluationRecord obj = new CosEvaluationRecord();
    @Wire
    private Window businessWindow;
    @Wire
    private Combobox cbtexLegal;
    @Wire("#incList #lbList")
    private Listbox lbList;
    private int flag_click = 0;
    private Long evalType = 10L;

       // @Wire
    // private Div gbLegal,divParent,divChild;
    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        // Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
        files = (new FilesDAOHE()).findById(fileId);

        // load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        nswFileCode = getAutoNswFileCode(documentTypeCode);
        flag_click = 0;
        listImportOderFileType = new CategoryDAOHE()
                .findAllCategory(Constants.CATEGORY_TYPE.METHOD_IMDOC);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("id", fileId);
        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO()
                .findAllIdByFileId(fileId);
        ListModelArray lstModelManufacturer = new ListModelArray(
                importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);
        lbFileId.setValue(String.valueOf(fileId));
        if (flag_click == 0 && importOrderProducts.size() > 0) {
            ImportOrderProduct first_obj = importOrderProducts.get(0);
            setViewFirstClick(first_obj.getProductId());
        }
        flag_click = 1;

        // load co quan kiem tra
        loadCheckDepartment();
    }

    // load co quan kiem tra
    private void loadCheckDepartment() {
        int selectedListitem = -1;

        List<Department> departmentsList = new DepartmentDAOHE()
                .findByDeptType(Constants.DEPARTMENT.DEPT_TYPE_CHECK);
        ListModel<Department> listModel = new ListModelArray<Department>(
                departmentsList);
        lbxDepartment.setModel(listModel);
        lbxDepartment.renderAll();

        // load content
        EvaluationRecordDAO evaluationRecordDAO = new EvaluationRecordDAO();
        // get lastest evaluationRecord
        EvaluationRecord evaluationRecord = evaluationRecordDAO
                .findMaxByFileIdAndEvalType(fileId, evalType);
        if (evaluationRecord != null) {
            String formContent = evaluationRecord.getFormContent();
            if (formContent != null) {
                EvaluationModel evaluationModel = new Gson().fromJson(
                        formContent, EvaluationModel.class);
//                if (listModel != null) {
                    int size = listModel.getSize();
                    for (int i = 0; i < size; i++) {
                        if (evaluationModel.getCheckDeptId().equals(
                                listModel.getElementAt(i).getDeptId())) {
                            selectedListitem = i;
                            break;
                        }
                    }
//                }
            }
        }

        lbxDepartment.setSelectedIndex(selectedListitem);

    }

    // load thông tin sản phẩm khilần đầu gọi from
    public void setViewFirstClick(Long pr_id) {
        // get san pham
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO
                .findById(pr_id);

        VCountStatusFileDAO vdao = new VCountStatusFileDAO();// lbProductHisstory
        List<VCountCheck> lst = vdao.getHisstryProduct(files.getCreatorId(),
                importOrderProduct.getConfirmAnnounceNo());
        if (lst != null) {
            ListModelArray lstModel_Hisstoryt = new ListModelArray(lst);
            lbProductHisstory.setModel(lstModel_Hisstoryt);
        }

        producIdHidden.setValue(String.valueOf(pr_id));

    }

    @Listen("onOpenUpdate = #lbProductTarget")
    public void onOpenUpdate(Event event) {
        VProductTarget obj = (VProductTarget) event.getData();
        Long targetId = obj.getId();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("producId", producIdHidden.getValue());
        arguments.put("targetId", targetId);
        arguments.put("method", "update");
        // arguments.put("parentWindow", businessWindow);
        setParam(arguments);
        createWindow(
                "windowCRUDCosmetic",
                "/Pages/module/importorder/evaluationOrder/importOrderAddNewTargets.zul",
                arguments, Window.HIGHLIGHTED);
    }

    @Listen("onDelete = #lbProductTarget")
    public void onDelete(Event event) {
        final VProductTarget obj = (VProductTarget) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {

                                        VProductTargetDAO objDAOHE = new VProductTargetDAO();
                                        objDAOHE.delete(obj.getId());
                                        reload();

                                    } catch (Exception ex) {
                                        showNotification(
                                                String.format(
                                                        Constants.Notification.DELETE_ERROR,
                                                        Constants.DOCUMENT_TYPE_NAME.FILE),
                                                Constants.Notification.ERROR);
                                        LogUtils.addLogDB(ex);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    public void reload() {
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        List<VProductTarget> productProductTarget = new VProductTargetDAO()
                .findByProdutId(productId);
        ListModelArray lstModel_ProducTarget = new ListModelArray(
                productProductTarget);
        lbProductTarget.setModel(lstModel_ProducTarget);
    }

    // save co quan kiem tra
    private void saveCheckDepartment() {
        Listitem listItem = lbxDepartment.getSelectedItem();
        if (listItem == null) {
            return;
        }

        Long checkDeptId = listItem.getValue();
        String checkDeptName = listItem.getLabel();

        // load content
        EvaluationRecordDAO evaluationRecordDAO = new EvaluationRecordDAO();
        EvaluationModel evaluationModel = new EvaluationModel();
        // get lastest evaluationRecord
        EvaluationRecord evaluationRecord = evaluationRecordDAO
                .findMaxByFileIdAndEvalType(fileId, evalType);
        if (evaluationRecord != null) {
            String formContent = evaluationRecord.getFormContent();
            if (formContent != null) {
                evaluationModel = new Gson().fromJson(formContent,
                        EvaluationModel.class);
            }
            // if not fond evaluationRecord then create new
        } else {
            evaluationRecord = new EvaluationRecord();
            evaluationRecord.setFileId(fileId);
            evaluationRecord.setEvalType(evalType);
        }

        evaluationModel.setUserId(getUserId());
        evaluationModel.setUserName(getUserName());
        evaluationModel.setDeptId(getDeptId());
        evaluationModel.setDeptName(getDeptName());
        evaluationModel.setCheckDeptId(checkDeptId);
        evaluationModel.setCheckDeptName(checkDeptName);
        String formContent = new Gson().toJson(evaluationModel);

        evaluationRecord.setFormContent(formContent);
        evaluationRecordDAO.saveOrUpdate(evaluationRecord);

    }

    @Listen("onShow =  #lbOrderProduct")
    public void onShow(Event event) {
        ImportOrderProduct obj = (ImportOrderProduct) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        Long productId = obj.getProductId();
        arguments.put("id", productId);
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);

        // get sanr pham
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO
                .findById(productId);

        VCountStatusFileDAO vdao = new VCountStatusFileDAO();// lbProductHisstory
        List<VCountCheck> lst = vdao.getHisstryProduct(files.getCreatorId(),
                importOrderProduct.getConfirmAnnounceNo());
        if (lst != null) {
            ListModelArray lstModel_Hisstoryt = new ListModelArray(lst);
            lbProductHisstory.setModel(lstModel_Hisstoryt);
        }
        producIdHidden.setValue(String.valueOf(productId));
    }

    @Listen("onClick =  #btnAddNew")
    public void onShowNewAdd() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("producId", producIdHidden.getValue());
        arguments.put("targetId", new Long("0"));
        //arguments.put("method", null);
        // arguments.put("parentWindow", businessWindow);
        setParam(arguments);
        createWindow(
                "windowCRUDCosmetic",
                "/Pages/module/importorder/evaluationOrder/importOrderAddNewTargets.zul",
                arguments, Window.HIGHLIGHTED);
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", businessWindow);
        return arguments;

    }

    @Listen("onChildWindowClosed=#businessWindow")
    public void onChildWindowClosed() {
    }

    public void fillListData(Long productId) {
//        List<VProductTarget> productProductTarget = new VProductTargetDAO()
//                .findByProdutId(productId);
//        if (productProductTarget != null) {
//            ListModelArray lstModel_ProducTarget = new ListModelArray(
//                    productProductTarget);
//            // lbProductTarget.setModel(lstModel_ProducTarget);
//        }

    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        clearWarningMessage();
        try {
            onApproveFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public void onApproveFile() throws Exception {
        if (!isValidate()) {
            return;
        }

        // save co quan kiem tra
        saveCheckDepartment();

        txtValidate.setValue("1");
        
    }

    private boolean checkMethod(String code) {
        if (StringUtils.validString(code)) {
            if ("GIAM".equals(code) || "CHAT".equals(code) || "THUONG".equals(code)) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidate() {
        String message;
        // load lai danh sachs san pham
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO()
                .findAllIdByFileId(fileId);
        ImportOrderProduct obj;
        for (ImportOrderProduct importOrder : importOrderProducts) {
            obj = importOrder;
            if (!checkMethod(obj.getCheckMethodCode())) {
                message = "Chưa chọn phương thức kiểm tra cho sản phẩm.Vui lòng kiểm tra lại và Click nút lưu kiểm tra";
                showWarningMessage(message);
                return false;
            }
        }

        // bat buoc check co quan kiem tra khi phuong thuc kiem tra CHAT +
        // THUONG
        for (ImportOrderProduct importOrder : importOrderProducts) {
            obj = importOrder;
            if (obj.getCheckMethodCode() == null
                    || "CHAT".equals(obj.getCheckMethodCode())
                    || "THUONG".equals(obj.getCheckMethodCode())) {

                if (lbxDepartment.getSelectedItem() == null) {
                    showWarningMessage("Chưa chọn cơ quan kiểm tra");
                    return false;
                }
            }
        }

        return true;
    }

    protected void showWarningMessage(String message) {
        lbBottomWarning.setValue(message);
    }

    private void clearWarningMessage() {
        lbBottomWarning.setValue("");
    }

    private String getAutoNswFileCode(Long documentTypeCode) {

        String autoNumber;

        ImportOrderFileDAO dao = new ImportOrderFileDAO();
        Long autoNumberL = dao.countImportfile();
        autoNumberL += 1L;// Tránh số 0 (linhdx)
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

    public void onChangeCQKT() {
        Listitem listItem = lbxDepartment.getSelectedItem();
        if (listItem == null) {
            return;
        }

        Long checkDeptId = listItem.getValue();

        VCountStatusFileDAO dao = new VCountStatusFileDAO();
        Long number = dao.countNumberUnprocess(checkDeptId.toString());
        lbTextNumberUnprocess.setVisible(true);
        lbNumberUnprocess.setVisible(true);
        lbNumberUnprocess.setValue("" + number + " hồ sơ");
    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    private void setWarningMessage(String message) {

        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);

    }

    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        return Constants.CHECK_VIEW.VIEW;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public ListModel<Category> getCheckMethodList() {
        List<Category> list = new ArrayList<Category>();
        if (listImportOderFileType != null) {
            for (Iterator<Category> iterator = listImportOderFileType
                    .iterator(); iterator.hasNext();) {
                Category category = (Category) iterator.next();
                list.add(category);
            }
        }

        return new ListModelArray<Category>(listImportOderFileType);
    }

    @Listen("onSelect = #lbOrderProduct listbox")
    public void onCheckMethod(Event event) {
        Listbox listbox = (Listbox) event.getTarget();
        Listitem listitem = listbox.getSelectedItem();

        if (listitem != null && !"-1".equals(listitem.getValue().toString())) {
            Long productId = (Long) listitem.getAttribute("productId");
            ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
            ImportOrderProduct importOrderProduct = importOrderProductDAO
                    .findById(productId);
            importOrderProduct.setCheckMethodName(listitem.getLabel());
            importOrderProduct.setCheckMethodCode(String.valueOf(listitem
                    .getValue()));

            importOrderProductDAO.saveOrUpdate(importOrderProduct);
        }
    }

}
