package com.viettel.module.importOrder.Controller.Evaluation;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.BO.ProductTarget;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.importOrder.BO.ProductProductTarget;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.module.importOrder.DAO.ProductProductTargetDAO;
import com.viettel.module.importOrder.DAO.ProductTargetDAO;
import com.viettel.module.importOrder.DAO.VProductTargetDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;

public class ImportOrderAddNewTargetController extends BusinessController {

    private static final long serialVersionUID = 1L;

    @Wire
    Listbox targetCategory, target;
    @Wire
    Label testMethod, targetCost;

    @Wire
    Button btnSave;
    @Wire
    Label productIdHidden, productTargetIdHidden, lbtargetWarning;
    @Wire
    Textbox comment, testValue, maxValue;
    @Wire
    private Window addNewTargetWindow, parentWindow;
    private String method;
    Long targetId;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        productIdHidden.setValue(String.valueOf(arguments.get("producId")));
        method = (String) arguments.get("method") == null ? "" : (String) arguments.get("method");
        targetId = (Long) arguments.get("targetId") == null ? 0 : (Long) arguments.get("targetId");

        CategoryDAOHE categoryDAOHE = new CategoryDAOHE();
        List<Category> categoryList = categoryDAOHE.findAllCategory(
                Constants.CATEGORY_TYPE.TESTTYPE_IMDOC);
        ListModelList lstModel = new ListModelList(categoryList);
        targetCategory.setModel(lstModel);
        if ("update".equals(method)) {
            VProductTargetDAO dao = new VProductTargetDAO();
            VProductTarget vp_target = dao.findById(targetId);
            Long productTargetId = vp_target.getProductTargetId();

            ProductTarget productTarget = new ProductTargetDAO().findId(productTargetId);

            if (productTarget != null) {
                String type = productTarget.getType();
                targetCategory.renderAll();
                targetCategory.setSelectedIndex(0);

                for (int i = 0; i < targetCategory.getListModel().getSize(); i++) {
                    Category cat = (Category) targetCategory.getListModel().getElementAt(i);
                    if (type.equals(cat.getCode())) {
                        targetCategory.setSelectedIndex(i);
                        break;
                    }
                }
                testMethod.setValue(productTarget.getTestMethod());
                targetCost.setValue(String.valueOf(productTarget.getCost()));
                testValue.setValue(vp_target.getTestValue());
                maxValue.setValue(vp_target.getMaxValue());
                comment.setValue(vp_target.getComments());

            }
            //set listbox chỉ tiêu
            String code = targetCategory.getSelectedItem().getValue();
            List<ProductTarget> typeList = new ProductTargetDAO()
                    .findAllTargetByCategory(code);
            ListModelArray<ProductTarget> listModelArray = new ListModelArray<ProductTarget>(typeList);
            // ProductTarget productTarget = new ProductTargetDAO().findId(productTargetId);
            target.setModel(listModelArray);
            target.renderAll();
            target.setSelectedIndex(0);
            if (productTarget != null) {
                for (int i = 0; i < target.getListModel().getSize(); i++) {
                    ProductTarget cat = (ProductTarget) target.getListModel().getElementAt(i);
                    if (productTarget.getId() == cat.getId()) {
                        target.setSelectedIndex(i);
                        break;
                    }
                }
            }

        } else {
            targetCategory.setModel(lstModel);
        }

    }

//    public void onChangeTargetCategory() {
//        String code = targetCategory.getSelectedItem().getValue();
//        List<ProductTarget> typeList = new ProductTargetDAO()
//                .findAllTargetByCategory(code);
//        ListModelArray<ProductTarget> listModelArray = new ListModelArray<ProductTarget>(typeList);
//        target.setModel(listModelArray);
//
//    }
    public void onChangeTargetCategory() {
        try {
            String code = targetCategory.getSelectedItem().getValue();
            List<ProductTarget> typeList = new ProductTargetDAO()
                    .findAllTargetByCategory(code);
            ListModelArray<ProductTarget> listModelArray = new ListModelArray<ProductTarget>(
                    typeList);
            target.setModel(listModelArray);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            target.setModel(new ListModelArray<ProductTarget>(
                    new ArrayList<ProductTarget>()));
        }

        // set empty for target
        productTargetIdHidden.setValue("");
        testMethod.setValue("");
        targetCost.setValue("");
        testValue.setValue("");
        maxValue.setValue("");
        comment.setValue("");

    }

    public void onChangeTarget() {
        Long id;
        ProductTarget productTarget;
        id = Long.valueOf(String.valueOf(target.getSelectedItem().getValue()));
        productTarget = new ProductTargetDAO().findId(id);
        testMethod.setValue(productTarget.getTestMethod());
        targetCost.setValue(String.valueOf(productTarget.getCost()));
        testValue.setValue("");
        maxValue.setValue("");
        productTargetIdHidden.setValue(String.valueOf(productTarget.getId()));

    }

    @Listen("onClick = #btnSave")
    public void onSave() {
        Long productId = Long.valueOf(productIdHidden.getValue());
        ProductProductTarget productProductTarget = new ProductProductTarget();
        ProductProductTargetDAO pdao = new ProductProductTargetDAO();
        if ("update".equals(method)) {
            productProductTarget = pdao.findById(targetId);
        }
        String proTargetId = productTargetIdHidden.getValue();
        if (proTargetId == null) {
            lbtargetWarning.setValue("Chưa chọn chỉ tiêu");
            return;
        }
        String code = "";
        try {
            code = targetCategory.getSelectedItem().getValue();

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            lbtargetWarning.setValue("Chưa nhóm chọn chỉ tiêu");
            return;
        }
        productProductTarget.setType(code);
        productProductTarget.setTypeName(targetCategory.getSelectedItem().getLabel());
        productProductTarget.setName(target.getSelectedItem().getLabel());
        productProductTarget.setTestMethod(testMethod.getValue());
        productProductTarget.setComments(comment.getValue());
        productProductTarget.setCreatedBy(getUserId());
        productProductTarget.setCreatedDate(new Date());
        productProductTarget.setIsActive(Long.valueOf(1));
        productProductTarget.setProductId(productId);
        // if(targetCategory.getSelectedItem())
        productProductTarget.setTestValue(testValue.getValue());
        productProductTarget.setMaxValue(maxValue.getValue());
        productProductTarget.setCost(Long.valueOf(targetCost.getValue()));
        //update
        if ("update".equals(method)) {
            VProductTargetDAO dao = new VProductTargetDAO();
            VProductTarget vp_target = dao.findById(targetId);
            Long productTargetId = vp_target.getProductTargetId();
            productProductTarget.setProductTargetId(productTargetId);
            productProductTarget.setId(targetId);
            String gethiden = productTargetIdHidden.getValue();
            if (gethiden != null && !"".equals(gethiden)) {
                productProductTarget.setProductTargetId(Long.valueOf(productTargetIdHidden.getValue()));
            }
        } else {
            productProductTarget.setProductTargetId(Long.valueOf(productTargetIdHidden.getValue()));
        }
        ProductProductTargetDAO productProductTargetDAO = new ProductProductTargetDAO();
        productProductTargetDAO.saveOrUpdate(productProductTarget);

        //close popup
        onCloseWindow(productId);

    }

//    /*close popup btnSave  */
//    public void onCloseWindow(Long prId) {
//        Map<String, Object> args = new ConcurrentHashMap();
//        args.put("pri_id", prId);
//        Events.sendEvent(new Event("onChildWindowClosed", parentWindow, args));
//
//        if (addNewTargetWindow != null) {
//            addNewTargetWindow.detach();
//        }
//    }
    /*close popup btnSave  */
    public void onCloseWindow(Long prId) {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("pri_id", prId);
        Events.sendEvent(new Event("onChildWindowClosed", parentWindow, args));

        if (addNewTargetWindow != null) {
            addNewTargetWindow.detach();
        }
    }

    public Listbox getTarget() {
        return target;
    }

    public void setTarget(Listbox target) {
        this.target = target;
    }

}
