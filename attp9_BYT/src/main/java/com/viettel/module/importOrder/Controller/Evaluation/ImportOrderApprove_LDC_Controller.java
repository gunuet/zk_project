package com.viettel.module.importOrder.Controller.Evaluation;

import com.viettel.module.cosmetic.Controller.include.*;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zul.Div;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class ImportOrderApprove_LDC_Controller extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    @Wire
    private Window windowEvaluation;
    private Long fileId;
    private Long userEvaluationType;
    private VFileImportOrder cosFile;
    @Wire
    Listbox lbOrderProduct;
    Long documentTypeCode;
    private Files files;
    private String nswFileCode;
    @Wire
    private Textbox userEvaluationTypeH, txtValidate;
    @Wire
    private Radiogroup rbStatus;
    @Wire
    private Textbox mainContent, legalContent, mechanismContent, ProductNameContent, criteriaContent, lbStatus;
    @Wire
    private Button btnApproveFile, btnApproveDispatch, btnPreviewDispatch;
    @Wire
    private Button btnApproveFile2, btnApproveDispatch2, btnPreviewDispatch2;
    private VFileRtfile vFileRtfile;
    private String sLegal, sMechanism, sCriteria, sNameProduct;
    private CosEvaluationRecord obj = new CosEvaluationRecord();

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        //Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
        files = (new FilesDAOHE()).findById(fileId);

        //load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        nswFileCode = getAutoNswFileCode(documentTypeCode);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("id", fileId);
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(fileId);
        ListModelArray lstModelManufacturer = new ListModelArray(importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);

    }

    @Listen("onClick=#btnSubmit")
    public void btnSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
            onApproveFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private String getAutoNswFileCode(Long documentTypeCode) {

        String autoNumber;

        ImportOrderFileDAO dao = new ImportOrderFileDAO();
        Long autoNumberL = dao.countImportfile();
        autoNumberL += 1L;//Tránh số 0 (linhdx)
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }


    public void onApproveFile() throws Exception {
//        if (!isValidate()) {
//            return;
//        }
        CosEvaluationRecordDAO objDAOHE = new CosEvaluationRecordDAO();
        obj = createApproveObject();
        objDAOHE.saveOrUpdate(obj);
        txtValidate.setValue("1");
        //Tu choi
        if (Long.valueOf((String) lbStatus.getValue()).equals(Constants.EVALUTION.FILE_NOK)) {
            base.showNotify("Từ chối hồ sơ thành công!");
        } //Bo sung
        else if (Long.valueOf((String) lbStatus.getValue()).equals(Constants.EVALUTION.FILE_NEED_ADD)) {
            base.showNotify("Yêu cầu bổ sung hồ sơ thành công!");
        } else {
            base.showNotify("Thẩm định đạt hồ sơ thành công!");
        }

    }

    private CosEvaluationRecord createApproveObject() throws Exception {
        Date dateNow = new Date();
        obj.setCreateDate(dateNow);
        obj.setFileId(fileId);
//        obj.setLegal(Long.valueOf((String) lbLegal.getSelectedItem().getValue()));

        obj.setLegalContent(textBoxGetValue(legalContent));
        obj.setStatus(Long.valueOf((String) lbStatus.getValue()));
        obj.setMainContent(textBoxGetValue(mainContent));
//        obj.setCriteria(Long.valueOf((String) lbCriteria.getSelectedItem().getValue()));
        obj.setCriteriaContent(textBoxGetValue(criteriaContent));
//        obj.setMechanism(Long.valueOf((String) lbMechanism.getSelectedItem().getValue()));
        obj.setMechanismContent(textBoxGetValue(mechanismContent));

//        obj.setNameproduct(Long.valueOf((String) lbProductName.getSelectedItem().getValue()));
        obj.setNameproductContent(textBoxGetValue(ProductNameContent));
        obj.setIsActive(Constants.Status.ACTIVE);
        obj.setUserId(getUserId());
        obj.setUserName(getUserFullName());
        return obj;
    }

    @Listen("onChange=#legalContent")
    public void txtlegalContent() {
        SetTextMainContent();
    }

    @Listen("onChange=#ProductNameContent")
    public void txtProductNameContent() {
        SetTextMainContent();
    }

    @Listen("onChange=#criteriaContent")
    public void txtcriteriaContent() {
        SetTextMainContent();
    }

    @Listen("onChange=#mechanismContent")
    public void txtmechanismContent() {
        SetTextMainContent();
    }

    @Listen("onClick=#btnCheck")
    public void onClickbtnCheck() {
        Map<String, Object> agrs = new ConcurrentHashMap<String, Object>();
        agrs.put("parentWindow", windowEvaluation);
        agrs.put("fileId", fileId);
        createWindow("windowCheckComponent ", "/Pages/module/cosmetic/include/evaluation/evaluationCheckComponent.zul", agrs, Window.OVERLAPPED);
    }


    public void SetTextMainContent() {
        sLegal = "Pháp chế: " + legalContent.getValue() + ".\n";
        sNameProduct = "Tên sản phẩm: " + ProductNameContent.getValue() + ".\n";
        sCriteria = "Chỉ tiêu yêu cầu của Hiệp định mỹ phẩm ASEAN và các phụ lục: " + criteriaContent.getValue() + ".\n";
        sMechanism = "Mục đích sử dụng: " + mechanismContent.getValue();

        String sTxt = "";
        if (legalContent.getValue() != "" && legalContent.getValue() != null) {
            sTxt += sLegal;
        }
        if (ProductNameContent.getValue() != "" && ProductNameContent.getValue() != null) {
            sTxt += sNameProduct;
        }
        if (criteriaContent.getValue() != "" && criteriaContent.getValue() != null) {
            sTxt += sCriteria;
        }
        if (mechanismContent.getValue() != "" && mechanismContent.getValue() != null) {
            sTxt += sMechanism;
        }
        mainContent.setValue(sTxt);
    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    private void setWarningMessage(String message) {

        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);

    }

    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        return Constants.CHECK_VIEW.VIEW;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }
     public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }
     public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }
}
