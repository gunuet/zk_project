package com.viettel.module.importOrder.Controller.Evaluation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Controller.include.EvaluationController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VProductTargetDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;

/**
 *
 * @author Linhdx
 */
public class ImportOrderRequireAdditionalTestingController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, labelCriteria,
            labelMechanism, labelLegalRequi, labelCriteriaRequi, labelMechanismRequi,
            labelProductNameRequi, labelProductName, labelChiTieu, lbProductWarning;
    @Wire
    private Window windowEvaluation;
    private Long fileId;
    private VFileImportOrder cosFile;
    @Wire
    private Groupbox gbAddtionalRequest;
    @Wire
    Listbox lbOrderProduct;
    @Wire
    Listbox lbProductTarget;
    Long documentTypeCode;
    private Files files;
    private String nswFileCode;
    private List<Category> listImportOderFileType;
    @Wire
    private Textbox userEvaluationTypeH, txtValidate, txtMessage;
    @Wire
    private Radiogroup rbStatus;
    @Wire
    private Textbox mainContent, legalContent, mechanismContent, ProductNameContent, criteriaContent, lbStatus;
    @Wire
    private Listbox lbEffective, lbLegal, lbMechanism, lbCriteria, lbProductName, labelLegal, lboxLegal, lboxlLegalCT;
    @Wire
    private Button btnApproveFile, btnApproveDispatch, btnPreviewDispatch;
    @Wire
    private Button btnApproveFile2, btnApproveDispatch2, btnPreviewDispatch2;
    private EvaluationRecord obj;
    @Wire
    private Window businessWindow;
    @Wire("#incList #lbList")
    private Listbox lbList;

    private Long productId;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        //Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
        files = (new FilesDAOHE()).findById(fileId);

        //load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        nswFileCode = getAutoNswFileCode(documentTypeCode);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("id", fileId);
//        Long userId = getUserId();
//        UserDAOHE userDAO = new UserDAOHE();
//        user = userDAO.findById(userId);

        /* collect product chat + thuong*/
        //get product kiem tra chat
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileIdAndCheckMethodCode(fileId, "CHAT");
        //get product kiem tra thuong
        List<ImportOrderProduct> importOrderProducts2 = new ImportOrderProductDAO().findAllIdByFileIdAndCheckMethodCode(fileId, "THUONG");
        //collect product chat + thuong
        importOrderProducts.addAll(importOrderProducts2);
        // display
        if (!importOrderProducts.isEmpty()) {
            ListModelArray lstModelManufacturer = new ListModelArray(importOrderProducts);
            lbOrderProduct.setModel(lstModelManufacturer);

            // display target for first product
            Object obj = lstModelManufacturer.get(0);
            if (obj != null) {
                Long productId = ((ImportOrderProduct) obj).getProductId();
                displayAllProductProductTarget(productId);
            }

        }
    }

//    @Listen("onOpenUpdate = #lbProductTarget")
//    public void onOpenUpdate(Event event) {
//        VProductTarget obj = (VProductTarget) event.getData();
//        Long targetId = obj.getId();
//        Map<String, Object> arguments = new ConcurrentHashMap<>();
//        arguments.put("producId", producIdHidden.getValue());
//        arguments.put("targetId", targetId);
//        arguments.put("method", "update");
//      //  arguments.put("parentWindow", businessWindow);
//        setParam(arguments);
//        createWindow("windowCRUDCosmetic", "/Pages/module/importorder/evaluationOrder/importOrderAddNewTargetsRequireAdditionalTesting.zul", arguments, Window.HIGHLIGHTED);
//    }
//    public boolean isAbleToDelete(VProductTarget obj) {
//        if (user.getUserId() != null && user.getUserId().equals(obj.getCreatorId())) {
//            if (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.INITIAL)
//                    || (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.VT_RETURN))) {
//                return true;
//            }
//            return false;
//        }
//        return false;
//    }
//    @Listen("onDelete = #lbProductTarget")
//    public void onDelete(Event event) {
//        final VProductTarget obj = (VProductTarget) event.getData();
//        String message = String.format(Constants.Notification.DELETE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
//        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
//                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
//
//                    @Override
//                    public void onEvent(Event event) {
//                        if (null != event.getName()) {
//                            switch (event.getName()) {
//                                case Messagebox.ON_OK:
//                                    // OK is clicked
//                                    try {
//
//                                        VProductTargetDAO objDAOHE = new VProductTargetDAO();
//                                        objDAOHE.delete(obj.getId());
//                                        reload();
//
//                                    } catch (Exception e) {
//                                        showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
//                                        LogUtils.addLogDB(e);
//                                    } finally {
//                                    }
//                                    break;
//                                case Messagebox.ON_NO:
//                                    break;
//                            }
//                        }
//                    }
//                });
//    }
//    public void reload() {
//        String prId = producIdHidden.getValue();
//        String fileID = lbFileId.getValue();
//        Long productId = new Long(prId);
//        List<VProductTarget> productProductTarget = new VProductTargetDAO().findByProdutId(productId);
//        ListModelArray lstModel_ProducTarget = new ListModelArray(productProductTarget);
//        lbProductTarget.setModel(lstModel_ProducTarget);
//    }
//    @Listen("onClick=#btnSave")
//    public void onClickbtnbtnSave() {
//        Map<String, Object> agrs = new ConcurrentHashMap<String, Object>();
//        String prId = producIdHidden.getValue();
//        String fileID = lbFileId.getValue();
//        Long productId = new Long(prId);
//        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
//        ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(productId);
//        String checkMethodName = lbtexMethodName.getValue();
//        String checkMethodCode = lbtexMethodCode.getValue();
//        if (checkMethodCode == null || "".equals(checkMethodCode)) {
//            importOrderProduct.setCheckMethodName("");
//        } else {
//            importOrderProduct.setCheckMethodName(checkMethodName);
//        }
//        importOrderProduct.setCheckMethodCode(checkMethodCode);
//
//        importOrderProductDAO.saveOrUpdate(importOrderProduct);
//
//        //load lai danh sachs san pham
//        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(new Long(fileID));
//        ListModelArray lstModelManufacturer = new ListModelArray(importOrderProducts);
//        lbOrderProduct.setModel(lstModelManufacturer);
//
//    }
    @Listen("onShow =  #lbOrderProduct")
    public void onShow(Event event) {
        ImportOrderProduct obj = (ImportOrderProduct) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        productId = obj.getProductId();
        arguments.put("id", productId);
        setParam(arguments);

        //set list danh sách chỉ tiêu của sản một sản phẩm
        displayAllProductProductTarget(productId);

    }

    private void displayAllProductProductTarget(Long productId) {
        List<VProductTarget> productProductTarget = new VProductTargetDAO().findByProdutId(productId);
        if (productProductTarget != null) {
            ListModelArray lstModel_ProducTarget = new ListModelArray(productProductTarget);
            //get san pham
//            ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
//            ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(productId);
            lbProductTarget.setModel(lstModel_ProducTarget);
        } else {
            lbProductTarget.setModel(new ListModelArray(new ArrayList<VProductTarget>()));
        }
    }

    @Listen("onClick =  checkbox")
    public void onClickCheckBox(Event event) {
        Checkbox checkbox = (Checkbox) event.getTarget();
        Long product_id = Long.valueOf(String.valueOf(checkbox.getValue()));
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(product_id);
        if (importOrderProduct != null) {
            if (checkbox.isChecked()) {
                importOrderProduct.setReTest(new Long(1));
            } else {
                importOrderProduct.setReTest(new Long(0));
            }
            importOrderProductDAO.saveOrUpdate(importOrderProduct);
        }
    }

//    @Listen("onClick =  #btnAddNew")
//    public void onShowNewAdd() {
//        Map<String, Object> arguments = new ConcurrentHashMap<>();
//        arguments.put("producId", producIdHidden.getValue());
//        arguments.put("targetId", new Long("0"));
//        arguments.put("method", null);
//        //   arguments.put("parentWindow", businessWindow);
//        setParam(arguments);
//        createWindow("windowCRUDCosmetic", "/Pages/module/importorder/evaluationOrder/importOrderAddNewTargetsRequireAdditionalTesting.zul", arguments, Window.HIGHLIGHTED);
//    }
    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", businessWindow);
        return arguments;

    }

//    @Listen("onChildWindowClosed=#businessWindow")
//    public void onChildWindowClosed() {
//        String prId = producIdHidden.getValue();
//        Long productId = new Long(prId);
//        fillListData(productId);
//    }
    private void fillListData(Long productId) {
        List<VProductTarget> productProductTarget = new VProductTargetDAO().findByProdutId(productId);
        if (productProductTarget != null) {
            ListModelArray lstModel_ProducTarget = new ListModelArray(productProductTarget);
            lbProductTarget.setModel(lstModel_ProducTarget);
        }

    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        clearWarningMessage();
        try {
            onApproveFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void onApproveFile() throws Exception {
        if (!isValidate()) {
            return;
        }
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        List<ImportOrderProduct> importOrderProducts = importOrderProductDAO.findAllIdByFileIdAndCheckMethodCode(fileId, "CHAT");
        //get product kiem tra thuong
        List<ImportOrderProduct> importOrderProducts2 = importOrderProductDAO.findAllIdByFileIdAndCheckMethodCode(fileId, "THUONG");
        //collect product chat + thuong
        importOrderProducts.addAll(importOrderProducts2);
        // display
        if (!importOrderProducts.isEmpty()) {
            for (int i = 0; i < importOrderProducts.size(); i++) {
                ImportOrderProduct temp = importOrderProducts.get(i);
                
                if(temp.getReTest()==1l)
                {
                	temp.setPass(0L);
                	importOrderProductDAO.saveOrUpdate(temp);
                }
            }
        }
        
        sendMS();

        txtValidate.setValue("1");

    }

    public boolean isValidate() {
        String message;
        //load lai danh sachs san pham
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileIdAndCheckMethodCode(fileId, "CHAT");
        //get product kiem tra thuong
        List<ImportOrderProduct> importOrderProducts2 = new ImportOrderProductDAO().findAllIdByFileIdAndCheckMethodCode(fileId, "THUONG");
        //collect product chat + thuong
        importOrderProducts.addAll(importOrderProducts2);
        //  List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(fileId);
        DANHSACH:
        for (ImportOrderProduct importOrder : importOrderProducts) {
            ImportOrderProduct obj = importOrder;
            if (obj.getReTest() == null || obj.getReTest() == 0) {
                message = "Chưa chọn bổ sung cho sản phẩm.Vui lòng kiểm tra lại ";
                showWarningMessage(message);
                return false;
            } else {
                break ;               
            }
        }
        return true;
    }

    protected void showWarningMessage(String message) {
        lbBottomWarning.setValue(message);
    }

    private void clearWarningMessage() {
        lbBottomWarning.setValue("");
    }

    private String getAutoNswFileCode(Long documentTypeCode) {

        String autoNumber;

        ImportOrderFileDAO dao = new ImportOrderFileDAO();
        Long autoNumberL = dao.countImportfile();
        autoNumberL += 1L;//Tránh số 0 (linhdx)
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

//    public void onChangelbLegal() {
//        String value = cbtexLegal.getSelectedItem().getValue();
//        if (value == null || !value.equals("CHAT")) {
//            lbProductTarget.setVisible(false);
//            btnAddNew.setVisible(false);
//        } else {
//            lbProductTarget.setVisible(true);
//            btnAddNew.setVisible(true);
//        }
//        // }
//
//    }
//
//    /* Tichnv :load phuong thc kie tra trong bảng Category
//     @param : 
//     @return
//     */
//    public void getListBoxModel(String methodCode) {
//        CategoryDAOHE categoryDAOHE;
//        cbtexLegal.getItems().clear();
//        categoryDAOHE = new CategoryDAOHE();
//        listImportOderFileType = categoryDAOHE.findAllCategory(
//                Constants.CATEGORY_TYPE.METHOD_IMDOC);
//        String name = null;
//        if (listImportOderFileType != null && listImportOderFileType.size() > 0) {
//            for (Category d : listImportOderFileType) {
//                if (name == null && (d.getCode() == null ? methodCode == null : d.getCode().trim().equals(methodCode.trim()))) {
//                    name = d.getName();
//                }
//                Comboitem ci = new Comboitem();
//                ci.setValue(d.getCode());
//                ci.setLabel(d.getName());
//                cbtexLegal.getItems().add(ci);
//            }
//        }
//        cbtexLegal.setValue(name);
//    }

    /* Tichnv :nhoms chir tieeu
     @param : 
     @return
     */


    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    public EvaluationRecord getObj() {
        return obj;
    }

    public void setObj(EvaluationRecord obj) {
        this.obj = obj;
    }

    private void setWarningMessage(String message) {

        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);

    }

    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        return Constants.CHECK_VIEW.VIEW;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }
    
    private void sendMS(){
		Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_24);
        md.setPhase(0l);
        md.setFeeUpdate(false);            
        String jsonMd = gson.toJson(md);        
        txtMessage.setValue(jsonMd);
	}
}
