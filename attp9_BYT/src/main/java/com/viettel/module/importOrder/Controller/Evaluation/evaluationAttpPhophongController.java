package com.viettel.module.importOrder.Controller.Evaluation;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.Controller.include.EvaluationController;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VProductTargetDAO;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;

import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listitem;

/**
 *
 * @author THANHDV
 */
public class evaluationAttpPhophongController extends BusinessController {

	/**
     *
     */
	private static final long serialVersionUID = 1L;
	@Wire
	private Label lbTopWarning, lbBottomWarning, labelCriteria, labelMechanism,
			labelLegalRequi, labelCriteriaRequi, labelMechanismRequi,
			labelProductNameRequi, labelProductName, labelChiTieu,
			producIdHidden, lbFileId;
	private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
	@Wire
	private Window windowEvaluation;
	private Long fileId;
	private VFileImportOrder cosFile;
	@Wire
	Listbox lbOrderProduct;
	Long documentTypeCode;
	private Files files;
	private String nswFileCode;
	private List<Category> listImportOderFileType;
	private String FILE_TYPE_STR;
	private long FILE_TYPE;
	private long DEPT_ID;
	private Users user;
	@Wire
	private Textbox userEvaluationTypeH, txtValidate;
	@Wire
	private Radiogroup rbStatus;
	@Wire
	private Textbox mainContent, legalContent, mechanismContent,
			ProductNameContent, criteriaContent, lbStatus;
	@Wire
	private Listbox lbEffective, lbLegal, lbMechanism, lbCriteria,
			lbProductName, lboxLegal, lboxlLegalCT;
	@Wire
	private Button btnApproveFile, btnApproveDispatch, btnPreviewDispatch;
	@Wire
	private Button btnApproveFile2, btnApproveDispatch2, btnPreviewDispatch2;
	private VFileRtfile vFileRtfile;
	private String sLegal, sMechanism, sCriteria, sNameProduct;
	private CosEvaluationRecord obj = new CosEvaluationRecord();
	@Wire
	private Window businessWindow;
	@Wire
	private Combobox cbtexLegal;
	@Wire("#incList #lbList")
	private Listbox lbList;
	@Wire
	private Label lbCheckDepartment;

	@Override
	public ComponentInfo doBeforeCompose(Page page, Component parent,
			ComponentInfo compInfo) {
		Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
		fileId = (Long) arguments.get("fileId");
		// Load Order
		cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
		files = (new FilesDAOHE()).findById(fileId);

		// load ho so
		documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
		nswFileCode = getAutoNswFileCode(documentTypeCode);

		return super.doBeforeCompose(page, parent, compInfo);
	}

	@SuppressWarnings("rawtypes")
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		txtValidate.setValue("0");
		Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
		arguments.put("id", fileId);
		Long userId = getUserId();
		UserDAOHE userDAO = new UserDAOHE();
		user = userDAO.findById(userId);
		List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO()
				.findAllIdByFileId(fileId);
		ListModelArray lstModelManufacturer = new ListModelArray(
				importOrderProducts);
		lbOrderProduct.setModel(lstModelManufacturer);
		lbFileId.setValue(String.valueOf(fileId));

		// load co quan kiem tra
		//loadCheckDepartment();
	}

	@SuppressWarnings("rawtypes")
	@Listen("onShow =  #lbOrderProduct")
	public void onShow(Event event) {
		ImportOrderProduct obj = (ImportOrderProduct) event.getData();
		Map<String, Object> arguments = new ConcurrentHashMap<>();
		Long productId = obj.getProductId();
		arguments.put("id", productId);
		// arguments.put("CRUDMode", "UPDATE");
		setParam(arguments);
		// getListBoxModel(obj.getCheckMethodCode());

		// set view

		producIdHidden.setValue(String.valueOf(productId));
	}

	private Map<String, Object> setParam(Map<String, Object> arguments) {
		arguments.put("parentWindow", businessWindow);
		arguments.put("filetype", FILE_TYPE_STR);
		arguments.put("procedureId", FILE_TYPE);
		arguments.put("deptId", businessWindow);
		return arguments;

	}

	@Listen("onClick=#btnSubmit")
	public void btnSubmit() {
		try {
			onSave();
		} catch (Exception ex) {
			LogUtils.addLogDB(ex);
		}
	}

	@Listen("onClick = #btnSave, .saveClose")
	public void onSave() throws Exception {
		clearWarningMessage();
		try {
			// Neu khong dong y thi yeu cau nhap comment va luu comment
			if (!isValidatedData()) {
				return;
			}
			txtValidate.setValue("1");
		} catch (WrongValueException ex) {
                        LogUtils.addLogDB(ex);
			showNotification(String.format(Constants.Notification.SAVE_ERROR,
					Constants.DOCUMENT_TYPE_NAME.FILE),
					Constants.Notification.ERROR);
		}
	}

//	// load co quan kiem tra
//	private void loadCheckDepartment() {
//		// load content
//		EvaluationRecordDAO evaluationRecordDAO = new EvaluationRecordDAO();
//		// get lastest evaluationRecord
//		EvaluationRecord evaluationRecord = evaluationRecordDAO
//				.findMaxByFileIdAndEvalType(fileId, Constants.EVAL_TYPE.CHECK_DEPT);
//		if (evaluationRecord != null) {
//			String formContent = evaluationRecord.getFormContent();
//			if (formContent != null) {
//				EvaluationModel evaluationModel = new Gson().fromJson(
//						formContent, EvaluationModel.class);
//				lbCheckDepartment.setValue(evaluationModel.getCheckDeptName());
//			}
//		}
//	}

	private boolean isValidatedData() {
		return true;
	}

	protected void clearWarningMessage() {
		lbTopWarning.setValue("");
		lbBottomWarning.setValue("");
	}

	private String getAutoNswFileCode(Long documentTypeCode) {

		String autoNumber;

		ImportOrderFileDAO dao = new ImportOrderFileDAO();
		Long autoNumberL = dao.countImportfile();
		autoNumberL += 1L;// Tránh số 0 (linhdx)
		autoNumber = String.valueOf(autoNumberL);
		Integer year = Calendar.getInstance().get(Calendar.YEAR);
		int len = String.valueOf(autoNumber).length();
		if (len < 6) {
			for (int a = len; a < 6; a++) {
				autoNumber = "0" + autoNumber;
			}
		}
		return documentTypeCode + year.toString() + autoNumber;

	}

	public int getSelectedIndexInModel(int type) {
		int selectedItem = 0;
		return selectedItem;
	}

	public CosEvaluationRecord getObj() {
		return obj;
	}

	public void setObj(CosEvaluationRecord obj) {
		this.obj = obj;
	}

	private void setWarningMessage(String message) {

		lbTopWarning.setValue(message);
		lbBottomWarning.setValue(message);

	}

	public VFileImportOrder getCosFile() {
		return cosFile;
	}

	public void setCosFile(VFileImportOrder cosFile) {
		this.cosFile = cosFile;
	}

	public int checkViewProcess() {
		return checkViewProcess(files.getFileId());
	}

	public int checkViewProcess(Long fileId) {
		return Constants.CHECK_VIEW.VIEW;
	}

	public String getNswFileCode() {
		return nswFileCode;
	}

	public void setNswFileCode(String nswFileCode) {
		this.nswFileCode = nswFileCode;
	}

	public Files getFiles() {
		return files;
	}

	public void setFiles(Files files) {
		this.files = files;
	}

	public String getStatus(Long status) {
		return WorkflowAPI.getStatusName(status);
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

}
