package com.viettel.module.importOrder.Controller;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;

public class IO_10_2_CQKTDongYController extends BusinessController {

	private static final long serialVersionUID = 1L;

	private Long fileId;
	@Wire
	private Textbox txtMessage, txtValidate;
	@Wire
	private Listbox finalFileListbox, lbOrderProduct;

	@Override
	public ComponentInfo doBeforeCompose(Page page, Component parent,
			ComponentInfo compInfo) {
		Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
		fileId = (Long) arguments.get("fileId");

		return super.doBeforeCompose(page, parent, compInfo);
	}

	@Override
	public void doAfterCompose(Component window) throws Exception {
		super.doAfterCompose(window);
/*		List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO()
				.findAllIdByFileIdAndCheckMethodCode(fileId, "CHAT");*/
		List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(fileId);
		ListModelArray lstModelManufacturer = new ListModelArray(
				importOrderProducts);
		lbOrderProduct.setModel(lstModelManufacturer);
		fillFinalFileListbox(fileId);
	}

	@Listen("onClick=#btnSubmit")
	public void btnSubmit(Event event) {

		Gson gson = new Gson();
		MessageModel md = new MessageModel();
		md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
		md.setFileId(fileId);
		md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_10);
		md.setPhase(0l);
		md.setFeeUpdate(false);
		String jsonMd = gson.toJson(md);
		txtMessage.setValue(jsonMd);

		txtValidate.setValue("1");
	}

	// load all of singed pdf file
	private void fillFinalFileListbox(Long fileId) {
		AttachDAOHE rDaoHe = new AttachDAOHE();
		List<Attachs> lstAttach = rDaoHe
				.getByObjectIdAndAttachCatAndAttachType(
						fileId,
						Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
						Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
		this.finalFileListbox.setModel(new ListModelArray(lstAttach));
	}

	// download signed pdf
	@Listen("onDownloadFinalFile = #finalFileListbox")
	public void onDownloadFinalFile(Event event) throws FileNotFoundException {
		Attachs attachs = (Attachs) event.getData();
		AttachDAO attDAO = new AttachDAO();
		attDAO.downloadFileAttach(attachs);
	}
}
