package com.viettel.module.importOrder.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VFeeDAO;
import com.viettel.module.importOrder.Model.IO_23_1_LDC_ATTP_KSTKQXLModel;
import com.viettel.module.importOrder.Model.ImportOrderProductModel;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.WordExportUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;

/**
 *
 * @author Linhdx
 */
public class IO_23_1_LDC_ATTP_KSTKQXLController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate, txtMessage;
    @Wire
    private Listbox finalFileListbox;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    @Wire
    private Textbox txtMainContent;
    @Wire
    private Label lbNote;

    // private Permit permit;
    private List listBook;
    private Long docType;
    private String bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_PERMIT;
    private String KySoSanPhamKhongDat = "/WEB-INF/template/KySoSanPhamKhongDat.docx";

    /**
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        // load all of signed file
        fillFinalFileListbox(fileId);
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_26);
        md.setPhase(0l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

    private boolean isValidatedData() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(
                        fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
        if (lstAttach.size() == 0) {
            setWarningMessage("Chưa ký văn bản");
            return false;
        }
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            // onApproveFile();
            createPayment();
            sendMS();
            txtValidate.setValue("1");


        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    /**
     *
     * @param event
     * @throws Exception
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) throws Exception {
        try
        {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
        String folderSignOut = resourceBundle.getString("signPdf");
        if (!new File(folderSignOut).exists()) {
            FileUtil.mkdirs(folderSignOut);
        }
        String fileSignOut = folderSignOut + "_signed_KySoSanPhamKhongDat"
                + (new Date()).getTime() + ".pdf";

        Session session = Sessions.getCurrent();
        SignPdfFile signPdfFile = (SignPdfFile) session
                .getAttribute("PDFSignature");

        String signature = event.getData().toString();
        signPdfFile.insertSignature(signature, fileSignOut);

        attachSave(fileSignOut);

        showNotification("Ký số thành công!", Constants.Notification.INFO);
        // refresh page
        fillFinalFileListbox(fileId);
         }
        catch(Exception ex)
        {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công!", Constants.Notification.ERROR);
        }
    }

    /**
     * save file path into Attach table
     *
     * @throws Exception
     */
    public void attachSave(String filePath) throws Exception {

        AttachDAO attachDAO = new AttachDAO();
        attachDAO.saveFileAttachPdfSign(filePath, fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     * @throws Exception
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        clearWarningMessage();
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        // get fdf file path that is insert data
        String tempPdfPath = createTempPdfFile();

        // sign pdf file (insert img)
        actionSignCA(event, tempPdfPath);
    }

    public String createTempPdfFile() throws Exception {

        // lay cong van
        Permit permit = this.getPermit(fileId);

        // vao so
        Long bookNumber = putInBook(permit.getPermitId());
        String receiveNo = getPermitReceiveNo(bookNumber);

        // update so vao so vao permit
        PermitDAO permitDAO = new PermitDAO();
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        // set infor for model to replay at template file
        ImportOrderFileDAO deviceDAO = new ImportOrderFileDAO();
        ImportOrderFile importOrderFile = deviceDAO.findByFileId(fileId);

        // get chu doanh nghiep
        Files files = new FilesDAOHE().findById(fileId);

        // set current date
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int days = cal.get(Calendar.DAY_OF_MONTH);
        int months = cal.get(Calendar.MONTH) + 1;
        int years = cal.get(Calendar.YEAR);
        String signedDate = "Hà Nội, ngày " + days + " tháng " + months
                + " năm " + years;

        ImportOrderProductDAO proDAO = new ImportOrderProductDAO();
        // get product kiem tra thuong
        List<ImportOrderProduct> productList = proDAO
                .findAllIdByFileIdAndCheckMethodCodeAndNotPass(fileId, "THUONG");
        // get product kiem tra chat
        List<ImportOrderProduct> productList2 = proDAO
                .findAllIdByFileIdAndCheckMethodCodeAndNotPass(fileId, "CHAT");
        // collect product kiem tra thuong + chat
        productList.addAll(productList2);

        List<ImportOrderProductModel> productModelList = new ArrayList<ImportOrderProductModel>();
        ImportOrderProductModel importOrderProductModel;
        Long pass;
        for (Iterator iterator = productList.iterator(); iterator.hasNext();) {
            importOrderProductModel = new ImportOrderProductModel();
            ImportOrderProduct importOrderProduct = (ImportOrderProduct) iterator
                    .next();
            importOrderProductModel.setProductName(importOrderProduct
                    .getProductName());
            importOrderProductModel.setConfirmAnnounceNo(importOrderProduct
                    .getConfirmAnnounceNo());
            importOrderProductModel.setNationalName(importOrderProduct
                    .getNationalName());
            importOrderProductModel.setTotal(importOrderProduct.getTotal());
            importOrderProductModel.setNetweight(importOrderProduct
                    .getNetweight());
            importOrderProductModel.setCheckMethodName(importOrderProduct
                    .getCheckMethodName());
            pass = importOrderProduct.getPass();
            if (pass != null && pass == 1) {
                importOrderProductModel.setPass("Đạt");
            } else {
                importOrderProductModel.setNotPass("Không đạt");
            }

            importOrderProductModel.setReason(importOrderProduct.getReason());
            productModelList.add(importOrderProductModel);
        }

        IO_23_1_LDC_ATTP_KSTKQXLModel model = new IO_23_1_LDC_ATTP_KSTKQXLModel();
        model.setSendNo(permit.getReceiveNo());
        model.setBusinessName(files.getBusinessName());
        model.setBusinessAddress(files.getBusinessAddress());
        model.setBusinessPhone(files.getBusinessPhone());
        model.setBusinessFax(files.getBusinessFax());
        model.setBusinessEmail(files.getBusinessEmail());
        model.setSignedDate(signedDate);
        model.setLeaderSigned(getUserFullName());
        model.setImportOrderFile(importOrderFile);
        model.setProductList(productModelList);

        String tempPdfFile = this.exportTempPdfFile(model);
        return tempPdfFile;
    }

    private Permit getPermit(Long fileId) {
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);
        Permit permit;

        // Da co cong van thi lay ban cu
        if (lstPermit.size() > 0) {
            permit = lstPermit.get(0);
            // Neu khong co thi tao ban moi
        } else {
            permit = new Permit();
            permit.setFileId(fileId);
            permit.setIsActive(Constants.Status.ACTIVE);
            permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
            permit.setSignDate(new Date());
            permit.setReceiveDate(new Date());
            permitDAO.saveOrUpdate(permit);
        }

        return permit;
    }

    public String exportTempPdfFile(IO_23_1_LDC_ATTP_KSTKQXLModel model) {
        String tempPdfFilePath = null;
        try {

            HttpServletRequest request = (HttpServletRequest) Executions
                    .getCurrent().getNativeRequest();
            String docPath = request.getRealPath(KySoSanPhamKhongDat);

            // Neu co dinh nghia bieu mau thi lay tu dinh nghia
            if (model.getPathTemplate() != null) {
                docPath = model.getPathTemplate();
            }

            // Date signedDate = model.getSignedDate();
            // String signDateStr = "";
            // if (signedDate != null) {
            // Calendar cal = Calendar.getInstance();
            // cal.setTime(signedDate);
            // int days = cal.get(Calendar.DAY_OF_MONTH);
            // int months = cal.get(Calendar.MONTH) + 1;
            // int years = cal.get(Calendar.YEAR);
            // signDateStr = "Hà Nội, ngày " + days + " tháng " + months
            // + " năm " + years;
            // } else {
            // int days = signedDate.getDay();
            // int months = signedDate.getMonth();
            // int years = signedDate.getYear();
            // signDateStr = "Hà Nội, ngày " + days + " tháng " + months
            // + " năm " + years;
            // }

            /* replace template doc by data of model */
            WordprocessingMLPackage wmp = WordprocessingMLPackage
                    .load(new FileInputStream(docPath));

            ConcurrentHashMap map = new ConcurrentHashMap();
            map.put("createForm", model);

            WordExportUtils word = new WordExportUtils();
            // word.replacePlaceholder(wmp, signDateStr, "${signDateStr}");
            word.replacePlaceholder(wmp, map);

            word.replaceTable(wmp, 0, model.getProductList());

            // convert word to PDF
            PdfDocxFile pdfDocxFile = word.writePDFToStream(wmp, false);

            // write pdf to file
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String tempFolder = resourceBundle.getString("signTemp");
            if (!new File(tempFolder).exists()) {
                FileUtil.mkdirs(tempFolder);
            }

            tempPdfFilePath = tempFolder + "KySoSanPhamKhongDat_"
                    + (new Date()).getTime() + ".pdf";
            OutputStream outputStream = new FileOutputStream(tempPdfFilePath);
            outputStream.write(pdfDocxFile.getContent());
            outputStream.flush();
            outputStream.close();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return tempPdfFilePath;
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Long permitId) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(permitId,
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param documentId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long documentId, Long bookId) {

        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);

        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(documentId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);

        // Cap nhat so hien tai trong bang book
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);

        return bookDocument;
    }

    private String getPermitReceiveNo(Long bookNumber) {
        String permitReceiveNo = "";

        if (bookNumber != null) {
            permitReceiveNo = String.valueOf(bookNumber);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy");
        String year = simpleDateFormat.format(Calendar.getInstance().getTime());
        permitReceiveNo += "/" + year + "/CBMP-QLD";

        return permitReceiveNo;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        // FileUtil.mkdirs(filePath);
        // String outputFileFinalName =
        String certSerial = x509Cert.getSerialNumber().toString(16);
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caUserList = ca
                .findCaBySerial(certSerial, 1L, getUserId());
        if (caUserList != null && caUserList.size() > 0) {
            SignPdfFile pdfSig = new SignPdfFile();
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String linkImageSign = folderPath + separator
                    + caUserList.get(0).getSignature();
            String linkImageStamp = folderPath + separator
                    + caUserList.get(0).getStamper();
            Pdf pdf = new Pdf();

            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String outputFileFinal = resourceBundle.getString("signTemp") + "_"
                    + (new Date()).getTime() + ".pdf";
            // chen chu ki
            pdf.insertImageAll(fileToSign, outputFileFinal, linkImageSign,
                    linkImageStamp, null);
            // chen CKS
            if (pdf.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }
            String base64Hash = pdfSig.createHash(outputFileFinal,
                    new Certificate[]{x509Cert});
            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64HASH.setValue(base64Hash);
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");

        } else {
            showNotification("Ký số không thành công!");
        }
    }

    // load all of singed pdf file
    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(
                        fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    // private Permit createPermit() throws Exception {
    // permit.setFileId(fileId);
    // permit.setIsActive(Constants.Status.ACTIVE);
    // permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
    // permit.setSignDate(new Date());
    // permit.setReceiveDate(new Date());
    // return permit;
    // }
    // download signed pdf
    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs attachs = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(attachs);
    }

    // delete signed pdf
    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    private void setWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    private void createPayment() {
        Long phase = 1l;
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        List passProductList = importOrderProductDAO
                .findAllIdByFileIdAndCheckMethodCode(fileId, "GIAM");
        if (passProductList != null) {

            int productSize = passProductList.size();

            PaymentInfoDAO paymentInfoDAOHE = new PaymentInfoDAO();

            PaymentInfo paymentInfo = new PaymentInfo();
            // set thông tin tài khoản của cục
            DepartmentDAOHE departmentDAOHE = new DepartmentDAOHE();

            Department depart_obj = departmentDAOHE
                    .findById(Constants.CUC_ATTP_XNN_ID);
            if (depart_obj != null) {
                paymentInfo.setBankNo(depart_obj.getBankNo());
                paymentInfo.setDeptCode(depart_obj.getDeptCode());
                paymentInfo.setDeptName(depart_obj.getDeptName());
            }
            paymentInfo.setFileId(fileId);

            long feeId = 7451;
            String name = new VFeeDAO().findById(feeId).getName();
            paymentInfo.setFeeId(feeId);
            paymentInfo.setFeeName(name);

            paymentInfo.setIsActive(Constants.Status.ACTIVE);
            paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_NEW);
            paymentInfo.setPhase(phase);

            long valueL = 500000 * productSize + 150000;
            paymentInfo.setCost(valueL);

            paymentInfoDAOHE.saveOrUpdate(paymentInfo);
        }

        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_11_12_13_14);
        md.setPhase(phase);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

}
