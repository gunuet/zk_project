package com.viettel.module.importOrder.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.Model.IO_4_6_TPKYCBSModel;
import com.viettel.module.importOrder.Model.TempContent;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.*;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;

/**
 *
 * @author Linhdx
 */
public class IO_27_1_TPKYCBSController1 extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Listbox finalFileListbox;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    @Wire
    private Textbox txtMainContent;
    private Textbox txtNote = (Textbox) Path
            .getComponent("/windowProcessing/txtNote");
    @Wire
    private Label lbNote;
    @Wire
    private Textbox txtMessage;
    
    private Permit permit;
    
    private List listBook;
    private Long docType;
    private String bCode;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private Long evalType = 1L;
    private String PheDuyetYeuCauBoSung = "/WEB-INF/template/ThongBaoSuaDoiBoSung.docx";

    /**
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_ADD_REQUEST_XNK;
        return super.doBeforeCompose(page, parent, compInfo);
    }
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillFinalFileListbox(fileId);
        txtValidate.setValue("0");

        // load content
        EvaluationRecord evaluationRecord = new EvaluationRecordDAO()
                .findMaxByFileIdAndEvalType(fileId, evalType);
        String formContent = evaluationRecord.getFormContent();
        Gson gson = new Gson();
        EvaluationModel evaluationModel = gson.fromJson(formContent,
                EvaluationModel.class);
        
        if (evaluationModel != null) {
            txtMainContent.setValue(evaluationModel.getContent());
            lbNote.setValue(evaluationModel.getResultEvaluationStr());
        }
    }
    
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }
    
    private void saveContent() {
        // get user infor
        String note = txtNote.getValue();
        String mainContentValue = txtMainContent.getValue();

        // create json for formContent
        EvaluationModel evaluationModel = new EvaluationModel();
        evaluationModel.setUserId(getUserId());
        evaluationModel.setUserName(getUserName());
        evaluationModel.setDeptId(getDeptId());
        evaluationModel.setDeptName(getDeptName());
        evaluationModel.setContent(mainContentValue);
        evaluationModel.setResultEvaluation(new Long(0));
        evaluationModel.setResultEvaluationStr(note);
        String formContent = new Gson().toJson(evaluationModel);

        // set data insert DB
        EvaluationRecord evaluationRecord = new EvaluationRecord();
        evaluationRecord.setMainContent(mainContentValue);
        evaluationRecord.setFormContent(formContent);
        evaluationRecord.setEvalType(evalType);
        evaluationRecord.setFileId(fileId);
        evaluationRecord.setCreatorId(getUserId());
        evaluationRecord.setCreatorName(getUserFullName());
        evaluationRecord.setCreateDate(new Date());
        EvaluationRecordDAO evaluationRecordDAO = new EvaluationRecordDAO();
        evaluationRecordDAO.saveOrUpdate(evaluationRecord);
    }
    
    private boolean isValidatedData() {
        // get addRequest lastest
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO
                .findAllActiveByFileId(fileId);
        // get file attach
        if (lstAdd.size() > 0) {
            AdditionalRequest additionalRequest = lstAdd.get(0);
            AttachDAOHE rDaoHe = new AttachDAOHE();
            List<Attachs> lstAttach = rDaoHe
                    .getByObjectIdAndAttachCatAndAttachType(
                            additionalRequest.getAdditionalRequestId(),
                            Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                            Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
            if (lstAttach.size() > 0) {
                return true;
            }
        }
        
        setWarningMessage("Chưa ký văn bản");
        return false;
        
    }
    
    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_04);
        md.setPhase(2l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            
            sendMS();
            //cập nhật lại trạng thái đã gửi của file đã ký sửa dổi bổ sung
            updateAttachFileSigned();
            
            txtValidate.setValue("1");

            
        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }
    
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) throws Exception {
        try
        {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
        String folderSignOut = resourceBundle.getString("signPdf");
        if (!new File(folderSignOut).exists()) {
            FileUtil.mkdirs(folderSignOut);
        }
        String fileSignOut = folderSignOut + "_signed_PheDuyetYeuCauBoSung"
                + (new Date()).getTime() + ".pdf";
        
        Session session = Sessions.getCurrent();
        SignPdfFile signPdfFile = (SignPdfFile) session
                .getAttribute("PDFSignature");
        
        String signature = event.getData().toString();
        signPdfFile.insertSignature(signature, fileSignOut);
        
        attachSave(fileSignOut);
        
        showNotification("Ký số thành công!", Constants.Notification.INFO);
        // refresh page
        fillFinalFileListbox(fileId);
         }
        catch(Exception ex)
        {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công!", Constants.Notification.ERROR);
        }
    }
    
    public void attachSave(String filePath) throws Exception {
        // update addrequest
        updateOrCreateAddRequest(fileId);

        // get addRequest lastest
        AdditionalRequest additionalRequest;
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO
                .findAllActiveByFileId(fileId);

        // save file ky so
        if (lstAdd.size() > 0) {
            additionalRequest = lstAdd.get(0);
            AttachDAO attachDAO = new AttachDAO();
            attachDAO.saveFileAttachPdfSign(filePath,
                    additionalRequest.getAdditionalRequestId(),
                    Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                    Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
        }
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     * @throws Exception
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        clearWarningMessage();
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        String text_content = txtMainContent.getText();
        if (text_content == null || "".equals(text_content)) {
            lbBottomWarning
                    .setValue("Chưa nhập nội dung yêu cầu sửa đổi bổ sung");
            return;
        }
        // save content
        saveContent();

		// get fdf file path that is insert data
        String tempPdfPath = createTempPdfFile();

        // sign pdf file (insert img)
        actionSignCA(event, tempPdfPath);
    }
    
    public String createTempPdfFile() throws Exception {

        // lay cong van
        Permit permit = this.getPermit(fileId);

        // vao so
        Long bookNumber = putInBook(permit.getPermitId());
        String receiveNo = getPermitReceiveNo(bookNumber);

        // update so vao so vao permit
        PermitDAO permitDAO = new PermitDAO();
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int days = cal.get(Calendar.DAY_OF_MONTH);
        int months = cal.get(Calendar.MONTH) + 1;
        int years = cal.get(Calendar.YEAR);
        String signedDate = "Hà Nội, ngày " + days + " tháng " + months
                + " năm " + years;
        
        Files files = new FilesDAOHE().findById(fileId);
        
        EvaluationRecord evaluationRecord = new EvaluationRecordDAO()
                .findMaxByFileIdAndEvalType(fileId, evalType);
        String fromContent = evaluationRecord.getFormContent();
        EvaluationModel evaluationModel = new Gson().fromJson(fromContent,
                EvaluationModel.class);
        String content = evaluationModel.getContent();
        if (content != null) {
            content = content.replaceAll("(\r\n|\n)", " ");
        }
        
        IO_4_6_TPKYCBSModel model = new IO_4_6_TPKYCBSModel();
        model.setSendNo(permit.getReceiveNo());
        model.setBusinessName(files.getBusinessName());
        model.setBusinessAddress(files.getBusinessAddress());
        model.setSignedDate(signedDate);
        model.setContentDispatch(content);
        model.setLeaderSigned(getUserFullName());
        
        String tempPdfFile = this.exportTempPdfFile(model);
        return tempPdfFile;
    }
    
    private Permit getPermit(Long fileId) {
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileIdAndType(
                fileId, Constants.PERMIT_TYPE.FILE_SDBS);
        Permit permit;

        // Da co cong van thi lay ban cu
        if (lstPermit.size() > 0) {
            permit = lstPermit.get(0);
            // Neu khong co thi tao ban moi
        } else {
            permit = new Permit();
            permit.setFileId(fileId);
            permit.setType(Constants.PERMIT_TYPE.FILE_SDBS);
            permit.setIsActive(Constants.Status.ACTIVE);
            permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
            permit.setSignDate(new Date());
            permit.setReceiveDate(new Date());
            permitDAO.saveOrUpdate(permit);
        }
        
        return permit;
    }
    
    public String exportTempPdfFile(IO_4_6_TPKYCBSModel model)
            throws Docx4JException, IOException, JAXBException {
        String tempPdfFilePath;
        
        HttpServletRequest request = (HttpServletRequest) Executions
                .getCurrent().getNativeRequest();
        String docPath = request.getRealPath(PheDuyetYeuCauBoSung);

        /* replace template doc by data of model */
        WordprocessingMLPackage wmp = WordprocessingMLPackage
                .load(new FileInputStream(docPath));
        
        ConcurrentHashMap map = new ConcurrentHashMap();
        map.put("createForm", model);
        String text_content = txtMainContent.getText();
        WordExportUtils wordExportUtils = new WordExportUtils();
        List<String> myList = new ArrayList<String>(Arrays.asList(text_content
                .split("\n")));
        List<TempContent> lst = new ArrayList<TempContent>();
        for (String s : myList) {
            TempContent em = new TempContent();
            em.setResonRequest(s);
            lst.add(em);
        }
        wordExportUtils.replaceTable(wmp, 1, lst);
        wordExportUtils.replacePlaceholder(wmp, map);

        // convert word to PDF
        PdfDocxFile pdfDocxFile = wordExportUtils.writePDFToStream(wmp, false);

        // write pdf to file
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
        String tempFolder = resourceBundle.getString("signTemp");
        if (!new File(tempFolder).exists()) {
            FileUtil.mkdirs(tempFolder);
        }
        
        tempPdfFilePath = tempFolder + "PheDuyetYeuCauBoSung_"
                + (new Date()).getTime() + ".pdf";
        OutputStream outputStream = new FileOutputStream(tempPdfFilePath);
        outputStream.write(pdfDocxFile.getContent());
        outputStream.flush();
        outputStream.close();
        
        return tempPdfFilePath;
    }
    
    private void updateOrCreateAddRequest(Long fileId) {
        AdditionalRequest additionalRequest;
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO
                .findAllActiveByFileId(fileId);
        
        if (lstAdd.size() > 0) {
            additionalRequest = lstAdd.get(0);
        } else {
            EvaluationRecord evaluationRecord = new EvaluationRecordDAO()
                    .findMaxByFileIdAndEvalType(fileId, evalType);
            String fromContent = evaluationRecord.getFormContent();
            EvaluationModel evaluationModel = new Gson().fromJson(fromContent,
                    EvaluationModel.class);
            
            additionalRequest = new AdditionalRequest();
            additionalRequest.setFileId(fileId);
            additionalRequest.setIsActive(new Long(1));
            additionalRequest.setCreatorId(this.getUserId());
            additionalRequest.setCreatorName(this.getUserFullName());
            additionalRequest.setCreateDate(new Date());
            additionalRequest.setContent(evaluationModel.getContent());
            
        }
        
        additionalRequestDAO.saveOrUpdate(additionalRequest);
        
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Long permitId) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(permitId,
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param documentId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long documentId, Long bookId) {
        
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(documentId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);

        // Cap nhat so hien tai trong bang book
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
        
        return bookDocument;
    }
    
    private String getPermitReceiveNo(Long bookNumber) {
        String permitReceiveNo = "";
        
        if (bookNumber != null) {
            permitReceiveNo = String.valueOf(bookNumber);
        }
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy");
        String year = simpleDateFormat.format(Calendar.getInstance().getTime());
        permitReceiveNo += "/"+ year + "/ATTP";
        
        return permitReceiveNo;
    }
    
    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        String certSerial = x509Cert.getSerialNumber().toString(16);
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caUserList = ca
                .findCaBySerial(certSerial, 1L, getUserId());
        if (caUserList != null && caUserList.size() > 0) {
            SignPdfFile pdfSig = new SignPdfFile();
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String linkImageSign = folderPath + separator
                    + caUserList.get(0).getSignature();
            String linkImageStamp = folderPath + separator
                    + caUserList.get(0).getStamper();
            Pdf pdf = new Pdf();
            
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String outputFileFinal = resourceBundle.getString("signTemp") + "_"
                    + (new Date()).getTime() + ".pdf";
            // chen chu ki
            pdf.insertImageAll(fileToSign, outputFileFinal, linkImageSign,
                    linkImageStamp, null);
            if (pdf.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }
            String base64Hash = pdfSig.createHash(outputFileFinal,
                    new Certificate[]{x509Cert});
            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64HASH.setValue(base64Hash);
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");
            
        } else {
            showNotification("Ký số không thành công!");
        }
    }

    // load all of singed pdf file
    private void fillFinalFileListbox(Long fileId) {
        // get addRequest lastest
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO
                .findAllActiveByFileId(fileId);
        // get file attach
        if (lstAdd != null && lstAdd.size() > 0) {
            AdditionalRequest additionalRequest = lstAdd.get(0);
            AttachDAOHE rDaoHe = new AttachDAOHE();
            List<Attachs> lstAttach = rDaoHe
                    .getByObjectIdAndAttachCatAndAttachType(
                            additionalRequest.getAdditionalRequestId(),
                            Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                            Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
            this.finalFileListbox.setModel(new ListModelArray(lstAttach));
        }else{
        	this.finalFileListbox.setModel(new ListModelArray(new ArrayList<Attachs>()));
        }
        
    }
    
    public void updateAttachFileSigned() {
        // get addRequest lastest
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO
                .findAllActiveByFileId(fileId);
        // get file attach
        if (lstAdd != null && lstAdd.size() > 0) {
            AdditionalRequest additionalRequest = lstAdd.get(0);
            AttachDAOHE rDaoHe = new AttachDAOHE();
            List<Attachs> lstAttach = rDaoHe
                    .getByObjectIdAndAttachCatAndAttachType(
                            additionalRequest.getAdditionalRequestId(),
                            Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                            Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
            for (Attachs att : lstAttach) {
                Attachs att_new = att;
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att_new);
                }
            }
        }
        
    }

    // download signed pdf
    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs attachs = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(attachs);
    }

    // delete signed pdf
    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }
    
    private void setWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }
    
}
