/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import java.util.Map;

import com.google.gson.Gson;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.utils.Constants;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;


/**
 *
 * @author duv
 */
public class IO_27_8_CV_GTBCKQKTController extends BusinessController {
    private static final long serialVersionUID = 1L;
    
    @Wire
	private Textbox txtMessage, txtValidate;
    
    
    private Long fileId;
    
    
    @Override
	public ComponentInfo doBeforeCompose(Page page, Component parent,
			ComponentInfo compInfo) {
		Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
		fileId = (Long) arguments.get("fileId");
		return super.doBeforeCompose(page, parent, compInfo);
	}
    
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit(){
    	sendMS();
    	txtValidate.setValue("1");

    }
    
    private void sendMS(){
		Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_24);
        md.setPhase(0l);
        md.setFeeUpdate(false);            
        String jsonMd = gson.toJson(md);        
        txtMessage.setValue(jsonMd);
	}
    
}
