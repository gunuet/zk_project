/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.importOrder.Controller;

import com.viettel.core.workflow.BusinessController;
import com.viettel.utils.Constants;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;

/**
 *
 * @author E5420
 */
public class IO_Index_fixDept extends BusinessController{
       private static final long serialVersionUID = 1L;
    @Wire
    Textbox txtCurrentDept;
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtCurrentDept.setValue("1");

    }
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit(){
       //showNotification(String.format(Constants.Notification.SENDED_SUCCESS,"hồ sơ"), Constants.Notification.INFO);
    }
}
