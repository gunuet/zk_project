package com.viettel.module.importOrder.Controller;

import com.viettel.voffice.BO.Files;
import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.Model.VFileImportOrderModel;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.voffice.model.SearchModel;
import com.viettel.voffice.model.exporter.ExcelExporter;
import com.viettel.voffice.model.exporter.Interceptor;
import com.viettel.voffice.model.exporter.RowRenderer;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.bind.JAXBException;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.zkoss.poi.ss.usermodel.Cell;
import org.zkoss.poi.ss.usermodel.CellStyle;
import org.zkoss.poi.ss.usermodel.IndexedColors;
import org.zkoss.poi.ss.usermodel.Row;
import org.zkoss.poi.ss.util.CellRangeAddress;
import org.zkoss.poi.xssf.usermodel.XSSFFont;
import org.zkoss.poi.xssf.usermodel.XSSFSheet;
import org.zkoss.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author
 */
public class ImPortOrderAllController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #dbFromDayModify")
    private Datebox dbFromDayModify;
    @Wire("#incSearchFullForm #dbToDayModify")
    private Datebox dbToDayModify;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #lboxStatus")
    private Listbox lboxStatus;
    @Wire("#incSearchFullForm #tbCosmeticBrand")
    private Textbox tbCosmeticBrand;
    @Wire("#incSearchFullForm #tbCosmeticProductName")
    private Textbox tbCosmeticProductName;
    @Wire("#incSearchFullForm #tbCosmeticCompanyName")
    private Textbox tbCosmeticCompanyName;
    @Wire("#incSearchFullForm #tbCosmeticCompanyAddress")
    private Textbox tbCosmeticCompanyAddress;
    @Wire("#incSearchFullForm #tbCosmeticDistributePersonName")
    private Textbox tbCosmeticDistributePersonName;
    @Wire("#incSearchFullForm #tbBusinessTaxCode")
    private Textbox tbBusinessTaxCode;
    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    private Window importOrderAll;
    private CosmeticFileModel lastSearchModel;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;
    Long UserId = getUserId();
    CategoryDAOHE dao = new CategoryDAOHE();
    // Long RoleId = Long.parseLong(dao.LayTruong("RoleUserDept", "userId",
    // UserId.toString(), "roleId"));
    private Users user;
    private String sPositionType;
    private Constants constants = new Constants();
    String sBoSung = Constants.PROCESS_STATUS.SENT_RETURN.toString();
    String statusStr;
    // list cac checkbox đã check trong list
    List<VFileImportOrder> itemsCheckedList, lstCosmetic;
    // list cac checkbox đã check trong 1 trang
    List<Listitem> itemsChecksedPage;
    private SearchModel sm; // luong
    private VFileImportOrder vFileCosfile;
    private Files files;
    String menuTypeStr;
    private List<Category> lstStatusAll = new ArrayList<>();
    private String danhsachhosothucpham = "/WEB-INF/template/danhsachhosothucpham.docx";

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            itemsCheckedList = new ArrayList<VFileImportOrder>();
            lastSearchModel = new CosmeticFileModel();
            Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
            FILE_TYPE_STR = (String) arguments.get("filetype");
            String deptIdStr = (String) arguments.get("deptid");
            menuTypeStr = (String) arguments.get("menuType");
            sPositionType = (String) arguments.get("PositionType");
            statusStr = (String) arguments.get("status");
            Long statusL = null;
            try {
                if (statusStr != null) {
                    statusL = Long.parseLong(statusStr);
                }
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
            if (statusL != null) {
                lastSearchModel.setStatus(statusL);
            }

            lastSearchModel.setMenuTypeStr(menuTypeStr);
            try {
                DEPT_ID = Long.parseLong(deptIdStr);
                DEPT_ID = 3300L;
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
                DEPT_ID = -1l;
            }
            FILE_TYPE = WorkflowAPI.getInstance().getProcedureTypeIdByCode(
                    FILE_TYPE_STR);

            Long userId = getUserId();
            UserDAOHE userDAO = new UserDAOHE();
            user = userDAO.findById(userId);
            CategoryDAOHE cdhe = new CategoryDAOHE();
            ImportOrderFileDAO objDAOHE = new ImportOrderFileDAO();
            // List lstStatusAll =
            // cdhe.getSelectCategoryByType(Constants.CATEGORY_TYPE.FILE_STATUS,
            // "name");
            List lstStatus;
            if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
                lstStatus = objDAOHE.findListStatusByCreatorId(getUserId());
                dbFromDay.setValue(null);
                lastSearchModel.setCreateDateFrom(null);
                dbToDay.setValue(new Date());
                lastSearchModel.setCreateDateTo(new Date());
            } else {
                lstStatus = objDAOHE.findListStatusByReceiverAndDeptId(
                        lastSearchModel, getUserId(), getDeptId());
                dbFromDayModify.setValue(null);
                lastSearchModel.setModifyDateFrom(null);
                dbToDayModify.setValue(new Date());
                lastSearchModel.setModifyDateTo(new Date());
            }
            if (lstStatus != null && lstStatus.size() > 0) {
                lstStatusAll = cdhe.findStatusByVal(lstStatus.toArray());
                ListModelArray lstModelProcedure = new ListModelArray(lstStatusAll);
                lboxStatus.setModel(lstModelProcedure);

                // set multi check
                onSearch();
            }
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    private static void bubbleSort(List unsortedArray, int length)
            throws Exception {
        int counter, index;
        Object temp;
        String value, value1;
        Object obj;
        Object obj1;
        for (counter = 0; counter < length - 1; counter++) { // Loop once for
            // each element
            // in the array.
            for (index = 1; index < length - 1 - counter; index++) { // Once for
                // each
                // element,
                // minus
                // the
                // counter.
                Field field = unsortedArray.get(counter).getClass()
                        .getDeclaredField("name");
                field.setAccessible(true);
                obj = unsortedArray.get(index);
                obj1 = unsortedArray.get(index + 1);
                value = String.valueOf(field.get(obj));
                value1 = String.valueOf(field.get(obj1));
                if (value.compareTo(value1) > 0) { // Test if need a swap or
                    // not.
                    temp = obj; // These three lines just swap the two elements:
                    unsortedArray.set(index, obj1);
                    unsortedArray.set(index + 1, temp);
                }
            }
        }
    }

    private List<Long> convert(String str) {
        List<Long> lstLong = new ArrayList<>();
        if (str != null) {
            String[] statusLst = str.split(";");
            for (String statusStr : statusLst) {
                try {
                    if (statusStr != null) {
                        Long statusL = Long.parseLong(statusStr);
                        lstLong.add(statusL);
                    }

                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }

            }
        }
        return lstLong;
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {

            userPagingBottom.setActivePage(0);
            userPagingTop.setActivePage(0);
            // validate search
            // neu an nut search reset lại itemsCheckedList
            itemsCheckedList = new ArrayList<VFileImportOrder>();
            if (dbFromDay != null && dbToDay != null
                    && dbFromDay.getValue() != null && dbToDay.getValue() != null) {
                if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                    showNotification(
                            "Thời gian từ ngày không được lớn hơn đến ngày",
                            Constants.Notification.WARNING, 2000);
                    return;
                }
            }
            if (dbFromDayModify != null && dbToDayModify != null
                    && dbFromDayModify.getValue() != null
                    && dbToDayModify.getValue() != null) {
                if (dbFromDayModify.getValue().compareTo(dbToDayModify.getValue()) > 0) {
                    showNotification(
                            "Thời gian từ ngày không được lớn hơn đến ngày",
                            Constants.Notification.WARNING, 2000);
                    return;
                }
            }

            lastSearchModel.setCreateDateFrom(dbFromDay == null ? null : dbFromDay
                    .getValue());
            lastSearchModel.setCreateDateTo(dbToDay == null ? null : dbToDay
                    .getValue());
            lastSearchModel.setModifyDateFrom(dbFromDayModify == null ? null
                    : dbFromDayModify.getValue());
            lastSearchModel.setModifyDateTo(dbToDayModify == null ? null
                    : dbToDayModify.getValue());
            lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
            lastSearchModel.setBrandName(tbCosmeticBrand == null ? null
                    : tbCosmeticBrand.getValue());
            lastSearchModel.setProductName(tbCosmeticProductName == null ? null
                    : tbCosmeticProductName.getValue());
            lastSearchModel.setBusinessName(tbCosmeticCompanyName == null ? null
                    : tbCosmeticCompanyName.getValue());
            lastSearchModel
                    .setBusinessAddress(tbCosmeticCompanyAddress == null ? null
                            : tbCosmeticCompanyAddress.getValue());
            lastSearchModel
                    .setDistributePersonName(tbCosmeticDistributePersonName == null ? null
                            : tbCosmeticDistributePersonName.getValue());
            lastSearchModel.setTaxCode(tbBusinessTaxCode == null ? null
                    : tbBusinessTaxCode.getValue());
            if (lboxStatus.getSelectedItem() != null) {
                try {
                    Object value = lboxStatus.getSelectedItem().getValue();
                    if (value != null && value instanceof String) {
                        Long valueL = Long.valueOf((String) value);
                        if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                            lastSearchModel.setStatus(valueL);
                        }
                    } else {
                        lastSearchModel.setStatus(null);
                    }
                } catch (NumberFormatException ex) {
                    LogUtils.addLogDB(ex);
                }

            }
            if (statusStr != null) {
                if (statusStr.equals(sBoSung)) {
                    lastSearchModel.setStatus(Long.parseLong(sBoSung));
                }
            }
            // if (lboxDocumentTypeCode.getSelectedItem() != null) {
            // String value = lboxDocumentTypeCode.getSelectedItem().getValue();
            // if (value != null) {
            // Long valueL = Long.valueOf(value);
            // if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
            // lastSearchModel.setDocumentTypeCode(valueL);
            // }
            // }
            // }
            {
                userPagingBottom.setActivePage(0);
                userPagingTop.setActivePage(0);
            }
            reloadModel(lastSearchModel);

        } catch (WrongValueException | NumberFormatException ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    /*
     * Xuat danh sach ho so linhdx 03/04/2016
     */
    @Listen("onClick = #incSearchFullForm #btnExportDS")
    public void onExportDS() throws Docx4JException, FileNotFoundException,
            IOException, JAXBException {
        ImportOrderFileDAO objDAOHE = new ImportOrderFileDAO();
        PagingListModel plm = objDAOHE
                .findFilesByReceiverAndDeptId(lastSearchModel, getUserId(),
                        getDeptId(), 0, Integer.MAX_VALUE);
        List<VFileImportOrder> lst = plm.getLstReturn();
        List<VFileImportOrderModel> lstModel = new ArrayList();
        if (lst != null && lst.size() > 0) {
            for (int i = 0; i < lst.size(); i++) {
                VFileImportOrder obj = lst.get(i);
                String sendDate = getSendDate(obj);
                String status = getStatus(obj.getStatus());
                VFileImportOrderModel model = new VFileImportOrderModel();
                model.setvFileImportOrder(lst.get(i));
                model.setSendDate(sendDate);
                model.setStatusStr(status);
                model.setRowNum(i);
                lstModel.add(model);
            }
        }
        final List data = lstModel;

        final ExcelExporter exporter = new ExcelExporter(9);
        exporter.setDataSheetName("Sheet1");
        exporter.setInterceptor(new Interceptor<XSSFWorkbook>() {
            @Override
            public void beforeRendering(XSSFWorkbook book) {
                ExcelExporter.ExportContext context = exporter
                        .getExportContext();
                XSSFSheet sheet = context.getSheet();

                // Font tiêu đề
                XSSFFont fontTitle = book.createFont();
                fontTitle.setFontHeightInPoints((short) 15);
                fontTitle.setFontName("Times New Roman");
                fontTitle.setColor(IndexedColors.BLACK.getIndex());
                fontTitle.setBold(true);
                fontTitle.setItalic(false);

                Row row = exporter.getOrCreateRow(context.moveToNextRow(),
                        sheet);
                Cell cell = exporter.getOrCreateCell(context.moveToNextCell(),
                        sheet);
                cell.setCellValue("Danh sách hồ sơ Xác nhận đạt yêu cầu nhập khẩu");
                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontTitle);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 8));
                // Font cho thời gian
                XSSFFont fontTime = book.createFont();
                fontTime.setFontHeightInPoints((short) 13);
                fontTime.setFontName("Times New Roman");
                fontTime.setColor(IndexedColors.BLACK.getIndex());
                fontTime.setBold(false);
                fontTime.setItalic(true);

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                row = exporter.getOrCreateRow(context.moveToNextRow(), sheet);
                cell = exporter.getOrCreateCell(context.moveToNextCell(), sheet);
                String value = "Thời gian từ ngày %s đến ngày %s";
                String fromDay = "";
                if (dbFromDayModify.getValue() != null) {
                    fromDay = format.format(dbFromDayModify.getValue());
                }
                String toDay = "";
                if (dbToDayModify.getValue() != null) {
                    toDay = format.format(dbToDayModify.getValue());
                }
                cell.setCellValue(String.format(value, fromDay, toDay));
                cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontTime);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 8));

                context.setRowHeaderIndex(2);

                // Row heading
                exporter.getOrCreateRow(context.moveToNextRow(), sheet);
                // Font cho header
                XSSFFont fontHeader = book.createFont();
                fontHeader.setFontHeightInPoints((short) 12);
                fontHeader.setFontName("Times New Roman");
                fontHeader.setColor(IndexedColors.BLACK.getIndex());
                fontHeader.setBold(true);
                fontHeader.setItalic(false);
                // Font cho nội dung
                XSSFFont fontContent = book.createFont();
                fontContent.setFontHeightInPoints((short) 11);
                fontContent.setFontName("Times New Roman");
                fontContent.setColor(IndexedColors.BLACK.getIndex());
                fontContent.setBold(false);
                fontContent.setItalic(false);
                // CellStyle cho headers
                cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontHeader);

                String[] headerNames = {"STT", "Mã HS", "Tên Doanh nghiệp",
                    "Số vận đơn", "Số hợp đồng", "Số hóa đơn", "Ngày đến",
                    "Đơn vị xuất khẩu", "Trạng thái"};
                for (int i = 0; i < headerNames.length; i++) {
                    cell = exporter.getOrCreateCell(context.moveToNextCell(),
                            context.getSheet());
                    cell.setCellValue(headerNames[i]);
                    cell.setCellStyle(cellStyle);
                }
                // Set CellStyle cho các cột
                CellStyle csCenter = sheet.getWorkbook().createCellStyle();
                csCenter.setAlignment(CellStyle.ALIGN_CENTER);
                csCenter.setFont(fontContent);
                exporter.setCellstyle(0, csCenter);
                exporter.setCellstyle(1, csCenter);
                CellStyle csLeft = sheet.getWorkbook().createCellStyle();
                csLeft.setAlignment(CellStyle.ALIGN_LEFT);
                csLeft.setFont(fontContent);
                exporter.setCellstyle(2, csLeft);
                exporter.setCellstyle(3, csLeft);
                exporter.setCellstyle(4, csLeft);
                exporter.setCellstyle(5, csLeft);
                exporter.setCellstyle(6, csLeft);
                exporter.setCellstyle(7, csLeft);
                exporter.setCellstyle(8, csLeft);
            }

            @Override
            public void afterRendering(XSSFWorkbook book) {
                ExcelExporter.ExportContext context = exporter
                        .getExportContext();
                XSSFSheet sheet = context.getSheet();

                XSSFFont fontFooter = book.createFont();
                fontFooter.setFontHeightInPoints((short) 11);
                fontFooter.setFontName("Times New Roman");
                fontFooter.setColor(IndexedColors.BLACK.getIndex());
                fontFooter.setBold(true);
                fontFooter.setItalic(false);

                Row row = exporter.getOrCreateRow(context.moveToNextRow(),
                        sheet);
                Cell cell = exporter.getOrCreateCell(context.moveToNextCell(),
                        sheet);
                cell.setCellValue("Có tổng số: " + data.size() + " hồ sơ");
                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
                cellStyle.setFont(fontFooter);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 8));
            }
        });

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            exporter.export(9, data,
                    new RowRenderer<Row, VFileImportOrderModel>() {

                @Override
                public void render(Row row,
                        VFileImportOrderModel vFileImportOrderModel,
                        boolean oddRow) {
                    final ExcelExporter.ExportContext context = exporter
                            .getExportContext();
                    final XSSFSheet sheet = context.getSheet();

                    Cell cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(row.getRowNum()
                            - context.getRowHeaderIndex());
                    cell.setCellStyle(exporter.getCellstyle(0));

                    cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(vFileImportOrderModel
                            .getvFileImportOrder().getFileCode());
                    cell.setCellStyle(exporter.getCellstyle(1));

                    cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(vFileImportOrderModel
                            .getvFileImportOrder().getBusinessName());
                    cell.setCellStyle(exporter.getCellstyle(2));

                    cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(vFileImportOrderModel
                            .getvFileImportOrder().getTransNo());
                    cell.setCellStyle(exporter.getCellstyle(3));

                    cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(vFileImportOrderModel
                            .getvFileImportOrder().getContractNo());
                    cell.setCellStyle(exporter.getCellstyle(4));

                    cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(vFileImportOrderModel
                            .getvFileImportOrder().getBillNo());
                    cell.setCellStyle(exporter.getCellstyle(5));

                    cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(vFileImportOrderModel
                            .getSendDate());
                    cell.setCellStyle(exporter.getCellstyle(6));

                    cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(vFileImportOrderModel
                            .getvFileImportOrder().getExporterName());
                    cell.setCellStyle(exporter.getCellstyle(7));

                    cell = exporter.getOrCreateCell(
                            context.moveToNextCell(), sheet);
                    cell.setCellValue(vFileImportOrderModel
                            .getStatusStr());
                    cell.setCellStyle(exporter.getCellstyle(8));
                }

            }, out);
            Calendar now = Calendar.getInstance();
            int day = now.get(Calendar.DAY_OF_MONTH);
            int month = now.get(Calendar.MONTH);
            int year = now.get(Calendar.YEAR);
            String name = "Bao cao-" + day + "-" + (month + 1) + "-" + year
                    + ".xlsx";
            AMedia amedia = new AMedia(name, "xls", "application/file",
                    out.toByteArray());
            Filedownload.save(amedia);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }

    }

    private void reloadModel(SearchModel searchModel) {
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage()
                * userPagingBottom.getPageSize();
        PagingListModel plm;

        ImportOrderFileDAO objDAOHE = new ImportOrderFileDAO();
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(),
                    start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel,
                    getUserId(), getDeptId(), start, take);
            sm = searchModel;
        }

        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstCosmetic = plm.getLstReturn();
        lstModel.setMultiple(true);

        // itemsCheckedList set selected
        lbList.setModel(lstModel);
        // tichnv=====comment 20/05/2015==============
        // lbList.renderAll();
        // List<Listitem> listItem = lbList.getItems();
        // Set<Listitem> itemselected = new HashSet();
        // for (Listitem itemlb : listItem) {
        // for (int i = 0; i < itemsCheckedList.size(); i++) {
        // if (itemsCheckedList.get(i).getFileId().equals(((VFileImportOrder)
        // itemlb.getValue()).getFileId())) {
        // itemselected.add(itemlb);
        // break;
        // }
        // }
        // }
        // lbList.setSelectedItems(itemselected);================//end

    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    public String getSendDate(VFileImportOrder f) {
        SimpleDateFormat formatterDateTime = new SimpleDateFormat("dd-MM-yyyy");
        String rs = f.getStartDate() == null ? formatterDateTime.format(f
                .getCreateDate()) : formatterDateTime.format(f.getStartDate());
        if (sm != null) {
            ProcessDAOHE pDAO = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process p = pDAO.getLastByUserId(
                    f.getFileId(), UserId);
            if (p != null) {
                rs = formatterDateTime.format(p.getSendDate());
            }
        }
        return rs;
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #importOrderAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch=#importOrderAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            // Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    // Show popup khi an nut thao tac
    @Listen("onShowActionMulti=#importOrderAll")
    public void onShowActionMulti() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("lstCosmetic", itemsCheckedList);
        Window window = createWindow("MultiProcess",
                "/Pages/module/cosmetic/viewMultiProcess.zul", arguments,
                Window.MODAL);
        window.setMode(Window.Mode.MODAL);
    }

    @Listen("onChangeTime=#importOrderAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            if (dbFromDay != null) {
                dbFromDay.setValue(model.getFromDate());
            }
            if (dbToDay != null) {
                dbToDay.setValue(model.getToDate());
            }
            if (dbFromDayModify != null) {
                dbFromDayModify.setValue(model.getFromDate());
            }
            if (dbToDayModify != null) {
                dbToDayModify.setValue(model.getToDate());
            }

            onSearch();
        }
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", importOrderAll);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("procedureId", FILE_TYPE);

        arguments.put("deptId", DEPT_ID);
        return arguments;

    }

    @Listen("onOpenCreate=#importOrderAll")
    public void onOpenCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("CRUDMode", "CREATE");
        setParam(arguments);
        Window window = createWindow("windowCRUD",
                "/Pages/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
        window.setMode(Window.Mode.EMBEDDED);
        // importOrderAll.setVisible(false);
    }

    @Listen("onSelect =#incList #lbList")
    public void onSelect(Event event) {
        Listbox lb = (Listbox) event.getTarget();
        List<Listitem> li = lb.getItems();
        Set<Listitem> liselect = lb.getSelectedItems();
        // loai bo nhưng item trong lb co trong itemsCheckedList
        for (Listitem itemlb : li) {
            for (VFileImportOrder items : itemsCheckedList) {
                if (((VFileImportOrder) itemlb.getValue()).getFileId().equals(
                        items.getFileId())) {
                    itemsCheckedList.remove(items);
                    break;
                }
            }
        }
        // add item check vao itemsCheckedList
        for (Listitem item : liselect) {
            itemsCheckedList.add((VFileImportOrder) item.getValue());
        }
    }

    @Listen("onOpenView =#incList #lbList")
    public void onOpenView(Event event) {
        VFileImportOrder obj = (VFileImportOrder) event.getData();
        createWindowView(obj.getFileId(), Constants.DOCUMENT_MENU.ALL,
                importOrderAll);
        importOrderAll.setVisible(false);

    }

    @Listen("onRefresh =#importOrderAll")
    public void onRefresh(Event event) {
        onOpenView(event);
    }

    @Listen("onSelectedProcess = #importOrderAll")
    public void onSelectedProcess(Event event) {
        importOrderAll.setVisible(false);

    }

    private void createWindowView(Long id, int menuType, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView",
                "/Pages/module/importorder/importOrderView.zul", arguments,
                Window.EMBEDDED);
    }

    @Listen("onSave = #importOrderAll")
    public void onSave(Event event) {
        importOrderAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi
     * windowDocIn len Con cac su kien save, update thi da xu li hien thi
     * windowDocIn trong phuong thuc onSave
     */
    @Listen("onVisible =  #importOrderAll")
    public void onVisible() {
        reloadModel(lastSearchModel);
        importOrderAll.setVisible(true);
    }

    @Listen("onOpenUpdate = #incList #lbList")
    public void onOpenUpdate(Event event) {
        VFileImportOrder obj = (VFileImportOrder) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowCRUDCosmetic",
                "/Pages/module/importorder/ycnkCrud.zul", arguments,
                Window.EMBEDDED);
        importOrderAll.setVisible(false);
    }

    @Listen("onDownLoadGiayPhep = #incList #lbList")
    public void onDownLoadGiayPhep(Event event) throws FileNotFoundException,
            IOException {
        VFileImportOrder obj = (VFileImportOrder) event.getData();
        Long fileid = obj.getFileId();
        ImportOrderFileDAO cosDao = new ImportOrderFileDAO();
        CategoryDAOHE catDaohe = new CategoryDAOHE();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        // co giay phep
        if (check == 1) {
            AttachDAOHE attachDaoHe = new AttachDAOHE();
            Long cospermit = Long.parseLong(catDaohe.LayTruongPermitId(fileid));

            Attachs attachs = attachDaoHe.getByObjectIdAndAttachCat(cospermit,
                    Constants.OBJECT_TYPE.COSMETIC_PERMIT);
            // Nếu có file
            if (attachs != null) {
                // attDAO.downloadFileAttach(attachs);

                String path = attachs.getFullPathFile();
                File f = new File(path);
                // neu ton tai
                if (f.exists()) {
                    File tempFile = FileUtil.createTempFile(f,
                            attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                } // Khong se tao file
                else {
                    String folderPath = ResourceBundleUtil
                            .getString("dir_upload");
                    FileUtil.mkdirs(folderPath);
                    String separator = ResourceBundleUtil
                            .getString("separator");
                    folderPath += separator
                            + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
                    File fd = new File(folderPath);
                    if (!fd.exists()) {
                        fd.mkdirs();
                    }
                    f = new File(attachs.getFullPathFile());
                    if (f.exists()) {
                    } else {
                        f.createNewFile();
                    }
                    File tempFile = FileUtil.createTempFile(f,
                            attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                }
            } // chua có file sẽ tạo trên hệ thông
            else {
                // showNotification("File không còn tồn tại trên hệ thống!");
            }
        }
        // se bo sung truong hop sua goi bo sung
        // Kiem tra bang permit
    }

    @Listen("onViewFlow = #lbList")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    @Listen("onDelete = #incList #lbList")
    public void onDelete(Event event) {
        final VFileImportOrder obj = (VFileImportOrder) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

            @Override
            public void onEvent(Event event) {
                if (null != event.getName()) {
                    switch (event.getName()) {
                        case Messagebox.ON_OK:
                            // OK is clicked
                            try {
                                if (isAbleToDelete(obj)) {
                                    FilesDAOHE objDAOHE = new FilesDAOHE();
                                    objDAOHE.delete(obj.getFileId());
                                    onSearch();
                                    showNotification(
                                            String.format(
                                                    Constants.Notification.DELETE_SUCCESS,
                                                    Constants.DOCUMENT_TYPE_NAME.FILE),
                                            Constants.Notification.INFO);
                                } else {
                                    showNotification("Bạn không có quyền xóa");
                                }

                            } catch (Exception ex) {
                                showNotification(
                                        String.format(
                                                Constants.Notification.DELETE_ERROR,
                                                Constants.DOCUMENT_TYPE_NAME.FILE),
                                        Constants.Notification.ERROR);
                                LogUtils.addLogDB(ex);
                            } finally {
                            }
                            break;
                        case Messagebox.ON_NO:
                            break;
                    }
                }
            }
        });
    }

    @Listen("onClick=#incSearchFullForm #btnReset")
    public void doResetForm() {
        tbCosmeticBrand.setValue("");
        tbCosmeticCompanyAddress.setValue("");
        tbCosmeticCompanyName.setValue("");
        tbCosmeticProductName.setValue("");
        tbCosmeticDistributePersonName.setValue("");
        tbNSWFileCode.setValue("");
        if (tbBusinessTaxCode != null) {
            tbBusinessTaxCode.setValue("");
        }
        if (dbFromDay != null) {
            dbFromDay.setValue(null);
        }
        if (dbToDay != null) {
            dbToDay.setValue(new Date());
        }
        if (dbFromDayModify != null) {
            dbFromDayModify.setValue(null);
        }
        if (dbToDayModify != null) {
            dbToDayModify.setValue(new Date());
        }
        lboxStatus.setSelectedIndex(0);

        onSearch();
    }

    // Thêm chức năng copy
    @Listen("onCopy = #incList #lbList")
    public void onCopy(Event event) {
        VFileImportOrder obj = (VFileImportOrder) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "COPY");
        setParam(arguments);
        createWindow("windowCRUDCosmetic",
                "/Pages/module/importorder/ycnkCrud.zul", arguments,
                Window.EMBEDDED);
        importOrderAll.setVisible(false);

    }

    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(VFileImportOrder obj) {
        if (user.getUserId() != null
                && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(),
                    Constants.PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.VT_RETURN))) {
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean isAbleToModify(VFileImportOrder obj) {

        if (user.getUserId() != null
                && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(),
                    Constants.PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.SENT_DISPATCH))
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.SENT_RETURN))
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.VT_RETURN))
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.SENT_SDBS1))
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.SENT_SDBS2))) {
                return true;
            }
            return false;
        }
        return false;
    }

    public String getStatus(Long status) {
        for (Category c : lstStatusAll) {
            if (status.toString().equals(c.getValue())) {
                return c.getName();
            }
        }
        return WorkflowAPI.getStatusName(status);
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public int CheckGiayPhep(Long fileid) {
        ImportOrderFileDAO cosDao = new ImportOrderFileDAO();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        return check;
    }

    public int checkDispathSDBS(Long fileid) {
        ImportOrderFileDAO cosDao = new ImportOrderFileDAO();
        int check = cosDao.checkDispathSDBS(fileid, getUserId());
        return check;
    }

    // public boolean CheckView(String name) {
    // if (name.equals("Edit")) {
    // if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDC)) {
    // return false;
    // } else if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDP)) {
    // return false;
    // } else {
    // return true;
    // }
    // }
    // if (name.equals("Delete")) {
    // if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDC)) {
    // return false;
    // }
    // return true;
    // }
    // return true;
    // }
    @Listen("onViewSDBS = #incList #lbList")
    public void onViewSDBS(Event event) throws FileNotFoundException,
            IOException {
        VFileImportOrder obj = (VFileImportOrder) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowViewSDBS",
                "/Pages/module/importreq/flow/viewDispatch.zul", arguments,
                Window.MODAL);
    }

    public Constants getConstants() {
        return constants;
    }

    public void setConstants(Constants constants) {
        this.constants = constants;
    }

    public Long getOldDeadlineWarning(VFileImportOrder f) {
        if (f.getFinishDate() != null) {
            return Constants.WARNING.DEADLINE_ON;
        }
        Long value = Constants.WARNING.DEADLINE_ON;

        ProcessDAOHE pDAO = new ProcessDAOHE();
        com.viettel.core.workflow.BO.Process p = pDAO.getLastByUserId(
                f.getFileId(), UserId);
        if (p != null) {
            Date sendDate = p.getSendDate();

            Date exceptedDate = new Date();

            if (sendDate != null
                    && menuTypeStr.equals(Constants.MENU_TYPE.WAITPROCESS_STR)) {
                SimpleDateFormat formatterDateTime = new SimpleDateFormat(
                        "yyyyMMdd");
                if (Long.valueOf(formatterDateTime.format(exceptedDate)) > Long
                        .valueOf(formatterDateTime.format(sendDate))) {
                    value = Constants.WARNING.DEADLINE_MISS;
                }

            }
        }
        return value;
    }

    public int CheckHaveQrCode(Long fileid) {
        if (!"admin".equals(getUserName())) {
            return Constants.CHECK_VIEW.NOT_ADMIN;
        }
        FilesDAOHE cosDao = new FilesDAOHE();
        int check = cosDao.checkHaveQrCode(fileid);
        return check;
    }
    //upload qrcode

    @Listen("onUploadQrCode =#incList #lbList")
    public void onUploadQrCode(Event event) {
        VFileImportOrder obj = (VFileImportOrder) event.getData();
//        obj.getFileId()
        PermitDAO permitdao = new PermitDAO();
        Permit permitbo = permitdao.findPermitByFileId(obj.getFileId());
        if (permitbo != null && permitbo.getFileId() != null) {
            QrCodeController qrcodec = new QrCodeController();
            int result = qrcodec.createQrCodeOnListFiles(obj.getFileId());
            if (result == 1) {
                showNotification(getLabelCos("qrcode_createSuccess"), Constants.Notification.INFO);
                reloadModel(lastSearchModel);
            } else {
                showNotification(getLabelCos("qrcode_createunsuccess"), Constants.Notification.ERROR);
            }
        } else {
            showNotification(getLabelCos("qrcode_createunsuccess"), Constants.Notification.ERROR);
        }

    }

    @Listen("onDownloadQrCode =#incList #lbList")
    public void onDownloadQrCode(Event event) {
        VFileImportOrder obj = (VFileImportOrder) event.getData();
        QrCodeController qrcodeconsole = new QrCodeController();
        qrcodeconsole.onDownloadQrCodeByFileId(obj.getFileId());
    }
}
