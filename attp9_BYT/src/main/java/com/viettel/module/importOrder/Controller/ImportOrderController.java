/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.utils.Constants;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosAdditionalRequest;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.BO.CosPermit;
import com.viettel.module.cosmetic.Controller.CosmeticBaseController;
import com.viettel.module.cosmetic.DAO.CosAdditionalRequestDAO;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.cosmetic.DAO.CosPermitDAO;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VAttfileCategory;
import com.viettel.module.importOrder.DAO.ImportOrderAttachDao;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VAttfileCategoryDAO;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.ValidatorUtil;
import com.viettel.voffice.BO.Business;
import com.viettel.voffice.BO.VFeeProcedure;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.BusinessDAOHE;
import com.viettel.voffice.DAOHE.VFeeProcedureDAOHE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class ImportOrderController extends CosmeticBaseController {

    private CosAdditionalRequest additionalRequest;

    private CosEvaluationRecord evaluationRecord = new CosEvaluationRecord();
    private final int A = 1;
    private final int SAVE_CLOSE = 2;
    private final int IMPORT_ORDER_FILE = 1;
    private long FILE_TYPE;
    private String FILE_NAME;
    private final String DATE_FORMAT = "dd-mm-yyyy";
    private final String NHAP_KHAU = "ThongTinYeuCauNhapKhau";
    private final String LO_HANG = "ThongTinChiTietLoHang";
    @Wire
    private Tabpanel mainpanel;
    @Wire
    private Tabbox tabs;
    @Wire
    Listbox lbOrderProduct;
    @Wire
    Label lbOwnerFax, lbOwnerName, lbOwnerAddress, lbOwnerPhone, lbMahoso;
    @Wire
    Label lbTopWarning, lbProductWarning, lbBottomWarning, lbImportExcelWarning, lblTaxCode, tbOwnerEmail;

    @Wire
    private Intbox boxRegisterCmt, boxStatusCode;
    @Wire
    Textbox tbProductIndex, tbPersonPhone, tbBranchName,
            tbPersonFax, tbPersonEmail, tbPersonName, tbPersonAddress, tbTransNo, tbContractNo, tbBillNo, tbExportGate, tbImportGate,
            tbExporterEmail, tbExporterName, tbExporterAddress, tbDepartment, tbCheckPlace;
    @Wire
    Textbox tbExporterFax, tbExportPhone;
    private List listImportOderFileType;
    private ImportOrderFile importOrderfiles;
    private Long originalFilesId;

    private Long originalImportFileId;
    private Long originalCosFileId;
    private Long originalFileType;
    private BookDocument bookDocument = new BookDocument();
    private CosPermit permit = new CosPermit();
    private PaymentInfo paymentInfo = new PaymentInfo();
    private Files files;
    private Long documentTypeCode;
    @Wire
    Textbox tbProductName, tbProductDescription, tbProductNationName, tbProductAnnounceNo,
            tbProductTotal, tbProductNetweight, tbProductNumber,
            tbProductBaseUnit, tbProductUnitName, tbProductManufacturer, tbPManufacturerAddress;

    @Wire
    Datebox dbCheckDate, dbComingDate, tbProductDateMauFacturer, tbProductExpriredDate;
    //Danh sach
    @Wire
    private Vlayout flist, fListImportExcel;
    @Wire
    private Listbox listDepartent, fileListbox, lbImportOrderFileType;
    private Users user;
    private Business business;
    @SuppressWarnings({"unchecked", "rawtypes"})
    private List<ImportOrderProduct> importOrderProducts = new ArrayList();

    @SuppressWarnings({"unchecked", "rawtypes"})
    private List<ImportOrderProduct> listOrderProduct = new ArrayList();
    private List<Department> listDepartment = new ArrayList<Department>();
    @Wire
    Window windowCRUDCosmetic;
    private Window parentWindow;

    @Wire
    Paging userPagingBottom;

    private List<Media> listMedia, listFileExcel;
    private String nswFileCode;
    private String crudMode;

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        // loadDataEdit();
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        crudMode = (String) arguments.get("CRUDMode");
        listMedia = new ArrayList();
        WorkflowAPI w = new WorkflowAPI();
        FILE_TYPE = w.getProcedureTypeIdByCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        // FILE_NAME = w.getProcedureNameByCode(Constants.IMPORT_ORDER_TYPE.ORDER_OBJECT);
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        Long id;
        FilesDAOHE fileDAOHE;
        ImportOrderFileDAO idpDAO;
        switch (crudMode) {
            case "CREATE":
                files = new Files();
                importOrderfiles = new ImportOrderFile();
                files.setFileCode(null);
                loadBusinessInfo();
                nswFileCode = getAutoNswFileCode(documentTypeCode);

                break;
            case "UPDATE":
                id = (Long) arguments.get("id");
                fileDAOHE = new FilesDAOHE();
                files = fileDAOHE.findById(id);
                idpDAO = new ImportOrderFileDAO();
//                ImportOrder = idpDAO.findById(id);
                ImportOrderFile impofOld = idpDAO.findByFileId(id);
                importOrderfiles = copyOrderFiles(impofOld);

                nswFileCode = importOrderfiles.getNswFileCode();
                originalFilesId = files.getFileId();
                originalFileType = files.getFileType();
                originalCosFileId = importOrderfiles.getImportOrderFileId();
                break;
            //-------Xu ly COPY-------
            case "COPY":
                nswFileCode = getAutoNswFileCode(documentTypeCode);
                id = (Long) arguments.get("id");
                fileDAOHE = new FilesDAOHE();
                Files filesOld = fileDAOHE.findById(id);
                files = copy(filesOld);
                idpDAO = new ImportOrderFileDAO();
                ImportOrderFile impdfOld = idpDAO.findByFileId(id);
                importOrderfiles = copyOrderFiles(impdfOld);
                originalFilesId = filesOld.getFileId();
                originalFileType = filesOld.getFileType();
                originalCosFileId = importOrderfiles.getImportOrderFileId();
                break;
        }

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        loadDataEdit();
        if ("UPDATE".equals(crudMode)) {

            fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE, originalFilesId);
            ImportOrderProductDAO idpDAO = new ImportOrderProductDAO();
            listOrderProduct = idpDAO.getImportOrderProductList(originalFilesId);
            ListModelArray lstModel = new ListModelArray(listOrderProduct);
            lbOrderProduct.setModel(lstModel);

        } else if ("COPY".equals(crudMode)) {
            ImportOrderProductDAO idpDAO = new ImportOrderProductDAO();
            listOrderProduct = idpDAO.getImportOrderProductList(originalFilesId);
            List<ImportOrderProduct> importOrderProducts = new ArrayList<ImportOrderProduct>();

            for (ImportOrderProduct obj : listOrderProduct) {
                ImportOrderProduct idp = new ImportOrderProduct();
                idp.setProductName(obj.getProductName());
                idp.setProductDescription(obj.getProductDescription());
                idp.setNationalName(obj.getNationalName());
                idp.setConfirmAnnounceNo(obj.getConfirmAnnounceNo());
                idp.setTotal(obj.getTotal());
                idp.setNetweight(obj.getNetweight());
                idp.setBaseUnit(obj.getBaseUnit());
                idp.setManufacTurer(obj.getManufacTurer());
                idp.setManufacturerAddress(obj.getManufacturerAddress());
                idp.setTotalUnitCode(obj.getTotalUnitCode());
                idp.setTotalUnitName(obj.getTotalUnitName());
                idp.setNetweightUnitCode(obj.getNetweightUnitCode());
                idp.setNetweightUnitName(obj.getNetweightUnitName());
                importOrderProducts.add(idp);
            }

            ListModelArray lstModel = new ListModelArray(importOrderProducts);
            lbOrderProduct.setModel(lstModel);
        }

    }

//    private void saveProduct(Long importFileId) {
//        ImportOrderProductDAO idpDAO = new ImportOrderProductDAO();
//        List<ImportOrderProduct> lstProductIdFromDB = idpDAO.findAllIdByFileId(importFileId);
////        for (ImportOrderProduct obj : lstProductIdFromDB) {
////            if (!isExistAssembler(obj.getProductId(), listOrderProduct)) {
////                idpDAO.delete(obj.getProductId());
////            }
////        }
//
//        List<Long> lstId = new ArrayList<>();
//        lbOrderProduct.getModel()
//        for (ImportOrderProduct obj : listOrderProduct) {
//        	obj.setFileId(importFileId);
//            obj.setProductId(null);
//            if ("COPY".equals(crudMode)) {
//                obj.setProductId(null);
//            }
//            idpDAO.saveOrUpdate(obj);
//            lstId.add(obj.getProductId());
//        }
//
//    }
    /*
     * Sua lai ham saveproduct
     */
    private void saveProduct(Long importFileId) {

        ListModel<ImportOrderProduct> list = lbOrderProduct.getModel();
        int size = list.getSize();
        for (int i = 0; i < size; i++) {
            ImportOrderProduct obj = list.getElementAt(i);
            obj.setFileId(importFileId);
            //        obj.setProductId(null);
//            if ("COPY".equals(crudMode)) {
//                obj.setProductId(null);
//            }
            new ImportOrderProductDAO().saveOrUpdate(obj);
        }

    }

    private Boolean isExistAssembler(Long id, List<ImportOrderProduct> lst) {
        for (ImportOrderProduct itemInList : lst) {

            if (id != null && id.equals(itemInList.getProductId())) {
                return true;
            }

        }
        return false;
    }

    private ImportOrderFile importOrderFile() {
        if ("COPY".equals(crudMode)) {
            importOrderfiles.setFileId(null);
            files.setFileCode(null);
            files.setNswFileCode(null);
            importOrderfiles.setImportOrderFileId(null);
//            if (files != null && originalFilesId != null) {
//                AttachDAOHE daoHE = new AttachDAOHE();
//                daoHE.copyAttachs(originalFilesId, files.getFileId(),Constants.OBJECT_TYPE.IMPORT_ORDER_FILE);
//            }
        }
        importOrderfiles.setFileId(files.getFileId());
        if (documentTypeCode != null) {
            importOrderfiles.setDocumentTypeCode(documentTypeCode);
            nswFileCode = getAutoNswFileCode(documentTypeCode);
            if (files.getFileCode() == null) {
                files.setFileCode(nswFileCode);
                files.setNswFileCode(nswFileCode);
            }
            importOrderfiles.setNswFileCode(nswFileCode);
            importOrderfiles.setResponsiblePersonName(tbPersonName.getText());
            importOrderfiles.setResponsiblePersonAddress(tbPersonAddress.getText());
            importOrderfiles.setResponsiblePersonPhone(tbPersonPhone.getText());
            importOrderfiles.setResponsiblePersonFax(tbPersonFax.getText());
            importOrderfiles.setResponsiblePersonEmail(tbPersonEmail.getText());
            importOrderfiles.setExporterName(tbExporterName.getText());
            importOrderfiles.setExporterAddress(tbExporterAddress.getText());
            importOrderfiles.setExporterFax(tbExporterFax.getText());
            importOrderfiles.setExporterPhone(tbExportPhone.getText());
            importOrderfiles.setExporterEmail(tbExporterEmail.getText());
            importOrderfiles.setComingDate(dbComingDate.getValue());
            importOrderfiles.setTransNo(tbTransNo.getText());
            importOrderfiles.setContractNo(tbContractNo.getText());
            importOrderfiles.setBillNo(tbBillNo.getText());
            importOrderfiles.setCheckPlace(tbCheckPlace.getText());
            importOrderfiles.setCheckTime(dbCheckDate.getValue());
            importOrderfiles.setCustomsBranchName(tbBranchName.getText());
            importOrderfiles.setImporterGateName(tbImportGate.getText());
            importOrderfiles.setExporterGateName(tbExportGate.getText());
            importOrderfiles.setDeptName(tbDepartment.getText());
        }
        return importOrderfiles;

    }

    private ImportOrderFile importOrderFileNew() {
        importOrderfiles = new ImportOrderFile();
        if ("COPY".equals(crudMode)) {
            importOrderfiles.setFileId(null);
            files.setFileCode(null);
            importOrderfiles.setImportOrderFileId(null);
            if (originalFilesId != null) {
                AttachDAOHE daoHE = new AttachDAOHE();
                daoHE.copyAttachs(originalFilesId, files.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE);
            }
        }
        importOrderfiles.setFileId(files.getFileId());
        if (documentTypeCode != null) {
            importOrderfiles.setDocumentTypeCode(documentTypeCode);
            nswFileCode = getAutoNswFileCode(documentTypeCode);
            if (files.getFileCode() == null) {
                files.setFileCode(nswFileCode);
                files.setNswFileCode(nswFileCode);
            }

            importOrderfiles.setResponsiblePersonName(tbPersonName.getText());
            importOrderfiles.setResponsiblePersonAddress(tbPersonAddress.getText());
            importOrderfiles.setResponsiblePersonPhone(tbPersonPhone.getText());
            importOrderfiles.setResponsiblePersonFax(tbPersonFax.getText());
            importOrderfiles.setResponsiblePersonEmail(tbPersonEmail.getText());
            importOrderfiles.setExporterName(tbExporterName.getText());
            importOrderfiles.setExporterAddress(tbExporterAddress.getText());
            importOrderfiles.setExporterFax(tbExporterFax.getText());
            importOrderfiles.setExporterPhone(tbExportPhone.getText());
            importOrderfiles.setExporterEmail(tbExporterEmail.getText());
            importOrderfiles.setComingDate(dbComingDate.getValue());
            importOrderfiles.setTransNo(tbTransNo.getText());
            importOrderfiles.setContractNo(tbContractNo.getText());
            importOrderfiles.setBillNo(tbBillNo.getText());
            importOrderfiles.setCheckPlace(tbCheckPlace.getText());
            importOrderfiles.setCheckTime(dbCheckDate.getValue());
            importOrderfiles.setCustomsBranchName(tbBranchName.getText());
            importOrderfiles.setImporterGateName(tbImportGate.getText());
            importOrderfiles.setExporterGateName(tbExportGate.getText());
            importOrderfiles.setDeptName(tbDepartment.getText());
        }
        return importOrderfiles;

    }

    //
    //Thanhdv Add new Product
    //
    //
    @Listen("onClick = #btnAddProduct")
    public void onAddProduct() throws Exception {

        ImportOrderProduct iop;
        if (isValidatedDataProduct()) {
            if (tbProductIndex.getValue() != null && !tbProductIndex.getValue().isEmpty() && listOrderProduct.size() > 0) {
                int index = Integer.parseInt(tbProductIndex.getValue());
                iop = listOrderProduct.get(index);
            } else {
                iop = new ImportOrderProduct();
            }
            iop.setProductName(tbProductName.getValue());
            iop.setProductDescription(tbProductDescription.getText());
            iop.setNationalName(tbProductNationName.getText());
            iop.setConfirmAnnounceNo(tbProductAnnounceNo.getText());
            iop.setTotal(Double.parseDouble(tbProductTotal.getText()));
            iop.setNetweight(Double.parseDouble(tbProductNetweight.getText()));
            iop.setBaseUnit(Long.parseLong(tbProductBaseUnit.getText()));
            if (tbProductIndex.getValue() != null && !tbProductIndex.getValue().isEmpty()) {
                //donothing
            } else {
                listOrderProduct.add(iop);

            }

            ListModelArray lstModelManufature = new ListModelArray(listOrderProduct);
            lbOrderProduct.setModel(lstModelManufature);
            lbProductWarning.setValue("");
            resetProduct();
        }

    }

    //edit product;
    @Listen("onEdit=#lbOrderProduct")
    public void onEditProduct(Event ev) throws IOException {
        ImportOrderProduct idp = (ImportOrderProduct) lbOrderProduct.getSelectedItem().getValue();
        tbProductName.setValue(idp.getProductName());
        tbProductDescription.setValue(idp.getProductDescription());
        tbProductAnnounceNo.setValue(idp.getConfirmAnnounceNo());
        // tbProductDateMauFacturer.setValue(idp.getDateOfManufacturer());
        tbProductNationName.setValue(idp.getNationalName());
        tbProductNetweight.setValue(String.valueOf(idp.getNetweight()));
        tbProductTotal.setValue(String.valueOf(idp.getTotal()));
        // tbProductExpriredDate.setValue(idp.getExpiredDate());
        tbProductIndex.setValue(String.valueOf(lbOrderProduct.getSelectedIndex()));
        tbProductBaseUnit.setValue(String.valueOf(idp.getBaseUnit()));
    }

    //Delete product
    @Listen("onDelete=#lbOrderProduct")
    public void onDeleteProduct(Event ev) throws IOException {
        ImportOrderProduct idp = (ImportOrderProduct) lbOrderProduct.getSelectedItem().getValue();
        listOrderProduct.remove(idp);
        ListModelArray lstModelAssembler = new ListModelArray(listOrderProduct);
        lbOrderProduct.setModel(lstModelAssembler);
    }

    private void resetProduct() {
        tbProductName.setText("");
        tbProductDescription.setText("");
        tbProductNetweight.setText("");
        tbProductNationName.setText("");
        tbProductIndex.setText("");
        tbProductAnnounceNo.setText("");
        tbProductBaseUnit.setText("");
        tbProductDescription.setText("");
        tbProductTotal.setText("");

    }

    private Files createFile() throws Exception {
        Date dateNow = new Date();
        Files files = new Files();
        files.setCreateDate(dateNow);
        files.setModifyDate(dateNow);
        files.setCreatorId(getUserId());
        files.setCreatorName(getUserFullName());
        files.setCreateDeptId(getDeptId());
        files.setCreateDeptName(getDeptName());
        files.setIsActive(Constants.Status.ACTIVE);
        files.setBusinessName(lbOwnerName.getValue());
        files.setBusinessAddress(lbOwnerAddress.getValue());
        files.setBusinessEmail(tbOwnerEmail.getValue());
        files.setBusinessPhone(lbOwnerPhone.getValue());
        files.setBusinessFax(lbOwnerFax.getValue());
        files.setTaxCode(getUserName());
        if (files.getStatus() == null) {
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
        }

        files.setFileType(FILE_TYPE);
        return files;
    }

    private void loadBusinessInfo() {
        Long userId = getUserId();
        BusinessDAOHE bhe = new BusinessDAOHE();
        business = bhe.findByUserId(userId);
        UserDAOHE uhe = new UserDAOHE();
        if (business != null) {

            user = uhe.getUserInBusiness(business.getBusinessId());

            files.setBusinessId(business.getBusinessId());
            files.setBusinessName(business.getBusinessName());
            files.setBusinessAddress(business.getBusinessAddress());
            files.setBusinessPhone(business.getBusinessTelephone());
            files.setBusinessFax(business.getBusinessFax());
            files.setTaxCode(business.getBusinessTaxCode());
        }
    }

    private String getAutoNswFileCode(Long documentTypeCode) {

        String autoNumber;

        ImportOrderFileDAO dao = new ImportOrderFileDAO();
        Long autoNumberL = dao.countImportfile();
        autoNumberL += 1L;//Tránh số 0 (linhdx)
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

    @SuppressWarnings({"unused", "unchecked"})
    private void onLoadDepartment() {
        listDepartment = new DepartmentDAOHE().getAllDepartment();
        ListModelArray lma = new ListModelArray(listDepartment);
        listDepartent.setModel(lma);
    }
//

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {

            case KeyEvent.F7:
                onSave(SAVE_CLOSE);
                break;

        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave(int typeSave) throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        try {
            if (!isValidatedData()) {
                return;
            }

            switch (crudMode) {
                case "CREATE": {
                    String message = String.format(Constants.Notification.SAVE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
                    Messagebox.show(message, "Xác nhận", Messagebox.YES | Messagebox.NO,
                            Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) {
                            if (null != event.getName()) {
                                switch (event.getName()) {
                                    case Messagebox.ON_YES:
                                        // OK is clicked
                                        try {

                                            createObjectCreatNew();
                                            //createPayment();
                                            showNotification(String.format(
                                                    Constants.Notification.SAVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                                                    Constants.Notification.INFO);
                                        } catch (Exception ex) {
                                            showNotification(String.format(Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                            LogUtils.addLogDB(ex);
                                        } finally {
                                        }
                                        break;
                                    case Messagebox.ON_NO:
                                        break;
                                }
                            }
                        }
                    });
                    break;
                }
                case "UPDATE":
                    String message = String.format(Constants.Notification.SAVE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
                    Messagebox.show(message, "Xác nhận", Messagebox.YES | Messagebox.NO,
                            Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) {
                            if (null != event.getName()) {
                                switch (event.getName()) {
                                    case Messagebox.ON_YES:
                                        // OK is clicked
                                        try {
//                                                    createOldVersion();
//                                                    createObject();
//                                                    createFileUpdates();
//                                                    //createPayment();

                                            uppdateImportOrder(originalFilesId);

                                            showNotification(String.format(
                                                    Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                                                    Constants.Notification.INFO);

                                        } catch (Exception ex) {
                                            showNotification(String.format(Constants.Notification.UPDATE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                            LogUtils.addLogDB(ex);
                                        } finally {
                                        }
                                        break;
                                    case Messagebox.ON_NO:
                                        break;
                                }
                            }
                        }
                    });

                    break;
                case "COPY":
                    createObject();
                    if ("COPY".equals(crudMode)) {
                        crudMode = "UPDATE";
                    }
                    //Reset lai cac gia tri moi
                    originalFilesId = files.getFileId();
                    originalFileType = files.getFileType();
                    originalCosFileId = importOrderfiles.getImportOrderFileId();

                    showNotification(String.format(
                            Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                            Constants.Notification.INFO);
                    break;

            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void uppdateImportOrder(Long fileId) {
        updateFiles(fileId);
        updateImportOrderFile(fileId);
        updateProduct(fileId);
    }

    private void updateImportOrderFile(Long fileId) {
        ImportOrderFileDAO importOrderFileDAO = new ImportOrderFileDAO();
        ImportOrderFile importOrderfiles = importOrderFileDAO.findByFileId(fileId);
        if (importOrderfiles == null) {
            importOrderfiles = new ImportOrderFile();
        }

        importOrderfiles.setFileId(files.getFileId());
        if (documentTypeCode != null) {
            importOrderfiles.setDocumentTypeCode(documentTypeCode);
            nswFileCode = getAutoNswFileCode(documentTypeCode);
            if (files.getFileCode() == null) {
                files.setFileCode(nswFileCode);
                files.setNswFileCode(nswFileCode);
            }
            importOrderfiles.setNswFileCode(nswFileCode);
            importOrderfiles.setResponsiblePersonName(tbPersonName.getText());
            importOrderfiles.setResponsiblePersonAddress(tbPersonAddress.getText());
            importOrderfiles.setResponsiblePersonPhone(tbPersonPhone.getText());
            importOrderfiles.setResponsiblePersonFax(tbPersonFax.getText());
            importOrderfiles.setResponsiblePersonEmail(tbPersonEmail.getText());
            importOrderfiles.setExporterName(tbExporterName.getText());
            importOrderfiles.setExporterAddress(tbExporterAddress.getText());
            importOrderfiles.setExporterFax(tbExporterFax.getText());
            importOrderfiles.setExporterPhone(tbExportPhone.getText());
            importOrderfiles.setExporterEmail(tbExporterEmail.getText());
            importOrderfiles.setComingDate(dbComingDate.getValue());
            importOrderfiles.setTransNo(tbTransNo.getText());
            importOrderfiles.setContractNo(tbContractNo.getText());
            importOrderfiles.setBillNo(tbBillNo.getText());
            importOrderfiles.setCheckPlace(tbCheckPlace.getText());
            importOrderfiles.setCheckTime(dbCheckDate.getValue());
            importOrderfiles.setCustomsBranchName(tbBranchName.getText());
            importOrderfiles.setImporterGateName(tbImportGate.getText());
            importOrderfiles.setExporterGateName(tbExportGate.getText());
            importOrderfiles.setDeptName(tbDepartment.getText());
        }

    }

    private void updateFiles(Long fileId) {
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        Files files = filesDAOHE.findById(fileId);
        if (files == null) {
            files = new Files();
        }

        files.setCreateDate(new Date());
        files.setModifyDate(new Date());
        files.setCreatorId(getUserId());
        files.setCreatorName(getUserFullName());
        files.setCreateDeptId(getDeptId());
        files.setCreateDeptName(getDeptName());
        files.setIsActive(Constants.Status.ACTIVE);
        files.setBusinessName(lbOwnerName.getValue());
        files.setBusinessAddress(lbOwnerAddress.getValue());
        files.setBusinessEmail(tbOwnerEmail.getValue());
        files.setBusinessPhone(lbOwnerPhone.getValue());
        files.setBusinessFax(lbOwnerFax.getValue());
        if (files.getStatus() == null) {
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
        }

        files.setFileType(FILE_TYPE);
    }

    private void updateProduct(Long fileId) {
        List<ImportOrderProduct> orderProductsList = new ImportOrderProductDAO().findAllIdByFileId(fileId);
        ListModel<ImportOrderProduct> listModel = lbOrderProduct.getModel();
        int size = listModel.getSize();

        //delete product
        ImportOrderProduct importOrderProductView;
        ImportOrderProduct importOrderProductDB;
        boolean deletedFlag;
        for (Iterator iterator = orderProductsList.iterator(); iterator
                .hasNext();) {
            deletedFlag = true;
            importOrderProductDB = (ImportOrderProduct) iterator
                    .next();
            for (int i = 0; i < size; i++) {
                importOrderProductView = listModel.getElementAt(i);

                if (importOrderProductDB.getProductId().equals(importOrderProductView.getProductId())) {
                    deletedFlag = false;
                    break;
                }
            }

            if (deletedFlag) {
                new ImportOrderProductDAO().delete(importOrderProductDB);
            }
        }

        /*update + insert*/
        //get products
        orderProductsList = new ImportOrderProductDAO().findAllIdByFileId(fileId);
        ImportOrderProduct importOrderProduct = null;
        boolean addNewFlag;
        for (int i = 0; i < size; i++) {
            importOrderProductView = listModel.getElementAt(i);
            addNewFlag = true;
            for (Iterator iterator = orderProductsList.iterator(); iterator
                    .hasNext();) {
                importOrderProductDB = (ImportOrderProduct) iterator
                        .next();

                if (importOrderProductDB.getProductId().equals(importOrderProductView.getProductId())) {
                    importOrderProduct = importOrderProductDB;
                    addNewFlag = false;
                    break;
                }
            }

            if (addNewFlag) {
                importOrderProduct = importOrderProductView;
                importOrderProduct.setFileId(fileId);
            }

            new ImportOrderProductDAO().saveOrUpdate(importOrderProduct);
        }

    }

    ///////////////////////////////
    private void createPayment() {
        VFeeProcedureDAOHE vFeeFileDAOHE = new VFeeProcedureDAOHE();
        List<VFeeProcedure> lstVFeeProcedure = vFeeFileDAOHE.getListFee(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        Long fileId = files.getFileId();
        if (lstVFeeProcedure.size() > 0) {
            PaymentInfoDAO paymentInfoDAOHE = new PaymentInfoDAO();
            List lst = paymentInfoDAOHE.getListPayment(fileId);
            if (lst.isEmpty()) {
                //Neu chua co ban ghi thanh toan thi luu lai 
                for (VFeeProcedure obj : lstVFeeProcedure) {

                    PaymentInfo paymentInfo = new PaymentInfo();
                    paymentInfo.setFileId(fileId);
                    paymentInfo.setFeeId(obj.getFeeId());
                    paymentInfo.setFeeName(obj.getName());
                    paymentInfo.setIsActive(Constants.Status.ACTIVE);
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_NEW);
                    paymentInfo.setPhase(obj.getPhase());
                    String value = obj.getCost();
                    Long valueL = 0L;
                    try {
                        valueL = Long.valueOf(value);
                    } catch (Exception ex) {
                        LogUtils.addLogDB(ex);
                    }
                    paymentInfo.setCost(valueL);
                    paymentInfoDAOHE.saveOrUpdate(paymentInfo);

                }
            }

        }
    }

    private void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    private boolean isValidatedDataProduct() {
        if (tbProductName.getText().matches("\\s*")) {
            ShowWarningMessageProduct("Tên mặt hàng không thể để trống");
            tbProductName.focus();
            return false;
        }

        if (tbProductName.getText().length() > 250) {
            ShowWarningMessageProduct("Tên mặt hàng quá dài (nhập nhỏ hơn 250 ký tự)");
            tbProductName.focus();
            return false;
        }

        if (tbProductDescription.getText().matches("\\s*")) {
            ShowWarningMessageProduct("Mô tả mặt hàng không thể để trống");
            tbProductDescription.focus();
            return false;
        }

        if (tbProductDescription.getText().length() > 250) {
            ShowWarningMessageProduct("Mô tả mặt hàng quá dài");
            tbProductDescription.focus();
            return false;
        }
        if (tbProductNationName.getText().matches("\\s*")) {
            ShowWarningMessageProduct(" Xuất xứ không thể để trống");
            tbProductNationName.focus();
            return false;
        }
        if (tbProductNationName.getText().length() > 100) {
            ShowWarningMessageProduct(" Xuất xứ quá dài");
            tbProductNationName.focus();
            return false;
        }
        if (tbProductAnnounceNo.getText().matches("\\s*")) {
            ShowWarningMessageProduct("Số công bố không thể để trống");
            tbProductAnnounceNo.focus();
            return false;
        }

        if (tbProductAnnounceNo.getText().length() > 50) {
            ShowWarningMessageProduct("Số công bố quá dài");
            tbProductAnnounceNo.focus();
            return false;
        }

        if (tbProductTotal.getText().matches("\\s*")) {
            ShowWarningMessageProduct(" Số lượng không thể để trống");
            tbProductTotal.focus();
            return false;
        }
        if (tbProductTotal.getText().length() > 9) {
            ShowWarningMessageProduct(" Số lượng nhập quá dài");
            tbProductTotal.focus();
            return false;
        }
        if ((ValidatorUtil.validateTextbox(tbProductTotal, true, 15, ValidatorUtil.PATTERN_CHECK_NUMBER)) != null) {
            ShowWarningMessageProduct("Số lượng là kiểu số");
            tbProductTotal.setText("");
            tbProductTotal.focus();
            return false;
        }
        if (tbProductNetweight.getText().matches("\\s*")) {
            ShowWarningMessageProduct("Khối lượng không thể để trống");
            tbProductNetweight.focus();
            return false;
        }

        if (tbProductNetweight.getText().length() > 9) {
            ShowWarningMessageProduct("Khối lượng quá dài");
            tbProductNetweight.focus();
            return false;
        }
        if ((ValidatorUtil.validateTextbox(tbProductNetweight, true, 15, ValidatorUtil.PATTERN_CHECK_NUMBER)) != null) {
            ShowWarningMessageProduct("Khối lượng là kiểu số");
            tbProductNetweight.setText("");
            tbProductNetweight.focus();
            return false;
        }
        if (tbProductBaseUnit.getText().matches("\\s*")) {
            ShowWarningMessageProduct(" Gía trị mặt hàng không thể để trống");
            tbProductBaseUnit.focus();
            return false;
        }
        if (tbProductBaseUnit.getText().length() > 18) {
            ShowWarningMessageProduct(" Gía trị mặt hàng quá dài");
            tbProductBaseUnit.focus();
            return false;
        }
        if ((ValidatorUtil.validateTextbox(tbProductBaseUnit, true, 18, ValidatorUtil.PATTERN_CHECK_NUMBER)) != null) {
            ShowWarningMessageProduct("Giá trị mặt hàng là kiểu số ");
            tbProductBaseUnit.setText("");
            tbProductBaseUnit.focus();
            return false;
        }
        return true;
    }

    private boolean isValidatedData() {
        if (tbPersonName.getText().matches("\\s*")) {
            showWarningMessage("Tên thương nhân chịu trách nhiệm không thể để trống");
            tbProductTotal.setText("");
            tbPersonName.focus();
            return false;
        }
        if (tbPersonAddress.getText().matches("\\s*")) {
            showWarningMessage("Địa chỉ thương nhân chịu trách nhiệm không thể để trống");
            tbPersonAddress.focus();
            return false;
        }
        if (tbPersonPhone.getText().matches("\\s*")) {
            showWarningMessage("Số điện thoại thương nhân chịu trách nhiệm không thể để trống");
            tbPersonPhone.focus();
            return false;
        }
        if ((ValidatorUtil.validateTextbox(tbPersonPhone, true, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER, 9)) != null) {
            showWarningMessage("Số điện thoại là kiểu số từ 9 đến 15 kí tự ");
            tbPersonPhone.setText("");
            tbPersonPhone.focus();
            return false;
        }
        if (!ValidatorUtil.validateRegex(tbPersonEmail.getText(), ValidatorUtil.PATTERN_CHECK_EMAIL)) {
            showWarningMessage("Email thương nhân chịu trách nhiệm sai định dạng");
            tbPersonEmail.setText("");
            tbPersonEmail.focus();
            return false;
        }

        if (tbExporterName.getText().matches("\\s*")) {
            showWarningMessage("Tên thương nhân xuất khẩu không thể để trống");

            tbExporterName.focus();
            return false;
        }
        if (tbExporterAddress.getText().matches("\\s*")) {
            showWarningMessage("Địa chỉ thương nhân xuất khẩu không thể để trống");

            tbExporterAddress.focus();
            return false;
        }
        if (tbTransNo.getText().matches("\\s*")) {
            showWarningMessage("Số đơn vận không thể để trống");
            tbTransNo.focus();
            return false;
        }
        if (tbContractNo.getText().matches("\\s*")) {
            showWarningMessage("Số hợp đồng không thể để trống");

            tbContractNo.focus();
            return false;
        }
        if (tbBillNo.getText().matches("\\s*")) {
            showWarningMessage("Số hóa đơn không thể để trống");

            tbBillNo.focus();
            return false;
        }
        if (dbComingDate.getText().matches("\\s*")) {
            showWarningMessage("Thời gian nhập khẩu dự kiến không thể để trống");

            dbComingDate.focus();
            return false;
        }

        if (tbExportGate.getText().matches("\\s*")) {
            showWarningMessage("Cửa khẩu đi không thể để trống");

            tbExportGate.focus();
            return false;
        }
        if (tbImportGate.getText().matches("\\s*")) {
            showWarningMessage("Cửa khẩu đến không thể để trống");

            tbImportGate.focus();
            return false;
        }
        if (tbBranchName.getText().matches("\\s*")) {
            showWarningMessage("Chi cục hải quan không thể để trống");

            tbBranchName.focus();
            return false;
        }
        if (dbCheckDate.getText().matches("\\s*")) {
            showWarningMessage("Thời gian kiểm tra không thể để trống");

            dbCheckDate.focus();
            return false;
        }
        if (tbCheckPlace.getText().matches("\\s*")) {
            showWarningMessage("Địa điểm không thể để trống");

            tbCheckPlace.focus();
            return false;
        }
        if (tbDepartment.getText().matches("\\s*")) {
            showWarningMessage("Tổ chức kiểm tra không thể để trống");

            tbDepartment.focus();
            return false;
        }
        return true;
    }

    private void createObject() throws Exception {
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        files = createFile();
        filesDAOHE.saveOrUpdate(files);

        ImportOrderFileDAO importfileDAO = new ImportOrderFileDAO();
        importOrderfiles = importOrderFile();

        importfileDAO.saveOrUpdate(importOrderfiles);

        saveProduct(importOrderfiles.getFileId());
    }

    private void createObjectCreatNew() throws Exception {
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        files = createFile();
        filesDAOHE.saveOrUpdate(files);

        ImportOrderFileDAO importfileDAO = new ImportOrderFileDAO();
        importOrderfiles = importOrderFileNew();

        importfileDAO.saveOrUpdate(importOrderfiles);

        saveProduct(importOrderfiles.getFileId());
    }

    /**
     * Xu ly su kien upload file
     *
     * @param event
     */
    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("z-valign-left");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException, Exception {
        int idx = lbImportOrderFileType.getSelectedItem().getIndex();
        if (idx == 0) {
            showNotification("Bạn phải chọn loại hồ sơ", Constants.Notification.INFO);
            return;
        }
        if (listMedia.isEmpty()) {
            showNotification("Chọn tệp tải lên trước khi thêm mới!",
                    Constants.Notification.WARNING);

            return;
        }

        if (saveObject() == false) {
            return;
        }
        Long rtFileFileType = Long.valueOf((String) lbImportOrderFileType.getSelectedItem().getValue());
        AttachDAO base = new AttachDAO();

        for (Media media : listMedia) {
            base.saveFileAttach(media, files.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE, rtFileFileType);
        }

        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE, files.getFileId());
        if ((listMedia != null) && (listMedia.size() > 0)) {
            listMedia.clear();
        }
        flist.getChildren().clear();

    }

    private Files createFileUpdates() throws Exception {
        Date dateNow = new Date();
        files.setModifyDate(dateNow);
        return files;
    }

    private void fillFileListbox(Long attcat, Long fileId) {
        VAttfileCategoryDAO categoryDAO = new VAttfileCategoryDAO();

        List<VAttfileCategory> listAttach = categoryDAO.findCheckedFilecAttach(attcat, fileId);
        this.fileListbox.setModel(new ListModelArray(listAttach));

    }

    public boolean saveObject() throws IOException, Exception {
        boolean returnValue = true;
        //neu chua co fileID thi them moi
        if (files.getFileId() == null) {
            //Luu thong tin ho so truoc khi tao
            if (isValidatedData()) {
                createObject();

                return true;
            } else {
                return false;
            }
        }
        return returnValue;

    }

    //thanhdv THEM TU FILE EXCEL;
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Listen("onUpload = #btnImportExcel")
    public void onUploadExcel(UploadEvent event) throws UnsupportedEncodingException {
        if ((listFileExcel != null) && (listFileExcel.size() > 0)) {
            showNotification("Chỉ được phép chọn 1 file import!",
                    Constants.Notification.WARNING);
            return;
        }
        listFileExcel = new ArrayList<Media>();
        final Media[] medias = event.getMedias();
        if (medias.length > 1) {
            showNotification("Chỉ được phép chọn 1 file import!",
                    Constants.Notification.WARNING);
            return;
        }
        for (final Media media : medias) {
            String extFile = media.getFormat().replace("\"", "");
            if (!("xlsx".equals(extFile.toLowerCase())
                    || "xls".equals(extFile.toLowerCase()))) {
                showNotification("Định dạng file không được phép tải lên",
                        Constants.Notification.WARNING);
                continue;
            }

            // luu file vao danh sach file
            listFileExcel.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listFileExcel.remove(media);
                }
            });
            hl.appendChild(rm);
            fListImportExcel.appendChild(hl);
        }
    }

    @Listen("onDownloadFile = #lbDownloadExcelTemplate")
    public void onDownloadFile() throws FileNotFoundException {
        String folderPath = Executions.getCurrent().getDesktop().getWebApp().getRealPath(Constants.UPLOAD.ATTACH_PATH);
        String path = folderPath + "\\BM_YCNK.xlsx";
        File f = new File(path);
//        if (f != null) {
        if (f.exists()) {
            File tempFile = FileUtil.createTempFile(f, f.getName());
            Filedownload.save(tempFile, path);
        } else {
            Clients.showNotification("File không còn tồn tại trên hệ thống!", Constants.Notification.INFO, null, "middle_center", 1500);
        }
//        } else {
//            Clients.showNotification("File không còn tồn tại trên hệ thống!", Constants.Notification.INFO, null, "middle_center", 1500);
//        }
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onClick=#btnCreateProFile")
    public void onCreateProFile() throws IOException, Exception {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", windowCRUDCosmetic);
        Window window = (Window) Executions.createComponents(
                "/Pages/module/importorder/PublicFileManageList.zul", null, arguments);
        window.doModal();
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long fileId = obj.getObjectId();
        ImportOrderAttachDao rDAOHE = new ImportOrderAttachDao();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE, fileId);
    }

    public Long getAttachmentSize() {
        if (files != null && files.getFileId() != null) {

            AttachDAOHE daoHE = new AttachDAOHE();
            Long currentSize = 0L;
            List<Attachs> attachs = daoHE.findByObjectId(files.getFileId());
            for (Attachs item : attachs) {
                try {
                    File f = new File(item.getFullPathFile());
                    if (f.exists()) {
                        currentSize += f.length();
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }
            }
            return currentSize;
        }
        return 0L;
    }

    protected void ShowWarningMessageProduct(String ms) {
        lbBottomWarning.setValue("");
        lbTopWarning.setValue("");
        lbProductWarning.setValue(ms);
    }

    protected void showWarningMessage(String message) {
        lbProductWarning.setValue("");
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    // Tao file excel theo bieu mau co san
    @SuppressWarnings("deprecation")
    @Listen("onClick=#btnCreateFromImport")
    public void onCreateFromImportExcel(Event event) throws IOException, Exception {
        if (listFileExcel == null || listFileExcel.isEmpty()) {
            lbImportExcelWarning.setValue("Chọn tệp tải lên trước khi thêm mới!");
            return;
        }

        lbImportExcelWarning.setValue(null);
        for (Media media : listFileExcel) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String fileName = media.getName();
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("yyyy");
            folderPath += separator + dateFormat.format(date);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                //tạo forlder
                fd.mkdirs();
            }
            File f = new File(folderPath + separator + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            //save to hard disk 
            InputStream inputStream = media.getStreamData();
            OutputStream outputStream = new FileOutputStream(f);;

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }

            //read excel
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(f));
            dateFormat.applyPattern(DATE_FORMAT);
            XSSFSheet sheet = wb.getSheet(NHAP_KHAU);
            tbPersonName.setValue(sheet.getRow(1).getCell(1).toString().trim());
            tbPersonAddress.setValue(sheet.getRow(1).getCell(3).toString().trim());
            tbPersonPhone.setValue(sheet.getRow(2).getCell(1).toString().trim());
            tbPersonFax.setValue(sheet.getRow(2).getCell(3).toString().trim());
            tbPersonEmail.setValue(sheet.getRow(3).getCell(1).toString().trim());

            tbExporterName.setValue(sheet.getRow(5).getCell(1).toString().trim());
            tbExporterAddress.setValue(sheet.getRow(5).getCell(3).toString().trim());
            tbExportPhone.setValue(sheet.getRow(6).getCell(1).toString().trim());
            tbExporterFax.setValue(sheet.getRow(6).getCell(3).toString().trim());
            tbExporterEmail.setValue(sheet.getRow(7).getCell(1).toString().trim());
            //load 1
            tbTransNo.setValue(sheet.getRow(9).getCell(1).toString().trim());
            tbContractNo.setValue(sheet.getRow(9).getCell(3).toString().trim());
            tbBillNo.setValue(sheet.getRow(10).getCell(1).toString().trim());
            if (!"".equals(sheet.getRow(10).getCell(3).toString())) {
                dbComingDate.setValue(new Date(sheet.getRow(10).getCell(3).toString()));
            }
            tbExportGate.setValue(sheet.getRow(11).getCell(1).toString().trim());

            tbImportGate.setValue(sheet.getRow(11).getCell(3).toString().trim());
            tbBranchName.setValue(sheet.getRow(12).getCell(1).toString().trim());
            if (!"".equals(sheet.getRow(12).getCell(3).toString())) {
                dbCheckDate.setValue(new Date(sheet.getRow(12).getCell(3).toString()));
            }

            tbCheckPlace.setValue(sheet.getRow(13).getCell(1).toString().trim());
            tbDepartment.setValue(sheet.getRow(13).getCell(3).toString().trim());

            XSSFSheet sheet1 = wb.getSheet(LO_HANG);
            int j = 2;
            importOrderProducts.clear();
            while (sheet1.getRow(j) != null && sheet1.getRow(j).getCell(1) != null
                    && !"".equals(sheet1.getRow(j).getCell(1).toString())) {
                ImportOrderProduct iop = new ImportOrderProduct();
                iop.setProductName(String.valueOf(sheet1.getRow(j).getCell(1).toString().trim()));
                iop.setProductDescription(String.valueOf(sheet1.getRow(j).getCell(2).toString().trim()));
                iop.setNationalName(String.valueOf(sheet1.getRow(j).getCell(3).toString().trim()));
                iop.setConfirmAnnounceNo(sheet1.getRow(j).getCell(4).toString().trim());
                //load
                if (!"".equals(sheet1.getRow(j).getCell(5).toString().trim())) {
                    Double total = Double.parseDouble(sheet1.getRow(j).getCell(5).toString().trim());
                    iop.setTotal(total);
                }
                if (!"".equals(sheet1.getRow(j).getCell(6).toString().trim())) {
                    Double netweight = Double.parseDouble(sheet1.getRow(j).getCell(6).toString().trim());
                    iop.setNetweight(netweight);
                }
                if (!"".equals(sheet1.getRow(j).getCell(7).toString().trim())) {
                    Long baseUnit = (long) Double.parseDouble(sheet1.getRow(j).getCell(7).toString().trim());
                    iop.setBaseUnit(baseUnit);
                }
                importOrderProducts.add(iop);
                j++;
            }
            ListModelArray lstModelManufacturer = new ListModelArray(importOrderProducts);
            lbOrderProduct.setModel(lstModelManufacturer);
//            tabs.setSelectedPanel(mainpanel);
            inputStream.close();
            outputStream.close();
        }
        return;
    }

    private ImportOrderFile copyOrderFiles(ImportOrderFile old) {
        ImportOrderFile newObj = new ImportOrderFile();
        newObj.setImportOrderFileId(old.getImportOrderFileId());
        newObj.setGoodsOwnerName(old.getGoodsOwnerName());
        newObj.setGoodsOwnerAddress(old.getGoodsOwnerAddress());
        newObj.setGoodsOwnerPhone(old.getGoodsOwnerPhone());
        newObj.setGoodsOwnerFax(old.getGoodsOwnerFax());
        newObj.setGoodsOwnerEmail(old.getGoodsOwnerEmail());
        newObj.setResponsiblePersonName(old.getResponsiblePersonName());
        newObj.setResponsiblePersonAddress(old.getResponsiblePersonAddress());
        newObj.setResponsiblePersonPhone(old.getResponsiblePersonPhone());
        newObj.setResponsiblePersonFax(old.getResponsiblePersonFax());
        newObj.setResponsiblePersonEmail(old.getResponsiblePersonEmail());
        newObj.setExporterName(old.getExporterName());
        newObj.setExporterAddress(old.getExporterAddress());
        newObj.setExporterFax(old.getExporterFax());
        newObj.setExporterPhone(old.getExporterPhone());
        newObj.setExporterEmail(old.getExporterEmail());
        newObj.setComingDate(old.getComingDate());
        newObj.setTransNo(old.getTransNo());
        newObj.setContractNo(old.getContractNo());
        newObj.setBillNo(old.getBillNo());
        newObj.setCheckPlace(old.getCheckPlace());
        newObj.setCheckTime(old.getCheckTime());
        newObj.setCustomsBranchName(old.getCustomsBranchName());
        newObj.setImporterGateName(old.getImporterGateName());
        newObj.setExporterGateName(old.getExporterGateName());
        newObj.setDeptName(old.getDeptName());

        return newObj;
    }

    private Files copy(Files old) {
        Files newObj = new Files();
        newObj.setBusinessAddress(old.getBusinessAddress());
        newObj.setBusinessFax(old.getBusinessFax());
        newObj.setBusinessId(old.getBusinessId());
        newObj.setBusinessName(old.getBusinessName());
        newObj.setBusinessPhone(old.getBusinessPhone());
        newObj.setCreateDate(new Date());
        newObj.setCreateDeptId(old.getCreateDeptId());
        newObj.setCreateDeptName(old.getCreateDeptName());
        newObj.setCreatorId(old.getCreatorId());
        newObj.setCreatorName(old.getCreatorName());
        newObj.setFileCode(old.getFileCode());
        newObj.setFileName(old.getFileName());
        newObj.setFileType(old.getFileType());
        newObj.setFileTypeName(old.getFileTypeName());
        newObj.setFlowId(old.getFlowId());
        newObj.setIsActive(old.getIsActive());
        newObj.setIsTemp(old.getIsTemp());
        newObj.setTaxCode(old.getTaxCode());
        return newObj;
    }

    //
    private void createOldVersion() {
        //Tao 1 file verion cu isActive = 0
        Files fileVerion = copy(files);
        fileVerion.setIsActive(Constants.Status.INACTIVE);
        if (fileVerion.getStatus() == null) {
            fileVerion.setStatus(Constants.PROCESS_STATUS.INITIAL);
        }
        fileVerion.setModifyDate(new Date());
        fileVerion.setParentFileId(files.getFileId());
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        filesDAOHE.saveOrUpdate(fileVerion);

        //Tao cosfile cu co fileId bang file Id vua tao
        ImportOrderFile idf = copyOrderFiles(importOrderfiles);
        idf.setFileId(fileVerion.getFileId());
//        idf.setNswFileCode(importDeviceFile.getNswFileCode());
        ImportOrderFileDAO idfDAO = new ImportOrderFileDAO();
        idfDAO.saveOrUpdate(idf);

        saveVersionProduct(idf.getFileId());
    }

    public void setProcessingView(Long fileId, Long fileType) {
        if (fileId != null && fileType != null) {

            // 13032015 Load danh sach yeu cau sdbs
            BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
            BookDocument bookDocument2 = bookDocumentDAOHE.getBookInFromDocumentId(fileId, fileType);
            if (bookDocument2 != null) {
                bookDocument = bookDocument2;
            }

            // 13032015 Load danh sach yeu cau sdbs
            CosAdditionalRequestDAO cosAdditionalRequestDAO = new CosAdditionalRequestDAO();
            List<CosAdditionalRequest> lstAddition = cosAdditionalRequestDAO.findAllActiveByFileId(fileId);
            if (lstAddition != null && lstAddition.size() > 0) {
                additionalRequest = lstAddition.get(0);
            }

            CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
            evaluationRecord = dao.getLastEvaluation(fileId);

            CosPermitDAO cosPermitDAO = new CosPermitDAO();
            List<CosPermit> lstPermit = cosPermitDAO.findAllActiveByFileId(fileId);
            if (lstPermit != null && lstPermit.size() > 0) {
                permit = lstPermit.get(0);
            }

            PaymentInfoDAO paymentInfoDAO = new PaymentInfoDAO();
            List<PaymentInfo> lstPayment = paymentInfoDAO.getListPayment(fileId, Constants.PAYMENT.PHASE.EVALUATION);
            if (lstPayment != null && lstPayment.size() > 0) {
                paymentInfo = lstPayment.get(0);
            }

        }

    }

    /**
     * Thanhdv Hàm load danh sach trong dropdown control
     *
     * @param type
     * @return
     */
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case IMPORT_ORDER_FILE:
                categoryDAOHE = new CategoryDAOHE();
                listImportOderFileType = categoryDAOHE.findAllCategory(
                        Constants.CATEGORY_TYPE.FILECAT_IMDOC);
                lstModel = new ListModelList(listImportOderFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    private void loadDataEdit() {
        ImportOrderFile iof;
        ImportOrderFileDAO idfDAO = new ImportOrderFileDAO();
        iof = idfDAO.findByFileId(originalFilesId);
        if (iof != null) {

            lblTaxCode.setValue(files.getTaxCode());
            lbMahoso.setValue(files.getFileCode());
            lbOwnerName.setValue(files.getBusinessName());
            lbOwnerAddress.setValue(files.getBusinessAddress());
            lbOwnerPhone.setValue(files.getBusinessPhone());
            lbOwnerFax.setValue(files.getBusinessFax());
            tbOwnerEmail.setValue(files.getBusinessEmail());

            tbPersonName.setValue(iof.getResponsiblePersonName());
            tbPersonAddress.setValue(iof.getResponsiblePersonAddress());
            tbPersonPhone.setValue(iof.getResponsiblePersonPhone());
            tbPersonFax.setValue(iof.getResponsiblePersonFax());
            tbPersonEmail.setValue(iof.getResponsiblePersonEmail());

            tbExporterName.setValue(iof.getExporterName());
            tbExporterAddress.setValue(iof.getExporterAddress());
            tbExportPhone.setValue(iof.getExporterFax());
            tbExporterFax.setValue(iof.getExporterFax());
            tbExporterEmail.setValue(iof.getExporterEmail());
            tbTransNo.setValue(iof.getTransNo());
            tbContractNo.setValue(iof.getContractNo());
            tbBillNo.setValue(iof.getBillNo());
            dbComingDate.setValue(iof.getComingDate());
            tbExportGate.setValue(iof.getExporterGateName());
            tbImportGate.setValue(iof.getImporterGateName());
            tbBranchName.setValue(iof.getCustomsBranchName());
            dbCheckDate.setValue(iof.getCheckTime());
            tbCheckPlace.setValue(iof.getCheckPlace());
            tbDepartment.setValue(iof.getDeptName());
        }
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Window getWindowCRUDCosmetic() {
        return windowCRUDCosmetic;
    }

    public void setWindowCRUDCosmetic(Window windowCRUDCosmetic) {
        this.windowCRUDCosmetic = windowCRUDCosmetic;
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    private void saveVersionProduct(Long fileIds) {
        for (ImportOrderProduct obj : listOrderProduct) {

            ImportOrderProduct idp = new ImportOrderProduct();
            idp.setProductId(obj.getProductId());
            idp.setFileId(fileIds);
            idp.setProductName(obj.getProductName());
            idp.setProductDescription(obj.getProductDescription());
            idp.setNationalName(obj.getNationalName());
            idp.setConfirmAnnounceNo(obj.getConfirmAnnounceNo());
            idp.setTotal(obj.getTotal());
            idp.setNetweight(obj.getNetweight());
            idp.setBaseUnit(obj.getBaseUnit());
            idp.setTotalUnitCode(obj.getTotalUnitCode());
            idp.setTotalUnitName(obj.getTotalUnitName());
            idp.setNetweightUnitCode(obj.getNetweightUnitCode());
            idp.setNetweightUnitName(obj.getNetweightUnitName());
            idp.setManufacTurer(obj.getManufacTurer());
            idp.setManufacturerAddress(obj.getManufacturerAddress());
            new ImportOrderProductDAO().saveOrUpdate(idp);
        }
    }

    //
    @Listen("onClose = #windowCRUDCosmetic")
    public void onClose() {
        try {
            if (windowCRUDCosmetic != null) {
                windowCRUDCosmetic.detach();
            }
            Events.sendEvent("onVisible", parentWindow, null);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }
}
