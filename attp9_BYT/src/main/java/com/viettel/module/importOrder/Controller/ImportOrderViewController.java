/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import com.viettel.module.cosmetic.Controller.*;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Model.CosProductTypeSubModel;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;

import java.io.FileNotFoundException;

import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Br;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;

import com.viettel.core.workflow.BO.Process;
import com.viettel.module.cosmetic.BO.*;
import com.viettel.module.cosmetic.DAO.*;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.importOrder.BO.*;
import com.viettel.module.importOrder.DAO.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.module.importOrder.Model.ExportOrderFileModel;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.module.importfood.DAO.IfdFilePaymentDAO;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBException;

/**
 *
 * @author giangnh20
 */
public class ImportOrderViewController extends CosmeticBaseController {

    private final int RAPID_TEST_FILE_TYPE = 1;
    private static long FILE_TYPE = Constants.FILE_TYPE_DYCNK;
    private long DEPT_ID;
    // private String nswFileCode;
    @Wire
    private Div divToolbarBottom;
    private List<Component> listTopActionComp;
    @Wire
    private Div divToolbarTop;
    private List<Component> listBottomActionComp;
    @Wire
    private Vlayout flist;
    private List<Media> listMedia;
    private List listCosmeticFileType;
    @Wire
    private Window windowView;
    private Window parentWindow;
    private String isSave;
    private ListModel model;
    private VFileImportOrder cosFile;
    private Files files;
    Long documentTypeCode;
    @Wire
    private Listbox finalFileListboxProKTKN, finalFileListboxProregister,
            finalFileListboxProductgiam, finalFileListboxProSDBS;
    //linhdx them cac ho so kiem tra
    @Wire
    private Listbox fileListboxOrderReturn, fileListboxOrderFile;
    @Wire
    private Listbox fileListboxDuplicateTransportNo;
    private String fileListboxDuplicateTransportNoFlag = "0";
    @Wire
    private Listbox lbManufacturer;
    private List<CosManufacturer> lstManufacturer = new ArrayList();
    @Wire
    private Listbox lbAssembler;
    @Wire
    private Listbox lFillFee;
    @Wire
    Checkbox cbAssemblerMain, cbAssemblerSub, cbIngreConfirm1, cbIngreConfirm2;
    private List<CosAssembler> lstAssembler = new ArrayList();
    @Wire
    private Listbox lbCosfileIngre;
    @Wire("#incList #lbCosfileIngreCheck")
    private Listbox lbCosfileIngreCheck;
    @Wire("#incList #lbAnnexe")
    private Listbox lbAnnexe;
    private List<CosCosfileIngre> lstCosfileIngre = new ArrayList();
    @Wire
    private Listbox lbProductPresentation, lbProductType;
    @Wire("#incListEvaluation #lbListEvaluation")
    private Listbox lbListEvaluation;
    @Wire("#incList #txtSubstance")
    Textbox txtSubstance;
    private List ingredientListCheck, lstAnnexe;
    // private VFileCosfile vFileCosfile;
    private com.viettel.core.workflow.BO.Process processCurrent;// process dang
    // duoc xu li
    private Integer menuType;
    private Long fileId;
    private CosAdditionalRequest additionalRequest;
    private BookDocument bookDocument = new BookDocument();
    private CosEvaluationRecord evaluationRecord = new CosEvaluationRecord();
    private CosPermit permit = new CosPermit();
    private PaymentInfo paymentInfo = new PaymentInfo();
    private CosReject reject = new CosReject();
    @Wire("#incList #userPagingBottom")
    private Paging userPagingBottom;
    @Wire
    private Div divDispath;
    @Wire
    private Div divDispathReject;
    @Wire
    private Tabpanel tabpanel1;
    private ImportOrderFile importOrderFile;
//	@Wire
//	Listbox lbOrderProduct;
    List<NodeToNode> lstNextAction;

    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        listMedia = new ArrayList();
        fileId = (Long) arguments.get("id");
        files = (new FilesDAOHE()).findById(fileId);
        //FILE_TYPE = files.getFileType();

        DEPT_ID = Constants.CUC_ATTP_XNN_ID;
        //linhdx add 20160316 thay doi quy trinh moi
        try {
            String date_ud = ResourceBundleUtil.getString("ATTP_CHANGE_QT_time", "config");
            Date d_ud = formatterDateTime.parse(date_ud);
            if (files != null && files.getCreateDate() != null && files.getCreateDate().before(d_ud)) {
                DEPT_ID = Constants.BOYTE;
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }

        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        // Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);

        // load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        // nswFileCode = getAutoNswFileCode(documentTypeCode);
        if (files != null) {
            processCurrent = WorkflowAPI.getInstance().getCurrentProcess(
                    files.getFileId(), files.getFileType(), files.getStatus(),
                    getUserId());
            // Xem qua trinh xu ly
            setProcessingView(files.getFileId(), files.getFileType());
        }

        menuType = (Integer) arguments.get("menuType");
        if (cosFile != null) {
            checkDuplicateFilecheckTransport(cosFile.getTransNo(), fileId);
        }

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        listTopActionComp = new ArrayList<>();
        listBottomActionComp = new ArrayList<>();
        addAllNextActions();
        if (menuType != null) {
            loadActionsToToolbar(menuType, files, processCurrent, windowView);
        }
        if (checkListOK() == 1) {
            fillFinalFileRegister(fileId);
        }

        if (checkAmendment() == 1) {
            fillFinalFileSDBX();
        }

        if (checkKTKN() == 1) {
            fillFinalListboxProKTKN(fileId);
        }
        if (checkReduced() == 1) {
            fillFinalListboxProductgiam(fileId);
        }

        if (checkFee() == 1) {
            fillFee(fileId);
        }
        //linhdx them danh sach cac ho so kiem tra
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, fileId);
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS, fileId);
        fillDuplicateFilecheckTransport(cosFile.getTransNo(), fileId);
    }
    //Yeu cau bo xung

    private void fillFinalFileSDBX() {

        AttachDAOHE rDaoHe = new AttachDAOHE();

        List<Attachs> attachs = new ArrayList<Attachs>();
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO.findAllActiveByFileId(fileId);
        // get file attach
        if (lstAdd.size() > 0) {
            AdditionalRequest additionalRequest = lstAdd.get(0);
            ///List<Attachs> lstAttach2 = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
            List<Attachs> lstAttach2 = rDaoHe.getByAllFileAttachSDBS(
                    additionalRequest.getAdditionalRequestId(),
                    Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                    Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
            attachs.addAll(lstAttach2);
        }
        this.finalFileListboxProSDBS.setModel(new ListModelArray(attachs));
        // TODO Auto-generated method stub

    }

    public int checkAmendment() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO.findAllActiveByFileId(fileId);
        // get file attach
        if (lstAdd.size() > 0) {
            AdditionalRequest additionalRequest = lstAdd.get(0);
            List<Attachs> lstAttach = rDaoHe.getByAllFileAttachSDBS(
                    additionalRequest.getAdditionalRequestId(),
                    Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                    Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
            if (lstAttach.size() > 0) {
                return 1;
            }
        }
        return 0;
    }
    //IMPORT_ORDER_FILE_REDUCED_PAPER. giay kiem tra giam

    private void fillFinalListboxProductgiam(Long fileId2) {

        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach4 = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId2,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
        // TODO Auto-generated method stub

        this.finalFileListboxProductgiam.setModel(new ListModelArray(lstAttach4));

    }

    public int checkReduced() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;

    }

    @Listen("onDownloadFinalFile = #finalFileListboxProSDBS, #finalFileListboxProKTKN, #finalFileListboxProductgiam, #finalFileListboxProregister")
    public void onDownloadFinalFileALL(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }
    //IMPORT_ORDER_FILE_PAPER

    private void fillFinalListboxProKTKN(Long fileId2) {

        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId2,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
        // TODO Auto-generated method stub
        this.finalFileListboxProKTKN.setModel(new ListModelArray(lstAttach));
    }

    public int checkKTKN() {
        AttachDAOHE rDaoHe = new AttachDAOHE();

        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    //IMPORT_ORDER_FILE_REGISTERED_PAPER giay cong bo 
    private void fillFinalFileRegister(Long fileId2) {

        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach3 = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
                fileId2,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);

        this.finalFileListboxProregister.setModel(new ListModelArray(lstAttach3));
        // TODO Auto-generated method stub

    }

    public int checkListOK() {
        AttachDAOHE rDaoHe = new AttachDAOHE();

        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
                fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    public void loadActionsToToolbar(int menuType, final Files files,
            final com.viettel.core.workflow.BO.Process currentProcess,
            final Window currentWindow) {

        for (Component comp : listTopActionComp) {
            divToolbarTop.appendChild(comp);
        }
        for (Component comp : listBottomActionComp) {
            divToolbarBottom.appendChild(comp);
        }

    }

    public void setProcessingView(Long fileId, Long fileType) {
        if (fileId != null && fileType != null) {

            // linhdx 13032015 Load danh sach yeu cau sdbs
            BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
            BookDocument bookDocument2 = bookDocumentDAOHE.getBookInFromDocumentId(fileId, fileType);
            if (bookDocument2 != null) {
                bookDocument = bookDocument2;
            }

            // linhdx 13032015 Load danh sach yeu cau sdbs
            CosAdditionalRequestDAO cosAdditionalRequestDAO = new CosAdditionalRequestDAO();
            List<CosAdditionalRequest> lstAddition = cosAdditionalRequestDAO.findAllActiveByFileId(fileId);
            if (lstAddition != null && lstAddition.size() > 0) {
                additionalRequest = lstAddition.get(0);
            }

            CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
            evaluationRecord = dao.getLastEvaluation(fileId);

            CosPermitDAO cosPermitDAO = new CosPermitDAO();
            List<CosPermit> lstPermit = cosPermitDAO.findAllActiveByFileId(fileId);
            if (lstPermit != null && lstPermit.size() > 0) {
                permit = lstPermit.get(0);
            }

            CosRejectDAO cosRejectDAO = new CosRejectDAO();
            List<CosReject> lstReject = cosRejectDAO.findAllActiveByFileId(fileId);
            if (lstReject != null && lstReject.size() > 0) {
                reject = lstReject.get(0);
            }

            PaymentInfoDAO paymentInfoDAO = new PaymentInfoDAO();
            List<PaymentInfo> lstPayment = paymentInfoDAO.getListPayment(
                    fileId, Constants.PAYMENT.PHASE.EVALUATION);
            if (lstPayment != null && lstPayment.size() > 0) {
                paymentInfo = lstPayment.get(0);
            }

        }

    }

    @Listen("onViewFlow = #windowView")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        Long docId = files.getFileId();
        //Long docType = files.getFileType();
        Long docType = Constants.FILE_TYPE_DYCNK;
        args.put("objectId", docId);
        args.put("objectType", docType);
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    @Listen("onClick = #btnOpenAttachFileFinal")
    public void onCreateAttachFileFinal() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdAttachFinalFileCRUD",
                "/Pages/module/cosmetic/attachFinalFileCRUD.zul", arguments,
                Window.POPUP);
    }

    private Map<String, Object> setArgument() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        return arguments;
    }

    public void setListEvaluation() {
        // lay danh sach cac tham dinh
        if (checkEvaluation() == 1) {
            CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
            List<CosEvaluationRecord> lstCosEvaluationRecord = dao.getAllEvaluation(files.getFileId());
            if (lbListEvaluation != null) {
                lbListEvaluation.setModel(new ListModelArray(
                        lstCosEvaluationRecord));
            }

        }
    }

    @Listen("onOpenView = #incListEvaluation #lbListEvaluation")
    public void onOpenView(Event event) {
        CosEvaluationRecord evaluationRecord = (CosEvaluationRecord) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("evaluationRecord", evaluationRecord);
        Window window = createWindow("windowViewEvalution",
                "/Pages/module/cosmetic/viewEvaluation.zul", arguments,
                Window.POPUP);
        window.doModal();

    }

    @Listen("onAfterRender = #lbProductPresentation")
    public void onAfterRenderProductPresentation() {
        loadProductPresentationCheck();
        disableListItem(lbProductPresentation);
    }

    /**
     * Ham disable list item sau khi render Neu set trong zul thi nhung truong
     * checked khong hien thi
     *
     * @param lb
     */
    public void disableListItem(Listbox lb) {
        if (lb != null) {
            for (Listitem item : lb.getItems()) {
                item.setDisabled(true);
            }
        }
    }

    private void addAllNextActions() {
        // linhdx
        //Long docStatus = files.getStatus();
        Long docId = files.getFileId();
        Long userId = getUserId();
        Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;
        List<Process> lstProcess = WorkflowAPI.getInstance().getProcess(docId,
                docType, userId);
        List<Process> lstAllProcess = WorkflowAPI.getInstance().getAllProcessNotFinish(
                docId, docType);
        if ((lstAllProcess == null || lstAllProcess.isEmpty()) && files.getStatus() != Constants.FILE_STATUS_CODE.STATUS_MOITAO) {
            return;
        }
        List<Long> lstStatus = new ArrayList();
        // linhdx tim nhung trang thai chua hoan thanh xu ly de tim action tiep
        // theo
        for (Process obj : lstProcess) {
            if (obj.getFinishDate() == null) {
                lstStatus.add(obj.getStatus());
            }
        }
        // Neu khong co add flag -1 de bao la khong phai xu ly nua
        if (lstStatus.isEmpty() && !lstProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }

        if (lstStatus.isEmpty() && lstAllProcess != null && !lstAllProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }

        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(docType, lstStatus, deptId);
        lstNextAction = actions;// linhdx
        if (actions != null && actions.size() > 0) {
            NodeToNode action = actions.get(0);
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
    }

    // viethd3: create new button for a processing action
    private Button createButtonForAction(NodeToNode action) {

        Button btn = new Button("Xử lý hồ sơ");
        final String actionName = "Xử lý hồ sơ";
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action
        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = files.getStatus();
        final List<NodeToNode> lstNextAction1 = lstNextAction;

        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, false));

        EventListener<Event> event = new EventListener<Event>() {

            @Override
            public void onEvent(Event t) throws Exception {
                Map<String, Object> data = new ConcurrentHashMap<>();
                data.put("fileId", files.getFileId());
                data.put("docId", files.getFileId());
                data.put("docType", files.getFileType());
                data.put("docStatus", status);
                data.put("actionId", actionId);
                data.put("actionName", actionName);
                data.put("actionType", actionType);
                data.put("nextId", nextId);
                data.put("previousId", prevId);
                if (processCurrent != null) {
                    data.put("process", processCurrent);
                }

                data.put("lstAvailableNDU", lstNDUs);
                data.put("parentWindow", windowView);
                data.put("lstNextAction", lstNextAction1);
                createWindow("windowComment", formName, data, Window.MODAL);
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    @Listen("onRefresh=#windowView")
    public void onRefresh() {
        windowView.detach();

        ImportOrderFileViewDAO viewCosFileDAO = new ImportOrderFileViewDAO();
        VFileImportOrder vFileCosfile = viewCosFileDAO.findById(fileId);
        Events.sendEvent("onRefresh", parentWindow, vFileCosfile);
        LogUtils.addLog("on refresh view window!");
    }

    public boolean validateUserRight(NodeToNode action, Process processCurrent,
            Files file) {
        List<Process> listCurrentProcess = WorkflowAPI.getInstance().findAllCurrentProcess(file.getFileId(), file.getFileType(),
                file.getStatus());
        Long userId = getUserId();
        Long creatorId = file.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return true;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                for (Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                if (!needToProcess) {
                    return true;
                }
            }
        }
        return false;
    }

    @Listen("onAfterRender = #lbProductType")
    public void onAfterRenderProductType() {
        for (int i = 0; i < lbProductType.getItemCount(); i++) {
            Listitem item = lbProductType.getItemAtIndex(i);
            Listcell cell = (Listcell) item.getChildren().get(0);
            CosProductTypeSubModel cos = item.getValue();
            List<CosProductTypeSub> lstCosProductTypeSub = cos.getLstCosProductTypeSub();
            if (lstCosProductTypeSub != null && lstCosProductTypeSub.size() > 0) {
                for (CosProductTypeSub obj : lstCosProductTypeSub) {
                    Label lb = new Label("- " + obj.getNameVi());
                    lb.setClass("product-type-sub");
                    lb.setStyle("font-style: normal !important;");
                    Label enLb = new Label(obj.getNameEn());
                    enLb.setClass("product-type-sub-en");
                    Br br = new Br();
                    cell.appendChild(br);
                    cell.appendChild(lb);
                    cell.appendChild(new Br());
                    cell.appendChild(enLb);
                }
            }
        }
        lbProductType.renderAll();
        loadProductTypeCheck();
        disableListItem(lbProductType);
    }

    private void loadProductPresentationCheck() {
    }

    private void loadProductTypeCheck() {
    }

    public String getNameLink(Long id) {
        return "123\n\r456";
    }

    public void setValueProductType(Textbox textbox) {
        Label label = new Label();
        label.setValue("123");
        Br br = new Br();
        textbox.getParent().appendChild(br);
        textbox.getParent().appendChild(label);

    }

    /**
     * linhdx Hàm load danh sach trong dropdown control
     *
     * @param type
     * @return
     */
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case RAPID_TEST_FILE_TYPE:
                categoryDAOHE = new CategoryDAOHE();
                listCosmeticFileType = categoryDAOHE.findAllCategory(Constants.CATEGORY_TYPE.COSMETIC_FILE_TYPE);
                lstModel = new ListModelList(listCosmeticFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    @Listen("onDownloadFinalFile = #finalFileListboxPro")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    /**
     * Dong cua so khi o dang popup
     */
    @Listen("onClose = #windowView")
    public void onClose() {
        windowView.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
    }

    public void onDownloadPermit() throws FileNotFoundException {
        CosPermitDAO cosPermitDAO = new CosPermitDAO();
        CosPermit permit = cosPermitDAO.findLastByFileId(fileId);

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> lstAttach = attDAOHE.getByObjectIdAndType(
                permit.getPermitId(), Constants.OBJECT_TYPE.COSMETIC_PERMIT);
        Attachs att = null;
        if (lstAttach != null && lstAttach.size() > 0) {
            att = lstAttach.get(0);

        }
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    public void onDownloadReject() throws FileNotFoundException {
        CosRejectDAO cosRejectDAO = new CosRejectDAO();
        reject = cosRejectDAO.findLastByFileId(fileId);

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> lstAttach = attDAOHE.getByObjectIdAndType(
                reject.getRejectId(),
                Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH);
        Attachs att = null;
        if (lstAttach != null && lstAttach.size() > 0) {
            att = lstAttach.get(0);

        }
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    public int checkListSDBS() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    public int checkDispathSDBS() {
        return checkDispathSDBS(files.getFileId());
    }

    public int checkDispathReject() {
        return checkDispathReject(files.getFileId());
    }

    public int checkBooked() {
        return checkBooked(files.getFileId(), files.getFileType());
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public ListModel getModel() {
        return model;
    }

    public void setModel(ListModel model) {
        this.model = model;
    }

    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getIsSave() {
        return isSave;
    }

    public void setIsSave(String isSave) {
        this.isSave = isSave;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    public CosAdditionalRequest getAdditionalRequest() {
        return additionalRequest;
    }

    public void setAdditionalRequest(CosAdditionalRequest additionalRequest) {
        this.additionalRequest = additionalRequest;
    }

    public BookDocument getBookDocument() {
        return bookDocument;
    }

    public void setBookDocument(BookDocument bookDocument) {
        this.bookDocument = bookDocument;
    }

    public int checkEvaluation() {
        return checkEvaluation(files.getFileId());
    }

    public int checkPermit() {
        return checkPermit(files.getFileId());
    }

    public int checkPayment() {
        return checkPayment(files.getFileId());
    }

    public Listbox getLbListEvaluation() {
        return lbListEvaluation;
    }

    public void setLbListEvaluation(Listbox lbListEvaluation) {
        this.lbListEvaluation = lbListEvaluation;
    }

    public CosEvaluationRecord getEvaluationRecord() {
        return evaluationRecord;
    }

    public void setEvaluationRecord(CosEvaluationRecord evaluationRecord) {
        this.evaluationRecord = evaluationRecord;
    }

    public CosPermit getPermit() {
        return permit;
    }

    public void setPermit(CosPermit permit) {
        this.permit = permit;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public CosReject getReject() {
        return reject;
    }

    public void setReject(CosReject reject) {
        this.reject = reject;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Boolean Checkingredient(CosCosfileIngre cosCosfileIngre,
            String hidden) {
        for (int i = 0; i < ingredientListCheck.size(); i++) {
            Long b = Long.parseLong(cosCosfileIngre.getCosfileIngreId().toString());
            Long a = Long.parseLong(ingredientListCheck.get(i).toString());
            if (a.equals(b)) {
                if ("1".equals(hidden)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        if ("1".equals(hidden)) {
            return false;
        } else {
            return true;
        }
    }

    @Listen("onOpenView = #incList #lbCosfileIngreCheck")
    public void onOpenViewCheck(Event event) {
        CosCosfileIngre cosfileingre = (CosCosfileIngre) event.getData();
        reloadModel(cosfileingre.getIngredientName());
        txtSubstance.setText(cosfileingre.getIngredientName());

    }

    @Listen("onClick= #incList #btnSearch")
    public void onClickbtnSearch() {
        userPagingBottom.setActivePage(0); 	
        reloadModel(txtSubstance.getText());
    }

    public void reloadModel(String text) {
        AnnexeDAO annexeDAO = new AnnexeDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage()
                * userPagingBottom.getPageSize();
        PagingListModel plm;
        plm = annexeDAO.findBySubstance(text, take, start);
        userPagingBottom.setTotalSize(plm.getCount());
        lstAnnexe = plm.getLstReturn();
        lbAnnexe.setModel(new ListModelArray(lstAnnexe));
    }

    @Listen("onPaging = #incList #userPagingBottom")
    public void onPaging(Event event) {

        reloadModel(txtSubstance.getText());
    }

    // ==tichnv 18/04/2014
    @Listen("onExportFile = #windowView")
    public void onExportFile() throws JAXBException {
        ExportOrderFileModel exportModel = new ExportOrderFileModel(cosFile.getFileId());
        ExportOrderFileDAO exp = new ExportOrderFileDAO();
        exp.exportDeviceFile(exportModel, true);
    }

    public int checkFee() {
        PaymentInfoDAO a = new PaymentInfoDAO();
        List<PaymentInfo> lsPaymentInfo = a.getListFeePayment(fileId);
        if (lsPaymentInfo.size() > 0) {
            return 1;
        }
        return 0;
    }

    public void fillFee(Long fileId2) {
        PaymentInfoDAO a = new PaymentInfoDAO();
        List<PaymentInfo> lsPaymentInfo1 = a.getListFeePayment(fileId2);
        this.lFillFee.setModel(new ListModelList(lsPaymentInfo1));
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VimportOrderFileAttach obj = (VimportOrderFileAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    private void fillFileListbox(Long obj_type, Long obj_id) {
        VAttfileCategoryDAO dao = new VAttfileCategoryDAO();
        List<VAttfileCategory> lstCosmeticAttach = dao.findCheckedFilecAttach(obj_type, obj_id);
        if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS.equals(obj_type)) {
            //lấy thêm file kiểm tra 
            List<VAttfileCategory> temp_list = dao.findCheckedFilecAttach(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, fileId);
            lstCosmeticAttach.addAll(temp_list);
            fileListboxOrderFile.setModel(new ListModelArray(lstCosmeticAttach));
        } else if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST.equals(obj_type)) {
            fileListboxOrderReturn.setModel(new ListModelArray(lstCosmeticAttach));
        }

    }

    private void fillDuplicateFilecheckTransport(String transportNo, Long fileId) {
        ImportOrderFileViewDAO importOrderDAO = new ImportOrderFileViewDAO();
        List<VFileImportOrder> lstDuplicate = importOrderDAO.findOtherFileByTransportNo(transportNo, fileId);
        if (lstDuplicate != null && lstDuplicate.size() > 0) {
            //fileListboxDuplicateTransportNo.setModel(new ListModelArray(lstDuplicate));
            fileListboxDuplicateTransportNoFlag = "1";

        }

    }

    private void checkDuplicateFilecheckTransport(String transportNo, Long fileId) {
        ImportOrderFileViewDAO importOrderDAO = new ImportOrderFileViewDAO();
        List<VFileImportOrder> lstDuplicate = importOrderDAO.findOtherFileByTransportNo(transportNo, fileId);
        if (lstDuplicate != null && lstDuplicate.size() > 0) {
            //fileListboxDuplicateTransportNo.setModel(new ListModelArray(lstDuplicate));
            fileListboxDuplicateTransportNoFlag = "1";

        }
    }

    //tải tệp đính kèm của một sản phẩm
    @Listen("onDownloadFileListboxOrderReturn = #fileListboxOrderReturn")
    public void onDownloadFileListboxOrderReturn(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    //tải tệp đính kèm của cả hồ sơ  
    @Listen("onDownloadOrderFile =#fileListboxOrderFile")
    public void onDownloadOrderFile(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    public String getFileListboxDuplicateTransportNoFlag() {
        return fileListboxDuplicateTransportNoFlag;
    }

    public void setFileListboxDuplicateTransportNoFlag(String fileListboxDuplicateTransportNoFlag) {
        this.fileListboxDuplicateTransportNoFlag = fileListboxDuplicateTransportNoFlag;
    }
}
