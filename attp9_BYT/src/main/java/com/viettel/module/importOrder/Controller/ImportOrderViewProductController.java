package com.viettel.module.importOrder.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

import com.viettel.core.workflow.BusinessController;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.ProductProductTarget;
import com.viettel.module.importOrder.BO.VimportOrderFileAttach;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.ProductProductTargetDAO;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Groupbox;

public class ImportOrderViewProductController extends BusinessController {

    private static final long serialVersionUID = 1L;
    @Wire
    private Window productWindow;
    @Wire
    private Groupbox gbAttachs, gbTargets;
    @Wire
    private Listbox lbproductarget, fileListboxProduct;
    private List<Attachs> lsAtt = new ArrayList<>();
    private ImportOrderProduct product;
    private List<ProductProductTarget> lstTarget = new ArrayList<>();

    @SuppressWarnings("rawtypes")
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> args = (Map<String, Object>) Executions.getCurrent().getArg();
        Long productId = (Long) args.get("productId");
        product = (new ImportOrderProductDAO()).findById(productId);
        return super.doBeforeCompose(page, parent, compInfo);

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> args = (Map<String, Object>) Executions.getCurrent().getArg();
        gbAttachs.setVisible(false);
        gbTargets.setVisible(false);
        Long productId = (Long) args.get("productId");
        AttachDAOHE attDAO = new AttachDAOHE();
        lsAtt.addAll(attDAO.getByObjectId(productId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST));
        lsAtt.addAll(attDAO.getByObjectId(productId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS));
        lsAtt.addAll(attDAO.getByObjectId(productId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS));
        lsAtt.addAll(attDAO.getByObjectId(productId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPORT));
        ProductProductTargetDAO dao = new ProductProductTargetDAO();
        lstTarget = dao.findByProductId(productId);
        if (lstTarget != null && lstTarget.size() > 0) {
            gbTargets.setVisible(true);
            fillFileListbox();
        }
        if (lsAtt != null && lsAtt.size() > 0) {
            gbAttachs.setVisible(true);
            fillAttListbox();
        }
    }

    public ImportOrderProduct getProduct() {
        return product;
    }

    public void setProduct(ImportOrderProduct product) {
        this.product = product;
    }

    private void fillFileListbox() {
        ListModelArray array = new ListModelArray(lstTarget);
        this.lbproductarget.setModel(array);
    }

    private void fillAttListbox() {
        ListModelArray array = new ListModelArray(lsAtt);
        fileListboxProduct.setModel(array);
    }

     @Listen("onDownloadFile = #fileListboxProduct")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }
    public String getCatName(Long Id) {
        String rs = "";
        if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST.equals(Id)) {
            rs = "File kiểm nghiệm";
        } else if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS.equals(Id)) {
            rs = "File xử lý sản phẩm không đạt";
        } else if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS.equals(Id)) {
            rs = "File kết quả xử lý sản phẩm không đạt";
        } else if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPORT.equals(Id)) {
            rs = "File báo cáo kết quả xử lý sản phẩm không đạt";
        }
        return rs;
    }
}
