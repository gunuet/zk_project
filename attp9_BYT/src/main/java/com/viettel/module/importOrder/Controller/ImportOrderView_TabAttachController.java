/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import com.viettel.module.cosmetic.Controller.*;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Model.CosProductTypeSubModel;
import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;

import java.io.FileNotFoundException;

import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Br;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;

import com.viettel.core.workflow.BO.Process;
import com.viettel.module.cosmetic.BO.*;
import com.viettel.module.cosmetic.DAO.*;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.importOrder.DAO.ImportOrderAttachDao;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.VAttfileCategoryDAO;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.VAttfileCategory;
import com.viettel.module.importOrder.BO.VimportOrderFileAttach;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;

import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Objects;

import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ExportOrderFileDAO;
import com.viettel.module.importOrder.Model.ExportOrderFileModel;

import javax.xml.bind.JAXBException;

/**
 *
 * @author giangnh20
 */
public class ImportOrderView_TabAttachController extends CosmeticBaseController {

    private final int RAPID_TEST_FILE_TYPE = 1;
    private long FILE_TYPE;
    private long DEPT_ID;
    @Wire
    private Listbox fileListbox;
    @Wire
    private Listbox finalFileListbox;

    private Long fileId;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fileId = Long.valueOf(Executions.getCurrent().getParameter("fileId"));

        fillFileListbox(fileId);
        fillFinalFileListbox(fileId);

    }

    private void fillFileListbox(Long fileId) {
        VAttfileCategoryDAO dao = new VAttfileCategoryDAO();
        List<VAttfileCategory> lstCosmeticAttach = dao.findCheckedFilecAttach(
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE, fileId);
        ListModelArray array = new ListModelArray(lstCosmeticAttach);
        this.fileListbox.setModel(array);
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    private void fillFinalFileListbox(Long fileId) {
        VAttfileCategoryDAO dao = new VAttfileCategoryDAO();
        List<VAttfileCategory> lstAttach = dao.findCheckedFilecAttach(
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, fileId);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

}
