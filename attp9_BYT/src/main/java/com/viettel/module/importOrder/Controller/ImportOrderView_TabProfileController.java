/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import com.viettel.module.cosmetic.Controller.*;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Model.CosProductTypeSubModel;
import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;

import java.io.FileNotFoundException;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Br;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;

import com.viettel.core.workflow.BO.Process;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.module.cosmetic.BO.*;
import com.viettel.module.cosmetic.DAO.*;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.importOrder.DAO.ImportOrderAttachDao;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.VimportOrderFileAttach;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;

import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Objects;

import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ExportOrderFileDAO;
import com.viettel.module.importOrder.Model.ExportOrderFileModel;

import javax.xml.bind.JAXBException;

/**
 *
 * @author giangnh20
 */
public class ImportOrderView_TabProfileController extends
        CosmeticBaseController {

    // private final int RAPID_TEST_FILE_TYPE = 1;
    // private long FILE_TYPE;
    // private long DEPT_ID;
    //
    // // private String nswFileCode;
    //
    // @Wire
    // private Div divToolbarBottom;
    // private List<Component> listTopActionComp;
    // @Wire
    // private Div divToolbarTop;
    // private List<Component> listBottomActionComp;
    // @Wire
    // private Vlayout flist;
    // private List<Media> listMedia;
    // private List listCosmeticFileType;
    // @Wire
    // private Window windowView;
    // private Window parentWindow;
    // private String isSave;
    // private ListModel model;
    private VFileImportOrder cosFile = new VFileImportOrder();
    private Files files = new Files();
    @Wire
    private Textbox tbStatus;
    @Wire
    private Label lbStatus;

    // Long documentTypeCode;
    // @Wire
    // private Listbox
    // finalFileListboxPro,finalFileListboxPro2,finalFileListboxProKTKN,finalFileListboxProregister,
    // finalFileListboxProductgiam,finalFileListboxProSDBS;
    // @Wire
    // private Listbox lbManufacturer;
    // private List<CosManufacturer> lstManufacturer = new ArrayList();
    // @Wire
    // private Listbox lbAssembler;
    // @Wire
    // Checkbox cbAssemblerMain, cbAssemblerSub, cbIngreConfirm1,
    // cbIngreConfirm2;
    // private List<CosAssembler> lstAssembler = new ArrayList();
    // @Wire
    // private Listbox lbCosfileIngre;
    //
    // @Wire("#incList #lbCosfileIngreCheck")
    // private Listbox lbCosfileIngreCheck;
    //
    // @Wire("#incList #lbAnnexe")
    // private Listbox lbAnnexe;
    //
    // private List<CosCosfileIngre> lstCosfileIngre = new ArrayList();
    // @Wire
    // private Listbox fileListbox, lbProductPresentation, lbProductType,
    // finalFileListbox;
    // @Wire("#incListEvaluation #lbListEvaluation")
    // private Listbox lbListEvaluation;
    // @Wire("#incList #txtSubstance")
    // Textbox txtSubstance;
    // private List ingredientListCheck, lstAnnexe;
    // // private VFileCosfile vFileCosfile;
    // private com.viettel.core.workflow.BO.Process processCurrent;// process
    // dang
    // // duoc xu li
    // private Integer menuType;
    private Long fileId;
    // private CosAdditionalRequest additionalRequest;
    // private BookDocument bookDocument = new BookDocument();
    // private CosEvaluationRecord evaluationRecord = new CosEvaluationRecord();
    // private CosPermit permit = new CosPermit();
    // private PaymentInfo paymentInfo = new PaymentInfo();
    // private CosReject reject = new CosReject();
    // @Wire("#incList #userPagingBottom")
    // private Paging userPagingBottom;
    //
    // @Wire
    // private Div divDispath;
    // @Wire
    // private Div divDispathReject;
    // @Wire
    // private Tabpanel tabpanel1;
    // private ImportOrderFile importOrderFile;
    //
    @Wire
    Listbox lbOrderProduct;

    //
    // List<NodeToNode> lstNextAction;
    //
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        // Map<String, Object> arguments = (Map)
        // Executions.getCurrent().getArg();
        // parentWindow = (Window) arguments.get("parentWindow");
        // listMedia = new ArrayList();
        //
        // WorkflowAPI w = new WorkflowAPI();
        // FILE_TYPE = w.getProcedureTypeIdByCode("HS_IMPORT_REQ");
        // DEPT_ID = Constants.CUC_ATTP_XNN_ID;
        // documentTypeCode =
        // Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        //
        fileId = Long.valueOf(Executions.getCurrent().getParameter("fileId"));
        files = (new FilesDAOHE()).findById(fileId);
        //
        // Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);

        //
        // // load ho so
        // documentTypeCode =
        // Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        // // nswFileCode = getAutoNswFileCode(documentTypeCode);
        //
        // processCurrent = WorkflowAPI.getInstance().getCurrentProcess(
        // files.getFileId(), files.getFileType(), files.getStatus(),
        // getUserId());
        // menuType = (Integer) arguments.get("menuType");
        //
        // // Xem qua trinh xu ly
        // setProcessingView(files.getFileId(), files.getFileType());
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO()
                .findAllIdByFileId(fileId);
        ListModelArray<ImportOrderProduct> lstModelManufacturer = new ListModelArray<ImportOrderProduct>(
                importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);

        // fileId = Long.valueOf(lbFileId.getValue());
        //
        // files = (new FilesDAOHE()).findById(fileId);
        //
        // cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
        // Map<String, Object> arguments = (Map)
        // Executions.getCurrent().getArg();
        // listTopActionComp = new ArrayList<>();
        // listBottomActionComp = new ArrayList<>();
        // // loadItemForEdit();
        // String statusStr = "Mới tạo";
        // Long status = files.getStatus();
        // if (status != null) {
        // statusStr = getStatus(status);
        // }
        //
        // fillFileListbox(cosFile.getFileId());
        // fillFinalFileListbox(cosFile.getFileId());
        // CosManufacturerDAO cmdao = new CosManufacturerDAO();
        // addAllNextActions();
        // if (menuType != null) {
        // loadActionsToToolbar(menuType, files, processCurrent, windowView);
        // }
        // if(checkListOK() == 1){
        // fillFinalFileRegister(fileId);
        // }
        //
        // if(checkAmendment() == 1){
        // fillFinalFileSDBX();
        // }
        //
        // if(checkKTKN() == 1){
        // fillFinalListboxProKTKN(fileId);
        // }
        // if(checkReduced() == 1){
        // fillFinalListboxProductgiam(fileId);
        // }
        //
        // // fillFinalFileListbox2(fileId);
        // // load produc
        // List<ImportOrderProduct> importOrderProducts = new
        // ImportOrderProductDAO().findAllIdByFileId(fileId);
        // ListModelArray lstModelManufacturer = new ListModelArray(
        // importOrderProducts);
        // lbOrderProduct.setModel(lstModelManufacturer);
    }

    // //Yeu cau bo xung
    // private void fillFinalFileSDBX() {
    //
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    //
    // List<Attachs>attachs=new ArrayList<Attachs>();
    // AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
    // List<AdditionalRequest> lstAdd =
    // additionalRequestDAO.findAllActiveByFileId(fileId);
    // // get file attach
    // if (lstAdd.size() > 0) {
    // AdditionalRequest additionalRequest = lstAdd.get(0);
    // List<Attachs> lstAttach2 =
    // rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
    // additionalRequest.getAdditionalRequestId(),
    // Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
    // Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
    // attachs.addAll(lstAttach2);
    // }
    // this.finalFileListboxPro2.setModel(new ListModelArray(attachs));
    // // TODO Auto-generated method stub
    //
    // }
    // public int checkAmendment(){
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    // AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
    // List<AdditionalRequest> lstAdd = additionalRequestDAO
    // .findAllActiveByFileId(fileId);
    // // get file attach
    // if (lstAdd.size() > 0) {
    // AdditionalRequest additionalRequest = lstAdd.get(0);
    // List<Attachs> lstAttach = rDaoHe
    // .getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
    // additionalRequest.getAdditionalRequestId(),
    // Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
    // Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
    // if(lstAttach.size() > 0){
    // return 1;
    // }
    // }
    // return 0;
    // }
    // //IMPORT_ORDER_FILE_REDUCED_PAPER. giay kiem tra giam
    // private void fillFinalListboxProductgiam(Long fileId2) {
    //
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    // List<Attachs> lstAttach4 = rDaoHe
    // .getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId2,
    // Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
    // Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
    // // TODO Auto-generated method stub
    //
    // this.finalFileListboxProductgiam.setModel(new
    // ListModelArray(lstAttach4));
    //
    // }
    // public int checkReduced(){
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    // List<Attachs> lstAttach = rDaoHe
    // .getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId,
    // Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
    // Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
    // if(lstAttach.size() > 0){
    // return 1;
    // }
    // return 0;
    //
    // }
    // @Listen("onDownloadFinalFile = #finalFileListboxPro2, #finalFileListboxProKTKN, #finalFileListboxProductgiam, #finalFileListboxProregister")
    // public void onDownloadFinalFileALL(Event event) throws
    // FileNotFoundException {
    // Attachs obj = (Attachs) event.getData();
    // AttachDAO attDAO = new AttachDAO();
    // attDAO.downloadFileAttach(obj);
    //
    // }
    // //IMPORT_ORDER_FILE_PAPER
    // private void fillFinalListboxProKTKN(Long fileId2) {
    //
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    // List<Attachs> lstAttach =
    // rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId2,
    // Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
    // Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
    // // TODO Auto-generated method stub
    // this.finalFileListboxProKTKN.setModel(new ListModelArray(lstAttach));
    // }
    // public int checkKTKN(){
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    //
    // List<Attachs> lstAttach =
    // rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId,
    // Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
    // Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
    // if(lstAttach.size() > 0){
    // return 1;
    // }
    // return 0;
    // }
    //
    //
    // //IMPORT_ORDER_FILE_REGISTERED_PAPER giay cong bo
    // private void fillFinalFileRegister(Long fileId2) {
    //
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    // List<Attachs> lstAttach3 =
    // rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
    // fileId2,
    // Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
    // Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
    //
    // this.finalFileListboxProregister.setModel(new
    // ListModelArray(lstAttach3));
    // // TODO Auto-generated method stub
    //
    // }
    // public int checkListOK(){
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    //
    // List<Attachs> lstAttach =
    // rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
    // fileId,
    // Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
    // Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
    // if(lstAttach.size() > 0){
    // return 1;
    // }
    // return 0;
    // }
    //
    // public void loadActionsToToolbar(int menuType, final Files files,
    // final com.viettel.core.workflow.BO.Process currentProcess,
    // final Window currentWindow) {
    //
    // for (Component comp : listTopActionComp) {
    // divToolbarTop.appendChild(comp);
    // }
    // for (Component comp : listBottomActionComp) {
    // divToolbarBottom.appendChild(comp);
    // }
    //
    // }
    //
    // public void setProcessingView(Long fileId, Long fileType) {
    // if (fileId != null && fileType != null) {
    //
    // // linhdx 13032015 Load danh sach yeu cau sdbs
    // BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
    // BookDocument bookDocument2 = bookDocumentDAOHE
    // .getBookInFromDocumentId(fileId, fileType);
    // if (bookDocument2 != null) {
    // bookDocument = bookDocument2;
    // }
    //
    // // linhdx 13032015 Load danh sach yeu cau sdbs
    // CosAdditionalRequestDAO cosAdditionalRequestDAO = new
    // CosAdditionalRequestDAO();
    // List<CosAdditionalRequest> lstAddition = cosAdditionalRequestDAO
    // .findAllActiveByFileId(fileId);
    // if (lstAddition != null && lstAddition.size() > 0) {
    // additionalRequest = lstAddition.get(0);
    // }
    //
    // CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
    // evaluationRecord = dao.getLastEvaluation(fileId);
    //
    // CosPermitDAO cosPermitDAO = new CosPermitDAO();
    // List<CosPermit> lstPermit = cosPermitDAO
    // .findAllActiveByFileId(fileId);
    // if (lstPermit != null && lstPermit.size() > 0) {
    // permit = lstPermit.get(0);
    // }
    //
    // CosRejectDAO cosRejectDAO = new CosRejectDAO();
    // List<CosReject> lstReject = cosRejectDAO
    // .findAllActiveByFileId(fileId);
    // if (lstReject != null && lstReject.size() > 0) {
    // reject = lstReject.get(0);
    // }
    //
    // PaymentInfoDAO paymentInfoDAO = new PaymentInfoDAO();
    // List<PaymentInfo> lstPayment = paymentInfoDAO.getListPayment(
    // fileId, Constants.PAYMENT.PHASE.EVALUATION);
    // if (lstPayment != null && lstPayment.size() > 0) {
    // paymentInfo = lstPayment.get(0);
    // }
    //
    // }
    //
    // }
    //
    // @Listen("onViewFlow = #windowView")
    // public void onViewFlow(Event event) {
    // Map args = new ConcurrentHashMap();
    // Long docId = files.getFileId();
    // Long docType = files.getFileType();
    // args.put("objectId", docId);
    // args.put("objectType", docType);
    // createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
    // args, Window.MODAL);
    // }
    //
    // show product
    @Listen("onShow =  #lbOrderProduct")
    public void onShow(Event event) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        ImportOrderProduct obj = (ImportOrderProduct) event.getData();
        Long productId = obj.getProductId();
        arguments.put("productId", productId);
        createWindow("showProduct",
                "/Pages/module/importorder/importOrderViewProduct.zul",
                arguments, Window.HIGHLIGHTED);
    }

    // // @Listen("onClick =#onShow")
    // // public void onShowProduct(Event event) {
    // // Listitem listitem = (Listitem) event.getTarget();
    // // ImportOrderProduct importOrderProduct = (ImportOrderProduct)
    // listitem.getValue();
    // // Map<String, Object> arguments = new ConcurrentHashMap<>();
    // // arguments.put("producId", importOrderProduct.getProductId());
    // //
    // createWindow("showProduct","/Pages/module/importorder/importOrderViewProduct.zul",
    // // arguments, Window.HIGHLIGHTED);
    // // }
    //
    // /**
    // * Đinh kem ho so goc
    // */
    // @Listen("onClick = #btnOpenAttachFileFinal")
    // public void onCreateAttachFileFinal() {
    // Map<String, Object> arguments = setArgument();
    // createWindow("wdAttachFinalFileCRUD",
    // "/Pages/module/cosmetic/attachFinalFileCRUD.zul", arguments,
    // Window.POPUP);
    // }
    //
    // private Map<String, Object> setArgument() {
    // Map<String, Object> arguments = new ConcurrentHashMap<>();
    // // arguments.put("CRUDMode", "CREATE");
    // // arguments.put("fileId", files.getFileId());
    // // arguments.put("cosFileId", cosFile.getCosFileId());
    // // arguments.put("parentWindow", windowView);
    // return arguments;
    // }
    //
    // // // Mở popup xử lý nghiệp vụ
    // // // Trường hợp khởi tạo luồng
    // // @Listen("onActiveFlow = #windowView")
    // // public void onActiveFlow() {
    // // Map<String, Object> arguments = new ConcurrentHashMap<>();
    // // arguments.put("docId", cosFile.getCosFileId());
    // // arguments.put("docType", cosFile.getDocumentTypeCode());
    // // Long status = files.getStatus();
    // // if (status == null) {
    // // status = 0l;
    // // }
    // // arguments.put("docStatus", status);
    // //
    // // // form nghiep vu tuong ung voi action
    // // String formName;
    // // if (status == 3 || status == 0) {
    // // formName = "/Pages/module/rapidtest/index.zul";
    // // } else if (status == 4) {
    // // formName = "/Pages/module/rapidtest/approveDoc.zul";
    // // } else {
    // // formName = "/Pages/module/rapidtest/processDoc.zul";
    // // }
    // // createWindow("windowActiveFlow", formName, arguments, Window.MODAL);
    // // }
    // public void setListEvaluation() {
    // // lay danh sach cac tham dinh
    // if (checkEvaluation() == 1) {
    // CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
    // List<CosEvaluationRecord> lstCosEvaluationRecord = dao
    // .getAllEvaluation(files.getFileId());
    // if (lbListEvaluation != null) {
    // lbListEvaluation.setModel(new ListModelArray(
    // lstCosEvaluationRecord));
    // }
    //
    // }
    // }
    //
    // @Listen("onOpenView = #incListEvaluation #lbListEvaluation")
    // public void onOpenView(Event event) {
    // CosEvaluationRecord evaluationRecord = (CosEvaluationRecord) event
    // .getData();
    // Map<String, Object> arguments = new ConcurrentHashMap<>();
    // arguments.put("evaluationRecord", evaluationRecord);
    // Window window = createWindow("windowViewEvalution",
    // "/Pages/module/cosmetic/viewEvaluation.zul", arguments,
    // Window.POPUP);
    // window.doModal();
    //
    // }
    //
    // @Listen("onAfterRender = #lbProductPresentation")
    // public void onAfterRenderProductPresentation() {
    // loadProductPresentationCheck();
    // disableListItem(lbProductPresentation);
    // }
    //
    // /**
    // * Ham disable list item sau khi render Neu set trong zul thi nhung truong
    // * checked khong hien thi
    // *
    // * @param lb
    // */
    // public void disableListItem(Listbox lb) {
    // if (lb != null) {
    // for (Listitem item : lb.getItems()) {
    // item.setDisabled(true);
    // }
    // }
    // }
    //
    // private void addAllNextActions() {
    // // linhdx
    // Long docStatus = files.getStatus();
    // Long docId = files.getFileId();
    // Long userId = getUserId();
    // Long docType = FILE_TYPE;
    // Long deptId = DEPT_ID;
    // List<Process> lstProcess = WorkflowAPI.getInstance().getProcess(docId,
    // docType, userId);
    // List<Process> lstAllProcess = WorkflowAPI.getInstance().getAllProcessNotFinish(
    // docId, docType);
    //
    // List<Long> lstStatus = new ArrayList();
    // // linhdx tim nhung trang thai chua hoan thanh xu ly de tim action tiep
    // // theo
    // for (Process obj : lstProcess) {
    // if (obj.getFinishDate() == null) {
    // lstStatus.add(obj.getStatus());
    // }
    // }
    // // Neu khong co add flag -1 de bao la khong phai xu ly nua
    // if (lstStatus.isEmpty() && !lstProcess.isEmpty()) {
    // lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
    // }
    //
    // if (lstStatus.isEmpty() && !lstAllProcess.isEmpty()) {
    // lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
    // }
    //
    // // List<Long> lstStatus =
    // // WorkflowAPI.getInstance().getProcessStatus(docId, docType, userId);
    //
    // // if(lstStatus.isEmpty()){
    // // lstStatus.add(docStatus);
    // // }
    // List<NodeToNode> actions = WorkflowAPI.getInstance()
    // .findAvaiableNextActions(docType, lstStatus, deptId);
    // lstNextAction = actions;// linhdx
    // for (NodeToNode action : actions) {
    // Button temp = createButtonForAction(action);
    // if (temp != null) {
    // listTopActionComp.add(temp);
    // }
    // }
    // }
    //
    // private void removeAllNextActions() {
    // List<Component> listChildren = divToolbarTop.getChildren();
    // if (listChildren != null && !listChildren.isEmpty()) {
    // for (Component comp : listChildren) {
    // if ("Button".equals(comp.getClass().getName())) {
    // divToolbarTop.removeChild(comp);
    // }
    // }
    // }
    // }
    //
    // // viethd3: create new button for a processing action
    // private Button createButtonForAction(NodeToNode action) {
    // // linhdx
    // // if (validateUserRight(action, processCurrent, files)) {
    // // return null;
    // // }
    //
    // Button btn = new Button(action.getAction());
    //
    // final String actionName = action.getAction();
    // final Long actionId = action.getId();
    // final Long actionType = action.getType();
    // final Long nextId = action.getNextId();
    // final Long prevId = action.getPreviousId();
    // // form nghiep vu tuong ung voi action
    // final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
    // final Long status = files.getStatus();
    // final List<NodeToNode> lstNextAction1 = lstNextAction;
    //
    // final List<NodeDeptUser> lstNDUs = new ArrayList<>();
    // lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId));
    //
    // EventListener<Event> event = new EventListener<Event>() {
    // @Override
    // public void onEvent(Event t) throws Exception {
    // Map<String, Object> data = new ConcurrentHashMap<>();
    // data.put("fileId", files.getFileId());
    // data.put("docId", files.getFileId());
    // data.put("docType", files.getFileType());
    // data.put("docStatus", status);
    // data.put("actionId", actionId);
    // data.put("actionName", actionName);
    // data.put("actionType", actionType);
    // data.put("nextId", nextId);
    // data.put("previousId", prevId);
    // if (processCurrent != null) {
    //                data.put("process", processCurrent);
    //            }
    // data.put("lstAvailableNDU", lstNDUs);
    // data.put("parentWindow", windowView);
    // data.put("lstNextAction", lstNextAction1);
    // createWindow("windowComment", formName, data, Window.MODAL);
    // }
    // };
    //
    // btn.addEventListener(Events.ON_CLICK, event);
    // return btn;
    // }
    //
    // @Listen("onRefresh=#windowView")
    // public void onRefresh() {
    // windowView.detach();
    //
    // ImportOrderFileViewDAO viewCosFileDAO = new ImportOrderFileViewDAO();
    // VFileImportOrder vFileCosfile = viewCosFileDAO.findById(fileId);
    // Events.sendEvent("onRefresh", parentWindow, vFileCosfile);
    // LogUtils.addLog("on refresh view window!");
    // }
    //
    // public boolean validateUserRight(NodeToNode action, Process
    // processCurrent,
    // Files file) {
    // // check if action is RETURN_PREVIOUS but parent process came from
    // // another node
    // // linhdx comment 20150405
    // // if (processCurrent != null
    // // && action.getType() == Constants.NODE_ASSOCIATE_TYPE.RETURN_PREVIOUS
    // // && action.getNextId() != processCurrent.getPreviousNodeId()) {
    // // return true;
    // // }
    // // check if receiverId is diferent to current user id
    // // find all process having current status of document
    // List<Process> listCurrentProcess = WorkflowAPI.getInstance()
    // .findAllCurrentProcess(file.getFileId(), file.getFileType(),
    // file.getStatus());
    // Long userId = getUserId();
    // Long creatorId = file.getCreatorId();
    // if (listCurrentProcess.isEmpty()) {
    // if (!userId.equals(creatorId)) {
    // return true;
    // }
    // } else {
    // if (processCurrent == null) {
    // boolean needToProcess = false;
    // for (Process p : listCurrentProcess) {
    // if (p.getReceiveUserId().equals(userId)) {
    // needToProcess = true;
    // }
    // }
    // if (!needToProcess) {
    // return true;
    // }
    // }
    // }
    // return false;
    // }
    //
    // @Listen("onAfterRender = #lbProductType")
    // public void onAfterRenderProductType() {
    // for (int i = 0; i < lbProductType.getItemCount(); i++) {
    // Listitem item = lbProductType.getItemAtIndex(i);
    // Listcell cell = (Listcell) item.getChildren().get(0);
    // CosProductTypeSubModel cos = item.getValue();
    // List<CosProductTypeSub> lstCosProductTypeSub = cos
    // .getLstCosProductTypeSub();
    // if (lstCosProductTypeSub != null && lstCosProductTypeSub.size() > 0) {
    // for (CosProductTypeSub obj : lstCosProductTypeSub) {
    // Label lb = new Label("- " + obj.getNameVi());
    // lb.setClass("product-type-sub");
    // lb.setStyle("font-style: normal !important;");
    // Label enLb = new Label(obj.getNameEn());
    // enLb.setClass("product-type-sub-en");
    // Br br = new Br();
    // cell.appendChild(br);
    // cell.appendChild(lb);
    // cell.appendChild(new Br());
    // cell.appendChild(enLb);
    // }
    // }
    // }
    // lbProductType.renderAll();
    // loadProductTypeCheck();
    // disableListItem(lbProductType);
    // }
    //
    // private void loadProductPresentationCheck() {
    // // String productPresentation = cosFile.getProductPresentation();
    // // if (productPresentation != null) {
    // // String[] lst = productPresentation.split(";");
    // // for (String idCosPresent : lst) {
    // // Long id = Long.valueOf(idCosPresent);
    // // List<Listitem> items = lbProductPresentation.getItems();
    // // for (Listitem item : items) {
    // // CosProductPresentaton cos = item.getValue();
    // // if (Objects.equals(cos.getProductPresentationId(), id)) {
    // // item.setSelected(true);
    // // }
    // //
    // // }
    // // }
    // // }
    //
    // }
    //
    // private void loadProductTypeCheck() {
    // // String productType = cosFile.getProductType();
    // // if (productType != null) {
    // // String[] lst = productType.split(";");
    // // for (String idCosPresent : lst) {
    // // Long id = Long.valueOf(idCosPresent);
    // // List<Listitem> items = lbProductType.getItems();
    // // for (Listitem item : items) {
    // // CosProductTypeSubModel cos = item.getValue();
    // // if (Objects.equals(cos.getProductTypeId(), id)) {
    // // item.setSelected(true);
    // // }
    // //
    // // }
    // // }
    // // }
    //
    // }
    //
    // private void loadItemForEdit() {
    // if (cosFile.getDocumentTypeCode() != null) {
    // documentTypeCode = cosFile.getDocumentTypeCode();
    // }
    //
    // CategoryDAOHE cdhe = new CategoryDAOHE();
    // List lstIngredient = cdhe
    // .findAllCategory(Constants.CATEGORY_TYPE.COS_INGREDIENT);
    // ListModelArray lstModelIngredient = new ListModelArray(lstIngredient);
    //
    // CosProductPresentatonDAO cppDAO = new CosProductPresentatonDAO();
    // List lstCosProductPresentation = cppDAO.getAllActivePlusDifferent();
    // ListModelArray lstModelPresentation = new ListModelArray(
    // lstCosProductPresentation);
    // lstModelPresentation.setMultiple(true);
    // lbProductPresentation.setModel(lstModelPresentation);
    //
    // CosProductTypeSubDAO cptsDAO = new CosProductTypeSubDAO();
    // List<CosProductTypeSub> lstCosProductTypeSub = cptsDAO
    // .getAllActiveOrderByProductType();
    // List<CosProductTypeSubModel> lstCosProductTypeSubModel = new ArrayList();
    // int i = 0;
    // CosProductTypeSub previeous = new CosProductTypeSub();
    // for (Object currentObject : lstCosProductTypeSub) {
    // CosProductTypeSub current = (CosProductTypeSub) currentObject;
    // // Neu ban ghi hien tai khong co productTypeId bang ban ghi cu thi
    // // add ban ghi moi
    // if (!Objects.equals(current.getProductTypeId(),
    // previeous.getProductTypeId())) {
    // CosProductTypeSubModel cModel = new CosProductTypeSubModel();
    // cModel.setProductTypeId(current.getProductTypeId());
    // cModel.setNameVi(current.getNameVi());
    // cModel.setNameEn(current.getNameEn());
    // cModel.setIsActive(current.getIsActive());
    // cModel.setNameLink("");
    // lstCosProductTypeSubModel.add(cModel);
    // i++;
    // } else {
    // // Neu giong thi chi add them link
    // CosProductTypeSubModel obj = lstCosProductTypeSubModel
    // .get(i - 1);
    // List<CosProductTypeSub> lstCosProductTypeSub2;
    // lstCosProductTypeSub2 = obj.getLstCosProductTypeSub();
    // if (lstCosProductTypeSub2 == null) {
    // lstCosProductTypeSub2 = new ArrayList<>();
    // }
    // lstCosProductTypeSub2.add(current);
    // obj.setLstCosProductTypeSub(lstCosProductTypeSub2);
    //
    // obj.setNameLink(obj.getNameLink() + "\r\n"
    // + current.getNameVi() + "(" + current.getNameEn() + ")");
    // }
    // previeous = current;
    // }
    //
    // CosProductTypeSubModel cModel = new CosProductTypeSubModel();
    // cModel.setProductTypeId(-1L);
    // cModel.setNameVi("Các dạng khác (Đề nghị ghi rõ)");
    // cModel.setNameEn("Others (please specify)");
    // cModel.setIsActive(1L);
    // cModel.setIsDifferent(1L);
    // cModel.setNameLink("");
    // lstCosProductTypeSubModel.add(cModel);
    // ListModelArray lstModelType = new ListModelArray(
    // lstCosProductTypeSubModel);
    // lstModelType.setMultiple(true);
    // lbProductType.setModel(lstModelType);
    //
    // }
    //
    // public String getNameLink(Long id) {
    // return "123\n\r456";
    // }
    //
    // public void setValueProductType(Textbox textbox) {
    // String id = textbox.getId();
    // String value = textbox.getValue();
    // Label label = new Label();
    // label.setValue("123");
    // Br br = new Br();
    // textbox.getParent().appendChild(br);
    // textbox.getParent().appendChild(label);
    //
    // }
    //
    // /**
    // * linhdx Hàm load danh sach trong dropdown control
    // *
    // * @param type
    // * @return
    // */
    // public ListModelList getListBoxModel(int type) {
    // CategoryDAOHE categoryDAOHE;
    // ListModelList lstModel;
    // switch (type) {
    // case RAPID_TEST_FILE_TYPE:
    // categoryDAOHE = new CategoryDAOHE();
    // listCosmeticFileType = categoryDAOHE
    // .findAllCategory(Constants.CATEGORY_TYPE.COSMETIC_FILE_TYPE);
    // lstModel = new ListModelList(listCosmeticFileType);
    // break;
    // }
    // return lstModel;
    //
    // }
    //
    // public int getSelectedIndexInModel(int type) {
    // int selectedItem = 0;
    // return selectedItem;
    // }
    //
    // private void fillFileListbox(Long fileId) {
    // ImportOrderFileDAO dao = new ImportOrderFileDAO();
    // List<VimportOrderFileAttach> lstCosmeticAttach = dao
    // .findImportOrderFilecAttach(fileId);
    // ListModelArray array = new ListModelArray(lstCosmeticAttach);
    // this.fileListbox.setModel(array);
    // }
    //
    // @Listen("onDownloadFile = #fileListbox")
    // public void onDownloadFile(Event event) throws FileNotFoundException {
    // VimportOrderFileAttach obj = (VimportOrderFileAttach) event.getData();
    // Long attachId = obj.getAttachId();
    // AttachDAOHE attDAOHE = new AttachDAOHE();
    // Attachs att = attDAOHE.findById(attachId);
    // AttachDAO attDAO = new AttachDAO();
    // attDAO.downloadFileAttach(att);
    //
    // }
    //
    // @Listen("onDeleteFile = #fileListbox")
    // public void onDeleteFile(Event event) {
    // VimportOrderFileAttach obj = (VimportOrderFileAttach) event.getData();
    // Long fileId = obj.getObjectId();
    // ImportOrderAttachDao rDAOHE = new ImportOrderAttachDao();
    // rDAOHE.delete(obj.getAttachId());
    // fillFileListbox(fileId);
    // }
    //
    // private void fillFinalFileListbox(Long fileId) {
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    // List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
    // Constants.OBJECT_TYPE.COSMETIC_HO_SO_GOC);
    // this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    // }
    //
    //
    // @Listen("onDownloadFinalFile = #finalFileListboxPro")
    // public void onDownloadFinalFile(Event event) throws FileNotFoundException
    // {
    // Attachs obj = (Attachs) event.getData();
    // AttachDAO attDAO = new AttachDAO();
    // attDAO.downloadFileAttach(obj);
    //
    // }
    //
    // /**
    // * Dong cua so khi o dang popup
    // */
    // @Listen("onClose = #windowView")
    // public void onClose() {
    // windowView.onClose();
    // Events.sendEvent("onVisible", parentWindow, null);
    // }
    //
    // public void onDownloadPermit() throws FileNotFoundException {
    // CosPermitDAO cosPermitDAO = new CosPermitDAO();
    // CosPermit permit = cosPermitDAO.findLastByFileId(fileId);
    //
    // AttachDAOHE attDAOHE = new AttachDAOHE();
    // List<Attachs> lstAttach = attDAOHE.getByObjectIdAndType(
    // permit.getPermitId(), Constants.OBJECT_TYPE.COSMETIC_PERMIT);
    // Attachs att = null;
    // if (lstAttach != null && lstAttach.size() > 0) {
    // att = lstAttach.get(0);
    //
    // }
    // AttachDAO attDAO = new AttachDAO();
    // attDAO.downloadFileAttach(att);
    //
    // }
    //
    // public void onDownloadReject() throws FileNotFoundException {
    // CosRejectDAO cosRejectDAO = new CosRejectDAO();
    // reject = cosRejectDAO.findLastByFileId(fileId);
    //
    // AttachDAOHE attDAOHE = new AttachDAOHE();
    // List<Attachs> lstAttach = attDAOHE.getByObjectIdAndType(
    // reject.getRejectId(),
    // Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH);
    // Attachs att = null;
    // if (lstAttach != null && lstAttach.size() > 0) {
    // att = lstAttach.get(0);
    //
    // }
    // AttachDAO attDAO = new AttachDAO();
    // attDAO.downloadFileAttach(att);
    //
    // }
    // public int checkListSDBS(){
    // AttachDAOHE rDaoHe = new AttachDAOHE();
    // List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
    // Constants.OBJECT_TYPE.COSMETIC_SDBS_DISPATH);
    // if(lstAttach.size() > 0){
    // return 1;
    // }
    // return 0;
    // }
    //
    // public int checkDispathSDBS() {
    // return checkDispathSDBS(files.getFileId());
    // }
    //
    // public int checkDispathReject() {
    // return checkDispathReject(files.getFileId());
    // }
    //
    // public int checkBooked() {
    // return checkBooked(files.getFileId(), files.getFileType());
    // }
    //
    // public int checkViewProcess() {
    // return checkViewProcess(files.getFileId());
    // }
    //
    // public ListModel getModel() {
    // return model;
    // }
    //
    // public void setModel(ListModel model) {
    // this.model = model;
    // }
    //
    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    // public String getIsSave() {
    // return isSave;
    // }
    //
    // public void setIsSave(String isSave) {
    // this.isSave = isSave;
    // }
    //
    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }
    //
    // public CosAdditionalRequest getAdditionalRequest() {
    // return additionalRequest;
    // }
    //
    // public void setAdditionalRequest(CosAdditionalRequest additionalRequest)
    // {
    // this.additionalRequest = additionalRequest;
    // }
    //
    // public BookDocument getBookDocument() {
    // return bookDocument;
    // }
    //
    // public void setBookDocument(BookDocument bookDocument) {
    // this.bookDocument = bookDocument;
    // }
    //
    // public int checkEvaluation() {
    // return checkEvaluation(files.getFileId());
    // }
    //
    // public int checkPermit() {
    // return checkPermit(files.getFileId());
    // }
    //
    // public int checkPayment() {
    // return checkPayment(files.getFileId());
    // }
    //
    // public Listbox getLbListEvaluation() {
    // return lbListEvaluation;
    // }
    //
    // public void setLbListEvaluation(Listbox lbListEvaluation) {
    // this.lbListEvaluation = lbListEvaluation;
    // }
    //
    // public CosEvaluationRecord getEvaluationRecord() {
    // return evaluationRecord;
    // }
    //
    // public void setEvaluationRecord(CosEvaluationRecord evaluationRecord) {
    // this.evaluationRecord = evaluationRecord;
    // }
    //
    // public CosPermit getPermit() {
    // return permit;
    // }
    //
    // public void setPermit(CosPermit permit) {
    // this.permit = permit;
    // }
    //
    // public PaymentInfo getPaymentInfo() {
    // return paymentInfo;
    // }
    //
    // public void setPaymentInfo(PaymentInfo paymentInfo) {
    // this.paymentInfo = paymentInfo;
    // }
    //
    // public CosReject getReject() {
    // return reject;
    // }
    //
    // public void setReject(CosReject reject) {
    // this.reject = reject;
    // }
    //
    // // public String getNswFileCode() {
    // // return nswFileCode;
    // // }
    // //
    // // public void setNswFileCode(String nswFileCode) {
    // // this.nswFileCode = nswFileCode;
    // // }
    //
    // public Boolean Checkingredient(CosCosfileIngre cosCosfileIngre,
    // String hidden) {
    // for (int i = 0; i < ingredientListCheck.size(); i++) {
    // Long b = Long.parseLong(cosCosfileIngre.getCosfileIngreId()
    // .toString());
    // Long a = Long.parseLong(ingredientListCheck.get(i).toString());
    // if (a.equals(b)) {
    // if ("1".equals(hidden)) {
    // return true;
    // } else {
    // return false;
    // }
    // }
    // }
    // if ("1".equals(hidden)) {
    // return false;
    // } else {
    // return true;
    // }
    // }
    //
    // @Listen("onOpenView = #incList #lbCosfileIngreCheck")
    // public void onOpenViewCheck(Event event) {
    // CosCosfileIngre cosfileingre = (CosCosfileIngre) event.getData();
    // reloadModel(cosfileingre.getIngredientName());
    // txtSubstance.setText(cosfileingre.getIngredientName());
    //
    // }
    //
    // @Listen("onClick= #incList #btnSearch")
    // public void onClickbtnSearch() {
    // userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
    // reloadModel(txtSubstance.getText());
    // }
    //
    // public void reloadModel(String text) {
    // AnnexeDAO annexeDAO = new AnnexeDAO();
    // int take = userPagingBottom.getPageSize();
    // int start = userPagingBottom.getActivePage()
    // * userPagingBottom.getPageSize();
    // PagingListModel plm;
    // plm = annexeDAO.findBySubstance(text, take, start);
    // userPagingBottom.setTotalSize(plm.getCount());
    // lstAnnexe = plm.getLstReturn();
    // lbAnnexe.setModel(new ListModelArray(lstAnnexe));
    // }
    //
    // @Listen("onPaging = #incList #userPagingBottom")
    // public void onPaging(Event event) {
    //
    // reloadModel(txtSubstance.getText());
    // }
    //
    // // ==tichnv 18/04/2014
    // @Listen("onExportFile = #windowView")
    // public void onExportFile() throws JAXBException {
    // ExportOrderFileModel exportModel = new
    // ExportOrderFileModel(cosFile.getFileId());
    // ExportOrderFileDAO exp = new ExportOrderFileDAO();
    // PdfDocxFile docx = exp.exportDeviceFile(exportModel, true);
    // }

    public boolean isAbleToModifyStatus() {
        boolean result = false;
        if ("admin".equals(getUserName())) {
            result = true;
        }
        return result;
    }

    public void updateStatus() {
        String status = tbStatus.getValue();
        if (status != null && Long.valueOf(status) > 0) {
            List<Long> lstStatus = new ArrayList();
            lstStatus.add(Long.valueOf(status));
            ProcessDAOHE pHe = new ProcessDAOHE();
            List<Process> lstProcess = pHe.getDescProcessListStatus(files.getFileId(), Constants.FILE_TYPE_DYCNK, lstStatus);
            if (lstProcess.size() > 0) {
                files.setStatus(Long.valueOf(status));
                FilesDAOHE fHe = new FilesDAOHE();
                fHe.SaveAndCommit(files);
                fHe.commit();
                showNotification("Cập nhật thành công", Constants.Notification.INFO);
            } else{
                showNotification("Không đúng trạng thái");
            }
            
        }
    }

}
