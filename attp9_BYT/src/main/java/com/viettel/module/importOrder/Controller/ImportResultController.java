package com.viettel.module.importOrder.Controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ProcessDocument;
import com.viettel.module.importOrder.BO.VAttfileCategory;
import com.viettel.module.importOrder.DAO.ImportOrderAttachDao;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ProcessDocumentDAO;
import com.viettel.module.importOrder.DAO.VAttfileCategoryDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAOHE.AttachDAOHE;

/**
 *
 * @author THANHDV
 */
public class ImportResultController extends BusinessController {

    /**
     *
     */
    private List listImportOderFileType;
    private static final long serialVersionUID = 1L;
    private static final int IMPORT_ORDER_FILE = 1;
//    @Wire
//    private Listbox fileListbox;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox txtValidate, txtCurrentDept;
    @Wire
    private Combobox cmbType;
    @Wire
    private Datebox dbCheckdate, lbCheckdate2;
    private ImportOrderFile cosFile;
    private ProcessDocument documentProcess;
    private ProcessDocument documentProcess2;
    @Wire
    private Window businessWindow;

    @Wire
    private Listbox listDepartent, fileListbox, lbImportOrderFileType;

    public ProcessDocument getDocumentProcess() {
        return documentProcess;
    }

    public void setDocumentProcess(ProcessDocument documentProcess) {
        this.documentProcess = documentProcess;
    }

    public ImportOrderFile getCosFile() {
        return cosFile;
    }

    public void setCosFile(ImportOrderFile cosFile) {
        this.cosFile = cosFile;
    }

    @Wire
    private Vlayout flist;
    private List<Media> listMedia;
    private Files files;
    private long FILE_TYPE;
    private final static long DN = 0;
    private final static long CV = 1;
    private String FILE_NAME;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {

        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        listMedia = new ArrayList();
        cosFile = (new ImportOrderFileDAO()).findById(fileId);
        documentProcess = (new ProcessDocumentDAO()).getProcessDocumentByType(DN, fileId);

        documentProcess2 = (new ProcessDocumentDAO()).getProcessDocumentByType(CV, fileId);
        files = new Files();
        WorkflowAPI w = new WorkflowAPI();

        FILE_TYPE = w.getProcedureTypeIdByCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        FILE_NAME = w.getProcedureNameByCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        txtCurrentDept.setValue("1");
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, fileId);

    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        VAttfileCategoryDAO categoryDAO = new VAttfileCategoryDAO();
        List<VAttfileCategory> listAttach = categoryDAO.findCheckedFilecAttach(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, fileId);
        if (listAttach == null || listAttach.size() == 0) {
            showWarningMessage("Chưa có file đính kèm, vui lòng xem lại !!!");
            return false;
        }
        return true;
    }

    //update trạng thai dã gủi khi chuyển form
    public void updateAttachFileSigned() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectId(fileId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK);
        if (lstAttach != null && lstAttach.size() > 0) {
            for (Attachs att : lstAttach) {
                Attachs att_new = att;
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att_new);
                }
            }
        }

    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    @SuppressWarnings("unused")
    public void onSave() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        try {
//        	if (flist.getChildren() == null || flist.getChildren().isEmpty()) {
//                showWarningMessage("Chưa chọn file kiểm tra đính kèm");
//                return;
            // }
            //Thieu phan comment
            updateAttachFileSigned();
            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    /**
     * Thanhdv Hàm load danh sach trong dropdown control
     *
     * @param type
     * @return
     */
    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long fileId = obj.getObjectId();
        ImportOrderAttachDao rDAOHE = new ImportOrderAttachDao();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, fileId);
    }

    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case IMPORT_ORDER_FILE:
                categoryDAOHE = new CategoryDAOHE();
                listImportOderFileType = categoryDAOHE.getSelectCategoryByParentCode(
                        Constants.CATEGORY_TYPE.FILECAT_IMDOC, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK.toString());
                lstModel = new ListModelList(listImportOderFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
//	        VimportOrderFileAttach obj = (VimportOrderFileAttach) event.getData();

        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("z-valign-left");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException, Exception {
        int idx = lbImportOrderFileType.getSelectedItem().getIndex();
        if (idx == 0) {
            showNotification("Bạn phải chọn loại tệp đính kèm", Constants.Notification.INFO);
            return;
        }
        if (listMedia.isEmpty()) {
            showNotification("Chọn tệp tải lên trước khi thêm mới!",
                    Constants.Notification.WARNING);

            return;
        }

        Long rtFileFileType = Long.valueOf((String) lbImportOrderFileType.getSelectedItem().getValue());
        AttachDAO base = new AttachDAO();

        for (Media media : listMedia) {
            base.saveFileAttach(media, fileId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, rtFileFileType);
        }

        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, fileId);

        if ((listMedia != null) && (listMedia.size() > 0)) {
            listMedia.clear();
        }
        flist.getChildren().clear();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void fillFileListbox(Long attcat, Long fileId) {
        VAttfileCategoryDAO categoryDAO = new VAttfileCategoryDAO();

        List<VAttfileCategory> listAttach = categoryDAO.findCheckedFilecAttach(attcat, fileId);
        this.fileListbox.setModel(new ListModelArray(listAttach));
    }

    public ProcessDocument getDocumentProcess2() {
        return documentProcess2;
    }

    public void setDocumentProcess2(ProcessDocument documentProcess2) {
        this.documentProcess2 = documentProcess2;
    }

}
