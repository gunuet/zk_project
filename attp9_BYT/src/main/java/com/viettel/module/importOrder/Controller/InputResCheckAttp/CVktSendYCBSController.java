package com.viettel.module.importOrder.Controller.InputResCheckAttp;

import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.utils.LogUtils;



public class CVktSendYCBSController extends BusinessController {

    private static final long serialVersionUID = 1L;
    
    @Wire
    private Textbox txtMainContent;
    
	private Textbox txtNote = (Textbox)Path.getComponent("/windowProcessing/txtNote");
    
    private Long fileId;
    private Long evalType = 1L;
    
    
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
		fileId = (Long) arguments.get("fileId");
        
        return super.doBeforeCompose(page, parent, compInfo);
    }
    
    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        

      
    }


    @Listen("onClick=#btnSubmit")
    public void btnSubmit() {
        try {
        	txtNote.getValue();
        	//get user infor
            String note = txtNote.getValue();
            String mainContentValue = txtMainContent.getValue();
            
            //create json for formContent
            EvaluationModel evaluationModel = new EvaluationModel();
            evaluationModel.setUserId(getUserId());
            evaluationModel.setUserName(getUserName());
            evaluationModel.setDeptId(getDeptId());
            evaluationModel.setDeptName(getDeptName());
            evaluationModel.setContent(mainContentValue);
            evaluationModel.setResultEvaluation(new Long(0));
            evaluationModel.setResultEvaluationStr(note);
            String formContent = new Gson().toJson(evaluationModel);
            
            //set data insert DB
            EvaluationRecord evaluationRecord = new EvaluationRecord();
            evaluationRecord.setMainContent(mainContentValue);
            evaluationRecord.setFormContent(formContent);
            evaluationRecord.setEvalType(evalType);
            evaluationRecord.setFileId(fileId);
            evaluationRecord.setCreatorId(getUserId());
            evaluationRecord.setCreatorName(getUserName());
            evaluationRecord.setCreateDate(new Date());
            EvaluationRecordDAO evaluationRecordDAO = new EvaluationRecordDAO();
            evaluationRecordDAO.saveOrUpdate(evaluationRecord);
            
            throw new Exception();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

        }
    }



//    private void setWarningMessage(String message) {
//
//        lbTopWarning.setValue(message);
//        lbBottomWarning.setValue(message);
//
//    }
}
