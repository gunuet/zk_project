package com.viettel.module.importOrder.Controller.InputResCheckAttp;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.BO.ProductTarget;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.importOrder.BO.ProductProductTarget;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.module.importOrder.DAO.ProductProductTargetDAO;
import com.viettel.module.importOrder.DAO.ProductTargetDAO;
import com.viettel.module.importOrder.DAO.VProductTargetDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Radiogroup;

public class InputResCheckAttpTargetController extends BusinessController {

    private static final long serialVersionUID = 1L;

    @Wire
    Listbox targetCategory, target;

    @Wire
    Button btnSave;
    @Wire
    Label productIdHidden, productTargetIdHidden, lbtargetWarning;
    @Wire
    Textbox comment, testValue, maxValue, targetCost, testMethod, methodName;
    @Wire
    private Window addNewTargetWindow, parentWindow;
    private String method;
    Long targetId;
    @Wire
    Radiogroup rdCheck;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        productIdHidden.setValue(String.valueOf(arguments.get("producId")));
        method = (String) arguments.get("method") == null ? "" : (String) arguments.get("method");
        targetId = (Long) arguments.get("targetId") == null ? 0 : (Long) arguments.get("targetId");

        CategoryDAOHE categoryDAOHE = new CategoryDAOHE();
        List<Category> categoryList = categoryDAOHE.findAllCategory(
                Constants.CATEGORY_TYPE.TESTTYPE_IMDOC);
        ListModelList lstModel = new ListModelList(categoryList);
        targetCategory.setModel(lstModel);

        if ("update".equals(method)) {
            VProductTargetDAO dao = new VProductTargetDAO();
            VProductTarget vp_target = dao.findById(targetId);
            Long productTargetId = vp_target.getProductTargetId();
            testMethod.setValue(vp_target.getTestMethod());
            methodName.setValue(vp_target.getName());
            targetCost.setValue(vp_target.getCost() == null ? "" : vp_target.getCost().toString());
            testValue.setValue(vp_target.getTestValue());
            maxValue.setValue(vp_target.getMaxValue());
            comment.setValue(vp_target.getComments());
            if (vp_target.getPass() != null && vp_target.getPass() == 1) {
                rdCheck.setSelectedIndex(0);
            } else {
                rdCheck.setSelectedIndex(1);
            }

            ProductTarget productTarget = new ProductTargetDAO().findId(productTargetId);

            if (productTarget != null) {
                String type = productTarget.getType();
                targetCategory.renderAll();
                for (int i = 0; i < targetCategory.getListModel().getSize(); i++) {
                    Category cat = (Category) targetCategory.getListModel().getElementAt(i);
                    if (type.equals(cat.getCode())) {
                        targetCategory.setSelectedIndex(i);
                        break;
                    }
                }

            }
            //set listbox chỉ tiêu
            if (targetCategory.getSelectedItem() != null) {
                String code = targetCategory.getSelectedItem().getValue();
                List<ProductTarget> typeList = new ProductTargetDAO()
                        .findAllTargetByCategory(code);
                ListModelArray<ProductTarget> listModelArray = new ListModelArray<ProductTarget>(typeList);
                // ProductTarget productTarget = new ProductTargetDAO().findId(productTargetId);
                target.setModel(listModelArray);
                target.renderAll();
                if (productTarget != null) {
                    for (int i = 0; i < target.getListModel().getSize(); i++) {
                        ProductTarget cat = (ProductTarget) target.getListModel().getElementAt(i);
                        if (productTarget.getId() == cat.getId()) {
                            target.setSelectedIndex(i);
                            break;
                        }
                    }
                }

            }
        } else {
            targetCategory.setModel(lstModel);
        }

    }

    public void onChangeTargetCategory() {
        try {
            String code = targetCategory.getSelectedItem().getValue();
            List<ProductTarget> typeList = new ProductTargetDAO()
                    .findAllTargetByCategory(code);
            ListModelArray<ProductTarget> listModelArray = new ListModelArray<ProductTarget>(
                    typeList);
            target.setModel(listModelArray);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            target.setModel(new ListModelArray<ProductTarget>(
                    new ArrayList<ProductTarget>()));
        }

        // set empty for target
        productTargetIdHidden.setValue("");
        testMethod.setValue("");
        methodName.setValue("");
        targetCost.setValue("");
        testValue.setValue("");
        maxValue.setValue("");
        comment.setValue("");

    }

    public void onChangeTarget() {
        Long id;
        ProductTarget productTarget;
        id = Long.valueOf(target.getSelectedItem().getValue().toString());
        productTarget = new ProductTargetDAO().findId(id);
        testMethod.setValue(productTarget.getTestMethod());
        methodName.setValue(productTarget.getName());
        targetCost.setValue(String.valueOf(productTarget.getCost()));
        testValue.setValue("");
        maxValue.setValue("");
        productTargetIdHidden.setValue(String.valueOf(productTarget.getId()));

    }

    @Listen("onClick = #btnSave")
    public void onSave() {
        if (!isValidate()) {
            return;
        }
        if (target.getSelectedItem() == null || !target.getSelectedItem().getLabel().trim().equals(methodName.getValue().trim())) {
            productTargetIdHidden.setValue(null);
        }
        Long productId = Long.valueOf(productIdHidden.getValue());
        ProductProductTarget productProductTarget = new ProductProductTarget();
        ProductProductTargetDAO pdao = new ProductProductTargetDAO();
        if ("update".equals(method)) {
            productProductTarget = pdao.findById(targetId);
        }
        if (targetCategory.getSelectedItem() != null) {
            productProductTarget.setType(targetCategory.getSelectedItem().getValue().toString());
            productProductTarget.setTypeName(targetCategory.getSelectedItem().getLabel());
        }
        productProductTarget.setName(methodName.getValue());
        productProductTarget.setTestMethod(testMethod.getValue());
        productProductTarget.setComments(comment.getValue());
        productProductTarget.setCreatedBy(getUserId());
        productProductTarget.setCreatedDate(new Date());
        productProductTarget.setIsActive(Long.valueOf(1));
        productProductTarget.setProductId(productId);
        // if(targetCategory.getSelectedItem())
        productProductTarget.setTestValue(testValue.getValue());
        productProductTarget.setMaxValue(maxValue.getValue());
        productProductTarget.setCost(Long.valueOf(targetCost.getValue()));
        productProductTarget.setPass(Long.parseLong(rdCheck.getSelectedItem().getValue().toString()));
        //update
        if (target.getSelectedItem() != null && target.getSelectedItem().getValue().toString().equals(methodName.getValue())) {

            productProductTarget.setProductTargetId(Long.parseLong(target.getSelectedItem().getValue().toString()));
        } else {
            productProductTarget.setProductTargetId(null);
        }
        if ("update".equals(method)) {
            productProductTarget.setId(targetId);
        }
        ProductProductTargetDAO productProductTargetDAO = new ProductProductTargetDAO();
        productProductTargetDAO.saveOrUpdate(productProductTarget);

        //close popup
        onCloseWindow(productId);

    }

    /*close popup btnSave  */
    public void onCloseWindow(Long prId) {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("pri_id", prId);
        Events.sendEvent(new Event("onChildWindowClosed", parentWindow, args));

        if (addNewTargetWindow != null) {
            addNewTargetWindow.detach();
        }
    }

    public Boolean isValidate() {
        if (targetCost.getValue().matches("\\s*")) {
            lbtargetWarning.setValue("Chưa nhập phí kiểm nghiệm !!!");
            return false;
        } else if (!targetCost.getValue().matches("\\d{1,10}")) {
            lbtargetWarning.setValue("Phí kiểm nghiệm phải nhập số nguyên!!!");
            return false;
        } else if (targetCost.getValue().startsWith("00")) {
            lbtargetWarning.setValue("Phí kiểm nghiệm phải nhập số nguyên!!!");
            return false;
        } else if (methodName.getValue().matches("\\s*")) {
            lbtargetWarning.setValue("Chưa nhập tên chỉ tiêu !!!");
            return false;
        } else if (rdCheck.getSelectedItem() == null) {
            lbtargetWarning.setValue("Chưa nhập kết quả !!!");
            return false;
        }
        return true;

    }

    public Listbox getTarget() {
        return target;
    }

    public void setTarget(Listbox target) {
        this.target = target;
    }

}
