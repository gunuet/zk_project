package com.viettel.module.importOrder.Controller.InputResCheckAttp;

import com.viettel.core.base.DAO.AttachDAO;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.Controller.include.EvaluationController;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VAttfileCategory;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.module.importOrder.BO.VimportOrderFileAttach;
import com.viettel.module.importOrder.DAO.ImportOrderAttachDao;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VAttfileCategoryDAO;
import com.viettel.module.importOrder.DAO.VProductTargetDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author THANHDV
 */
public class LdpCqktCheckAttpController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, producIdHidden, lbFileId;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private Long fileId;
    private VFileImportOrder cosFile;
    @Wire
    Listbox lbOrderProduct;
    @Wire
    Listbox lbProductTarget;
    Long documentTypeCode;
    private Files files;
    private String nswFileCode;
    private List<Category> listImportOderFileType;
    private List<Media> listMedia, listFileExcel;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;
    private final int IMPORT_ORDER_FILE = 1;
    private Users user;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Button   btnCreateProFile;

    private CosEvaluationRecord obj = new CosEvaluationRecord();
    @Wire
    private Window businessWindow;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    private Vlayout flist, fListImportExcel;
    @Wire
    private Listbox listDepartent, fileListbox;
     @Wire
    private Paging userPagingTop, userPagingBottom;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        listMedia = new ArrayList();
        //Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
        files = (new FilesDAOHE()).findById(fileId);
        //load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        nswFileCode = getAutoNswFileCode(documentTypeCode);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("id", fileId);
        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(fileId);
        ListModelArray lstModelManufacturer = new ListModelArray(importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);
        lbFileId.setValue(String.valueOf(fileId));
        
        if(importOrderProducts != null)
        {
        	showProduct(importOrderProducts.get(0).getProductId());
        }

    }

    @Listen("onOpenUpdate = #lbProductTarget")
    public void onOpenUpdate(Event event) {
        VProductTarget obj = (VProductTarget) event.getData();
        Long targetId = obj.getId();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("producId", producIdHidden.getValue());
        arguments.put("targetId", targetId);
        arguments.put("method", "update");
        setParam(arguments);
        createWindow("windowCRUDCosmetic", "/Pages/module/importorder/inputResultCheck/inputResCheckProTarget.zul", arguments, Window.HIGHLIGHTED);
    }

    @Listen("onDelete = #lbProductTarget")
    public void onDelete(Event event) {
        final VProductTarget obj = (VProductTarget) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {

                                        VProductTargetDAO objDAOHE = new VProductTargetDAO();
                                        objDAOHE.delete(obj.getId());
                                        reload();

                                    } catch (Exception ex) {
                                        showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                        LogUtils.addLogDB(ex);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    public void reload() {
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        List<VProductTarget> productProductTarget = new VProductTargetDAO().findByProdutId(productId);
        ListModelArray lstModel_ProducTarget = new ListModelArray(productProductTarget);
        lbProductTarget.setModel(lstModel_ProducTarget);
    }
/*
    @Listen("onClick=#btnSave")
    public void onClickbtnbtnSave() {
        Map<String, Object> agrs = new ConcurrentHashMap<String, Object>();
        String prId = producIdHidden.getValue();
        String fileID = lbFileId.getValue();
        Long productId = new Long(prId);
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(productId);
        //lưu kết quả
        String pass = cbtexLegal.getSelectedItem().getValue();
        importOrderProduct.setPass(new Long(pass));
        importOrderProductDAO.saveOrUpdate(importOrderProduct);
        //load lai danh sachs san pham      
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(new Long(fileID));

        ListModelArray lstModelManufacturer = new ListModelArray(importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);
    */
    

    @Listen("onShow =  #lbOrderProduct")
    public void onShow(Event event) {
        ImportOrderProduct obj = (ImportOrderProduct) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        Long productId = obj.getProductId();
        arguments.put("id", productId);
        arguments.put("CRUDMode", "UPDATE");
        showProduct(productId);
    }
    
    public void showProduct(Long productId) {
        //set list danh sách chỉ tiêu của sản một sản phẩm
        List<VProductTarget> productProductTarget = new VProductTargetDAO().findByProdutId(productId);
        //get sanr pham
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(productId);
        if (productProductTarget != null) {
            ListModelArray lstModel_ProducTarget = new ListModelArray(productProductTarget);

            lbProductTarget.setModel(lstModel_ProducTarget);
            String value = importOrderProduct.getCheckMethodCode();
            if (value == null) {
                lbProductTarget.setVisible(false);
            } else if ("THUONG".equals(value)) {
                lbProductTarget.setVisible(true);
                //  btnAddNew.setVisible(true);
            } else {
                lbProductTarget.setVisible(true);
                // btnAddNew.setVisible(false);
            }
        } else {
            lbProductTarget.setVisible(false);
            //btnAddNew.setVisible(false);
        }
      /*  Long pass = importOrderProduct.getPass();
        if (pass != null) {
            if (pass == 1) {
                cbtexLegal.setSelectedIndex(1);
            } else {
                cbtexLegal.setSelectedIndex(2);
            }
        } else {
            cbtexLegal.setSelectedIndex(0);
        }*/
        //load lại danh sách tập tin theo productID
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, productId);

        //set view
      //  btnAddNew.setVisible(true);
        producIdHidden.setValue(String.valueOf(productId));
        //lbtexLegal.setVisible(true);
        //cbtexLegal.setVisible(true);
        //btnSave.setVisible(true);
        fileListbox.setVisible(true);
        //lbAttachFile.setVisible(true);
       // lbImportOrderFileType.setVisible(true);
       // lbUploadFile.setVisible(true);
       // btnAttach.setVisible(true);
        //btnCreate.setVisible(true);
        // btnCreateProFile.setVisible(true);
    }
    /*
    @Listen("onClick =  #btnAddNew")
    public void onShowNewAdd() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("producId", producIdHidden.getValue());
        arguments.put("targetId", new Long("0"));
        //arguments.put("method", null);
        arguments.put("parentWindow", businessWindow);

        setParam(arguments);
        createWindow("windowCRUDCosmetic", "/Pages/module/importorder/inputResultCheck/inputResCheckProTarget.zul", arguments, Window.HIGHLIGHTED);
    }
      */
    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", businessWindow);
        return arguments;
    }
//    }
//    @Listen("onChildWindowClosed=#businessWindow")
//    public void onChildWindowClosed() {
//        String prId = producIdHidden.getValue();
//        Long productId = new Long(prId);
//        fillListData(productId);
//    }

    @Listen("onChildWindowClosed=#businessWindow")
    public void onChildWindowClosed() {
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        fillListData(productId);
    }

    public void fillListData(Long productId) {
        List<VProductTarget> productProductTarget = new VProductTargetDAO().findByProdutId(productId);
        if (productProductTarget != null) {
            ListModelArray lstModel_ProducTarget = new ListModelArray(productProductTarget);
            lbProductTarget.setModel(lstModel_ProducTarget);
        }

    }

    @Listen("onClick=#btnSubmit")
    public void btnSubmit() {
        clearWarningMessage();
        try {
            onApproveFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public void onApproveFile() throws Exception {
        if (!isValidate()) {
            return;
        }
        txtValidate.setValue("1");

    }

    public boolean isValidate() {
        String message;
        //load lai danh sachs san pham
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(fileId);
        DANHSACH:
        for (ImportOrderProduct importOrder : importOrderProducts) {
            ImportOrderProduct obj = importOrder;
            if (obj.getPass() == null) {
                message = "Chưa nhập kết quả kiểm tra cho sản phẩm.Vui lòng kiểm tra lại sau đó Click nút lưu Kết quả";
                showWarningMessage(message);
                return false;
            }
        }
        return true;
    }

    protected void showWarningMessage(String message) {
        lbBottomWarning.setValue(message);
    }

    private void clearWarningMessage() {
        lbBottomWarning.setValue("");
    }

    private String getAutoNswFileCode(Long documentTypeCode) {

        String autoNumber;

        ImportOrderFileDAO dao = new ImportOrderFileDAO();
        Long autoNumberL = dao.countImportfile();
        autoNumberL += 1L;//Tránh số 0 (linhdx)
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

    public void onChangelbLegal() {
       // String value = cbtexLegal.getSelectedItem().getValue();
        String prId = producIdHidden.getValue();
        if (prId != null && !"".equals(prId)) {
//            Long productId = new Long(prId);
//            ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
//            ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(productId);
            lbProductTarget.setVisible(true);
           /* if (value.equals("1")) {
                if (importOrderProduct != null) {
                    String checkmethod = importOrderProduct.getCheckMethodCode();
                    if (checkmethod != null) {
            if (//<editor-fold defaultstate="collapsed" desc="comment">
            checkmethod.equals("THUONG")
            //</editor-fold>
) {
                            //  lbProductTarget.setVisible(true);
                           // btnAddNew.setVisible(true);
                        } else if (checkmethod.equals("CHAT")) {
                            //lbProductTarget.setVisible(true);
                            //btnAddNew.setVisible(false);
                        }
                    } else {
                        lbProductTarget.setVisible(false);
                      //  btnAddNew.setVisible(false);
                    }
                } else {
                    lbProductTarget.setVisible(false);
                   // btnAddNew.setVisible(false);
                }
            } else {
                lbProductTarget.setVisible(true);
              //  btnAddNew.setVisible(true);
            }
        } else {
            lbProductTarget.setVisible(false);
         //   btnAddNew.setVisible(false);
          * 
          */
        }

    }

    /* Tichnv :nhoms chi tieeu
     @param : 
     @return
     */
    public ListModelList getListBoxModel_ChiTieu() {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;

        categoryDAOHE = new CategoryDAOHE();
        listImportOderFileType = categoryDAOHE.findAllCategory(
                Constants.CATEGORY_TYPE.TESTTYPE_IMDOC);
        lstModel = new ListModelList(listImportOderFileType);

        return lstModel;
    }
    
//    //phân trang
//     @Listen("onPaging = #userPagingTop, #userPagingBottom")
//    public void onPaging(Event event) {
//        final PagingEvent pe = (PagingEvent) event;
//        if (userPagingTop.equals(pe.getTarget())) {
//            userPagingBottom.setActivePage(userPagingTop.getActivePage());
//        } else {
//            userPagingTop.setActivePage(userPagingBottom.getActivePage());
//        }
//        reloadModel(lastSearchModel);
//    }

    //attach File 
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case IMPORT_ORDER_FILE:
                categoryDAOHE = new CategoryDAOHE();
                listImportOderFileType = categoryDAOHE.findAllCategory(
                        Constants.CATEGORY_TYPE.FILECAT_IMDOC);
                lstModel = new ListModelList(listImportOderFileType);
                return lstModel;
        }
        return new ListModelList();

    }
  
    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VimportOrderFileAttach obj = (VimportOrderFileAttach) event.getData();
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        ImportOrderAttachDao rDAOHE = new ImportOrderAttachDao();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, productId);
    }
  /*
    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                showNotification("Định dạng file không được phép tải lên",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("z-valign-left");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listMedia.remove(media);
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }
      */
  
 /*
    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException, Exception {
        int idx = lbImportOrderFileType.getSelectedItem().getIndex();
        if (idx == 0) {
            showNotification("Bạn phải chọn loại hồ sơ", Constants.Notification.INFO);
            return;
        }
        if (listMedia.isEmpty()) {
            showNotification("Chọn tệp tải lên trước khi thêm mới!",
                    Constants.Notification.WARNING);

            return;
        }

//        if (saveObject() == false) {
//            return;
//        }
        Long rtFileFileType = Long.valueOf((String) lbImportOrderFileType.getSelectedItem().getValue());
        AttachDAO base = new AttachDAO();
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        for (Media media : listMedia) {
            base.saveFileAttach(media, productId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, rtFileFileType);
        }

        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, productId);
        if ((listMedia != null) && (listMedia.size() > 0)) {
            listMedia.clear();
        }
        flist.getChildren().clear();

    }
    */

    private void fillFileListbox(Long obj_type, Long obj_id) {
        VAttfileCategoryDAO dao = new VAttfileCategoryDAO();
        List<VAttfileCategory> lstCosmeticAttach = dao.findCheckedFilecAttach(obj_type, obj_id);
        this.fileListbox.setModel(new ListModelArray(lstCosmeticAttach));
    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    private void setWarningMessage(String message) {

        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);

    }

    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        return Constants.CHECK_VIEW.VIEW;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }
}
