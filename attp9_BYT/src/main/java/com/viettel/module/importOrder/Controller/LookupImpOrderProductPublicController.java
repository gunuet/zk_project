/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importOrder.BO.VFIofIopP;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VFIofIopPDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.ws.BO.Attachment;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkforge.bwcaptcha.Captcha;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

/**
 *
 * @author binhnt53
 */
public class LookupImpOrderProductPublicController extends BaseComposer {

    @Wire
    Textbox txtBusinessName, txtFileCode, txtTaxCode, txtReceiveNo, txtCaptchaId, txtConfirmAnnounceNo;
    @Wire
    Listbox lbFiles;
    @Wire
    Paging userPagingBottom;
    VFIofIopP searchForm;
    @Wire
    Captcha capcha;
    @Wire
    Vbox vboxCaptchaId;
    @Wire
    private Label lbTopWarning;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        this.getPage().setTitle(ResourceBundleUtil.getString("title"));
        Session ss = Sessions.getCurrent();
        if (ss.getAttribute("countCaptcha1") == null) {
            ss.setAttribute("countCaptcha1", 0);
        }
        capcha.setBgColor(16777215);
        capcha.setCaptchars(Constants.getCaptchars());
        if (Integer.valueOf(ss.getAttribute("countCaptcha1").toString()) >= 2) {
            vboxCaptchaId.setVisible(true);
            ss.setAttribute("isVisibleCaptcha", true);
        }
        searchForm = new VFIofIopP();
        //onSearch();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        userPagingBottom.setActivePage(0);
        search(0);
    }

    private void search(int actp) {
        if (!"".equals(txtFileCode.getValue().trim())
                || !"".equals(txtTaxCode.getValue().trim())
                || !"".equals(txtBusinessName.getValue().trim())
                || !"".equals(txtReceiveNo.getValue().trim())
                || !"".equals(txtConfirmAnnounceNo.getValue().trim())) {
            int take = userPagingBottom.getPageSize();
            int start = actp * userPagingBottom.getPageSize();
            PagingListModel plm;
            searchForm.setFileCode(txtFileCode.getValue());
            searchForm.setTaxCode(txtTaxCode.getValue().trim());
            searchForm.setBusinessName(txtBusinessName.getValue().trim());
            searchForm.setReceiveNo(txtReceiveNo.getValue().trim());
            searchForm.setConfirmAnnounceNo(txtConfirmAnnounceNo.getValue().trim());
            VFIofIopPDAO fDAO = new VFIofIopPDAO();
            plm = fDAO.searchDataPermited(searchForm, start, take);
            userPagingBottom.setTotalSize(plm.getCount());
            if (plm.getCount() == 0) {
                userPagingBottom.setVisible(false);
            } else {
                userPagingBottom.setVisible(true);
            }
            ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
            lbFiles.setModel(lstModel);
            lbFiles.setMultiple(true);
        } else {
            showNotification(getLabelCos("qrcode_error1"), Constants.Notification.ERROR);
            //showNotification(String.format(Cmsgonstants.Notification.SENDED_SUCCESS, "hồ sơ"), Constants.Notification.INFO);
        }
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        search(userPagingBottom.getActivePage());
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public String getLabel(String key) {
        try {
            return ResourceBundleUtil.getString(key, "language_XNN_vi");

        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLogDB(ex);
        }
        return "";
    }

    //binhnt add method download qrcode
    @Listen("onDownloadQrCode =#lbFiles")
    public void onDownloadQrCode(Event event) {
        VFIofIopP obj = (VFIofIopP) event.getData();
        QrCodeController qrcodeconsole = new QrCodeController();
        qrcodeconsole.onDownloadQrCodeByProductId(obj.getProductId());
    }

    //binhnt add download giấy phép hồ sô đã công bố.
    @Listen("onDownloadPermit =#lbFiles")
    public void onDownloadPermit(Event event) {
        VFIofIopP obj = (VFIofIopP) event.getData();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> lstAtt = attDAOHE.getByObjectIdAndAttachCatAndAttachType(obj.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
        AttachDAO attDAO = new AttachDAO();
        try {
            if (lstAtt != null && lstAtt.size() > 0) {
                attDAO.downloadFileAttach(lstAtt.get(0));
            } else {
                showNotification(getLabelCos("tracuu_taibancongboError"), Constants.Notification.ERROR);
            }
        } catch (FileNotFoundException ex) {
            LogUtils.addLogDB(ex);
            showNotification(getLabelCos("tracuu_taibancongboError"), Constants.Notification.ERROR);
        }
    }
//kioem tra qr code ton tai hay khong

    public int CheckHaveQrCode(Long productId) {
        ImportOrderProductDAO iopDao = new ImportOrderProductDAO();
        int check = iopDao.checkHaveQrCode(productId);
        return check;
    }
}
