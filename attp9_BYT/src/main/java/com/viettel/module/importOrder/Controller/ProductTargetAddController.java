/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.BO.ProductTarget;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.module.importOrder.DAO.ProductTargetDAO;
import com.viettel.utils.Constants;

/**
 *
 * @author THANHDV
 */
@SuppressWarnings("serial")
public class ProductTargetAddController extends BaseComposer {

	@Wire
	Textbox tbTagetId, tbtargetName, tbtargetCost, tbMethodtest, tbcode;
	@Wire
	private Listbox lbtargetType;
	private Users user;
	private List listImportOderFileType;
	@Wire
	Window productCreateWnd;

	@Wire
	Paging userPagingBottom;
	private String crudMode;
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ComponentInfo doBeforeCompose(Page page, Component parent,
			ComponentInfo compInfo) {
		return super.doBeforeCompose(page, parent, compInfo);
	}

	@Override
	public void doAfterCompose(Component window) throws Exception {
		super.doAfterCompose(window);
		//Loadform();

        CategoryDAOHE dao = new CategoryDAOHE();
		List<Category> listImportOderFileType = dao
				.findAllCategorySearch(Constants.CATEGORY_TYPE.TESTTYPE_IMDOC);
		ListModelArray lstModelProcedure = new ListModelArray(
				listImportOderFileType);
		lbtargetType.setModel(lstModelProcedure);
		lbtargetType.renderAll();
		
		loadInfoToForm();

	}



	@Listen("onClick=#btnSave")
	public void onSave() {
		
		ProductTarget product = new  ProductTarget();
		if (tbTagetId.getValue() != null && !tbTagetId.getValue().isEmpty()) {
            product.setId(Long.parseLong(tbTagetId.getValue()));
        }

        if (tbtargetName.getValue() != null&& tbtargetName.getValue().trim().length() == 0) {
			showNotification("Phải nhập tên chỉ tiêu kiểm tra ",
					Constants.Notification.ERROR);
			tbtargetName.focus();
			return ;
		}else {
            product.setName(tbtargetName.getValue());
        }

        if (tbcode.getValue() != null	&& tbcode.getValue().trim().length() == 0) {
			showNotification("Phải nhập mã code", Constants.Notification.ERROR);
			tbcode.focus();
			return ;
        } else {
            product.setCode(tbcode.getValue());
        }
        if (tbtargetCost.getValue() != null && tbtargetCost.getValue().trim().length() == 0) {
			showNotification("Phải nhập phí chỉ tiêu", Constants.Notification.ERROR);
			tbtargetCost.focus();
			return ;
        } else {
            product.setCost(Long.parseLong(tbtargetCost.getValue().toString()));
        } 
        if (tbMethodtest.getValue() != null && tbMethodtest.getValue().trim().length() == 0) {
        	showNotification("Phải nhập phương thức thử ",
					Constants.Notification.ERROR);
			tbMethodtest.focus();
			return ;
        } else {
            product.setTestMethod(tbMethodtest.getValue());
        }
        if (lbtargetType.getSelectedItem() != null)
        {
			int idx = lbtargetType.getSelectedItem().getIndex();
			if (idx == 0l) {
				showNotification("Phải chọn nhóm  kiểm nghiệm",
						Constants.Notification.ERROR);
				lbtargetType.focus();
				return ;
			}else {
				product.setType(lbtargetType.getSelectedItem().getValue().toString());
				product.setTypeName(lbtargetType.getSelectedItem().getLabel().toString());
			}
         product.setIsActive(1l);
         product.setCreatedBy(getUserId());
         product.setCreatedDate(new Date());
		ProductTargetDAO dao = new ProductTargetDAO();
     	dao.saveOrUpdate(product);
		if (tbTagetId.getValue() != null && !tbTagetId.getValue().isEmpty()) {
            //update thì detach window
			productCreateWnd.detach();
        } else {
            // them moi thi clear window
            loadInfoToForm();
        }
        showNotification("Lưu thành công", Constants.Notification.INFO);

        Window parentWnd = (Window) Path.getComponent("/producttargetWnd");
        Events.sendEvent(new Event("onReload", parentWnd, null));
        }
	}

	public int getSelectedIndexInModel(int type) {
		int selectedItem = 0;
		return selectedItem;
	}
	 public void loadInfoToForm() {
	        Long id = (Long) Executions.getCurrent().getArg().get("id");

		    lbtargetType.setSelectedIndex(0);
	        if (id != null) {
	            ProductTargetDAO objhe = new ProductTargetDAO();
	            ProductTarget rs = objhe.findById(id);
	            if (rs.getId()!=null) {
	            	tbTagetId.setValue(rs.getId().toString());
				}
	            if (rs.getName() != null) {
	                tbtargetName.setValue(rs.getName().toString());
	            }
	            if (rs.getCode() != null) {
	                tbcode.setValue(rs.getCode().toString());
	            }
	            if (rs.getTestMethod() != null) {
	                tbMethodtest.setValue(rs.getTestMethod().toString());
	            }

	            if (rs.getCost() != null) {
	                tbtargetCost.setValue(rs.getCost().toString());
	            }
	            if (rs.getType() != null) {
	             //   lbtargetType.setSelected(rs.getType().toString());
	            }
	            
	            CategoryDAOHE dao = new CategoryDAOHE();
	    		List<Category> listImportOderFileType = dao
	    				.findAllCategorySearch(Constants.CATEGORY_TYPE.TESTTYPE_IMDOC);
	    		ListModelArray lstModelProcedure = new ListModelArray(
	    				listImportOderFileType);
	    		lbtargetType.setModel(lstModelProcedure);
	    		lbtargetType.renderAll();
	    		
	    		int i = -1;
	    		for (Iterator iterator = listImportOderFileType.iterator(); iterator
						.hasNext();) {
					Category category = (Category) iterator.next();
					i++;
					if(category.getCode() !=null && category.getCode().equals(rs.getType())){
						

					    lbtargetType.setSelectedIndex(i);
						break;
					}
					
				}
	    		
	    		
	            
	        } else {
	        	tbTagetId.setValue(null);
	            tbcode.setValue("");
	            tbtargetCost.setValue(null);
	            tbtargetName.setValue("");
	            tbMethodtest.setText("");
	           lbtargetType.setSelectedIndex(0);
	        }
	        
	        
	 }
	 public void Loadform()
	 {

         CategoryDAOHE dao = new CategoryDAOHE();
 		List<Category> listImportOderFileType = dao
 				.findAllCategorySearch(Constants.CATEGORY_TYPE.TESTTYPE_IMDOC);
 		ListModelArray lstModelProcedure = new ListModelArray(
 				listImportOderFileType);
 		lbtargetType.setModel(lstModelProcedure);
 		lbtargetType.renderAll();
 		lbtargetType.setSelectedIndex(-1);
	 }

}
