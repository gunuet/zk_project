/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.ProductTarget;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.module.cosmetic.BO.Annexe;
import com.viettel.module.cosmetic.DAO.AnnexeDAO;
import com.viettel.module.importOrder.DAO.ProductTargetDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author THANHDV
 */
public class ProductTargetManageController extends BaseComposer {

    @Wire
    Listbox lbAnnexe, lbAnnexeType, lbIsActive, lbtargetType;
    @Wire
    Paging userPagingBottom;
    @Wire

    Textbox tbTagetId, tbtargetName, tbtargetCost, tbMethodtest, tbcode;
    ProductTarget productTarget;
    @Wire
    Datebox dbAllowedUntil;
    @Wire
    Window producttargetWnd;

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component window) {
        try {
            super.doAfterCompose(window);
            CategoryDAOHE dao = new CategoryDAOHE();
            List<?> listImportOderFileType = dao
                    .findAllCategorySearch(Constants.CATEGORY_TYPE.TESTTYPE_IMDOC);
            ListModelArray<?> lstModelProcedure = new ListModelArray<Object>(
                    listImportOderFileType);
            lbtargetType.setModel(lstModelProcedure);
            lbtargetType.renderAll();

            fillDataToList(productTarget);
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
        //   onSearch();
    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    @Listen("onReload=#producttargetWnd")
    public void onReload() {
        onSearch();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {
            userPagingBottom.setActivePage(0);
            productTarget = new ProductTarget();
//        // userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
            if (tbtargetName.getValue() != null && !"".equals(tbtargetName.getValue())) {
                productTarget.setName(tbtargetName.getValue());
            }
            if (tbcode.getValue() != null && !"".equals(tbcode.getValue())) {
                productTarget.setCode(tbcode.getValue().trim().toString());
            }
            if (tbMethodtest.getValue() != null && !"".equals(tbMethodtest.getValue())) {
                productTarget.setTestMethod(tbMethodtest.getValue().trim().toString());
            }
            if (lbtargetType.getSelectedItem() != null && lbtargetType.getSelectedIndex() > 0) {
                String type = lbtargetType.getSelectedItem().getValue().toString();

                productTarget.setType(type);
            }

            fillDataToList(productTarget);
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException {
        Window window = (Window) Executions.createComponents(
                "/Pages/admin/producttarget/producttargetCreate.zul", null, null);
        window.doModal();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Listen("onEdit=#lbAnnexe")
    public void onUpdate(Event ev) throws IOException {
        ProductTarget abo = (ProductTarget) lbAnnexe.getSelectedItem().getValue();
        Map<String, Long> args = new ConcurrentHashMap<String, Long>();
        args.put("id", abo.getId());
        Window window = (Window) Executions.createComponents("/Pages/admin/producttarget/producttargetCreate.zul", null, args);
        window.doModal();
    }

    @Listen("onDelete=#lbAnnexe")
    public void onDelete(Event ev) throws IOException {
        Messagebox.show("Bạn có chắc chắn muốn xóa không?", "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    try {
                                        ProductTarget rs = (ProductTarget) lbAnnexe.getSelectedItem().getValue();
                                        ProductTargetDAO dao = new ProductTargetDAO();
                                        dao.delete(rs.getId());
                                        // onSearch();
                                    } catch (Exception ex) {
                                        LogUtils.addLogDB(ex);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void fillDataToList(ProductTarget productTarget) {
        ProductTargetDAO objhe = new ProductTargetDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(productTarget, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }
        ListModelArray<?> lstModel = new ListModelArray<Object>(plm.getLstReturn());
        lbAnnexe.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList(productTarget);
    }
}
