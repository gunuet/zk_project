/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Writer;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VFIofIopP;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VFIofIopPDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;
import org.zkoss.zul.Filedownload;

/**
 *
 * @author Administrator
 */
public class QrCodeController extends BaseComposer {

    //binhnt add code download qr code
    public void onDownloadQrCodeByFileId(Long filesId) {
        FilesDAOHE filesdaohe = new FilesDAOHE();

        Files obj = filesdaohe.findById(filesId);
        if (obj != null && obj.getQrCode() != null) {
            try {
                String fileName = "qrcode_" + obj.getFileId().toString() + ".png";
                ResourceBundle rb = ResourceBundle.getBundle("config");
                String filePath = rb.getString("signTemp");
                FileUtil.mkdirs(filePath);
                File f = new File(filePath + fileName);
                if (f.exists()) {
                } else {
                    f.createNewFile();
                }
                InputStream in = new ByteArrayInputStream(obj.getQrCode());
                BufferedImage bImageFromConvert = ImageIO.read(in);
                ImageIO.write(bImageFromConvert, "png", f);
                OutputStream outputStream = new FileOutputStream(fileName);
                outputStream.write(obj.getQrCode());
                outputStream.flush();
                Filedownload.save(f, "png");
                outputStream.close();
            } catch (IOException ex) {
                LogUtils.addLog(ex);
                showNotification(getLabelCos("qrcode_error2"), Constants.Notification.ERROR);
            }
        }
    }
//tai qr code bang product id

    public void onDownloadQrCodeByProductId(Long productId) {
        ImportOrderProductDAO iopdao = new ImportOrderProductDAO();
        ImportOrderProduct obj = iopdao.findByProductId(productId);
        if (obj != null && obj.getQrCode() != null) {
            try {
                String fileName = "qrcode_" + obj.getProductId().toString() + ".png";
                ResourceBundle rb = ResourceBundle.getBundle("config");
                String filePath = rb.getString("signTemp");
                FileUtil.mkdirs(filePath);
                File f = new File(filePath + fileName);
                if (f.exists()) {
                } else {
                    f.createNewFile();
                }
                InputStream in = new ByteArrayInputStream(obj.getQrCode());
                BufferedImage bImageFromConvert = ImageIO.read(in);
                ImageIO.write(bImageFromConvert, "png", f);
                OutputStream outputStream = new FileOutputStream(fileName);
                outputStream.write(obj.getQrCode());
                outputStream.flush();
                Filedownload.save(f, "png");
                outputStream.close();
            } catch (IOException ex) {
                LogUtils.addLog(ex);
                showNotification(getLabelCos("qrcode_error2"), Constants.Notification.ERROR);
            }
        } else {
            showNotification(getLabelCos("qrcode_error2"), Constants.Notification.ERROR);
        }
    }

    //ham xu ly lay so cong bo tu he thong cong bo san pham
    public byte[] createQrCodeByVFIofIopP(VFIofIopP obj) {
        String barcodeInfo = "";

        if (obj != null) {
            if (obj.getNationalName() != null) {
                barcodeInfo += "X.xu: " + obj.getNationalName();
            } else {
                barcodeInfo += "X.xu: N/A";
            }
            if (obj.getConfirmAnnounceNo() != null) {
                barcodeInfo += "\nCong bo: " + obj.getConfirmAnnounceNo();
            } else {
                barcodeInfo += "\nCong bo: N/A";
            }
            if (obj.getReceiveNo() != null) {
                barcodeInfo += "\nXac nhan: " + obj.getReceiveNo();
            } else {
                barcodeInfo += "\nXac nhan: N/A";
            }
            if (obj.getSignDate() != null) {
                barcodeInfo += "\nNgay: " + convertDateToString(obj.getSignDate());
            } else {
                barcodeInfo += "\nNgay: N/A";
            }
            if (obj.getBusinessPhone() != null) {
                barcodeInfo += "\nDt DN: " + obj.getBusinessPhone();
            } else {
                barcodeInfo += "\nDt DN: N/A";
            }
            barcodeInfo += "\nWebsite: moh.gov.vn";
        } else {
            barcodeInfo = "\nSan pham khong co thong tin";
        }

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String dir = rb.getString("directoryQR");
        File folderExisting = new File(dir);
        if (!folderExisting.isDirectory()) {
            folderExisting.mkdir();
        }
        if (folderExisting.isDirectory()) {//tao folder theo ngay thang
            File temp = new File(dir);
            if (!temp.isDirectory()) {
                temp.mkdirs();
            }
        }
        Charset charset = Charset.forName("UTF-8");
        CharsetEncoder encoder = charset.newEncoder();
        byte[] b = null;
        ByteBuffer bbuf = null;
        try {
            bbuf = encoder.encode(CharBuffer.wrap(barcodeInfo));
        } catch (CharacterCodingException ex) {
            LogUtils.addLog(ex);
        }
        b = bbuf.array();

        String data;
        String fName = dir + obj.getProductId() + "_QRCode.PNG";
        try {
            data = new String(b, "UTF-8");
            // get a byte matrix for the data
            BitMatrix matrix = null;
            int h = 500;
            int w = 500;
            Writer writer = new MultiFormatWriter();
            try {
                Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>(2);
                hints.put(EncodeHintType.CHARACTER_SET, "ISO-8859-1");
                matrix = writer.encode(data, BarcodeFormat.QR_CODE, w, h, hints);
            } catch (com.google.zxing.WriterException ex) {
                LogUtils.addLog(ex);
            }
            // change this path to match yours (this is my mac home folder, you can use: c:\\qr_png.png if you are on windows)
            File file = new File(fName);
            try {
                MatrixToImageWriter.writeToFile(matrix, "PNG", file);//System.out.println("printing to " + file.getAbsolutePath());
            } catch (IOException ex) {
                LogUtils.addLog(ex);
            }
        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLog(ex);
        }
        //save image into database
        File file = new File(fName);
        byte[] bFile = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            //convert file into array of bytes
            fileInputStream.read(bFile);
            fileInputStream.close();
        } catch (IOException ex) {
            LogUtils.addLog(ex);

        }
        //cap nhat ho so da co barcode - binhnt add
        ImportOrderProductDAO iopdao = new ImportOrderProductDAO();
        ImportOrderProduct iopbo = iopdao.findByProductId(obj.getProductId());
        if (iopbo != null) {
            iopbo.setHaveQrCode(Constants.Status.ACTIVE);
            iopbo.setQrCode(bFile);
            iopdao.saveOrUpdate(iopbo);
        }
        file.deleteOnExit();
        return bFile;
    }

    public byte[] createQrCodeOnSigning(Long fileId, String barcodeInfo) {

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String dir = rb.getString("directoryQR");
        File folderExisting = new File(dir);
        if (!folderExisting.isDirectory()) {
            folderExisting.mkdir();
        }
        if (folderExisting.isDirectory()) {//tao folder theo ngay thang
            File temp = new File(dir);
            if (!temp.isDirectory()) {
                temp.mkdirs();
            }
        }
        Charset charset = Charset.forName("UTF-8");
        CharsetEncoder encoder = charset.newEncoder();
        byte[] b = null;
        ByteBuffer bbuf = null;
        try {
            bbuf = encoder.encode(CharBuffer.wrap(barcodeInfo));
        } catch (CharacterCodingException ex) {
            LogUtils.addLog(ex);
        }
        b = bbuf.array();

        String data;
        String fName = dir + fileId + "_QRCode.PNG";
        try {
            data = new String(b, "UTF-8");
            // get a byte matrix for the data
            BitMatrix matrix = null;
            int h = 500;
            int w = 500;
            Writer writer = new MultiFormatWriter();
            try {
                Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>(2);
                hints.put(EncodeHintType.CHARACTER_SET, "ISO-8859-1");
                matrix = writer.encode(data, BarcodeFormat.QR_CODE, w, h, hints);
            } catch (com.google.zxing.WriterException ex) {
                LogUtils.addLog(ex);
            }
            // change this path to match yours (this is my mac home folder, you can use: c:\\qr_png.png if you are on windows)
            File file = new File(fName);
            try {
                MatrixToImageWriter.writeToFile(matrix, "PNG", file);//System.out.println("printing to " + file.getAbsolutePath());
            } catch (IOException ex) {
                LogUtils.addLog(ex);
            }
        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLog(ex);
        }
        //save image into database
        File file = new File(fName);
        byte[] bFile = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            //convert file into array of bytes
            fileInputStream.read(bFile);
            fileInputStream.close();
        } catch (IOException ex) {
            LogUtils.addLog(ex);
        }
        //cap nhat ho so da co barcode - binhnt add
        file.deleteOnExit();
        return bFile;
    }

    public int createQrcode4ProductAdmin(Long fileId) {
        VFIofIopPDAO VFIofIopPDAO = new VFIofIopPDAO();
        List<VFIofIopP> lstProduct = VFIofIopPDAO.findProduct4FileId(fileId);
        int count = 0;
        if (!lstProduct.isEmpty()) {
            for (int i = 0; i < lstProduct.size(); i++) {
                createQrCodeOnSign4ProductAdmin(lstProduct.get(i));
                count++;
            }
        }
        return count;
    }

    public int createQrCodeOnListFiles(Long fileId) {
        String barcodeInfo = "";
        FilesDAOHE filehe = new FilesDAOHE();
        Files fileBo = filehe.findById(fileId);

        if (fileBo != null) {
            if (fileBo.getFileCode() != null) {
                barcodeInfo += "Ma ho so: " + fileBo.getFileCode();
            } else {
                barcodeInfo += "Ma ho so: N/A";
            }
            //lay ra thong tin xac nhan
            PermitDAO pdao = new PermitDAO();
            Permit pbo = pdao.findPermitByFileId(fileId);
            String soCongBo = "";
            Date ngayCongBo = null;
            if (pbo != null) {
                if (pbo.getReceiveNo() != null) {
                    barcodeInfo += "\nSo xac nhan: " + pbo.getReceiveNo();
                    soCongBo = pbo.getReceiveNo();
                } else {
                    barcodeInfo += "\nSo xac nhan: N/A";
                }
                if (pbo.getSignDate() != null) {
                    barcodeInfo += "\nNgay xac nhan: " + convertDateToString(pbo.getSignDate());
                } else {
                    barcodeInfo += "\nNgay xac nhan: N/A";
                }

            }
        } else {
            barcodeInfo = "San pham hien tai khong co thong tin";
        }
        byte[] bQrcode = createQrCodeOnSigning(fileId, barcodeInfo);
        if (fileBo != null && bQrcode != null) {
            fileBo.setHaveQrCode(Constants.Status.ACTIVE);
            fileBo.setQrCode(bQrcode);
            filehe.saveOrUpdate(fileBo);
        }
        VFIofIopPDAO VFIofIopPDAO = new VFIofIopPDAO();
        List<VFIofIopP> lstProduct = VFIofIopPDAO.findProduct4FileId(fileId);
        int count = 0;
        if (!lstProduct.isEmpty()) {
            for (int i = 0; i < lstProduct.size(); i++) {
                createQrCodeOnSign4ProductAdmin(lstProduct.get(i));
                count++;
            }
        }
        if (bQrcode != null && count > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    //ham xu ly lay so cong bo tu he thong cong bo san pham
    public byte[] createQrCodeOnSign4ProductSigning(ImportOrderProduct obj, String soCongBo, Date ngayCongBo, String businessPhone) {
        String barcodeInfo = "";
        if (obj != null) {
            if (obj.getNationalName() != null) {
                barcodeInfo += "X.xu: " + obj.getNationalName();
            } else {
                barcodeInfo += "X.xu: N/A";
            }
            if (obj.getConfirmAnnounceNo() != null) {
                barcodeInfo += "\nCong bo: " + obj.getConfirmAnnounceNo();
            } else {
                barcodeInfo += "\nCong bo: N/A";
            }
            if (soCongBo != null) {
                barcodeInfo += "\nXac nhan: " + soCongBo;
            } else {
                barcodeInfo += "\nXac nhan: N/A";
            }
            if (ngayCongBo != null) {
                barcodeInfo += "\nNgay: " + convertDateToString(ngayCongBo);
            } else {
                barcodeInfo += "\nNgay: N/A";
            }
            if (businessPhone != null) {
                barcodeInfo += "\nDt DN: " + businessPhone;
            } else {
                barcodeInfo += "\nDt DN: N/A";
            }
            barcodeInfo += "\nWebsite: moh.gov.vn";
        } else {
            barcodeInfo = "\nSan pham khong co thong tin";
        }

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String dir = rb.getString("directoryQR");
        File folderExisting = new File(dir);
        if (!folderExisting.isDirectory()) {
            folderExisting.mkdir();
        }
        if (folderExisting.isDirectory()) {//tao folder theo ngay thang
            File temp = new File(dir);
            if (!temp.isDirectory()) {
                temp.mkdirs();
            }
        }
        Charset charset = Charset.forName("UTF-8");
        CharsetEncoder encoder = charset.newEncoder();
        byte[] b = null;
        ByteBuffer bbuf = null;
        try {
            bbuf = encoder.encode(CharBuffer.wrap(barcodeInfo));
        } catch (CharacterCodingException ex) {
            LogUtils.addLog(ex);
        }
        b = bbuf.array();

        String data;
        String fName = dir + obj.getProductId() + "_QRCode.PNG";
        try {
            data = new String(b, "UTF-8");
            // get a byte matrix for the data
            BitMatrix matrix = null;
            int h = 500;
            int w = 500;
            Writer writer = new MultiFormatWriter();
            try {
                Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>(2);
                hints.put(EncodeHintType.CHARACTER_SET, "ISO-8859-1");
                matrix = writer.encode(data, BarcodeFormat.QR_CODE, w, h, hints);
            } catch (com.google.zxing.WriterException ex) {
                LogUtils.addLog(ex);
            }
            // change this path to match yours (this is my mac home folder, you can use: c:\\qr_png.png if you are on windows)
            File file = new File(fName);
            try {
                MatrixToImageWriter.writeToFile(matrix, "PNG", file);//System.out.println("printing to " + file.getAbsolutePath());
            } catch (IOException ex) {
                LogUtils.addLog(ex);
            }
        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLog(ex);
        }
        //save image into database
        File file = new File(fName);
        byte[] bFile = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            //convert file into array of bytes
            fileInputStream.read(bFile);
            fileInputStream.close();
        } catch (IOException ex) {
            LogUtils.addLog(ex);

        }
        //cap nhat ho so da co barcode - binhnt add
        ImportOrderProductDAO iopdao = new ImportOrderProductDAO();
        ImportOrderProduct iopbo = iopdao.findByProductId(obj.getProductId());
        if (iopbo != null) {
            iopbo.setHaveQrCode(Constants.Status.ACTIVE);
            iopbo.setQrCode(bFile);
            iopdao.saveOrUpdate(iopbo);
        }
        file.deleteOnExit();
        return bFile;
    }

    public byte[] createQrCodeOnSign4ProductAdmin(VFIofIopP obj) {
        String barcodeInfo = "";

        if (obj != null) {
            if (obj.getNationalName() != null) {
                barcodeInfo += "X.xu: " + obj.getNationalName();
            } else {
                barcodeInfo += "X.xu: N/A";
            }
            if (obj.getConfirmAnnounceNo() != null) {
                barcodeInfo += "\nCong bo: " + obj.getConfirmAnnounceNo();
            } else {
                barcodeInfo += "\nCong bo: N/A";
            }
            if (obj.getReceiveNo() != null) {
                barcodeInfo += "\nXac nhan: " + obj.getReceiveNo();
            } else {
                barcodeInfo += "\nXac nhan: N/A";
            }
            if (obj.getReceiveDate() != null) {
                barcodeInfo += "\nNgay: " + convertDateToString(obj.getReceiveDate());
            } else {
                barcodeInfo += "\nNgay: N/A";
            }
            if (obj.getBusinessPhone() != null) {
                barcodeInfo += "\nDt DN: " + obj.getBusinessPhone();
            } else {
                barcodeInfo += "\nDt DN: N/A";
            }
            barcodeInfo += "\nWebsite: moh.gov.vn";
        } else {
            barcodeInfo = "\nSan pham khong co thong tin";
        }

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String dir = rb.getString("directoryQR");
        File folderExisting = new File(dir);
        if (!folderExisting.isDirectory()) {
            folderExisting.mkdir();
        }
        if (folderExisting.isDirectory()) {//tao folder theo ngay thang
            File temp = new File(dir);
            if (!temp.isDirectory()) {
                temp.mkdirs();
            }
        }
        Charset charset = Charset.forName("UTF-8");
        CharsetEncoder encoder = charset.newEncoder();
        byte[] b = null;
        ByteBuffer bbuf = null;
        try {
            bbuf = encoder.encode(CharBuffer.wrap(barcodeInfo));
        } catch (CharacterCodingException ex) {
            LogUtils.addLog(ex);
        }
        b = bbuf.array();

        String data;
        String fName = dir + obj.getProductId() + "_QRCode.PNG";
        try {
            data = new String(b, "UTF-8");
            // get a byte matrix for the data
            BitMatrix matrix = null;
            int h = 500;
            int w = 500;
            Writer writer = new MultiFormatWriter();
            try {
                Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>(2);
                hints.put(EncodeHintType.CHARACTER_SET, "ISO-8859-1");
                matrix = writer.encode(data, BarcodeFormat.QR_CODE, w, h, hints);
            } catch (com.google.zxing.WriterException ex) {
                LogUtils.addLog(ex);
            }
            // change this path to match yours (this is my mac home folder, you can use: c:\\qr_png.png if you are on windows)
            File file = new File(fName);
            try {
                MatrixToImageWriter.writeToFile(matrix, "PNG", file);//System.out.println("printing to " + file.getAbsolutePath());
            } catch (IOException ex) {
                LogUtils.addLog(ex);
            }
        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLog(ex);
        }
        //save image into database
        File file = new File(fName);
        byte[] bFile = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            //convert file into array of bytes
            fileInputStream.read(bFile);
            fileInputStream.close();
        } catch (IOException ex) {
            LogUtils.addLog(ex);

        }
        //cap nhat ho so da co barcode - binhnt add
        ImportOrderProductDAO iopdao = new ImportOrderProductDAO();
        ImportOrderProduct iopbo = iopdao.findByProductId(obj.getProductId());
        if (iopbo != null) {
            iopbo.setHaveQrCode(Constants.Status.ACTIVE);
            iopbo.setQrCode(bFile);
            iopdao.saveOrUpdate(iopbo);
        }
        file.deleteOnExit();
        return bFile;
    }
    //tao qr code

    public byte[] createQrCodeOnSigning(Long fileId, String soCongBo, Date ngayCongBo) {
        byte[] bQrcode = null;
        String barcodeInfo = "";
        FilesDAOHE filehe = new FilesDAOHE();
        Files fileBo = filehe.findById(fileId);

        if (fileBo != null) {
            if (fileBo.getFileCode() != null) {
                barcodeInfo += "Ma ho so: " + fileBo.getFileCode();
            } else {
                barcodeInfo += "Ma ho so: N/A";
            }
            if (soCongBo != null) {
                barcodeInfo += "\nSo xac nhan: " + soCongBo;
            } else {
                barcodeInfo += "\nSo xac nhan: N/A";
            }
            if (ngayCongBo != null) {
                barcodeInfo += "\nNgay xac nhan: " + ngayCongBo;
            } else {
                barcodeInfo += "\nNgay xac nhan: N/A";
            }

        } else {
            barcodeInfo = "San pham hien tai khong co thong tin";
        }
        bQrcode = createQrCodeOnSigning(fileId, barcodeInfo);
        if (fileBo != null && bQrcode != null) {
            fileBo.setHaveQrCode(Constants.Status.ACTIVE);
            fileBo.setQrCode(bQrcode);
            filehe.saveOrUpdate(fileBo);
        }
        ImportOrderProductDAO iopdao = new ImportOrderProductDAO();
        List<ImportOrderProduct> lstProduct = iopdao.findAllIdByFileIdPassed(fileId);
        int count = 0;
        if (!lstProduct.isEmpty()) {
            for (int i = 0; i < lstProduct.size(); i++) {
                createQrCodeOnSign4ProductSigning(lstProduct.get(i), soCongBo, ngayCongBo, fileBo.getBusinessPhone());
                count++;
            }
        }
        return bQrcode;
    }
}
