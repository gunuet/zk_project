/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.BO.Annexe;
import com.viettel.module.cosmetic.Controller.CosmeticAttachDAO;
import com.viettel.module.importOrder.DAO.RtAssayDAO;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.RapidTest.VImportFileRtAttach;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author hungpv32
 */
public class RtAssayManagementController extends BaseComposer {

    @Wire
    Datebox dbDay;
    @Wire
    Window managertAssayWindow;
    @Wire
    Listbox lbManageHolidays, lbArrange;
    @Wire
    Paging userPagingBottom;
    RtAssay rtAssay;
    @Wire
    Textbox tbrtassayName, tbrtassaycode, tbrtAssayAddress;

    @Override
    public void doAfterCompose(Component window) {
        try {
            super.doAfterCompose(window);
            onSearch();
            fillDataToList(rtAssay);
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo); //To change body of generated methods, choose Tools | Templates.
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {
            userPagingBottom.setActivePage(0);
            rtAssay = new RtAssay();
            if (tbrtassayName.getValue() != null && !equals("".equals(tbrtassayName.getValue()))) {
                rtAssay.setName(tbrtassayName.getValue().trim());
            }
            if (tbrtassaycode.getValue() != null && !("".equals(tbrtassaycode.getValue()))) {
                rtAssay.setCode(tbrtassaycode.getValue().trim());
            }
            if (tbrtAssayAddress.getValue() != null && !equals("".equals(tbrtAssayAddress.getValue()))) {
                rtAssay.setAddress(tbrtAssayAddress.getValue().trim());
            }
            fillDataToList(rtAssay);
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    private void fillDataToList(RtAssay rtAssay1) {
        RtAssayDAO objhe = new RtAssayDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.searchForm(rtAssay1, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }
        @SuppressWarnings({"unchecked", "rawtypes"})
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbManageHolidays.setModel(lstModel);
    }

    @Listen("onReload=#managertAssayWindow")
    public void onReload() {
        onSearch();
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException {
        Window window = (Window) Executions.createComponents(
                "/Pages/admin/rtassay/createUpdateRtAssay.zul", null, null);
        window.doModal();

    }

    @Listen("onEdit=#lbManageHolidays")
    public void onUpdate(Event ev) throws IOException {
        RtAssay abo = (RtAssay) lbManageHolidays.getSelectedItem().getValue();
        Map args = new ConcurrentHashMap();
        args.put("id", abo.getId());
        Window window = (Window) Executions.createComponents("/Pages/admin/rtassay/createUpdateRtAssay.zul", null, args);
        window.doModal();
    }

    @SuppressWarnings("unchecked")
    @Listen("onDelete=#lbManageHolidays")
    public void doDeleteItem(ForwardEvent evt) {
        String message = String.format(Constants.Notification.DELETE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {
                                        RtAssay rs = (RtAssay) lbManageHolidays.getSelectedItem().getValue();
                                        RtAssayDAO daohe = new RtAssayDAO();
                                        daohe.delete(rs.getId());
                                        onSearch();
                                    } catch (Exception ex) {
                                        showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                        LogUtils.addLogDB(ex);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });

    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList(rtAssay);
    }
}
