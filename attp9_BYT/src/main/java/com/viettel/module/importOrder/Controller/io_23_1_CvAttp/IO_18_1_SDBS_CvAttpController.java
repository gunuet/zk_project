package com.viettel.module.importOrder.Controller.io_23_1_CvAttp;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.utils.Constants;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class IO_18_1_SDBS_CvAttpController extends BusinessController {
    
    @Wire
    Textbox txtMessage;
    private Long fileId;
    
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        
        return super.doBeforeCompose(page, parent, compInfo);
    }
    
    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
    }
    
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_18);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
        showNotification(String.format(Constants.Notification.SENDED_SUCCESS, "hồ sơ"), Constants.Notification.INFO);
    }
    
}
