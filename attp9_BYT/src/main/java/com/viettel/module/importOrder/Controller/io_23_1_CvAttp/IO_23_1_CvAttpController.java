package com.viettel.module.importOrder.Controller.io_23_1_CvAttp;

import com.viettel.core.base.DAO.AttachDAO;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VAttfileCategory;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.module.importOrder.DAO.ImportOrderAttachDao;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VAttfileCategoryDAO;
import com.viettel.module.importOrder.DAO.VProductTargetDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Vlayout;

/**
 *
 * @author Linhdx
 */
public class IO_23_1_CvAttpController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbtexLegal, producIdHidden, lbFileId, lbAttachFile, lbUploadFile, tbxWrnResult;
    // private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private Long fileId;
    private VFileImportOrder cosFile;
    @Wire
    Listbox lbOrderProduct;
    @Wire
    Listbox lbProductTarget, cbtexLegal;
    Long documentTypeCode;
    private Files files;
    private String nswFileCode;
    private List<Category> listImportOderFileType;
    private List<Media> listMedia;
    private final int IMPORT_ORDER_FILE = 1;
    @Wire
    private Textbox txtValidate, tbxResult;
    @Wire
    private Button btnAddNew, btnSave, btnAttach, btnCreate;

    private CosEvaluationRecord obj = new CosEvaluationRecord();
    @Wire
    private Window businessWindow;
    @Wire
    private Vlayout flist;
    @Wire
    private Listbox fileListbox, lbImportOrderFileType, fileListboxDn;
    private int flag_click = 0;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        listMedia = new ArrayList();
        //Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
        files = (new FilesDAOHE()).findById(fileId);
        //load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        nswFileCode = files.getNswFileCode();
        flag_click = 0;

        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("id", fileId);

        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findPassAndCheckMethodCode(fileId, 0L);
        ListModelArray lstModelManufacturer = new ListModelArray(importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);
        lbFileId.setValue(String.valueOf(fileId));
        if (flag_click == 0 && importOrderProducts.size() > 0) {
            ImportOrderProduct first_obj = importOrderProducts.get(0);
            setViewFirstClick(first_obj.getProductId());
        }
        flag_click = 1;

    }

    //load thông tin sản phẩm khilần đầu gọi from 
    public void setViewFirstClick(Long pr_id) {
        //get sanr pham
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(pr_id);

        //load lại danh sách tập tin theo productID
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS, pr_id);

        //set view
        producIdHidden.setValue(String.valueOf(pr_id));
        lbtexLegal.setVisible(true);
        btnSave.setVisible(true);
        fileListbox.setVisible(true);
        lbAttachFile.setVisible(true);
        lbImportOrderFileType.setVisible(true);
        lbUploadFile.setVisible(true);
        btnAttach.setVisible(true);
        btnCreate.setVisible(true);
        fileListboxDn.setVisible(true);
        tbxResult.setVisible(true);
        tbxResult.setValue(importOrderProduct.getResult());
        tbxWrnResult.setValue("");
    }

    @Listen("onOpenUpdate = #lbProductTarget")
    public void onOpenUpdate(Event event) {
        VProductTarget obj = (VProductTarget) event.getData();
        Long targetId = obj.getId();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("producId", producIdHidden.getValue());
        arguments.put("targetId", targetId);
        arguments.put("method", "update");
        setParam(arguments);
        createWindow("windowCRUDCosmetic", "/Pages/module/importorder/inputResultCheck/inputResCheckProTarget.zul", arguments, Window.HIGHLIGHTED);
    }

    @Listen("onDelete = #lbProductTarget")
    public void onDelete(Event event) {
        final VProductTarget obj = (VProductTarget) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

            @Override
            public void onEvent(Event event) {
                if (null != event.getName()) {
                    switch (event.getName()) {
                        case Messagebox.ON_OK:
                            // OK is clicked
                            try {

                                VProductTargetDAO objDAOHE = new VProductTargetDAO();
                                objDAOHE.delete(obj.getId());
                                reload();

                            } catch (Exception ex) {
                                showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                LogUtils.addLogDB(ex);
                            } finally {
                            }
                            break;
                        case Messagebox.ON_NO:
                            break;
                    }
                }
            }
        });
    }

    public void reload() {
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        List<VProductTarget> productProductTarget = new VProductTargetDAO().findByProdutId(productId);
        ListModelArray lstModel_ProducTarget = new ListModelArray(productProductTarget);
        lbProductTarget.setModel(lstModel_ProducTarget);
    }

    @Listen("onClick=#btnSave")
    public void onClickbtnbtnSave() {
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(productId);

        String txresult = tbxResult.getValue();
        if (txresult == null || "".equals(txresult)) {
            tbxWrnResult.setValue("Chưa nhập ghi chú xử lý");
            tbxWrnResult.setVisible(true);
            return;
        }
        //lưu kết quả
        importOrderProduct.setResult(tbxResult.getValue());
        importOrderProductDAO.saveOrUpdate(importOrderProduct);

        //load lai danh sachs san pham      
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findPassAndCheckMethodCode(fileId, 0L);
        ListModelArray lstModelManufacturer = new ListModelArray(importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);
        // tbxResult.setValue("");
        tbxWrnResult.setValue("Lưu kết quả thành công");
    }

    @Listen("onShow =  #lbOrderProduct")
    public void onShow(Event event) {
        ImportOrderProduct obj = (ImportOrderProduct) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        Long productId = obj.getProductId();
        arguments.put("id", productId);
        //get sanr pham
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO.findById(productId);
        //load lại danh sách tập tin theo productID
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS, productId);

        //set view
        producIdHidden.setValue(String.valueOf(productId));
        lbtexLegal.setVisible(true);
        btnSave.setVisible(true);
        fileListbox.setVisible(true);
        lbAttachFile.setVisible(true);
        lbImportOrderFileType.setVisible(true);
        lbUploadFile.setVisible(true);
        btnAttach.setVisible(true);
        btnCreate.setVisible(true);
        fileListboxDn.setVisible(true);
        tbxResult.setVisible(true);
        tbxResult.setValue(importOrderProduct.getResult());
        tbxWrnResult.setValue("");
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", businessWindow);
        return arguments;
    }

    @Listen("onChildWindowClosed=#businessWindow")
    public void onChildWindowClosed() {
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        fillListData(productId);
    }

    public void fillListData(Long productId) {
        List<VProductTarget> productProductTarget = new VProductTargetDAO().findByProdutId(productId);
        if (productProductTarget != null) {
            ListModelArray lstModel_ProducTarget = new ListModelArray(productProductTarget);
            lbProductTarget.setModel(lstModel_ProducTarget);
            lbProductTarget.setVisible(true);
        }

    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        clearWarningMessage();
        try {
            onApproveFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public void onApproveFile() throws Exception {
        if (!isValidate()) {
            return;
        }
        txtValidate.setValue("1");

    }

    public boolean isValidate() {
        VAttfileCategoryDAO categoryDAO = new VAttfileCategoryDAO();
        //load lai danh sachs san pham
        int checkAttach = 0;
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findPassAndCheckMethodCode(fileId, 0L);
        DANHSACH:
        for (ImportOrderProduct importOrder : importOrderProducts) {
            List<VAttfileCategory> listAttach = categoryDAO.findCheckedFilecAttach(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS,
                    importOrder.getProductId());
            if (listAttach == null || listAttach.size() == 0) {
                checkAttach++;
            }
        }

        if (checkAttach > 0) {
            showWarningMessage("Chưa có file đính kèm, vui lòng xem lại !!!");
            return false;
        }
        return true;
    }

    protected void showWarningMessage(String message) {
        lbBottomWarning.setValue(message);
    }

    private void clearWarningMessage() {
        lbBottomWarning.setValue("");
    }

    //attach File 
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;

        switch (type) {
            case IMPORT_ORDER_FILE:
                categoryDAOHE = new CategoryDAOHE();
                listImportOderFileType = categoryDAOHE.getSelectCategoryByParentCode(
                        Constants.CATEGORY_TYPE.FILETYPE_IMDOC, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS.toString());
                lstModel = new ListModelList(listImportOderFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    @Listen("onDownloadFileDN = #fileListboxDn")
    public void onDownloadFileDN(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        ImportOrderAttachDao rDAOHE = new ImportOrderAttachDao();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS, productId);
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("z-valign-left");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException, Exception {
        int idx = lbImportOrderFileType.getSelectedItem().getIndex();
        if (idx == 0) {
            showNotification("Bạn phải chọn loại hồ sơ", Constants.Notification.INFO);
            return;
        }
        if (listMedia.isEmpty()) {
            showNotification("Chọn tệp tải lên trước khi thêm mới!",
                    Constants.Notification.WARNING);

            return;
        }
        Long rtFileFileType = Long.valueOf((String) lbImportOrderFileType.getSelectedItem().getValue());
        AttachDAO base = new AttachDAO();
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        for (Media media : listMedia) {
            base.saveFileAttach(media, productId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS, rtFileFileType);
        }

        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS, productId);
        if ((listMedia != null) && (listMedia.size() > 0)) {
            listMedia.clear();
        }
        flist.getChildren().clear();

    }

    private void fillFileListbox(Long obj_type, Long obj_id) {
        VAttfileCategoryDAO dao = new VAttfileCategoryDAO();
        //danh sách tệp tien của chuyen vien
        List<VAttfileCategory> lstCosmeticAttach = dao.findCheckedFilecAttach(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS, obj_id);

        this.fileListbox.setModel(new ListModelArray(lstCosmeticAttach));
        //danh sách tệp tin doanh nghiệp theo sản phẩm
        List<VAttfileCategory> lstCosmeticAttachCV = dao.findCheckedFilecAttachDN(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS, obj_id);
        fileListboxDn.setModel(new ListModelArray(lstCosmeticAttachCV));
    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    private void setWarningMessage(String message) {

        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);

    }

    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        return Constants.CHECK_VIEW.VIEW;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }
}
