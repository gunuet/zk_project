package com.viettel.module.importOrder.Controller.payment;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.DAO.HolidaysManagementDAOHE;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VImportOrderPaymentInfo;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VImportOrderPaymentInfoDAO;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.controller.ViewAcceptBillController;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;

/**
 *
 */
public class AcceptPaymentFullController extends PaymentController {

    @Wire
    private Textbox txtPaymentName;
    @Wire
    private Textbox txtPaymentAddress;
    @Wire
    private Window businessWindow;
    @Wire
    Radiogroup rgTypePayment;
    @Wire
    Radio radioKeypay, radioBanktransfer, radioImmediaacy;
    @Wire
    private Label lbWarning, lbNgayTiepNhan, lbPaymentActionUser, lbPhase;
    @Wire
    private Label lbWarningUnder;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    private Window parentWindow;
    Long lsumMoney = 0L;
    // danh cho upload file
    @Wire
    Textbox txtBillNo;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    private Textbox txtPaymentConfirm;
    @Wire
    Textbox txtValidate;
    @Wire
    private Listbox lbFeePayment;
    @Wire
    private Label lbMahoso;
//    @Wire
//    private Label lbTensp;
    @Wire
    Groupbox gbReject;
    @Wire("#lbtextMoney")
    private Label lbtextMoney;
    Bill objBill = null;
    Long typePayment = 0L; // 1 radioKeypay //2 chuyen khoan // 3 truc tiep
    boolean bUploadFile = false, clickDeletefile = false;
    Long docId = null;
    Long docType = null;
    Long phase = null;
    private List<VImportOrderPaymentInfo> listPaymented;
    VImportOrderPaymentInfo vImportOrderPaymentInfo = null;
    //private String type;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("windowParent");
        docId = (Long) arguments.get("docId");
        docType = (Long) arguments.get("docType");
        phase = Long.valueOf(arguments.get("phase").toString());
        //lsumMoney = objBill.getTotal();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onAfterRender = #lbListPaymented")
    public void onAfterRender() {
    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    private void reloadModel() throws IOException {

        VImportOrderPaymentInfoDAO objDAOHE = new VImportOrderPaymentInfoDAO();

        listPaymented = objDAOHE.getListFeePayment(docId);
        if (listPaymented.size() > 0) {
            vImportOrderPaymentInfo = listPaymented.get(0);

            int sizeList = listPaymented.size();
            lsumMoney = 0l;
            for (int i = 0; i < sizeList; i++) {
                lsumMoney = lsumMoney + listPaymented.get(i).getCost();
            }

            lbMahoso.setValue(vImportOrderPaymentInfo.getCosmeticNo());
            //lbTensp.setValue(vCosPaymentInfo.getProductName());
            //txtPaymentAddress.setValue(vImportOrderPaymentInfo.getPaymentPerson());

            lbtextMoney.setValue("(Viết bằng chữ): " + numberToString(lsumMoney));
            BillDAO objBillDAOHE = new BillDAO();

            objBill = objBillDAOHE.findById(vImportOrderPaymentInfo.getBillId());

            if (objBill != null) {
                if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
                    rgTypePayment.setSelectedItem(radioKeypay);
                } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
                    rgTypePayment.setSelectedItem(radioBanktransfer);

                } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                    rgTypePayment.setSelectedItem(radioImmediaacy);
                    //   txtBillNo.setValue(objBill.getCode());
                }
                dbDayPayment.setValue(objBill.getPaymentDate());
                txtPaymentName.setValue(objBill.getCreatetorName());
                txtPaymentAddress.setValue(objBill.getCreatorAddress());
                setTypePayment();
                AttachDAOHE attDAOHE = new AttachDAOHE();
                List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
                if ((items != null) && (items.size() > 0)) {
                    try {
	                    //AMedia amedia = new AMedia(mAttach.getAttachName(), sType, WorkflowAPI.getCtypeFile(sType), is);
                        //iframeFile.setContent(amedia);
                        txtFileName.setValue(items.get(0).getAttachName());
                    } catch (Exception ex) {
                        LogUtils.addLogDB(ex);
                    }
                }
            }
        }
    }

    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            reloadModel();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }

        loadInforPayment();
        BookDocumentDAOHE bookDao = new BookDocumentDAOHE();
        if ((docId != null) && (docType != null)) {
            BookDocument mBook = bookDao.getBookInFromDocumentId(docId, docType);
            if (mBook != null) {
//               ;
            } else {
//                lbBookNumber.setValue("");
            }
        }
        lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));
        txtValidate.setValue("0");
       // txtBillNo.setFocus(true);

    }

    @Listen("onClick = #btnAccept")
    public void onAccept() {
        if (isValidate()) {
            PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
            PaymentInfo paymentInfo;
            int sizeList = listPaymented.size();
            for (int i = 0; i < sizeList; i++) {
                VImportOrderPaymentInfo app = listPaymented.get(i);
                paymentInfo = objFreePaymentInfo.findById(app.getPaymentInfoId());
                paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED); // trang thai moi tao
                paymentInfo.setDateConfirm(new Date());
                paymentInfo.setPaymentActionUser(getUserFullName());
                paymentInfo.setPaymentActionUserId(getUserId());
                paymentInfo.setNote(txtPaymentConfirm.getText().trim());
                objFreePaymentInfo.saveOrUpdate(paymentInfo);
                //System.out.print("gia tri"+app.getRapidTestNo());
            }

            if (objBill != null) {
                BillDAO objBillDAOHE = new BillDAO();
                objBill.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED);
                objBill.setConfirmDate(new Date());
                objBill.setConfirmUserId(getUserId());
                objBill.setConfirmUserName(getUserFullName());
	          //  objBill.setCode(txtBillNo.getValue().trim());

                objBillDAOHE.saveOrUpdate(objBill);
                objBillDAOHE.flush();
                showNotification(String.format(Constants.Notification.APROVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.BILL), Constants.Notification.INFO);
                businessWindow.onClose();

                FilesDAOHE filesDAOHE = new FilesDAOHE();
                Files files = filesDAOHE.findById(docId);
                files.setStartDate(new Date());
                files.setNumDayProcess(Constants.WARNING.NUM_DAY_PROCESS);
                HolidaysManagementDAOHE holidaysManagementDAOHE = new HolidaysManagementDAOHE();
                Date deadline = holidaysManagementDAOHE.getDeadline(files.getStartDate(), files.getNumDayProcess());
                files.setDeadline(deadline);
                filesDAOHE.saveOrUpdate(files);
            }

            txtValidate.setValue("1");
        }

    }

    //Load danh sach cac loai phi
    //:Thanhdv
    public void loadInforPayment() {

        List<PaymentInfo> lPaymentInfos = new PaymentInfoDAO().getListFeePayment(docId);
        ListModelArray lstModelManufacturer = new ListModelArray(lPaymentInfos);
        lbFeePayment.setModel(lstModelManufacturer);
    }

    @Listen("onClosePage = #windowsViewAcceptBill")
    public void onClosePage() {
        businessWindow.onClose();
        if (parentWindow != null) {
            Events.sendEvent("onReLoadPage", parentWindow, null);
        }
        //Events.sendEvent("onVisible", parentWindow, null);
    }

    private void setTypePayment() {
        if (rgTypePayment.getSelectedItem() != null) {
            if ("keypay".equals(rgTypePayment.getSelectedItem().getId())) {
                //gbTypePayment.setVisible(false);
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
            } else if ("immediaacy".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;

            } else if ("banktransfer".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;

            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        setTypePayment();

    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
        lbWarningUnder.setValue(message);
        lbWarning.setVisible(true);
        lbWarningUnder.setVisible(true);
    }

    @Listen("onClick = #btnDownload")
    public void onDownload() throws IOException {

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {

        onAccept();

        if (parentWindow != null) {
            Events.sendEvent("onClosePage", parentWindow, null);
        }
    }

    private boolean isValidate() {
        String message;
//        if ("".equals(txtBillNo.getText().trim())) {
//            message = "Không được để trống số biên lai";
//            txtBillNo.focus();
//            setWarningMessage(message);
//            return false;
//        }
        return true;
    }
}
