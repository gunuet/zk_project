/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.utils.Constants;

/**
 *
 * @author Linhdx
 */
public class EvaluationRecordDAO extends
        GenericDAOHibernate<EvaluationRecord, Long> {

    public EvaluationRecordDAO() {
        super(EvaluationRecord.class);
    }

    @Override
    public void saveOrUpdate(EvaluationRecord obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }

        getSession().flush();
    }

    @Override
    public EvaluationRecord findById(Long id) {
        Query query = getSession().getNamedQuery(
                "EvaluationRecord.findByEvaluationRecordId");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (EvaluationRecord) result.get(0);
        }
    }

    @Override
    public void delete(EvaluationRecord obj) {
        obj.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(obj);
    }
    
    public EvaluationRecord getLastEvaluation(Long fileId){
         Query query = getSession().createQuery("select a from EvaluationRecord a where a.fileId = :fileId "
                 + "order by a.evaluationRecordId desc");
         query.setParameter("fileId", fileId);
         List<EvaluationRecord> lst = query.list();
         if(lst.size()>0){
             return lst.get(0);
         }else{
             return null;
         }
    }
    
    public List<EvaluationRecord> getAllEvaluation(Long fileId){
         Query query = getSession().createQuery("select a from EvaluationRecord a where a.fileId = :fileId "
                 + "order by a.evaluationRecordId desc");
         query.setParameter("fileId", fileId);
         List<EvaluationRecord> lst = query.list();
         return lst;
//         if(lst.size()>0){
//             return lst;
//         }else{
//             return new ArrayList<>();
//         }
    }
    
    public int checkEvaluation(Long fileId){
        EvaluationRecord obj = getLastEvaluation(fileId);
        if(obj==null){
           return Constants.CHECK_VIEW.NOT_VIEW;
        }else{
            return Constants.CHECK_VIEW.VIEW;
        }
    }

}
