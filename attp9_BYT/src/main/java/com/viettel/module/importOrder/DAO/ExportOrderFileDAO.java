package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.VRtPaymentInfo;
import com.viettel.module.payment.DAO.VRtPaymentInfoDAO;
import com.viettel.module.rapidtest.model.ExportModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.WordExportUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.module.cosmetic.BO.CosAdditionalRequest;
import com.viettel.module.cosmetic.BO.CosCosfileIngre;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.BO.CosReject;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.importOrder.Model.ExportOrderFileModel;
import com.viettel.utils.FileUtil;
import com.viettel.utils.model.GroupModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Filedownload;

/**
 *
 * @author ChucHV
 */
public class ExportOrderFileDAO extends BaseComposer {

    private Long fileId;
    private String content;
    private String congvancapphepnhapkhau = "/WEB-INF/template/congvancapphepnhapkhau.docx"; //MinhNV - test congvancapphepnhapkhau
    private String bieumaubancongbo = "/WEB-INF/template/bancongbomypham.docx";//xuat giay cong bo cho doanh nghiep 03
    private String bieumaucongvantuchoi = "/WEB-INF/template/congvantuchoicapphep.docx";//xuat cong van tu choi
    private String bieumauthongbaosuadoibosung = "/WEB-INF/template/congvanyeucaubosung.docx";//xuat giay cong bo cho doanh nghiep 03
    private String bieumaubienlaithutienphilephi = "/WEB-INF/template/thutienphilephi.docx";//bien lai thu tien le phi
    private String bieumauphieubaothu = "/WEB-INF/template/phieubaothu.docx";//bien lai thu tien le phi
    private String bieumaugiayphep = "/WEB-INF/template/cos_permit_sign.docx";//xuat giay cong bo cho doanh nghiep 03
    private String giaydangkykiemtrathucphamnhapkhau = "/WEB-INF/template/giaydangkykiemtrathucphamnhapkhau.docx"; //MinhNV - test congvancapphepnhapkhau
    

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
    }

    private WordprocessingMLPackage prepareDataToExportCvSdbs(ExportModel model) {
        try {
            String sendNo = "0";
            if (model.getSendNo() != null) {
                sendNo = model.getSendNo();
            }
            String businessName = model.getBusinessName();
            Date signedDate = model.getSignDate();
            String signedDateStr = "Hà Nội, ngày " + " tháng " + " năm ";
            if (signedDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(signedDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                signedDateStr = "Hà Nội, ngày " + days + " tháng " + months + " năm " + years;
            }

            String signer = model.getSigner();
            String rolesigner = model.getRolesigner();
            String leaderSinged = model.getLeaderSinged();

            content = model.getContent();
            WordExportUtils wU = new WordExportUtils();

            if (model.getPathTemplate() != null) {
                bieumauthongbaosuadoibosung = model.getPathTemplate();
            }

            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(new FileInputStream(new File(bieumauthongbaosuadoibosung)));

            wU.replacePlaceholder(wmp, "BỘ Y TẾ", "${deptParent}");
            wU.replacePlaceholder(wmp, "CỤC QUẢN LÝ DƯỢC", "${receiptDeptName}");
            wU.replacePlaceholder(wmp, "Cục Quản lý dược ", "${receiptDeptNames}");
            wU.replacePlaceholder(wmp, sendNo, "${sendNo}");
            wU.replacePlaceholder(wmp, signedDateStr, "${signDateStr}");
            wU.replacePlaceholder(wmp, businessName, "${businessName}");
            wU.replacePlaceholder(wmp, signer, "${signer}");
            wU.replacePlaceholder(wmp, rolesigner, "${roleSigner}");
            wU.replacePlaceholder(wmp, leaderSinged, "${LeaderSigned}");

            wU.replacePlaceholder(wmp, content, "${contentDispatch}");
            ConcurrentHashMap map = new ConcurrentHashMap();

            wU.replacePlaceholder(wmp, map);
            return wmp;
        } catch (Docx4JException | IOException en) {
            LogUtils.addLogDB(en);
        }
        return null;

    }

    private WordprocessingMLPackage prepareDataToWord_BLTPLP(Bill mBill) {
        try {

            String businessName = mBill.getConfirmUserName();
            Date signedDate = mBill.getCreateDate();
            String signedDateStr = "Hà Nội, ngày " + " tháng " + " năm ";
            if (signedDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(signedDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                signedDateStr = "Hà Nội, ngày " + days + " tháng " + months + " năm " + years;
            }

            String signer = mBill.getConfirmUserName();
            String rolesigner = mBill.getConfirmUserName();
            String leaderSinged = mBill.getConfirmUserName();
            WordExportUtils wU = new WordExportUtils();

//            if (model.getPathTemplate() != null) {
//                bieumauthongbaosuadoibosung = model.getPathTemplate();
//            }
            //WordprocessingMLPackage wmp = WordprocessingMLPackage.load(new FileInputStream(new File(bieumaubienlaithutienphilephi)));
            String pathTemplate = getPathTemplate(mBill.getBillId());
            WordprocessingMLPackage wmp = null;
            if (pathTemplate != null) {
                File mTempFile = new File(pathTemplate);
                wmp = WordprocessingMLPackage.load(mTempFile);
                wU.replacePlaceholder(wmp, signedDateStr, "${businessName}");
                wU.replacePlaceholder(wmp, businessName, "${businessAddress}");
                wU.replacePlaceholder(wmp, signer, "${noMonney}");
                wU.replacePlaceholder(wmp, rolesigner, "${textMonney}");
                wU.replacePlaceholder(wmp, leaderSinged, "${typePayment}");
                ConcurrentHashMap map = new ConcurrentHashMap();
                wU.replacePlaceholder(wmp, map);
            }
            return wmp;
        } catch (Docx4JException en) {
            LogUtils.addLogDB(en);
        }
        return null;

    }

//    public void ExportPDF(ExportModel model) throws Docx4JException {
//        WordprocessingMLPackage temp = prepareDataToExportCvSdbs(model);
//        // 2) Prepare Pdf settings
//        try {
//            PdfSettings pdfSettings = new PdfSettings();
//
//            // 3) Convert WordprocessingMLPackage to Pdf
//            OutputStream out;
//
//            out = new FileOutputStream(new File(
//                    "E:/HelloWorld.pdf"));
//            PdfConversion converter = new org.docx4j.convert.out.pdf.viaXSLFO.Conversion(
//                    temp);
//            converter.output(out, pdfSettings);
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ExportDeviceDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    private WordprocessingMLPackage prepareDataToExportPermit(ExportModel model) {
        try {
            String sendNo = "0";
            if (model.getSendNo() != null) {
                sendNo = model.getSendNo();
            }
            Date signedDate = model.getSignDate();
            String signedDateStr = "Hà Nội, ngày " + " tháng " + " năm ";
            if (signedDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(signedDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                signedDateStr = "Hà Nội, ngày " + days + " tháng " + months + " năm " + years;
            }

            String signer = model.getSigner();
            String rolesigner = model.getRolesigner();
            String leaderSinged = model.getLeaderSinged();

            String rapidTestName = model.getRapidTestName();
            String placeOfManufacture = model.getPlaceOfManufacture();
            String businessName = model.getBusinessName();
            String businessAddress = model.getBusinessAddress();

            WordExportUtils wU = new WordExportUtils();

            if (model.getPathTemplate() != null) {
                bieumaugiayphep = model.getPathTemplate();
            }

            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(
                    new FileInputStream(new File(bieumaugiayphep)));

            wU.replacePlaceholder(wmp, "BỘ Y TẾ", "${deptParent}");

            wU.replacePlaceholder(wmp, "CỤC QUẢN LÝ DƯỢC", "${receiptDeptName}");
            wU.replacePlaceholder(wmp, "Cục Quản lý dược ", "${receiptDeptNames}");

            wU.replacePlaceholder(wmp, sendNo, "${sendNo}");

            wU.replacePlaceholder(wmp, signedDateStr, "${signDateStr}");
            wU.replacePlaceholder(wmp, businessName, "${businessName}");
            wU.replacePlaceholder(wmp, businessAddress, "${businessAddress}");
            wU.replacePlaceholder(wmp, signer, "${signer}");
            wU.replacePlaceholder(wmp, rolesigner, "${roleSigner}");
            wU.replacePlaceholder(wmp, leaderSinged, "${LeaderSigned}");

            wU.replacePlaceholder(wmp, placeOfManufacture, "${placeOfManufacture}");
            wU.replacePlaceholder(wmp, rapidTestName, "${rapidTestName}");

            ConcurrentHashMap map = new ConcurrentHashMap();

            wU.replacePlaceholder(wmp, map);

            return wmp;
        } catch (Docx4JException | IOException en) {
            LogUtils.addLogDB(en);

        }
        return null;

    }

    private WordprocessingMLPackage prepareDataToExportPermit_Cos(ExportModel model) {
        try {
            String sendNo = "0";
            if (model.getSendNo() != null) {
                sendNo = model.getSendNo();
            }
            Date signedDate = model.getSignDate();
            String signedDateStr = "Hà Nội, ngày " + " tháng " + " năm ";
            if (signedDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(signedDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                signedDateStr = "Hà Nội, ngày " + days + " tháng " + months + " năm " + years;
            }

            String signer = model.getSigner();
            String rolesigner = model.getRolesigner();
            String leaderSinged = model.getLeaderSinged();

            String rapidTestName = model.getRapidTestName();
            String placeOfManufacture = model.getPlaceOfManufacture();
            String businessName = model.getBusinessName();
            String businessAddress = model.getBusinessAddress();

            WordExportUtils wU = new WordExportUtils();

            if (model.getPathTemplate() != null) {
                // bieumaugiayphep = model.getPathTemplate();
            }
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            bieumaugiayphep = request.getRealPath(bieumaugiayphep);
            FileInputStream file1 = new FileInputStream(new File(bieumaugiayphep));
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(file1);

            wU.replacePlaceholder(wmp, "BỘ Y TẾ", "${deptParent}");

            wU.replacePlaceholder(wmp, "CỤC QUẢN LÝ DƯỢC", "${receiptDeptName}");
            wU.replacePlaceholder(wmp, "Cục Quản lý dược ", "${receiptDeptNames}");

            wU.replacePlaceholder(wmp, sendNo, "${sendNo}");

            wU.replacePlaceholder(wmp, signedDateStr, "${signDateStr}");
            wU.replacePlaceholder(wmp, businessName, "${businessName}");
            wU.replacePlaceholder(wmp, businessAddress, "${businessAddress}");
            wU.replacePlaceholder(wmp, signer, "${signer}");
            wU.replacePlaceholder(wmp, rolesigner, "${roleSigner}");
            wU.replacePlaceholder(wmp, leaderSinged, "${LeaderSigned}");

            wU.replacePlaceholder(wmp, placeOfManufacture, "${placeOfManufacture}");
            wU.replacePlaceholder(wmp, rapidTestName, "${rapidTestName}");

            ConcurrentHashMap map = new ConcurrentHashMap();

            wU.replacePlaceholder(wmp, map);

            return wmp;
        } catch (Docx4JException | IOException en) {
            LogUtils.addLogDB(en);

        }
        return null;

    }

    public void exportTempCvSdbs(ExportModel model) throws IOException {
//        String fileName = "CongvanSDBS_" + (new Date()).getTime() + ".docx";
//        WordprocessingMLPackage wmp = prepareDataToExportCvSdbs(model);
//        BaseGenericForwardComposer base = new BaseGenericForwardComposer();
        //base.downloadTemFile(wmp,fileName); 
    }

    @SuppressWarnings("CallToPrintStackTrace")
    public Boolean exportDataCvSdbs(ExportModel model) {
        try {

            int typeExport = model.getTypeExport();
            String fileName = "CongvanSDBS_" + (new Date()).getTime() + ".docx";
            Long objectId = model.getObjectId();
            Long objectType = model.getObjectType();
            WordprocessingMLPackage wmp = prepareDataToExportCvSdbs(model);
            switch (typeExport) {
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP:
                    AttachDAO base = new AttachDAO();
                    List<Attachs> lstAttach = getListSDBS(objectId, objectType);
                    base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    if (lstAttach.isEmpty()) {
                        base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    } else {
                        base.downloadFileAttach(lstAttach.get(0));
                    }

                    return true;
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_SIGN:
                    break;
            }
        } catch (IOException en) {
            LogUtils.addLogDB(en);
        }
        return false;
    }

    private List<Attachs> getListSDBS(Long objectId, Long objectType) {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        List<Attachs> lst = attachDAOHE.getAllByObjectIdAndType(objectId, objectType);
        if (lst != null && lst.size() > 0) {
            return lst;
        }
        return new ArrayList();
    }

    public Boolean exportPermit(ExportModel model) {
        try {

            int typeExport = model.getTypeExport();
            String fileName = "Giayphep_" + (new Date()).getTime() + ".docx";
            Long objectId = model.getObjectId();
            Long objectType = model.getObjectType();
            WordprocessingMLPackage wmp = prepareDataToExportPermit(model);
            switch (typeExport) {
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP:
                    AttachDAO base = new AttachDAO();
                    List<Attachs> lstAttach = getListAttach(objectId, objectType);
                    //test
                    base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    if (lstAttach.isEmpty()) {
                        base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    } else {
                        base.downloadFileAttach(lstAttach.get(0));
                    }
                    return true;
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_SIGN:
                    break;
            }
        } catch (IOException en) {
            LogUtils.addLogDB(en);
        }
        return false;
    }

    public Boolean exportBillPDF(Bill mBill) {
        try {

            String fileName = "thu_tien_phi_le_phi" + (new Date()).getTime() + ".docx";
            Long objectId = mBill.getBillId();
            WordprocessingMLPackage wmp = prepareDataToWord_BLTPLP(mBill);
            AttachDAO base = new AttachDAO();
            List<Attachs> lstAttach = getListAttach(objectId, Constants.OBJECT_TYPE.PAYMENT_EXPORT_PDF);
            //test
            base.saveFileAttach(wmp, fileName, objectId, Constants.OBJECT_TYPE.PAYMENT_EXPORT_PDF, null);
            if (lstAttach.isEmpty()) {
                base.saveFileAttach(wmp, fileName, objectId, Constants.OBJECT_TYPE.PAYMENT_EXPORT_PDF, null);
            } else {
                base.downloadFileAttach(lstAttach.get(0));
            }
        } catch (IOException en) {
            LogUtils.addLogDB(en);
        }
        return false;
    }

    private String getPathTemplate(Long billId) {
        List<VRtPaymentInfo> listFile;
        VRtPaymentInfoDAO objDAOHE = new VRtPaymentInfoDAO();
        listFile = objDAOHE.getListRequestPaymentBillId(getDeptId(), billId);
        if (listFile.size() > 0) {
//            Long fileId = listFile.get(0).getFileId();
//            //Get Template
//            WorkflowAPI wAPI = new WorkflowAPI();
//            Flow flow = wAPI.getFlowByFileId(fileId);
            TemplateDAOHE the = new TemplateDAOHE();
            String pathTemplate = the.findPathTemplate(Constants.PROCEDURE_TEMPLATE_TYPE.PHILEPHI);
            return pathTemplate;
        }
        return null;
    }

    public Boolean exportReject_Cos(ExportModel model) {
        try {

            int typeExport = 1;
            String fileName = "PhieuCongBoSanPhamMyPham_" + (new Date()).getTime() + ".docx";
            Long objectId = model.getObjectId();
            Long objectType = model.getObjectType();
            WordprocessingMLPackage wmp = prepareDataToExportPermit_Cos(model);
            switch (typeExport) {
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP:
                    AttachDAO base = new AttachDAO();
//                    List<Attachs> lstAttach = getListAttach(objectId, objectType);
                    //test
//                    if (lstAttach.isEmpty()) {
//                        base.saveFileAttach(wmp, fileName, objectId, objectType, null);
//                    } else {
//                        base.downloadFileAttach(lstAttach.get(0));
//                    }
                    base.saveFileAttach(wmp, fileName, objectId, objectType, null);
                    return true;
                case Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_SIGN:
                    break;
            }
        } catch (IOException en) {
            LogUtils.addLogDB(en);
        }
        return false;
    }

    private List<Attachs> getListAttach(Long objectId, Long objectType) {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        List<Attachs> lst = attachDAOHE.getAllByObjectIdAndType(objectId, objectType);
        if (lst != null && lst.size() > 0) {
            return lst;
        }
        return new ArrayList();
    }

    /**
     *
     * @param fileModel
     * @param b
     * @return
     */
    public PdfDocxFile exportCosmeticAnnouncement(FilesModel fileModel, boolean b) {
        PdfDocxFile outputStreamPdf = null;
//        ResourceBundle rb = ResourceBundle.getBundle("config");
//        String filePath = rb.getString("signTemp");//String filePath = "C:\\tempSign\\";        
//        String fileName = "PhieuCongBoSanPhamMyPham_" + (new Date()).getTime() + ".pdf";
        try {
            WordExportUtils wU = new WordExportUtils();
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            String path = request.getRealPath(bieumaubancongbo);
            //Neu co dinh nghia bieu mau thi lay tu dinh nghia
            if (fileModel.getPathTemplate() != null) {
                path = fileModel.getPathTemplate();
            }
            FileInputStream fileTemplate = new FileInputStream(new File(path));
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(fileTemplate);
            ConcurrentHashMap map = new ConcurrentHashMap();
            map.put("createForm", fileModel);
            wU.replacePlaceholder(wmp, map);
            wU.replaceTable(wmp, 1, fileModel.getLstProductTypes());
            wU.replaceTable(wmp, 2, fileModel.getLstPresentations());
            wU.replaceTable(wmp, 3, fileModel.getLstManufacturer());
            wU.replaceTable(wmp, 4, fileModel.getLstAssembler());
            List<GroupModel> lstGroups = new ArrayList();
            for (int i = 0; i < fileModel.getLstIngredient().size(); i++) {
                CosCosfileIngre item = (CosCosfileIngre) fileModel.getLstIngredient().get(i);
                if (item.getVariantOrShade() != null && !item.getVariantOrShade().trim().isEmpty()) {
                    boolean badd;
                    if (lstGroups.isEmpty()) {
                        badd = true;
                    } else {
                        badd = true;
                        for (GroupModel group : lstGroups) {
                            if (group.getGroupName().toLowerCase().equals(item.getVariantOrShade().toLowerCase())) {
                                group.getLstItems().add(item);
                                badd = false;
                                break;
                            }
                        }
                    }
                    if (badd) {
                        GroupModel model = new GroupModel();
                        model.setGroupName(item.getVariantOrShade());
                        model.setLstItems(new ArrayList());
                        model.getLstItems().add(item);
                        lstGroups.add(model);
                    }

                }
            }

            if (lstGroups.size() > 1) {
                wU.createGroupTable(wmp, 9, lstGroups);
                wU.deleteTable(wmp, 8);
            } else {
                wU.replaceTable(wmp, 8, fileModel.getLstIngredient());
                wU.deleteTable(wmp, 9);

            }
            outputStreamPdf = wU.writePDFToStream(wmp, b);
        } catch (Docx4JException | JAXBException | IOException en) {
            LogUtils.addLogDB(en);
        }
        return outputStreamPdf;
    }

    
      public PdfDocxFile exportDeviceFile(ExportOrderFileModel exportModel, boolean b) throws JAXBException {
        PdfDocxFile outputStreamPdf = null;
        
        try {
            WordExportUtils wU = new WordExportUtils();
            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            String path = request.getRealPath(giaydangkykiemtrathucphamnhapkhau);
            //Neu co dinh nghia bieu mau thi lay tu dinh nghia
            if (exportModel.getPathTemplate() != null) {
                path = exportModel.getPathTemplate();
            }                                  

            FileInputStream fileTemplate = new FileInputStream(new File(path));
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(fileTemplate);
            ConcurrentHashMap map = new ConcurrentHashMap();
            map.put("createForm", exportModel);
            wU.replacePlaceholder(wmp, map);

          wU.replaceTable(wmp, 0, exportModel.getLstProduct());

            outputStreamPdf = wU.writePDFToStream(wmp, b);
        } catch (Docx4JException  | JAXBException | IOException en) {
            LogUtils.addLogDB(en);
        }
        return outputStreamPdf;
        
    }
    
    ///////////
    /**
     * linhdx Xuat giay phep Luu vao bang Attach
     *
     * @param fileModel
     * @param b
     * @return
     */
    public void exportCosPermit(FilesModel fileModel, boolean b) throws IOException {
        String fileName = "PhieuCongBoSanPhamMyPham_" + (new Date()).getTime() + ".pdf";

        PdfDocxFile outputStreamPdf = exportCosmeticAnnouncement(fileModel, b);
        Long objectId = fileModel.getCosmeticPermitId();
        Long objectType = fileModel.getCosmeticPermitType();
        AttachDAO base = new AttachDAO();
        List<Attachs> lstAttach = getListAttach(objectId, objectType);
        if (lstAttach.isEmpty()) {
            base.saveFileAttach(outputStreamPdf, fileName, objectId, objectType, null);
        } else {
            base.downloadFileAttach(lstAttach.get(0));
        }
    }

    
     /**
     * linhdx xuat file ho so goc chua ky
     * @param fileModel
     * @param b
     * @return 
     */
    public String exportFinalFileNoSign(FilesModel fileModel, boolean b) {
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");//String filePath = "C:\\tempSign\\";  
        FileUtil.mkdirs(filePath);

        String fileName = "Hosogoc_" + (new Date()).getTime() + ".pdf";

        try {

            PdfDocxFile outputStreamPdf = exportCosmeticAnnouncement(fileModel, b);

            File f = new File(filePath + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            OutputStream outputStream = new FileOutputStream(f);
            outputStream.write(outputStreamPdf.getContent());
            outputStream.close();
        } catch (IOException en) {
            LogUtils.addLogDB(en);
        }
        return filePath + fileName;
    }
    //hieptq update 190315
    public String exportCosmeticAnnouncementNoSign(FilesModel fileModel, boolean b) {
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");//String filePath = "C:\\tempSign\\";  
        FileUtil.mkdirs(filePath);

        String fileName = "PhieuCongBoSanPhamMyPham_" + (new Date()).getTime() + ".pdf";

        try {

            PdfDocxFile outputStreamPdf = exportCosmeticAnnouncement(fileModel, b);

            File f = new File(filePath + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            OutputStream outputStream = new FileOutputStream(f);
            outputStream.write(outputStreamPdf.getContent());
            outputStream.close();
        } catch (IOException en) {
            LogUtils.addLogDB(en);
        }
        return filePath + fileName;
    }

    public void updateAttachSignFile(FilesModel fileModel, String fileName) {
        AttachDAO base = new AttachDAO();
        try {
            base.saveFileAttachPdfSign(fileName, fileModel.getCosmeticPermitId(), fileModel.getCosmeticPermitType(), null);

        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }

    }

    public void updateAttachSignFileSDBS(CosReject cosReject, String fileName) {
        AttachDAO base = new AttachDAO();
        try {
            base.saveFileAttachPdfSign(fileName, cosReject.getRejectId(), Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH, null);
            //base.saveFileAttachPdfSign(fileName, fileModel.getCosmeticPermitId(), fileModel.getCosmeticPermitType(), null);

        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }

    }

   
    public void exportAdditionalRequestDocument(Long userId, String userName, Long fileId, CosAdditionalRequest cosReject, boolean download) {
        try {
            CosEvaluationRecordDAO edao = new CosEvaluationRecordDAO();
            CosEvaluationRecord record = edao.getLastEvaluation(fileId);
            if (record == null) {
                throw new Exception("Không có bản đánh giá");
            }
            Date now = new Date();
            String date = String.valueOf(now.getDate());
            String month = String.valueOf(now.getMonth() + 1);
            String year = String.valueOf(now.getYear() + 1900);

            FilesModel model = new FilesModel(fileId);
            WordExportUtils wU = new WordExportUtils();

            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            String path = request.getRealPath(bieumauthongbaosuadoibosung);
            //Neu co dinh nghia bieu mau thi lay tu dinh nghia
            FileInputStream fileTemplate = new FileInputStream(new File(path));
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(fileTemplate);
            WordExportUtils.resolveFragmentText(wmp);
            wU.replacePlaceholder(wmp, cosReject.getReceiveNo(), "${documentNumber}");
            wU.replacePlaceholder(wmp, date, "${date}");
            wU.replacePlaceholder(wmp, month, "${month}");
            wU.replacePlaceholder(wmp, year, "${year}");
            wU.replacePlaceholder(wmp, model.getBusinessName(), "${businessName}");
            wU.replacePlaceholder(wmp, model.getBusinessAddress(), "${businessAddress}");
            wU.replacePlaceholder(wmp, model.getCosFile().getProductName(), "${productName}");
            wU.replacePlaceholder(wmp, record.getMainContent(), "${content}");
            wU.replacePlaceholder(wmp, userName, "${userName}");

            PdfDocxFile outputStream = wU.writePDFToStream(wmp, download);
            Long objectId = cosReject.getAdditionalRequestId();
            Long objectType = model.getCosmeticAdditionalType();
            String fileName = "congvansuadoibosung_" + (new Date()).getTime() + ".pdf";

            AttachDAO base = new AttachDAO();

            List<Attachs> lstAttach = getListAttach(objectId, objectType);
            if (lstAttach.isEmpty()) {

                base.saveFileAttach(outputStream, fileName, objectId, objectType, null);
            } else {
                base.downloadFileAttach(lstAttach.get(0));
            }
            //base.saveFileAttach(outputStream, fileName, cosReject.getRejectId(), Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH, null);
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
    }
    
     public PdfDocxFile exportRejectCommon(Long userId, String userName, Long fileId, CosReject cosReject) throws Exception {
        CosEvaluationRecordDAO edao = new CosEvaluationRecordDAO();
        CosEvaluationRecord record = edao.getLastEvaluation(fileId);
        if (record == null) {
            throw new Exception("Không có bản đánh giá");
        }

        UserDAOHE udhe = new UserDAOHE();
        Users u = udhe.findById(userId);

        String posName = u.getPosName();

        Date now = new Date();
        String date = String.valueOf(now.getDate());
        String month = String.valueOf(now.getMonth() + 1);
        String year = String.valueOf(now.getYear() + 1900);

        FilesModel model = new FilesModel(fileId);
        WordExportUtils wU = new WordExportUtils();

        HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
        String path = request.getRealPath(bieumaucongvantuchoi);
        //Neu co dinh nghia bieu mau thi lay tu dinh nghia
        FileInputStream fileTemplate = new FileInputStream(new File(path));
        WordprocessingMLPackage wmp = WordprocessingMLPackage.load(fileTemplate);
        WordExportUtils.resolveFragmentText(wmp);
        String documentNumber = null;
        if(cosReject !=null){
            documentNumber = cosReject.getReceiveNo();
        }
        wU.replacePlaceholder(wmp, documentNumber, "${documentNumber}");
        wU.replacePlaceholder(wmp, date, "${date}");
        wU.replacePlaceholder(wmp, month, "${month}");
        wU.replacePlaceholder(wmp, year, "${year}");
        wU.replacePlaceholder(wmp, model.getBusinessName(), "${businessName}");
        wU.replacePlaceholder(wmp, model.getBusinessAddress(), "${businessAddress}");
        wU.replacePlaceholder(wmp, model.getCosFile().getProductName(), "${productName}");
        wU.replacePlaceholder(wmp, record.getMainContent(), "${content}");
        wU.replacePlaceholder(wmp, posName, "${posName}");
        wU.replacePlaceholder(wmp, userName, "${userName}");

        PdfDocxFile outputStream = wU.writePDFToStream(wmp, false);
        
        return outputStream;
    }

    public void exportRejectDocument(Long userId, String userName, Long fileId, CosReject cosReject, boolean download) {
        try {
            PdfDocxFile outputStream = exportRejectCommon(userId, userName, fileId, cosReject);
            Long objectId = cosReject.getRejectId();
            Long objectType = Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH;
            String fileName = "congvantuchoi_";
            fileName += (new Date()).getTime() + ".pdf";
            AttachDAO base = new AttachDAO();

            List<Attachs> lstAttach = getListAttach(objectId, objectType);
            if (lstAttach.isEmpty()) {
                base.saveFileAttach(outputStream, fileName, objectId, objectType, null);
            } else {
                base.downloadFileAttach(lstAttach.get(0));
            }

            //base.saveFileAttach(outputStream, fileName, cosReject.getRejectId(), Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH, null);
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
    }

    public String exportRejectDocumentSign(Long userId, String userName, Long fileId, CosReject cosReject, boolean download) {
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");//String filePath = "C:\\tempSign\\";     
        FileUtil.mkdirs(filePath);
        String fileName = "Congvantuchoi_" + (new Date()).getTime() + ".pdf";
        try {
            OutputStream outputStream;
            PdfDocxFile output = exportRejectCommon(userId, userName, fileId, cosReject);
            File f = new File(filePath + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            outputStream = new FileOutputStream(f);
            outputStream.write(output.getContent());
            outputStream.close();
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
        return filePath + fileName;
    }

    public void exportRejectDocumentNosign(Long userId, String userName, Long fileId, CosReject cosReject, boolean download) {
        try {
            PdfDocxFile output = exportRejectCommon(userId, userName, fileId, cosReject);
            Filedownload.save(output.getContent(), "application/pdf", output.getFilename());
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
    }

    // exportFileLephi
    //quynhhv1
    public void exportPhieuThu(String businessName, String businessAddress,
            String noMonney, String textMonney, String typePayment, boolean download) {
        try {

            Date now = new Date();
            String date = String.valueOf(now.getDate());
            String month = String.valueOf(now.getMonth() + 1);
            if (month.length() == 1) {
                month = "0" + month;
            }
            String year = String.valueOf(now.getYear() + 1900);

            WordExportUtils wU = new WordExportUtils();

            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            String path = request.getRealPath(bieumaubienlaithutienphilephi);

            FileInputStream fileTemplate = new FileInputStream(new File(path));
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(fileTemplate);
            WordExportUtils.resolveFragmentText(wmp);
            wU.replacePlaceholder(wmp, businessName, "${businessName}");
            wU.replacePlaceholder(wmp, date, "${d}");
            wU.replacePlaceholder(wmp, month, "${m}");
            wU.replacePlaceholder(wmp, year, "${y}");
            wU.replacePlaceholder(wmp, businessAddress, "${businessAddress}");
            wU.replacePlaceholder(wmp, noMonney, "${noMonney}");
            wU.replacePlaceholder(wmp, textMonney, "${textMonney}");
            wU.replacePlaceholder(wmp, typePayment, "${typePayment}");

            PdfDocxFile outputStreamPdf = wU.writePDFToStream(wmp, download);
            //FilesModel model = new FilesModel(fileId);

            //Long objectType = model.getCosmeticRejectType();
            String fileName = "phieubaothu_" + (new Date()).getTime() + ".pdf";
            ResourceBundle rb = ResourceBundle.getBundle("config");
            String filePath = rb.getString("signTemp");//String filePath = "C:\\tempSign\\"; 
            FileUtil.mkdirs(filePath);
            FileUtil.mkdirs(filePath);
            File f = new File(filePath + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            OutputStream outputStream = new FileOutputStream(f);
            outputStream.write(outputStreamPdf.getContent());
            outputStream.close();
            Filedownload.save(f, filePath + fileName);
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
    }

    public void exportPhieuBaoThu(Date mDate,String businessName, String businessAddress,
            String noMonney, String textMonney,
            String paymentName, String paymentNo, String bookNumber, String creatorName, boolean download) {
        try {

            
            String date = String.valueOf(mDate.getDate());
            String month = String.valueOf(mDate.getMonth() + 1);
            if (month.length() == 1) {
                month = "0" + month;
            }
            String year = String.valueOf(mDate.getYear() + 1900);
            WordExportUtils wU = new WordExportUtils();

            HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
            String path = request.getRealPath(bieumauphieubaothu);

            FileInputStream fileTemplate = new FileInputStream(new File(path));
            WordprocessingMLPackage wmp = WordprocessingMLPackage.load(fileTemplate);
            WordExportUtils.resolveFragmentText(wmp);
            wU.replacePlaceholder(wmp, businessName, "${businessName}");
            wU.replacePlaceholder(wmp, date, "${d}");
            wU.replacePlaceholder(wmp, month, "${m}");
            wU.replacePlaceholder(wmp, year, "${y}");
            wU.replacePlaceholder(wmp, businessAddress, "${businessAddress}");
            wU.replacePlaceholder(wmp, noMonney, "${noMonney}");
            wU.replacePlaceholder(wmp, textMonney, "${textMonney}");
            wU.replacePlaceholder(wmp, paymentName, "${paymentName}");
            wU.replacePlaceholder(wmp, paymentNo, "${paymentNo}");
            wU.replacePlaceholder(wmp, creatorName, "${creatorName}");
            wU.replacePlaceholder(wmp, bookNumber, "${bookNumber}");

            PdfDocxFile outputStreamPdf = wU.writePDFToStream(wmp, download);
            //FilesModel model = new FilesModel(fileId);

            //Long objectType = model.getCosmeticRejectType();
            String fileName = "phieubaothu_" + (new Date()).getTime() + ".pdf";
            ResourceBundle rb = ResourceBundle.getBundle("config");
            String filePath = rb.getString("signTemp");//String filePath = "C:\\tempSign\\"; 
            FileUtil.mkdirs(filePath);
            FileUtil.mkdirs(filePath);
            File f = new File(filePath + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            OutputStream outputStream = new FileOutputStream(f);
            outputStream.write(outputStreamPdf.getContent());
            outputStream.close();
            Filedownload.save(f, filePath + fileName);
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
    }
}
