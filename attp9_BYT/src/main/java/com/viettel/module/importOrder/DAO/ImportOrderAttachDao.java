/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author THANHDV
 */
public class ImportOrderAttachDao extends
        GenericDAOHibernate<Attachs, Long> {

    public ImportOrderAttachDao() {
        super(Attachs.class);
    }

   

    @Override
    public void saveOrUpdate(Attachs rapidTestAttach) {
        if (rapidTestAttach != null) {
            super.saveOrUpdate(rapidTestAttach);
            //getSession().getTransaction().commit();
        }
    }

    @Override
    public Attachs findById(Long id) {
        Query query = getSession().getNamedQuery(
                "Attachs.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Attachs) result.get(0);
        }
    }

    @Override
    public void delete(Attachs attach) {
        attach.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(attach);
    }
    
    public void delete(Long id) {
        Query query = getSession().createQuery("update Attachs a set a.isActive = 0 where a.id = ?");
        query.setParameter(0, id);
        query.executeUpdate();
    }
}
