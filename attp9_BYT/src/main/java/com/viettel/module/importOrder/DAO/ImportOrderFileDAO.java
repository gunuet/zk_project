/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.VimportOrderFileAttach;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.model.SearchModel;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Thanhdv
 */
public class ImportOrderFileDAO extends
        GenericDAOHibernate<ImportOrderFile, Long> {

    public ImportOrderFileDAO() {
        super(ImportOrderFile.class);
    }

    @Override
    public void saveOrUpdate(ImportOrderFile ycnkFile) {
        if (ycnkFile != null) {
            super.saveOrUpdate(ycnkFile);
            getSession().flush();
        }
    }

    @Override
    public ImportOrderFile findById(Long id) {
        Query query = getSession().getNamedQuery(
                "ImportOrderFile.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ImportOrderFile) result.get(0);
        }
    }

    public Long countImportfile() {
        Query query = getSession().createQuery("select count(a) from ImportOrderFile a");
        Long count = (Long) query.uniqueResult();
        return count;
    }

    /*tichnv
     16/04/2015
     */
    public List findListStatusByCreatorId(Long creatorId) {//VFileImportOrder
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) FROM VFileImportOrder  n WHERE n.isActive = 1 AND n.status is not null ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);
        hql.append(" order by n.status desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }

    //thanhdv.
    public List<VimportOrderFileAttach> findImportOrderFilecAttach(Long fileId) {
        Query query = getSession().createQuery("select a from VimportOrderFileAttach a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");

        query.setParameter(0, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE);
        query.setParameter(1, fileId);
        List<VimportOrderFileAttach> lsroder = query.list();
        return lsroder;

    }

    public List<VimportOrderFileAttach> findImportOrderFileCheck(Long fileId) {
        Query query = getSession().createQuery("select a from VimportOrderFileAttach a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");

        query.setParameter(0, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK);
        query.setParameter(1, fileId);
        List<VimportOrderFileAttach> lsroder = query.list();
        return lsroder;

    }

    /*tichnv
     16/04/2015
     */
    public List findListStatusByReceiverAndDeptId(SearchModel searchModel, Long receiverId, Long receiveDeptId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            //+ " n.status = p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.sendUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileImportOrder n WHERE "
                            + " n.finishDate is not null"
                    //  + " n.fileId = p.objectId AND "
                    //  + " n.fileType = p.objectType AND "
                    //  + " n.status != p.status AND "
                    //  + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                    //  + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                    //  + " AND no.type = ? "
                    //  + " ) AND "
                    //+ " 1 = 1 ");
                    //        + " p.receiveUserId = ? "
                    );
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.finishDate is null "
                    //   + " (p.receiveUserId is null AND "
                    // + "AND p.receiveGroupId = ?  "
                    );
                    // listParam.add(receiveDeptId);
                    break;
                default:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }
        //  hql.append(" order by n.fileId desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }

    /*tichnv
     16/04/2015
     */

    public PagingListModel findFilesByCreatorId(SearchModel searchModel, Long creatorId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT n FROM VFileImportOrder n WHERE n.isActive = 1  ");
        StringBuilder strCountBuf = new StringBuilder("select count( n.cosFileId) FROM VFileImportOrder n WHERE n.isActive = 1  ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);

        if (searchModel != null) {
            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getModifyDateFrom() != null) {
                hql.append(" AND n.modifyDate >= ? ");
                listParam.add(searchModel.getModifyDateFrom());
            }
            if (searchModel.getModifyDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getModifyDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.fileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(n.cosmeticNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }

            if (searchModel instanceof CosmeticFileModel) {
                CosmeticFileModel cosmeticFileModel = (CosmeticFileModel) searchModel;
                if (cosmeticFileModel.getBrandName() != null && cosmeticFileModel.getBrandName().trim().length() > 0) {
                    hql.append(" AND lower(n.brandName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBrandName()));
                }

                if (cosmeticFileModel.getProductName() != null && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getProductName()));
                }

                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null && cosmeticFileModel.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessAddress()));
                }

                if (cosmeticFileModel.getDistributePersonName() != null && cosmeticFileModel.getDistributePersonName().trim().length() > 0) {
                    hql.append(" AND lower(n.distributePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getDistributePersonName()));
                }

                if (cosmeticFileModel.getIntendedUse() != null && cosmeticFileModel.getIntendedUse().trim().length() > 0) {
                    hql.append(" AND lower(n.intendedUse) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getIntendedUse()));
                }

            }

        }

        hql.append(" order by n.modifyDate desc,n.nswFileCode desc");

        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    /*tichnv
     16/04/2015
     */

    public PagingListModel findFilesByReceiverAndDeptId(SearchModel searchModel,
            Long receiverId, Long receiveDeptId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct n");
        StringBuilder strCountBuf = new StringBuilder("select count(distinct n.fileId)  ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    strBuf = new StringBuilder("SELECT n ");
                    hql.append("  FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            //+ " n.status = p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " p.sendUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileImportOrder n WHERE"
                            + " n.finishDate is not null "
                    //  + " n.fileType = p.objectType AND "
                    //+ " n.status != p.status AND "
                    //  + " p.finishDate is null AND "
                    //  + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                    //  + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                    //  + " AND no.type = ? "
                    //   + " ) AND "
                    //+ " 1 = 1 ");
                    //   + " p.sendUserId = ? "
                    );
                    //  listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    //  listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    //strBuf = new StringBuilder("SELECT n ");
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.nswFileCode !=null AND "
                            + " n.status = p.status ");
                    break;
                default:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }

        //listParam.add(receiverId);
        //listParam.add(receiveDeptId);
        if (searchModel != null) {

            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel != null && !Constants.MENU_TYPE.PROCESSED_STR.equals(searchModel.getMenuTypeStr())) {
                if (searchModel.getModifyDateFrom() != null) {
                    hql.append(" AND p.sendDate >= ? ");
                    listParam.add(searchModel.getModifyDateFrom());
                }
                if (searchModel.getModifyDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getModifyDateTo());
                    hql.append(" AND p.sendDate < ? ");
                    listParam.add(toDate);
                }
            } else {
                if (searchModel.getModifyDateFrom() != null) {
                    hql.append(" AND n.createDate >= ? ");
                    listParam.add(searchModel.getModifyDateFrom());
                }
                if (searchModel.getModifyDateTo() != null) {
                    Date toDate = DateTimeUtils.addOneDay(searchModel
                            .getModifyDateTo());
                    hql.append(" AND n.createDate < ? ");
                    listParam.add(toDate);
                }
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.fileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(n.cosmeticNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }
            if (searchModel.getDocumentTypeCode() != null) {
                hql.append(" AND n.documentTypeCode = ?");
                listParam.add(searchModel.getDocumentTypeCode());
            }

            if (searchModel instanceof CosmeticFileModel) {
                CosmeticFileModel cosmeticFileModel = (CosmeticFileModel) searchModel;
                if (cosmeticFileModel.getBrandName() != null && cosmeticFileModel.getBrandName().trim().length() > 0) {
                    hql.append(" AND lower(n.brandName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBrandName()));
                }

                if (cosmeticFileModel.getProductName() != null && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getProductName()));
                }

                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null && cosmeticFileModel.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessAddress()));
                }

                if (cosmeticFileModel.getDistributePersonName() != null && cosmeticFileModel.getDistributePersonName().trim().length() > 0) {
                    hql.append(" AND lower(n.distributePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getDistributePersonName()));
                }

                if (cosmeticFileModel.getIntendedUse() != null && cosmeticFileModel.getIntendedUse().trim().length() > 0) {
                    hql.append(" AND lower(n.intendedUse) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getIntendedUse()));
                }
                if (cosmeticFileModel.getTaxCode() != null && cosmeticFileModel.getTaxCode().trim().length() > 0) {
                    hql.append(" AND lower(n.taxCode) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getTaxCode()));
                }

            }

        }

        strBuf.append(hql);
        strCountBuf.append(hql);
        if (null != searchModel.getMenuTypeStr() && (searchModel.getMenuTypeStr().equals(Constants.MENU_TYPE.WAITPROCESS_STR))) {
            strBuf.append(" order by p.sendDate");
        } else {
            strBuf.append(" order by n.modifyDate desc");
        }
        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }
        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }
        Long count = (Long) countQuery.uniqueResult();
        List lst = query.list();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    /*tichnv
     16/04/2015
     */
    public int CheckGiayPhep(Long fileId, Long userID) {
        String HQL = " SELECT count(r) FROM CosPermit r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        //Check PERMIT_STATUS  da dong dau
        //query.setParameter("fileId", Constants_Cos.EVALUTION.PERMIT_STATUS.PROVIDED_NUMBER);
        Long count = (Long) query.uniqueResult();
        //neu co du lieu bang permit
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }

        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    /*tichnv
     16/04/2015
     */

    public int checkDispathSDBS(Long fileId, Long userID) {

        String HQL = " SELECT count(r) FROM AdditionalRequest r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    public ImportOrderFile findByFileId(Long id) {
//        Query query = getSession().getNamedQuery(
//                "CosFile.findByFileId");
        Query query = getSession().getNamedQuery(
                "ImportOrderFile.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ImportOrderFile) result.get(0);
        }
    }

    public Files getFileByFileId(Long fileId) {
        String hql = "select f from Files f where f.fileId = ?";
        Query query = session.createQuery(hql);
        query.setParameter(0, fileId);
        List<Files> lstFile = query.list();
        if (lstFile != null && lstFile.size() > 0) {
            return lstFile.get(0);
        }
        return null;
    }

    public List findByImportFileId(Long importFileId) {
        List<ImportDeviceProduct> lst;
        try {
            StringBuilder stringBuilder = new StringBuilder(" from ImportOrderFile a ");
            stringBuilder.append("  where  a.fileId = ? "
                    + " order by import_order_file_id asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, importFileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    //binhnt add 170112
    public List findListByStatus(SearchModel searchModel, Long status) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT n ");
        StringBuilder hql = new StringBuilder();

        hql.append(" FROM VFileImportOrder n, Process p WHERE"
                + " n.fileId = p.objectId AND "
                + " n.fileType = p.objectType AND "
                + " n.status = p.status AND "
                + " n.status = ? ");
        listParam.add(status);
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }
    //binhnt add 170112
}
