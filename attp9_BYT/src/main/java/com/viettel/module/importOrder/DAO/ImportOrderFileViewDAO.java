/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
//import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.model.SearchModel;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.hibernate.Query;


/**
 *
 * @author Thanhdv
 */
public class ImportOrderFileViewDAO extends
        GenericDAOHibernate<VFileImportOrder, Long> {

    public ImportOrderFileViewDAO() {
        super(VFileImportOrder.class);
    }

    @Override
    public void saveOrUpdate(VFileImportOrder ycnkFile) {
        if (ycnkFile != null) {
            super.saveOrUpdate(ycnkFile);
            getSession().getTransaction().commit();
        }
    }

    @Override
    public VFileImportOrder findById(Long id) {
        Query query = getSession().getNamedQuery(
                "VFileImportOrder.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFileImportOrder) result.get(0);
        }
    }
    
        public Long countImportfile() {
        Query query = getSession().createQuery("select count(a) from ImportOrderFile a");
        Long count = (Long) query.uniqueResult();
        return count;
    }
    
         /*tichnv
            16/04/2015
        */
    public List findListStatusByCreatorId(Long creatorId) {//VFileImportOrder
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) FROM VFileImportOrder  n WHERE n.isActive = 1 AND n.status is not null ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);
        hql.append(" order by n.status desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }
    
    
     /*tichnv
            16/04/2015
        */
    public List findListStatusByReceiverAndDeptId(SearchModel searchModel, Long receiverId, Long receiveDeptId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                            + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                            + " AND no.type = ? "
                            + " ) AND "
                            //+ " 1 = 1 ");
                            + " p.receiveUserId = ? ");
                    listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " (p.receiveUserId is null AND "
                            + " p.receiveGroupId = ? ) ");
                    listParam.add(receiveDeptId);
                    break;
                default:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }
        //  hql.append(" order by n.fileId desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }
     /*tichnv
            16/04/2015
        */
     public PagingListModel findFilesByCreatorId(SearchModel searchModel, Long creatorId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) FROM VFileImportOrder n WHERE n.isActive = 1  ");
        StringBuilder strCountBuf = new StringBuilder("select count(distinct n.cosFileId) FROM VFileImportOrder n WHERE n.isActive = 1  ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);

        if (searchModel != null) {
            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getModifyDateFrom() != null) {
                hql.append(" AND n.modifyDate >= ? ");
                listParam.add(searchModel.getModifyDateFrom());
            }
            if (searchModel.getModifyDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getModifyDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(n.cosmeticNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }

            if (searchModel instanceof CosmeticFileModel) {
                CosmeticFileModel cosmeticFileModel = (CosmeticFileModel) searchModel;
                if (cosmeticFileModel.getBrandName() != null && cosmeticFileModel.getBrandName().trim().length() > 0) {
                    hql.append(" AND lower(n.brandName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBrandName()));
                }

                if (cosmeticFileModel.getProductName() != null && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getProductName()));
                }

                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null && cosmeticFileModel.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessAddress()));
                }

                if (cosmeticFileModel.getDistributePersonName() != null && cosmeticFileModel.getDistributePersonName().trim().length() > 0) {
                    hql.append(" AND lower(n.distributePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getDistributePersonName()));
                }

                if (cosmeticFileModel.getIntendedUse() != null && cosmeticFileModel.getIntendedUse().trim().length() > 0) {
                    hql.append(" AND lower(n.intendedUse) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getIntendedUse()));
                }

            }

        }

        hql.append(" order by n.modifyDate desc,n.nswFileCode desc");

        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }
    /*tichnv
            16/04/2015
        */
      public PagingListModel findFilesByReceiverAndDeptId(SearchModel searchModel,
            Long receiverId, Long receiveDeptId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) ");
        StringBuilder strCountBuf = new StringBuilder("select count(distinct n.cosFileId) ");
        StringBuilder hql = new StringBuilder();
        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " p.finishDate is null AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status != p.status AND "
                            + " n.status in ( SELECT DISTINCT no.status FROM Node no WHERE "
                            + " no.flowId in (SELECT fl.flowId FROM Flow fl WHERE fl.isActive=1) "
                            + " AND no.type = ? "
                            + " ) AND "
                            //+ " 1 = 1 ");
                            + " p.receiveUserId = ? ");
                    listParam.add(Constants.NODE_TYPE.NODE_TYPE_FINISH);
                    listParam.add(receiverId);
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " (p.receiveUserId is null AND "
                            + " p.receiveGroupId = ? ) ");
                    listParam.add(receiveDeptId);
                    break;
                default:
                    hql.append(" FROM VFileImportOrder n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType AND "
                            + " n.status = p.status AND "
                            + " p.receiveUserId = ? ");
                    listParam.add(receiverId);
                    break;
            }
        }

        //listParam.add(receiverId);
        //listParam.add(receiveDeptId);
        if (searchModel != null) {

            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getModifyDateFrom() != null) {
                hql.append(" AND n.modifyDate >= ? ");
                listParam.add(searchModel.getModifyDateFrom());
            }
            if (searchModel.getModifyDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getModifyDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(n.cosmeticNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }
            if (searchModel.getDocumentTypeCode() != null) {
                hql.append(" AND n.documentTypeCode = ?");
                listParam.add(searchModel.getDocumentTypeCode());
            }

            if (searchModel instanceof CosmeticFileModel) {
                CosmeticFileModel cosmeticFileModel = (CosmeticFileModel) searchModel;
                if (cosmeticFileModel.getBrandName() != null && cosmeticFileModel.getBrandName().trim().length() > 0) {
                    hql.append(" AND lower(n.brandName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBrandName()));
                }

                if (cosmeticFileModel.getProductName() != null && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getProductName()));
                }

                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null && cosmeticFileModel.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessAddress()));
                }

                if (cosmeticFileModel.getDistributePersonName() != null && cosmeticFileModel.getDistributePersonName().trim().length() > 0) {
                    hql.append(" AND lower(n.distributePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getDistributePersonName()));
                }

                if (cosmeticFileModel.getIntendedUse() != null && cosmeticFileModel.getIntendedUse().trim().length() > 0) {
                    hql.append(" AND lower(n.intendedUse) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getIntendedUse()));
                }
                if (cosmeticFileModel.getTaxCode()!= null && cosmeticFileModel.getTaxCode().trim().length() > 0) {
                    hql.append(" AND lower(n.taxCode) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getTaxCode()));
                }

            }

        }

        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
                default:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
            }
        } else {
            hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
        }

        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }
    
      /*tichnv
            16/04/2015
        */
      
       public int CheckGiayPhep(Long fileId, Long userID) {
        String HQL = " SELECT count(r) FROM CosPermit r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        //Check PERMIT_STATUS  da dong dau
        //query.setParameter("fileId", Constants_Cos.EVALUTION.PERMIT_STATUS.PROVIDED_NUMBER);
        Long count = (Long) query.uniqueResult();
        //neu co du lieu bang permit
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }

        return Constants.CHECK_VIEW.NOT_VIEW;
    }
       /*tichnv
            16/04/2015
        */
        public int checkDispathSDBS(Long fileId, Long userID) {

        String HQL = " SELECT count(r) FROM CosAdditionalRequest r WHERE r.isActive=:isActive AND r.fileId=:fileId ";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }
        
        

    public List<VFileImportOrder> findOtherFileByTransportNo(String transportNo, Long fileId) {
        
//        List listParam = new ArrayList();
        String strBuf = "SELECT n FROM VFileImportOrder  n WHERE n.isActive = 1 "
                + " and n.transNo = ? and n.fileId <> ?";
        
        Query query = session.createQuery(strBuf);
        query.setParameter(0, transportNo);
        query.setParameter(1, fileId);
        List lst = query.list();
        return lst;
        
    }    

}
