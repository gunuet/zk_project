/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.Model.ImportOrderProductModel;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.LogUtils;
import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author THANHDV
 */
public class ImportOrderProductDAO extends
        GenericDAOHibernate<ImportOrderProduct, Long> {

    public ImportOrderProductDAO() {
        super(ImportOrderProduct.class);
    }

    @Override
    public void saveOrUpdate(ImportOrderProduct importProduct) {
        if (importProduct != null) {
            super.saveOrUpdate(importProduct);
            getSession().flush();
        }

    }

    public void deleteByFileId(Long fileId) {
        Query query = getSession().createQuery(
                "delete from ImportOrderProduct where fileId = :fileId");
        query.setParameter("fileId", fileId);
        query.executeUpdate();
    }

    public void setIsTempProduct(Long newfileId, Long oldfileId) {
        Query query = getSession()
                .createQuery(
                        "update ImportOrderProduct set fileId = :newfileId where fileId = :oldfileId");
        query.setParameter("newfileId", newfileId);
        query.setParameter("oldfileId", oldfileId);
        query.executeUpdate();
    }

    @Override
    public ImportOrderProduct findById(Long id) {
        Query query = getSession().getNamedQuery(
                "ImportOrderProduct.findByProductId");
        query.setParameter("productId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ImportOrderProduct) result.get(0);
        }
    }

    public ImportOrderProduct findByFileId(Long id) {
        Query query = getSession().getNamedQuery(
                "ImportOrderProduct.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ImportOrderProduct) result.get(0);
        }
    }
//binhnt add 22/2/17

    public ImportOrderProduct findByProductId(Long id) {
        Query query = getSession().getNamedQuery(
                "ImportOrderProduct.findByProductId");
        query.setParameter("productId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ImportOrderProduct) result.get(0);
        }
    }

    public ImportOrderProduct findByProductCode(Long fileId, String code) {
        Query query = getSession()
                .createQuery(
                        "Select p from ImportOrderProduct p where p.fileId = :fileId and p.productCode = :code");
        query.setParameter("fileId", fileId);
        query.setParameter("code", code);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ImportOrderProduct) result.get(0);
        }
    }

    // @Override
    // public void delete(YcnkProduct importProduct) {
    // importProduct.setIsDeleted2(Constants.TYPE_FILE.DELETE);
    // getSession().saveOrUpdate(importProduct);
    // }
    public List findAllIdByFileId(Long fileId) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder.append("  where  a.fileId = ?  "
                    + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findByCheckMethodCode(Long fileId) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder
                    .append("  where  a.fileId = ? and checkMethodCode in ('THUONG','CHAT') "
                            + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List<ImportOrderProduct> findPassAndCheckMethodCode(Long fileId,
            Long pass) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder
                    .append("  where  a.fileId = ? and a.pass = ? and checkMethodCode in ('THUONG','CHAT') "
                            + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            query.setParameter(1, pass);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return lst;
    }

    public List<ImportOrderProduct> findAllByFileIdAndReTest(Long fileId) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder.append("  where  a.fileId = ? AND a.reTest = 1"
                    + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List<ImportOrderProduct> findAllIdByFileIdAndPass(Long fileId,
            Long pass) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder.append("  where  a.fileId = ? and a.pass = ?"
                    + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            query.setParameter(1, pass);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return lst;
    }

    public List<ImportOrderProduct> findAllIdByFileIdAndCheckMethodCode(
            Long fileId, String checkMethodCode) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder
                    .append("  where  a.fileId = ? and a.checkMethodCode = ?"
                            + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            query.setParameter(1, checkMethodCode);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return lst;
    }

    public List<ImportOrderProduct> findAllIdByFileIdAndCheckMethodCodeAndNotPass(
            Long fileId, String checkMethodCode) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder
                    .append("  where  a.fileId = ? and a.checkMethodCode = ? and a.pass <> 1 "
                            + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            query.setParameter(1, checkMethodCode);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return lst;
    }

    public List<Long> findOrderIdByFileId(Long fileId) {
        List<Long> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder.append("  where  a.fileId = ? "
                    + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List findAllProducByFileIdAndCheckMeThod(Long fileId) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder
                    .append("  where  a.fileId = ? AND a.checkMethodCode = 'GIAM'"
                            + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List getImportOrderProductList(Long fileId) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder.append("  where  a.fileId = ? "
                    + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public List getImportOrderProductList(Long fileId, Boolean reduce) {
        List<ImportOrderProduct> lst = null;
        try {
            String code = Constants.NSW_SERVICE.TYPE_GIAM_HT;
            StringBuilder stringBuilder = new StringBuilder(
                    "select a from ImportOrderProduct a ");
            if (reduce) {
                stringBuilder
                        .append("  where  a.fileId = ? and a.checkMethodCode = ?  ");
            } else {
                stringBuilder
                        .append("  where  a.fileId = ? and a.checkMethodCode != ?  ");
            }
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            query.setParameter(1, code);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }

    public void delete(Long id) {
        ImportOrderProduct obj = findById(id);
        delete(obj);
    }

    public List<ImportOrderProductModel> convertObjList(
            List<ImportOrderProduct> productList) {

        List<ImportOrderProductModel> list_resul = null;

        DecimalFormat format = new DecimalFormat("###,###,###,###,###.###");
        if (productList != null) {
            list_resul = new ArrayList<ImportOrderProductModel>();
            for (ImportOrderProduct imp : productList) {
                ImportOrderProductModel objConvert = new ImportOrderProductModel();

                objConvert.setBaseUnit(imp.getBaseUnit());
                objConvert.setCheckMethodCode(imp.getCheckMethodCode());
                objConvert.setCheckMethodName(imp.getCheckMethodName());
                objConvert.setComments(imp.getComments());
                objConvert.setConfirmAnnounceNo(imp.getConfirmAnnounceNo());
                objConvert.setDateOfManufacturer(imp.getDateOfManufacturer());
                objConvert.setFileId(imp.getFileId());
                objConvert.setExpiredDate(imp.getExpiredDate());
                objConvert.setHsCode(imp.getHsCode());
                objConvert.setManufacTurer(imp.getManufacTurer());
                objConvert.setManufacturerAddress(imp.getManufacturerAddress());
                objConvert.setNationalCode(imp.getNationalCode());
                objConvert.setNationalName(imp.getNationalName());
                objConvert.setNetweight(imp.getNetweight());
                objConvert.setNetweightUnitCode(imp.getNetweightUnitCode());
                objConvert.setNetweightUnitName(imp.getNetweightUnitName());
                objConvert.setPass(String.valueOf(imp.getPass()));
                objConvert.setProcessType(imp.getProcessType());
                objConvert.setProcessTypeName(imp.getProcessTypeName());
                objConvert.setProductCode(imp.getProductCode());
                objConvert.setProductName(imp.getProductName());
                objConvert.setProductDescription(imp.getProductDescription());
                objConvert.setProductId(imp.getProductId());
                objConvert.setProductNumber(imp.getProductNumber());
                objConvert.setReason(imp.getReason());
                objConvert.setResult(imp.getResult());
                objConvert.setReTest(imp.getReTest());
                objConvert.setReProcess(imp.getReProcess());
                objConvert.setTotal(imp.getTotal());
                objConvert.setTotalUnitCode(imp.getTotalUnitCode());
                objConvert.setTotalUnitName(imp.getTotalUnitName());
                objConvert.setV_baseUnit(format.format(imp.getBaseUnit()) + " VNĐ");
                objConvert.setV_netweight(format.format(imp.getNetweight()) + (imp.getNetweightUnitName() == null ? "" : " " + imp.getNetweightUnitName()));
                objConvert.setV_total(format.format(imp.getTotal()) + (imp.getTotalUnitName() == null ? "" : " " + imp.getTotalUnitName()));

                list_resul.add(objConvert);

            }
        }
        return list_resul;
    }

//check ton tai qrcode khong binhnt
    public int checkHaveQrCode(Long productId) {
        String HQL = " SELECT count(r) FROM ImportOrderProduct r WHERE r.productId=:productId AND r.haveQrCode = :haveQrCode";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("productId", productId);
        query.setParameter("haveQrCode", Constants_Cos.Status.ACTIVE);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    public List findAllIdByFileIdPassed(Long fileId) {
        List<ImportOrderProduct> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder(
                    " from ImportOrderProduct a ");
            stringBuilder.append(" where  a.fileId = ? and a.pass=1"
                    + " order by productId asc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, fileId);
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }

        return lst;
    }
}
