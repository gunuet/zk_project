/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.ProcessDocument;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author E5420
 */
public class ProcessDocumentDAO extends
        GenericDAOHibernate<ProcessDocument, Long> {

    public ProcessDocumentDAO() {
        super(ProcessDocument.class);
    }

    @Override
    public void saveOrUpdate(ProcessDocument processDoc) {
        if (processDoc != null) {
            super.saveOrUpdate(processDoc);
            getSession().getTransaction().commit();
        }
    }

    @Override
    public ProcessDocument findById(Long id) {
        Query query = getSession().getNamedQuery(
                "ProcessDocument.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ProcessDocument) result.get(0);
        }
    }

    @Override
    public void delete(ProcessDocument processDoc) {
        processDoc.setIsActive(0L);
        getSession().saveOrUpdate(processDoc);
    }

    @SuppressWarnings({"rawtypes"})
    public ProcessDocument getProcessDocumentByType(Long type, Long fileId) {
    	Query query = getSession().createQuery("Select p from ProcessDocument p where p.fileId = :fileId and p.type = :type and ( p.isActive is null or p.isActive = 1 ) order by createdDate desc ");
        query.setParameter("fileId", fileId);
        query.setParameter("type", type);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ProcessDocument) result.get(0);
        }
    }
}
