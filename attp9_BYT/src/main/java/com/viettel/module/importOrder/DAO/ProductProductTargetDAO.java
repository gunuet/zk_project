/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.ProductProductTarget;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Thanhdv
 */
public class ProductProductTargetDAO extends
		GenericDAOHibernate<ProductProductTarget, Long> {

	public ProductProductTargetDAO() {
		super(ProductProductTarget.class);
	}

	@Override
	public void saveOrUpdate(ProductProductTarget ppt) {
		if (ppt != null) {
			super.saveOrUpdate(ppt);
			getSession().getTransaction().commit();
		}
	}

	@Override
	public void delete(ProductProductTarget ppt) {
		ppt.setIsActive(0L);
		getSession().saveOrUpdate(ppt);
	}

	@Override
	public ProductProductTarget findById(Long id) {
		Query query = getSession().getNamedQuery(
				"ProductProductTarget.findById");
		query.setParameter("id", id);
		List result = query.list();
		if (result.isEmpty()) {
			return null;
		} else {
			return (ProductProductTarget) result.get(0);
		}
	}

	public List<ProductProductTarget> findByProductId(Long productId) {
		Query query = getSession().getNamedQuery(
				"ProductProductTarget.findByProductId");
		query.setParameter("productId", productId);
		List result = query.list();
                return result;
		
	}

	public Long countImportfile() {
		Query query = getSession().createQuery(
				"select count(a) from ProductProductTarget a");
		Long count = (Long) query.uniqueResult();
		return count;
	}

}
