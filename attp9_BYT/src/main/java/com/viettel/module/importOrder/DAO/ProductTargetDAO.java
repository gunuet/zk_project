/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.ProductTarget;
import com.viettel.module.cosmetic.BO.Annexe;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.ProductProductTarget;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

public class ProductTargetDAO extends GenericDAOHibernate<ProductTarget, Long> {

	public ProductTargetDAO() {
		super(ProductTarget.class);
	}

	public List<String> findAllType() {
		String sql = new String(
				" SELECT distinct p.type FROM ProductTarget p WHERE p.isActive = 1");
		Query query = getSession().createQuery(sql.toString());
		return query.list();
	}

	public List<ProductTarget> findAll() {
		Query query = getSession().getNamedQuery("ProductTarget.findAll");
		List result = query.list();
		return result;
	}

	public List<ProductTarget> findAllTargetByCategory(String code) {
		String sql = new String(
				" SELECT distinct p FROM ProductTarget p WHERE p.isActive = 1 AND p.type = :type");
		Query query = getSession().createQuery(sql.toString());
		query.setParameter("type", code);

		return query.list();
	}

	public void delete(Long id) {
		ProductTarget obj = findById(id);
		obj.setIsActive(0l);
		update(obj);
	}

	public ProductTarget findId(Long targetId) {
		String sql = new String(
				" SELECT distinct p FROM ProductTarget p WHERE p.isActive = 1 AND p.id = :targetId");
		Query query = getSession().createQuery(sql.toString());
		query.setParameter("targetId", targetId);

		List<ProductTarget> list = query.list();

		return list.isEmpty() ? null : list.get(0);
	}

	// serach
	public PagingListModel search(ProductTarget productTarget, int start, int take) {
		List<String> listParam = new ArrayList();
		try {
			StringBuilder strBuf = new StringBuilder(
					"select a from ProductTarget a where a.isActive =1 ");
			StringBuilder strCountBuf = new StringBuilder(
					"select count(a) from ProductTarget a where a.isActive = 1 ");
			StringBuilder hql = new StringBuilder();
			if (productTarget != null) {
				if (productTarget.getName() != null && !"".equals(productTarget.getName().trim())) {
					hql.append(" and lower(a.name) like ? escape '/' ");
					listParam.add(StringUtils.toLikeString(productTarget.getName()));
				}
				if (productTarget.getCode() != null && !"".equals(productTarget.getCode().trim())) {
					hql.append(" and lower(a.code) like ? escape '/' ");
					listParam.add(StringUtils.toLikeString(productTarget.getCode()));
				}
				if (productTarget.getTestMethod() != null&& !"".equals(productTarget.getTestMethod().trim())) {
					hql.append(" and lower(a.testMethod) like ? escape '/' ");
					listParam.add(StringUtils.toLikeString(productTarget.getTestMethod()));
				}
				if (productTarget.getType() != null && !"".equals(productTarget.getType().trim())) {
					hql.append(" and lower(a.type) like ? escape '/' ");
					listParam.add(StringUtils.toLikeString(productTarget.getType()));
				}

			}
			strBuf.append(hql);
			strCountBuf.append(hql);

			Query query = session.createQuery(strBuf.toString());
			Query countQuery = session.createQuery(strCountBuf.toString());

			for (int i = 0; i < listParam.size(); i++) {
				query.setParameter(i, listParam.get(i));
				countQuery.setParameter(i, listParam.get(i));
			}
			query.setFirstResult(start);
			if (take < Integer.MAX_VALUE) {
				query.setMaxResults(take);
			}

			List lst = query.list();
			Long count = (Long) countQuery.uniqueResult();
			PagingListModel model = new PagingListModel(lst, count);
			return model;
		} catch (Exception ex) {
			LogUtils.addLogDB(ex);
			return null;
		}
	}

}
