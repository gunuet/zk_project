/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.module.rapidtest.BO.Template;
import com.viettel.utils.Constants;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class RtAssayDAO extends GenericDAOHibernate<RtAssay, Long> {

    public RtAssayDAO() {
        super(RtAssay.class);
    }

    public boolean hasDuplicate(RtAssay rs) {
        StringBuilder hql = new StringBuilder("select count(r) from RtAssay r where r.isActive = 1 ");
        List lstParams = new ArrayList();
        if (rs.getId() != null && rs.getId() > 0L) {
            hql.append(" and r.RtAssay <> ?");
            lstParams.add(rs.getId());
        }

        Query query = session.createQuery(hql.toString());
        for (int i = 0; i < lstParams.size(); i++) {
            query.setParameter(i, lstParams.get(i));
        }
        Long count = (Long) query.uniqueResult();
        return count > 0L;
    }

    public void delete(Long id) {
        RtAssay obj = findById(id);
        obj.setIsActive(0l);
        update(obj);
    }

    public PagingListModel searchForm(RtAssay rtAssay, int start, int take) {
        List listParam = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder("select r from RtAssay r where r.isActive = 1 ");
            StringBuilder strCountBuf = new StringBuilder("select count(r) from RtAssay r where r.isActive = 1 ");
            StringBuilder hql = new StringBuilder();
            if (rtAssay != null) {

                if (rtAssay.getName() != null && !"".equals(rtAssay.getName())) {
                    hql.append(" and lower(r.name) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(rtAssay.getName()));
                }
                if (rtAssay.getCode() != null && !"".equals(rtAssay.getCode()) ) {
             	   hql.append(" and lower(r.code) like ? escape '/' ");
                    listParam.add(StringUtils.toLikeString(rtAssay.getCode()));
                 } 
                if (rtAssay.getAddress() != null && !"".equals(rtAssay.getAddress())) {
          	   hql.append(" and lower(r.address) like ? escape '/' ");
               listParam.add(StringUtils.toLikeString(rtAssay.getAddress()));
                 }  

            }
            strBuf.append(hql);
            strCountBuf.append(hql);

            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }
            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    
    }
    
    
}
