/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.VAttfileCategory;
import com.viettel.module.importOrder.BO.VimportOrderFileAttach;
import com.viettel.utils.Constants;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Tichnv
 */
public class VAttfileCategoryDAO extends
        GenericDAOHibernate<VAttfileCategory, Long> {

    public VAttfileCategoryDAO() {
        super(VAttfileCategory.class);
    }

    public List<VAttfileCategory> findCheckedFilecAttach(Long bbj_type, Long obj_id) {
        Query query = getSession().createQuery("select a from VAttfileCategory a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");
        query.setParameter(0, bbj_type);
        query.setParameter(1, obj_id);
        List<VAttfileCategory> lsroder = query.list();
        return lsroder;

    }
     public List<VAttfileCategory> findCheckedFilecAttachDN(Long bbj_type,Long bbj_type_no, Long obj_id) {
        Query query = getSession().createQuery("select a from VAttfileCategory a "
                + "where (a.attachCat = ? or attachCat= ?)"
                + "and a.objectId = ? "
                + "and a.isActive = 1");
        query.setParameter(0, bbj_type);
        query.setParameter(1, bbj_type_no);
        query.setParameter(2, obj_id);
        List<VAttfileCategory> lsroder = query.list();
        return lsroder;

    }
}
