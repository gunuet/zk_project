/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VCountCheck;
import com.viettel.module.importOrder.BO.VCountStatusFile;
import com.viettel.utils.LogUtils;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;

/**
 *
 * @author Tichnv
 */
public class VCountStatusFileDAO extends
        GenericDAOHibernate<VCountCheck, Long> {

    public VCountStatusFileDAO() {
        super(VCountCheck.class);
    }

    public Long countNumberUnprocess(String dep_id) {

        Query query = getSession().createQuery("select count(a) from VCountStatusFile a  where deptPath like  ? ");
        query.setParameter(0, "%/" + dep_id + "/%");
        Long count = (Long) query.uniqueResult();

        return count;
    }

    public Long countCheckProduct(String productName, Long creatorId) {

        Query query = getSession().createQuery("select count(a) from VCountCheck a where creatorId = ? and  productName like  ?");
        query.setParameter(0, creatorId);
        query.setParameter(1, "%" + productName.toLowerCase() + "%");
        Long count = (Long) query.uniqueResult();

        return count;
    }
    
    public List<VCountCheck> getHisstryProduct(Long creatorId, String confirmAnnounceNo) {
        List<VCountCheck> lst = null;
        try {
            StringBuilder stringBuilder = new StringBuilder("select a from VCountCheck a ");
            stringBuilder.append("  where creatorId = ? and confirmAnnounceNo = ?  and rownum <= 10"
                    + " order by createDate desc) ");
            Query query = getSession().createQuery(stringBuilder.toString());
            query.setParameter(0, creatorId);
            query.setParameter(1, confirmAnnounceNo);
            
            lst = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return lst;
    }
}
