/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importOrder.BO.VFIofIopP;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author binh.ninhthanh
 */
public class VFIofIopPDAO extends GenericDAOHibernate<VFIofIopP, Long> {

    public VFIofIopPDAO() {
        super(VFIofIopP.class);
    }

    public PagingListModel searchDataPermited(VFIofIopP searchForm, int start, int take) {
        List listParam = new ArrayList();
//        String sqlSelect = "SELECT n from VFIofIopP n where (n.status = ? or  n.status = ?) ";
//        String sqlCount = "select count(n) from VFIofIopP n where (n.status = ? or  n.status = ?) ";
        String sqlSelect = "SELECT n from VFIofIopP n where 1 = 1 ";
        String sqlCount = "select count(n) from VFIofIopP n where 1 =1  ";
        StringBuilder strBuf = new StringBuilder(sqlSelect);
        StringBuilder strCountBuf = new StringBuilder(sqlCount);
//        listParam.add(Constants.ExportData.status_Banhanhhosogiam_moi);
//        listParam.add(Constants.ExportData.status_Banhanhhosothuong_moi);
        StringBuilder hql = new StringBuilder();
        //ma so thue
        if (StringUtils.validString(searchForm.getTaxCode())) {
            hql.append(" AND lower(n.taxCode) like ? escape '/'");
            listParam.add(StringUtils.toLikeString(searchForm.getTaxCode()));
        }
        //ma ho so
        if (StringUtils.validString(searchForm.getFileCode())) {
            hql.append(" AND lower(n.fileCode) like ? escape '/'");
            listParam.add(StringUtils.toLikeString(searchForm.getFileCode()));
        }
        //ten doanh nghiep
        if (StringUtils.validString(searchForm.getBusinessName())) {
            hql.append(" AND lower(n.businessName) like ? escape '/'");
            listParam.add(StringUtils.toLikeString(searchForm.getBusinessName()));
        }
        //so cong bo san pham
        if (StringUtils.validString(searchForm.getConfirmAnnounceNo())) {
            hql.append(" AND lower(n.confirmAnnounceNo) like ? escape '/'");
            listParam.add(StringUtils.toLikeString(searchForm.getConfirmAnnounceNo()));
        }
        //so xac nhan nhap khau
        if (StringUtils.validString(searchForm.getReceiveNo())) {
            hql.append(" AND lower(n.receiveNo) like ? escape '/'");
            listParam.add(StringUtils.toLikeString(searchForm.getReceiveNo()));
        }
        strBuf.append(hql);
        strCountBuf.append(hql);
        strBuf.append(" order by n.fileId desc, n.receiveNo desc");
        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    public VFIofIopP findByNswCode(String code, Long type, Long temp, Long active) {
        Query query;
        if (temp == 1L) {
            query = getSession()
                    .createQuery(
                            "Select f from VFIofIopP f where f.nswFileCode = :code "
                            + " and f.fileType=:type "
                            + " and  f.isActive = :active and  f.isTemp = :temp  order by f.startDate desc");
            query.setParameter("code", code);
            query.setParameter("type", type);
            query.setParameter("temp", temp);
            query.setParameter("active", active);

        } else {
            query = getSession()
                    .createQuery(
                            "Select f from VFIofIopP f where f.nswFileCode = :code "
                            + " and f.fileType=:type "
                            + " and  f.isActive = :active  order by f.startDate desc");
            query.setParameter("code", code);
            query.setParameter("type", type);
            query.setParameter("active", active);

        }
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFIofIopP) result.get(0);
        }
    }

    public VFIofIopP findByNswCodeAndDoctype(String code, Long type) {
        Query query = getSession()
                .createQuery(
                        "Select f from VFIofIopP f where f.nswFileCode = :code "
                        + " and f.fileType=:type"
                        + " and f.isActive = 1 and ( f.isTemp = 0 or f.isTemp is null )");
        query.setParameter("code", code);
        query.setParameter("type", type);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFIofIopP) result.get(0);
        }
    }
//tim kiem danh sach san pham chua co qrcode

    public List<VFIofIopP> findProductNotHaveQrCode() {
        Query query = getSession().createQuery("SELECT n from VFIofIopP n where (n.haveQrCode <> 1 or n.haveQrCode is null) and n.pass = 1").setMaxResults(100);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return result;
        }
    }
//tim kiem danh sach san pham thewo ho so giay xac nhan

    public List<VFIofIopP> findProduct4FileId(Long fileId) {
        Query query = getSession()
                .createQuery("SELECT n from VFIofIopP n where n.isActive = 1 and n.fileId = :fileId");
        query.setParameter("fileId", fileId);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return result;
        }
    }

    public int checkHaveQrCode(Long fileId) {

        String HQL = " SELECT count(r) FROM VFIofIopP r WHERE r.isActive=:isActive AND r.fileId=:fileId AND r.haveQrCode = :haveQrCode";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        query.setParameter("haveQrCode", Constants_Cos.Status.ACTIVE);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    @Override
    public void saveOrUpdate(VFIofIopP files) {
        if (files != null) {
            //getSession().merge(files);
            super.saveOrUpdate(files);
        }

        getSession().flush();

    }

    public void SaveAndCommit(VFIofIopP files) {
        if (files != null) {
            super.saveOrUpdate(files);
        }
        getSession().flush();
        //getSession().getTransaction().commit();

    }

    public void commit() {
        getSession().getTransaction().commit();

    }

    public void rollBack() {
        getSession().getTransaction().rollback();
    }

    @Override
    public VFIofIopP findById(Long id) {
        Query query = getSession().getNamedQuery("VFIofIopP.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFIofIopP) result.get(0);
        }
    }
}
