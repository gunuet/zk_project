/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.VFee;
import com.viettel.voffice.BO.Files;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Tichnv
 */
public class VFeeDAO extends
        GenericDAOHibernate<VFee, Long> {

    public VFeeDAO() {
        super(VFee.class);
    }

     public VFee findById(Long feeId) {
        Query query = getSession().getNamedQuery(
                "VFee.findByFeeId");
        query.setParameter("feeId", feeId);
        List<VFee> result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    } 
}
