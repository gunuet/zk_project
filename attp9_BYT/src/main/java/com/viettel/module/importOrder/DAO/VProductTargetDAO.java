/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Files;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Tichnv
 */
public class VProductTargetDAO extends
        GenericDAOHibernate<VProductTarget, Long> {

    public VProductTargetDAO() {
        super(VProductTarget.class);
    }

    public List<VProductTarget> findByProdutId(Long productId) {
        Query query = getSession().getNamedQuery(
                "VProductTarget.findByProductId");
        query.setParameter("productId", productId);
        List<VProductTarget> result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return result;
        }
    }
    
     public VProductTarget findById(Long productId) {
        Query query = getSession().getNamedQuery(
                "VProductTarget.findById");
        query.setParameter("id", productId);
        List<VProductTarget> result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VProductTarget)result.get(0);
        }
    } 
    public void delete(Long productId) {
        VProductTarget vptarget = findById(productId);
         delete(vptarget);
    }
}
