/*
' * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Model;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.utils.LogUtils;

/**
 *
 * @author Vu Manh Ha
 */
public class DepartmentLeadershipImportOrderModel {

	private ImportOrderFile importOrderFile;
	private List<ImportOrderProductModel> productList;
	private String createName;
	private String signedDate;
	private String sendNo;
	private String businessName;
	private String businessAddress;
	private String businessPhone;
	private String businessFax;
	private String businessEmail;
	private String pathTemplate;
	private Long cosmeticPermitId;
	private Long fileType;
	private String deptParent;
	private String receiptDeptName;
	private String contentDispatch;
	private String leaderSigned;

	public DepartmentLeadershipImportOrderModel() {

	}

	public ImportOrderFile getImportOrderFile() {
		return importOrderFile;
	}

	public void setImportOrderFile(ImportOrderFile importOrderFile) {
		this.importOrderFile = importOrderFile;
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public Long getCosmeticPermitId() {
		return cosmeticPermitId;
	}

	public String getPathTemplate() {
		return pathTemplate;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public void setCosmeticPermitId(Long cosmeticPermitId) {
		this.cosmeticPermitId = cosmeticPermitId;
	}

	public void setPathTemplate(String pathTemplate) {
		this.pathTemplate = pathTemplate;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public void setProductList(List<ImportOrderProductModel> productList) {
		this.productList = productList;
	}

	public List<ImportOrderProductModel> getProductList() {
		return productList;
	}

	public String getSignedDate() {
		return signedDate;
	}

	public void setSignedDate(String signedDate) {
		this.signedDate = signedDate;
	}

	public String getSendNo() {
		return sendNo;
	}

	public void setSendNo(String sendNo) {
		this.sendNo = sendNo;
	}

	public String getDeptParent() {
		return deptParent;
	}

	public void setDeptParent(String deptParent) {
		this.deptParent = deptParent;
	}

	public String getReceiptDeptName() {
		return receiptDeptName;
	}

	public void setReceiptDeptName(String receiptDeptName) {
		this.receiptDeptName = receiptDeptName;
	}

	public String getContentDispatch() {
		return contentDispatch;
	}

	public void setContentDispatch(String contentDispatch) {
		this.contentDispatch = contentDispatch;
	}

	public String getLeaderSigned() {
		return leaderSigned;
	}

	public void setLeaderSigned(String leaderSigned) {
		this.leaderSigned = leaderSigned;
	}

	public Long getFileType() {
		return fileType;
	}

	public void setFileType(Long fileType) {
		this.fileType = fileType;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getBusinessFax() {
		return businessFax;
	}

	public void setBusinessFax(String businessFax) {
		this.businessFax = businessFax;
	}

	public String getBusinessEmail() {
		return businessEmail;
	}

	public void setBusinessEmail(String businessEmail) {
		this.businessEmail = businessEmail;
	}

	public String toXML() throws JAXBException {
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(IO_20_1_LDC_KSPDHSModel.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			StringWriter builder = new StringWriter();
			jaxbMarshaller.marshal(this, builder);
			return builder.toString();
		} catch (Exception en) {
			LogUtils.addLogDB(en);
			return null;
		}

	}
}
