/*
' * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Model;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;

/**
 *
 * @author Vu Manh Ha
 */
public class ExportImportOrderModel {

	private ImportOrderFile importOrderFile;
	private List productList;
	private String createName;
	private Date signedDate;
	private String sendNo;
	private String businessName;
	private String businessAddress;
	private String pathTemplate;
	private Long cosmeticPermitId;
	private Long cosmeticPermitType;
	private Long cosmeticRejectType;
	private Long cosmeticAdditionalType;

	public ExportImportOrderModel(Long fileId) {
		ImportOrderFileDAO deviceDAO = new ImportOrderFileDAO();
		importOrderFile = deviceDAO.findByFileId(fileId);

		Files file = deviceDAO.getFileByFileId(fileId);
		businessName = file.getBusinessName();
		businessAddress = file.getBusinessAddress();
		createName = file.getCreatorName();
		cosmeticRejectType = Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH;
		// Giay phep cong bo
		cosmeticPermitType = Constants.OBJECT_TYPE.COSMETIC_PERMIT;
		// Giay phep sua doi bo sung
		cosmeticAdditionalType = Constants.OBJECT_TYPE.COSMETIC_SDBS_DISPATH;

		ImportOrderProductDAO proDAO = new ImportOrderProductDAO();
		productList = proDAO.findAllIdByFileIdAndCheckMethodCode(fileId, "GIAM");

		// hem duong dan file template
//		pathTemplate = getPathTemplate(fileId);

	}

	public ImportOrderFile getImportOrderFile() {
		return importOrderFile;
	}

	public void setImportOrderFile(ImportOrderFile importOrderFile) {
		this.importOrderFile = importOrderFile;
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public Long getCosmeticPermitId() {
		return cosmeticPermitId;
	}

	public Long getCosmeticPermitType() {
		return cosmeticPermitType;
	}

	public Long getCosmeticRejectType() {
		return cosmeticRejectType;
	}

	public String getPathTemplate() {
		return pathTemplate;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public void setCosmeticPermitId(Long cosmeticPermitId) {
		this.cosmeticPermitId = cosmeticPermitId;
	}

	public void setCosmeticPermitType(Long cosmeticPermitType) {
		this.cosmeticPermitType = cosmeticPermitType;
	}

	public void setCosmeticRejectType(Long cosmeticRejectType) {
		this.cosmeticRejectType = cosmeticRejectType;
	}

	public void setPathTemplate(String pathTemplate) {
		this.pathTemplate = pathTemplate;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public void setProductList(List productList) {
		this.productList = productList;
	}

	public List getProductList() {
		return productList;
	}

	public Date getSignedDate() {
		return signedDate;
	}

	public void setSignedDate(Date signedDate) {
		this.signedDate = signedDate;
	}

	public String getSendNo() {
		return sendNo;
	}

	public void setSendNo(String sendNo) {
		this.sendNo = sendNo;
	}

	public Long getCosmeticAdditionalType() {
		return cosmeticAdditionalType;
	}

	public void setCosmeticAdditionalType(Long cosmeticAdditionalType) {
		this.cosmeticAdditionalType = cosmeticAdditionalType;
	}

//	private String getPathTemplate(Long fileId) {
//		// Get Template
//		WorkflowAPI wAPI = new WorkflowAPI();
//		Flow flow = wAPI.getFlowByFileId(fileId);
//		Long deptId = flow.getDeptId();
//		Long procedureId = flow.getObjectId();
//
//		TemplateDAOHE the = new TemplateDAOHE();
//		String pathTemplate = the.findPathTemplate(deptId, procedureId,
//				Constants.PROCEDURE_TEMPLATE_TYPE.PERMIT);
//		return pathTemplate;
//	}

	public String toXML() throws JAXBException {
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(ExportImportOrderModel.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			StringWriter builder = new StringWriter();
			jaxbMarshaller.marshal(this, builder);
			return builder.toString();
		} catch (Exception en) {
			LogUtils.addLogDB(en);
			return null;
		}

	}
}
