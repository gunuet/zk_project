/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Model;

import com.viettel.module.importDevice.Model.*;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.zkoss.xel.fn.CommonFns;

/**
 *
 * @author Vu Manh Ha
 */
public class ExportOrderFileModel {
    //file info

    ImportOrderFile importOrderFile;
    List lstProduct;
    private Long documentTypeCode;
    private static final long serialVersionUID = 1L;
//    private Long importOrderFileId;
//    private Long fileId;
//    private String nswFileCode;
//    private String transNo;
//    private String contractNo;
//    private String billNo;
//    private String goodsOwnerName;
//    private String goodsOwnerAddress;
//    private String goodsOwnerPhone;
//    private String goodsOwnerFax;
//    private String goodsOwnerEmail;
//    private String responsiblePersonName;
//    private String responsiblePersonAddress;
//    private String responsiblePersonPhone;
//    private String responsiblePersonFax;
//    private String responsiblePersonEmail;
//    private String exporterName;
//    private String exporterAddress;
//    private String exporterPhone;
//    private String exporterFax;
//    private String exporterEmail;
//    private Date comingDate;
//    private String exporterGateCode;
//    private String exporterGateName;
//    private String importerGateCode;
//    private String importerGateName;
//    private String customsBranchCode;
//    private String customsBranchName;
//    private Date checkTime;
//    private String checkPlace;
//    private String deptCode;
//    private String deptName;
//    private String signPlace;
//    private Date signDate;
//    private String signName;
//    private String signedData;
    private String pathTemplate;
    private Long cosmeticPermitId;
    private Long cosmeticPermitType;
    private Long cosmeticRejectId;
    private Long cosmeticRejectType;
    private Long cosmeticAdditionalId;
    private Long cosmeticAdditionalType;
    private Files files;
    Long lsumMoney = 0L;

    public ExportOrderFileModel(Long id) {
        ImportOrderFileDAO deviceDAO = new ImportOrderFileDAO();
        importOrderFile = deviceDAO.findByFileId(id);
        files = (new FilesDAOHE()).findById(id);
        DecimalFormat format = new DecimalFormat("###,###,###,###,###.###");
        ImportOrderProductDAO proDAO = new ImportOrderProductDAO();
        List listTemp;
        listTemp = proDAO.findAllIdByFileId(importOrderFile.getFileId());
        lstProduct = new ArrayList<ImportOrderProductModel>();
        for (int i = 0; i < listTemp.size(); i++) {
            ImportOrderProduct ip_temp = (ImportOrderProduct) listTemp.get(i);
            ImportOrderProductModel obj_mode = new ImportOrderProductModel();
            obj_mode.setV_baseUnit(CommonFns.formatNumber(ip_temp.getBaseUnit(), "###,###,###,###,###.###"));
            obj_mode.setV_netweight(format.format(ip_temp.getNetweight()) + (ip_temp.getNetweightUnitName() == null ? "" : " " + ip_temp.getNetweightUnitName()));
            obj_mode.setV_total(format.format(ip_temp.getTotal()) + (ip_temp.getTotalUnitName() == null ? "" : " " + ip_temp.getTotalUnitName()));
            obj_mode.setBaseUnit(ip_temp.getBaseUnit());
            obj_mode.setCheckMethodCode(ip_temp.getCheckMethodCode());
            obj_mode.setCheckMethodName(ip_temp.getCheckMethodName());
            obj_mode.setComments(ip_temp.getComments());
            obj_mode.setConfirmAnnounceNo(ip_temp.getConfirmAnnounceNo());
            obj_mode.setDateOfManufacturer(ip_temp.getDateOfManufacturer());
            obj_mode.setExpiredDate(ip_temp.getExpiredDate());
            obj_mode.setFileId(ip_temp.getFileId());
            obj_mode.setHsCode(ip_temp.getHsCode());
            obj_mode.setManufacTurer(ip_temp.getManufacTurer());
            obj_mode.setManufacturerAddress(ip_temp.getManufacturerAddress());
            obj_mode.setNationalCode(ip_temp.getNationalCode());
            obj_mode.setNationalName(ip_temp.getNationalName());
            obj_mode.setNetweight(ip_temp.getNetweight());
            obj_mode.setProductName(ip_temp.getProductName());
            obj_mode.setProductDescription(ip_temp.getProductDescription());

            lstProduct.add(obj_mode);
        }
        //linhdx them duong dan file template
        pathTemplate = null;
        //pathTemplate = getPathTemplate(fileId);
    }

    public ImportOrderFile getIdfFile() {
        return importOrderFile;
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

    public void setLstProduct(List lstProduct) {
        this.lstProduct = lstProduct;
    }

    public List getLstProduct() {
        return lstProduct;
    }

    public ImportOrderFile getImportOrderFile() {
        return importOrderFile;
    }

    public void setImportOrderFile(ImportOrderFile importOrderFile) {
        this.importOrderFile = importOrderFile;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

//    public Long getImportOrderFileId() {
//        return importOrderFileId;
//    }
//
//    public void setImportOrderFileId(Long importOrderFileId) {
//        this.importOrderFileId = importOrderFileId;
//    }
//
//    public Long getFileId() {
//        return fileId;
//    }
//
//    public void setFileId(Long fileId) {
//        this.fileId = fileId;
//    }
//
//    public String getNswFileCode() {
//        return nswFileCode;
//    }
//
//    public void setNswFileCode(String nswFileCode) {
//        this.nswFileCode = nswFileCode;
//    }
//
//    public String getTransNo() {
//        return transNo;
//    }
//
//    public void setTransNo(String transNo) {
//        this.transNo = transNo;
//    }
//
//    public String getContractNo() {
//        return contractNo;
//    }
//
//    public void setContractNo(String contractNo) {
//        this.contractNo = contractNo;
//    }
//
//    public String getBillNo() {
//        return billNo;
//    }
//
//    public void setBillNo(String billNo) {
//        this.billNo = billNo;
//    }
//
//    public String getGoodsOwnerName() {
//        return goodsOwnerName;
//    }
//
//    public void setGoodsOwnerName(String goodsOwnerName) {
//        this.goodsOwnerName = goodsOwnerName;
//    }
//
//    public String getGoodsOwnerAddress() {
//        return goodsOwnerAddress;
//    }
//
//    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
//        this.goodsOwnerAddress = goodsOwnerAddress;
//    }
//
//    public String getGoodsOwnerPhone() {
//        return goodsOwnerPhone;
//    }
//
//    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
//        this.goodsOwnerPhone = goodsOwnerPhone;
//    }
//
//    public String getGoodsOwnerFax() {
//        return goodsOwnerFax;
//    }
//
//    public void setGoodsOwnerFax(String goodsOwnerFax) {
//        this.goodsOwnerFax = goodsOwnerFax;
//    }
//
//    public String getGoodsOwnerEmail() {
//        return goodsOwnerEmail;
//    }
//
//    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
//        this.goodsOwnerEmail = goodsOwnerEmail;
//    }
//
//    public String getResponsiblePersonName() {
//        return responsiblePersonName;
//    }
//
//    public void setResponsiblePersonName(String responsiblePersonName) {
//        this.responsiblePersonName = responsiblePersonName;
//    }
//
//    public String getResponsiblePersonAddress() {
//        return responsiblePersonAddress;
//    }
//
//    public void setResponsiblePersonAddress(String responsiblePersonAddress) {
//        this.responsiblePersonAddress = responsiblePersonAddress;
//    }
//
//    public String getResponsiblePersonPhone() {
//        return responsiblePersonPhone;
//    }
//
//    public void setResponsiblePersonPhone(String responsiblePersonPhone) {
//        this.responsiblePersonPhone = responsiblePersonPhone;
//    }
//
//    public String getResponsiblePersonFax() {
//        return responsiblePersonFax;
//    }
//
//    public void setResponsiblePersonFax(String responsiblePersonFax) {
//        this.responsiblePersonFax = responsiblePersonFax;
//    }
//
//    public String getResponsiblePersonEmail() {
//        return responsiblePersonEmail;
//    }
//
//    public void setResponsiblePersonEmail(String responsiblePersonEmail) {
//        this.responsiblePersonEmail = responsiblePersonEmail;
//    }
//
//    public String getExporterName() {
//        return exporterName;
//    }
//
//    public void setExporterName(String exporterName) {
//        this.exporterName = exporterName;
//    }
//
//    public String getExporterAddress() {
//        return exporterAddress;
//    }
//
//    public void setExporterAddress(String exporterAddress) {
//        this.exporterAddress = exporterAddress;
//    }
//
//    public String getExporterPhone() {
//        return exporterPhone;
//    }
//
//    public void setExporterPhone(String exporterPhone) {
//        this.exporterPhone = exporterPhone;
//    }
//
//    public String getExporterFax() {
//        return exporterFax;
//    }
//
//    public void setExporterFax(String exporterFax) {
//        this.exporterFax = exporterFax;
//    }
//
//    public String getExporterEmail() {
//        return exporterEmail;
//    }
//
//    public void setExporterEmail(String exporterEmail) {
//        this.exporterEmail = exporterEmail;
//    }
//
//    public Date getComingDate() {
//        return comingDate;
//    }
//
//    public void setComingDate(Date comingDate) {
//        this.comingDate = comingDate;
//    }
//
//    public String getExporterGateCode() {
//        return exporterGateCode;
//    }
//
//    public void setExporterGateCode(String exporterGateCode) {
//        this.exporterGateCode = exporterGateCode;
//    }
//
//    public String getExporterGateName() {
//        return exporterGateName;
//    }
//
//    public void setExporterGateName(String exporterGateName) {
//        this.exporterGateName = exporterGateName;
//    }
//
//    public String getImporterGateCode() {
//        return importerGateCode;
//    }
//
//    public void setImporterGateCode(String importerGateCode) {
//        this.importerGateCode = importerGateCode;
//    }
//
//    public String getImporterGateName() {
//        return importerGateName;
//    }
//
//    public void setImporterGateName(String importerGateName) {
//        this.importerGateName = importerGateName;
//    }
//
//    public String getCustomsBranchCode() {
//        return customsBranchCode;
//    }
//
//    public void setCustomsBranchCode(String customsBranchCode) {
//        this.customsBranchCode = customsBranchCode;
//    }
//
//    public String getCustomsBranchName() {
//        return customsBranchName;
//    }
//
//    public void setCustomsBranchName(String customsBranchName) {
//        this.customsBranchName = customsBranchName;
//    }
//
//    public Date getCheckTime() {
//        return checkTime;
//    }
//
//    public void setCheckTime(Date checkTime) {
//        this.checkTime = checkTime;
//    }
//
//    public String getCheckPlace() {
//        return checkPlace;
//    }
//
//    public void setCheckPlace(String checkPlace) {
//        this.checkPlace = checkPlace;
//    }
//
//    public String getDeptCode() {
//        return deptCode;
//    }
//
//    public void setDeptCode(String deptCode) {
//        this.deptCode = deptCode;
//    }
//
//    public String getDeptName() {
//        return deptName;
//    }
//
//    public void setDeptName(String deptName) {
//        this.deptName = deptName;
//    }
//
//    public String getSignPlace() {
//        return signPlace;
//    }
//
//    public void setSignPlace(String signPlace) {
//        this.signPlace = signPlace;
//    }
//
//    public Date getSignDate() {
//        return signDate;
//    }
//
//    public void setSignDate(Date signDate) {
//        this.signDate = signDate;
//    }
//
//    public String getSignName() {
//        return signName;
//    }
//
//    public void setSignName(String signName) {
//        this.signName = signName;
//    }
//
//    public String getSignedData() {
//        return signedData;
//    }
//
//    public void setSignedData(String signedData) {
//        this.signedData = signedData;
//    }
    public Long getCosmeticPermitId() {
        return cosmeticPermitId;
    }

    public void setCosmeticPermitId(Long cosmeticPermitId) {
        this.cosmeticPermitId = cosmeticPermitId;
    }

    public Long getCosmeticPermitType() {
        return cosmeticPermitType;
    }

    public void setCosmeticPermitType(Long cosmeticPermitType) {
        this.cosmeticPermitType = cosmeticPermitType;
    }

    public Long getCosmeticRejectId() {
        return cosmeticRejectId;
    }

    public void setCosmeticRejectId(Long cosmeticRejectId) {
        this.cosmeticRejectId = cosmeticRejectId;
    }

    public Long getCosmeticRejectType() {
        return cosmeticRejectType;
    }

    public void setCosmeticRejectType(Long cosmeticRejectType) {
        this.cosmeticRejectType = cosmeticRejectType;
    }

    public Long getCosmeticAdditionalId() {
        return cosmeticAdditionalId;
    }

    public void setCosmeticAdditionalId(Long cosmeticAdditionalId) {
        this.cosmeticAdditionalId = cosmeticAdditionalId;
    }

    public Long getCosmeticAdditionalType() {
        return cosmeticAdditionalType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPathTemplate() {
        return pathTemplate;
    }

    public void setCosmeticAdditionalType(Long cosmeticAdditionalType) {
        this.cosmeticAdditionalType = cosmeticAdditionalType;
    }

    private String getPathTemplate(Long fileId) {
        //Get Template
        WorkflowAPI wAPI = new WorkflowAPI();
        Flow flow = wAPI.getFlowByFileId(fileId);
        Long deptId = flow.getDeptId();
        Long procedureId = flow.getObjectId();

        TemplateDAOHE the = new TemplateDAOHE();
        String pathTemplate = the.findPathTemplate(deptId, procedureId,
                Constants.PROCEDURE_TEMPLATE_TYPE.PERMIT);
        return pathTemplate;
    }

    public String toXML() throws JAXBException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ExportDeviceModel.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            StringWriter builder = new StringWriter();
            jaxbMarshaller.marshal(this, builder);
            return builder.toString();
        } catch (Exception en) {
            LogUtils.addLogDB(en);
            return null;
        }

    }
}
