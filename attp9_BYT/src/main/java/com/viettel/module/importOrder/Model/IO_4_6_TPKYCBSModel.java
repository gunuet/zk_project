/*
' * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Model;



public class IO_4_6_TPKYCBSModel {


	private String signedDate;
	private String sendNo;
	private String businessName;
	private String businessAddress;
	private String contentDispatch;
	private String leaderSigned;
	
	
	public String getSignedDate() {
		return signedDate;
	}
	public void setSignedDate(String signedDate) {
		this.signedDate = signedDate;
	}
	public String getSendNo() {
		return sendNo;
	}
	public void setSendNo(String sendNo) {
		this.sendNo = sendNo;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getContentDispatch() {
		return contentDispatch;
	}
	public void setContentDispatch(String contentDispatch) {
		this.contentDispatch = contentDispatch;
	}
	public String getLeaderSigned() {
		return leaderSigned;
	}
	public void setLeaderSigned(String leaderSigned) {
		this.leaderSigned = leaderSigned;
	}
	public String getBusinessAddress() {
		return businessAddress;
	}
	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}
	

}
