/*
' * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Model;

import java.util.List;

import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.voffice.BO.Files;

/**
 *
 * @author Vu Manh Ha
 */
public class IO_5_1_EvaluationAttpLDCucModel {

	private ImportOrderFile importOrderFile;
	private List<ImportOrderProduct> productList;
	private List<ImportOrderProductModel> productListModle;
	private String createName;
	private String signedDate;
	private String sendNo;
	private String businessName;
	private String businessAddress;
	private String businessPhone;
	private String businessFax;
	private String businessEmail;
	private String pathTemplate;
	private Long cosmeticPermitId;
	private Long fileType;
	private String deptParent;
	private String receiptDeptName;
	private String contentDispatch;
	private String leaderSigned;
	private Files files;

	public IO_5_1_EvaluationAttpLDCucModel() {

	}

	public ImportOrderFile getImportOrderFile() {
		return importOrderFile;
	}

	public void setImportOrderFile(ImportOrderFile importOrderFile) {
		this.importOrderFile = importOrderFile;
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public Long getCosmeticPermitId() {
		return cosmeticPermitId;
	}

	public String getPathTemplate() {
		return pathTemplate;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName==null?"":businessName;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress==null?"":businessAddress;
	}

	public void setCosmeticPermitId(Long cosmeticPermitId) {
		this.cosmeticPermitId = cosmeticPermitId;
	}

	public void setPathTemplate(String pathTemplate) {
		this.pathTemplate = pathTemplate;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName==null?"":createName;
	}

	public void setProductList(List<ImportOrderProduct> productList) {
		this.productList = productList;
	}

	public List<ImportOrderProduct> getProductList() {
		return productList;
	}

	public String getSignedDate() {
		return signedDate;
	}

	public void setSignedDate(String signedDate) {
		this.signedDate = signedDate==null?"":signedDate;
	}

	public String getSendNo() {
		return sendNo;
	}

	public void setSendNo(String sendNo) {
		this.sendNo = sendNo==null?"":sendNo;
	}

	public String getDeptParent() {
		return deptParent;
	}

	public void setDeptParent(String deptParent) {
		this.deptParent = deptParent==null?"":deptParent;
	}

	public String getReceiptDeptName() {
		return receiptDeptName;
	}

	public void setReceiptDeptName(String receiptDeptName) {
		this.receiptDeptName = receiptDeptName==null?"":receiptDeptName;
	}

	public String getContentDispatch() {
		return contentDispatch;
	}

	public void setContentDispatch(String contentDispatch) {
		this.contentDispatch = contentDispatch==null?"":contentDispatch;
	}

	public String getLeaderSigned() {
		return leaderSigned;
	}

	public void setLeaderSigned(String leaderSigned) {
		this.leaderSigned = leaderSigned==null?"":leaderSigned;
	}

	public Long getFileType() {
		return fileType;
	}

	public void setFileType(Long fileType) {
		this.fileType = fileType;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone==null?"":businessPhone;
	}

	public String getBusinessFax() {
		return businessFax;
	}

	public void setBusinessFax(String businessFax) {
		this.businessFax = businessFax==null?"":businessFax;
	}

	public String getBusinessEmail() {
		return businessEmail;
	}

	public void setBusinessEmail(String businessEmail) {
		this.businessEmail = businessEmail==null?"":businessEmail;
	}

	public Files getFiles() {
		return files;
	}

	public void setFiles(Files files) {
		this.files = files;
	}

	public List<ImportOrderProductModel> getProductListModle() {
		return productListModle;
	}

	public void setProductListModle(List<ImportOrderProductModel> productListModle) {
		this.productListModle = productListModle;
	}
	
	
}
