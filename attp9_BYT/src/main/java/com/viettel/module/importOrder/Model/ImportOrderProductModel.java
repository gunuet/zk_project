/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

public class ImportOrderProductModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long productId;
    private String productName;
    private String productDescription;
    private String productCode;
    private String nationalCode;
    private String nationalName;
    private String confirmAnnounceNo;
    private Double total;
    private String totalUnitCode;
    private String totalUnitName;
    private Double netweight;
    private String netweightUnitCode;
    private String netweightUnitName;
    private Long baseUnit;
    private Date dateOfManufacturer;
    private Date expiredDate;
    private String productNumber;
    private Long fileId;
    private String hsCode;
    private String manufacTurer;
    private String manufacturerAddress;
    private String checkMethodCode;
    private String checkMethodName;
    private String pass;
    private String notPass;
    private String reason;
    private Long processType;
    private String processTypeName;
    private Long reProcess;
    private String comments;
    private String results;
    private Long reTest;
    private String v_total;
    private String v_netweight;
    private String v_baseUnit;

    public ImportOrderProductModel() {
    }

    public ImportOrderProductModel(Long productId) {
        this.productId = productId;
    }

    public Long getReTest() {
        return reTest;
    }

    public void setReTest(Long reTest) {
        this.reTest = reTest;
    }

    public String getResult() {
        return results;
    }

    public void setResult(String results) {
        this.results = results == null ? "" : results;
    }

    public Long getProcessType() {
        return processType;
    }

    public void setProcessType(Long processType) {
        this.processType = processType;
    }

    public String getProcessTypeName() {
        return processTypeName;
    }

    public void setProcessTypeName(String processTypeName) {
        this.processTypeName = processTypeName == null ? "" : processTypeName;
    }

    public Long getReProcess() {
        return reProcess;
    }

    public void setReProcess(Long reProcess) {
        this.reProcess = reProcess;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments== null ? "" : comments;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass== null ? "" : pass;
    }

    public String getNotPass() {
        return notPass;
    }

    public void setNotPass(String notPass) {
        this.notPass = notPass== null ? "" : notPass;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results== null ? "" : results;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason== null ? "" : reason;
    }

    public String getCheckMethodCode() {
        return checkMethodCode;
    }

    public void setCheckMethodCode(String checkMethodCode) {
        this.checkMethodCode = checkMethodCode== null ? "" : checkMethodCode;;
    }

    public String getCheckMethodName() {
        return checkMethodName;
    }

    public void setCheckMethodName(String checkMethodName) {
        this.checkMethodName = checkMethodName== null ? "" : checkMethodName;
    }

    public String getManufacTurer() {
        return manufacTurer;
    }

    public void setManufacTurer(String manufacTurer) {
        this.manufacTurer = manufacTurer== null ? "" : manufacTurer;
    }

    public String getManufacturerAddress() {
        return manufacturerAddress;
    }

    public void setManufacturerAddress(String manufacturerAddress) {
        this.manufacturerAddress = manufacturerAddress== null ? "" : manufacturerAddress;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode== null ? "" : hsCode;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName== null ? "" : productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription== null ? "" : productDescription;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode== null ? "" : productCode;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode== null ? "" : nationalCode;
    }

    public String getNationalName() {
        return nationalName;
    }

    public void setNationalName(String nationalName) {
        this.nationalName = nationalName== null ? "" : nationalName;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo== null ? "" : confirmAnnounceNo;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getTotalUnitCode() {
        return totalUnitCode;
    }

    public void setTotalUnitCode(String totalUnitCode) {
        this.totalUnitCode = totalUnitCode== null ? "" : totalUnitCode;
    }

    public String getTotalUnitName() {
        return totalUnitName;
    }

    public void setTotalUnitName(String totalUnitName) {
        this.totalUnitName = totalUnitName== null ? "" : totalUnitName;
    }

    public Double getNetweight() {
        return netweight;
    }

    public void setNetweight(Double netweight) {
        this.netweight = netweight;
    }

    public String getNetweightUnitCode() {
        return netweightUnitCode;
    }

    public void setNetweightUnitCode(String netweightUnitCode) {
        this.netweightUnitCode = netweightUnitCode== null ? "" : netweightUnitCode;
    }

    public String getNetweightUnitName() {
        return netweightUnitName;
    }

    public void setNetweightUnitName(String netweightUnitName) {
        this.netweightUnitName = netweightUnitName== null ? "" : netweightUnitName;
    }

    public Long getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(Long baseUnit) {
        this.baseUnit = baseUnit;
    }

    public Date getDateOfManufacturer() {
        return dateOfManufacturer;
    }

    public void setDateOfManufacturer(Date dateOfManufacturer) {
        this.dateOfManufacturer = dateOfManufacturer;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber== null ? "" : productNumber;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getV_total() {
        return v_total;
    }

    public void setV_total(String v_total) {
        this.v_total = v_total;
    }

    public String getV_netweight() {
        return v_netweight;
    }

    public void setV_netweight(String v_netweight) {
        this.v_netweight = v_netweight;
    }

    public String getV_baseUnit() {
        return v_baseUnit;
    }

    public void setV_baseUnit(String v_baseUnit) {
        this.v_baseUnit = v_baseUnit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImportOrderProductModel)) {
            return false;
        }
        ImportOrderProductModel other = (ImportOrderProductModel) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importOrder.BO.ImportOrderProduct[ productId=" + productId + " ]";
    }

}
