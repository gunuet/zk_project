package com.viettel.module.importOrder.Model;

public class TempContent {
    private String resonRequest;
    private String resultEvaluation;
    
    public String getResonRequest() {
        return resonRequest;
    }

    public void setResonRequest(String resonRequest) {
        this.resonRequest = resonRequest;
    }
    
     public String getResultEvaluation() {
        return resultEvaluation;
    }

    public void setResultEvaluation(String resultEvaluation) {
        this.resultEvaluation = resultEvaluation;
    }
    
}