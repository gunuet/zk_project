/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrder.Model;

import com.viettel.module.importOrder.BO.VFileImportOrder;

public class VFileImportOrderModel {

    private int rowNum;
    private VFileImportOrder vFileImportOrder;
    private String sendDate;
    private String statusStr;

    public VFileImportOrderModel() {
    }

    public VFileImportOrder getvFileImportOrder() {
        return vFileImportOrder;
    }

    public void setvFileImportOrder(VFileImportOrder vFileImportOrder) {
        this.vFileImportOrder = vFileImportOrder;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

}
