/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrderNew.Controller.AfterPublish;

import com.google.gson.Gson;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class DnSendReport extends
        BusinessController {
        @Wire
    private Label lbTopWarning, lbBottomWarning;
    @Wire
    private Textbox txtValidate, txtFinishDate, txtMessage;

    private Textbox txtNote = (Textbox) Path.getComponent("/windowProcessing/txtNote");
    private Long fileId;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            clearWarningMessage();
            txtValidate.setValue("1");
            ImportOrderFileDAO dao = new ImportOrderFileDAO();
            ImportOrderFile obj = dao.findByFileId(fileId);
            if (obj != null) {
                obj.setComments(txtNote.getValue());
                obj.setCommentsType(Constants.DYCNK_HS_KHONGDAT.COMMENTS_TYPE_DN_BCKQ);
                dao.saveOrUpdate(obj);
            }
            showWarningMessage("Gửi thành công");
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

  

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }
}
