package com.viettel.module.importOrderNew.Controller.Evaluation;

import com.viettel.module.importOrder.Controller.includes.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.newsignature.plugin.SignPdfFile;
import com.viettel.newsignature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;

/**
 *
 * @author Linhdx
 */
public class DepartmentClericalImportOrderController extends
        BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate, txtFinishDate;
    @Wire
    private Listbox finalFileListbox, finalFileListboxKy;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    @Wire
    private Textbox txtMainContent;
    @Wire
    private Label lbNote;
    @Wire
    private Textbox txtMessage;

    private List listBook;
    private Long docType;
    private String bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_PERMIT;

    private List<Attachs> lstAttach, lstAttachKy;
    private UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
            "userToken");
//	private String KetQuaKiemTraKiemNghiem = "/WEB-INF/template/KetQuaKiemTraKiemNghiem.docx";

    /**
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        // load all of signed file
        fillFinalFileListbox(fileId);
        fillFinalFileListboxKy(fileId);
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
            updateAttachFileSigned();
            sendMS();
            txtValidate.setValue("1");
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_16);
        md.setPhase(0l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
        ImportOrderProductDAO dao = new ImportOrderProductDAO();
        List<ImportOrderProduct> lsP = dao.findAllIdByFileIdAndPass(fileId, 0L);
        if (lsP == null || lsP.isEmpty()) {
            txtFinishDate.setValue("1");
        }
    }

    private boolean isValidatedData() {
        AttachDAOHE attDAO = new AttachDAOHE();
        Attachs att = attDAO.getByObjectIdAndAttachType(fileId, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER, true);
        if (att == null) {
            showNotification(String.format(
                    "Hãy thực hiện ký số", Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
            return false;
        }
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            if (!isValidatedData()) {
                return;
            }
//			onApproveFile();
            showNotification(String.format(Constants.Notification.SAVE_SUCCESS,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.INFO);

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    // load all of singed pdf file
    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeIsSent(fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER, 1l);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    private void fillFinalFileListboxKy(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        lstAttachKy = rDaoHe
                .getByObjectIdAndAttachCatAndAttachTypeIsSent(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER, 0l);
        this.finalFileListboxKy.setModel(new ListModelArray(lstAttachKy));
    }

    // download signed pdf
    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs attachs = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(attachs);
    }

    @Listen("onDownloadFinalFile = #finalFileListboxKy")
    public void onDownloadFinalFileKy(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);
    }

    @Listen("onDeleteFinalFile = #finalFileListboxKy")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListboxKy(fileId);
    }

    private void setWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        clearWarningMessage();
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        // get fdf file path that is insert data\
        AttachDAOHE attDAO = new AttachDAOHE();
        Attachs a = attDAO.getByObjectIdAndAttachType(fileId, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER, false);
        actionSignCA(event, a.getFullPathFile());
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        String certSerial = x509Cert.getSerialNumber().toString(16);
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caUserList = ca
                .findCaBySerial(certSerial, 1L, getUserId());
        if (caUserList != null && caUserList.size() > 0) {
            //SignPdfFile pdfSig = new SignPdfFile();
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String linkImageStamp = folderPath + separator
                    + caUserList.get(0).getStamper();
            Pdf pdf = new Pdf();
            String fieldName = tk.getUserFullName();
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String outputFileFinal = resourceBundle.getString("signTemp") + "_"
                    + (new Date()).getTime() + ".pdf";
            SignPdfFile pdfSig = pdf.createHash(fileToSign, outputFileFinal, new Certificate[]{x509Cert}, linkImageStamp, fieldName);
            if (pdfSig == null) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }
            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", pdf.getBase64Hash());
            session.setAttribute("outputFileFinal", outputFileFinal);
            txtBase64HASH.setValue(pdf.getBase64Hash());
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");

        } else {
            showNotification("Ký số không thành công!");
        }
    }

    @Listen("onSign = #businessWindow")
    public void onSign(Event event) throws Exception {
        try {
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String folderSignOut = resourceBundle.getString("signPdf");
            if (!new File(folderSignOut).exists()) {
                FileUtil.mkdirs(folderSignOut);
            }
            String fileSignOut = folderSignOut
                    + "_signed_GiayPheDuyetThucPhamNhapKhauChat"
                    + (new Date()).getTime() + ".pdf";

            Session session = Sessions.getCurrent();
            SignPdfFile signPdfFile = (SignPdfFile) session
                    .getAttribute("PDFSignature");
            String outputFileFinal = session
                    .getAttribute("outputFileFinal").toString();
            String signature = event.getData().toString();
            signPdfFile.insertSignature(signature, outputFileFinal, fileSignOut);

            attachSave(fileSignOut);

            showNotification("Ký số thành công!", Constants.Notification.INFO);
            // refresh page
            fillFinalFileListboxKy(fileId);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công!", Constants.Notification.ERROR);
        }
    }

    public void attachSave(String filePath) throws Exception {

        AttachDAO attachDAO = new AttachDAO();
        attachDAO.saveFileAttachPdfSign(filePath, fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
    }

    public void updateAttachFileSigned() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        for (Attachs att : lstAttachKy) {
            Attachs ats = rDaoHe.findByAttachId(att.getAttachId());
            ats.setIsSent(1L);
            rDaoHe.saveOrUpdate(ats);
        }
    }
}
