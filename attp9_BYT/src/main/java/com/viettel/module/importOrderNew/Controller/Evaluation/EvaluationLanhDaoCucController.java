package com.viettel.module.importOrderNew.Controller.Evaluation;

import com.viettel.module.importOrder.Controller.Evaluation.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.Model.IO_5_1_EvaluationAttpLDCucModel;
import com.viettel.module.importOrder.Model.ImportOrderProductModel;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.WordExportUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import org.docx4j.convert.out.pdf.PdfConversion;
import org.docx4j.convert.out.pdf.viaXSLFO.PdfSettings;

/**
 *
 * @author THANHDV
 */
public class EvaluationLanhDaoCucController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, labelCriteria, labelMechanism,
            labelLegalRequi, labelCriteriaRequi, labelMechanismRequi,
            labelProductNameRequi, labelProductName, labelChiTieu,
            producIdHidden, lbFileId;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    @Wire
    private Window windowEvaluation;
    private Long fileId;
    private VFileImportOrder cosFile;
    @Wire
    Listbox lbOrderProduct;
    Long documentTypeCode;
    private Files files;
    private String nswFileCode;
    private List<Category> listImportOderFileType;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;
    private Users user;
    @Wire
    private Textbox userEvaluationTypeH, txtValidate, txtMessage;
    @Wire
    private Radiogroup rbStatus;
    @Wire
    private Textbox mainContent, legalContent, mechanismContent,
            ProductNameContent, criteriaContent, lbStatus;
    @Wire
    private Listbox lbEffective, lbLegal, lbMechanism, lbCriteria,
            lbProductName, lboxLegal, lboxlLegalCT;
    @Wire
    private Button btnApproveFile, btnApproveDispatch, btnPreviewDispatch;
    @Wire
    private Button btnApproveFile2, btnApproveDispatch2, btnPreviewDispatch2;
    private VFileRtfile vFileRtfile;
    private String sLegal, sMechanism, sCriteria, sNameProduct;
    private CosEvaluationRecord obj = new CosEvaluationRecord();
    @Wire
    private Window businessWindow;
    @Wire
    private Combobox cbtexLegal;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    private Listbox finalFileListbox;
    @Wire
    private Textbox txtCertSERIAL;
    @Wire
    private Textbox txtBase64HASH;
    @Wire
    private Label lbCheckDepartment;

    private Long docType;
    private String bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_REGISTER_XNK;
    private String PheDuyetDonDangKy = "/WEB-INF/template/PheDuyetDonDangKy_CQKT.docx";

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        docType = (Long) arguments.get("docType");
        fileId = (Long) arguments.get("fileId");

        // Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
        files = (new FilesDAOHE()).findById(fileId);

        // load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        nswFileCode = getAutoNswFileCode(documentTypeCode);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @SuppressWarnings("rawtypes")
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("id", fileId);
        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO()
                .findAllIdByFileId(fileId);
        ListModelArray lstModelManufacturer = new ListModelArray(
                importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);
        lbFileId.setValue(String.valueOf(fileId));

        // load co quan kiem tra
        loadCheckDepartment();
        //load all files  signed
        fillFinalFileListbox(fileId);
    }

    @SuppressWarnings("rawtypes")
    @Listen("onShow =  #lbOrderProduct")
    public void onShow(Event event) {
        ImportOrderProduct obj = (ImportOrderProduct) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        Long productId = obj.getProductId();
        arguments.put("id", productId);
        // arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
		// getListBoxModel(obj.getCheckMethodCode());

        // set view
        producIdHidden.setValue(String.valueOf(productId));
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", businessWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("procedureId", FILE_TYPE);
        arguments.put("deptId", businessWindow);
        return arguments;

    }

    @Listen("onClick=#btnSubmit")
    public void btnSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }

            Gson gson = new Gson();
            MessageModel md = new MessageModel();
            md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
            md.setFileId(fileId);
            md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_38);
            md.setPhase(0l);
            md.setFeeUpdate(false);
            String jsonMd = gson.toJson(md);
            txtMessage.setValue(jsonMd);
            //cập nhật trạng thái đã gửi của file đã ký
            updateAttachFileSigned();

            ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
            List<ImportOrderProduct> importOrderProducts = importOrderProductDAO.findAllIdByFileIdAndCheckMethodCode(fileId, "GIAM");
            if (importOrderProducts != null && !importOrderProducts.isEmpty()) {
                for (int i = 0; i < importOrderProducts.size(); i++) {
                    ImportOrderProduct temp = importOrderProducts.get(i);
                    temp.setPass(1L);
                    importOrderProductDAO.saveOrUpdate(temp);
                }
            }

            txtValidate.setValue("1");

            
        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    // load co quan kiem tra
    private void loadCheckDepartment() {
        // load content
        EvaluationRecordDAO evaluationRecordDAO = new EvaluationRecordDAO();
        // get lastest evaluationRecord
        EvaluationRecord evaluationRecord = evaluationRecordDAO
                .findMaxByFileIdAndEvalType(fileId, Constants.EVAL_TYPE.CHECK_DEPT);
        if (evaluationRecord != null) {
            String formContent = evaluationRecord.getFormContent();
            if (formContent != null) {
                EvaluationModel evaluationModel = new Gson().fromJson(
                        formContent, EvaluationModel.class);
                lbCheckDepartment.setValue(evaluationModel.getCheckDeptName());
            }
        }
    }

    private boolean isValidatedData() {
        clearWarningMessage();
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
        if (lstAttach == null || lstAttach.size() == 0) {
            setWarningMessage("Chưa ký văn bản phê duyệt");
            return false;
        } 

        return true;
    }

    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    private String getAutoNswFileCode(Long documentTypeCode) {

        String autoNumber;

        ImportOrderFileDAO dao = new ImportOrderFileDAO();
        Long autoNumberL = dao.countImportfile();
        autoNumberL += 1L;// Tránh số 0 (linhdx)
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    private void setWarningMessage(String message) {

        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);

    }

    /**
     *
     * @param event
     * @throws Exception
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) throws Exception {
        try
        {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
        String folderSignOut = resourceBundle.getString("signPdf");
        if (!new File(folderSignOut).exists()) {
            FileUtil.mkdirs(folderSignOut);
        }
        String fileSignOut = folderSignOut
                + "_signed_PheDuyetDonDangKy"
                + (new Date()).getTime() + ".pdf";

        Session session = Sessions.getCurrent();
        SignPdfFile signPdfFile = (SignPdfFile) session
                .getAttribute("PDFSignature");

        String signature = event.getData().toString();
        signPdfFile.insertSignature(signature, fileSignOut);

        attachSave(fileSignOut);

        showNotification("Ký số thành công!", Constants.Notification.INFO);
        // refresh page
        fillFinalFileListbox(fileId);
        

        
        }
        catch(Exception ex)
        {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công!", Constants.Notification.ERROR);
        }
    }


    /**
     * save file path into Attach table
     *
     * @throws Exception
     */
    public void attachSave(String filePath) throws Exception {

        AttachDAO attachDAO = new AttachDAO();
        attachDAO.saveFileAttachPdfSign(filePath, fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
    }

    public void updateAttachFileSigned() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
        if (lstAttach != null && lstAttach.size() > 0) {
            for (Attachs att : lstAttach) {
                Attachs att_new = att;
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att_new);
                }
            }
        }

    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     * @throws Exception
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        clearWarningMessage();
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        // get fdf file path that is insert data
        String tempPdfPath = createTempPdfFile();

        // sign pdf file (insert img)
        actionSignCA(event, tempPdfPath);
    }

    public String createTempPdfFile() throws Exception {

        // lay cong van
        Permit permit = this.getPermit(fileId);

        // vao so
        Long bookNumber = putInBook(permit.getPermitId());
        String receiveNo = getPermitReceiveNo(bookNumber);

        // update so vao so vao permit
        PermitDAO permitDAO = new PermitDAO();
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        // set infor for model to replay at template file
        ImportOrderFileDAO deviceDAO = new ImportOrderFileDAO();
        ImportOrderFile importOrderFile = deviceDAO.findByFileId(fileId);
        //linhdx - truong email phai bo ky tu < > neu khong se khong xuat ra file doc duoc
        importOrderFile.setResponsiblePersonEmail(importOrderFile.getResponsiblePersonEmail());
        importOrderFile.setExporterEmail(importOrderFile.getExporterEmail());
        importOrderFile.setGoodsOwnerEmail(importOrderFile.getGoodsOwnerEmail());
//        //not display
//        importOrderFile.setCheckTime(null);
//        importOrderFile.setCheckPlace(null);
//        importOrderFile.setCustomsBranchName(null);

        // get chu doanh nghiep
        Files files = new FilesDAOHE().findById(fileId);

        // set current date
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int days = cal.get(Calendar.DAY_OF_MONTH);
        int months = cal.get(Calendar.MONTH) + 1;
        int years = cal.get(Calendar.YEAR);
        String signedDate = "Ngày " + days + " tháng " + months
                + " năm " + years;

        ImportOrderProductDAO proDAO = new ImportOrderProductDAO();
        List<ImportOrderProduct> productList = proDAO.findAllIdByFileId(fileId);

        List<ImportOrderProductModel> productList_convert = proDAO.convertObjList(productList);

        IO_5_1_EvaluationAttpLDCucModel model = new IO_5_1_EvaluationAttpLDCucModel();
        model.setSendNo(permit.getReceiveNo());
        model.setFiles(files);
        model.setSignedDate(signedDate);
        model.setLeaderSigned(getUserFullName());
        model.setImportOrderFile(importOrderFile);
        model.setProductList(productList);
        model.setProductListModle(productList_convert);

        String tempPdfFile = this.exportTempPdfFile(model);
        return tempPdfFile;
    }

    private Permit getPermit(Long fileId) {
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileIdAndType(fileId, Constants.PERMIT_TYPE.REGISTERED_PAPER);
        Permit permit;

        // Da co cong van thi lay ban cu
        if (lstPermit.size() > 0) {
            permit = lstPermit.get(0);
            // Neu khong co thi tao ban moi
        } else {
            permit = new Permit();
            permit.setFileId(fileId);
            permit.setType(Constants.PERMIT_TYPE.REGISTERED_PAPER);
            permit.setIsActive(Constants.Status.ACTIVE);
            permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
            permit.setSignDate(new Date());
            permit.setReceiveDate(new Date());
            permitDAO.saveOrUpdate(permit);
        }

        return permit;
    }

    public String exportTempPdfFile(IO_5_1_EvaluationAttpLDCucModel model) {
        String tempPdfFilePath = null;
        try {

            HttpServletRequest request = (HttpServletRequest) Executions
                    .getCurrent().getNativeRequest();
            String docPath = request.getRealPath(PheDuyetDonDangKy);

            // Neu co dinh nghia bieu mau thi lay tu dinh nghia
            if (model.getPathTemplate() != null) {
                docPath = model.getPathTemplate();
            }

            WordprocessingMLPackage wmp = WordprocessingMLPackage
                    .load(new FileInputStream(docPath));

            ConcurrentHashMap map = new ConcurrentHashMap();
            map.put("createForm", model);

            WordExportUtils word = new WordExportUtils();
            word.replacePlaceholder(wmp, map);
            word.replaceTable(wmp, 1, model.getProductListModle());

            // convert word to PDF
            //PdfDocxFile pdfDocxFile = word.writePDFToStream(wmp, false);
            
            
            

            // write pdf to file
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String tempFolder = resourceBundle.getString("signTemp");
            if (!new File(tempFolder).exists()) {
                FileUtil.mkdirs(tempFolder);
            }

            tempPdfFilePath = tempFolder
                    + "PheDuyetDonDangKy_"
                    + (new Date()).getTime() + ".pdf";
            OutputStream outputStream = new FileOutputStream(tempPdfFilePath);
            //outputStream.write(pdfDocxFile.getContent());
            PdfSettings pdfSettings = new PdfSettings();
            PdfConversion converter = new org.docx4j.convert.out.pdf.viaXSLFO.Conversion(wmp);
            converter.output(outputStream, pdfSettings);
            
            outputStream.flush();
            outputStream.close();
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
        return tempPdfFilePath;
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Long permitId) {
        ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
        ImportOrderFile iof=imDAO.findByFileId(fileId);
        if(iof.getRegisterNo()!=null)
        {
            return iof.getRegisterNo();
        }
        BookDAOHE bookDAOHE = new BookDAOHE();
        List<Books> listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(permitId,
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        Long num=bookDocument.getBookNumber();
        iof.setRegisterNo(num);
        imDAO.saveOrUpdate(iof);
        return num;
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param documentId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long documentId, Long bookId) {

        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);

        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(documentId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);

        // Cap nhat so hien tai trong bang book
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);

        return bookDocument;
    }

    private String getPermitReceiveNo(Long bookNumber) {
        String permitReceiveNo = "";

        if (bookNumber != null) {
            permitReceiveNo = String.valueOf(bookNumber);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        String year = simpleDateFormat.format(Calendar.getInstance().getTime());
        permitReceiveNo += "/" + year + "/ĐKNK";

        return permitReceiveNo;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        String certSerial = x509Cert.getSerialNumber().toString(16);
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caUserList = ca
                .findCaBySerial(certSerial, 1L, getUserId());
        if (caUserList != null && caUserList.size() > 0) {
            SignPdfFile pdfSig = new SignPdfFile();
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String linkImageSign = folderPath + separator
                    + caUserList.get(0).getSignature();
            String linkImageStamp = folderPath + separator
                    + caUserList.get(0).getStamper();
            Pdf pdf = new Pdf();

            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String outputFileFinal = resourceBundle.getString("signTemp") + "_"
                    + (new Date()).getTime() + ".pdf";
            // chen chu ki
            pdf.insertImageAll(fileToSign, outputFileFinal, linkImageSign,
                    linkImageStamp, null);
            if (pdf.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }
            // chen CKS
            String base64Hash = pdfSig.createHash(outputFileFinal,
                    new Certificate[]{x509Cert});
            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64HASH.setValue(base64Hash);
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");

        } else {
            showNotification("Ký số không thành công!");
        }
    }

    // load all of singed pdf file
    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    // download signed pdf
    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs attachs = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(attachs);
    }

    // delete signed pdf
    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        return Constants.CHECK_VIEW.VIEW;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }
}
