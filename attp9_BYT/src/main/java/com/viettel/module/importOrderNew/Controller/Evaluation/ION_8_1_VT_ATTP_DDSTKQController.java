/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrderNew.Controller.Evaluation;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.newsignature.plugin.SignPdfFile;
import com.viettel.newsignature.utils.CertUtils;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;

import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.ResourceBundle;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author duv
 */
public class ION_8_1_VT_ATTP_DDSTKQController extends BusinessController {

    private static final long serialVersionUID = 1L;

    @Wire
    private Textbox txtMessage;
    @Wire
    private Listbox finalFileListbox, finalFileListboxKy;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    @Wire
    private Textbox txtValidate, txtFinishDate;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    private Long fileId;
    private List<Attachs> lstAttach;
    private List<Attachs> lstAttachKy;
    private UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
            "userToken");

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        // load all of signed file
        fillFinalFileListbox(fileId);
        fillFinalFileListboxKy(fileId);
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        clearWarningMessage();
        try {
            if (!isValidatedData()) {
                return;
            }

            updateAttachFileSigned();

            sendMS();

            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    private boolean isValidatedData() {
        AttachDAOHE attDAO = new AttachDAOHE();
        Attachs at = attDAO.getByObjectIdAndAttachType(fileId, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER, true,"VT");
        if (at == null) {
            showNotification(String.format(
                    "Hãy thực hiện đóng dấu số", Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
            return false;
        }
        return true;
    }

    @Listen("onSign = #businessWindow")
    public void onSign(Event event) throws Exception {
        try {
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String folderSignOut = resourceBundle.getString("signPdf");
            if (!new File(folderSignOut).exists()) {
                FileUtil.mkdirs(folderSignOut);
            }
            String fileSignOut = folderSignOut
                    + "_signed_GiayThongbaoketquakiemtragiam"
                    + (new Date()).getTime() + ".pdf";

            Session session = Sessions.getCurrent();
            SignPdfFile signPdfFile = (SignPdfFile) session
                    .getAttribute("PDFSignature");
            String outputFileFinal = session
                    .getAttribute("outputFileFinal").toString();
            String signature = event.getData().toString();
            signPdfFile.insertSignature(signature, outputFileFinal, fileSignOut);

            attachSave(fileSignOut);

            showNotification("Ký số thành công!", Constants.Notification.INFO);
            // refresh page
            fillFinalFileListboxKy(fileId);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công!", Constants.Notification.ERROR);
        }
    }

    public void attachSave(String filePath) throws Exception {

        AttachDAO attachDAO = new AttachDAO();
        attachDAO.saveFileAttachPdfSign(filePath, fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER,"VT");
    }

    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        clearWarningMessage();
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        // get fdf file path that is insert data\

        
        AttachDAOHE rDaoHe = new AttachDAOHE();
        lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachTypeIsSent(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER, 1l);
        if(lstAttach.size()>0){
            Attachs a = lstAttach.get(0);
            actionSignCA(event, a.getFullPathFile());
        } else{
             showNotification("Chưa có file lãnh đạo ký số");
        }
//        AttachDAOHE attDAO = new AttachDAOHE();
//        Attachs a = attDAO.getByObjectIdAndAttachType(fileId, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER, false);
//        if(a==null){
//            a = attDAO.getByObjectIdAndAttachType(fileId, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER, true);
//        }
//        actionSignCA(event, a.getFullPathFile());
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        // FileUtil.mkdirs(filePath);
        // String outputFileFinalName =
        String certSerial = x509Cert.getSerialNumber().toString(16);
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caUserList = ca
                .findCaBySerial(certSerial, 1L, getUserId());
        if (caUserList != null && caUserList.size() > 0) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
//            String linkImageSign = folderPath + separator
//                    + caUserList.get(0).getSignature();
            String linkImageStamp = folderPath + separator
                    + caUserList.get(0).getStamper();
            Pdf pdf = new Pdf();
            String fieldName = "VT" + tk.getUserFullName();
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String outputFileFinal = resourceBundle.getString("signTemp") + "_"
                    + (new Date()).getTime() + ".pdf";
            SignPdfFile pdfSig = pdf.createHash(fileToSign, outputFileFinal, new Certificate[]{x509Cert}, linkImageStamp, fieldName);
//			// chen chu ki
//			pdf.insertImageAll(fileToSign, outputFileFinal, linkImageSign,
//					linkImageStamp, null);
//			// chen CKS
//			String base64Hash = pdfSig.createHash(outputFileFinal,
//					new Certificate[] { x509Cert });
            if (pdfSig == null) {
                showNotification("Ký số không thành công!");
                return;
            }
            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", pdf.getBase64Hash());
            session.setAttribute("outputFileFinal", outputFileFinal);
            txtBase64HASH.setValue(pdf.getBase64Hash());
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");

        } else {
            showNotification("Ký số không thành công!");
        }
    }

    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_15);
        md.setPhase(0l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
        txtFinishDate.setValue("1");
    }

    // load all of singed pdf file
    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachTypeIsSent(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER, 1l);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    // load all of singed pdf file
    private void fillFinalFileListboxKy(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        lstAttachKy = rDaoHe
                .getByObjectIdAndAttachCatAndAttachTypeIsSent(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER, 0l);
        this.finalFileListboxKy.setModel(new ListModelArray(lstAttachKy));
    }

    // download signed pdf
    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs attachs = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(attachs);
    }

    @Listen("onDownloadFinalFile = #finalFileListboxKy")
    public void onDownloadFinalFileKy(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);
    }

    @Listen("onDeleteFinalFile = #finalFileListboxKy")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListboxKy(fileId);
    }

    private void setWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    public void updateAttachFileSigned() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        if (lstAttachKy != null && lstAttachKy.size() > 0) {
            for (Attachs att : lstAttachKy) {
                Attachs att_new = att;
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.getSession().merge(att_new);
                    //rDaoHe.saveOrUpdate(att_new);
                }
            }
        }
    }

}
