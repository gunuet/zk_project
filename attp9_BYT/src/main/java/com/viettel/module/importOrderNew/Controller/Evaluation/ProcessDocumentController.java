package com.viettel.module.importOrderNew.Controller.Evaluation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.ProcessDocument;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.ProcessDocumentDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ValidatorUtil;
import com.viettel.voffice.BO.Files;

import java.text.DateFormat;

import org.zkoss.zul.Timebox;

/**
 *
 * @author THANHDV
 */
public class ProcessDocumentController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Window businessWindow;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbProcessname, tbProcessAdddress, tbProcessPhone;
    @Wire
    private Textbox txtValidate, txtMessage, txtCurrentDept;
    @Wire
    private Combobox cmbType;
    @Wire
    Label lbBusiness, lbCheckdate, lbAddress, lbBusinessPhone;
    @Wire
    private Datebox dbCheckdate;
    @Wire
    private Timebox dbChecktime;
    @Wire
    private Listbox lbOrderProduct;
    private ImportOrderFile cosFile;
    private ProcessDocument documentProcess;

    public ProcessDocument getDocumentProcess() {
        return documentProcess;
    }

    public void setDocumentProcess(ProcessDocument documentProcess) {
        this.documentProcess = documentProcess;
    }

    public ImportOrderFile getCosFile() {
        return cosFile;
    }

    public void setCosFile(ImportOrderFile cosFile) {
        this.cosFile = cosFile;
    }

    @Wire
    private Vlayout flist;
    private List<Media> listMedia;
    private Files files;
    private long FILE_TYPE;
    private final static long DN = 0;
    private final static long CV = 1;
    private String FILE_NAME;

    /**
     * vunt Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {

        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        listMedia = new ArrayList();
        cosFile = (new ImportOrderFileDAO()).findById(fileId);
        documentProcess = (new ProcessDocumentDAO()).getProcessDocumentByType(
                DN, fileId);
        files = new Files();
        // Vao truc tiep file
        WorkflowAPI w = new WorkflowAPI();

        FILE_TYPE = w
                .getProcedureTypeIdByCode(Constants.IMPORT_ORDER_TYPE.ORDER_OBJECT);
        FILE_NAME = w
                .getProcedureNameByCode(Constants.IMPORT_ORDER_TYPE.ORDER_OBJECT);
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        /*	List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO()
         .findAllIdByFileIdAndCheckMethodCode(fileId, "CHAT");
         */ List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(fileId);
        ListModelArray lstModelManufacturer = new ListModelArray(
                importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);
        txtValidate.setValue("0");
        txtCurrentDept.setValue("1");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        if (!isValidatedData()) {
            return;
        }
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbProcessname.getText().matches("\\s*")) {
            showWarningMessage("Tên cán bộ kiểm tra không thể để trống");
            tbProcessname.focus();
            return false;
        }
        if (dbCheckdate.getText().matches("\\s*")) {
            showWarningMessage("Thời gian kiểm tra hàng hóa  không thể để trống");
            dbCheckdate.focus();
            return false;
        }
        if (dbChecktime.getText().matches("\\s*")) {
            showWarningMessage("Thời gian kiểm tra hàng hóa  không thể để trống");
            dbChecktime.focus();
            return false;
        }
        
        if (tbProcessAdddress.getText().matches("\\s*")) {
            showWarningMessage("Địa điểm tập kết  không thể để trống");
            tbProcessAdddress.focus();
            return false;
        }
        
        if (tbProcessPhone.getText().matches("\\s*")) {
            showWarningMessage("Số điện thoại  không thể để trống");
            tbProcessPhone.focus();
            return false;
        }
        if ((ValidatorUtil.validateTextbox(tbProcessPhone, true, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER, 9)) != null) {
            showWarningMessage("Số điện thoại phải là kiểu số từ 9 đến 15 kí tự ");
            tbProcessPhone.focus();
            return false;
        }
        
        

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    @SuppressWarnings("unused")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Thieu phan comment
            Gson gson = new Gson();
            ProcessDocument processDocument = new ProcessDocument();
            processDocument.setProcessName(tbProcessname.getText().toString()
                    .trim());
            processDocument.setProcessPhone(tbProcessPhone.getText().toString()
                    .trim());
            processDocument.setCheckPlace(tbProcessAdddress.getText()
                    .toString().trim());
            // xử lý date
            String inputDateDate = "" + dbCheckdate.getText() + " "
                    + dbChecktime.getText();
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = format.parse(inputDateDate);

            processDocument.setCheckDate(date);
            processDocument.setIsActive(Constants.Status.ACTIVE);
            processDocument.setCreatedDate(new Date());

            processDocument.setFileId(fileId);
            processDocument.setType(CV);
            ProcessDocumentDAO objDAO = new ProcessDocumentDAO();
            objDAO.saveOrUpdate(processDocument);

            MessageModel md = new MessageModel();
            md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
            md.setFileId(fileId);
            md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_23);
            md.setPhase(0l);
            md.setFeeUpdate(false);
            String jsonMd = gson.toJson(md);
            txtMessage.setValue(jsonMd);

            txtValidate.setValue("1");

        } catch (WrongValueException e) {
            LogUtils.addLogDB(e);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

}
