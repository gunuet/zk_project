package com.viettel.module.importOrderNew.Controller.InputResCheckAttp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.BO.Port;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.sys.DAO.PortDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.BO.ProcessDocument;
import com.viettel.module.importOrder.BO.ProductProductTarget;
import com.viettel.module.importOrder.BO.VAttfileCategory;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.BO.VProductTarget;
import com.viettel.module.importOrder.BO.VimportOrderFileAttach;
import com.viettel.module.importOrder.Controller.QrCodeController;
import com.viettel.module.importOrder.DAO.ImportOrderAttachDao;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.ProcessDocumentDAO;
import com.viettel.module.importOrder.DAO.ProductProductTargetDAO;
import com.viettel.module.importOrder.DAO.VAttfileCategoryDAO;
import com.viettel.module.importOrder.DAO.VProductTargetDAO;
import com.viettel.module.importOrder.Model.IO_20_1_LDC_KSPDHSModel;
import com.viettel.module.importOrder.Model.ImportOrderProductModel;
import com.viettel.newsignature.plugin.SignPdfFile;
import com.viettel.newsignature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.utils.WordExportUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import java.text.DecimalFormat;

/**
 *
 * @author THANHDV
 */
public class LdcSignAndAcceptController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, producIdHidden, lbFileId;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private Long fileId;
    private VFileImportOrder cosFile;
    @Wire
    Listbox lbOrderProduct;
    @Wire
    Listbox lbProductTarget;
    Long documentTypeCode;
    private Files files;
    private String nswFileCode;
    private List<Category> listImportOderFileType;
    private List<Media> listMedia, listFileExcel;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;
    private final int IMPORT_ORDER_FILE = 1;
    private Users user;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Button btnCreateProFile;

    private CosEvaluationRecord obj = new CosEvaluationRecord();
    @Wire
    private Window businessWindow;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    private Vlayout flist, fListImportExcel;
    @Wire
    private Listbox listDepartent, fileListbox, fileListboxOrderFile;
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    @Wire
    private Listbox finalFileListbox;

    private Long docType;
    private String bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_PERMIT_XNK;
    private String templatePheDuyetHoSo = "/WEB-INF/template/PheDuyetHoSo_CQKT.docx";
    private int flag_click = 0;

    @Wire
    Textbox txtCurrentDept;

    private UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute("userToken");

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        listMedia = new ArrayList();
        // Load Order
        cosFile = (new ImportOrderFileViewDAO()).findById(fileId);
        files = (new FilesDAOHE()).findById(fileId);
        // load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        nswFileCode = getAutoNswFileCode(documentTypeCode);
        docType = (Long) arguments.get("docType");

        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        txtValidate.setValue("0");
        txtCurrentDept.setValue("1");
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("id", fileId);
        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);
        //List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findByCheckMethodCode(fileId);
        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO().findAllIdByFileId(fileId);
        ListModelArray lstModelManufacturer = new ListModelArray(
                importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);
        lbFileId.setValue(String.valueOf(fileId));

        if (importOrderProducts != null && !importOrderProducts.isEmpty()) {
            showProduct(importOrderProducts.get(0).getProductId());
        }

        // load all of signed file
        fillFinalFileListbox(fileId);
        if (importOrderProducts != null) {
            if (flag_click == 0 && importOrderProducts.size() > 0) {
                ImportOrderProduct first_obj = importOrderProducts.get(0);
                setViewFirstClick(first_obj.getProductId());
            }
        }

        flag_click = 1;
    }

    public void setViewFirstClick(Long pr_id) {
        showProduct(pr_id);
        fileListboxOrderFile.setVisible(true);
    }

    @Listen("onOpenUpdate = #lbProductTarget")
    public void onOpenUpdate(Event event) {
        VProductTarget obj = (VProductTarget) event.getData();
        Long targetId = obj.getId();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("producId", producIdHidden.getValue());
        arguments.put("targetId", targetId);
        arguments.put("method", "update");
        setParam(arguments);
        createWindow(
                "windowCRUDCosmetic",
                "/Pages/module/importorder/inputResultCheck/inputResCheckProTarget.zul",
                arguments, Window.HIGHLIGHTED);
    }

    @Listen("onDelete = #lbProductTarget")
    public void onDelete(Event event) {
        final VProductTarget obj = (VProductTarget) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

            @Override
            public void onEvent(Event event) {
                if (null != event.getName()) {
                    switch (event.getName()) {
                        case Messagebox.ON_OK:
                            // OK is clicked
                            try {

                                VProductTargetDAO objDAOHE = new VProductTargetDAO();
                                objDAOHE.delete(obj.getId());
                                reload();

                            } catch (Exception e) {
                                showNotification(
                                        String.format(
                                                Constants.Notification.DELETE_ERROR,
                                                Constants.DOCUMENT_TYPE_NAME.FILE),
                                        Constants.Notification.ERROR);
                                LogUtils.addLogDB(e);
                            } finally {
                            }
                            break;
                        case Messagebox.ON_CANCEL:
                            break;
                    }
                }
            }
        });
    }

    public void reload() {
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        List<VProductTarget> productProductTarget = new VProductTargetDAO()
                .findByProdutId(productId);
        ListModelArray lstModel_ProducTarget = new ListModelArray(
                productProductTarget);
        lbProductTarget.setModel(lstModel_ProducTarget);
    }

    @Listen("onShow =  #lbOrderProduct")
    public void onShow(Event event) {
        ImportOrderProduct obj = (ImportOrderProduct) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        Long productId = obj.getProductId();
        arguments.put("id", productId);
        arguments.put("CRUDMode", "UPDATE");
        showProduct(productId);
    }

    public void showProduct(Long productId) {
        // set list danh sách chỉ tiêu của sản một sản phẩm
        List<VProductTarget> productProductTarget = new VProductTargetDAO()
                .findByProdutId(productId);

        // get sanr pham
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();
        ImportOrderProduct importOrderProduct = importOrderProductDAO
                .findById(productId);
        if (productProductTarget != null) {
            ListModelArray lstModel_ProducTarget = new ListModelArray(
                    productProductTarget);

            lbProductTarget.setModel(lstModel_ProducTarget);
            String value = importOrderProduct.getCheckMethodCode();
            if (value == null) {
                lbProductTarget.setVisible(false);
            } else if ("THUONG".equals(value)) {
                lbProductTarget.setVisible(true);
            } else {
                lbProductTarget.setVisible(true);
            }
        } else {
            lbProductTarget.setVisible(false);
        }
        // load lại danh sách tập tin theo productID
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, fileId);
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS, fileId);

        // set view
        producIdHidden.setValue(String.valueOf(productId));
        fileListboxOrderFile.setVisible(true);
        fileListbox.setVisible(true);
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", businessWindow);
        return arguments;
    }

    @Listen("onChildWindowClosed=#businessWindow")
    public void onChildWindowClosed() {
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        fillListData(productId);
    }

    public void fillListData(Long productId) {
        List<VProductTarget> productProductTarget = new VProductTargetDAO()
                .findByProdutId(productId);
        if (productProductTarget != null) {
            ListModelArray lstModel_ProducTarget = new ListModelArray(
                    productProductTarget);
            lbProductTarget.setModel(lstModel_ProducTarget);
        }

    }

    // update status = 1 for productproducttarget
    private void approveProducProducTarget() {
        // get all of product for fileId
        List<ImportOrderProduct> importOrderProductsList = new ImportOrderProductDAO()
                .findByCheckMethodCode(fileId);
        ProductProductTargetDAO productProductTargetDAO;
        List<ProductProductTarget> productProductTargetsList;
        for (Iterator iterator = importOrderProductsList.iterator(); iterator
                .hasNext();) {
            ImportOrderProduct importOrderProduct = (ImportOrderProduct) iterator
                    .next();
            // get all of productProductTarget for each product
            productProductTargetDAO = new ProductProductTargetDAO();
            productProductTargetsList = productProductTargetDAO
                    .findByProductId(importOrderProduct.getProductId());
            for (Iterator iterator2 = productProductTargetsList.iterator(); iterator2
                    .hasNext();) {
                ProductProductTarget productProductTarget = (ProductProductTarget) iterator2
                        .next();

                // set status = 1 for productProductTarget
                productProductTarget.setStatus(new Long(1));
                productProductTargetDAO = new ProductProductTargetDAO();
                productProductTargetDAO.saveOrUpdate(productProductTarget);
            }
        }
    }

    public boolean isValidate() {

        // check da ky van ban chua
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
        if (lstAttach == null || lstAttach.size() == 0) {
            showWarningMessage("Chưa ký văn bản phê duyệt");
            return false;
        }

        return true;
    }

    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    private void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    private String getAutoNswFileCode(Long documentTypeCode) {

        String autoNumber;

        ImportOrderFileDAO dao = new ImportOrderFileDAO();
        Long autoNumberL = dao.countImportfile();
        autoNumberL += 1L;// Tránh số 0 (linhdx)
        autoNumber = String.valueOf(autoNumberL);
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

    public void onChangelbLegal() {
        // String value = cbtexLegal.getSelectedItem().getValue();
        String prId = producIdHidden.getValue();
        if (prId != null && !"".equals(prId)) {
            lbProductTarget.setVisible(true);

        }

    }

    /*
     * Tichnv :nhoms chi tieeu
     * 
     * @param :
     * 
     * @return
     */
    public ListModelList getListBoxModel_ChiTieu() {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;

        categoryDAOHE = new CategoryDAOHE();
        listImportOderFileType = categoryDAOHE
                .findAllCategory(Constants.CATEGORY_TYPE.TESTTYPE_IMDOC);
        lstModel = new ListModelList(listImportOderFileType);

        return lstModel;
    }

    // attach File
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case IMPORT_ORDER_FILE:
                categoryDAOHE = new CategoryDAOHE();
                listImportOderFileType = categoryDAOHE
                        .findAllCategory(Constants.CATEGORY_TYPE.FILECAT_IMDOC);
                lstModel = new ListModelList(listImportOderFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    // tải tệp đính kèm của cả hồ sơ
    @Listen("onDownloadOrderFile =#fileListboxOrderFile")
    public void onDownloadOrderFile(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VAttfileCategory obj = (VAttfileCategory) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VimportOrderFileAttach obj = (VimportOrderFileAttach) event.getData();
        String prId = producIdHidden.getValue();
        Long productId = new Long(prId);
        ImportOrderAttachDao rDAOHE = new ImportOrderAttachDao();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, productId);
    }

    private void fillFileListbox(Long obj_type, Long obj_id) {
        VAttfileCategoryDAO dao = new VAttfileCategoryDAO();
        List<VAttfileCategory> lstCosmeticAttach = dao.findCheckedFilecAttach(obj_type, obj_id);
        if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS.equals(obj_type)) {
            //lấy thêm file kiểm tra 
            List<VAttfileCategory> temp_list = dao.findCheckedFilecAttach(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, fileId);
            lstCosmeticAttach.addAll(temp_list);
            fileListboxOrderFile.setModel(new ListModelArray(lstCosmeticAttach));
        } else if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST.equals(obj_type)) {
            fileListbox.setModel(new ListModelArray(lstCosmeticAttach));
        }

    }

    public CosEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(CosEvaluationRecord obj) {
        this.obj = obj;
    }

    public VFileImportOrder getCosFile() {
        return cosFile;
    }

    public void setCosFile(VFileImportOrder cosFile) {
        this.cosFile = cosFile;
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        return Constants.CHECK_VIEW.VIEW;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            if (!isValidate()) {
                return;
            }

            approveProducProducTarget();
            updateAttachFileSigned();

            txtValidate.setValue("1");

        } catch (WrongValueException e) {
            LogUtils.addLogDB(e);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    public void updateAttachFileSigned() {
        // get file attach
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
        if (lstAttach != null && lstAttach.size() > 0) {
            for (Attachs att : lstAttach) {
                Attachs att_new = att;
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att_new);
                }
            }
        }

    }

    /**
     *
     * @param event
     * @throws Exception
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) throws Exception {
        try {
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String folderSignOut = resourceBundle.getString("signPdf");
            if (!new File(folderSignOut).exists()) {
                FileUtil.mkdirs(folderSignOut);
            }
            String fileSignOut = folderSignOut + "_signed_PheDuyetHoSo"
                    + (new Date()).getTime() + ".pdf";

            Session session = Sessions.getCurrent();
            SignPdfFile signPdfFile = (SignPdfFile) session
                    .getAttribute("PDFSignature");

            String outputFileFinal = session
                    .getAttribute("outputFileFinal").toString();

            String signature = event.getData().toString();
            //signPdfFile.insertSignature(signature, fileSignOut);

            signPdfFile.insertSignature(signature, outputFileFinal, fileSignOut);

            attachSave(fileSignOut);

            showNotification("Ký số thành công!", Constants.Notification.INFO);
            // refresh page
            fillFinalFileListbox(fileId);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công!", Constants.Notification.ERROR);
        }
    }

    /**
     * save file path into Attach table
     *
     * @throws Exception
     */
    public void attachSave(String filePath) throws Exception {

        AttachDAO attachDAO = new AttachDAO();
        attachDAO.saveFileAttachPdfSign(filePath, fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     * @throws Exception
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        clearWarningMessage();
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        // get fdf file path that is insert data
        String tempPdfPath = createTempPdfFile();

        // sign pdf file (insert img)
        actionSignCA(event, tempPdfPath);
    }

    /**
     * tao file ki so.
     *
     * @return
     * @throws Exception
     */
    public String createTempPdfFile() throws Exception {

        // lay cong van
        Permit permit = this.getPermit(fileId);
        // vao so
        Long bookNumber = putInBook(permit.getPermitId());
        String receiveNo = getPermitReceiveNo(bookNumber);

        // update so vao so vao permit
        PermitDAO permitDAO = new PermitDAO();
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        // set infor for model to replay at template file
        ImportOrderFileDAO deviceDAO = new ImportOrderFileDAO();
        ImportOrderFile importOrderFile = deviceDAO.findByFileId(fileId);

        // set thời gain kiểm tra,địa điểm kiểm tra
        ProcessDocumentDAO processDocumentDAO = new ProcessDocumentDAO();
        ProcessDocument processDocument = processDocumentDAO
                .getProcessDocumentByType(1L, fileId);

        // get chu doanh nghiep
        Files files = new FilesDAOHE().findById(fileId);

        // set product
        ImportOrderProductDAO proDAO = new ImportOrderProductDAO();

        List<ImportOrderProduct> productList = proDAO.findAllIdByFileId(fileId);

        List<ImportOrderProductModel> productModelList = new ArrayList<ImportOrderProductModel>();
        ImportOrderProductModel importOrderProductModel;
        Long pass;
        DecimalFormat format = new DecimalFormat("###,###,###,###,###.###");
        for (Iterator iterator = productList.iterator(); iterator.hasNext();) {
            importOrderProductModel = new ImportOrderProductModel();
            ImportOrderProduct importOrderProduct = (ImportOrderProduct) iterator
                    .next();
            importOrderProductModel.setProductName(importOrderProduct
                    .getProductName());
            importOrderProductModel.setConfirmAnnounceNo(importOrderProduct
                    .getConfirmAnnounceNo());
            importOrderProductModel.setNationalName(importOrderProduct
                    .getNationalName() + (importOrderProduct.getManufacTurer() == null ? "" : ", " + importOrderProduct.getManufacTurer()) + (importOrderProduct.getManufacturerAddress() == null ? "" : ", " + importOrderProduct.getManufacturerAddress()));

            importOrderProductModel.setTotal(importOrderProduct.getTotal());
            importOrderProductModel.setNetweight(importOrderProduct
                    .getNetweight());
            if (!StringUtils.validString(importOrderProduct
                    .getCheckMethodName()) && StringUtils.validString(importOrderProduct
                            .getCheckMethodCode())) {
                switch (importOrderProduct
                        .getCheckMethodCode()) {
                    case "GIAM":
                        importOrderProduct.setCheckMethodName("Kiểm tra giảm");
                        break;
                    case "THUONG":
                        importOrderProduct.setCheckMethodName("Kiểm tra thường");
                        break;
                    case "CHAT":
                        importOrderProduct.setCheckMethodName("Kiểm tra chặt");
                        break;
                }
            }
            importOrderProductModel.setCheckMethodName(importOrderProduct
                    .getCheckMethodName());
            importOrderProductModel.setV_baseUnit(format.format(importOrderProduct.getBaseUnit()) + " VNĐ");
            importOrderProductModel.setV_netweight(format.format(importOrderProduct.getNetweight()) + (importOrderProduct.getNetweightUnitName() == null ? "" : " " + importOrderProduct.getNetweightUnitName()));
            importOrderProductModel.setV_total(format.format(importOrderProduct.getTotal()) + (importOrderProduct.getTotalUnitName() == null ? "" : " " + importOrderProduct.getTotalUnitName()));
            pass = importOrderProduct.getPass();
            if (pass != null && pass == 1) {
                importOrderProductModel.setPass("Đạt");
                importOrderProductModel.setNotPass("Không đạt");
            } else {
                importOrderProductModel.setPass("Không đạt");
                importOrderProductModel.setNotPass("Không đạt");
            }

            importOrderProductModel.setReason(importOrderProduct.getReason());
            importOrderProductModel.setProductDescription(importOrderProduct.getProductDescription());
            productModelList.add(importOrderProductModel);
        }

        //
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int days = cal.get(Calendar.DAY_OF_MONTH);
        int months = cal.get(Calendar.MONTH) + 1;
        int years = cal.get(Calendar.YEAR);
        String signedDate = "Ngày " + days + " tháng " + months
                + " năm " + years;

        IO_20_1_LDC_KSPDHSModel model = new IO_20_1_LDC_KSPDHSModel();
        model.setSendNo(permit.getReceiveNo());
        model.setBusinessName(files.getBusinessName());
        model.setBusinessAddress(files.getBusinessAddress());
        model.setBusinessPhone(files.getBusinessPhone());
        model.setBusinessFax(files.getBusinessFax());
        model.setBusinessEmail(files.getBusinessEmail());
        model.setSignedDate(signedDate);
        model.setLeaderSigned(getUserFullName());
        model.setProductList(productModelList);
        model.setProcessDocument(processDocument);
        if (!StringUtils.validString(importOrderFile.getImporterGateName()) && StringUtils.validString(importOrderFile.getImporterGateCode())) {
            PortDAOHE pDAO = new PortDAOHE();
            Port p = pDAO.findByCode(importOrderFile.getImporterGateCode());
            if (p != null) {
                importOrderFile.setImporterGateName(p.getName());
            }
        }
        model.setImportOrderFile(importOrderFile);
        QrCodeController qrCodeController = new QrCodeController();
        byte[] qrcode = qrCodeController.createQrCodeOnSigning(fileId, receiveNo, new Date());
        String tempPdfFile = this.exportTempPdfFile(model, qrcode);
        return tempPdfFile;
    }

    /*
    *tạo bản công bố by FileId - binhnt edit
     */
    private Permit getPermit(Long fileId) {
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileIdAndType(
                fileId, Constants.PERMIT_TYPE.FILE_PAPER);
        Permit permit;
        // Da co cong van thi lay ban cu
        if (lstPermit.size() > 0) {
            permit = lstPermit.get(0);
            // Neu khong co thi tao ban moi
        } else {
            permit = new Permit();
            permit.setFileId(fileId);
            permit.setType(Constants.PERMIT_TYPE.FILE_PAPER);
            permit.setIsActive(Constants.Status.ACTIVE);
            permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
            permit.setSignDate(new Date());
            permit.setReceiveDate(new Date());
            permitDAO.saveOrUpdate(permit);
        }

        return permit;
    }
//binhnt edit - 17/02/06 - bổ sung barcode - 

    private String exportTempPdfFile(IO_20_1_LDC_KSPDHSModel model, byte[] qrcode) {
        String tempPdfFilePath = null;
        try {

            HttpServletRequest request = (HttpServletRequest) Executions
                    .getCurrent().getNativeRequest();
            String docPath = request.getRealPath(templatePheDuyetHoSo);

            // Neu co dinh nghia bieu mau thi lay tu dinh nghia
            if (model.getPathTemplate() != null) {
                docPath = model.getPathTemplate();
            }

            // Date signedDate = model.getSignedDate();
            // String signDateStr = "";
            // if (signedDate != null) {
            // Calendar cal = Calendar.getInstance();
            // cal.setTime(signedDate);
            // int days = cal.get(Calendar.DAY_OF_MONTH);
            // int months = cal.get(Calendar.MONTH) + 1;
            // int years = cal.get(Calendar.YEAR);
            // signDateStr = "Hà Nội, ngày " + days + " tháng " + months
            // + " năm " + years;
            // } else {
            // int days = signedDate.getDay();
            // int months = signedDate.getMonth();
            // int years = signedDate.getYear();
            // signDateStr = "Hà Nội, ngày " + days + " tháng " + months
            // + " năm " + years;
            // }

            /* replace template doc by data of model */
            WordprocessingMLPackage wmp = WordprocessingMLPackage
                    .load(new FileInputStream(docPath));

            ConcurrentHashMap map = new ConcurrentHashMap();
            map.put("createForm", model);

            WordExportUtils wordExportUtils = new WordExportUtils();
            wordExportUtils.replacePlaceholder(wmp, map);
            // wordExportUtils.replacePlaceholder(wmp, signDateStr,
            // "${signDateStr}");
            wordExportUtils.replaceTable(wmp, 1, model.getProductList());

            // convert word to PDF binhnt edit 17/02/07
            PdfDocxFile pdfDocxFile = wordExportUtils.writePDFQrCodeToStream(wmp, qrcode, false);

            // write pdf to file
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String tempFolder = resourceBundle.getString("signTemp");
            if (!new File(tempFolder).exists()) {
                FileUtil.mkdirs(tempFolder);
            }

            tempPdfFilePath = tempFolder + "PheDuyetHoSo_"
                    + (new Date()).getTime() + ".pdf";
            OutputStream outputStream = new FileOutputStream(tempPdfFilePath);
            outputStream.write(pdfDocxFile.getContent());
            outputStream.flush();
            outputStream.close();
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
        return tempPdfFilePath;
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Long permitId) {
        ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
        ImportOrderFile iof = imDAO.findByFileId(fileId);
        if (iof.getPermitNo() != null) {
            return iof.getPermitNo();
        }
        BookDAOHE bookDAOHE = new BookDAOHE();
        List<Books> listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(permitId,
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        Long num = bookDocument.getBookNumber();
        iof.setPermitNo(num);
        imDAO.saveOrUpdate(iof);
        return num;
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param documentId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long documentId, Long bookId) {

        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);

        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(documentId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);

        // Cap nhat so hien tai trong bang book
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);

        return bookDocument;
    }

    private String getPermitReceiveNo(Long bookNumber) {
        String permitReceiveNo = "";

        if (bookNumber != null) {
            permitReceiveNo = String.valueOf(bookNumber);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy");
        String year = simpleDateFormat.format(Calendar.getInstance().getTime());
        permitReceiveNo += "/" + year + "/ATTP";

        return permitReceiveNo;
    }

    /**
     *
     * @param event
     * @param fileToSign
     * @throws Exception
     */
    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        String certSerial = x509Cert.getSerialNumber().toString(16);
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caUserList = ca
                .findCaBySerial(certSerial, 1L, getUserId());
        if (caUserList != null && caUserList.size() > 0) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String linkImageSign = folderPath + separator
                    + caUserList.get(0).getSignature();
            Pdf pdf = new Pdf();
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String outputFileFinal = resourceBundle.getString("signTemp") + "_"
                    + (new Date()).getTime() + ".pdf";

            String fieldName = tk.getUserFullName();

            SignPdfFile pdfSig = pdf.createSignHash(fileToSign, outputFileFinal, new Certificate[]{x509Cert}, linkImageSign, fieldName);
            if (pdfSig == null) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }
            // chen chu ki
            /*String base64Hash;
             try {
             pdf.insertImageAll(fileToSign, outputFileFinal, linkImageSign,
             linkImageStamp, null);
             } catch (IOException ex) {
             Logger.getLogger(LdcSignAndAcceptController.class.getName())
             .log(Level.SEVERE, null, ex);
             }
             // chen CKS
             try {
             base64Hash = pdfSig.createHash(outputFileFinal,	new Certificate[] { x509Cert });
             } catch (Exception ex) {
             Logger.getLogger(LdcSignAndAcceptController.class.getName())
             .log(Level.SEVERE, null, ex);
             }*/

            Session session = Sessions.getCurrent();
            session.setAttribute("outputFileFinal", outputFileFinal);
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", pdf.getBase64Hash());
            txtBase64HASH.setValue(pdf.getBase64Hash());
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");

        } else {
            showNotification("Ký số không thành công!");
        }
    }

    // load all of singed pdf file
    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    // download signed pdf
    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs attachs = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(attachs);
    }

    // delete signed pdf
    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

}
