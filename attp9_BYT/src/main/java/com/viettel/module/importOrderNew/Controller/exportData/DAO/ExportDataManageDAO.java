/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrderNew.Controller.exportData.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrderNew.Controller.exportData.Model.SearchModel;
import com.viettel.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class ExportDataManageDAO extends
        GenericDAOHibernate<ImportOrderFile, Long> {

    public ExportDataManageDAO() {
        super(ImportOrderFile.class);
    }

    public List searchFile(SearchModel searchModel) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) ");
        StringBuilder hql = new StringBuilder();
        hql.append(" FROM VFileImportOrder n, Process p, Department d WHERE"
                + " n.fileId = p.objectId AND "
                + " n.fileType = p.objectType AND"
                + " p.sendGroupId = d.deptId ");
        if (searchModel.getDbFromDay() != null) {
            hql.append(" AND n.createDate >= ? ");
            listParam.add(searchModel.getDbFromDay());
        }
        if (searchModel.getDbToDay()!= null) {
            hql.append(" AND n.createDate <= ? ");
            listParam.add(searchModel.getDbToDay());
        }
        if (searchModel.getIdCqkt() > 0L) {
            hql.append(" AND (d.deptId = ? OR d.parentId = ?)");
            listParam.add(searchModel.getIdCqkt());
            listParam.add(searchModel.getIdCqkt());
        }
        if (searchModel.getLstStatus()!= null && searchModel.getLstStatus().size()>0) {
            hql.append(" AND p.status in :lstStatus ");
        }
   

        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        if (searchModel.getLstStatus()!= null && searchModel.getLstStatus().size()>0) {
            query.setParameterList("lstStatus", searchModel.getLstStatus());
        }
        
        List lst = query.list();
        return lst;
    }
    
    
    public List searchProduct(SearchModel searchModel) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT new com.viettel.module.importOrderNew.Controller.exportData.Model.DataModel (count(a.baseUnit),sum(a.baseUnit), sum(a.netweight)) ");
        StringBuilder hql = new StringBuilder();
        hql.append(" FROM ImportOrderProduct a, VFileImportOrder n, Process p, Department d WHERE"
                + " n.fileId = p.objectId AND "
                + " a.fileId = n.fileId AND"
                + " n.fileType = p.objectType AND"
                + " p.sendGroupId = d.deptId");
        if (searchModel.getDbFromDay() != null) {
            hql.append(" AND n.createDate >= ? ");
            listParam.add(searchModel.getDbFromDay());
        }
        if (searchModel.getDbToDay()!= null) {
            hql.append(" AND n.createDate <= ? ");
            listParam.add(searchModel.getDbToDay());
        }
        if (searchModel.getIdCqkt() > 0L) {
            hql.append(" AND (d.deptId = ? OR d.parentId = ?)");
            listParam.add(searchModel.getIdCqkt());
            listParam.add(searchModel.getIdCqkt());
        }
        if (searchModel.getLstStatus()!= null && searchModel.getLstStatus().size()>0) {
            hql.append(" AND p.status in :lstStatus ");
        }
   

        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        if (searchModel.getLstStatus()!= null && searchModel.getLstStatus().size()>0) {
            query.setParameterList("lstStatus", searchModel.getLstStatus());
        }
        
        List lst = query.list();
        return lst;
    }

}
