/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this nation file, choose Tools | Nations
 * and open the nation in the editor.
 */
package com.viettel.module.importOrderNew.Controller.exportData;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.module.importOrderNew.Controller.exportData.DAO.ExportDataManageDAO;
import com.viettel.module.importOrderNew.Controller.exportData.Model.DataModel;
import com.viettel.module.importOrderNew.Controller.exportData.Model.SearchModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.model.exporter.ExcelExporter;
import com.viettel.voffice.model.exporter.Interceptor;
import com.viettel.voffice.model.exporter.RowRenderer;
import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.zkoss.poi.ss.usermodel.Cell;
import org.zkoss.poi.ss.usermodel.CellStyle;
import org.zkoss.poi.ss.usermodel.IndexedColors;
import org.zkoss.poi.ss.usermodel.Row;
import org.zkoss.poi.ss.util.CellRangeAddress;
import org.zkoss.poi.xssf.usermodel.XSSFFont;
import org.zkoss.poi.xssf.usermodel.XSSFSheet;
import org.zkoss.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.util.media.AMedia;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

/**
 *
 * @author linhdx
 */
public class ExportDataManageController extends BaseComposer {

    //Control tim kiem
    @Wire
    Textbox txtName, txtCode;

    @Wire("#dbFromDay")
    private Datebox dbFromDay;
    @Wire("#dbToDay")
    private Datebox dbToDay;
    @Wire
    Listbox lbCqkt, lbData;
    @Wire
    private Listbox lboxStatus;
    @Wire
    Paging userPagingBottom;
    SearchModel searchForm = new SearchModel();

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        Long idParentCqkt = 3850L;
        List<Department> lstDepartment = getAllChildIdByParentIdAddHeader(idParentCqkt);
        ListModelArray lstModelDepartment = new ListModelArray(lstDepartment);
        lbCqkt.setModel(lstModelDepartment);
        lbCqkt.renderAll();

        List<Category> lstStatusAll = new ArrayList();
        Category zero = new Category(-1l);
        zero.setCode("-1");
        zero.setName("--- Chọn ---");
        lstStatusAll.add(0, zero);
        // Da tiep nhan
        Category cat1 = new Category(1l);
        cat1.setValue("1");
        cat1.setName("Đã tiếp nhận");
        lstStatusAll.add(1, cat1);
        Category cat2 = new Category(2l);
        cat2.setValue("2");
        cat2.setName("Đã phê duyệt đơn đăng ký");
        lstStatusAll.add(2, cat2);

        Category cat3 = new Category(3l);
        cat3.setValue("3");
        cat3.setName("Đã cấp phép");
        lstStatusAll.add(3, cat3);

        Category cat4 = new Category(4l);
        cat4.setValue("4");
        cat4.setName("Đã nộp phí");
        lstStatusAll.add(4, cat4);

        ListModelArray lstModelStatus = new ListModelArray(lstStatusAll);
        lboxStatus.setModel(lstModelStatus);
        lboxStatus.renderAll();
        lboxStatus.setSelectedIndex(0);
        lbCqkt.setSelectedIndex(0);
        dbToDay.setValue(new Date());
        onSearch();
    }

    private List<Department> getAllChildIdByParentIdAddHeader(Long idParentCqkt) {
        DepartmentDAOHE deDAO = new DepartmentDAOHE();
        List<Department> lstDepartment = deDAO.getAllChildIdByParentId(idParentCqkt, Constants.Status.ACTIVE);
        Department first = new Department();
        first.setDeptId(Constants.COMBOBOX_HEADER_VALUE);
        first.setDeptName(Constants.COMBOBOX_HEADER_TEXT_SELECT);
        List<Department> lstFull = new ArrayList();
        lstFull.add(first);
        lstFull.addAll(lstDepartment);
        return lstFull;

    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        if (validate()) {
            Clients.showBusy("");
            try {
                userPagingBottom.setActivePage(0);
                fillDataToList();
            } catch (Exception ex) {
                LogUtils.addLogNoImportant(ex);
                showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            } finally {
                Clients.clearBusy();
            }
        }

    }

    private boolean validate() {
        if (dbFromDay != null && dbToDay != null && dbFromDay.getValue() != null && dbToDay.getValue() != null) {
            if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                showNotification(
                        "Thời gian từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                return false;
            }
        }
        return true;
    }

    @Listen("onClick=#btnExport")
    public void onExport() {
        if (!validate()) {
            return;
        }

        List<DataModel> lstModel = searchData();
        final List data = lstModel;
        final ExcelExporter exporter = new ExcelExporter(9);
        exporter.setDataSheetName("Sheet1");
        exporter.setInterceptor(new Interceptor<XSSFWorkbook>() {
            @Override
            public void beforeRendering(XSSFWorkbook book) {
                ExcelExporter.ExportContext context = exporter.getExportContext();
                XSSFSheet sheet = context.getSheet();

                //Font tiêu đề
                XSSFFont fontTitle = book.createFont();
                fontTitle.setFontHeightInPoints((short) 15);
                fontTitle.setFontName("Times New Roman");
                fontTitle.setColor(IndexedColors.BLACK.getIndex());
                fontTitle.setBold(true);
                fontTitle.setItalic(false);

                Row row = exporter.getOrCreateRow(context.moveToNextRow(),
                        sheet);
                Cell cell = exporter.getOrCreateCell(context.moveToNextCell(),
                        sheet);
                cell.setCellValue("Báo cáo số liệu kiểm tra nhà nước về ATTP nhập khẩu");
                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontTitle);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 5));
                //Font cho thời gian
                XSSFFont fontTime = book.createFont();
                fontTime.setFontHeightInPoints((short) 13);
                fontTime.setFontName("Times New Roman");
                fontTime.setColor(IndexedColors.BLACK.getIndex());
                fontTime.setBold(false);
                fontTime.setItalic(true);

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                row = exporter.getOrCreateRow(context.moveToNextRow(), sheet);
                cell = exporter.getOrCreateCell(context.moveToNextCell(), sheet);
                String value = "Thời gian từ ngày %s đến ngày %s";
                String fromDay = "";
                if (dbFromDay.getValue() != null) {
                    fromDay = format.format(dbFromDay.getValue());
                }
                String toDay = "";
                if (dbToDay.getValue() != null) {
                    toDay = format.format(dbToDay.getValue());
                }
                cell.setCellValue(String.format(value,
                        fromDay,
                        toDay));
                cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontTime);
                cell.setCellStyle(cellStyle);
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row
                        .getRowNum(), 0, 5));

                context.setRowHeaderIndex(2);

                //Row heading
                exporter.getOrCreateRow(context.moveToNextRow(), sheet);
                //Font cho header
                XSSFFont fontHeader = book.createFont();
                fontHeader.setFontHeightInPoints((short) 12);
                fontHeader.setFontName("Times New Roman");
                fontHeader.setColor(IndexedColors.BLACK.getIndex());
                fontHeader.setBold(true);
                fontHeader.setItalic(false);
                //Font cho nội dung
                XSSFFont fontContent = book.createFont();
                fontContent.setFontHeightInPoints((short) 11);
                fontContent.setFontName("Times New Roman");
                fontContent.setColor(IndexedColors.BLACK.getIndex());
                fontContent.setBold(false);
                fontContent.setItalic(false);
                //CellStyle cho headers
                cellStyle = sheet.getWorkbook().createCellStyle();
                cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellStyle.setFont(fontHeader);

                String[] headerNames = {"STT", "Tổng số hồ sơ", "Tổng số lô hàng",
                    "Tổng khối lượng (Kg)",
                    "Tổng giá trị(VNĐ)"};
                for (String headerName : headerNames) {
                    cell = exporter.getOrCreateCell(context.moveToNextCell(), context.getSheet());
                    cell.setCellValue(headerName);
                    cell.setCellStyle(cellStyle);
                }
                //Set CellStyle cho các cột
                CellStyle csCenter = sheet.getWorkbook().createCellStyle();
                csCenter.setAlignment(CellStyle.ALIGN_CENTER);
                csCenter.setFont(fontContent);
                exporter.setCellstyle(0, csCenter);

                CellStyle csLeft = sheet.getWorkbook().createCellStyle();
                csLeft.setAlignment(CellStyle.ALIGN_LEFT);
                csLeft.setFont(fontContent);
                exporter.setCellstyle(1, csLeft);
                exporter.setCellstyle(2, csLeft);
                exporter.setCellstyle(3, csLeft);
                exporter.setCellstyle(4, csLeft);

            }

            @Override
            public void afterRendering(XSSFWorkbook book) {
                XSSFFont fontFooter = book.createFont();
                fontFooter.setFontHeightInPoints((short) 11);
                fontFooter.setFontName("Times New Roman");
                fontFooter.setColor(IndexedColors.BLACK.getIndex());
                fontFooter.setBold(true);
                fontFooter.setItalic(false);
            }
        });

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            exporter.export(9, data,
                    new RowRenderer<Row, DataModel>() {

                        @Override
                        public void render(Row row,
                                DataModel dataModel, boolean oddRow) {
                            final ExcelExporter.ExportContext context = exporter
                            .getExportContext();
                            final XSSFSheet sheet = context.getSheet();

                            Cell cell = exporter.getOrCreateCell(
                                    context.moveToNextCell(), sheet);
                            cell.setCellValue(row.getRowNum()
                                    - context.getRowHeaderIndex());
                            cell.setCellStyle(exporter.getCellstyle(0));

                            cell = exporter.getOrCreateCell(context.moveToNextCell(), sheet);
                            cell.setCellValue(dataModel.getTotalFileStr());
                            cell.setCellStyle(exporter.getCellstyle(1));

                            cell = exporter.getOrCreateCell(context.moveToNextCell(), sheet);
                            cell.setCellValue(dataModel.getCountProductStr());
                            cell.setCellStyle(exporter.getCellstyle(2));

                            cell = exporter.getOrCreateCell(context.moveToNextCell(), sheet);
                            cell.setCellValue(dataModel.getNetweightStr());
                            cell.setCellStyle(exporter.getCellstyle(3));

                            cell = exporter.getOrCreateCell(context.moveToNextCell(), sheet);
                            cell.setCellValue(dataModel.getBaseUnitStr());
                            cell.setCellStyle(exporter.getCellstyle(4));

                        }

                    }, out);
            Calendar now = Calendar.getInstance();
            int day = now.get(Calendar.DAY_OF_MONTH);
            int month = now.get(Calendar.MONTH);
            int year = now.get(Calendar.YEAR);
            String name = "Bao cao-" + day + "-" + (month + 1) + "-" + year
                    + ".xlsx";
            AMedia amedia = new AMedia(name, "xls", "application/file",
                    out.toByteArray());
            Filedownload.save(amedia);

        } catch (IOException e) {
            LogUtils.addLogDB(e);
        }

    }

    private void fillDataToList() {
        List<DataModel> lstDataModel = searchData();
        ListModelArray lstModel = new ListModelArray(lstDataModel);
        lbData.setModel(lstModel);

    }

    private List<DataModel> searchData() {
        searchForm.setDbFromDay(dbFromDay == null ? null : dbFromDay.getValue());
        searchForm.setDbToDay(dbToDay == null ? null : dbToDay.getValue());
        if (lbCqkt.getSelectedItem() != null) {
            Long idCqkt = (Long) lbCqkt.getSelectedItem().getValue();
            searchForm.setIdCqkt(idCqkt);
        }
        List<DataModel> lstDataModel = new ArrayList();
        String tonghoso = "Tổng số hồ sơ đăng ký kiểm tra:";
        String tonglohang = "Tổng số lô hàng đăng ký kiểm tra:";
        String tongkhoiluong = "Tổng khối lượng:";
        String tonggiatri = "Tổng giá trị hàng hóa:";
        DataModel dataModel1 = tongsodangky(tonghoso, tonglohang, tongkhoiluong, tonggiatri);
        lstDataModel.add(dataModel1);
        return lstDataModel;

    }

    private DataModel tongsodangky(String tonghoso, String tonglohang, String tongkhoiluong, String tonggiatri) {
        ExportDataManageDAO dao = new ExportDataManageDAO();
        List<Long> lstStatus = new ArrayList();
        if ("-1".equals(lboxStatus.getSelectedItem().getValue()) || "1".equals(lboxStatus.getSelectedItem().getValue())) {
            lstStatus.add(Constants.ExportData.statusLD_CQKT_phancong);
            //lstStatus.add(Constants.ExportData.statusLDC_PhancongCQKT);
        } else if ("2".equals(lboxStatus.getSelectedItem().getValue())) {
//            lstStatus.add(Constants.ExportData.status_Pheduyetdondk);
            lstStatus.add(Constants.ExportData.status_Pheduyetdondk_moi);
        } else if ("3".equals(lboxStatus.getSelectedItem().getValue())) {
//            lstStatus.add(Constants.ExportData.status_Banhanhhosogiam);
//            lstStatus.add(Constants.ExportData.status_Banhanhhosothuong);
            lstStatus.add(Constants.ExportData.status_Banhanhhosogiam_moi);
            lstStatus.add(Constants.ExportData.status_Banhanhhosothuong_moi);
        } else if ("4".equals(lboxStatus.getSelectedItem().getValue())) {
            lstStatus.add(Constants.ExportData.status_Xacnhanphigiam_moi);
            lstStatus.add(Constants.ExportData.status_Xacnhanphithuong_moi);
        } else {
            //lstStatus.add(Constants.ExportData.statusDN_GuiCQKT);
            lstStatus.add(Constants.ExportData.statusLD_CQKT_phancong);
        }

        searchForm.setLstStatus(lstStatus);
        List lstTotalFile = dao.searchFile(searchForm);
        Long totalFile = (long) lstTotalFile.size();
        List lstTotalNetweightAndBaseUnit = dao.searchProduct(searchForm);
        DataModel dataModel = (DataModel) lstTotalNetweightAndBaseUnit.get(0);
        DecimalFormat format = new DecimalFormat("###,###,###,###,###,###,###.###");
        dataModel.setTotalFile(totalFile);
        dataModel.setTotalFileStr(tonghoso + format.format(totalFile));
        String countProductStr;
        if (dataModel.getCountProduct() != null) {
            countProductStr = tonglohang + format.format(dataModel.getCountProduct());
        } else {
            countProductStr = tonglohang + "0";
        }
        dataModel.setCountProductStr(countProductStr);
        String netweight;
        if (dataModel.getNetweight() != null) {
            netweight = tongkhoiluong + format.format(dataModel.getNetweight());
        } else {
            netweight = tongkhoiluong + "0";
        }
        dataModel.setNetweightStr(netweight);
        String baseUnit;
        if (dataModel.getBaseUnit() != null) {
            baseUnit = tonggiatri + format.format(dataModel.getBaseUnit());
        } else {
            baseUnit = tonggiatri + "0";
        }
        dataModel.setBaseUnitStr(baseUnit);
        return dataModel;
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }

}
