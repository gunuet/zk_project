/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrderNew.Controller.exportData.Model;

/**
 *
 * @author Linhdx
 */
public class DataModel {

    private Long countProduct;
    private Long baseUnit;
    private Double netweight;
    private Long totalFile;

    private String countProductStr;
    private String totalFileStr;
    private String baseUnitStr;
    private String netweightStr;

    public DataModel(Long countProduct, Long baseUnit, Double netweight) {
        this.countProduct = countProduct;
        this.baseUnit = baseUnit;
        this.netweight = netweight;
    }

    public Long getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(Long baseUnit) {
        this.baseUnit = baseUnit;
    }

    public Double getNetweight() {
        return netweight;
    }

    public void setNetweight(Double netweight) {
        this.netweight = netweight;
    }

    public Long getTotalFile() {
        return totalFile;
    }

    public void setTotalFile(Long totalFile) {
        this.totalFile = totalFile;
    }

    public String getTotalFileStr() {
        return totalFileStr;
    }

    public void setTotalFileStr(String totalFileStr) {
        this.totalFileStr = totalFileStr;
    }

    public String getBaseUnitStr() {
        return baseUnitStr;
    }

    public void setBaseUnitStr(String baseUnitStr) {
        this.baseUnitStr = baseUnitStr;
    }

    public String getNetweightStr() {
        return netweightStr;
    }

    public void setNetweightStr(String netweightStr) {
        this.netweightStr = netweightStr;
    }

    public Long getCountProduct() {
        return countProduct;
    }

    public void setCountProduct(Long countProduct) {
        this.countProduct = countProduct;
    }

    public String getCountProductStr() {
        return countProductStr;
    }

    public void setCountProductStr(String countProductStr) {
        this.countProductStr = countProductStr;
    }

}
