/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importOrderNew.Controller.exportData.Model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author ChucHV
 */
public class SearchModel {

    protected Long idCqkt;
    protected Date dbFromDay;
    protected Date dbToDay;
    List<Long> lstStatus;

    public Long getIdCqkt() {
        return idCqkt;
    }

    public void setIdCqkt(Long idCqkt) {
        this.idCqkt = idCqkt;
    }

    public Date getDbFromDay() {
        return dbFromDay;
    }

    public void setDbFromDay(Date dbFromDay) {
        this.dbFromDay = dbFromDay;
    }

    public Date getDbToDay() {
        return dbToDay;
    }

    public void setDbToDay(Date dbToDay) {
        this.dbToDay = dbToDay;
    }

    public List<Long> getLstStatus() {
        return lstStatus;
    }

    public void setLstStatus(List<Long> lstStatus) {
        this.lstStatus = lstStatus;
    }

}
