package com.viettel.module.importOrderNew.Controller.includes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.convert.service.PdfDocxFile;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.BO.Port;
import com.viettel.core.sys.DAO.PortDAOHE;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importOrder.BO.ImportOrderFile;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.Controller.QrCodeController;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.Model.DepartmentLeadershipImportOrderModel;
import com.viettel.module.importOrder.Model.ImportOrderProductModel;
import com.viettel.module.importOrderNew.Controller.Evaluation.LDCqktCheckAttpController;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
// com.viettel.signature.plugin.SignPdfFile;
//import com.viettel.signature.utils.CertUtils;
import com.viettel.newsignature.plugin.SignPdfFile;
import com.viettel.newsignature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.utils.WordExportUtils;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import java.text.DecimalFormat;

/**
 *
 * @author Linhdx
 */
public class DepartmentLeadershipImportOrderController extends
        BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate, txtMessage;
    @Wire
    private Listbox finalFileListbox, lbOrderProduct;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    @Wire
    private Textbox txtMainContent;
    @Wire
    private Label lbNote;
    private UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
            "userToken");
    // private Permit permit;
    private List listBook;
    private Long docType;
    private String bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_PERMIT_XNK;
    private String GiayDangKyKiemTraThucPhamNhapKhauGiam = "/WEB-INF/template/PheDuyetHoSo_CQKT.docx";

    /**
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        List<ImportOrderProduct> importOrderProducts = new ImportOrderProductDAO()
                .findAllProducByFileIdAndCheckMeThod(fileId);
        ListModelArray lstModelManufacturer = new ListModelArray(
                importOrderProducts);
        lbOrderProduct.setModel(lstModelManufacturer);

        // load all of signed file
        fillFinalFileListbox(fileId);
        txtValidate.setValue("0");
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private boolean isValidatedData() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
        if (lstAttach.isEmpty()) {
            showNotification("Chưa ký văn bản");
            setWarningMessage("Chưa ký văn bản");
            return false;
        }
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }

            updatPassForReducedProduct();

            createPayment();
            updateAttachFileSigned();
            txtValidate.setValue("1");

        } catch (WrongValueException e) {
            LogUtils.addLogDB(e);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    /**
     *
     * @param event
     * @throws Exception
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) throws Exception {
        try {
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String folderSignOut = resourceBundle.getString("signPdf");
            if (!new File(folderSignOut).exists()) {
                FileUtil.mkdirs(folderSignOut);
            }
            String fileSignOut = folderSignOut
                    + "_signed_LD_Giaythongbaoketquakiemtragiam"
                    + (new Date()).getTime() + ".pdf";

            Session session = Sessions.getCurrent();
            SignPdfFile signPdfFile = (SignPdfFile) session
                    .getAttribute("PDFSignature");
            String outputFileFinal = session
                    .getAttribute("outputFileFinal").toString();
            String signature = event.getData().toString();
            signPdfFile.insertSignature(signature, outputFileFinal, fileSignOut);

            attachSave(fileSignOut);

            showNotification("Ký số thành công!", Constants.Notification.INFO);
            // refresh page
            fillFinalFileListbox(fileId);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công!", Constants.Notification.ERROR);
        }
    }

    /**
     * save file path into Attach table
     *
     * @throws Exception
     */
    public void attachSave(String filePath) throws Exception {

        AttachDAO attachDAO = new AttachDAO();
        attachDAO.saveFileAttachPdfSign(filePath, fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
    }

    public void updateAttachFileSigned() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
        if (lstAttach != null && lstAttach.size() > 0) {
            for (Attachs att : lstAttach) {
                Attachs att_new = att;
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att_new);
                }
            }
        }

    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     * @throws Exception
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        clearWarningMessage();
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        // get fdf file path that is insert data
        String tempPdfPath = createTempPdfFile();
        if ("-1".equals(tempPdfPath)) {
            showNotification("Không có sản phẩm kiểm tra giảm!");
            return;
        }
        // sign pdf file (insert img)
        actionSignCA(event, tempPdfPath);
    }

    public String createTempPdfFile() throws Exception {

        // lay cong van
        Permit permit = this.getPermit(fileId);
        // vao so
        Long bookNumber = putInBook(permit.getPermitId());
        String receiveNo = getPermitReceiveNo(bookNumber);

        // update so vao so vao permit
        PermitDAO permitDAO = new PermitDAO();
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        // set infor for model to replay at template file
        ImportOrderFileDAO deviceDAO = new ImportOrderFileDAO();
        ImportOrderFile importOrderFile = deviceDAO.findByFileId(fileId);
        // not display
        importOrderFile.setCheckTime(null);
        importOrderFile.setCheckPlace(null);
        importOrderFile.setDeptName(null);
        importOrderFile.setCheckDeptName(null);

        // get chu doanh nghiep
        Files files = new FilesDAOHE().findById(fileId);

        // set current date
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int days = cal.get(Calendar.DAY_OF_MONTH);
        int months = cal.get(Calendar.MONTH) + 1;
        int years = cal.get(Calendar.YEAR);
        String signedDate = "Hà Nội, ngày " + days + " tháng " + months
                + " năm " + years;
        DecimalFormat format = new DecimalFormat("###,###,###,###,###.###");
        ImportOrderProductDAO proDAO = new ImportOrderProductDAO();
        List<ImportOrderProduct> productList = proDAO
                .findAllIdByFileIdAndCheckMethodCode(fileId, "GIAM");
        if (productList == null || productList.isEmpty()) {

            return "-1";// khong ky duoc
        }
        List<ImportOrderProductModel> productModelList = new ArrayList<ImportOrderProductModel>();
        ImportOrderProductModel importOrderProductModel;
        Long pass;
        for (Iterator iterator = productList.iterator(); iterator.hasNext();) {
            importOrderProductModel = new ImportOrderProductModel();
            ImportOrderProduct importOrderProduct = (ImportOrderProduct) iterator
                    .next();
            importOrderProductModel.setProductName(importOrderProduct
                    .getProductName());
            importOrderProductModel.setConfirmAnnounceNo(importOrderProduct
                    .getConfirmAnnounceNo());
            importOrderProductModel.setNationalName(importOrderProduct
                    .getNationalName() + (importOrderProduct.getManufacTurer() == null ? "" : ", " + importOrderProduct.getManufacTurer()) + (importOrderProduct.getManufacturerAddress() == null ? "" : ", " + importOrderProduct.getManufacturerAddress()));
            importOrderProductModel.setTotal(importOrderProduct.getTotal());
            importOrderProductModel.setNetweight(importOrderProduct
                    .getNetweight());
            importOrderProductModel.setCheckMethodName(importOrderProduct
                    .getCheckMethodName());
            importOrderProductModel.setV_baseUnit(format.format(importOrderProduct.getBaseUnit()) + " VNĐ");
            importOrderProductModel.setV_netweight(format.format(importOrderProduct.getNetweight()) + (importOrderProduct.getNetweightUnitName() == null ? "" : " " + importOrderProduct.getNetweightUnitName()));
            importOrderProductModel.setV_total(format.format(importOrderProduct.getTotal()) + (importOrderProduct.getTotalUnitName() == null ? "" : " " + importOrderProduct.getTotalUnitName()));

            // pass = importOrderProduct.getPass();
            // if (pass != null && pass == 1) {
            importOrderProductModel.setPass("Đạt");
            // } else {
            // importOrderProductModel.setNotPass("Không đạt");
            // }

            importOrderProductModel.setReason(importOrderProduct.getReason());
            productModelList.add(importOrderProductModel);
        }

        DepartmentLeadershipImportOrderModel model = new DepartmentLeadershipImportOrderModel();
        model.setSendNo(permit.getReceiveNo());
        model.setBusinessName(files.getBusinessName());
        model.setBusinessAddress(files.getBusinessAddress());
        model.setBusinessPhone(files.getBusinessPhone());
        model.setBusinessFax(files.getBusinessFax());
        model.setBusinessEmail(files.getBusinessEmail());
        model.setSignedDate(signedDate);
        model.setLeaderSigned(getUserFullName());
        if (!StringUtils.validString(importOrderFile.getImporterGateName()) && StringUtils.validString(importOrderFile.getImporterGateCode())) {
            PortDAOHE pDAO = new PortDAOHE();
            Port p = pDAO.findByCode(importOrderFile.getImporterGateCode());
            importOrderFile.setImporterGateName(p.getName());
        }
        model.setImportOrderFile(importOrderFile);
        model.setProductList(productModelList);
        QrCodeController qrCodeController = new QrCodeController();
        byte[] qrcode = qrCodeController.createQrCodeOnSigning(fileId, receiveNo, new Date());
        String tempPdfFile = this.exportTempPdfFile(model, qrcode);
        qrcode = null;
        return tempPdfFile;
    }

    private Permit getPermit(Long fileId) {
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileIdAndType(
                fileId, Constants.PERMIT_TYPE.REDUCED_PAPER);
        Permit permit;

        // Da co cong van thi lay ban cu
        if (lstPermit.size() > 0) {
            permit = lstPermit.get(0);
            // Neu khong co thi tao ban moi
        } else {
            permit = new Permit();
            permit.setFileId(fileId);
            permit.setType(Constants.PERMIT_TYPE.REDUCED_PAPER);
            permit.setIsActive(Constants.Status.ACTIVE);
            permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
            permit.setSignDate(new Date());
            permit.setReceiveDate(new Date());
            permitDAO.saveOrUpdate(permit);
        }

        return permit;
    }

    public String exportTempPdfFile(DepartmentLeadershipImportOrderModel model, byte[] qrcode) {
        String tempPdfFilePath = null;
        try {

            HttpServletRequest request = (HttpServletRequest) Executions
                    .getCurrent().getNativeRequest();
            String docPath = request
                    .getRealPath(GiayDangKyKiemTraThucPhamNhapKhauGiam);

            // Neu co dinh nghia bieu mau thi lay tu dinh nghia
            if (model.getPathTemplate() != null) {
                docPath = model.getPathTemplate();
            }

            // Date signedDate = model.getSignedDate();
            // String signDateStr = "";
            // if (signedDate != null) {
            // Calendar cal = Calendar.getInstance();
            // cal.setTime(signedDate);
            // int days = cal.get(Calendar.DAY_OF_MONTH);
            // int months = cal.get(Calendar.MONTH) + 1;
            // int years = cal.get(Calendar.YEAR);
            // signDateStr = "Hà Nội, ngày " + days + " tháng " + months
            // + " năm " + years;
            // } else {
            // int days = signedDate.getDay();
            // int months = signedDate.getMonth();
            // int years = signedDate.getYear();
            // signDateStr = "Hà Nội, ngày " + days + " tháng " + months
            // + " năm " + years;
            // }

            /* replace template doc by data of model */
            WordprocessingMLPackage wmp = WordprocessingMLPackage
                    .load(new FileInputStream(docPath));

            ConcurrentHashMap map = new ConcurrentHashMap();
            map.put("createForm", model);

            WordExportUtils word = new WordExportUtils();
            // word.replacePlaceholder(wmp, signDateStr, "${signDateStr}");
            word.replacePlaceholder(wmp, map);

            word.replaceTable(wmp, 1, model.getProductList());

            // convert word to PDF
//            PdfDocxFile pdfDocxFile = word.writePDFToStream(wmp, false);//binhnt
            PdfDocxFile pdfDocxFile = word.writePDFQrCodeToStream(wmp, qrcode, false);
            // write pdf to file
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String tempFolder = resourceBundle.getString("signTemp");
            if (!new File(tempFolder).exists()) {
                FileUtil.mkdirs(tempFolder);
            }

            tempPdfFilePath = tempFolder
                    + "Giaythongbaoketquakiemtragiam_1"
                    + (new Date()).getTime() + ".pdf";
            OutputStream outputStream = new FileOutputStream(tempPdfFilePath);
            outputStream.write(pdfDocxFile.getContent());
            outputStream.flush();
            outputStream.close();
        } catch (Exception en) {
            LogUtils.addLogDB(en);
        }
        return tempPdfFilePath;
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Long permitId) {
        ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
        ImportOrderFile iof = imDAO.findByFileId(fileId);
        if (iof.getPermitNo() != null) {
            return iof.getPermitNo();
        }
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(permitId,
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        Long num = bookDocument.getBookNumber();
        iof.setPermitNo(num);
        imDAO.saveOrUpdate(iof);
        return num;
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param documentId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long documentId, Long bookId) {

        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);

        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(documentId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);

        // Cap nhat so hien tai trong bang book
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);

        return bookDocument;
    }

    private String getPermitReceiveNo(Long bookNumber) {
        String permitReceiveNo = "";

        if (bookNumber != null) {
            permitReceiveNo = String.valueOf(bookNumber);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy");
        String year = simpleDateFormat.format(Calendar.getInstance().getTime());
        permitReceiveNo += "/" + year + "/ATTP";

        return permitReceiveNo;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        // FileUtil.mkdirs(filePath);
        // String outputFileFinalName =
        String certSerial = x509Cert.getSerialNumber().toString(16);
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caUserList = ca
                .findCaBySerial(certSerial, 1L, getUserId());
        if (caUserList != null && caUserList.size() > 0) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String linkImageSign = folderPath + separator
                    + caUserList.get(0).getSignature();
//			String linkImageStamp = folderPath + separator
//					+ caUserList.get(0).getStamper();
            Pdf pdf = new Pdf();
            String fieldName = tk.getUserFullName();
            ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
            String outputFileFinal = resourceBundle.getString("signTemp") + "_"
                    + (new Date()).getTime() + ".pdf";
            SignPdfFile pdfSig = pdf.createSignHash(fileToSign, outputFileFinal, new Certificate[]{x509Cert}, linkImageSign, fieldName);
//			// chen chu ki
//			pdf.insertImageAll(fileToSign, outputFileFinal, linkImageSign,
//					linkImageStamp, null);
//			// chen CKS
//			String base64Hash = pdfSig.createHash(outputFileFinal,
//					new Certificate[] { x509Cert });
            if (pdfSig == null) {
                showNotification("Ký số không thành công!");
                LogUtils.addLogDB("Exception " + "Ký số không thành công" + new Date());
                return;
            }
            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", pdf.getBase64Hash());
            session.setAttribute("outputFileFinal", outputFileFinal);
            txtBase64HASH.setValue(pdf.getBase64Hash());
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");

        } else {
            showNotification("Ký số không thành công!");
        }
    }

    // load all of singed pdf file
    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe
                .getByObjectIdAndAttachCatAndAttachType(fileId,
                        Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                        Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    // private Permit createPermit() throws Exception {
    // permit.setFileId(fileId);
    // permit.setIsActive(Constants.Status.ACTIVE);
    // permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
    // permit.setSignDate(new Date());
    // permit.setReceiveDate(new Date());
    // return permit;
    // }
    // download signed pdf
    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs attachs = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(attachs);
    }

    // delete signed pdf
    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    private void setWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    private void createPayment() {
        Long phase = 1l;
        PaymentInfoDAO paymentInfoDAOHE = new PaymentInfoDAO();
        paymentInfoDAOHE.deletePayment(fileId, phase);
        ImportOrderProductDAO importOrderProductDAO = new ImportOrderProductDAO();

        List<ImportOrderProduct> lsGiam = new ArrayList<>();
        lsGiam.addAll(importOrderProductDAO.findAllIdByFileIdAndCheckMethodCode(fileId, "GIAM"));
        long feeId3 = 7451;
        LDCqktCheckAttpController controller = new LDCqktCheckAttpController();
        long tongktgiam = controller.tongtienkiemtra(lsGiam);
        Department depart_obj = null;
        ImportOrderFileDAO fileDAO = new ImportOrderFileDAO();
        ImportOrderFile importOrderFile = fileDAO.findByFileId(fileId);
        importOrderFile.setCheckDeptId(getDeptId());
        importOrderFile.setCheckDeptName(getDeptName());
        fileDAO.saveOrUpdate(importOrderFile);

        DepartmentDAOHE departmentDAOHE = new DepartmentDAOHE();
        if (importOrderFile.getCheckDeptId() != null) {
            depart_obj = departmentDAOHE.findBOById(importOrderFile.getCheckDeptId());

        }
        controller.savePaymentInfo(depart_obj, feeId3, fileId, tongktgiam, phase);

        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_IMPORT_ODDER.SENMS_11_12_13_14);
        md.setPhase(phase);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

    private void updatPassForReducedProduct() {
        ImportOrderProductDAO proDAO = new ImportOrderProductDAO();
        List<ImportOrderProduct> productList = proDAO
                .findAllIdByFileIdAndCheckMethodCode(fileId, "GIAM");
        Long pass;
        for (Iterator iterator = productList.iterator(); iterator.hasNext();) {
            ImportOrderProduct importOrderProduct = (ImportOrderProduct) iterator
                    .next();
            importOrderProduct.setPass(new Long(1));
            proDAO.saveOrUpdate(importOrderProduct);
        }
    }

}
