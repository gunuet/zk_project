package com.viettel.module.import_food.controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.import_food.service.ActionPaymentService;
import com.viettel.module.import_food.service.FileService;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.utils.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;

/**
 *
 * @author
 */
public class FileActionPaymentController extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    private Window importFoodActionPayment;
    
    @Wire
    private Listbox lbActionType;
    
    @Wire
    private Textbox tbContent;

    private IfdFile file;
    
    private ActionPaymentService actionPaymentService;
    private FileService fileService;
    
    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            System.out.println("init importFoodActionPayment");
            Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
            //parentWindow = (Window) arguments.get("windowParent");
            Long fileId = (Long) arguments.get("fileId");
            //docType = (Long) arguments.get("docType");
            System.out.println("fileId::" + fileId);
            //fileId = Long.parseLong("1");
            fileService = new FileService();
            file = fileService.get(fileId);
            
            System.out.println("file::" + file);
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        System.out.println("save..." + lbActionType.getSelectedItem().getValue());
        System.out.println("save..." + tbContent.getText());
        try {
            
        String content = tbContent.getText();
        
        System.out.println("file::" + file);
        if(file == null || file.getFileId() < 1){
            System.out.println("file-not-found");
        }
        actionPaymentService = new ActionPaymentService();
        actionPaymentService.reject(getUserId(), file.getFileId(), file.getFileCode(), content);
        importFoodActionPayment.detach();
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.addLogNoImportant(e);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }
    
    @Listen("onClick=#btnClose")
    public void detach(){
        importFoodActionPayment.detach();
    }
    
}
