/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.import_food.controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.import_food.service.ActionPaymentService;
import com.viettel.module.import_food.service.FileProductService;
import com.viettel.module.import_food.service.FileService;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Admin
 */
public class FileApproveController  extends BaseComposer {
    
    private static final long serialVersionUID = 1L;

    @Wire
    private Window importFoodActionPayment;
    
    @Wire
    private Listbox lbActionType;
    
    @Wire
    private Textbox tbContent;

    private IfdFile file;
    
    private ActionPaymentService actionPaymentService;
    private FileProductService productService;
    
    @Wire("#incList #lbList")
    Listbox lbList;
    
    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
            //parentWindow = (Window) arguments.get("windowParent");
            Long fileId = (Long) arguments.get("fileId");
            //docType = (Long) arguments.get("docType");
            System.out.println("fileId::" + fileId);
            //fileId = Long.parseLong("1");
            productService = new FileProductService();
            
            List<IfdFileProduct> list = productService.getProductByFileId(fileId);
                        ListModelArray lstModel = new ListModelArray(list);
            lbList.setModel(lstModel);

            System.out.println(list);
            System.out.println("file::" + file);
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        System.out.println("save..." + lbActionType.getSelectedItem().getValue());
        System.out.println("save..." + tbContent.getText());
        try {
            
        String content = tbContent.getText();
        
        System.out.println("file::" + file);
        if(file == null || file.getFileId() < 1){
            System.out.println("file-not-found");
        }
        actionPaymentService = new ActionPaymentService();
        actionPaymentService.reject(getUserId(), file.getFileId(), file.getFileCode(), content);
        importFoodActionPayment.detach();
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.addLogNoImportant(e);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }
    
    @Listen("onClick=#btnClose")
    public void detach(){
        importFoodActionPayment.detach();
    }
    

}
