package com.viettel.module.import_food.controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.import_food.service.FileService;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.utils.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author
 */
public class ImportFoodController extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire("#incSearchForm #gbxSearchForm")
    private Groupbox gbxSearchForm;

    @Wire
    private Window wdImportFood;
    
    @Wire("#incList #lbList")
    Listbox lbList;

    @Wire("#incSearchForm #gbxSearchForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;

    @Wire("#incSearchForm #gbxSearchForm #lboxStatus")
    private Listbox lboxStatus;

    @Wire("#incSearchForm #gbxSearchForm #dbFromDay")
    private Datebox dbFromDay;

    @Wire("#incSearchForm #gbxSearchForm #dbToDay")
    private Datebox dbToDay;

    @Wire("#incSearchForm #gbxSearchForm #tbTenDoanhNghiep")
    private Textbox tbTenDoanhNghiep;
    
    @Wire
    private Paging pagingTop, pagingBottom;
    
    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            gbxSearchForm.setVisible(true);
            onSearch();
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Listen("onClick=#wdImportFood #incSearchForm #btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {
            int take = pagingBottom.getPageSize();
            int start = pagingBottom.getActivePage()
                    * pagingBottom.getPageSize();
           
            System.out.println("searching......." + tbNSWFileCode);
            System.out.println(tbNSWFileCode.getValue());
            FileService fileService = new FileService();
            int stt = 0;
            
            List<IfdFile> resultList = fileService.filter(tbNSWFileCode.getValue(), stt,
                    tbTenDoanhNghiep.getValue(), dbFromDay.getValue(), dbToDay.getValue(), take, start);
            
            System.out.println("size::" + resultList.size());
            
            Long count = fileService.count(tbNSWFileCode.getValue(), stt,
                    tbTenDoanhNghiep.getValue(), dbFromDay.getValue(), dbToDay.getValue());
            
            ListModelArray lstModel = new ListModelArray(resultList);
            lbList.setModel(lstModel);
            
            pagingBottom.setTotalSize(Math.toIntExact(count));
            pagingTop.setTotalSize(Math.toIntExact(count));
            if (count == 0) {
                pagingBottom.setVisible(false);
                pagingTop.setVisible(false);
            } else {
                pagingBottom.setVisible(true);
                pagingTop.setVisible(true);
            }

        } catch (WrongValueException | NumberFormatException ex) {
            LogUtils.addLogNoImportant(ex);
            ex.printStackTrace();
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    @Listen("onPaging=#pagingTop, #pagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (pagingTop.equals(pe.getTarget())) {
            pagingBottom.setActivePage(pagingTop.getActivePage());
        } else {
            pagingTop.setActivePage(pagingBottom.getActivePage());
        }
        onSearch();
    }

    @Listen("onShowFullSearch=#importFood")
    public void onShowFullSearch() {
        System.out.println("show form...");
        if (gbxSearchForm.isVisible()) {
            gbxSearchForm.setVisible(false);
        } else {
            gbxSearchForm.setVisible(true);
        }
    }
    
    @Listen("onOpenView=#incList #lbList")
    public void onOpenView(Event event) {
        System.out.println("open show view...");
        IfdFile obj = (IfdFile) event.getData();
        System.out.println("fileId..." + obj.getFileId());
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("fileId", obj.getFileId());
        arguments.put("parentWindow", wdImportFood);
        
        System.out.println("create windown...");
        createWindow("windowView",
                "/Pages/module/import_food/detail.zul", arguments,
                Window.EMBEDDED);
        wdImportFood.setVisible(false);
    }
}
