package com.viettel.module.import_food.controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosAdditionalRequest;
import com.viettel.module.cosmetic.BO.CosAssembler;
import com.viettel.module.cosmetic.BO.CosCosfileIngre;
import com.viettel.module.cosmetic.BO.CosEvaluationRecord;
import com.viettel.module.cosmetic.BO.CosPermit;
import com.viettel.module.cosmetic.BO.CosProductTypeSub;
import com.viettel.module.cosmetic.BO.CosReject;
import com.viettel.module.cosmetic.Controller.CosmeticBaseController;
import com.viettel.module.cosmetic.DAO.AnnexeDAO;
import com.viettel.module.cosmetic.DAO.CosAdditionalRequestDAO;
import com.viettel.module.cosmetic.DAO.CosEvaluationRecordDAO;
import com.viettel.module.cosmetic.DAO.CosPermitDAO;
import com.viettel.module.cosmetic.DAO.CosRejectDAO;
import com.viettel.module.cosmetic.Model.CosProductTypeSubModel;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.importOrder.BO.VAttfileCategory;
import com.viettel.module.importOrder.BO.VFileImportOrder;
import com.viettel.module.importOrder.BO.VimportOrderFileAttach;
import com.viettel.module.importOrder.DAO.ExportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importOrder.DAO.VAttfileCategoryDAO;
import com.viettel.module.importOrder.Model.ExportOrderFileModel;
import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.module.importfood.DAO.IfdFileAttachDAO;
import com.viettel.module.importfood.DAO.IfdFileDAO;
import com.viettel.module.importfood.DAO.IfdFilePaymentDAO;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.utils.*;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.bind.JAXBException;
import org.zkoss.zhtml.Br;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

/**
 *
 * @author
 */
public class ImportFoodDetailController extends CosmeticBaseController {

    private static final long serialVersionUID = 1L;
    
    @Wire
    private Window importFoodDetail;
 
    private static long FILE_TYPE = Constants.FILE_TYPE_DYCNK;
    private final int RAPID_TEST_FILE_TYPE = 1;
    private long DEPT_ID;
    // private String nswFileCode;
    @Wire
    private Div divToolbarBottom;
    private List<Component> listTopActionComp;
    @Wire
    private Div divToolbarTop;
    private List<Component> listBottomActionComp;
    private List listCosmeticFileType;
    @Wire
    private Window windowView;
    private Window parentWindow;
    private String isSave;
    private ListModel model;
    private IfdFile files;
    Long documentTypeCode;
    @Wire
    private Listbox finalFileListboxProKTKN, finalFileListboxProregister,
            finalFileListboxProductgiam, finalFileListboxProSDBS;
    //linhdx them cac ho so kiem tra
    @Wire
    private Listbox fileListboxOrderReturn, fileListboxOrderFile;
    private String fileListboxDuplicateTransportNoFlag = "0";
    @Wire
    private Listbox lFillFee;
    @Wire
    Checkbox cbAssemblerMain, cbAssemblerSub, cbIngreConfirm1, cbIngreConfirm2;
    @Wire
    private Listbox lbProductPresentation, lbProductType;
    @Wire("#incListEvaluation #lbListEvaluation")
    private Listbox lbListEvaluation;
    @Wire("#incList #txtSubstance")
    Textbox txtSubstance;
    private List ingredientListCheck, lstAnnexe;
    // private VFileCosfile vFileCosfile;
    private com.viettel.core.workflow.BO.Process processCurrent;// process dang
    // duoc xu li
    private Integer menuType;
    private Long fileId;
    @Wire("#incList #userPagingBottom")
    private Paging userPagingBottom;
    List<NodeToNode> lstNextAction;

    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        fileId = (Long)arguments.get("fileId");
        files = (new IfdFileDAO()).findById(fileId);
        //FILE_TYPE = files.getFileType();

        DEPT_ID = Constants.CUC_ATTP_XNN_ID;
        //linhdx add 20160316 thay doi quy trinh moi
        try {
            String date_ud = ResourceBundleUtil.getString("ATTP_CHANGE_QT_time", "config");
            Date d_ud = formatterDateTime.parse(date_ud);
            if (files != null && files.getCreateDate() != null && files.getCreateDate().before(d_ud)) {
                DEPT_ID = Constants.BOYTE;
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }

        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;
        // Load Order
        // load ho so
        documentTypeCode = Constants.IMPORT_ORDER.DOCUMENT_TYPE_ORDERCODE_TAOMOI;

        menuType = (Integer) arguments.get("menuType");

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        listTopActionComp = new ArrayList<>();
        listBottomActionComp = new ArrayList<>();
        addAllNextActions();
//        if (menuType != null) {
//            loadActionsToToolbar(menuType, files, processCurrent, windowView);
//        }
        if (checkListOK() == 1) {
            fillFinalFileRegister(fileId);
        }

        if (checkAmendment() == 1) {
            fillFinalFileSDBX();
        }

        if (checkKTKN() == 1) {
            fillFinalListboxProKTKN(fileId);
        }
        if (checkReduced() == 1) {
            fillFinalListboxProductgiam(fileId);
        }

//        if (checkFee() == 1) {
            fillFee(fileId);
//        }
        //linhdx them danh sach cac ho so kiem tra
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST, fileId);
        fillFileListbox(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS, fileId);
    }
    //Yeu cau bo xung

    private void fillFinalFileSDBX() {

        AttachDAOHE rDaoHe = new AttachDAOHE();

        List<Attachs> attachs = new ArrayList<Attachs>();
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO.findAllActiveByFileId(fileId);
        // get file attach
        if (lstAdd.size() > 0) {
            AdditionalRequest additionalRequest = lstAdd.get(0);
            ///List<Attachs> lstAttach2 = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
            List<Attachs> lstAttach2 = rDaoHe.getByAllFileAttachSDBS(
                    additionalRequest.getAdditionalRequestId(),
                    Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                    Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
            attachs.addAll(lstAttach2);
        }
        this.finalFileListboxProSDBS.setModel(new ListModelArray(attachs));
        // TODO Auto-generated method stub

    }

    public int checkAmendment() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO.findAllActiveByFileId(fileId);
        // get file attach
        if (lstAdd.size() > 0) {
            AdditionalRequest additionalRequest = lstAdd.get(0);
            List<Attachs> lstAttach = rDaoHe.getByAllFileAttachSDBS(
                    additionalRequest.getAdditionalRequestId(),
                    Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                    Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
            if (lstAttach.size() > 0) {
                return 1;
            }
        }
        return 0;
    }
    //IMPORT_ORDER_FILE_REDUCED_PAPER. giay kiem tra giam

    private void fillFinalListboxProductgiam(Long fileId2) {

        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach4 = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId2,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
        // TODO Auto-generated method stub

        this.finalFileListboxProductgiam.setModel(new ListModelArray(lstAttach4));

    }

    public int checkReduced() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;

    }

    @Listen("onDownloadFinalFile = #finalFileListboxProSDBS, #finalFileListboxProKTKN, #finalFileListboxProductgiam, #finalFileListboxProregister")
    public void onDownloadFinalFileALL(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }
    //IMPORT_ORDER_FILE_PAPER

    private void fillFinalListboxProKTKN(Long fileId2) {

        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId2,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
        // TODO Auto-generated method stub
        this.finalFileListboxProKTKN.setModel(new ListModelArray(lstAttach));
    }

    public int checkKTKN() {
        AttachDAOHE rDaoHe = new AttachDAOHE();

        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    //IMPORT_ORDER_FILE_REGISTERED_PAPER giay cong bo 
    private void fillFinalFileRegister(Long fileId2) {

        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach3 = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
                fileId2,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);

        this.finalFileListboxProregister.setModel(new ListModelArray(lstAttach3));
        // TODO Auto-generated method stub

    }

    public int checkListOK() {
        AttachDAOHE rDaoHe = new AttachDAOHE();

        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndAttachCatAndAttachTypeByIdMAX(
                fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS,
                Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    public void loadActionsToToolbar(int menuType, final Files files,
            final com.viettel.core.workflow.BO.Process currentProcess,
            final Window currentWindow) {

        for (Component comp : listTopActionComp) {
            divToolbarTop.appendChild(comp);
        }
        for (Component comp : listBottomActionComp) {
            divToolbarBottom.appendChild(comp);
        }

    }

    @Listen("onViewFlow = #windowView")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        Long docId = files.getFileId();
        //Long docType = files.getFileType();
        Long docType = Constants.FILE_TYPE_DYCNK;
        args.put("objectId", docId);
        args.put("objectType", docType);
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    @Listen("onClick = #btnOpenAttachFileFinal")
    public void onCreateAttachFileFinal() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdAttachFinalFileCRUD",
                "/Pages/module/cosmetic/attachFinalFileCRUD.zul", arguments,
                Window.POPUP);
    }

    private Map<String, Object> setArgument() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        return arguments;
    }

    public void setListEvaluation() {
        // lay danh sach cac tham dinh
        if (checkEvaluation() == 1) {
            CosEvaluationRecordDAO dao = new CosEvaluationRecordDAO();
            List<CosEvaluationRecord> lstCosEvaluationRecord = dao.getAllEvaluation(files.getFileId());
            if (lbListEvaluation != null) {
                lbListEvaluation.setModel(new ListModelArray(
                        lstCosEvaluationRecord));
            }

        }
    }

    @Listen("onOpenView = #incListEvaluation #lbListEvaluation")
    public void onOpenView(Event event) {
        CosEvaluationRecord evaluationRecord = (CosEvaluationRecord) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("evaluationRecord", evaluationRecord);
        Window window = createWindow("windowViewEvalution",
                "/Pages/module/cosmetic/viewEvaluation.zul", arguments,
                Window.POPUP);
        window.doModal();

    }

    @Listen("onAfterRender = #lbProductPresentation")
    public void onAfterRenderProductPresentation() {
        loadProductPresentationCheck();
        disableListItem(lbProductPresentation);
    }

    /**
     * Ham disable list item sau khi render Neu set trong zul thi nhung truong
     * checked khong hien thi
     *
     * @param lb
     */
    public void disableListItem(Listbox lb) {
        if (lb != null) {
            for (Listitem item : lb.getItems()) {
                item.setDisabled(true);
            }
        }
    }

    private void addAllNextActions() {
        // linhdx
        //Long docStatus = files.getStatus();
        Long docId = files.getFileId();
        Long userId = getUserId();
        Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;
        List<com.viettel.core.workflow.BO.Process> lstProcess = WorkflowAPI.getInstance().getProcess(docId,
                docType, userId);
        List<com.viettel.core.workflow.BO.Process> lstAllProcess = WorkflowAPI.getInstance().getAllProcessNotFinish(
                docId, docType);
        try{
            if ((lstAllProcess == null || lstAllProcess.isEmpty()) && files.getStatus() != Constants.FILE_STATUS_CODE.STATUS_MOITAO) {
                return;
            }
        }catch(NullPointerException ex)
        {
            LogUtils.addLog(ex);
        }
        List<Long> lstStatus = new ArrayList();
        // linhdx tim nhung trang thai chua hoan thanh xu ly de tim action tiep
        // theo
        for (com.viettel.core.workflow.BO.Process obj : lstProcess) {
            if (obj.getFinishDate() == null) {
                lstStatus.add(obj.getStatus());
            }
        }
        // Neu khong co add flag -1 de bao la khong phai xu ly nua
        if (lstStatus.isEmpty() && !lstProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }

        if (lstStatus.isEmpty() && lstAllProcess != null && !lstAllProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }

        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(docType, lstStatus, deptId);
        lstNextAction = actions;// linhdx
        if (actions != null && actions.size() > 0) {
            NodeToNode action = actions.get(0);
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
    }

    // viethd3: create new button for a processing action
    private Button createButtonForAction(NodeToNode action) {

        Button btn = new Button("Xử lý hồ sơ");
        final String actionName = "Xử lý hồ sơ";
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action
        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = Long.valueOf(files.getStatus());
        final List<NodeToNode> lstNextAction1 = lstNextAction;

        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, false));

        EventListener<Event> event = new EventListener<Event>() {

            @Override
            public void onEvent(Event t) throws Exception {
                Map<String, Object> data = new ConcurrentHashMap<>();
                data.put("fileId", files.getFileId());
                data.put("docId", files.getFileId());
                data.put("docType", files.getFileCode());
                data.put("docStatus", status);
                data.put("actionId", actionId);
                data.put("actionName", actionName);
                data.put("actionType", actionType);
                data.put("nextId", nextId);
                data.put("previousId", prevId);
                if (processCurrent != null) {
                    data.put("process", processCurrent);
                }

                data.put("lstAvailableNDU", lstNDUs);
                data.put("parentWindow", windowView);
                data.put("lstNextAction", lstNextAction1);
                createWindow("windowComment", formName, data, Window.MODAL);
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    @Listen("onRefresh=#windowView")
    public void onRefresh() {
        windowView.detach();

        ImportOrderFileViewDAO viewCosFileDAO = new ImportOrderFileViewDAO();
        VFileImportOrder vFileCosfile = viewCosFileDAO.findById(fileId);
        Events.sendEvent("onRefresh", parentWindow, vFileCosfile);
        LogUtils.addLog("on refresh view window!");
    }

    public boolean validateUserRight(NodeToNode action, com.viettel.core.workflow.BO.Process processCurrent,
            Files file) {
        List<com.viettel.core.workflow.BO.Process> listCurrentProcess = WorkflowAPI.getInstance().findAllCurrentProcess(file.getFileId(), file.getFileType(),
                file.getStatus());
        Long userId = getUserId();
        Long creatorId = file.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return true;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                for (com.viettel.core.workflow.BO.Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                if (!needToProcess) {
                    return true;
                }
            }
        }
        return false;
    }

    @Listen("onAfterRender = #lbProductType")
    public void onAfterRenderProductType() {
        for (int i = 0; i < lbProductType.getItemCount(); i++) {
            Listitem item = lbProductType.getItemAtIndex(i);
            Listcell cell = (Listcell) item.getChildren().get(0);
            CosProductTypeSubModel cos = item.getValue();
            List<CosProductTypeSub> lstCosProductTypeSub = cos.getLstCosProductTypeSub();
            if (lstCosProductTypeSub != null && lstCosProductTypeSub.size() > 0) {
                for (CosProductTypeSub obj : lstCosProductTypeSub) {
                    Label lb = new Label("- " + obj.getNameVi());
                    lb.setClass("product-type-sub");
                    lb.setStyle("font-style: normal !important;");
                    Label enLb = new Label(obj.getNameEn());
                    enLb.setClass("product-type-sub-en");
                    Br br = new Br();
                    cell.appendChild(br);
                    cell.appendChild(lb);
                    cell.appendChild(new Br());
                    cell.appendChild(enLb);
                }
            }
        }
        lbProductType.renderAll();
        loadProductTypeCheck();
        disableListItem(lbProductType);
    }

    private void loadProductPresentationCheck() {
    }

    private void loadProductTypeCheck() {
    }

    public String getNameLink(Long id) {
        return "123\n\r456";
    }

    public void setValueProductType(Textbox textbox) {
        Label label = new Label();
        label.setValue("123");
        Br br = new Br();
        textbox.getParent().appendChild(br);
        textbox.getParent().appendChild(label);

    }

    /**
     * linhdx Hàm load danh sach trong dropdown control
     *
     * @param type
     * @return
     */
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case RAPID_TEST_FILE_TYPE:
                categoryDAOHE = new CategoryDAOHE();
                listCosmeticFileType = categoryDAOHE.findAllCategory(Constants.CATEGORY_TYPE.COSMETIC_FILE_TYPE);
                lstModel = new ListModelList(listCosmeticFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    @Listen("onDownloadFinalFile = #finalFileListboxPro")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    /**
     * Dong cua so khi o dang popup
     */
    @Listen("onClose = #windowView")
    public void onClose() {
        windowView.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
    }

    public void onDownloadPermit() throws FileNotFoundException {
        CosPermitDAO cosPermitDAO = new CosPermitDAO();
        CosPermit permit = cosPermitDAO.findLastByFileId(fileId);

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> lstAttach = attDAOHE.getByObjectIdAndType(
                permit.getPermitId(), Constants.OBJECT_TYPE.COSMETIC_PERMIT);
        Attachs att = null;
        if (lstAttach != null && lstAttach.size() > 0) {
            att = lstAttach.get(0);

        }
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }
    public int checkListSDBS() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS);
        if (lstAttach.size() > 0) {
            return 1;
        }
        return 0;
    }

    public int checkDispathSDBS() {
        return checkDispathSDBS(files.getFileId());
    }

    public int checkDispathReject() {
        return checkDispathReject(files.getFileId());
    }

    public int checkBooked() {
        return checkBooked(files.getFileId(), 1L);
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public ListModel getModel() {
        return model;
    }

    public void setModel(ListModel model) {
        this.model = model;
    }

    public IfdFile getFiles() {
        return files;
    }

    public void setFiles(IfdFile files) {
        this.files = files;
    }

    public String getIsSave() {
        return isSave;
    }

    public void setIsSave(String isSave) {
        this.isSave = isSave;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    public int checkEvaluation() {
        return checkEvaluation(files.getFileId());
    }

    public int checkPermit() {
        return checkPermit(files.getFileId());
    }

    public int checkPayment() {
        return checkPayment(files.getFileId());
    }

    public Listbox getLbListEvaluation() {
        return lbListEvaluation;
    }

    public void setLbListEvaluation(Listbox lbListEvaluation) {
        this.lbListEvaluation = lbListEvaluation;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Boolean Checkingredient(CosCosfileIngre cosCosfileIngre,
            String hidden) {
        for (int i = 0; i < ingredientListCheck.size(); i++) {
            Long b = Long.parseLong(cosCosfileIngre.getCosfileIngreId().toString());
            Long a = Long.parseLong(ingredientListCheck.get(i).toString());
            if (a.equals(b)) {
                if ("1".equals(hidden)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        if ("1".equals(hidden)) {
            return false;
        } else {
            return true;
        }
    }
    public int checkFee() {
        IfdFilePaymentDAO a = new IfdFilePaymentDAO();
        List<IfdFilePayment> lsPaymentInfo = a.findByFileId(fileId);
        if (lsPaymentInfo.size() > 0) {
            return 1;
        }
        return 0;
    }

    public void fillFee(Long fileId2) {
        IfdFilePaymentDAO a = new IfdFilePaymentDAO();
        List<IfdFilePayment> lsPaymentInfo1 = a.findByFileId(fileId2);
        System.out.println("FileId "+fileId2+":"+lsPaymentInfo1.size());
        this.lFillFee.setModel(new ListModelList(lsPaymentInfo1));
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        downLoadFileEvent(event);

    }

    private void fillFileListbox(Long obj_type, Long obj_id) {
        IfdFileAttachDAO dao = new IfdFileAttachDAO();
        List<IfdAttachment> lstCosmeticAttach = dao.findCheckedFilecAttach(obj_type, obj_id);
        if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_RS.equals(obj_type)) {
            //lấy thêm file kiểm tra 
            List<IfdAttachment> temp_list = dao.findCheckedFilecAttach(Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK, fileId);
            lstCosmeticAttach.addAll(temp_list);
            fileListboxOrderFile.setModel(new ListModelArray(lstCosmeticAttach));
        } else if (Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST.equals(obj_type)) {
            fileListboxOrderReturn.setModel(new ListModelArray(lstCosmeticAttach));
        }

    }

    //tải tệp đính kèm của một sản phẩm
    @Listen("onDownloadFileListboxOrderReturn = #fileListboxOrderReturn")
    public void onDownloadFileListboxOrderReturn(Event event) throws FileNotFoundException {
        downLoadFileEvent(event);
    }

    //tải tệp đính kèm của cả hồ sơ  
    @Listen("onDownloadOrderFile =#fileListboxOrderFile")
    public void onDownloadOrderFile(Event event) throws FileNotFoundException {
        downLoadFileEvent(event);
    }

    public void downLoadFileEvent(Event event) {
        IfdAttachment obj = (IfdAttachment) event.getData();
        Long attachId = obj.getAttachId();
        IfdFileAttachDAO attDAOHE = new IfdFileAttachDAO();
        IfdAttachment att = attDAOHE.findById(attachId);
        attDAOHE.downloadFileAttach(att);
    }

    public String getFileListboxDuplicateTransportNoFlag() {
        return fileListboxDuplicateTransportNoFlag;
    }

    public void setFileListboxDuplicateTransportNoFlag(String fileListboxDuplicateTransportNoFlag) {
        this.fileListboxDuplicateTransportNoFlag = fileListboxDuplicateTransportNoFlag;
    }
    
    @Listen("onClick=#btnAction")
    public void onShowAction() {
        System.out.println("open show action page..." + fileId);
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("fileId", fileId);
        arguments.put("parentWindow", importFoodDetail);
        createWindow("windowView",
                "/Pages/module/importfood/action/pheduyet.zul", arguments,
                Window.MODAL);
    }
}
