/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.import_food.controller;

import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.Controller.*;
import com.viettel.module.importOrder.BO.ImportOrderProduct;
import com.viettel.module.importOrder.DAO.ImportOrderFileViewDAO;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.DAO.IfdFileDAO;
import com.viettel.module.importfood.DAO.ImportFoodProductDAO;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author giangnh20
 */
public class ImportFoodView_TabProfileController extends
        CosmeticBaseController {
    private IfdFile files = new IfdFile();
    @Wire
    private Textbox tbStatus;
    @Wire
    private Label lbStatus;
    
    private Long fileId;

    @Wire
    Listbox lbFoodProduct;

    //
    // List<NodeToNode> lstNextAction;
    //
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
  
        fileId = Long.valueOf(Executions.getCurrent().getParameter("fileId"));
        files = (new IfdFileDAO()).findById(fileId);
        //
        // Load Order
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        List<IfdFileProduct> importFoodProducts = new ImportFoodProductDAO()
                .findByFileId(fileId);
        ListModelArray<IfdFileProduct> lstModelManufacturer = new ListModelArray<IfdFileProduct>(
                importFoodProducts);
        lbFoodProduct.setModel(lstModelManufacturer);
    }


    @Listen("onShow =  #lbOrderProduct")
    public void onShow(Event event) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        ImportOrderProduct obj = (ImportOrderProduct) event.getData();
        Long productId = obj.getProductId();
        arguments.put("productId", productId);
        createWindow("showProduct",
                "/Pages/module/importorder/importOrderViewProduct.zul",
                arguments, Window.HIGHLIGHTED);
    }

    public IfdFile getFiles() {
        return files;
    }

    public void setFiles(IfdFile files) {
        this.files = files;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    public boolean isAbleToModifyStatus() {
        boolean result = false;
        if ("admin".equals(getUserName())) {
            result = true;
        }
        return result;
    }

    public String getCheckTime(Date from,Date to)
    {
        try{
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(from)+" - "+sdf.format(to);
        }
        catch(Exception ex){
            System.out.println("ex "+ex);
            return "";
        }
    }
}
