/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.import_food.service;

import com.viettel.module.importfood.BO.IfdFileResult;
import com.viettel.module.importfood.Constant.IfdFileStatus;
import com.viettel.module.importfood.DAO.FileResultDAO;
import java.util.Date;

/**
 *
 * @author vietn
 */
public class ActionPaymentService {
    
    private FileResultDAO resultDao;
    
    public IfdFileResult reject(long userId, long fileId, String fileCode, String content){
        
        IfdFileResult result = new IfdFileResult();
        try {
            result.setCreateBy(userId);
            result.setCreateDate(new Date());
            result.setFileCode(fileCode);
            result.setFileId(fileId);
            result.setFileStatus(IfdFileStatus.CHO_PHAN_CONG.getStatus());
            result.setFileStatusName(IfdFileStatus.CHO_PHAN_CONG.getStatusName());
            result.setProcessContent(content);
            result.setProcessDate(new Date());
            result.setUpdateDate(new Date());
            result.setUpdateBy(userId);
            result.setProcessName("BO_SUNG_PHI");

            resultDao = new FileResultDAO();
            Long resultId = resultDao.create(result);
            result.setFileResultId(resultId);
            System.out.println("result" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
