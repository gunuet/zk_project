/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.import_food.service;

import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.DAO.dao.IfdFileProductDAO;
import java.util.List;

/**
 *
 * @author Admin
 */
public class FileProductService {
    private IfdFileProductDAO productDao;
    
    public IfdFileProduct get(Long productId){
            productDao = new IfdFileProductDAO();
            System.out.println(productId);
            return productDao.findById(productId);
         }
        
    public List<IfdFileProduct> getProductByFileId(Long fileId){
        productDao = new IfdFileProductDAO();
        List<IfdFileProduct> searchList = productDao.findAllByFileId(fileId);
        return searchList;
    }
}
