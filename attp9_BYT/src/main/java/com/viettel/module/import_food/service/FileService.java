/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.import_food.service;

import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.DAO.IfdFileDAO;
import java.util.Date;
import java.util.List;

/**
 *
 * @author vietn
 */
public class FileService {
    
    private IfdFileDAO fileDao;
    
    public IfdFile get(Long fileId){
        fileDao = new IfdFileDAO();
        System.out.println(fileId);
        return fileDao.findById(fileId);
    }
    
    public List<IfdFile> filter(String fileCode, int status, String ownerName, 
            Date comingDateFrom, Date comingDateTo,
            int take, int start){
        fileDao = new IfdFileDAO();
        return fileDao.filter(fileCode, status, ownerName, comingDateFrom, comingDateTo, take, start);
    }
    
    public Long count(String fileCode, int status, String ownerName, 
            Date comingDateFrom, Date comingDateTo){
        fileDao = new IfdFileDAO();
        return fileDao.count(fileCode, status, ownerName, comingDateFrom, comingDateTo);
    }
    
}
