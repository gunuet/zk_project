/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_ATTACHMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdAttachment.findAll", query = "SELECT i FROM IfdAttachment i")
    , @NamedQuery(name = "IfdAttachment.findByAttachId", query = "SELECT i FROM IfdAttachment i WHERE i.attachId = :attachId")
    , @NamedQuery(name = "IfdAttachment.findByObjectId", query = "SELECT i FROM IfdAttachment i WHERE i.objectId = :objectId")
    , @NamedQuery(name = "IfdAttachment.findByObjectType", query = "SELECT i FROM IfdAttachment i WHERE i.objectType = :objectType")
    , @NamedQuery(name = "IfdAttachment.findByAttachmentCode", query = "SELECT i FROM IfdAttachment i WHERE i.attachmentCode = :attachmentCode")
    , @NamedQuery(name = "IfdAttachment.findByAttachmentName", query = "SELECT i FROM IfdAttachment i WHERE i.attachmentName = :attachmentName")
    , @NamedQuery(name = "IfdAttachment.findByAttachTypeCode", query = "SELECT i FROM IfdAttachment i WHERE i.attachTypeCode = :attachTypeCode")
    , @NamedQuery(name = "IfdAttachment.findByAttachTypeName", query = "SELECT i FROM IfdAttachment i WHERE i.attachTypeName = :attachTypeName")
    , @NamedQuery(name = "IfdAttachment.findByAttachMohId", query = "SELECT i FROM IfdAttachment i WHERE i.attachMohId = :attachMohId")
    , @NamedQuery(name = "IfdAttachment.findByAttachPath", query = "SELECT i FROM IfdAttachment i WHERE i.attachPath = :attachPath")
    , @NamedQuery(name = "IfdAttachment.findByStatus", query = "SELECT i FROM IfdAttachment i WHERE i.status = :status")
    , @NamedQuery(name = "IfdAttachment.findByCreateDate", query = "SELECT i FROM IfdAttachment i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdAttachment.findByCreateBy", query = "SELECT i FROM IfdAttachment i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdAttachment.findByUpdateDate", query = "SELECT i FROM IfdAttachment i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdAttachment.findByUpdateBy", query = "SELECT i FROM IfdAttachment i WHERE i.updateBy = :updateBy")})
public class IfdAttachment implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_ATTACHMENT_SEQ")
    @SequenceGenerator(sequenceName = "IFD_ATTACHMENT_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_ATTACHMENT_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATTACH_ID")
    private Long attachId;
    @Column(name = "OBJECT_ID")
    private Long objectId;
    @Column(name = "OBJECT_TYPE")
    private Long objectType;
    @Column(name = "ATTACHMENT_CODE")
    private Long attachmentCode;
    @Size(max = 255)
    @Column(name = "ATTACHMENT_NAME")
    private String attachmentName;
    @Column(name = "ATTACH_TYPE_CODE")
    private Long attachTypeCode;
    @Size(max = 255)
    @Column(name = "ATTACH_TYPE_NAME")
    private String attachTypeName;
    @Size(max = 50)
    @Column(name = "ATTACH_MOH_ID")
    private String attachMohId;
    @Size(max = 255)
    @Column(name = "ATTACH_PATH")
    private String attachPath;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;

    public IfdAttachment() {
    }

    public IfdAttachment(Long attachId) {
        this.attachId = attachId;
    }

    public IfdAttachment(Long attachId, Long status, Date createDate, Date updateDate) {
        this.attachId = attachId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getAttachId() {
        return attachId;
    }

    public void setAttachId(Long attachId) {
        this.attachId = attachId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getObjectType() {
        return objectType;
    }

    public void setObjectType(Long objectType) {
        this.objectType = objectType;
    }

    public Long getAttachmentCode() {
        return attachmentCode;
    }

    public void setAttachmentCode(Long attachmentCode) {
        this.attachmentCode = attachmentCode;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public Long getAttachTypeCode() {
        return attachTypeCode;
    }

    public void setAttachTypeCode(Long attachTypeCode) {
        this.attachTypeCode = attachTypeCode;
    }

    public String getAttachTypeName() {
        return attachTypeName;
    }

    public void setAttachTypeName(String attachTypeName) {
        this.attachTypeName = attachTypeName;
    }

    public String getAttachMohId() {
        return attachMohId;
    }

    public void setAttachMohId(String attachMohId) {
        this.attachMohId = attachMohId;
    }

    public String getAttachPath() {
        return attachPath;
    }

    public void setAttachPath(String attachPath) {
        this.attachPath = attachPath;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attachId != null ? attachId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdAttachment)) {
            return false;
        }
        IfdAttachment other = (IfdAttachment) object;
        if ((this.attachId == null && other.attachId != null) || (this.attachId != null && !this.attachId.equals(other.attachId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdAttachment[ attachId=" + attachId + " ]";
    }

}
