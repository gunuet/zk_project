/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_CERTIFICATE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdCertificate.findAll", query = "SELECT i FROM IfdCertificate i")
    , @NamedQuery(name = "IfdCertificate.findByCertificateId", query = "SELECT i FROM IfdCertificate i WHERE i.certificateId = :certificateId")
    , @NamedQuery(name = "IfdCertificate.findByFileId", query = "SELECT i FROM IfdCertificate i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdCertificate.findByCertifaicteFilesNo", query = "SELECT i FROM IfdCertificate i WHERE i.certifaicteFilesNo = :certifaicteFilesNo")
    , @NamedQuery(name = "IfdCertificate.findByFileCode", query = "SELECT i FROM IfdCertificate i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdCertificate.findByCheckMethod", query = "SELECT i FROM IfdCertificate i WHERE i.checkMethod = :checkMethod")
    , @NamedQuery(name = "IfdCertificate.findByCertificateCreateDate", query = "SELECT i FROM IfdCertificate i WHERE i.certificateCreateDate = :certificateCreateDate")
    , @NamedQuery(name = "IfdCertificate.findByCertificateCreateBy", query = "SELECT i FROM IfdCertificate i WHERE i.certificateCreateBy = :certificateCreateBy")
    , @NamedQuery(name = "IfdCertificate.findByGoodsOwnerName", query = "SELECT i FROM IfdCertificate i WHERE i.goodsOwnerName = :goodsOwnerName")
    , @NamedQuery(name = "IfdCertificate.findByGoodsOwnerAddress", query = "SELECT i FROM IfdCertificate i WHERE i.goodsOwnerAddress = :goodsOwnerAddress")
    , @NamedQuery(name = "IfdCertificate.findByGoodsOwnerPhone", query = "SELECT i FROM IfdCertificate i WHERE i.goodsOwnerPhone = :goodsOwnerPhone")
    , @NamedQuery(name = "IfdCertificate.findByGoodsOwnerFax", query = "SELECT i FROM IfdCertificate i WHERE i.goodsOwnerFax = :goodsOwnerFax")
    , @NamedQuery(name = "IfdCertificate.findByGoodsOwnerEmail", query = "SELECT i FROM IfdCertificate i WHERE i.goodsOwnerEmail = :goodsOwnerEmail")
    , @NamedQuery(name = "IfdCertificate.findByResponesiblePersonName", query = "SELECT i FROM IfdCertificate i WHERE i.responesiblePersonName = :responesiblePersonName")
    , @NamedQuery(name = "IfdCertificate.findByResponesiblePersonAddress", query = "SELECT i FROM IfdCertificate i WHERE i.responesiblePersonAddress = :responesiblePersonAddress")
    , @NamedQuery(name = "IfdCertificate.findByResponesiblePersonPhone", query = "SELECT i FROM IfdCertificate i WHERE i.responesiblePersonPhone = :responesiblePersonPhone")
    , @NamedQuery(name = "IfdCertificate.findByResponesiblePersonFax", query = "SELECT i FROM IfdCertificate i WHERE i.responesiblePersonFax = :responesiblePersonFax")
    , @NamedQuery(name = "IfdCertificate.findByResponesiblePersonEmail", query = "SELECT i FROM IfdCertificate i WHERE i.responesiblePersonEmail = :responesiblePersonEmail")
    , @NamedQuery(name = "IfdCertificate.findByExporterName", query = "SELECT i FROM IfdCertificate i WHERE i.exporterName = :exporterName")
    , @NamedQuery(name = "IfdCertificate.findByExporterAddress", query = "SELECT i FROM IfdCertificate i WHERE i.exporterAddress = :exporterAddress")
    , @NamedQuery(name = "IfdCertificate.findByExporterPhone", query = "SELECT i FROM IfdCertificate i WHERE i.exporterPhone = :exporterPhone")
    , @NamedQuery(name = "IfdCertificate.findByExporterFax", query = "SELECT i FROM IfdCertificate i WHERE i.exporterFax = :exporterFax")
    , @NamedQuery(name = "IfdCertificate.findByExporterEmail", query = "SELECT i FROM IfdCertificate i WHERE i.exporterEmail = :exporterEmail")
    , @NamedQuery(name = "IfdCertificate.findByCustomDeclarationNo", query = "SELECT i FROM IfdCertificate i WHERE i.customDeclarationNo = :customDeclarationNo")
    , @NamedQuery(name = "IfdCertificate.findByComingDateFrom", query = "SELECT i FROM IfdCertificate i WHERE i.comingDateFrom = :comingDateFrom")
    , @NamedQuery(name = "IfdCertificate.findByComingDateTo", query = "SELECT i FROM IfdCertificate i WHERE i.comingDateTo = :comingDateTo")
    , @NamedQuery(name = "IfdCertificate.findByExporterGateCode", query = "SELECT i FROM IfdCertificate i WHERE i.exporterGateCode = :exporterGateCode")
    , @NamedQuery(name = "IfdCertificate.findByExporterGateName", query = "SELECT i FROM IfdCertificate i WHERE i.exporterGateName = :exporterGateName")
    , @NamedQuery(name = "IfdCertificate.findByImporterGateCode", query = "SELECT i FROM IfdCertificate i WHERE i.importerGateCode = :importerGateCode")
    , @NamedQuery(name = "IfdCertificate.findByImporterGateName", query = "SELECT i FROM IfdCertificate i WHERE i.importerGateName = :importerGateName")
    , @NamedQuery(name = "IfdCertificate.findByCheckTimeFrom", query = "SELECT i FROM IfdCertificate i WHERE i.checkTimeFrom = :checkTimeFrom")
    , @NamedQuery(name = "IfdCertificate.findByCheckTimeTo", query = "SELECT i FROM IfdCertificate i WHERE i.checkTimeTo = :checkTimeTo")
    , @NamedQuery(name = "IfdCertificate.findByCheckPlace", query = "SELECT i FROM IfdCertificate i WHERE i.checkPlace = :checkPlace")
    , @NamedQuery(name = "IfdCertificate.findByCheckDeptCode", query = "SELECT i FROM IfdCertificate i WHERE i.checkDeptCode = :checkDeptCode")
    , @NamedQuery(name = "IfdCertificate.findByCheckDeptName", query = "SELECT i FROM IfdCertificate i WHERE i.checkDeptName = :checkDeptName")
    , @NamedQuery(name = "IfdCertificate.findByCertificateStatus", query = "SELECT i FROM IfdCertificate i WHERE i.certificateStatus = :certificateStatus")
    , @NamedQuery(name = "IfdCertificate.findByCertificateStatusName", query = "SELECT i FROM IfdCertificate i WHERE i.certificateStatusName = :certificateStatusName")
    , @NamedQuery(name = "IfdCertificate.findByStatus", query = "SELECT i FROM IfdCertificate i WHERE i.status = :status")
    , @NamedQuery(name = "IfdCertificate.findByIsDelete", query = "SELECT i FROM IfdCertificate i WHERE i.isDelete = :isDelete")
    , @NamedQuery(name = "IfdCertificate.findByCreateDate", query = "SELECT i FROM IfdCertificate i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdCertificate.findByCreateBy", query = "SELECT i FROM IfdCertificate i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdCertificate.findByUpdateDate", query = "SELECT i FROM IfdCertificate i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdCertificate.findByUpdateBy", query = "SELECT i FROM IfdCertificate i WHERE i.updateBy = :updateBy")})
public class IfdCertificate implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_CERTIFICATE")
    @SequenceGenerator(sequenceName = "IFD_CERTIFICATE", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_CERTIFICATE")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CERTIFICATE_ID")
    private Long certificateId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "CERTIFAICTE_FILES_NO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date certifaicteFilesNo;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Column(name = "CHECK_METHOD")
    private Short checkMethod;
    @Column(name = "CERTIFICATE_CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date certificateCreateDate;
    @Size(max = 14)
    @Column(name = "CERTIFICATE_CREATE_BY")
    private String certificateCreateBy;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_NAME")
    private String goodsOwnerName;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_ADDRESS")
    private String goodsOwnerAddress;
    @Size(max = 50)
    @Column(name = "GOODS_OWNER_PHONE")
    private String goodsOwnerPhone;
    @Size(max = 20)
    @Column(name = "GOODS_OWNER_FAX")
    private String goodsOwnerFax;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_EMAIL")
    private String goodsOwnerEmail;
    @Size(max = 255)
    @Column(name = "RESPONESIBLE_PERSON_NAME")
    private String responesiblePersonName;
    @Size(max = 255)
    @Column(name = "RESPONESIBLE_PERSON_ADDRESS")
    private String responesiblePersonAddress;
    @Size(max = 50)
    @Column(name = "RESPONESIBLE_PERSON_PHONE")
    private String responesiblePersonPhone;
    @Size(max = 20)
    @Column(name = "RESPONESIBLE_PERSON_FAX")
    private String responesiblePersonFax;
    @Size(max = 255)
    @Column(name = "RESPONESIBLE_PERSON_EMAIL")
    private String responesiblePersonEmail;
    @Size(max = 255)
    @Column(name = "EXPORTER_NAME")
    private String exporterName;
    @Size(max = 255)
    @Column(name = "EXPORTER_ADDRESS")
    private String exporterAddress;
    @Size(max = 50)
    @Column(name = "EXPORTER_PHONE")
    private String exporterPhone;
    @Size(max = 20)
    @Column(name = "EXPORTER_FAX")
    private String exporterFax;
    @Size(max = 255)
    @Column(name = "EXPORTER_EMAIL")
    private String exporterEmail;
    @Size(max = 255)
    @Column(name = "CUSTOM_DECLARATION_NO")
    private String customDeclarationNo;
    @Column(name = "COMING_DATE_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comingDateFrom;
    @Column(name = "COMING_DATE_TO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comingDateTo;
    @Size(max = 6)
    @Column(name = "EXPORTER_GATE_CODE")
    private String exporterGateCode;
    @Size(max = 255)
    @Column(name = "EXPORTER_GATE_NAME")
    private String exporterGateName;
    @Size(max = 6)
    @Column(name = "IMPORTER_GATE_CODE")
    private String importerGateCode;
    @Size(max = 255)
    @Column(name = "IMPORTER_GATE_NAME")
    private String importerGateName;
    @Column(name = "CHECK_TIME_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkTimeFrom;
    @Column(name = "CHECK_TIME_TO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkTimeTo;
    @Size(max = 255)
    @Column(name = "CHECK_PLACE")
    private String checkPlace;
    @Size(max = 12)
    @Column(name = "CHECK_DEPT_CODE")
    private String checkDeptCode;
    @Size(max = 255)
    @Column(name = "CHECK_DEPT_NAME")
    private String checkDeptName;
    @Column(name = "CERTIFICATE_STATUS")
    private Long certificateStatus;
    @Size(max = 255)
    @Column(name = "CERTIFICATE_STATUS_NAME")
    private String certificateStatusName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "IS_DELETE")
    private Short isDelete;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    @Size(max = 100)
    @Column(name = "SIGN_NAME")
    private String signName;
    
    @Transient
    private List<IfdCertificateProduct> lstProduct;

    @Transient
    private IfdAttachment attach;

    public IfdCertificate() {
    }

    public IfdCertificate(Long certificateId) {
        this.certificateId = certificateId;
    }

    public IfdCertificate(Long certificateId, Long status, Date createDate, Date updateDate) {
        this.certificateId = certificateId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Date getCertifaicteFilesNo() {
        return certifaicteFilesNo;
    }

    public void setCertifaicteFilesNo(Date certifaicteFilesNo) {
        this.certifaicteFilesNo = certifaicteFilesNo;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Short getCheckMethod() {
        return checkMethod;
    }

    public void setCheckMethod(Short checkMethod) {
        this.checkMethod = checkMethod;
    }

    public Date getCertificateCreateDate() {
        return certificateCreateDate;
    }

    public void setCertificateCreateDate(Date certificateCreateDate) {
        this.certificateCreateDate = certificateCreateDate;
    }

    public String getCertificateCreateBy() {
        return certificateCreateBy;
    }

    public void setCertificateCreateBy(String certificateCreateBy) {
        this.certificateCreateBy = certificateCreateBy;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return goodsOwnerEmail;
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = goodsOwnerEmail;
    }

    public String getResponesiblePersonName() {
        return responesiblePersonName;
    }

    public void setResponesiblePersonName(String responesiblePersonName) {
        this.responesiblePersonName = responesiblePersonName;
    }

    public String getResponesiblePersonAddress() {
        return responesiblePersonAddress;
    }

    public void setResponesiblePersonAddress(String responesiblePersonAddress) {
        this.responesiblePersonAddress = responesiblePersonAddress;
    }

    public String getResponesiblePersonPhone() {
        return responesiblePersonPhone;
    }

    public void setResponesiblePersonPhone(String responesiblePersonPhone) {
        this.responesiblePersonPhone = responesiblePersonPhone;
    }

    public String getResponesiblePersonFax() {
        return responesiblePersonFax;
    }

    public void setResponesiblePersonFax(String responesiblePersonFax) {
        this.responesiblePersonFax = responesiblePersonFax;
    }

    public String getResponesiblePersonEmail() {
        return responesiblePersonEmail;
    }

    public void setResponesiblePersonEmail(String responesiblePersonEmail) {
        this.responesiblePersonEmail = responesiblePersonEmail;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterFax() {
        return exporterFax;
    }

    public void setExporterFax(String exporterFax) {
        this.exporterFax = exporterFax;
    }

    public String getExporterEmail() {
        return exporterEmail;
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = exporterEmail;
    }

    public String getCustomDeclarationNo() {
        return customDeclarationNo;
    }

    public void setCustomDeclarationNo(String customDeclarationNo) {
        this.customDeclarationNo = customDeclarationNo;
    }

    public Date getComingDateFrom() {
        return comingDateFrom;
    }

    public void setComingDateFrom(Date comingDateFrom) {
        this.comingDateFrom = comingDateFrom;
    }

    public Date getComingDateTo() {
        return comingDateTo;
    }

    public void setComingDateTo(Date comingDateTo) {
        this.comingDateTo = comingDateTo;
    }

    public String getExporterGateCode() {
        return exporterGateCode;
    }

    public void setExporterGateCode(String exporterGateCode) {
        this.exporterGateCode = exporterGateCode;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateCode() {
        return importerGateCode;
    }

    public void setImporterGateCode(String importerGateCode) {
        this.importerGateCode = importerGateCode;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public Date getCheckTimeFrom() {
        return checkTimeFrom;
    }

    public void setCheckTimeFrom(Date checkTimeFrom) {
        this.checkTimeFrom = checkTimeFrom;
    }

    public Date getCheckTimeTo() {
        return checkTimeTo;
    }

    public void setCheckTimeTo(Date checkTimeTo) {
        this.checkTimeTo = checkTimeTo;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getCheckDeptCode() {
        return checkDeptCode;
    }

    public void setCheckDeptCode(String checkDeptCode) {
        this.checkDeptCode = checkDeptCode;
    }

    public String getCheckDeptName() {
        return checkDeptName;
    }

    public void setCheckDeptName(String checkDeptName) {
        this.checkDeptName = checkDeptName;
    }

    public Long getCertificateStatus() {
        return certificateStatus;
    }

    public void setCertificateStatus(Long certificateStatus) {
        this.certificateStatus = certificateStatus;
    }

    public String getCertificateStatusName() {
        return certificateStatusName;
    }

    public void setCertificateStatusName(String certificateStatusName) {
        this.certificateStatusName = certificateStatusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Short getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Short isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public List<IfdCertificateProduct> getLstProduct() {
        return lstProduct;
    }

    public void setLstProduct(List<IfdCertificateProduct> lstProduct) {
        this.lstProduct = lstProduct;
    }

    public IfdAttachment getAttach() {
        return attach;
    }

    public void setAttach(IfdAttachment attach) {
        this.attach = attach;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (certificateId != null ? certificateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdCertificate)) {
            return false;
        }
        IfdCertificate other = (IfdCertificate) object;
        if ((this.certificateId == null && other.certificateId != null) || (this.certificateId != null && !this.certificateId.equals(other.certificateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdCertificate[ certificateId=" + certificateId + " ]";
    }

}
