/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_CERTIFICATE_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdCertificateProduct.findAll", query = "SELECT i FROM IfdCertificateProduct i")
    , @NamedQuery(name = "IfdCertificateProduct.findByCertificateProductId", query = "SELECT i FROM IfdCertificateProduct i WHERE i.certificateProductId = :certificateProductId")
    , @NamedQuery(name = "IfdCertificateProduct.findByCertificateId", query = "SELECT i FROM IfdCertificateProduct i WHERE i.certificateId = :certificateId")
    , @NamedQuery(name = "IfdCertificateProduct.findByProductCode", query = "SELECT i FROM IfdCertificateProduct i WHERE i.productCode = :productCode")
    , @NamedQuery(name = "IfdCertificateProduct.findByProductName", query = "SELECT i FROM IfdCertificateProduct i WHERE i.productName = :productName")
    , @NamedQuery(name = "IfdCertificateProduct.findByProductGroupCode", query = "SELECT i FROM IfdCertificateProduct i WHERE i.productGroupCode = :productGroupCode")
    , @NamedQuery(name = "IfdCertificateProduct.findByProductGroupName", query = "SELECT i FROM IfdCertificateProduct i WHERE i.productGroupName = :productGroupName")
    , @NamedQuery(name = "IfdCertificateProduct.findByManufacturer", query = "SELECT i FROM IfdCertificateProduct i WHERE i.manufacturer = :manufacturer")
    , @NamedQuery(name = "IfdCertificateProduct.findByManufacturerAddress", query = "SELECT i FROM IfdCertificateProduct i WHERE i.manufacturerAddress = :manufacturerAddress")
    , @NamedQuery(name = "IfdCertificateProduct.findByProductCheckMethod", query = "SELECT i FROM IfdCertificateProduct i WHERE i.productCheckMethod = :productCheckMethod")
    , @NamedQuery(name = "IfdCertificateProduct.findByCheckMethodConfirmNo", query = "SELECT i FROM IfdCertificateProduct i WHERE i.checkMethodConfirmNo = :checkMethodConfirmNo")
    , @NamedQuery(name = "IfdCertificateProduct.findByPass", query = "SELECT i FROM IfdCertificateProduct i WHERE i.pass = :pass")
    , @NamedQuery(name = "IfdCertificateProduct.findByReason", query = "SELECT i FROM IfdCertificateProduct i WHERE i.reason = :reason")
    , @NamedQuery(name = "IfdCertificateProduct.findByHandlingMeasuresCode", query = "SELECT i FROM IfdCertificateProduct i WHERE i.handlingMeasuresCode = :handlingMeasuresCode")
    , @NamedQuery(name = "IfdCertificateProduct.findByHandlingMeasuresName", query = "SELECT i FROM IfdCertificateProduct i WHERE i.handlingMeasuresName = :handlingMeasuresName")
    , @NamedQuery(name = "IfdCertificateProduct.findByNote", query = "SELECT i FROM IfdCertificateProduct i WHERE i.note = :note")
    , @NamedQuery(name = "IfdCertificateProduct.findByStatus", query = "SELECT i FROM IfdCertificateProduct i WHERE i.status = :status")
    , @NamedQuery(name = "IfdCertificateProduct.findByCreateDate", query = "SELECT i FROM IfdCertificateProduct i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdCertificateProduct.findByCreateBy", query = "SELECT i FROM IfdCertificateProduct i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdCertificateProduct.findByUpdateDate", query = "SELECT i FROM IfdCertificateProduct i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdCertificateProduct.findByUpdateBy", query = "SELECT i FROM IfdCertificateProduct i WHERE i.updateBy = :updateBy")})
public class IfdCertificateProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_CERTIFICATE_PRODUCT_SEQ")
    @SequenceGenerator(sequenceName = "IFD_CERTIFICATE_PRODUCT_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_CERTIFICATE_PRODUCT_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CERTIFICATE_PRODUCT_ID")
    private Long certificateProductId;
    @Column(name = "CERTIFICATE_ID")
    private Long certificateId;
    @Size(max = 18)
    @Column(name = "PRODUCT_CODE")
    private String productCode;
    @Size(max = 255)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 6)
    @Column(name = "PRODUCT_GROUP_CODE")
    private String productGroupCode;
    @Size(max = 255)
    @Column(name = "PRODUCT_GROUP_NAME")
    private String productGroupName;
    @Size(max = 255)
    @Column(name = "MANUFACTURER")
    private String manufacturer;
    @Size(max = 255)
    @Column(name = "MANUFACTURER_ADDRESS")
    private String manufacturerAddress;
    @Column(name = "PRODUCT_CHECK_METHOD")
    private Long productCheckMethod;
    @Size(max = 255)
    @Column(name = "CHECK_METHOD_CONFIRM_NO")
    private String checkMethodConfirmNo;
    @Size(max = 255)
    @Column(name = "CONFIRM_ANNOUNCE_NO")
    private String confirmAnnounceNo;
    @Column(name = "PASS")
    private Long pass;
    @Size(max = 2000)
    @Column(name = "REASON")
    private String reason;
    @Column(name = "HANDLING_MEASURES_CODE")
    private Long handlingMeasuresCode;
    @Size(max = 255)
    @Column(name = "HANDLING_MEASURES_NAME")
    private String handlingMeasuresName;
    @Size(max = 2000)
    @Column(name = "NOTE")
    private String note;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;

    public IfdCertificateProduct() {
    }

    public IfdCertificateProduct(Long certificateProductId) {
        this.certificateProductId = certificateProductId;
    }

    public IfdCertificateProduct(Long certificateProductId, Long status, Date createDate, Date updateDate) {
        this.certificateProductId = certificateProductId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getCertificateProductId() {
        return certificateProductId;
    }

    public void setCertificateProductId(Long certificateProductId) {
        this.certificateProductId = certificateProductId;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductGroupCode() {
        return productGroupCode;
    }

    public void setProductGroupCode(String productGroupCode) {
        this.productGroupCode = productGroupCode;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerAddress() {
        return manufacturerAddress;
    }

    public void setManufacturerAddress(String manufacturerAddress) {
        this.manufacturerAddress = manufacturerAddress;
    }

    public Long getProductCheckMethod() {
        return productCheckMethod;
    }

    public void setProductCheckMethod(Long productCheckMethod) {
        this.productCheckMethod = productCheckMethod;
    }

    public String getCheckMethodConfirmNo() {
        return checkMethodConfirmNo;
    }

    public void setCheckMethodConfirmNo(String checkMethodConfirmNo) {
        this.checkMethodConfirmNo = checkMethodConfirmNo;
    }

    public Long getPass() {
        return pass;
    }

    public void setPass(Long pass) {
        this.pass = pass;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getHandlingMeasuresCode() {
        return handlingMeasuresCode;
    }

    public void setHandlingMeasuresCode(Long handlingMeasuresCode) {
        this.handlingMeasuresCode = handlingMeasuresCode;
    }

    public String getHandlingMeasuresName() {
        return handlingMeasuresName;
    }

    public void setHandlingMeasuresName(String handlingMeasuresName) {
        this.handlingMeasuresName = handlingMeasuresName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (certificateProductId != null ? certificateProductId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdCertificateProduct)) {
            return false;
        }
        IfdCertificateProduct other = (IfdCertificateProduct) object;
        if ((this.certificateProductId == null && other.certificateProductId != null) || (this.certificateProductId != null && !this.certificateProductId.equals(other.certificateProductId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdCertificateProduct[ certificateProductId=" + certificateProductId + " ]";
    }

}
