/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_DEPT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdDept.findAll", query = "SELECT i FROM IfdDept i")
    , @NamedQuery(name = "IfdDept.findById", query = "SELECT i FROM IfdDept i WHERE i.id = :id")
    , @NamedQuery(name = "IfdDept.findByDeptcode", query = "SELECT i FROM IfdDept i WHERE i.deptcode = :deptcode")
    , @NamedQuery(name = "IfdDept.findByNumberincre", query = "SELECT i FROM IfdDept i WHERE i.numberincre = :numberincre")
    , @NamedQuery(name = "IfdDept.year", query = "SELECT i FROM IfdDept i WHERE i.year = :year")
    , @NamedQuery(name = "IfdDept.accountNumber", query = "SELECT i FROM IfdDept i WHERE i.accountNumber = :accountNumber")
    , @NamedQuery(name = "IfdDept.bank", query = "SELECT i FROM IfdDept i WHERE i.bank = :bank")})
public class IfdDept implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "IFD_DEPT_SEQ", sequenceName = "IFD_DEPT_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_DEPT_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 20)
    @Column(name = "DEPTCODE")
    private String deptcode;
    @Column(name = "NUMBERINCRE")
    private Long numberincre;
    @Column(name = "YEAR")
    private Long year;
    @Column(name = "ACCOUNT_NUMBER")
    private Long accountNumber;
    @Size(max = 255)
    @Column(name = "BANK")
    private String bank;

    public IfdDept() {
    }
    
    public IfdDept(String deptcode, Long num) {
        this.deptcode = deptcode;
        this.numberincre = num;
    }

    public IfdDept(String deptcode, Long numberincre, Long year) {
        this.deptcode = deptcode;
        this.numberincre = numberincre;
        this.year = year;
    }

    public IfdDept(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public Long getNumberincre() {
        return numberincre;
    }

    public void setNumberincre(Long numberincre) {
        this.numberincre = numberincre;
    }
    
    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdDept)) {
            return false;
        }
        IfdDept other = (IfdDept) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importfood.BO.IfdDept[ id=" + id + " ]";
    }
    
}
