/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_EVALUATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdEvaluation.findAll", query = "SELECT i FROM IfdEvaluation i")
    , @NamedQuery(name = "IfdEvaluation.findByEvaluationId", query = "SELECT i FROM IfdEvaluation i WHERE i.evaluationId = :evaluationId")
    , @NamedQuery(name = "IfdEvaluation.findByEvaluationContent", query = "SELECT i FROM IfdEvaluation i WHERE i.evaluationContent = :evaluationContent")
    , @NamedQuery(name = "IfdEvaluation.findByLeaderId", query = "SELECT i FROM IfdEvaluation i WHERE i.leaderId = :leaderId")
    , @NamedQuery(name = "IfdEvaluation.findByCreateBy", query = "SELECT i FROM IfdEvaluation i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdEvaluation.findByCreateDate", query = "SELECT i FROM IfdEvaluation i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdEvaluation.findByUpdateBy", query = "SELECT i FROM IfdEvaluation i WHERE i.updateBy = :updateBy")
    , @NamedQuery(name = "IfdEvaluation.findByUpdateDate", query = "SELECT i FROM IfdEvaluation i WHERE i.updateDate = :updateDate")})
public class IfdEvaluation implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_EVALUATION_SEQ")
    @SequenceGenerator(sequenceName = "IFD_EVALUATION_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_EVALUATION_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVALUATION_ID")
    private Long evaluationId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 2000)
    @Column(name = "EVALUATION_CONTENT")
    private String evaluationContent;
    @Column(name = "LEADER_ID")
    private Long leaderId;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public IfdEvaluation() {
    }

    public IfdEvaluation(Long evaluationId) {
        this.evaluationId = evaluationId;
    }

    public Long getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(Long evaluationId) {
        this.evaluationId = evaluationId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }
    
    public String getEvaluationContent() {
        return evaluationContent;
    }

    public void setEvaluationContent(String evaluationContent) {
        this.evaluationContent = evaluationContent;
    }

    public Long getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(Long leaderId) {
        this.leaderId = leaderId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }    

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaluationId != null ? evaluationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdEvaluation)) {
            return false;
        }
        IfdEvaluation other = (IfdEvaluation) object;
        if ((this.evaluationId == null && other.evaluationId != null) || (this.evaluationId != null && !this.evaluationId.equals(other.evaluationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importfood.BO.IfdEvaluation[ evaluationId=" + evaluationId + " ]";
    }
    
}
