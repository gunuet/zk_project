/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_EVALUATION_COMMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdEvaluationComment.findAll", query = "SELECT i FROM IfdEvaluationComment i")
    , @NamedQuery(name = "IfdEvaluationComment.findByEvaluationCommentId", query = "SELECT i FROM IfdEvaluationComment i WHERE i.evaluationCommentId = :evaluationCommentId")
    , @NamedQuery(name = "IfdEvaluationComment.findByContentEvaluation", query = "SELECT i FROM IfdEvaluationComment i WHERE i.contentEvaluation = :contentEvaluation")
    , @NamedQuery(name = "IfdEvaluationComment.findByEvaluationId", query = "SELECT i FROM IfdEvaluationComment i WHERE i.evaluationId = :evaluationId")
    , @NamedQuery(name = "IfdEvaluationComment.findByCreateBy", query = "SELECT i FROM IfdEvaluationComment i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdEvaluationComment.findByCreateDate", query = "SELECT i FROM IfdEvaluationComment i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdEvaluationComment.findByUpdateBy", query = "SELECT i FROM IfdEvaluationComment i WHERE i.updateBy = :updateBy")
    , @NamedQuery(name = "IfdEvaluationComment.findByUpdateDate", query = "SELECT i FROM IfdEvaluationComment i WHERE i.updateDate = :updateDate")})
public class IfdEvaluationComment implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_EVALUATION_COMMENT_SEQ")
    @SequenceGenerator(sequenceName = "IFD_EVALUATION_COMMENT_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_EVALUATION_COMMENT_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVALUATION_COMMENT_ID")
    private Long evaluationCommentId;
    @Size(max = 2000)
    @Column(name = "CONTENT_EVALUATION")
    private String contentEvaluation;
    @Column(name = "EVALUATION_ID")
    private Long evaluationId;
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public IfdEvaluationComment() {
    }

    public IfdEvaluationComment(Long evaluationCommentId) {
        this.evaluationCommentId = evaluationCommentId;
    }

    public Long getEvaluationCommentId() {
        return evaluationCommentId;
    }

    public void setEvaluationCommentId(Long evaluationCommentId) {
        this.evaluationCommentId = evaluationCommentId;
    }

    public String getContentEvaluation() {
        return contentEvaluation;
    }

    public void setContentEvaluation(String contentEvaluation) {
        this.contentEvaluation = contentEvaluation;
    }

    public Long getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(Long evaluationId) {
        this.evaluationId = evaluationId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }   

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaluationCommentId != null ? evaluationCommentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdEvaluationComment)) {
            return false;
        }
        IfdEvaluationComment other = (IfdEvaluationComment) object;
        if ((this.evaluationCommentId == null && other.evaluationCommentId != null) || (this.evaluationCommentId != null && !this.evaluationCommentId.equals(other.evaluationCommentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importfood.BO.IfdEvaluationComment[ evaluationCommentId=" + evaluationCommentId + " ]";
    }
    
}
