/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import com.google.gson.Gson;
import com.viettel.module.importfood.Model.IfdEvaluationProductModel;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_EVALUATION_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdEvaluationProduct.findAll", query = "SELECT i FROM IfdEvaluationProduct i")
    , @NamedQuery(name = "IfdEvaluationProduct.findByEvaluationProductId", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.evaluationProductId = :evaluationProductId")
    , @NamedQuery(name = "IfdEvaluationProduct.findByEvaluationId", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.evaluationId = :evaluationId")
    , @NamedQuery(name = "IfdEvaluationProduct.findByProductCode", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.productCode = :productCode")
    , @NamedQuery(name = "IfdEvaluationProduct.findByProductName", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.productName = :productName")
    , @NamedQuery(name = "IfdEvaluationProduct.findByProductGroupCode", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.productGroupCode = :productGroupCode")
    , @NamedQuery(name = "IfdEvaluationProduct.findByProductGroupName", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.productGroupName = :productGroupName")
    , @NamedQuery(name = "IfdEvaluationProduct.findByManufacturer", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.manufacturer = :manufacturer")
    , @NamedQuery(name = "IfdEvaluationProduct.findByManufacturerAddress", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.manufacturerAddress = :manufacturerAddress")
    , @NamedQuery(name = "IfdEvaluationProduct.findByProductCheckMethod", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.productCheckMethod = :productCheckMethod")
    , @NamedQuery(name = "IfdEvaluationProduct.findByCheckMethodConfirmNo", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.checkMethodConfirmNo = :checkMethodConfirmNo")
    
    , @NamedQuery(name = "IfdEvaluationProduct.findByReason", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.reason = :reason")
    , @NamedQuery(name = "IfdEvaluationProduct.findByHandlingMeasuresCode", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.handlingMeasuresCode = :handlingMeasuresCode")
    , @NamedQuery(name = "IfdEvaluationProduct.findByHandlingMeasuresName", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.handlingMeasuresName = :handlingMeasuresName")
    , @NamedQuery(name = "IfdEvaluationProduct.findByNote", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.note = :note")
    , @NamedQuery(name = "IfdEvaluationProduct.findByStatus", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.status = :status")
    , @NamedQuery(name = "IfdEvaluationProduct.findByCreateDate", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdEvaluationProduct.findByCreateBy", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdEvaluationProduct.findByUpdateDate", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdEvaluationProduct.findByUpdateBy", query = "SELECT i FROM IfdEvaluationProduct i WHERE i.updateBy = :updateBy")})
public class IfdEvaluationProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_EVALUATION_PRODUCT_SEQ")
    @SequenceGenerator(sequenceName = "IFD_EVALUATION_PRODUCT_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_EVALUATION_PRODUCT_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVALUATION_PRODUCT_ID")
    private Long evaluationProductId;
    @Column(name = "EVALUATION_ID")
    private Long evaluationId;
    @Size(max = 18)
    @Column(name = "PRODUCT_CODE")
    private String productCode;
    @Size(max = 255)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 6)
    @Column(name = "PRODUCT_GROUP_CODE")
    private String productGroupCode;
    @Size(max = 255)
    @Column(name = "PRODUCT_GROUP_NAME")
    private String productGroupName;
    @Size(max = 255)
    @Column(name = "MANUFACTURER")
    private String manufacturer;
    @Size(max = 255)
    @Column(name = "MANUFACTURER_ADDRESS")
    private String manufacturerAddress;
    @Size(max = 1)
    @Column(name = "PRODUCT_CHECK_METHOD")
    private Long productCheckMethod;
    @Size(max = 255)
    @Column(name = "CHECK_METHOD_CONFIRM_NO")
    private String checkMethodConfirmNo;
    @Size(max = 255)
    @Column(name = "CONFIRM_ANNOUNCE_NO")
    private String confirmAnnounceNo;
    @Size(max = 1)
    @Column(name = "CHECK_RESULT_CODE")
    private Long checkResultCode;
    @Size(max = 255)
    @Column(name = "CHECK_RESULT_NAME")
    private String checkResultName;    
    @Size(max = 2000)
    @Column(name = "REASON")
    private String reason;
    @Size(max = 20)
    @Column(name = "HANDLING_MEASURES_CODE")
    private Long handlingMeasuresCode;
    @Size(max = 255)
    @Column(name = "HANDLING_MEASURES_NAME")
    private String handlingMeasuresName;
    @Size(max = 2000)
    @Column(name = "NOTE")
    private String note;
    @NotNull
    @Size(max = 1)
    @Column(name = "STATUS")
    private Long status;
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;

    public IfdEvaluationProduct() {
    }

    public IfdEvaluationProduct(Long evaluationProductId) {
        this.evaluationProductId = evaluationProductId;
    }

    public Long getEvaluationProductId() {
        return evaluationProductId;
    }

    public void setEvaluationProductId(Long evaluationProductId) {
        this.evaluationProductId = evaluationProductId;
    }

    public Long getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(Long evaluationId) {
        this.evaluationId = evaluationId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductGroupCode() {
        return productGroupCode;
    }

    public void setProductGroupCode(String productGroupCode) {
        this.productGroupCode = productGroupCode;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerAddress() {
        return manufacturerAddress;
    }

    public void setManufacturerAddress(String manufacturerAddress) {
        this.manufacturerAddress = manufacturerAddress;
    }

    public Long getProductCheckMethod() {
        return productCheckMethod;
    }

    public void setProductCheckMethod(Long productCheckMethod) {
        this.productCheckMethod = productCheckMethod;
    }

    public String getCheckMethodConfirmNo() {
        return checkMethodConfirmNo;
    }

    public void setCheckMethodConfirmNo(String checkMethodConfirmNo) {
        this.checkMethodConfirmNo = checkMethodConfirmNo;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo;
    }

    public Long getCheckResultCode() {
        return checkResultCode;
    }

    public void setCheckResultCode(Long checkResultCode) {
        this.checkResultCode = checkResultCode;
    }

    public String getCheckResultName() {
        return checkResultName;
    }

    public void setCheckResultName(String checkResultName) {
        this.checkResultName = checkResultName;
    }    

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getHandlingMeasuresCode() {
        return handlingMeasuresCode;
    }

    public void setHandlingMeasuresCode(Long handlingMeasuresCode) {
        this.handlingMeasuresCode = handlingMeasuresCode;
    }

    public String getHandlingMeasuresName() {
        return handlingMeasuresName;
    }

    public void setHandlingMeasuresName(String handlingMeasuresName) {
        this.handlingMeasuresName = handlingMeasuresName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }
    
    public IfdEvaluationProductModel convertToIfdFileModel(IfdEvaluationProduct ifdFile) {
        IfdEvaluationProductModel result = null;
        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(ifdFile), IfdEvaluationProductModel.class);
        if (result != null) {
           
        }

        return result;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaluationProductId != null ? evaluationProductId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdEvaluationProduct)) {
            return false;
        }
        IfdEvaluationProduct other = (IfdEvaluationProduct) object;
        if ((this.evaluationProductId == null && other.evaluationProductId != null) || (this.evaluationProductId != null && !this.evaluationProductId.equals(other.evaluationProductId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importfood.BO.IfdEvaluationProduct[ evaluationProductId=" + evaluationProductId + " ]";
    }
    
}
