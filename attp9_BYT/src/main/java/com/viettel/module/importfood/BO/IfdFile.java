/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import com.google.gson.Gson;
import com.viettel.module.importfood.Model.IfdFileModel;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_FILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdFile.findAll", query = "SELECT i FROM IfdFile i")
    , @NamedQuery(name = "IfdFile.findByFileId", query = "SELECT i FROM IfdFile i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdFile.findByFileCode", query = "SELECT i FROM IfdFile i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdFile.findByFileCreateDate", query = "SELECT i FROM IfdFile i WHERE i.fileCreateDate = :fileCreateDate")
    , @NamedQuery(name = "IfdFile.findByFileCreateBy", query = "SELECT i FROM IfdFile i WHERE i.fileCreateBy = :fileCreateBy")
    , @NamedQuery(name = "IfdFile.findByFileModifiedDate", query = "SELECT i FROM IfdFile i WHERE i.fileModifiedDate = :fileModifiedDate")
    , @NamedQuery(name = "IfdFile.findByCheckMethod", query = "SELECT i FROM IfdFile i WHERE i.checkMethod = :checkMethod")
    , @NamedQuery(name = "IfdFile.findByGoodsOwnerName", query = "SELECT i FROM IfdFile i WHERE i.goodsOwnerName = :goodsOwnerName")
    , @NamedQuery(name = "IfdFile.findByGoodsOwnerAddress", query = "SELECT i FROM IfdFile i WHERE i.goodsOwnerAddress = :goodsOwnerAddress")
    , @NamedQuery(name = "IfdFile.findByGoodsOwnerPhone", query = "SELECT i FROM IfdFile i WHERE i.goodsOwnerPhone = :goodsOwnerPhone")
    , @NamedQuery(name = "IfdFile.findByGoodsOwnerFax", query = "SELECT i FROM IfdFile i WHERE i.goodsOwnerFax = :goodsOwnerFax")
    , @NamedQuery(name = "IfdFile.findByGoodsOwnerEmail", query = "SELECT i FROM IfdFile i WHERE i.goodsOwnerEmail = :goodsOwnerEmail")
    , @NamedQuery(name = "IfdFile.findByResponesiblePersonName", query = "SELECT i FROM IfdFile i WHERE i.responesiblePersonName = :responesiblePersonName")
    , @NamedQuery(name = "IfdFile.findByResponesiblePersonAddress", query = "SELECT i FROM IfdFile i WHERE i.responesiblePersonAddress = :responesiblePersonAddress")
    , @NamedQuery(name = "IfdFile.findByResponesiblePersonPhone", query = "SELECT i FROM IfdFile i WHERE i.responesiblePersonPhone = :responesiblePersonPhone")
    , @NamedQuery(name = "IfdFile.findByResponesiblePersonFax", query = "SELECT i FROM IfdFile i WHERE i.responesiblePersonFax = :responesiblePersonFax")
    , @NamedQuery(name = "IfdFile.findByResponesiblePersonEmail", query = "SELECT i FROM IfdFile i WHERE i.responesiblePersonEmail = :responesiblePersonEmail")
    , @NamedQuery(name = "IfdFile.findByExporterName", query = "SELECT i FROM IfdFile i WHERE i.exporterName = :exporterName")
    , @NamedQuery(name = "IfdFile.findByExporterAddress", query = "SELECT i FROM IfdFile i WHERE i.exporterAddress = :exporterAddress")
    , @NamedQuery(name = "IfdFile.findByExporterPhone", query = "SELECT i FROM IfdFile i WHERE i.exporterPhone = :exporterPhone")
    , @NamedQuery(name = "IfdFile.findByExporterFax", query = "SELECT i FROM IfdFile i WHERE i.exporterFax = :exporterFax")
    , @NamedQuery(name = "IfdFile.findByExporterEmail", query = "SELECT i FROM IfdFile i WHERE i.exporterEmail = :exporterEmail")
    , @NamedQuery(name = "IfdFile.findByCustomDeclarationNo", query = "SELECT i FROM IfdFile i WHERE i.customDeclarationNo = :customDeclarationNo")
    , @NamedQuery(name = "IfdFile.findByComingDateFrom", query = "SELECT i FROM IfdFile i WHERE i.comingDateFrom = :comingDateFrom")
    , @NamedQuery(name = "IfdFile.findByComingDateTo", query = "SELECT i FROM IfdFile i WHERE i.comingDateTo = :comingDateTo")
    , @NamedQuery(name = "IfdFile.findByExporterGateCode", query = "SELECT i FROM IfdFile i WHERE i.exporterGateCode = :exporterGateCode")
    , @NamedQuery(name = "IfdFile.findByExporterGateName", query = "SELECT i FROM IfdFile i WHERE i.exporterGateName = :exporterGateName")
    , @NamedQuery(name = "IfdFile.findByImporterGateCode", query = "SELECT i FROM IfdFile i WHERE i.importerGateCode = :importerGateCode")
    , @NamedQuery(name = "IfdFile.findByImporterGateName", query = "SELECT i FROM IfdFile i WHERE i.importerGateName = :importerGateName")
    , @NamedQuery(name = "IfdFile.findByCheckTimeFrom", query = "SELECT i FROM IfdFile i WHERE i.checkTimeFrom = :checkTimeFrom")
    , @NamedQuery(name = "IfdFile.findByCheckTimeTo", query = "SELECT i FROM IfdFile i WHERE i.checkTimeTo = :checkTimeTo")
    , @NamedQuery(name = "IfdFile.findByCheckPlace", query = "SELECT i FROM IfdFile i WHERE i.checkPlace = :checkPlace")
    , @NamedQuery(name = "IfdFile.findByCheckDeptCode", query = "SELECT i FROM IfdFile i WHERE i.checkDeptCode = :checkDeptCode")
    , @NamedQuery(name = "IfdFile.findByCheckDeptName", query = "SELECT i FROM IfdFile i WHERE i.checkDeptName = :checkDeptName")
    , @NamedQuery(name = "IfdFile.findByFileStatus", query = "SELECT i FROM IfdFile i WHERE i.fileStatus = :fileStatus")
    , @NamedQuery(name = "IfdFile.findByFileStatusName", query = "SELECT i FROM IfdFile i WHERE i.fileStatusName = :fileStatusName")
    , @NamedQuery(name = "IfdFile.findByStatus", query = "SELECT i FROM IfdFile i WHERE i.status = :status")
    , @NamedQuery(name = "IfdFile.findByIsDelete", query = "SELECT i FROM IfdFile i WHERE i.isDelete = :isDelete")
    , @NamedQuery(name = "IfdFile.findByCreateDate", query = "SELECT i FROM IfdFile i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdFile.findByCreateBy", query = "SELECT i FROM IfdFile i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdFile.findByUpdateDate", query = "SELECT i FROM IfdFile i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdFile.findBySignName", query = "SELECT i FROM IfdFile i WHERE i.signName = :signName")
    , @NamedQuery(name = "IfdFile.findBySignDate", query = "SELECT i FROM IfdFile i WHERE i.signDate = :signDate")
    , @NamedQuery(name = "IfdFile.findByUpdateBy", query = "SELECT i FROM IfdFile i WHERE i.updateBy = :updateBy")})
public class IfdFile implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_FILE_SEQ")
    @SequenceGenerator(sequenceName = "IFD_FILE_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_FILE_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Column(name = "FILE_CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fileCreateDate;
    @Size(max = 14)
    @Column(name = "FILE_CREATE_BY")
    private String fileCreateBy;
    @Column(name = "FILE_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fileModifiedDate;
    @Column(name = "CHECK_METHOD")
    private Long checkMethod;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_NAME")
    private String goodsOwnerName;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_ADDRESS")
    private String goodsOwnerAddress;
    @Size(max = 50)
    @Column(name = "GOODS_OWNER_PHONE")
    private String goodsOwnerPhone;
    @Size(max = 20)
    @Column(name = "GOODS_OWNER_FAX")
    private String goodsOwnerFax;
    @Size(max = 255)
    @Column(name = "GOODS_OWNER_EMAIL")
    private String goodsOwnerEmail;
    @Size(max = 255)
    @Column(name = "RESPONESIBLE_PERSON_NAME")
    private String responesiblePersonName;
    @Size(max = 255)
    @Column(name = "RESPONESIBLE_PERSON_ADDRESS")
    private String responesiblePersonAddress;
    @Size(max = 50)
    @Column(name = "RESPONESIBLE_PERSON_PHONE")
    private String responesiblePersonPhone;
    @Size(max = 20)
    @Column(name = "RESPONESIBLE_PERSON_FAX")
    private String responesiblePersonFax;
    @Size(max = 255)
    @Column(name = "RESPONESIBLE_PERSON_EMAIL")
    private String responesiblePersonEmail;
    @Size(max = 255)
    @Column(name = "EXPORTER_NAME")
    private String exporterName;
    @Size(max = 255)
    @Column(name = "EXPORTER_ADDRESS")
    private String exporterAddress;
    @Size(max = 50)
    @Column(name = "EXPORTER_PHONE")
    private String exporterPhone;
    @Size(max = 20)
    @Column(name = "EXPORTER_FAX")
    private String exporterFax;
    @Size(max = 255)
    @Column(name = "EXPORTER_EMAIL")
    private String exporterEmail;
    @Size(max = 255)
    @Column(name = "CUSTOM_DECLARATION_NO")
    private String customDeclarationNo;
    @Column(name = "COMING_DATE_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comingDateFrom;
    @Column(name = "COMING_DATE_TO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comingDateTo;
    @Size(max = 6)
    @Column(name = "EXPORTER_GATE_CODE")
    private String exporterGateCode;
    @Size(max = 255)
    @Column(name = "EXPORTER_GATE_NAME")
    private String exporterGateName;
    @Size(max = 6)
    @Column(name = "IMPORTER_GATE_CODE")
    private String importerGateCode;
    @Size(max = 255)
    @Column(name = "IMPORTER_GATE_NAME")
    private String importerGateName;
    @Column(name = "CHECK_TIME_FROM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkTimeFrom;
    @Column(name = "CHECK_TIME_TO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date checkTimeTo;
    @Size(max = 255)
    @Column(name = "CHECK_PLACE")
    private String checkPlace;
    @Column(name = "CHECK_DEPT_ID")
    private Long checkDeptId;
    @Size(max = 12)
    @Column(name = "CHECK_DEPT_CODE")
    private String checkDeptCode;
    @Size(max = 255)
    @Column(name = "CHECK_DEPT_NAME")
    private String checkDeptName;
    @Column(name = "FILE_STATUS")
    private Long fileStatus;
    @Size(max = 255)
    @Column(name = "FILE_STATUS_NAME")
    private String fileStatusName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "IS_DELETE")
    private Long isDelete;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;

    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    @Size(max = 100)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Size(max = 100)
    @Column(name = "DOCUMENT_TYPE")
    private String documentType;

    @Transient
    private List<IfdFileProduct> lstProduct;

    @Transient
    private List<IfdAttachment> lstAttach;

    public IfdFile() {
    }

    public IfdFile(Long fileId) {
        this.fileId = fileId;
    }

    public IfdFile(Long fileId, long status, Date createDate, Date updateDate) {
        this.fileId = fileId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Date getFileCreateDate() {
        return fileCreateDate;
    }

    public void setFileCreateDate(Date fileCreateDate) {
        this.fileCreateDate = fileCreateDate;
    }

    public String getFileCreateBy() {
        return fileCreateBy;
    }

    public void setFileCreateBy(String fileCreateBy) {
        this.fileCreateBy = fileCreateBy;
    }

    public Date getFileModifiedDate() {
        return fileModifiedDate;
    }

    public void setFileModifiedDate(Date fileModifiedDate) {
        this.fileModifiedDate = fileModifiedDate;
    }

    public Long getCheckMethod() {
        return checkMethod;
    }

    public void setCheckMethod(Long checkMethod) {
        this.checkMethod = checkMethod;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return goodsOwnerEmail;
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = goodsOwnerEmail;
    }

    public String getResponesiblePersonName() {
        return responesiblePersonName;
    }

    public void setResponesiblePersonName(String responesiblePersonName) {
        this.responesiblePersonName = responesiblePersonName;
    }

    public String getResponesiblePersonAddress() {
        return responesiblePersonAddress;
    }

    public void setResponesiblePersonAddress(String responesiblePersonAddress) {
        this.responesiblePersonAddress = responesiblePersonAddress;
    }

    public String getResponesiblePersonPhone() {
        return responesiblePersonPhone;
    }

    public void setResponesiblePersonPhone(String responesiblePersonPhone) {
        this.responesiblePersonPhone = responesiblePersonPhone;
    }

    public String getResponesiblePersonFax() {
        return responesiblePersonFax;
    }

    public void setResponesiblePersonFax(String responesiblePersonFax) {
        this.responesiblePersonFax = responesiblePersonFax;
    }

    public String getResponesiblePersonEmail() {
        return responesiblePersonEmail;
    }

    public void setResponesiblePersonEmail(String responesiblePersonEmail) {
        this.responesiblePersonEmail = responesiblePersonEmail;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterFax() {
        return exporterFax;
    }

    public void setExporterFax(String exporterFax) {
        this.exporterFax = exporterFax;
    }

    public String getExporterEmail() {
        return exporterEmail;
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = exporterEmail;
    }

    public String getCustomDeclarationNo() {
        return customDeclarationNo;
    }

    public void setCustomDeclarationNo(String customDeclarationNo) {
        this.customDeclarationNo = customDeclarationNo;
    }

    public Date getComingDateFrom() {
        return comingDateFrom;
    }

    public void setComingDateFrom(Date comingDateFrom) {
        this.comingDateFrom = comingDateFrom;
    }

    public Date getComingDateTo() {
        return comingDateTo;
    }

    public void setComingDateTo(Date comingDateTo) {
        this.comingDateTo = comingDateTo;
    }

    public String getExporterGateCode() {
        return exporterGateCode;
    }

    public void setExporterGateCode(String exporterGateCode) {
        this.exporterGateCode = exporterGateCode;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateCode() {
        return importerGateCode;
    }

    public void setImporterGateCode(String importerGateCode) {
        this.importerGateCode = importerGateCode;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public Date getCheckTimeFrom() {
        return checkTimeFrom;
    }

    public void setCheckTimeFrom(Date checkTimeFrom) {
        this.checkTimeFrom = checkTimeFrom;
    }

    public Date getCheckTimeTo() {
        return checkTimeTo;
    }

    public void setCheckTimeTo(Date checkTimeTo) {
        this.checkTimeTo = checkTimeTo;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getCheckDeptCode() {
        return checkDeptCode;
    }

    public void setCheckDeptCode(String checkDeptCode) {
        this.checkDeptCode = checkDeptCode;
    }

    public String getCheckDeptName() {
        return checkDeptName;
    }

    public void setCheckDeptName(String checkDeptName) {
        this.checkDeptName = checkDeptName;
    }

    public Long getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(Long fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getFileStatusName() {
        return fileStatusName;
    }

    public void setFileStatusName(String fileStatusName) {
        this.fileStatusName = fileStatusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(long isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public List<IfdFileProduct> getLstProduct() {
        return lstProduct;
    }

    public void setLstProduct(List<IfdFileProduct> lstProduct) {
        this.lstProduct = lstProduct;
    }

    public List<IfdAttachment> getLstAttach() {
        return lstAttach;
    }

    public void setLstAttach(List<IfdAttachment> lstAttach) {
        this.lstAttach = lstAttach;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public Long getCheckDeptId() {
        return checkDeptId;
    }

    public void setCheckDeptId(Long checkDeptId) {
        this.checkDeptId = checkDeptId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileId != null ? fileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFile)) {
            return false;
        }
        IfdFile other = (IfdFile) object;
        if ((this.fileId == null && other.fileId != null) || (this.fileId != null && !this.fileId.equals(other.fileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFile[ fileId=" + fileId + " ]";
    }

    public IfdFileModel convertToIfdFileModel(IfdFile ifdFile) {
        IfdFileModel result = null;
        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(ifdFile), IfdFileModel.class);
        if (result != null) {
            String commingDateFromTo = "";
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            if (result.getComingDateFrom() != null){
                String strDatefrom = dateFormat.format(result.getComingDateFrom());
                commingDateFromTo += strDatefrom;
            }
            
            if (result.getComingDateTo() != null){
                String strDateTo = dateFormat.format(result.getComingDateTo());
                if (commingDateFromTo != null && !"".equals(commingDateFromTo)){
                    commingDateFromTo = commingDateFromTo + " - " + strDateTo;
                }
            }
            
            result.setCommingDateFromTo(commingDateFromTo);
        }

        return result;
    }

}
