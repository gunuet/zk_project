/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import com.google.gson.Gson;
import com.viettel.module.importfood.Model.IfdFileFeeNoticeModel;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author quannn
 */
@Entity
@Table(name = "IFD_FILE_FEE_NOTICE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdFileFeeNotice.findAll", query = "SELECT i FROM IfdFileFeeNotice i")
    , @NamedQuery(name = "IfdFileFeeNotice.findByFeeNoticeId", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.feeNoticeId = :feeNoticeId")
    , @NamedQuery(name = "IfdFileFeeNotice.findByFileId", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdFileFeeNotice.findByFileCode", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdFileFeeNotice.findByDeptCode", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.deptCode = :deptCode")
    , @NamedQuery(name = "IfdFileFeeNotice.findByDeptName", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.deptName = :deptName")
    , @NamedQuery(name = "IfdFileFeeNotice.findByAccountNumber", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.accountNumber = :accountNumber")
    , @NamedQuery(name = "IfdFileFeeNotice.findByBank", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.bank = :bank")
    , @NamedQuery(name = "IfdFileFeeNotice.findByStatus", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.status = :status")
    , @NamedQuery(name = "IfdFileFeeNotice.findByCreateDate", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdFileFeeNotice.findByCreateBy", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdFileFeeNotice.findByUpdateDate", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdFileFeeNotice.findByUpdateBy", query = "SELECT i FROM IfdFileFeeNotice i WHERE i.updateBy = :updateBy")})
public class IfdFileFeeNotice implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_FILE_FEE_NOTICE_SEQ")
    @SequenceGenerator(sequenceName = "IFD_FILE_FEE_NOTICE_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_FILE_FEE_NOTICE_SEQ")
    @Column(name = "FEE_NOTICE_ID")
    private Long feeNoticeId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 12)
    @Column(name = "DEPT_CODE")
    private String deptCode;
    @Size(max = 255)
    @Column(name = "DEPT_NAME")
    private String deptName;
    @Column(name = "TOTAL_OF_FEE")
    private Long totalOfFee;
    @Column(name = "ACCOUNT_NUMBER")
    private Long accountNumber;
    @Size(max = 255)
    @Column(name = "BANK")
    private String bank;
    @Column(name = "NUMBER_OF_PRODUCT")
    private Long numberOfProduct;
    @Column(name = "FEE_DEFAUT")
    private Long feeDefaut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;

    public IfdFileFeeNotice() {
    }

    public IfdFileFeeNotice(Long feeNoticeId) {
        this.feeNoticeId = feeNoticeId;
    }

    public IfdFileFeeNotice(Long feeNoticeId, Long status, Date createDate, Date updateDate) {
        this.feeNoticeId = feeNoticeId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFeeNoticeId() {
        return feeNoticeId;
    }

    public void setFeeNoticeId(Long feeNoticeId) {
        this.feeNoticeId = feeNoticeId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getTotalOfFee() {
        return totalOfFee;
    }

    public void setTotalOfFee(Long totalOfFee) {
        this.totalOfFee = totalOfFee;
    }

    public Long getNumberOfProduct() {
        return numberOfProduct;
    }

    public void setNumberOfProduct(Long numberOfProduct) {
        this.numberOfProduct = numberOfProduct;
    }

    public Long getFeeDefaut() {
        return feeDefaut;
    }

    public void setFeeDefaut(Long feeDefaut) {
        this.feeDefaut = feeDefaut;
    }


    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }
    
    public IfdFileFeeNoticeModel convertToIfdFileModel(IfdFileFeeNotice ifdFile) {
        IfdFileFeeNoticeModel result = null;
        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(ifdFile), IfdFileFeeNoticeModel.class);

        return result;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (feeNoticeId != null ? feeNoticeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileFeeNotice)) {
            return false;
        }
        IfdFileFeeNotice other = (IfdFileFeeNotice) object;
        if ((this.feeNoticeId == null && other.feeNoticeId != null) || (this.feeNoticeId != null && !this.feeNoticeId.equals(other.feeNoticeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importfood.BO.IfdFileFeeNotice[ feeNoticeId=" + feeNoticeId + " ]";
    }
    
}
