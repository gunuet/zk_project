/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_FILE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdFileHistory.findAll", query = "SELECT i FROM IfdFileHistory i")
    , @NamedQuery(name = "IfdFileHistory.findByFileHistoryId", query = "SELECT i FROM IfdFileHistory i WHERE i.fileHistoryId = :fileHistoryId")
    , @NamedQuery(name = "IfdFileHistory.findByFileId", query = "SELECT i FROM IfdFileHistory i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdFileHistory.findByFileCode", query = "SELECT i FROM IfdFileHistory i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdFileHistory.findBySenderName", query = "SELECT i FROM IfdFileHistory i WHERE i.senderName = :senderName")
    , @NamedQuery(name = "IfdFileHistory.findBySenderDepartment", query = "SELECT i FROM IfdFileHistory i WHERE i.senderDepartment = :senderDepartment")
    , @NamedQuery(name = "IfdFileHistory.findByReceiverName", query = "SELECT i FROM IfdFileHistory i WHERE i.receiverName = :receiverName")
    , @NamedQuery(name = "IfdFileHistory.findByReceiverDepartment", query = "SELECT i FROM IfdFileHistory i WHERE i.receiverDepartment = :receiverDepartment")
    , @NamedQuery(name = "IfdFileHistory.findByContent", query = "SELECT i FROM IfdFileHistory i WHERE i.content = :content")
    , @NamedQuery(name = "IfdFileHistory.findByNote", query = "SELECT i FROM IfdFileHistory i WHERE i.note = :note")
    , @NamedQuery(name = "IfdFileHistory.findByFileResultId", query = "SELECT i FROM IfdFileHistory i WHERE i.fileResultId = :fileResultId")
    , @NamedQuery(name = "IfdFileHistory.findByEndDate", query = "SELECT i FROM IfdFileHistory i WHERE i.endDate = :endDate")
    , @NamedQuery(name = "IfdFileHistory.findByFileStatus", query = "SELECT i FROM IfdFileHistory i WHERE i.fileStatus = :fileStatus")
    , @NamedQuery(name = "IfdFileHistory.findByFileStatusName", query = "SELECT i FROM IfdFileHistory i WHERE i.fileStatusName = :fileStatusName")
    , @NamedQuery(name = "IfdFileHistory.findByStatus", query = "SELECT i FROM IfdFileHistory i WHERE i.status = :status")
    , @NamedQuery(name = "IfdFileHistory.findByCreateDate", query = "SELECT i FROM IfdFileHistory i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdFileHistory.findByCreateBy", query = "SELECT i FROM IfdFileHistory i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdFileHistory.findByUpdateDate", query = "SELECT i FROM IfdFileHistory i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdFileHistory.findByUpdateBy", query = "SELECT i FROM IfdFileHistory i WHERE i.updateBy = :updateBy")})
public class IfdFileHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_FILE_HISTORY_SEQ")
    @SequenceGenerator(sequenceName = "IFD_FILE_HISTORY_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_FILE_HISTORY_SEQ")
    @Column(name = "FILE_HISTORY_ID")
    private Long fileHistoryId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 255)
    @Column(name = "SENDER_NAME")
    private String senderName;
    @Size(max = 255)
    @Column(name = "SENDER_DEPARTMENT")
    private String senderDepartment;
    @Size(max = 255)
    @Column(name = "RECEIVER_NAME")
    private String receiverName;
    @Size(max = 255)
    @Column(name = "RECEIVER_DEPARTMENT")
    private String receiverDepartment;
    @Size(max = 4000)
    @Column(name = "CONTENT")
    private String content;
    @Size(max = 4000)
    @Column(name = "NOTE")
    private String note;
    @Column(name = "FILE_RESULT_ID")
    private Long fileResultId;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "FILE_STATUS")
    private Long fileStatus;
    @Size(max = 255)
    @Column(name = "FILE_STATUS_NAME")
    private String fileStatusName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;

    public IfdFileHistory() {
    }

    public IfdFileHistory(Long fileHistoryId) {
        this.fileHistoryId = fileHistoryId;
    }

    public IfdFileHistory(Long fileHistoryId, Long status, Date createDate, Date updateDate) {
        this.fileHistoryId = fileHistoryId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFileHistoryId() {
        return fileHistoryId;
    }

    public void setFileHistoryId(Long fileHistoryId) {
        this.fileHistoryId = fileHistoryId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderDepartment() {
        return senderDepartment;
    }

    public void setSenderDepartment(String senderDepartment) {
        this.senderDepartment = senderDepartment;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverDepartment() {
        return receiverDepartment;
    }

    public void setReceiverDepartment(String receiverDepartment) {
        this.receiverDepartment = receiverDepartment;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getFileResultId() {
        return fileResultId;
    }

    public void setFileResultId(Long fileResultId) {
        this.fileResultId = fileResultId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(Long fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getFileStatusName() {
        return fileStatusName;
    }

    public void setFileStatusName(String fileStatusName) {
        this.fileStatusName = fileStatusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileHistoryId != null ? fileHistoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileHistory)) {
            return false;
        }
        IfdFileHistory other = (IfdFileHistory) object;
        if ((this.fileHistoryId == null && other.fileHistoryId != null) || (this.fileHistoryId != null && !this.fileHistoryId.equals(other.fileHistoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFileHistory[ fileHistoryId=" + fileHistoryId + " ]";
    }
    
}
