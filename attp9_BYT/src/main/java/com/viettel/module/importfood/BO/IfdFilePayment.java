/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import com.google.gson.Gson;
import com.viettel.module.importfood.Model.IfdFilePaymentModel;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_FILE_PAYMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdFilePayment.findAll", query = "SELECT i FROM IfdFilePayment i")
    , @NamedQuery(name = "IfdFilePayment.findByPaymentId", query = "SELECT i FROM IfdFilePayment i WHERE i.paymentId = :paymentId")
    , @NamedQuery(name = "IfdFilePayment.findByFileId", query = "SELECT i FROM IfdFilePayment i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdFilePayment.findByFileCode", query = "SELECT i FROM IfdFilePayment i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdFilePayment.findByProcessDate", query = "SELECT i FROM IfdFilePayment i WHERE i.processDate = :processDate")
    , @NamedQuery(name = "IfdFilePayment.findByProcessName", query = "SELECT i FROM IfdFilePayment i WHERE i.processName = :processName")
    , @NamedQuery(name = "IfdFilePayment.findByPaymentType", query = "SELECT i FROM IfdFilePayment i WHERE i.paymentType = :paymentType")
    , @NamedQuery(name = "IfdFilePayment.findByCost", query = "SELECT i FROM IfdFilePayment i WHERE i.cost = :cost")
    , @NamedQuery(name = "IfdFilePayment.findByBankNo", query = "SELECT i FROM IfdFilePayment i WHERE i.bankNo = :bankNo")
    , @NamedQuery(name = "IfdFilePayment.findByDeptCode", query = "SELECT i FROM IfdFilePayment i WHERE i.deptCode = :deptCode")
    , @NamedQuery(name = "IfdFilePayment.findByDeptName", query = "SELECT i FROM IfdFilePayment i WHERE i.deptName = :deptName")
    , @NamedQuery(name = "IfdFilePayment.findByStatus", query = "SELECT i FROM IfdFilePayment i WHERE i.status = :status")
    , @NamedQuery(name = "IfdFilePayment.findByCreateDate", query = "SELECT i FROM IfdFilePayment i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdFilePayment.findByCreateBy", query = "SELECT i FROM IfdFilePayment i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdFilePayment.findByUpdateDate", query = "SELECT i FROM IfdFilePayment i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdFilePayment.findByUpdateBy", query = "SELECT i FROM IfdFilePayment i WHERE i.updateBy = :updateBy")})
public class IfdFilePayment implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_FILE_PAYMENT_SEQ")
    @SequenceGenerator(sequenceName = "IFD_FILE_PAYMENT_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_FILE_PAYMENT_SEQ")
    @Column(name = "PAYMENT_ID")
    private Long paymentId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Column(name = "PROCESS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processDate;
    @Size(max = 50)
    @Column(name = "PROCESS_NAME")
    private String processName;
    @Column(name = "PAYMENT_TYPE")
    private Long paymentType;
    @Column(name = "COST")
    private Double cost;
    @Size(max = 50)
    @Column(name = "BANK_NO")
    private String bankNo;
    @Size(max = 12)
    @Column(name = "DEPT_CODE")
    private String deptCode;
    @Size(max = 50)
    @Column(name = "DEPT_NAME")
    private String deptName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    @Size(max = 20)
    @Column(name = "ONL_PAYMENT_DEPT_CODE")
    private String onlPaymentDeptCode;
    @Size(max = 500)
    @Column(name = "ONL_PAYMENT_DEPT_NAME")
    private String onlPaymentDeptName;
    
    @Transient
    private IfdAttachment attach;

    public IfdFilePayment() {
    }

    public IfdFilePayment(Long paymentId) {
        this.paymentId = paymentId;
    }

    public IfdFilePayment(Long paymentId, Long status, Date createDate, Date updateDate) {
        this.paymentId = paymentId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Long getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Long paymentType) {
        this.paymentType = paymentType;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public IfdAttachment getAttach() {
        return attach;
    }

    public void setAttach(IfdAttachment attach) {
        this.attach = attach;
    }

    public String getOnlPaymentDeptCode() {
        return onlPaymentDeptCode;
    }

    public void setOnlPaymentDeptCode(String onlPaymentDeptCode) {
        this.onlPaymentDeptCode = onlPaymentDeptCode;
    }

    public String getOnlPaymentDeptName() {
        return onlPaymentDeptName;
    }

    public void setOnlPaymentDeptName(String onlPaymentDeptName) {
        this.onlPaymentDeptName = onlPaymentDeptName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paymentId != null ? paymentId.hashCode() : 0);
        return hash;
    }
    
    public IfdFilePaymentModel convertToIfdFileModel(IfdFilePayment ifdFilePayent) {
        IfdFilePaymentModel result = null;
        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(ifdFilePayent), IfdFilePaymentModel.class);
        if (result != null) {
            int type = Integer.parseInt(result.getPaymentType()+"");
            switch(type){
                case 1:
                    result.setPaymentTypeName("Nộp tiền mặt");
                    break;
                case 2:
                    result.setPaymentTypeName("Chuyển khoản");
                    break;
                case 3:
                    result.setPaymentTypeName("Thanh toán trực tuyến");
            }            
        }

        return result;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFilePayment)) {
            return false;
        }
        IfdFilePayment other = (IfdFilePayment) object;
        if ((this.paymentId == null && other.paymentId != null) || (this.paymentId != null && !this.paymentId.equals(other.paymentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFilePayment[ paymentId=" + paymentId + " ]";
    }
    
}
