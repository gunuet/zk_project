/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author quannn
 */
@Entity
@Table(name = "IFD_FILE_PROCESS_USER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdFileProcessUser.findAll", query = "SELECT i FROM IfdFileProcessUser i")
    , @NamedQuery(name = "IfdFileProcessUser.findByFileProcessUserId", query = "SELECT i FROM IfdFileProcessUser i WHERE i.fileProcessUserId = :fileProcessUserId")
    , @NamedQuery(name = "IfdFileProcessUser.findByFileId", query = "SELECT i FROM IfdFileProcessUser i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdFileProcessUser.findBySendUserId", query = "SELECT i FROM IfdFileProcessUser i WHERE i.sendUserId = :sendUserId")
    , @NamedQuery(name = "IfdFileProcessUser.findByReceiveUserId", query = "SELECT i FROM IfdFileProcessUser i WHERE i.receiveUserId = :receiveUserId")
    , @NamedQuery(name = "IfdFileProcessUser.findBySendDeptId", query = "SELECT i FROM IfdFileProcessUser i WHERE i.sendDeptId = :sendDeptId")
    , @NamedQuery(name = "IfdFileProcessUser.findBySendDeptCode", query = "SELECT i FROM IfdFileProcessUser i WHERE i.sendDeptCode = :sendDeptCode")
    , @NamedQuery(name = "IfdFileProcessUser.findByReceiveDeptId", query = "SELECT i FROM IfdFileProcessUser i WHERE i.receiveDeptId = :receiveDeptId")
    , @NamedQuery(name = "IfdFileProcessUser.findByReceiveDeptCode", query = "SELECT i FROM IfdFileProcessUser i WHERE i.receiveDeptCode = :receiveDeptCode")
    , @NamedQuery(name = "IfdFileProcessUser.findBySendRoleId", query = "SELECT i FROM IfdFileProcessUser i WHERE i.sendRoleId = :sendRoleId")
    , @NamedQuery(name = "IfdFileProcessUser.findBySendRoleCode", query = "SELECT i FROM IfdFileProcessUser i WHERE i.sendRoleCode = :sendRoleCode")
    , @NamedQuery(name = "IfdFileProcessUser.findByReceiveRoleId", query = "SELECT i FROM IfdFileProcessUser i WHERE i.receiveRoleId = :receiveRoleId")
    , @NamedQuery(name = "IfdFileProcessUser.findByReceiveRoleCode", query = "SELECT i FROM IfdFileProcessUser i WHERE i.receiveRoleCode = :receiveRoleCode")
    , @NamedQuery(name = "IfdFileProcessUser.findByStatus", query = "SELECT i FROM IfdFileProcessUser i WHERE i.status = :status")
    , @NamedQuery(name = "IfdFileProcessUser.findByCreateDate", query = "SELECT i FROM IfdFileProcessUser i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdFileProcessUser.findByCreateBy", query = "SELECT i FROM IfdFileProcessUser i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdFileProcessUser.findByUpdateDate", query = "SELECT i FROM IfdFileProcessUser i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdFileProcessUser.findByUpdateBy", query = "SELECT i FROM IfdFileProcessUser i WHERE i.updateBy = :updateBy")})
public class IfdFileProcessUser implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_FILE_PROCESS_USER_SEQ")
    @SequenceGenerator(sequenceName = "IFD_FILE_PROCESS_USER_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_FILE_PROCESS_USER_SEQ")
    @Column(name = "FILE_PROCESS_USER_ID")
    private Long fileProcessUserId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_ID")
    private Long fileId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_USER_ID")
    private Long sendUserId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RECEIVE_USER_ID")
    private Long receiveUserId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_DEPT_ID")
    private Long sendDeptId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SEND_DEPT_CODE")
    private String sendDeptCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RECEIVE_DEPT_ID")
    private Long receiveDeptId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RECEIVE_DEPT_CODE")
    private String receiveDeptCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_ROLE_ID")
    private Long sendRoleId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "SEND_ROLE_CODE")
    private String sendRoleCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RECEIVE_ROLE_ID")
    private Long receiveRoleId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RECEIVE_ROLE_CODE")
    private String receiveRoleCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_BY")
    private Long updateBy;

    public IfdFileProcessUser() {
    }

    public IfdFileProcessUser(Long fileProcessUserId) {
        this.fileProcessUserId = fileProcessUserId;
    }

    public IfdFileProcessUser(Long fileProcessUserId, Long fileId, Long sendUserId, Long receiveUserId, Long sendDeptId, String sendDeptCode, Long receiveDeptId, String receiveDeptCode, Long sendRoleId, String sendRoleCode, Long receiveRoleId, String receiveRoleCode, Long status, Long createBy, Long updateBy) {
        this.fileProcessUserId = fileProcessUserId;
        this.fileId = fileId;
        this.sendUserId = sendUserId;
        this.receiveUserId = receiveUserId;
        this.sendDeptId = sendDeptId;
        this.sendDeptCode = sendDeptCode;
        this.receiveDeptId = receiveDeptId;
        this.receiveDeptCode = receiveDeptCode;
        this.sendRoleId = sendRoleId;
        this.sendRoleCode = sendRoleCode;
        this.receiveRoleId = receiveRoleId;
        this.receiveRoleCode = receiveRoleCode;
        this.status = status;
        this.createBy = createBy;
        this.updateBy = updateBy;
    }

    public Long getFileProcessUserId() {
        return fileProcessUserId;
    }

    public void setFileProcessUserId(Long fileProcessUserId) {
        this.fileProcessUserId = fileProcessUserId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(Long sendUserId) {
        this.sendUserId = sendUserId;
    }

    public Long getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(Long receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public Long getSendDeptId() {
        return sendDeptId;
    }

    public void setSendDeptId(Long sendDeptId) {
        this.sendDeptId = sendDeptId;
    }

    public String getSendDeptCode() {
        return sendDeptCode;
    }

    public void setSendDeptCode(String sendDeptCode) {
        this.sendDeptCode = sendDeptCode;
    }

    public Long getReceiveDeptId() {
        return receiveDeptId;
    }

    public void setReceiveDeptId(Long receiveDeptId) {
        this.receiveDeptId = receiveDeptId;
    }

    public String getReceiveDeptCode() {
        return receiveDeptCode;
    }

    public void setReceiveDeptCode(String receiveDeptCode) {
        this.receiveDeptCode = receiveDeptCode;
    }

    public Long getSendRoleId() {
        return sendRoleId;
    }

    public void setSendRoleId(Long sendRoleId) {
        this.sendRoleId = sendRoleId;
    }

    public String getSendRoleCode() {
        return sendRoleCode;
    }

    public void setSendRoleCode(String sendRoleCode) {
        this.sendRoleCode = sendRoleCode;
    }

    public Long getReceiveRoleId() {
        return receiveRoleId;
    }

    public void setReceiveRoleId(Long receiveRoleId) {
        this.receiveRoleId = receiveRoleId;
    }

    public String getReceiveRoleCode() {
        return receiveRoleCode;
    }

    public void setReceiveRoleCode(String receiveRoleCode) {
        this.receiveRoleCode = receiveRoleCode;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileProcessUserId != null ? fileProcessUserId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileProcessUser)) {
            return false;
        }
        IfdFileProcessUser other = (IfdFileProcessUser) object;
        if ((this.fileProcessUserId == null && other.fileProcessUserId != null) || (this.fileProcessUserId != null && !this.fileProcessUserId.equals(other.fileProcessUserId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importfood.BO.IfdFileProcessUser[ fileProcessUserId=" + fileProcessUserId + " ]";
    }
    
}
