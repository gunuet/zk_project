/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_FILE_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdFileProduct.findAll", query = "SELECT i FROM IfdFileProduct i")
    , @NamedQuery(name = "IfdFileProduct.findByFileProductId", query = "SELECT i FROM IfdFileProduct i WHERE i.fileProductId = :fileProductId")
    , @NamedQuery(name = "IfdFileProduct.findByFileId", query = "SELECT i FROM IfdFileProduct i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdFileProduct.findByFileCode", query = "SELECT i FROM IfdFileProduct i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdFileProduct.findByProductCode", query = "SELECT i FROM IfdFileProduct i WHERE i.productCode = :productCode")
    , @NamedQuery(name = "IfdFileProduct.findByProductName", query = "SELECT i FROM IfdFileProduct i WHERE i.productName = :productName")
    , @NamedQuery(name = "IfdFileProduct.findByProductGroupCode", query = "SELECT i FROM IfdFileProduct i WHERE i.productGroupCode = :productGroupCode")
    , @NamedQuery(name = "IfdFileProduct.findByProductGroupName", query = "SELECT i FROM IfdFileProduct i WHERE i.productGroupName = :productGroupName")
    , @NamedQuery(name = "IfdFileProduct.findByManufacturer", query = "SELECT i FROM IfdFileProduct i WHERE i.manufacturer = :manufacturer")
    , @NamedQuery(name = "IfdFileProduct.findByManufacturerAddress", query = "SELECT i FROM IfdFileProduct i WHERE i.manufacturerAddress = :manufacturerAddress")
    , @NamedQuery(name = "IfdFileProduct.findByProductCheckMethod", query = "SELECT i FROM IfdFileProduct i WHERE i.productCheckMethod = :productCheckMethod")
    , @NamedQuery(name = "IfdFileProduct.findByCheckMethodConfirmNo", query = "SELECT i FROM IfdFileProduct i WHERE i.checkMethodConfirmNo = :checkMethodConfirmNo")
    , @NamedQuery(name = "IfdFileProduct.findByConfirmAnnounceNo", query = "SELECT i FROM IfdFileProduct i WHERE i.confirmAnnounceNo = :confirmAnnounceNo")
    , @NamedQuery(name = "IfdFileProduct.findByStatus", query = "SELECT i FROM IfdFileProduct i WHERE i.status = :status")
    , @NamedQuery(name = "IfdFileProduct.findByCreateDate", query = "SELECT i FROM IfdFileProduct i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdFileProduct.findByCreateBy", query = "SELECT i FROM IfdFileProduct i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdFileProduct.findByUpdateDate", query = "SELECT i FROM IfdFileProduct i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdFileProduct.findByUpdateBy", query = "SELECT i FROM IfdFileProduct i WHERE i.updateBy = :updateBy")})
public class IfdFileProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_FILE_PRODUCT_SEQ")
    @SequenceGenerator(sequenceName = "IFD_FILE_PRODUCT_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_FILE_PRODUCT_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_PRODUCT_ID")
    private Long fileProductId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 18)
    @Column(name = "PRODUCT_CODE")
    private String productCode;
    @Size(max = 255)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Size(max = 6)
    @Column(name = "PRODUCT_GROUP_CODE")
    private String productGroupCode;
    @Size(max = 255)
    @Column(name = "PRODUCT_GROUP_NAME")
    private String productGroupName;
    @Size(max = 255)
    @Column(name = "MANUFACTURER")
    private String manufacturer;
    @Size(max = 255)
    @Column(name = "MANUFACTURER_ADDRESS")
    private String manufacturerAddress;
    @Column(name = "PRODUCT_CHECK_METHOD")
    private Long productCheckMethod;
    @Size(max = 255)
    @Column(name = "CHECK_METHOD_CONFIRM_NO")
    private String checkMethodConfirmNo;
    @Size(max = 255)
    @Column(name = "CONFIRM_ANNOUNCE_NO")
    private String confirmAnnounceNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    
    @Transient
    private IfdAttachment attach;

    public IfdFileProduct() {
    }

    public IfdFileProduct(Long fileProductId) {
        this.fileProductId = fileProductId;
    }

    public IfdFileProduct(Long fileProductId, Long status, Date createDate, Date updateDate) {
        this.fileProductId = fileProductId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFileProductId() {
        return fileProductId;
    }

    public void setFileProductId(Long fileProductId) {
        this.fileProductId = fileProductId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductGroupCode() {
        return productGroupCode;
    }

    public void setProductGroupCode(String productGroupCode) {
        this.productGroupCode = productGroupCode;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerAddress() {
        return manufacturerAddress;
    }

    public void setManufacturerAddress(String manufacturerAddress) {
        this.manufacturerAddress = manufacturerAddress;
    }

    public Long getProductCheckMethod() {
        return productCheckMethod;
    }

    public void setProductCheckMethod(Long productCheckMethod) {
        this.productCheckMethod = productCheckMethod;
    }

    public String getCheckMethodConfirmNo() {
        return checkMethodConfirmNo;
    }

    public void setCheckMethodConfirmNo(String checkMethodConfirmNo) {
        this.checkMethodConfirmNo = checkMethodConfirmNo;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public IfdAttachment getAttach() {
        return attach;
    }

    public void setAttach(IfdAttachment attach) {
        this.attach = attach;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileProductId != null ? fileProductId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileProduct)) {
            return false;
        }
        IfdFileProduct other = (IfdFileProduct) object;
        if ((this.fileProductId == null && other.fileProductId != null) || (this.fileProductId != null && !this.fileProductId.equals(other.fileProductId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFileProduct[ fileProductId=" + fileProductId + " ]";
    }

}
