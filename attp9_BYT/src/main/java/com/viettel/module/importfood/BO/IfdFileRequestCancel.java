/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_FILE_REQUEST_CANCEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdFileRequestCancel.findAll", query = "SELECT i FROM IfdFileRequestCancel i")
    , @NamedQuery(name = "IfdFileRequestCancel.findByFileRequestCancelId", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.fileRequestCancelId = :fileRequestCancelId")
    , @NamedQuery(name = "IfdFileRequestCancel.findByFileId", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdFileRequestCancel.findByFileCode", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdFileRequestCancel.findByCancelDate", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.cancelDate = :cancelDate")
    , @NamedQuery(name = "IfdFileRequestCancel.findByStatus", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.status = :status")
    , @NamedQuery(name = "IfdFileRequestCancel.findByCreateDate", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdFileRequestCancel.findByCreateBy", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdFileRequestCancel.findByUpdateDate", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdFileRequestCancel.findByUpdateBy", query = "SELECT i FROM IfdFileRequestCancel i WHERE i.updateBy = :updateBy")})
public class IfdFileRequestCancel implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_FILE_REQUEST_CANCEL_SEQ")
    @SequenceGenerator(sequenceName = "IFD_FILE_REQUEST_CANCEL_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_FILE_REQUEST_CANCEL_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_REQUEST_CANCEL_ID")
    private Long fileRequestCancelId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Column(name = "CANCEL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Size(max = 500)
    @Column(name = "REASON")
    private String reason;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;

    public IfdFileRequestCancel() {
    }

    public IfdFileRequestCancel(Long fileRequestCancelId) {
        this.fileRequestCancelId = fileRequestCancelId;
    }

    public IfdFileRequestCancel(Long fileRequestCancelId, Long status, Date createDate, Date updateDate) {
        this.fileRequestCancelId = fileRequestCancelId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFileRequestCancelId() {
        return fileRequestCancelId;
    }

    public void setFileRequestCancelId(Long fileRequestCancelId) {
        this.fileRequestCancelId = fileRequestCancelId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileRequestCancelId != null ? fileRequestCancelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileRequestCancel)) {
            return false;
        }
        IfdFileRequestCancel other = (IfdFileRequestCancel) object;
        if ((this.fileRequestCancelId == null && other.fileRequestCancelId != null) || (this.fileRequestCancelId != null && !this.fileRequestCancelId.equals(other.fileRequestCancelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFileRequestCancel[ fileRequestCancelId=" + fileRequestCancelId + " ]";
    }

}
