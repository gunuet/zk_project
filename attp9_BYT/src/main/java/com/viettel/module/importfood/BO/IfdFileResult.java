/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_FILE_RESULT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdFileResult.findAll", query = "SELECT i FROM IfdFileResult i")
    , @NamedQuery(name = "IfdFileResult.findByFileResultId", query = "SELECT i FROM IfdFileResult i WHERE i.fileResultId = :fileResultId")
    , @NamedQuery(name = "IfdFileResult.findByFileId", query = "SELECT i FROM IfdFileResult i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdFileResult.findByFileCode", query = "SELECT i FROM IfdFileResult i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdFileResult.findByDeptCode", query = "SELECT i FROM IfdFileResult i WHERE i.deptCode = :deptCode")
    , @NamedQuery(name = "IfdFileResult.findByDeptName", query = "SELECT i FROM IfdFileResult i WHERE i.deptName = :deptName")
    , @NamedQuery(name = "IfdFileResult.findByProcessDate", query = "SELECT i FROM IfdFileResult i WHERE i.processDate = :processDate")
    , @NamedQuery(name = "IfdFileResult.findByProcessName", query = "SELECT i FROM IfdFileResult i WHERE i.processName = :processName")
    , @NamedQuery(name = "IfdFileResult.findByProcessContent", query = "SELECT i FROM IfdFileResult i WHERE i.processContent = :processContent")
    , @NamedQuery(name = "IfdFileResult.findByReceiveNo", query = "SELECT i FROM IfdFileResult i WHERE i.receiveNo = :receiveNo")
    , @NamedQuery(name = "IfdFileResult.findByFileStatus", query = "SELECT i FROM IfdFileResult i WHERE i.fileStatus = :fileStatus")
    , @NamedQuery(name = "IfdFileResult.findByFileStatusName", query = "SELECT i FROM IfdFileResult i WHERE i.fileStatusName = :fileStatusName")
    , @NamedQuery(name = "IfdFileResult.findByStatus", query = "SELECT i FROM IfdFileResult i WHERE i.status = :status")
    , @NamedQuery(name = "IfdFileResult.findByCreateDate", query = "SELECT i FROM IfdFileResult i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdFileResult.findByCreateBy", query = "SELECT i FROM IfdFileResult i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdFileResult.findByUpdateDate", query = "SELECT i FROM IfdFileResult i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdFileResult.findByUpdateBy", query = "SELECT i FROM IfdFileResult i WHERE i.updateBy = :updateBy")})
public class IfdFileResult implements Serializable {

    private static final Long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_FILE_RESULT_SEQ")
    @SequenceGenerator(sequenceName = "IFD_FILE_RESULT_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_FILE_RESULT_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_RESULT_ID")
    private Long fileResultId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 20)
    @Column(name = "DEPT_CODE")
    private String deptCode;
    @Size(max = 100)
    @Column(name = "DEPT_NAME")
    private String deptName;
    @Column(name = "PROCESS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processDate;
    @Size(max = 50)
    @Column(name = "PROCESS_NAME")
    private String processName;
    @Size(max = 2000)
    @Column(name = "PROCESS_CONTENT")
    private String processContent;
    @Size(max = 250)
    @Column(name = "RECEIVE_NO")
    private String receiveNo;
    @Column(name = "FILE_STATUS")
    private Long fileStatus;
    @Size(max = 255)
    @Column(name = "FILE_STATUS_NAME")
    private String fileStatusName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    
    @Transient
    private IfdAttachment attach;

    public IfdFileResult() {
    }

    public IfdFileResult(Long fileResultId) {
        this.fileResultId = fileResultId;
    }

    public IfdFileResult(Long fileResultId, Long status, Date createDate, Date updateDate) {
        this.fileResultId = fileResultId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFileResultId() {
        return fileResultId;
    }

    public void setFileResultId(Long fileResultId) {
        this.fileResultId = fileResultId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessContent() {
        return processContent;
    }

    public void setProcessContent(String processContent) {
        this.processContent = processContent;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public Long getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(Long fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getFileStatusName() {
        return fileStatusName;
    }

    public void setFileStatusName(String fileStatusName) {
        this.fileStatusName = fileStatusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public IfdAttachment getAttach() {
        return attach;
    }

    public void setAttach(IfdAttachment attach) {
        this.attach = attach;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileResultId != null ? fileResultId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileResult)) {
            return false;
        }
        IfdFileResult other = (IfdFileResult) object;
        if ((this.fileResultId == null && other.fileResultId != null) || (this.fileResultId != null && !this.fileResultId.equals(other.fileResultId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFileResult[ fileResultId=" + fileResultId + " ]";
    }
    
}
