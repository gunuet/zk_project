/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.BO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "IFD_HANDLING_REPORT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IfdHandlingReport.findAll", query = "SELECT i FROM IfdHandlingReport i")
    , @NamedQuery(name = "IfdHandlingReport.findByHandlingReportId", query = "SELECT i FROM IfdHandlingReport i WHERE i.handlingReportId = :handlingReportId")
    , @NamedQuery(name = "IfdHandlingReport.findByFileId", query = "SELECT i FROM IfdHandlingReport i WHERE i.fileId = :fileId")
    , @NamedQuery(name = "IfdHandlingReport.findByFileCode", query = "SELECT i FROM IfdHandlingReport i WHERE i.fileCode = :fileCode")
    , @NamedQuery(name = "IfdHandlingReport.findByContent", query = "SELECT i FROM IfdHandlingReport i WHERE i.content = :content")
    , @NamedQuery(name = "IfdHandlingReport.findByStatus", query = "SELECT i FROM IfdHandlingReport i WHERE i.status = :status")
    , @NamedQuery(name = "IfdHandlingReport.findByCreateDate", query = "SELECT i FROM IfdHandlingReport i WHERE i.createDate = :createDate")
    , @NamedQuery(name = "IfdHandlingReport.findByCreateBy", query = "SELECT i FROM IfdHandlingReport i WHERE i.createBy = :createBy")
    , @NamedQuery(name = "IfdHandlingReport.findByUpdateDate", query = "SELECT i FROM IfdHandlingReport i WHERE i.updateDate = :updateDate")
    , @NamedQuery(name = "IfdHandlingReport.findByUpdateBy", query = "SELECT i FROM IfdHandlingReport i WHERE i.updateBy = :updateBy")})
public class IfdHandlingReport implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IFD_HANDLING_REPORT_SEQ")
    @SequenceGenerator(sequenceName = "IFD_HANDLING_REPORT_SEQ", schema = "BYT_ATTP_THAT2", initialValue = 1, allocationSize = 1, name = "IFD_HANDLING_REPORT_SEQ")
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "HANDLING_REPORT_ID")
    private Long handlingReportId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 35)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 2000)
    @Column(name = "CONTENT")
    private String content;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Long status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CREATE_BY")
    private Long createBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY")
    private Long updateBy;
    
    @Transient
    private List<IfdAttachment> lstAttachs;

    public IfdHandlingReport() {
    }

    public IfdHandlingReport(Long handlingReportId) {
        this.handlingReportId = handlingReportId;
    }

    public IfdHandlingReport(Long handlingReportId, Long status, Date createDate, Date updateDate) {
        this.handlingReportId = handlingReportId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getHandlingReportId() {
        return handlingReportId;
    }

    public void setHandlingReportId(Long handlingReportId) {
        this.handlingReportId = handlingReportId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public List<IfdAttachment> getLstAttachs() {
        return lstAttachs;
    }

    public void setLstAttachs(List<IfdAttachment> lstAttachs) {
        this.lstAttachs = lstAttachs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (handlingReportId != null ? handlingReportId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdHandlingReport)) {
            return false;
        }
        IfdHandlingReport other = (IfdHandlingReport) object;
        if ((this.handlingReportId == null && other.handlingReportId != null) || (this.handlingReportId != null && !this.handlingReportId.equals(other.handlingReportId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdHandlingReport[ handlingReportId=" + handlingReportId + " ]";
    }
    
}
