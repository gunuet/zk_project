/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Constant;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Tien update FileStatusName
 */
public enum FileStatus {
    CHON(Long.valueOf("-1"), "---Chọn---"),
    HO_SO_GUI_MOI_CHO_AP_PHI(Long.valueOf("1"), "Hồ sơ chờ áp phí"),
    HO_SO_GUI_SUA_CHO_AP_PHI(Long.valueOf("2"), "Hồ sơ gửi sửa"), //trạng thái hồ sơ lấy theo trạng thái hiện tại
    DA_GUI_THONG_BAO_AP_PHI(Long.valueOf("3"), "Đã gửi thông báo áp phí"),
    HO_SO_CHO_XAC_NHAN_PHI(Long.valueOf("4"), "Hồ sơ chờ xác nhận phí"),
    YEU_CAU_NOP_LAI_PHI(Long.valueOf("5"), "Yêu cầu nộp lại phí"),
    HO_SO_GUI_BO_SUNG_PHI(Long.valueOf("6"), "Hồ sơ gửi bổ sung phí"),
    HO_SO_CHO_PHAN_CONG(Long.valueOf("7"), "Hồ sơ chờ phân công"),
    HO_SO_CHO_XU_LY(Long.valueOf("8"), "Hồ sơ chờ xử lý"),
    HO_SO_CHO_XEM_XET(Long.valueOf("9"), "Hồ sơ chờ xem xét"),
    HO_SO_CHO_PHE_DUYET(Long.valueOf("10"), "Hồ sơ chờ phê duyệt"),
    LDP_YEU_CAU_XU_LY_LAI(Long.valueOf("11"), "LĐP yêu cầu xử lý lại"),
    HO_SO_YEU_CAU_SUA_DOI_BO_SUNG(Long.valueOf("12"), "Hồ sơ yêu cầu sửa đổi bổ sung"),
    HO_SO_DA_TRA_KET_QUA(Long.valueOf("13"), "Hồ sơ đã trả kết quả"),
    HO_SO_TU_CHOI_CAP_PHEP(Long.valueOf("14"), "Hồ sơ từ chối cấp phép"),
    LDCQ_YEU_CAU_XU_LY_LAI(Long.valueOf("15"), "LĐCQ yêu cầu xử lý lại"),
    HO_SO_GUI_SUA_DOI_BO_SUNG(Long.valueOf("16"), "Hồ sơ gửi sửa đổi bổ sung"),
    HO_SO_DA_GUI_BAO_CAO_XU_LY(Long.valueOf("17"), "Hồ sơ đã gửi báo cáo xử lý"),
    HO_SO_DA_RUT(Long.valueOf("18"), "Hồ sơ đã rút"),;

    private Long status;
    private String statusName;

    private FileStatus(Long status, String statusName) {
        this.status = status;
        this.statusName = statusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public static FileStatus getByStatus(Long status) {
        for (FileStatus statusObject : FileStatus.values()) {
            if (Objects.equals(statusObject.getStatus(), status)) {
                return statusObject;
            }
        }
        return null;
    }

    /**
     * Trả về list fileStatus theo roleCode cho màn hình chờ xử lý
     *
     * @param roleCode
     * @return
     */
    public static List<Long> getListFileStatusByRole(RoleCodeType roleCode) {
        List<Long> lstFileStatus = new ArrayList<>();
        switch (roleCode) {
            case ATTP_KTNN_KT:
                lstFileStatus.add(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatus());
                lstFileStatus.add(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus());
                break;
            case ATTP_KTNN_CV:
                lstFileStatus.add(FileStatus.HO_SO_CHO_XU_LY.getStatus());
                lstFileStatus.add(FileStatus.HO_SO_GUI_SUA_DOI_BO_SUNG.getStatus());
                lstFileStatus.add(FileStatus.LDP_YEU_CAU_XU_LY_LAI.getStatus());
                lstFileStatus.add(FileStatus.LDCQ_YEU_CAU_XU_LY_LAI.getStatus());
                break;
            case ATTP_KTNN_LĐP:
                lstFileStatus.add(FileStatus.HO_SO_CHO_XEM_XET.getStatus());
                break;
            case ATTP_KTNN_LĐĐV:
                lstFileStatus.add(FileStatus.HO_SO_CHO_PHE_DUYET.getStatus());
                lstFileStatus.add(FileStatus.HO_SO_CHO_PHAN_CONG.getStatus());
                break;
            default:
                break;
        }
        return lstFileStatus;
    }

}
