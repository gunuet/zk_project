/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Constant;

import java.util.Objects;

/**
 *
 * @author vietn
 */
public enum IfdFileStatus {
    
    CHO_PHAN_CONG(Long.valueOf("1"), "Chờ phân công"),
    HO_SO_GUI_BO_SUNG(Long.valueOf("2"), "Hồ sơ gửi bổ sung"),
    DA_BO_SUNG(Long.valueOf("3"), "Đã bổ sung"),
    YEU_CAU_BO_SUNG_HO_SO(Long.valueOf("4"), "Yêu cầu bổ sung hồ sơ"),
    THAM_DINH_DAT(Long.valueOf("5"), "Thẩm định đạt"),
    CHO_PHE_DUYET_YEU_CAU_BO_SUNG_HO_SO(Long.valueOf("6"), "Chờ phê duyệt yêu cầu bổ sung hồ sơ"),
    YEU_CAU_XU_LY_LAI(Long.valueOf("7"), "Yêu cầu xử lý lại"),
    PHE_DUYET_YEU_CAU_BO_SUNG_HO(Long.valueOf("8"), "Phê duyệt yêu cầu bổ sung hồ sơ"),
    CHO_PHE_DUYET_DANG_KY(Long.valueOf("9"), "Chờ phê duyệt đơn đăng ký");
    
    private Long status;
    private String statusName;
    
    private IfdFileStatus(Long status, String statusName) {
       this.status = status;
       this.statusName = statusName;
   }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
   
     public static IfdFileStatus getByStatus(Long status) {
       for (IfdFileStatus statusObject : IfdFileStatus.values()) {
           if (Objects.equals(statusObject.getStatus(), status)) {
               return statusObject;
           }
       }
       return null;
   }
}
