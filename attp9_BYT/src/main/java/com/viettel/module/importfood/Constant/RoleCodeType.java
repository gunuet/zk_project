/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Constant;

import java.util.Objects;

/**
 *
 * @author Tien update FileStatusName
 */
public enum RoleCodeType {

    ATTP_KTNN_KT("ATTP_KTNN_KT", 2750L),
    ATTP_KTNN_CV("ATTP_KTNN_CV", 2751L),
    ATTP_KTNN_LĐP("ATTP_KTNN_LĐP", 2752L),
    ATTP_KTNN_LĐĐV("ATTP_KTNN_LĐĐV", 2753L),; 

    private String roleCode;
    private Long roleId;

    private RoleCodeType(String roleCode, Long roleId) {
        this.roleCode = roleCode;
        this.roleId = roleId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }


    public static RoleCodeType getByRoleCode(String roleCode) {
        for (RoleCodeType statusObject : RoleCodeType.values()) {
            if (Objects.equals(statusObject.getRoleCode(), roleCode)) {
                return statusObject;
            }
        }
        return null;
    }
    
    public static RoleCodeType getByRoleId(Long roleId) {
        for (RoleCodeType statusObject : RoleCodeType.values()) {
            if (Objects.equals(statusObject.getRoleId(), roleId)) {
                return statusObject;
            }
        }
        return null;
    }
    
}
