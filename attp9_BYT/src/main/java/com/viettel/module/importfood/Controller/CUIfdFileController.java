/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import java.lang.reflect.Type;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.DAO.IfdFileDAO;
import com.viettel.module.importfood.DAO.ImportFoodProductDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileProductDAO;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Selectbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Administrator
 */
public class CUIfdFileController extends BaseComposer{
    
    @Wire
    private Window CUIfdFileWnd;
    
    @Wire
    private Listbox listData, fileStatus;
    
    @Wire
    private Textbox fileCode, createdBy, goodsOwnerName, goodsOwnerAddress, goodsOwnerPhone,
            goodsOwnerFax, goodsOwnerEmail, responesiblePersonName, responesiblePersonAddress,
            responesiblePersonPhone, responesiblePersonFax, responesiblePersonEmail, exporterName,
            exporterAddress, exporterPhone, exporterFax, exporterEmail, exporterGateCode, exporterGateName,
            customDeclarationNo, importerGateCode, importerGateName, checkPlace, checkDeptCode, checkDeptName;
    
    @Wire
    private Datebox createdDate, modifiedDate, comingDateFrom, comingDateTo, checkTimeFrom, checkTimeTo; 
    
    @Wire
    private Selectbox checkMethod;
    
    @Wire
    private Button btnAddProducts;
    
    @Wire
    private Paging userPagingBottom;
    
    private Long fileID;
    private String crudMode;
    private Window parentWindow;
    private IfdFile ifdFile;
    private List<IfdFileProduct> products;
    private List<Long> deletedProduct;
    
    public IfdFile getIfdFile(){
        return this.ifdFile;
    }
    
    public String getCrudMode(){
        return this.crudMode;
    }
    
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component comp, ComponentInfo comif){
        com.viettel.module.importfood.DAO.dao.IfdFileDAO dao = new com.viettel.module.importfood.DAO.dao.IfdFileDAO();
        IfdFileProductDAO productDao = new IfdFileProductDAO();
        fileID = (Long) Executions.getCurrent().getArg().get("fileID");
        crudMode = (String) Executions.getCurrent().getArg().get("crudmode");

        ifdFile = (IfdFile) dao.getById("fileId", fileID);
        if(this.crudMode!=null&&
                this.crudMode.equals("update")){
            ArrayList<IfdFileProduct> productsFromDB = new ArrayList<IfdFileProduct>();
            Gson gson = new Gson();
            
            String json = gson.toJson(productDao.findAllByFileId(fileID));
            Type collectionType = new TypeToken<List<IfdFileProduct>>(){}.getType();
            
            productsFromDB = gson.fromJson(json, collectionType);
            
            products = new ArrayList();
            if(productsFromDB!=null&&productsFromDB.size()>0)
            {
                products = (List<IfdFileProduct>) productsFromDB.clone();
            }
            parentWindow = (Window) Executions.getCurrent().getArg().get("parentWindow");
        }
        if (ifdFile == null) {
            ifdFile = new IfdFile();
        }
        if(crudMode == null){
            crudMode = "create";
        }
        deletedProduct = new ArrayList();
        return super.doBeforeCompose(page, comp, comif);
    }
    
    @Override
    public void doAfterCompose(Component comp) throws Exception{
        super.doAfterCompose(comp);
        if(products==null){
            products = new ArrayList();
        }
        if(crudMode=="update"){                        
            if(ifdFile.getFileStatus()!=null){
                if(ifdFile.getFileStatus()>=3){
                    fileStatus.setSelectedIndex(0);
                }else{
                    fileStatus.setSelectedIndex(Integer.parseInt(ifdFile.getFileStatus()+""));
                }
            }                        
        }
        loadDataToSelectBox();
        fillProductDataToList();
    }
       
    @Listen("onReload = #CUIfdFileWnd")
    public void onReload(){
        fillProductDataToList();
    }
    
    @Listen("onClick = #btnBack")
    public void onBackToParentPage(){
//        Executions.sendRedirect("/viewFile.zul");
        this.CUIfdFileWnd.detach();
    }
    
    @Listen("onDelete = #listData")
    public void onDelete(){
        String message, title;
        message = "Bạn có muốn xóa sản phẩm này??";
        title = "Delete";
        Messagebox.show(message, title, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener(){
            @Override
            public void onEvent(Event t) throws Exception {
                String ms = "", type = "";
                switch(t.getName()){
                    case Messagebox.ON_OK:                       
                        try{ 
                            IfdFileProductDAO productDao = new IfdFileProductDAO();
                            IfdFileProduct product = products.get(listData.getSelectedIndex());                           
                            if(product.getFileProductId()!=null){
                                long id = product.getFileProductId();
                                productDao.deleteById(id);
                            }  
                            products.remove(listData.getSelectedIndex());                           
                            ms += "Xóa thành công";
                            type = "info";
                            onReload();
                        }catch(Exception ex){
                            ms += "Đã xảy ra lỗi "+ ex.getMessage();
                            type = "error";
                        }
                        Clients.showNotification(ms, type, null, "top_center", 3000);
                        break;
                    case Messagebox.ON_CANCEL:
                        break;
                }

            }        
        });
    }
    
    @Listen("onEdit = #listData")
    public void onEdit(){
        Map<String, Object> args = new ConcurrentHashMap<>();
        IfdFileProduct product = products.get(listData.getSelectedIndex());
        args.put("crudPMode", "update");
        args.put("parentWindow", this.CUIfdFileWnd);
        args.put("product", product);
        args.put("index", listData.getSelectedIndex());
        createWindow("CUProductsWnd","/Pages/module/import_food/fileImportFood/cu_products.zul", args, Window.MODAL);
    }
    
    @Listen("onClick = #btnAddProducts")
    public void onAddProduct(){
        Map<String, Object> args = new ConcurrentHashMap<>();
        IfdFileProduct product = new IfdFileProduct();  
        product.setProductCheckMethod(Long.parseLong(0+""));
        product.setProductGroupCode(0+"");
        args.put("crudPMode", "create");
        args.put("parentWindow", this.CUIfdFileWnd);
        args.put("product", product);
        createWindow("CUProductsWnd","/Pages/module/import_food/fileImportFood/cu_products.zul", args, Window.MODAL);
    }
    
    @Listen("onClick = #btnSave")
    public void onSave(){
        if(!isValidatedData()){
            return;
        }
        Date date = new Date();
        IfdFileDAO dao = new IfdFileDAO();
        IfdFileProductDAO pdao = new IfdFileProductDAO();
        String message = "", type="";
        try{
            ifdFile.setFileCode(this.fileCode.getValue());

            ifdFile.setCheckMethod(Long.parseLong(this.checkMethod.getSelectedIndex()+""));

            ifdFile.setGoodsOwnerName(this.goodsOwnerName.getValue());
            ifdFile.setGoodsOwnerAddress(this.goodsOwnerAddress.getValue());
            ifdFile.setGoodsOwnerPhone(this.goodsOwnerPhone.getValue());
            ifdFile.setGoodsOwnerFax(this.goodsOwnerFax.getValue());
            ifdFile.setGoodsOwnerEmail(this.goodsOwnerEmail.getValue());

            ifdFile.setResponesiblePersonName(this.responesiblePersonName.getValue());
            ifdFile.setResponesiblePersonAddress(this.responesiblePersonAddress.getValue());
            ifdFile.setResponesiblePersonPhone(this.responesiblePersonPhone.getValue());
            ifdFile.setResponesiblePersonFax(this.responesiblePersonFax.getValue());
            ifdFile.setResponesiblePersonEmail(this.responesiblePersonEmail.getValue());

            ifdFile.setExporterName(this.exporterName.getValue());
            ifdFile.setExporterAddress(this.exporterAddress.getValue());
            ifdFile.setExporterPhone(this.exporterPhone.getValue());
            ifdFile.setExporterFax(this.exporterFax.getValue());
            ifdFile.setExporterEmail(this.exporterEmail.getValue());

            ifdFile.setCustomDeclarationNo(this.customDeclarationNo.getValue());
            ifdFile.setComingDateFrom(this.comingDateFrom.getValue());
            ifdFile.setComingDateTo(this.comingDateTo.getValue());

            ifdFile.setExporterGateCode(this.exporterGateCode.getValue());
            ifdFile.setExporterGateName(this.exporterGateName.getValue());

            ifdFile.setImporterGateCode(this.importerGateCode.getValue());
            ifdFile.setImporterGateName(this.importerGateName.getValue());

            ifdFile.setCheckTimeFrom(this.checkTimeFrom.getValue());
            ifdFile.setCheckTimeTo(this.checkTimeTo.getValue());

            ifdFile.setCheckPlace(this.checkPlace.getValue());

            ifdFile.setCheckDeptCode(this.checkDeptCode.getValue());
            ifdFile.setCheckDeptName(this.checkDeptCode.getValue());
            
            if(fileStatus.getSelectedIndex()==IfdConstants.FILE_STATUS.ADDITIONAL_FEES_FILE){
                ifdFile.setFileStatus(IfdConstants.FILE_STATUS.ADDITIONAL_FEES_FILE);
                ifdFile.setFileStatusName(IfdConstants.FILE_STATUS.ADDITIONAL_FEES_FILE_STR);
            }else if(fileStatus.getSelectedIndex()==IfdConstants.FILE_STATUS.NEW_SUBMIT_FILE){
                ifdFile.setFileStatus(IfdConstants.FILE_STATUS.NEW_SUBMIT_FILE);
                ifdFile.setFileStatusName(IfdConstants.FILE_STATUS.NEW_SUBMIT_FILE_STR);
            }
            
            switch(this.crudMode){
                case "create":                
                    ifdFile.setIsDelete(IfdConstants.OBJECT_STATUS.INACTIVE);
                    ifdFile.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
                    ifdFile.setCreateDate(date);
                    ifdFile.setUpdateDate(date);
                    message = "Tạo thành công";
                    break;
                case "update":
                    ifdFile.setUpdateDate(date);
                    message = "Chỉnh sửa thành công";
                    break;
            }
            dao.save(this.ifdFile);
            
            IfdFile tfile = dao.findByFileCode(this.ifdFile.getFileCode());
            for(IfdFileProduct pd : this.products){
                pd.setFileCode(tfile.getFileCode());
                pd.setFileId(tfile.getFileId());
            }   
            pdao.saveAll(this.products);
            ifdFile = new IfdFile();
            resetPage();
            type="info";
        }catch(Exception ex){
                message = "Gặp lỗi: " + ex.getMessage();
            type="error";
        }
        showNotification(message, type);
    }
    
    public void fillProductDataToList(){
        
        Session s = Sessions.getCurrent();
        if(s.getAttribute("nProduct")!=null){
            if(s.getAttribute("index")!=null){
                products.set((Integer) s.getAttribute("index"), (IfdFileProduct) s.getAttribute("nProduct"));
                s.removeAttribute("index");
            }else{
                products.add((IfdFileProduct) s.getAttribute("nProduct"));
            }
            s.removeAttribute("nProduct");
        }
        ListModelList model = new ListModelList(products);
        this.listData.setModel(model);
    }
    
    public void loadDataToSelectBox(){
        List<String> checkMethodList = new ArrayList();
        checkMethodList.add("-- chọn --");
        checkMethodList.add("Kiểm tra thường");
        checkMethodList.add("Kiểm tra chặt");
        ListModelList lmodel = new ListModelList(checkMethodList);
        
        switch(this.crudMode){
            case "create":
                lmodel.addToSelection(checkMethodList.get(0));
                break;
            case "update":
                lmodel.addToSelection(checkMethodList.get(Integer.parseInt(this.ifdFile.getCheckMethod()+"")));
                break;
        }
        
        this.checkMethod.setModel(lmodel);
    }
    
    public boolean isValidatedData(){
        if(this.fileCode.getValue().matches("\\s*")){
            showMessage("Mã hồ sơ không được để trống");
            fileCode.focus();
            return false;
        }else if(this.fileCode.getValue().length()>12){
            showMessage("Mã hồ sơ không vượt quá 12 ký tự");
            fileCode.focus();
            return false;
        }else if(this.fileCode.getValue().matches("^[^\\w]+[\\W\\w]+|^[\\W\\w]+[^\\w]+[\\W\\w]+|[\\W\\w]+[^\\w]+$")){
            showMessage("Mã hồ sơ không bao gồm các ký tự đặc biệt");
            fileCode.focus();
            return false;
        }
        
        if(this.checkMethod.getSelectedIndex()==-1||
                this.checkMethod.getSelectedIndex()==0){
            showMessage("Phương thức kiểm tra không được để trống");
            checkMethod.focus();
            return false;
        }
        
        if(this.fileStatus.getSelectedIndex()==-1||
                this.fileStatus.getSelectedIndex()==0){
            showMessage("Trạng thái hồ sơ không được để trống");
            fileStatus.focus();
            return false;
        }
        
        if(this.goodsOwnerName.getValue().matches("\\s*")){           
            showMessage("Tên chủ hàng không được để trống");
            goodsOwnerName.focus();
            return false;
        }else if(this.goodsOwnerName.getValue().length()>255){
            showMessage("Tên chủ hàng không vượt quá 255 ký tự");
            goodsOwnerName.focus();
            return false;
        }
        
        if(this.goodsOwnerAddress.getValue().matches("\\s*")){
            showMessage("Địa chỉ của chủ hàng không được để trống");
            goodsOwnerAddress.focus();
            return false;
        }else if(this.goodsOwnerAddress.getValue().length()>500){
            showMessage("Địa chỉ của chủ hàng không vượt quá 500 ký tự");
            goodsOwnerAddress.focus();
            return false;
        }
        
        if(this.goodsOwnerPhone.getValue().matches("\\s*")){
            showMessage("Điện thoại của chủ hàng không được để trống");
            this.goodsOwnerPhone.focus();
            return false;
        }else if(this.goodsOwnerPhone.getValue().length()>50){
            showMessage("Điện thoại của chủ hàng không vượt quá 50 ký tự");
            this.goodsOwnerPhone.focus();
            return false;
        }
        
        if(this.goodsOwnerFax.getValue().length()>20){
            showMessage("Fax của chủ hàng không vượt quá 20 ký tự");
            this.goodsOwnerFax.focus();
            return false;
        }
        
        if(this.goodsOwnerEmail.getValue().matches("\\s*")){
            showMessage("Email của chủ hàng không được để trống");
            this.goodsOwnerEmail.focus();
            return false;
        }else if(this.goodsOwnerEmail.getValue().length()>255){
            showMessage("Email của chủ hàng không vượt quá 255 ký tự");
            this.goodsOwnerEmail.focus();
            return false;
        }else if(!this.goodsOwnerEmail.getValue().matches("^\\w+@\\w{2,6}.\\w{2,6}")){
            showMessage("Không đúng định dạng của email");
            goodsOwnerEmail.focus();
            return false;
        }
        
        if(this.responesiblePersonName.getValue().matches("\\s*")){
            showMessage("Tên thương nhân chịu trách nhiệm không được để trống");
            responesiblePersonName.focus();
            return false;
        }else if(this.responesiblePersonName.getValue().length()>255){
            showMessage("Tên thương nhân chịu trách nhiệm không vượt quá 255 ký tự");
            responesiblePersonName.focus();
            return false;
        }
        
        if(this.responesiblePersonAddress.getValue().matches("\\s*")){
            showMessage("Địa chỉ thương nhân chịu trách nhiệm không được để trống");
            this.responesiblePersonAddress.focus();
            return false;
        }else if(this.responesiblePersonAddress.getValue().length()>500){
            showMessage("Địa chỉ thương nhân chịu trách nhiệm không vượt quá 500 ký tự");
            this.responesiblePersonAddress.focus();
            return false;
        }
        
        if(this.responesiblePersonPhone.getValue().matches("\\s*")){
            showMessage("Điện thoại thương nhân chịu trách nhiệm không được để trống");
            return false;
        }else if(this.responesiblePersonPhone.getValue().length()>50){
            showMessage("Điện thoại thương nhân chịu trách nhiệm không vượt quá 255 ký tự");
            this.responesiblePersonPhone.focus();
            return false;
        }
        
        if(this.responesiblePersonFax.getValue().length()>20){
            showMessage("Fax của thương nhân chịu trách nhiệm không vượt quá 20 ký tự");
            this.responesiblePersonFax.focus();
            return false;
        }
        
        if(this.responesiblePersonEmail.getValue().length()>255){
            showMessage("Email của thương nhân chịu trách nhiệm không vượt quá 255 ký tự");
            this.responesiblePersonEmail.focus();
            return false;
        }if(this.responesiblePersonEmail.getValue().length()>0&&
                !this.responesiblePersonEmail.getValue().matches("^\\w+@\\w{2,6}.\\w{2,6}")){
            showMessage("Không đúng định dạng của email");
            responesiblePersonEmail.focus();
            return false;
        }
        
        if(this.exporterName.getValue().matches("\\s*")){
            showMessage("Tên tổ chức/cá nhân xuất khẩu không được để trống");
            this.exporterName.focus();
            return false;
        }else if(this.exporterName.getValue().length()>255){
            showMessage("Tên tổ chức/cá nhân xuất khẩu không vượt quá 255 ký tự");
            this.exporterName.focus();
            return false;
        }
        
        if(this.exporterAddress.getValue().matches("\\s*")){
            showMessage("Địa chỉ tổ chức/cá nhân xuất khẩu không được để trống");
            this.exporterAddress.focus();
            return false;
        }else if(this.exporterAddress.getValue().length()>500){
            showMessage("Tên tổ chức/cá nhân xuất khẩu không vượt quá 500 ký tự");
            this.exporterAddress.focus();
            return false;
        }
        
        if(this.exporterPhone.getValue().length()>50){
            showMessage("Điện thoại tổ chức/cá nhân xuất khẩu không vượt quá 50 ký tự");
            this.exporterPhone.focus();
            return false;
        }
        
        if(this.exporterFax.getValue().length()>20){
            showMessage("Fax tổ chức/cá nhân xuất khẩu không vượt quá 20 ký tự");
            return false;
        }
        
        if(this.exporterEmail.getValue().length()>255){
            showMessage("Email tổ chức/cá nhân xuất khẩu không được để trống");
            this.exporterEmail.focus();
            return false;
        }if(this.exporterEmail.getValue().length()>0&&
                !this.exporterEmail.getValue().matches("^\\w+@\\w{2,6}.\\w{2,6}")){
            showMessage("Không đúng định dạng của email");
            exporterEmail.focus();
            return false;
        }
        
        if(this.customDeclarationNo.getValue().length()>255){
            showMessage("Số tờ khai hải quan không vượt quá 255 ký tự");
            this.customDeclarationNo.focus();
            return false;
        }
        
        if(this.comingDateFrom.getValue()==null||
                this.comingDateTo.getValue()==null){
            showMessage("Thời gian nhập khẩu dự kiến không được để trống");
            return false;
        }
        
        if (this.comingDateTo.getValue() != null && this.comingDateFrom.getValue() != null) {
            if (this.comingDateFrom.getValue().compareTo(this.comingDateTo.getValue()) > 0) {
                showMessage("(Thời gian nhập khẩu dự kiến) 'từ ngày' không được lớn hơn thời gian 'đến ngày' !");
                this.comingDateFrom.focus();
                return false;
            }
        }
        
        if(this.exporterGateCode.getValue().matches("\\s*")){
            showMessage("Mã cửa khẩu đi không được để trống");
            this.exporterGateCode.focus();
            return false;
        } if(this.exporterGateCode.getValue().length()>6){
            showMessage("Mã cửa khẩu đi không vượt quá 6 ký tự");
            this.exporterGateCode.focus();
            return false;
        }
        
        if(this.exporterGateName.getValue().matches("\\s*")){
            showMessage("Tên cửa khẩu đi không được để trống");
            this.exporterGateName.focus();
            return false;
        }else if(this.exporterGateName.getValue().length()>255){
            showMessage("Tên cửa khẩu đi không vượt quá 255 ký tự");
            this.exporterGateName.focus();
            return false;
        }
        
        if(this.importerGateCode.getValue().matches("\\s*")){
            showMessage("Mã cửa khẩu đến không được để trống");
            this.importerGateCode.focus();
            return false;
        }if(this.importerGateCode.getValue().length()>6){
            showMessage("Mã cửa khẩu đến không được để trống");
            this.importerGateCode.focus();
            return false;
        }
        
        if(this.importerGateName.getValue().matches("\\s*")){
            showMessage("Tên cửa khẩu đến không được để trống");
            this.importerGateName.focus();
            return false;
        }else if(this.importerGateName.getValue().length()>255){
            showMessage("Tên cửa khẩu đến không vượt quá 255 ký tự");
            this.importerGateName.focus();
            return false;
        }
        
        if(this.checkTimeFrom.getValue()==null||
                this.checkTimeTo.getValue()==null){
            showMessage("Thời gian kiểm tra không được để trống");
            return false;
        }
        
        if (this.checkTimeTo.getValue() != null && this.checkTimeFrom.getValue() != null) {
            if (this.checkTimeFrom.getValue().compareTo(this.checkTimeTo.getValue()) > 0) {
                showMessage("(Thời gian kiểm tra) 'từ ngày' không được lớn hơn thời gian 'đến ngày' !");
                this.comingDateFrom.focus();
                return false;
            }
        }
        
        if(this.checkPlace.getValue().matches("\\s*")){
            showMessage("Địa điểm kiểm tra không được để trống");
            this.checkPlace.focus();
            return false;
        }else if(this.checkPlace.getValue().length()>500){
            showMessage("Địa điểm kiểm tra không vượt quá 500 ký tự");
            this.checkPlace.focus();
            return false;
        }
        
        if(this.checkDeptCode.getValue().matches("\\s*")){
            showMessage("Mã tổ chức kiểm tra không được để trống");
            this.checkDeptName.focus();
            return false;
        }else if(this.checkDeptCode.getValue().length()>12){
            showMessage("Mã tổ chức kiểm tra không vượt quá 12 ký tự");
            this.checkDeptCode.focus();
            return false;
        }
        
        if(this.checkDeptName.getValue().matches("\\s*")){
            showMessage("Tên tổ chức kiểm tra không được để trống");
            this.checkDeptName.focus();
            return false;
        }else if(this.checkDeptName.getValue().length()>255){
            showMessage("Tên tổ chức kiểm tra không vượt quá 255 ký tự");
            this.checkDeptName.focus();
            return false;
        }
        return true;
    }
    
    public void resetPage(){
        products = new ArrayList();
        
        this.fileCode.setValue("");
        
        List<String> checkMethodList = new ArrayList();
        checkMethodList.add("-- chọn --");
        checkMethodList.add("Kiểm tra thường");
        checkMethodList.add("Kiểm tra chặt");
        ListModelList lmodel = new ListModelList(checkMethodList);
        lmodel.addToSelection(checkMethodList.get(0));
        this.checkMethod.setModel(lmodel);
        
        this.goodsOwnerName.setValue("");
        this.goodsOwnerAddress.setValue("");
        this.goodsOwnerPhone.setValue("");
        this.goodsOwnerFax.setValue("");
        this.goodsOwnerEmail.setValue("");
        
        this.responesiblePersonName.setValue("");
        this.responesiblePersonAddress.setValue("");
        this.responesiblePersonPhone.setValue("");
        this.responesiblePersonFax.setValue("");
        this.responesiblePersonEmail.setValue("");
      
        this.exporterName.setValue("");
        this.exporterAddress.setValue("");
        this.exporterPhone.setValue("");
        this.exporterFax.setValue("");
        this.exporterEmail.setValue("");
        
        this.customDeclarationNo.setValue("");
        this.comingDateFrom.setValue(null);
        this.comingDateTo.setValue(null);
        
        this.exporterGateCode.setValue("");
        this.exporterGateName.setValue("");

        this.importerGateCode.setValue("");
        this.importerGateName.setValue("");
        
        this.checkTimeFrom.setValue(null);
        this.checkTimeTo.setValue(null);
        
        this.checkPlace.setValue("");
       
        this.checkDeptCode.setValue("");
        this.checkDeptName.setValue("");
        
        this.fileStatus.setSelectedIndex(0);
        
        fillProductDataToList();
    }
    
    public void showMessage(String message){
        Clients.showNotification(message, "warning", null, "top_center", 3000);
    }
}
