/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importfood.BO.IfdFileProduct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Selectbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import java.io.UnsupportedEncodingException;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vlayout;
/**
 *
 * @author Administrator
 */
public class CUIfdFileProductController extends BaseComposer{
    @Wire
    private Window CUProductsWnd;
    
    @Wire
    private Textbox productName, productGroupName, manufacturer, manufacturerAddress,
            checkMethodConfirmNo, confirmAnnounceNo;
    @Wire
    private Selectbox productGroupCode, productCheckMethod;
    
    @Wire
    private Vlayout fileList;
    
    @Wire
    private Iframe attachfiles;
    
    private Window parentWindow;
    private IfdFileProduct product;
    private String crudMode;
    private int index;
    private List<Media> listMedia;
    
    public IfdFileProduct getProduct(){
        return product;
    }
    
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component comp, ComponentInfo compInfo) {
                
        Map<String, Object> args = (Map<String, Object>) Executions.getCurrent().getArg();
        crudMode = (String) args.get("crudPMode");
        parentWindow = (Window) args.get("parentWindow");
        product = (IfdFileProduct) args.get("product");
        if(crudMode.equals("update")){
            index = (Integer) args.get("index");
        }
        
        listMedia = new ArrayList();
        return super.doBeforeCompose(page, comp, compInfo);
    }
    
    @Override
    public void doAfterCompose(Component comp) throws Exception{
        super.doAfterCompose(comp);
        loadDataOfSelectBox();
    }
    
    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException{
        fileList.getChildren().clear();
        final Media media = event.getMedia();
        String extFile = media.getFormat();
        
            listMedia.clear();
            listMedia.add(media);
            
            attachfiles.setContent(media);
            
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.appendChild(new Label(media.getName()));
            
            fileList.appendChild(hl);
        
    }
    
    @Listen("onClick = #btnClose")
    public void onClose(){
       CUProductsWnd.detach();
    }
    
    @Listen("onClick = #btnSave")
    public void onSave(){
        if(!isValidatedData()){
            return;
        }
        Date date = new Date();
        String message = "", type = "";
        try{
            product.setProductName(productName.getValue());
            product.setProductGroupCode(productGroupCode.getSelectedIndex()+"");
            product.setProductGroupName(productGroupName.getValue());
            product.setManufacturer(manufacturer.getValue());
            product.setManufacturerAddress(manufacturerAddress.getValue());
            product.setProductCheckMethod(Long.parseLong(productCheckMethod.getSelectedIndex()+""));
            product.setCheckMethodConfirmNo(checkMethodConfirmNo.getValue());
            product.setConfirmAnnounceNo(confirmAnnounceNo.getValue());
            product.setCreateDate(date);
            product.setUpdateDate(date);
            product.setStatus(Long.parseLong(1+""));

            Session session = Sessions.getCurrent();
            switch(crudMode){
                case "create":
                    message += "Tạo thành công";
                    break;
                case "update":
                    message += "Chỉnh sửa thành công";
                    session.setAttribute("index", index);
                    break;
            }

            session.setAttribute("nProduct", product);
            type = "info";
    
        }catch(Exception ex){
            message +=  "Phát hiện lỗi: " + ex.getMessage();
            type = "error";
        }
        
        Clients.showNotification(message, type, null, "top_center", 3000);
        onClose(); 
        Events.sendEvent("onReload", parentWindow, null);
    }
    
    public void showWarningMessage(String message){
        Clients.showNotification(message,"warning" , null, "top_center", 3000);
    }
    
    public void showErrorMessage(String message){
        Clients.showNotification(message, "error" , null, "top_center", 3000);
    }
    
    public void showSuccessMessage(String message){
        Clients.showNotification(message, "info" , null, "top_center", 3000);
    }
    
    public void loadDataOfSelectBox(){
        List<String> grcode = new ArrayList();
        grcode.add("--Chọn--");
        grcode.add("Nước uống đóng chai, nước khoáng thiên nhiên, đá thực phẩm (nước đá dùng liền và nước đá dùng để chế biến thực phẩm)");
        grcode.add("Thực phẩm chức năng");
        grcode.add("Các vi chất bổ sung vào thực phẩm");
        grcode.add("Phụ gia, hương liệu, chất hỗ trợ chế biến thực phẩm");
        grcode.add("Dụng cụ, vật liệu bao gói, chứa đựng tiếp xúc trực tiếp với thực phẩm");
        grcode.add("Các sản phẩm khác không được quy định tại danh mục của Bộ Công Thương, Bộ Nông nghiệp và phát triển nông thôn");
        ListModelList grcodemodl = new ListModelList(grcode);

        productGroupCode.setModel(grcodemodl);
        
        List<String> cmethod = new ArrayList();
        cmethod.add("--Chọn--");
        cmethod.add("Kiểm tra thường");
        cmethod.add("Kiểm tra chặt");
        ListModelList cmethodmodl = new ListModelList(cmethod);       
        
        grcodemodl.addToSelection(grcode.get(Integer.parseInt(product.getProductGroupCode()+"")));
        cmethodmodl.addToSelection(cmethod.get(Integer.parseInt(product.getProductCheckMethod()+"")));
        productCheckMethod.setModel(cmethodmodl);
    }
    
    public boolean isValidatedData(){
        if(productName.getValue().matches("\\s*")){
            showWarningMessage("Tên sản phẩm không được để trống");
            productName.focus();
            return false;
        }else if(productName.getValue().length()>255){
            showWarningMessage("Tên sản phẩm không vượt quá 255 ký tự");
            productName.focus();
            return false;
        }
        
        if(productGroupCode.getSelectedIndex()==0||
            productGroupCode.getSelectedIndex()==-1){
            showWarningMessage("Nhóm sản phẩm không được để trống");
            return false;
        }
        
        if(productGroupName.getValue().matches("\\s*")){
            showWarningMessage("Tên nhóm sản phẩm không được để trống");
            productGroupName.focus();
            return false;
        }else if(productGroupName.getValue().length()>255){
            showWarningMessage("Tên nhóm sản phẩm không vượt quá 255 ký tự");
            productGroupName.focus();
            return false;
        }
        
        if(manufacturer.getValue().matches("\\s*")){
            showWarningMessage("Tên nhà sản xuất không được để trống");
            manufacturer.focus();
            return false;
        }else if(manufacturer.getValue().length()>255){
            showWarningMessage("Tên nhà sản xuất không vượt quá 255 ký tự");
            manufacturer.focus();
            return false;
        }
        
        if(manufacturerAddress.getValue().matches("\\s*")){
            showWarningMessage("Địa chỉ nhà sản xuất không được để trống");
            manufacturerAddress.focus();
            return false;
        }else if(manufacturerAddress.getValue().length()>255){
            showWarningMessage("Địa chỉ nhà sản xuất không vượt quá 255 ký tự");
            manufacturerAddress.focus();
            return false;
        }
        
        if(productCheckMethod.getSelectedIndex()==0||
                productCheckMethod.getSelectedIndex()==-1){
            showWarningMessage("Phương thức kiểm tra không được để trống");
            return false;
        }
        
        if(checkMethodConfirmNo.getValue().matches("\\s*")){
            showWarningMessage("Số văn bản xác nhận phương thức kiểm tra không được để trống");
            checkMethodConfirmNo.focus();
            return false;
        }else if(checkMethodConfirmNo.getValue().length()>255){
            showWarningMessage("Số văn bản xác nhận phương thức kiểm tra không vượt quá 255 ký tự");
            checkMethodConfirmNo.focus();
            return false;
        }
        
        if(confirmAnnounceNo.getValue().matches("\\s*")){
            showWarningMessage("Số công bố không được để trống");
            confirmAnnounceNo.focus();
            return false;
        }else if(confirmAnnounceNo.getValue().length()>255){
            showWarningMessage("Số công bố không vượt quá 255 ký tự");
            confirmAnnounceNo.focus();
            return false;
        }
        return true;
    }
}
