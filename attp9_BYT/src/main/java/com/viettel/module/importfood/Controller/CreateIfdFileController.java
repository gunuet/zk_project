/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Administrator
 */
public class CreateIfdFileController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    private Long fileID;
    private IfdFile ifdFile;
    private String crudmode;
    @Wire
    private Window windowCreateIfdFile;
    private Window parentWindow;
    @Wire
    private Listbox lbxProduct;
    @Wire
    private Textbox fileCode, createdBy, goodsOwnerName, goodsOwnerAddress, goodsOwnerPhone,
            goodsOwnerFax, goodsOwnerEmail, responsiblePersonName, responsiblePersonAddress,
            responsiblePersonPhone, responsiblePersonFax, responsiblePersonEmail, exporterName,
            exporterAddress, exporterPhone, exporterFax, exporterEmail, exporterGateCode, exporterGateName,
            customDeclarationNo, importerGateCode, importerGateName, checkPlace, checkDeptCode, checkDeptName,checkMethod;

    @Wire
    private Datebox createdDate, modifiedDate, comingDateFrom, comingDateTo, checkTimeFrom, checkTimeTo;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        fileID = (Long) Executions.getCurrent().getArg().get("fileID");
        crudmode = (String) Executions.getCurrent().getArg().get("crudmode");
        IfdFileDAO dao = new IfdFileDAO();
        ifdFile = (IfdFile) dao.getById("fileId", fileID);
        if (ifdFile == null) {
            ifdFile = new IfdFile();
        }
        return super.doBeforeCompose(page, parent, compInfo);

    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
              Execution execution = Executions.getCurrent();
            setParentWindow((Window) execution.getArg().get("parentWindow"));
        } catch (Exception ex) {
            Logger.getLogger(ViewTabFileSavedController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Listen("onClick = #btnSave")
    public void onSave() {
        if (!isValidatedData()) {
            return;
        }
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy  HH:mm:ss");
        com.viettel.module.importfood.DAO.IfdFileDAO dao = new com.viettel.module.importfood.DAO.IfdFileDAO();
        String message = "";
        ifdFile.setFileCode(fileCode.getValue());
        ifdFile.setGoodsOwnerName(goodsOwnerName.getValue());
        ifdFile.setGoodsOwnerAddress(goodsOwnerAddress.getValue());
        ifdFile.setGoodsOwnerPhone(goodsOwnerPhone.getValue());
        ifdFile.setGoodsOwnerFax(goodsOwnerFax.getValue());
        ifdFile.setGoodsOwnerEmail(goodsOwnerEmail.getValue());

        ifdFile.setResponesiblePersonName(responsiblePersonName.getValue());
        ifdFile.setResponesiblePersonAddress(responsiblePersonAddress.getValue());
        ifdFile.setResponesiblePersonPhone(responsiblePersonPhone.getValue());
        ifdFile.setResponesiblePersonFax(responsiblePersonFax.getValue());
        ifdFile.setResponesiblePersonEmail(responsiblePersonEmail.getValue());

        ifdFile.setExporterName(exporterName.getValue());
        ifdFile.setExporterAddress(exporterAddress.getValue());
        ifdFile.setExporterPhone(exporterPhone.getValue());
        ifdFile.setExporterFax(exporterFax.getValue());
        ifdFile.setExporterEmail(exporterEmail.getValue());

        ifdFile.setCustomDeclarationNo(customDeclarationNo.getValue());
        ifdFile.setComingDateFrom(comingDateFrom.getValue());
        ifdFile.setComingDateTo(comingDateTo.getValue());

        ifdFile.setExporterGateCode(exporterGateCode.getValue());
        ifdFile.setExporterGateName(exporterGateName.getValue());

        ifdFile.setImporterGateCode(importerGateCode.getValue());
        ifdFile.setImporterGateName(importerGateName.getValue());

        ifdFile.setCheckTimeFrom(checkTimeFrom.getValue());
        ifdFile.setCheckTimeTo(checkTimeTo.getValue());

        ifdFile.setCheckPlace(checkPlace.getValue());

        ifdFile.setCheckDeptCode(checkDeptCode.getValue());
        ifdFile.setCheckDeptName(checkDeptName.getValue());

        switch (this.crudmode) {
            case "create":

                ifdFile.setCreateDate(date);
                ifdFile.setUpdateDate(date);
                dao.saveOrUpdate(ifdFile);
                ifdFile = new IfdFile();
                break;
            case "update":
                ifdFile.setUpdateDate(date);
                dao.saveOrUpdate(ifdFile);
                Events.sendEvent(new Event("onCreateIfdFileSaved", parentWindow, null));
                windowCreateIfdFile.detach();
                break;
        }

        showNotification(message, "info");
    }

    public Long getFileID() {
        return fileID;
    }

    public void setFileID(Long fileID) {
        this.fileID = fileID;
    }

    public IfdFile getIfdFile() {
        return ifdFile;
    }

    public void setIfdFile(IfdFile ifdFile) {
        this.ifdFile = ifdFile;
    }

    public boolean isValidatedData() {
        if (this.fileCode.getValue().matches("\\s*")) {
            showMessage("Mã hồ sơ không được để trống");
            fileCode.focus();
            return false;
        } else if (this.fileCode.getValue().matches("^[^\\w]+[\\W\\w]+|^[\\W\\w]+[^\\w]+[\\W\\w]+|[\\W\\w]+[^\\w]+$")) {
            showMessage("Mã hồ sơ không bao gồm các ký tự đặc biệt");
            fileCode.focus();
            return false;
        }

        if (this.goodsOwnerName.getValue().matches("\\s*")) {
            showMessage("Tên chủ hàng không được để trống");
            goodsOwnerName.focus();
            return false;
        }
        if (this.goodsOwnerAddress.getValue().matches("\\s*")) {
            showMessage("Địa chỉ của chủ hàng không được để trống");
            goodsOwnerAddress.focus();
            return false;
        }

        if (this.goodsOwnerPhone.getValue().matches("\\s*")) {
            showMessage("Điện thoại của chủ hàng không được để trống");
            this.goodsOwnerPhone.focus();
            return false;
        }

        if (this.goodsOwnerFax.getValue().length() > 20) {
            showMessage("Fax của chủ hàng không vượt quá 20 ký tự");
            this.goodsOwnerFax.focus();
            return false;
        }

        if (this.goodsOwnerEmail.getValue().matches("\\s*")) {
            showMessage("Email của chủ hàng không được để trống");
            this.goodsOwnerEmail.focus();
            return false;

        } else if (!this.goodsOwnerEmail.getValue().matches("^\\w+@\\w{2,6}.\\w{2,6}")) {
            showMessage("Không đúng định dạng của email");
            goodsOwnerEmail.focus();
            return false;
        }

        if (this.responsiblePersonName.getValue().matches("\\s*")) {
            showMessage("Tên thương nhân chịu trách nhiệm không được để trống");
            responsiblePersonName.focus();
            return false;
        }

        if (this.responsiblePersonAddress.getValue().matches("\\s*")) {
            showMessage("Địa chỉ thương nhân chịu trách nhiệm không được để trống");
            this.responsiblePersonAddress.focus();
            return false;
        }

        if (this.responsiblePersonPhone.getValue().matches("\\s*")) {
            showMessage("Điện thoại thương nhân chịu trách nhiệm không được để trống");
            return false;
        }

        if (this.responsiblePersonFax.getValue().length() > 20) {
            showMessage("Fax của thương nhân chịu trách nhiệm không vượt quá 20 ký tự");
            this.responsiblePersonFax.focus();
            return false;
        }

        if (this.responsiblePersonEmail.getValue().length() > 0
                && !this.responsiblePersonEmail.getValue().matches("^\\w+@\\w{2,6}.\\w{2,6}")) {
            showMessage("Không đúng định dạng của email");
            responsiblePersonEmail.focus();
            return false;
        }

        if (this.exporterName.getValue().matches("\\s*")) {
            showMessage("Tên tổ chức/cá nhân xuất khẩu không được để trống");
            this.exporterName.focus();
            return false;
        }

        if (this.exporterAddress.getValue().matches("\\s*")) {
            showMessage("Địa chỉ tổ chức/cá nhân xuất khẩu không được để trống");
            this.exporterAddress.focus();
            return false;
        }

        if (this.exporterEmail.getValue().length() > 255) {
            showMessage("Email tổ chức/cá nhân xuất khẩu không được để trống");
            this.exporterEmail.focus();
            return false;
        }
        if (this.exporterEmail.getValue().length() > 0
                && !this.exporterEmail.getValue().matches("^\\w+@\\w{2,6}.\\w{2,6}")) {
            showMessage("Không đúng định dạng của email");
            exporterEmail.focus();
            return false;
        }

        if (this.comingDateFrom.getValue() == null
                || this.comingDateTo.getValue() == null) {
            showMessage("Thời gian nhập khẩu dự kiến không được để trống");
            return false;
        }

        if (this.exporterGateCode.getValue().matches("\\s*")) {
            showMessage("Mã cửa khẩu đi không được để trống");
            this.exporterGateCode.focus();
            return false;
        }

        if (this.exporterGateName.getValue().matches("\\s*")) {
            showMessage("Tên cửa khẩu đi không được để trống");
            this.exporterGateName.focus();
            return false;
        }

        if (this.importerGateCode.getValue().matches("\\s*")) {
            showMessage("Mã cửa khẩu đến không được để trống");
            this.importerGateCode.focus();
            return false;
        }

        if (this.importerGateName.getValue().matches("\\s*")) {
            showMessage("Tên cửa khẩu đến không được để trống");
            this.importerGateName.focus();
            return false;
        }

        if (this.checkTimeFrom.getValue() == null
                || this.checkTimeTo.getValue() == null) {
            showMessage("Thời gian kiểm tra không được để trống");
            return false;
        }

        if (this.checkPlace.getValue().matches("\\s*")) {
            showMessage("Địa điểm kiểm tra không được để trống");
            this.checkPlace.focus();
            return false;
        }

        if (this.checkDeptCode.getValue().matches("\\s*")) {
            showMessage("Mã tổ chức kiểm tra không được để trống");
            this.checkDeptCode.focus();
            return false;
        } else if (this.checkDeptCode.getValue().length() > 12) {
            showMessage("Mã tổ chức kiểm tra không vượt quá 12 ký tự");
            this.checkDeptCode.focus();
            return false;
        }

        if (this.checkDeptName.getValue().matches("\\s*")) {
            showMessage("Tên tổ chức kiểm tra không được để trống");
            this.checkDeptName.focus();
            return false;
        }
        return true;
    }

    public void showMessage(String message) {
        Clients.showNotification(message, "warning", null, "top_center", 3000);
    }

    @Listen("onClick =  #btnClose")
    public void onClose() {
        Events.sendEvent(new Event("onCreateIfdFileSaved", parentWindow, null));
        windowCreateIfdFile.detach();
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }
}
