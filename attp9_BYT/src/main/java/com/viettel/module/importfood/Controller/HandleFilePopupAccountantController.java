/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdDept;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileResult;
import com.viettel.module.importfood.Constant.FileStatus;
import com.viettel.module.importfood.DAO.FileResultDAO;
import com.viettel.module.importfood.DAO.IfdDeptDAO;
import com.viettel.module.importfood.DAO.IfdFileAttachDAO;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.module.importfood.ws.NSWSendHelper;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window;
import com.viettel.module.importfood.ws.message.IfdSendMessage;

/**
 *
 * @author Administrator
 */
public class HandleFilePopupAccountantController extends BaseComposer {

    @Wire
    private Listbox fileStatus;

    @Wire
    private Textbox receiveNo, comment;

    @Wire
    private Button uploadBtn, submitBtn, closeBtn;

    @Wire
    private Window viewFileHandlePopupAccountantWnd;

    @Wire
    private Div viewReceiveNumber, viewUpload;

    @Wire
    private Listitem activeEvt;

    @Wire
    private Vbox viewFileName;

    @Wire
    private Label fileName;

    private Window parentWindow;

    private IfdFile ifdFile;
    private IfdFileResult fileResult;
    private IfdAttachment attachFile;
    private List<Media> medias;
    private long incrementNumber;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        loadData();
    }

    public void loadData() {
        medias = new ArrayList();
        Map<String, Object> args = (Map<String, Object>) Executions.getCurrent().getArg();
        ifdFile = (IfdFile) args.get("file");
        fileResult = new IfdFileResult();
        incrementNumber = -1;
        parentWindow = (Window) args.get("parentWindow");
    }

    @Listen("onSelect = #fileStatus")
    public void onSelectItem() {
        if (!fileStatus.getSelectedItem().getValue().toString().equals("5")) {
            viewUpload.setVisible(true);
            viewReceiveNumber.setVisible(true);
        } else {
            viewUpload.setVisible(false);
            viewReceiveNumber.setVisible(false);
        }
    }

    @Listen("onUpload = #uploadBtn")
    public void onUploadFile(UploadEvent event) throws UnsupportedEncodingException {
        attachFile = new IfdAttachment();
        final Media media = event.getMedia();
        String extFile = media.getName().replace("\"", "");
        String message = "Tải file thành công ", type = "info";
        if (!FileUtil.validFileType(extFile)) {
            String sExt = ResourceBundleUtil.getString("extend_file", "config");
            message = "Định dạng file không được phép tải lên (" + sExt + ")";
            type = "warning";
            return;
        }

        attachFile.setAttachmentName(media.getName());
        attachFile.setCreateDate(new Date());
        attachFile.setUpdateDate(new Date());
        attachFile.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
        attachFile.setUpdateBy(getUserId());
//        attachFile.setAttachTypeCode();chua co
//        attachFile.setAttachTypeName();hoa don

        fileName.setValue(media.getName());
        viewFileName.setVisible(true);

        uploadBtn.setDisabled(true);
        showMessage(message, type);
        medias.add(media);
    }

    @Listen("onRemoveFile = #viewFileHandlePopupAccountantWnd")
    public void onRemoveFile() {
        attachFile = new IfdAttachment();
        medias.remove(0);
        fileName.setValue("");
        viewFileName.setVisible(false);
        uploadBtn.setDisabled(false);
    }

    @Listen("onClick = #closeBtn")
    public void onCloseWindow() {
        viewFileHandlePopupAccountantWnd.detach();
    }

    @Listen("onClick = #submitBtn")
    public void onSubmit() {
        if (!isValidate()) {
            return;
        }

        if (getFileResult()) {
            IfdDeptDAO dao = new IfdDeptDAO();

            //get ra object sau do set lai gia tri increnum moi' ko cong
            //check neu nam hien tai > getyear() -> setYear = currentYear;
            IfdDept oldDept = dao.findByDeptcode(fileResult.getDeptCode());
            long currentYear = Calendar.getInstance().get(Calendar.YEAR);

            if (oldDept.getYear() < currentYear) {
                oldDept.setYear(currentYear);
                oldDept.setNumberincre(1L);
            } else {
                oldDept.setNumberincre(incrementNumber);
            }

            //luu xuong db
            dao.saveOrUpdate(oldDept);
            
            fileStatus.setDisabled(true);
            submitBtn.setVisible(false);
        }
    }

    public boolean getFileResult() {
        boolean isSuccess = true;
        String message = "";
        String type = "";
        String receiveNum = "";
        String function="";
        IfdFileResult temp = new IfdFileResult();
        FileResultDAO dao = new FileResultDAO();
        IfdFileAttachDAO attdao = new IfdFileAttachDAO();
        try {
            fileResult.setFileId(ifdFile.getFileId());
            fileResult.setFileCode(ifdFile.getFileCode());
            fileResult.setDeptCode(ifdFile.getCheckDeptCode());
            fileResult.setDeptName(ifdFile.getCheckDeptName());
            fileResult.setProcessDate(new Date());
            fileResult.setProcessName(getUserName());
            fileResult.setProcessContent(comment.getValue());
            if (Long.parseLong(fileStatus.getSelectedItem().getValue().toString()) == FileStatus.HO_SO_CHO_PHAN_CONG.getStatus()) {
                //Lay so tiep nhan
                receiveNum = getReceiveNumber();
                fileResult.setReceiveNo(receiveNum);
                
                fileResult.setFileStatus(FileStatus.HO_SO_CHO_PHAN_CONG.getStatus());
                fileResult.setFileStatusName(FileStatus.HO_SO_CHO_PHAN_CONG.getStatusName());
                function = IfdConstants.ATTP_FUNCTION.FUNCTION_06;
                
            } else if (Long.parseLong(fileStatus.getSelectedItem().getValue().toString()) == FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus()) {
                fileResult.setFileStatus(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus());
                fileResult.setFileStatusName(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatusName());
                function = IfdConstants.ATTP_FUNCTION.FUNCTION_04;
            }
            fileResult.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            fileResult.setCreateDate(ifdFile.getCreateDate());
            fileResult.setCreateBy(getUserId());
            fileResult.setUpdateDate(ifdFile.getUpdateDate());
            fileResult.setUpdateBy(getUserId());

            //save file tra ve
            dao.saveOrUpdate(fileResult);

            if (Long.parseLong(fileStatus.getSelectedItem().getValue().toString()) == FileStatus.HO_SO_CHO_PHAN_CONG.getStatus()) {
                if(attachFile!=null){
                    //Save file dinh kem
                    temp = dao.findByReceiveNo(receiveNum);
                    attachFile.setObjectId(temp.getFileResultId());
                    attachFile.setObjectType(IfdConstants.ATTACH_OBJEC_TYPE.FILE_RESULT);
                    attdao.save(attachFile);
                }
            }
            
            IfdSendMessage sendMessage = new IfdSendMessage();
            sendMessage.setFileId(ifdFile.getFileId());
            sendMessage.setFileCode(ifdFile.getFileCode());
            sendMessage.setType(IfdConstants.ATTP_TYPE.TYPE_102);
            sendMessage.setFunction(function);

            NSWSendHelper sendHelper = new NSWSendHelper();
            boolean isSendSucces = sendHelper.sendMessage(sendMessage);
            
            if (isSendSucces) {
                message = "Thao tác thành công!!";
                type = "info";

                //Hien thi so da nhan
                receiveNo.setValue(receiveNum);
            } else {
                message = "Gặp lỗi trong quá trình gửi bản tin";
                type = "error";
                isSuccess = false;
            }
            
            
        } catch (Exception ex) {
            message = "Gặp lỗi " + ex.getMessage();
            type = "error";
            isSuccess = false;
        }
        showMessage(message, type);
        return isSuccess;
    }

    public String getReceiveNumber() {
        String result = "";
        String deptCode = "";
        String number = "";
        Long increNum = 0L;
        long currentYear = 0L;
        IfdDeptDAO dao = new IfdDeptDAO();

        //lay deptCode
        deptCode = ifdFile.getCheckDeptCode();

        IfdDept dept = dao.findByDeptcode(deptCode);
        currentYear = Calendar.getInstance().get(Calendar.YEAR);

        if (dept == null) {
            IfdDept tempDept = new IfdDept();
            tempDept.setDeptcode(deptCode);
            tempDept.setYear(currentYear);
            tempDept.setNumberincre(1L);
            dao.saveOrUpdate(tempDept);

            dept = dao.findByDeptcode(deptCode);
        }

        if (currentYear > dept.getYear()) {
            incrementNumber = 1L;
        }
        increNum = dept.getNumberincre();
        incrementNumber = increNum + 1;

        NumberFormat formatter = new DecimalFormat("00000");
        number = formatter.format(increNum);

        result = number + "/" + currentYear + "/" + deptCode;
        return result;
    }

    public boolean isValidate() {

        if (fileStatus.getSelectedItem().getValue().toString().equals("-1")) {
            showMessage("Thao tác xử lý không được để trống", "warning");
            return false;
        }

        if (comment.getValue().toString().length() > 2000) {
            showMessage("Nội dung của 'Ý kiến' không vượt quá 2000 ký tự", "warning");
            return false;
        }
        return true;
    }

    public void showMessage(String message, String type) {
        Clients.showNotification(message, type, null, "top_center", 3000);
    }
}
