/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdEvaluation;
import com.viettel.module.importfood.BO.IfdEvaluationComment;
import com.viettel.module.importfood.BO.IfdEvaluationProduct;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.DAO.dao.IfdFileProductDAO;
import com.viettel.module.importfood.Service.IfdEvaluationService;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author Administrator
 */
public class HandleFilePopupExpertController extends BaseComposer {

    @Wire
    private Window viewFileHandlePopupExpertWnd;

    @Wire
    private Listbox fileStatusLb, listLeadersLb, listAttachFileLb, listProductLb;

//    @Wire
//    private Grid listProductGrid;
    @Wire
    private Button choosePersonBtn, viewResultBtn, sendBtn, closeBtn, uploadBtn, createNewAttFileBtn;

    @Wire
    private Textbox commentTextBox;

    @Wire
    private Vlayout boxContainFileName;

    private List<Media> listMedia;

    private IfdFile ifdFile;

    private IfdEvaluation ifdEvaluation;
    private IfdEvaluationComment ifdEvaluationComment;
    private List<IfdEvaluationProduct> ifdEvaluationProductList;

    private List<IfdAttachment> listTempAttachFiles;

    private Window parentWindow;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        loadDataToStart();
    }

    public void loadDataToStart() {
        Map<String, Object> args = (Map<String, Object>) Executions.getCurrent().getArg();
        ifdFile = (IfdFile) args.get("file");
        parentWindow = (Window) args.get("parentWindow");

        listTempAttachFiles = new ArrayList();
        listMedia = new ArrayList();

        loadAllProductToEvaluationProductList();
        loadDefaultDataForEvaluationComment();
        loadDefaultDataForIfdEvaluationFile();
    }

    public void loadAllProductToEvaluationProductList() {
        ifdEvaluationProductList = new ArrayList();
        IfdFileProductDAO productDao = new IfdFileProductDAO();

        IfdEvaluationProduct tempProduct;
        List<IfdFileProduct> productsListFromIfdFileProduct = productDao.findAllByFileId(ifdFile.getFileId());

        for (IfdFileProduct product : productsListFromIfdFileProduct) {
            tempProduct = new IfdEvaluationProduct();

            tempProduct.setProductCode(product.getProductCode());
            tempProduct.setProductName(product.getProductName());
            tempProduct.setProductGroupCode(product.getProductGroupCode());
            tempProduct.setProductGroupName(product.getProductGroupName());
            tempProduct.setManufacturer(product.getManufacturer());
            tempProduct.setManufacturerAddress(product.getManufacturerAddress());
            tempProduct.setProductCheckMethod(product.getProductCheckMethod());
            tempProduct.setCheckMethodConfirmNo(product.getCheckMethodConfirmNo());
            tempProduct.setConfirmAnnounceNo(product.getConfirmAnnounceNo());
            tempProduct.setStatus(product.getStatus());
            tempProduct.setCreateBy(product.getCreateBy());
            tempProduct.setCreateDate(product.getCreateDate());
            tempProduct.setUpdateBy(product.getUpdateBy());
            tempProduct.setUpdateDate(product.getUpdateDate());

            ifdEvaluationProductList.add(tempProduct);
            tempProduct = null;
        }

        ListModelArray tempListModel = new ListModelArray(ifdEvaluationProductList);

        //Nap vao grid
        listProductLb.setModel(tempListModel);
    }

    public void loadDefaultDataForEvaluationComment() {
        ifdEvaluationComment = new IfdEvaluationComment();
        ifdEvaluationComment.setContentEvaluation("");

        ifdEvaluationComment.setStatus(Constants.Status.ACTIVE);

        ifdEvaluationComment.setCreateBy(getUserId());
        ifdEvaluationComment.setUpdateBy(getUserId());

        ifdEvaluationComment.setCreateDate(new Date());
        ifdEvaluationComment.setUpdateDate(new Date());
    }

    public void loadDefaultDataForIfdEvaluationFile() {
        ifdEvaluation = new IfdEvaluation();

        ifdEvaluation.setFileId(ifdFile.getFileId());

        ifdEvaluation.setStatus(Constants.Status.ACTIVE);

        ifdEvaluation.setCreateBy(getUserId());
        ifdEvaluation.setUpdateBy(getUserId());

        ifdEvaluation.setCreateDate(new Date());
        ifdEvaluation.setUpdateDate(new Date());
    }

    @Listen("onViewFile = #listProductGrid")
    public void onViewFileProduct() {

    }

    @Listen("onClick = #createNewAttFileBtn")
    public void onAddNewAttachFileToList() {
        loadDataToListTempAttachFiles();
        listAttachFileLb.setModel(new ListModelArray(listTempAttachFiles));
        boxContainFileName.getChildren().removeAll(boxContainFileName.getChildren());
    }

    @Listen("onDeleteAttachFileFromList = #listAttachFileLb")
    public void onDeleteAttachFileFromList() {
        int index = listAttachFileLb.getSelectedIndex();

        listAttachFileLb.removeItemAt(index);
        listTempAttachFiles.remove(index);
    }

    public void loadDataToListTempAttachFiles() {
        if (listMedia.size() != 0) {

            createNewAttFileBtn.setDisabled(true);
            uploadBtn.setDisabled(true);

            IfdAttachment attachFileTemp;
            for (final Media media : listMedia) {
                attachFileTemp = new IfdAttachment();

                attachFileTemp.setAttachmentName(media.getName());
                attachFileTemp.setCreateDate(new Date());
                attachFileTemp.setUpdateDate(new Date());
                attachFileTemp.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
                attachFileTemp.setUpdateBy(getUserId());

                listTempAttachFiles.add(attachFileTemp);
            }

            createNewAttFileBtn.setDisabled(false);
            uploadBtn.setDisabled(false);
            boxContainFileName.setVisible(false);
            listMedia.clear();
        }
    }

    @Listen("onDownloadAttachFile = #listAttachFileLb")
    public void onDownloadAttachFile() {

    }

    @Listen("onUpload = #uploadBtn")
    public void onUploadAttachFile(UploadEvent event) throws UnsupportedEncodingException {
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            String message = "Tải file thành công ";
            String type = "info";
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                message = "Định dạng file không được phép tải lên (" + sExt + ")";
                type = "warning";
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);
            createAttachFileNameLable(media);
        }

        boxContainFileName.setVisible(true);
    }

    //Tao ten file trong phan giao dien
    public void createAttachFileNameLable(final Media media) {
        final Hlayout box = new Hlayout();
        A removeFileBtn = new A("xóa");

        box.appendChild(new Label(media.getName()));
        removeFileBtn.addEventListener(Events.ON_CLICK,
                new org.zkoss.zk.ui.event.EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                box.detach();
                // xoa file khoi danh sach file
                listMedia.remove(media);
            }
        });
        box.appendChild(removeFileBtn);
        boxContainFileName.appendChild(box);
    }

    @Listen("onChanging = #commentTextBox")
    public void loadComment(InputEvent event) {
        String valueOfComment = "";
        valueOfComment = event.getValue();
        ifdEvaluationComment.setContentEvaluation(valueOfComment);
        Clients.showNotification(valueOfComment);
    }

    //Xem thong bao ket qua
    @Listen("onClick = #viewResultBtn")
    public void onShowResult() {

    }

    //Trinh lanh dao
    @Listen("onClick = #sendBtn")
    public void onSubmitForm() {
       ifdEvaluation.setEvaluationContent(ifdEvaluationComment.getContentEvaluation());
       IfdEvaluationService service = new IfdEvaluationService();
       
//       service.saveEvaluation(ifdEvaluation);
////       service.saveEvaluationProduct(product);
//       service.saveEvaluationComment(ifdEvaluationComment);
    }
    
    public List<IfdEvaluationProduct> getEvaluationProducts(){
        List<IfdEvaluationProduct> listIfdEvaluationProduct = new ArrayList<>();

        List<Listitem> listitem = listProductLb.getItems();

        for (Listitem item : listitem) {
            IfdEvaluationProduct product = item.getValue();
            
            Listbox listBoxResult = (Listbox) item.getChildren().get(5).getChildren().get(0);
            Long result = Long.parseLong((String) listBoxResult.getSelectedItem().getValue());
            
            Textbox txtReason = (Textbox) item.getChildren().get(6).getChildren().get(0);
            String reason = txtReason.getValue();
            
            Listbox listBoxSolution = (Listbox) item.getChildren().get(7).getChildren().get(0);
            Long solution = Long.parseLong((String) listBoxSolution.getSelectedItem().getValue());
            
            Textbox txtNote = (Textbox) item.getChildren().get(8).getChildren().get(0);
            String note = txtNote.getValue();

            product.setCheckResultCode(result);
            String resultName = getCheckResultName(result);
            product.setCheckResultName(resultName);
            
            product.setReason(reason);
            
            product.setHandlingMeasuresCode(solution);
            String MeasuresName = getHandlingMeasuresName(solution);
            product.setHandlingMeasuresName(MeasuresName);
            
            product.setNote(note);
            
            product.setEvaluationId(ifdEvaluation.getEvaluationId());
            listIfdEvaluationProduct.add(product);
        }
        //luu cai list listIfdEvaluationProduct vao DB nhe
        return listIfdEvaluationProduct;
    }
    

    public String getCheckResultName(Long code){
        String result = "";
        int resultCode = Integer.parseInt(code+"");
        switch(resultCode){
            case 1: 
                result = "Đạt";
                break;
            case 2: 
                result = "Không đạt";
                break;
            case 3:
                result = "Yêu cầu bổ sung hồ sơ";
                break;
            case 4:
                result = "Sai luồng";    
        }
        return result;
    }
    
    public String getHandlingMeasuresName(Long code){
        String result = "";
        switch(Integer.parseInt(code+"")){
            case 1: 
                result = "Tái xuất";
                break;
            case 2: 
                result = "Tiêu hủy";   
        }
        return result;
    }

    //Dong cua so popup
    @Listen("onClick = #closeBtn")
    public void onCloseWindow() {
        viewFileHandlePopupExpertWnd.detach();
    }
}
