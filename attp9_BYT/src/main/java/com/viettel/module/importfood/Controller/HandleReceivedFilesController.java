/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.BO.RoleUserDept;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.DAO.RoleUserDeptDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProcessUser;
import com.viettel.module.importfood.Constant.FileStatus;
import com.viettel.module.importfood.Constant.RoleCodeType;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileProcessUserDAO;
import com.viettel.module.importfood.Model.IfdFileModel;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author Administrator
 */
public class HandleReceivedFilesController extends BaseComposer {

    @Wire
    private Window listFileReceivedWnd;

    @Wire
    private Button btnOpenSearchForm, btnSearch, btnExport;

    @Wire
    private Groupbox gb0, gb1;

    @Wire
    private Textbox txtFileCode, txtGoodsOwnerName;

    @Wire
    private Datebox dbComingDateForm, dbComingDateTo;

    @Wire
    private Listbox lboxFileStatus, listData;

    @Wire
    private Paging userPagingTop, userPagingBottom;

    private IfdFile searchFile;
    private List<IfdFileModel> listFile;
    String documentType;
    Long deptId = null;
    Long userId = null;
    Long roleId = null;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        documentType = (String) arguments.get("documentType");
        userPagingBottom.setActivePage(0);
        userPagingTop.setActivePage(0);
        dbComingDateTo.setValue(new Date());
        onLoad();
        onSearch();
    }

    @Listen("onView = #listData")
    public void onOpenViewPage(Event evt) {
        IfdFileModel viewFile = (IfdFileModel) listFile.get(listData.getSelectedIndex());
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", viewFile.getFileId());
        arguments.put("parentWindow", listFileReceivedWnd);
        createWindow("testWindView", "/Pages/module/import_food/fileImportFood/viewFileDetail.zul", arguments, Window.EMBEDDED);
        listFileReceivedWnd.setVisible(false);
    }

    @Listen("onClick = #btnOpenSearchForm")
    public void openSearchForm() {
        if (gb0.isVisible()) {
            gb0.setVisible(false);
        } else {
            gb0.setVisible(true);
        }
    }

    public void onLoad() {
        List<Long> listFileStatusId = new ArrayList<>();
        List<FileStatus> listFileStatus = new ArrayList<>();
        RoleUserDeptDAOHE roleUserDeptDAO = new RoleUserDeptDAOHE();
        List<RoleUserDept> lstRoleUserDepts = roleUserDeptDAO.findRoleUserDept(getUserId(), getDeptId());
        if (lstRoleUserDepts != null) {
            for (RoleUserDept roleUserDept : lstRoleUserDepts) {
                roleId = roleUserDept.getRoleId();
                List<Long> listTemp = FileStatus.getListFileStatusByRole(RoleCodeType.getByRoleId(roleId));
                if (listTemp != null && !listTemp.isEmpty()) {
                    listFileStatusId.addAll(listTemp);
                }
            }
        }
        for (Long fileStatusId : listFileStatusId) {
            FileStatus fileStatus = FileStatus.getByStatus(fileStatusId);
            listFileStatus.add(fileStatus);
        }
        listFileStatus.add(0, FileStatus.CHON);
        ListModelList<FileStatus> model = new ListModelList<>(listFileStatus);
        lboxFileStatus.setModel(model);
        lboxFileStatus.renderAll();
        if (!listFileStatus.isEmpty()) {
            lboxFileStatus.setSelectedIndex(0);
        }
    }

    @Listen("onReload = #listFileReceivedWnd")
    public void onReload() {
        fillData();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {

        userPagingBottom.setActivePage(0);
        userPagingTop.setActivePage(0);
        searchFile = new IfdFile();
        if (validate()) {
            fillData();
        }
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        fillData();
    }

    public void fillData() {
        UserToken userToken = getToken();
        deptId = userToken.getDeptId();
        userId = userToken.getUserId();
        RoleUserDeptDAOHE roleUserDeptDAO = new RoleUserDeptDAOHE();
        List<RoleUserDept> lstRoleUserDepts = roleUserDeptDAO.findRoleUserDept(userId, deptId);
        //lay deptCode
        Department dept = new DepartmentDAOHE().getById("deptId", deptId);
        List<Long> listFileStatus = new ArrayList<>();
        //lay ra list process neu co
        RoleCodeType roleCodeType = null;
        List<IfdFileProcessUser> listFileProcess;
        List<Long> listFileId = null;
        if (lstRoleUserDepts != null) {
            for (RoleUserDept roleUserDept : lstRoleUserDepts) {
                roleId = roleUserDept.getRoleId();
                roleCodeType = RoleCodeType.getByRoleId(roleId);
                if (roleCodeType != null) {
                    break;
                }
            }
        }
        if (roleCodeType != null) {
            listFileProcess = new IfdFileProcessUserDAO().findListIfdFileProcessUser(userId, dept.getDeptCode(), roleCodeType.getRoleCode());
            if (listFileProcess != null && !listFileProcess.isEmpty()) {
                listFileId = new ArrayList<>();
                for (IfdFileProcessUser fileProces : listFileProcess) {
                    listFileId.add(fileProces.getFileId());
                }
            }
        }
        //lay ra trang thai ung voi vai tro
        if (lstRoleUserDepts != null) {
            List<Long> listTemp = null;
            for (RoleUserDept roleUserDept : lstRoleUserDepts) {
                roleId = roleUserDept.getRoleId();

                listTemp = FileStatus.getListFileStatusByRole(RoleCodeType.getByRoleId(roleId));
                if (listTemp != null && !listTemp.isEmpty()) {
                    listFileStatus.addAll(listTemp);
                }
            }
        }

        //sử dụng tiếp listFileStatus
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        int take = userPagingBottom.getPageSize();
        IfdFileDAO dao = new IfdFileDAO();
        PagingListModel paging = dao.searchFile(searchFile, start, take, listFileStatus, dept.getDeptCode(), listFileId);
        userPagingTop.setTotalSize(paging.getCount());
        userPagingBottom.setTotalSize(paging.getCount());
        listFile = paging.getLstReturn();
        ListModelArray lstModel = new ListModelArray(listFile);
        listData.setModel(lstModel);
    }

    public String getCheckMethod(Long checkMethod) {
        String status = "";
        if (checkMethod != null) {
            if (IfdConstants.CHECK_METHOD.NORMAL == checkMethod) {
                status = IfdConstants.CHECK_METHOD.NORMAL_STR;
            } else {
                if (IfdConstants.CHECK_METHOD.TIGHT == checkMethod) {
                    status = IfdConstants.CHECK_METHOD.TIGHT_STR;
                }
            }
        }
        return status;
    }

    public boolean validate() {
        if (!"".equals(txtFileCode.getValue().trim()) && txtFileCode.getValue() != null) {
            searchFile.setFileCode(txtFileCode.getValue());
        }
        if (!"".equals(txtGoodsOwnerName.getValue().trim()) && txtGoodsOwnerName.getValue() != null) {
            searchFile.setGoodsOwnerName(txtGoodsOwnerName.getValue());
        }
        if (dbComingDateTo.getValue() != null && dbComingDateForm.getValue() != null) {
            if (dbComingDateForm.getValue().compareTo(dbComingDateTo.getValue()) > 0) {
                showNotification("Thời gian từ ngày không được lớn hơn thời gian đến ngày !", com.viettel.utils.Constants.Notification.WARNING, 3000);
                dbComingDateForm.focus();
                return false;
            }
        }
        if (dbComingDateForm.getValue() != null) {
            searchFile.setComingDateFrom(dbComingDateForm.getValue());
        }

        if (dbComingDateTo.getValue() != null) {
            searchFile.setComingDateTo(dbComingDateTo.getValue());
        }

        if (lboxFileStatus.getSelectedIndex() >= 0) {
            Listitem item = (Listitem) lboxFileStatus.getSelectedItem();
            FileStatus fileStatus = (FileStatus) item.getValue();
            searchFile.setFileStatus(fileStatus.getStatus());
        }

//        if (lboxCheckMethod.getSelectedIndex() != 0) {
//            searchFile.setCheckMethod(Long.valueOf(lboxCheckMethod.getSelectedItem().getValue().toString()));
//        } else {
//            searchFile.setCheckMethod(null);
//        }
        if (documentType != null && !"".equals(documentType)) {
            searchFile.setDocumentType(documentType);
        } else {
            searchFile.setDocumentType(null);
        }

        return true;
    }

    public boolean visibleColView() {
        RoleUserDeptDAOHE roleUserDeptDAO = new RoleUserDeptDAOHE();
        List<RoleUserDept> lstRoleUserDepts = roleUserDeptDAO.findRoleUserDept(getUserId(), getDeptId());
        if (lstRoleUserDepts != null) {
            for (RoleUserDept roleUserDept : lstRoleUserDepts) {
                roleId = roleUserDept.getRoleId();
                RoleCodeType roleCodeType = RoleCodeType.getByRoleId(roleId);
                if (roleCodeType.getRoleCode().equals(RoleCodeType.ATTP_KTNN_CV.getRoleCode())) {
                    return true;
                }
            }
        }
        return false;
    }
}
