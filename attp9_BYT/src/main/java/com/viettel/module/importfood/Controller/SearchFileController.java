/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Administrator
 */
public class SearchFileController extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    private Textbox txtFileCode, txtGoodsOwnerName;
    
    @Wire
    private Datebox dbComingDateForm, dbComingToDay;

    @Wire
    private Groupbox testSearchGbx;

 
    @Wire
    private Listbox lboxFileStatus,lboxCheckMethod;

    @Override
    public void doAfterCompose(Component cmpt) {
        try {
            super.doAfterCompose(cmpt);
        } catch (Exception e) {

        }
    }
}
