/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.Service.ViewDetailFileService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;

/**
 *
 * @author Administrator
 */
public class ViewDetailTabFileController extends BaseComposer {

    private static final long serialVersionUID = 1L;
//    private IfdFile ifdFile = new IfdFile();
//    private List<IfdFileProduct> listProduct ;
//    private Long fileID;
//    
    
    private Listbox lbxProduct;
//
//    @Override
//    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
//        String fileI = (String) Executions.getCurrent().getParameter("fileID");
//        fileID = Long.valueOf(fileI);
//         
//        ViewDetailFileService service = new ViewDetailFileService();
//        ifdFile = service.getIfdFileById(fileID);
//        
//        listProduct = new ArrayList<>();
//        listProduct = service.getListProductByIfFile(fileID);
//        ListModelArray listModelProduct = new ListModelArray(listProduct);
//        lbxProduct.setModel(listModelProduct);
//        return super.doBeforeCompose(page, parent, compInfo);
//
//    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
        } catch (Exception ex) {
            Logger.getLogger(ViewDetailTabFileController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onLoadInfor() {

    }

    public String getDate(Date date){
        String formatDate ="";
        if(date != null){
            SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
            formatDate = formate.format(date);
        }
        return formatDate; 
    }
//    public void setIfdFile(IfdFile ifdFile) {
//        this.ifdFile = ifdFile;
//    }
//
//    public IfdFile getIfdFile() {
//        return ifdFile;
//    }
//
//    public List<IfdFileProduct> getListProduct() {
//        return listProduct;
//    }
//
//    public void setListProduct(List<IfdFileProduct> listProduct) {
//        this.listProduct = listProduct;
//    }
//
//    public Long getFileID() {
//        return fileID;
//    }
//
//    public void setFileID(Long fileID) {
//        this.fileID = fileID;
//    }
//
//    
}
