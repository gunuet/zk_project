/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author Administrator
 */
public class ViewFileController extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbListIfdFile")
    private Listbox lbListIfdFile;
    @Wire
    private Window viewListIfdFile;
    private IfdFile searchFile;

    @Wire("#incTestSeach #testSearchGbx")
    private Groupbox testSearchGbx;

    @Wire("#incTestSeach #txtFileCode")
    private Textbox txtFileCode;

    @Wire("#incTestSeach #txtGoodsOwnerName")
    private Textbox txtGoodsOwnerName;

    @Wire("#incTestSeach #dbComingDateForm")
    private Datebox dbComingDateForm;

    @Wire("#incTestSeach #dbComingDateTo")
    private Datebox dbComingDateTo;

    @Wire("#incTestSeach #lboxFileStatus")
    private Listbox lboxFileStatus;

    @Wire("#incTestSeach #lboxCheckMethod")
    private Listbox lboxCheckMethod;

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            dbComingDateTo.setValue(new Date());
            searchFile = new IfdFile();
            onSearch();
        } catch (Exception ex) {
            Logger.getLogger(ViewFileController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Listen("onClick=#incTestSeach #btnSearch")
    public void onSearch() {
        userPagingBottom.setActivePage(0);
        userPagingTop.setActivePage(0);
        searchFile = new IfdFile();
        if (validate()) {
            fillData();
        }
    }

    public boolean validate() {
        if (!"".equals(txtFileCode.getValue().trim()) && txtFileCode.getValue() != null) {
            searchFile.setFileCode(txtFileCode.getValue());
        }
        if (!"".equals(txtGoodsOwnerName.getValue().trim()) && txtGoodsOwnerName.getValue() != null) {
            searchFile.setGoodsOwnerName(txtGoodsOwnerName.getValue());
        }
        if (dbComingDateTo.getValue() != null && dbComingDateForm.getValue() != null) {
            if (dbComingDateForm.getValue().compareTo(dbComingDateTo.getValue()) > 0) {
                showNotification("Thời gian từ ngày không được lớn hơn thời gian đến ngày !", com.viettel.utils.Constants.Notification.WARNING, 3000);
                dbComingDateForm.focus();
                return false;
            }
        }
        if (dbComingDateForm.getValue() != null) {
            searchFile.setComingDateFrom(dbComingDateForm.getValue());
        }

        if (dbComingDateTo.getValue() != null) {
            searchFile.setComingDateTo(dbComingDateTo.getValue());
        }

        if (lboxFileStatus.getSelectedIndex() != 0) {
            searchFile.setFileStatus(Long.valueOf(lboxFileStatus.getSelectedItem().getValue().toString()));
        } else {
            searchFile.setFileStatus(null);
        }

        if (lboxCheckMethod.getSelectedIndex() != 0) {
            searchFile.setCheckMethod(Long.valueOf(lboxCheckMethod.getSelectedItem().getValue().toString()));
        } else {
            searchFile.setCheckMethod(null);
        }

        return true;

    }

    public void fillData() {
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        int take = userPagingBottom.getPageSize();
        IfdFileDAO dao = new IfdFileDAO();
        PagingListModel paging = dao.searchFile(searchFile, start, take);
        userPagingTop.setTotalSize(paging.getCount());
        userPagingBottom.setTotalSize(paging.getCount());
        ListModelArray lstModel = new ListModelArray(paging.getLstReturn());
        lbListIfdFile.setModel(lstModel);
    }
    
    @Listen("onReload = #viewListIfdFile")
    public void onReload(){
        fillData();
    }

    @Listen("onOpenView= #incList #lbListIfdFile ")
    public void onOpenView(Event evt) {
        IfdFile viewFile = (IfdFile) evt.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", viewFile.getFileId());
        arguments.put("parentWindow", viewListIfdFile);
        createWindow("testWindView", "/Pages/module/import_food/fileImportFood/viewFileDetail.zul", arguments, Window.EMBEDDED);
        viewListIfdFile.setVisible(false);
    }

    @Listen("onOpenEdit= #incList #lbListIfdFile ")
    public void onOpenEdit(Event evt) {
        IfdFile viewFile = (IfdFile) evt.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("fileID", viewFile.getFileId());
        arguments.put("crudmode", "update");
        arguments.put("parentWindow", viewListIfdFile);
        createWindow("windowCreateIfd", "/Pages/module/import_food/fileImportFood/edit_file.zul", arguments, Window.EMBEDDED);
        viewListIfdFile.setVisible(false);
    }
    
    @Listen("onOpenDelete = #incList #lbListIfdFile")
    public void onOpendDelete(Event evt){
        String message, title;
        message = "Bạn có muốn xóa hồ sơ này ?";
        title = "DELETE";
        
        Messagebox.show(message, title, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener(){
            @Override
            public void onEvent(Event t) throws Exception {
                String ms = "", type = "";
                switch(t.getName()){
                    case Messagebox.ON_OK: 
                        try{
                            IfdFile delFile = (IfdFile) lbListIfdFile.getSelectedItem().getValue();
                            IfdFileDAO fileDao = new IfdFileDAO();
                            fileDao.deleteByFileId(delFile.getFileId());
                            ms = "Xóa thành công";
                            type = "info";
                            onReload();
                        }catch(Exception ex){
                            ms = "Đã xảy ra lỗi: " + ex.getMessage();
                            type = "error";
                        }
                        Clients.showNotification(ms, type, null, "top_center", 3000);
                        break;
                    case Messagebox.ON_CANCEL:
                        break;
                }
            }            
        });       
    }

    public String getCheckMethod(Long checkMethod) {
        String status = "";
        if (checkMethod != null) {
            if (IfdConstants.CHECK_METHOD.NORMAL == checkMethod) {
                status = IfdConstants.CHECK_METHOD.NORMAL_STR;
            } else {
                if (IfdConstants.CHECK_METHOD.TIGHT == checkMethod) {
                    status = IfdConstants.CHECK_METHOD.TIGHT_STR;
                }
            }
        }
        return status;
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        fillData();
    }

    @Listen("onShowFullSearch = #viewListIfdFile")
    public void onShowFullSearch() {
        if (testSearchGbx.isVisible()) {
            testSearchGbx.setVisible(false);
        } else {
            testSearchGbx.setVisible(true);
        }

    }

    @Listen("onClick =  #btnCreate")
    public void onCreateIfdFile() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", viewListIfdFile);
        arguments.put("fileId", "");
        arguments.put("crudmode", "create");
        Window window = (Window) Executions.createComponents("/Pages/module/import_food/fileImportFood/createIfdFile.zul", null, arguments);
        window.doModal();
    }

    @Listen("onCreateIfdFileSaved =#viewListIfdFile")
    public void onCreateIfdFileSaved(Event event) {
        onSearch();
    }
}
