/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.user.BO.RoleUserDept;
import com.viettel.core.user.DAO.RoleUserDeptDAOHE;
import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.Constant.RoleCodeType;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileProductDAO;
import com.viettel.module.importfood.Model.ViewFileModel;
import com.viettel.module.importfood.Service.IfdAttachmentService;
import com.viettel.module.importfood.Service.IfdFileProductService;
import com.viettel.module.importfood.Service.ViewDetailFileService;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Administrator
 */
public class ViewFileDetailController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    private IfdFile ifdFile = new IfdFile();
    private Long fileID;
    private ViewFileModel fileModel;

    @Wire
    private Window windowViewDetail;
    // Thông tin chi tiết lô hàng ( Hồ sơ)
    @Wire("#incViewTabFile #lbxProduct")
    private Listbox lbxProduct;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        fileID = (Long) Executions.getCurrent().getArg().get("id");
        IfdFileDAO dao = new IfdFileDAO();
        ifdFile = (IfdFile) dao.getById("fileId", fileID);
        fileModel = new ViewFileModel(ifdFile);
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            onLoadProductList();
        } catch (Exception ex) {
            Logger.getLogger(ViewFileDetailController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onLoadProductList() {
        IfdFileProductService service = new IfdFileProductService();
        List<IfdFileProduct> listProduct = new ArrayList();
        listProduct = service.findAllByFileId(fileID);

        if (listProduct != null && !listProduct.isEmpty()) {
            ListModelArray listModelProduct = new ListModelArray(listProduct);
            lbxProduct.setModel(listModelProduct);
        }
    }

    @Listen("onGoBack = #windowViewDetail #incToolBarAction ")
    public void onGoBackToMainPage() {
        Clients.showNotification("It run!!");
    }

    @Listen("onViewFlow = #windowViewDetail #incToolBarAction")
    public void onViewFlow() {
        Clients.showNotification("It onViewFlow!!");
    }

    @Listen("onViewExportFile = #windowViewDetail #incToolBarAction")
    public void onViewExportFile() {
        Clients.showNotification("It onViewExportFile!!");
    }

    @Listen("onViewHandleFile = #windowViewDetail #incToolBarAction")
    public void onViewHandleFile() {
        Map<String, Object> map = new ConcurrentHashMap<>();
        map.put("file", ifdFile);
        map.put("parentWindow", windowViewDetail);
        RoleUserDeptDAOHE roleUserDeptDAO = new RoleUserDeptDAOHE();
        List<RoleUserDept> lstRoleUserDepts = roleUserDeptDAO.findRoleUserDept(getUserId(), getDeptId());
        if (lstRoleUserDepts != null) {
            for (RoleUserDept roleUserDept : lstRoleUserDepts) {
                RoleCodeType roleCodeType = RoleCodeType.getByRoleId(roleUserDept.getRoleId());
                if (roleCodeType.getRoleCode().equals(RoleCodeType.ATTP_KTNN_KT.getRoleCode())) {
                    createWindow("viewFileHandlePopupAccountantWnd", "/Pages/module/import_food/fileImportFood/viewFileHandle_Accountant.zul", map, Window.POPUP);
                    break;
                } else if (roleCodeType.getRoleCode().equals(RoleCodeType.ATTP_KTNN_CV.getRoleCode())) {
                    createWindow("viewFileHandlePopupExpertWnd", "/Pages/module/import_food/fileImportFood/viewFileHandle_Expert.zul", map, Window.POPUP);
                    break;
                } else if (roleCodeType.getRoleCode().equals(RoleCodeType.ATTP_KTNN_LĐP.getRoleCode())) {
                    break;
                } else if (roleCodeType.getRoleCode().equals(RoleCodeType.ATTP_KTNN_LĐĐV.getRoleCode())) {
                    break;
                }
            }
        }
    }

    public void setIfdile(IfdFile ifdFile) {
        this.ifdFile = ifdFile;
    }

    public IfdFile getIfdFile() {
        return ifdFile;
    }

    public Long getFileID() {
        return fileID;
    }

    public void setFileID(Long fileID) {
        this.fileID = fileID;
    }

    public ViewFileModel getFileModel() {
        return fileModel;
    }

    public void setFileModel(ViewFileModel fileModel) {
        this.fileModel = fileModel;
    }

}
