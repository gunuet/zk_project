/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileFeeNotice;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.Model.IfdFileFeeNoticeModel;
import com.viettel.module.importfood.Model.IfdFilePaymentModel;
import com.viettel.module.importfood.Service.IfdFileFeeNoticeService;
import com.viettel.module.importfood.Service.IfdFilePaymentService;
import com.viettel.module.importfood.Service.ViewDetailFileService;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;

/**
 *
 * @author Administrator
 */
public class ViewTabFileFeeController extends BaseComposer{
     private static final long serialVersionUID = 1L;
    private IfdFile ifdFile = new IfdFile();
    private Long fileID;

   //Thông tin phí
    @Wire
    private Listbox lbxFilePayment;
    
    @Wire
    private Label lbFee, lbNumOfProduct, lbTotal;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        String fileI = (String) Executions.getCurrent().getParameter("fileID");
        fileID = Long.valueOf(fileI);
        IfdFileDAO dao = new IfdFileDAO();
        ifdFile = (IfdFile) dao.getById("fileId", fileID);
        
//        feeNotice = new IfdFileFeeNoticeModel();
        return super.doBeforeCompose(page, parent, compInfo);

    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            onLoadFilePayment();
            onLoadFileFee();
        } catch (Exception ex) {
            Logger.getLogger(ViewTabFileFeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onLoadFileFee() {
        IfdFileFeeNoticeService service = new IfdFileFeeNoticeService();       
        IfdFileFeeNoticeModel fee = service.findLatestModelByFileId(fileID);  
        
        lbFee.setValue(fee.getFeeDefaut()+"");
        lbNumOfProduct.setValue(fee.getNumberOfProduct()+"");
        lbTotal.setValue(fee.getTotalOfFee()+"");       
    }
    
    public void onLoadFilePayment(){
        IfdFilePaymentService service = new IfdFilePaymentService();

        List<IfdFilePaymentModel> paymentList = service.findAllByFileId(fileID);
        
        if (paymentList != null && !paymentList.isEmpty()) {
            ListModelArray listModel = new ListModelArray(paymentList);
            lbxFilePayment.setModel(listModel);
        }
    }
    
    @Listen("onDownloadAttFile = #lbxFilePayment")
    public void onDownloadEvaluateAttachment(){
        Clients.showNotification("Selected index: " + lbxFilePayment.getSelectedIndex());
    }
    
    @Listen("onViewAttFile = #lbxFilePayment")
    public void onDownloadCertificateAttachment(){
        Clients.showNotification("Selected index: " + lbxFilePayment.getSelectedIndex());
    }
}
