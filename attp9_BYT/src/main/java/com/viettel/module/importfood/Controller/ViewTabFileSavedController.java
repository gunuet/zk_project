/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.Service.IfdAttachmentService;
import com.viettel.module.importfood.Service.ViewDetailFileService;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;

/**
 *
 * @author Administrator
 */
public class ViewTabFileSavedController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    private IfdFile ifdFile = new IfdFile();
    private Long fileID;

    @Wire
    private Listbox lbxFileSaved, lbxResult;
    
    public void setIfdFile(IfdFile ifdFile) {
        this.ifdFile = ifdFile;
    }

    public IfdFile getIfdFile() {
        return ifdFile;
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        String fileI = (String) Executions.getCurrent().getParameter("fileID");
        fileID = Long.valueOf(fileI);
        IfdFileDAO dao = new IfdFileDAO();
        ifdFile = (IfdFile) dao.getById("fileId", fileID);
        return super.doBeforeCompose(page, parent, compInfo);

    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            onLoadEvaluationFileAttach();
//            onLoadFileAttach();
        } catch (Exception ex) {
            Logger.getLogger(ViewTabFileSavedController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onLoadEvaluationFileAttach() {
        IfdAttachmentService service = new IfdAttachmentService();
        
        List<IfdAttachment> listEvaluationAttFile = new ArrayList();
        listEvaluationAttFile = service.findAttachmentEvaluationByFileID(fileID);
        
        List<IfdAttachment> listCertificateAttFile = new ArrayList();
        listCertificateAttFile = service.findAttachmentCertificateByFileID(fileID);
        
        //Nap model cho listbox
        if(listEvaluationAttFile!=null && !listEvaluationAttFile.isEmpty()){
            ListModelArray evaluateAttList = new ListModelArray(listEvaluationAttFile);
            lbxFileSaved.setModel(evaluateAttList);
        }
        
        if(listCertificateAttFile!=null && !listCertificateAttFile.isEmpty()){
            ListModelArray certificateAttList = new ListModelArray(listCertificateAttFile);
            lbxResult.setModel(certificateAttList);
        }               
    }
    
    public void onLoadCertificateFileAttach(){
        
    }

    @Listen("onDownloadFileAttachment = #lbxFileSaved")
    public void onDownloadEvaluateAttachment(){
        Clients.showNotification("Selected index: " + lbxFileSaved.getSelectedIndex());
    }
    @Listen("onDownloadFileAttachment = #lbxResult")
    public void onDownloadCertificateAttachment(){
        Clients.showNotification("Selected index: " + lbxResult.getSelectedIndex());
    }
}
