/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFileResult;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author vietn
 */
public class FileResultDAO extends GenericDAOHibernate<IfdFileResult, Long>{
    
    public FileResultDAO() {
        super(IfdFileResult.class);
    }
    
    public void saveOrUpdate(IfdFileResult file){
        session.saveOrUpdate(file);
    }
    
    public IfdFileResult findByFileResultId(Long id){        
        Query query = session.getNamedQuery("IfdFileResult.findByFileResultId");
        query.setParameter("fileResultId", id);
        
        return (IfdFileResult) query.uniqueResult();
    }
    
    public IfdFileResult findByReceiveNo(String number){
        StringBuilder hql = new StringBuilder("SELECT i FROM IfdFileResult i WHERE i.receiveNo = :receiveNo ORDER BY i.fileResultId DESC");
//        Query query = session.getNamedQuery("IfdFileResult.findByReceiveNo");
        Query query = session.createQuery(hql.toString());
        query.setParameter("receiveNo", number);
        
        List<IfdFileResult> rsList = query.list();
        return rsList.get(0); 
    }
}
