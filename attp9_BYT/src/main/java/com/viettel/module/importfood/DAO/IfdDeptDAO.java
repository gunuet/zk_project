/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdDept;
import com.viettel.module.importfood.BO.IfdFileResult;
import java.util.Calendar;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class IfdDeptDAO extends GenericDAOHibernate<IfdDept, Long>{

    public IfdDeptDAO() {
        super(IfdDept.class);
    }
    
    public IfdDept findByDeptcode(String deptcode){
        Query query = session.getNamedQuery("IfdDept.findByDeptcode");
        query.setParameter("deptcode", deptcode);
        
        return (IfdDept) query.uniqueResult();
    }
    
    public void updateIncrementNumber(String deptcode, int num){
        StringBuilder hql = new StringBuilder("Update IfdDept d Set d.numberincre = :numberincre Where d.deptcode = :deptcode"); 
        Query query = session.createQuery(hql.toString());
        query.setParameter("numberincre", num);
        query.setParameter("deptcode", deptcode);
        
        query.executeUpdate();
    }
    
    public void updateIncrementNumber(String deptcode, int num, int year){
        StringBuilder hql = new StringBuilder("Update IfdDept d Set d.numberincre = :numberincre, d.year = :year Where d.deptcode = :deptcode"); 
        Query query = session.createQuery(hql.toString());
        query.setParameter("deptcode", deptcode);
        query.setParameter("numberincre", num);
        query.setParameter("year", year);
        
        query.executeUpdate();
    }
    
    public void saveOrUpdate(IfdDept file){
        session.saveOrUpdate(file);
    }
}
