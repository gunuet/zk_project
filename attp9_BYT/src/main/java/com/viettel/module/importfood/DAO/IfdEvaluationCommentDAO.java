/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdEvaluationComment;

/**
 *
 * @author Administrator
 */
public class IfdEvaluationCommentDAO extends GenericDAOHibernate<IfdEvaluationComment, Long> {
    
    public IfdEvaluationCommentDAO() {
        super(IfdEvaluationComment.class);
    }
    
    public IfdEvaluationCommentDAO(Class<IfdEvaluationComment> type) {
        super(type);
    }
    
    public void save(IfdEvaluationComment comment){
        session.save(comment);
    }
    
}
