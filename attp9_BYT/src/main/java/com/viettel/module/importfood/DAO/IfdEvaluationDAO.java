/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdEvaluation;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class IfdEvaluationDAO extends GenericDAOHibernate<IfdEvaluation, Long>{
    
    public IfdEvaluationDAO() {
        super(IfdEvaluation.class);
    }
    
    public IfdEvaluationDAO(Class<IfdEvaluation> type) {
        super(type);
    }
    
    public void save(IfdEvaluation evaluation){
        session.save(evaluation);
    }
    
    public IfdEvaluation findLatestByIfdFileID(Long fileId){
        StringBuilder hql = new StringBuilder("Select e From IfdEvaluation e Where e.fileId = :fileId Order By e.evaluationId desc");
        Query query = session.createQuery(hql.toString());
        
        query.setParameter("fileId", fileId);
        return (IfdEvaluation) query.list().get(0);
    }
}
