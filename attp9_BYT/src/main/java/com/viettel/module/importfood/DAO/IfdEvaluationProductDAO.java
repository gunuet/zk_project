/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdEvaluationProduct;

/**
 *
 * @author Administrator
 */
public class IfdEvaluationProductDAO extends GenericDAOHibernate<IfdEvaluationProduct, Long> {
    
    public IfdEvaluationProductDAO() {
        super(IfdEvaluationProduct.class);
    }
    
    public IfdEvaluationProductDAO(Class<IfdEvaluationProduct> type) {
        super(type);
    }
    
    public void save(IfdEvaluationProduct product){
        session.save(product);
    }
}
