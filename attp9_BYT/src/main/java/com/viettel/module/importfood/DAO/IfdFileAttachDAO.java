/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Filedownload;

/**
 *
 * @author freestyle
 */
public class IfdFileAttachDAO extends GenericDAOHibernate<IfdAttachment, Long> {

    public IfdFileAttachDAO() {
        super(IfdAttachment.class);
    }

    public IfdFileAttachDAO(Class<IfdAttachment> type) {
        super(type);
    }

    public List<IfdAttachment> findByFileId(long fileId) {
        List<IfdAttachment> list;
        try {
            Query query = getSession().getNamedQuery("IfdAttachment.findByObjectId");
            query.setParameter("objectId", fileId);
            list = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return list;
    }

    @Override
    public IfdAttachment findById(Long attachId) {
        Query query = getSession().getNamedQuery("IfdAttachment.findByAttachId");
        query.setParameter("attachId", attachId);
        List list = query.list();
        if (list.isEmpty()) {
            return null;
        }
        return (IfdAttachment) list.get(0);
    }
    
    public void save(IfdAttachment file){
        session.save(file);
    }

    public void downloadFileAttach(IfdAttachment att) {
        if (att != null) {

            String path = att.getAttachPath();
            File f = new File(path);
            if (f.exists()) {
                try {
                    File tempFile = FileUtil.createTempFile(f, att.getAttachmentName());
                    Filedownload.save(tempFile, path);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(IfdFileAttachDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                Clients.showNotification("File không còn tồn tại trên hệ thống!!", Constants.Notification.INFO, null, "after_end", 3000);
            }
        } else {
            Clients.showNotification("File không còn tồn tại trên hệ thống!!", Constants.Notification.INFO, null, "after_end", 3000);
        }
    }

    public List<IfdAttachment> findCheckedFilecAttach(Long obj_type, Long obj_id) {
        Query query=this.getSession().createQuery("from IfdAttachment where objectType=:objectType and objectId=:objectId");
        query.setParameter("objectType", obj_type);
        query.setParameter("objectId", obj_id);
        List<IfdAttachment> attachs=query.list();
        if(attachs.isEmpty())
        {
            return new ArrayList<>();
        }else
        {
            return attachs;
        }
    }
}
