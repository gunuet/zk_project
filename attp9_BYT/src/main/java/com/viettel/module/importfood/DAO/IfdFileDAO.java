/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFile;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Admin
 */
public class IfdFileDAO extends
        GenericDAOHibernate<IfdFile, Long>{
    
    public IfdFileDAO() {
        super(IfdFile.class);
    }
    
    public void save(IfdFile file){
        session.saveOrUpdate(file);
    }
    
    public void deleteByFileId(Long id){
        StringBuilder hql = new StringBuilder("Update IfdFile f Set f.isDelete = 1 where f.fileId = :fileId");
        Query query = session.createQuery(hql.toString());
        query.setParameter("fileId", id);
        query.executeUpdate();
    }
    
    public IfdFile findByFileCode(String fileCode){
        StringBuilder hql = new StringBuilder("select f from IfdFile f where f.fileCode = :fileCode");
        Query query = session.createQuery(hql.toString());
        query.setParameter("fileCode", fileCode);
        return (IfdFile) query.uniqueResult();
    }

    public List<IfdFile> filter(String fileCode, int status, String ownerName, 
            Date comingDateFrom, Date comingDateTo,
            int take, int start){
        
        Criteria cr  = session.createCriteria(IfdFile.class);
        if(fileCode != null && !"".equals(fileCode.trim())){
            cr.add(Restrictions.like("fileCode", "%" + fileCode + "%"));
        }
        //cr.add(Restrictions.eq("status", (short) status));
        if(ownerName != null && !"".equals(ownerName.trim())){
            cr.add(Restrictions.like("goodsOwnerName", "%" +ownerName + "%"));
        }
        if(comingDateFrom != null){
          cr.add(Restrictions.ge("comingDateFrom", comingDateFrom));
        }
        if(comingDateTo != null){
           cr.add(Restrictions.le("comingDateTo", comingDateTo));
        }
        System.out.println(take + "/" + start);
        cr.setMaxResults(take);
        cr.setFirstResult(start);
        
        List<IfdFile> listFile = cr.list();
        System.out.println(listFile);
        return listFile;
    }
    
    public Long count(String fileCode, int status, String ownerName, 
            Date comingDateFrom, Date comingDateTo){
        
        Criteria cr  = session.createCriteria(IfdFile.class);
        if(fileCode != null && !"".equals(fileCode.trim())){
            cr.add(Restrictions.like("fileCode", "%" + fileCode + "%"));
        }
        //cr.add(Restrictions.eq("status", (short) status));
        if(ownerName != null && !"".equals(ownerName.trim())){
            cr.add(Restrictions.like("goodsOwnerName", "%" +ownerName + "%"));
        }
        if(comingDateFrom != null){
          cr.add(Restrictions.ge("comingDateFrom", comingDateFrom));
        }
        if(comingDateTo != null){
           cr.add(Restrictions.le("comingDateTo", comingDateTo));
        }
        
        cr.setProjection(Projections.rowCount());
        Long count = (Long) cr.uniqueResult();
        System.out.println("count + " + count);
        
        return count;
    }

    public IfdFile findById(long id)
    {
        Query query=this.getSession().getNamedQuery("IfdFile.findByFileId");
        query.setParameter("fileId", id);
        List result=query.list();
        if(result.isEmpty())
        {
            return null;
        }else
        {
            return (IfdFile)result.get(0);
        }
    }
}
