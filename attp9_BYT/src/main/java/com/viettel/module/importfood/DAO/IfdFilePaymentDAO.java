/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;

/**
 *
 * @author Admin
 */
public class IfdFilePaymentDAO extends
        GenericDAOHibernate<IfdFilePayment, Long>{
    
    public IfdFilePaymentDAO() {
        super(IfdFilePayment.class);
    }
    
    @Override
    public IfdFilePayment findById(Long id)
    {
        Query query=this.getSession().getNamedQuery("IfdFilePayment.findByPaymentId");
        query.setParameter("paymentId", id);
        List result=query.list();
        if(result.isEmpty())
        {
            return null;
        }else
        {
            return (IfdFilePayment)result.get(0);
        }
    }
    public List<IfdFilePayment> findByFileId(long fileId)
    {
        List<IfdFilePayment> list;
        try {
            Query query = getSession().getNamedQuery("IfdFilePayment.findByFileId");
            query.setParameter("fileId", fileId);
            list = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return list;

    }
}
