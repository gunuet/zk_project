/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author freestyle
 */
public class ImportFoodProductDAO extends GenericDAOHibernate<IfdFileProduct, Long>{

    public ImportFoodProductDAO() {
        super(IfdFileProduct.class);
    }
    
    public ImportFoodProductDAO(Class<IfdFileProduct> type) {
        super(type);
    }
    public List<IfdFileProduct> findByFileId(long fileId)
    {
        List<IfdFileProduct> list;
        try {
            Query query = getSession().getNamedQuery("IfdFileProduct.findByFileId");
            query.setParameter("fileId", fileId);
            list = query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return new ArrayList<>();
        }
        return list;
    }
}
