/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO;

import java.util.Map;
import org.zkoss.zul.Window;

/**
 *
 * @author Administrator
 */
public interface IntegratorVMC {

    void addChildren(String id, IntegratorVMC child);

    void handleEvent(String eventName, Object data);

    <T> T getData(String eventName, Class<T> type, Map<String, Object> arguments);

    Window createWindow(String id, String url, Map<String, Object> arg, int mode);
}
