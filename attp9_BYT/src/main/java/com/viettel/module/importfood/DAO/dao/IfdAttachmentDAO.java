/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class IfdAttachmentDAO extends GenericDAOHibernate<IfdAttachment,Long> {

    public IfdAttachmentDAO() {
        super(IfdAttachment.class);
    }

    @Override
    public void saveOrUpdate(IfdAttachment o) {
        if (o != null){
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null){
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
//            getSession().getTransaction().commit();
        }
        
    }
    
    
    
    
    public List<IfdAttachment> findListAttachmentByIdIfdFile(Long idIfdFile){
        List<IfdAttachment> searchList =  new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdAttachment t");
        if(idIfdFile != null){
            strBuil.append(" WHERE t.objectId = "+ idIfdFile );
        }
        Query query = session.createQuery(strBuil.toString());
        searchList = query.list();
        return searchList;
    }
    
    public List<IfdAttachment> findListAttachmentByObjectTypeAndObjectId(Long objectType, Long objectId){
        List<IfdAttachment> searchList ;
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdAttachment t WHERE t.status = :active_status");
        if(objectType != null){
            strBuil.append(" AND t.objectType = "+ objectType );
        }
        if(objectId != null){
            strBuil.append(" AND t.objectId = "+ objectId );
        }
        strBuil.append(" order by t.attachId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("active_status", IfdConstants.OBJECT_STATUS.ACTIVE);
        searchList = query.list();
        return searchList;
    }
    
    
}
