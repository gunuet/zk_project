/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdCertificate;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class IfdCertificateDAO extends GenericDAOHibernate<IfdCertificate, Long> {

    public IfdCertificateDAO() {
        super(IfdCertificate.class);
    }

    public IfdCertificateDAO(Class<IfdCertificate> type) {
        super(type);
    }

    public IfdCertificate findLatestByFileID(Long fileId) {
        IfdCertificate result = null;
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdCertificate t WHERE t.fileId = :fileId AND t.status = :status");
        strBuil.append(" order by t.certificateId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", fileId);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        List<IfdCertificate> lstResults = query.list();
        if (lstResults != null && !lstResults.isEmpty()) {
            result = lstResults.get(0);
        }
        return result;
    }

    @Override
    public void saveOrUpdate(IfdCertificate o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setCreateDate(new Date());
            o.setUpdateDate(new Date());
            super.saveOrUpdate(o);
        }
    }

}
