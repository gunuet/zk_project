/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdCertificateProduct;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author quannn
 */
public class IfdCertificateProductDAO extends GenericDAOHibernate<IfdCertificateProduct,Long> {

    public IfdCertificateProductDAO() {
        super(IfdCertificateProduct.class);
    }
    
    public void saveAll(List<IfdCertificateProduct> list){
        for(IfdCertificateProduct pr: list){
            session.saveOrUpdate(pr);
        }
    }
    
    public List<IfdCertificateProduct> findAllByCertificateId(Long certificateId){
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdCertificateProduct t WHERE t.certificateId = :certificateId AND t.status = :status");
        strBuil.append(" order by t.certificateProductId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("certificateId", certificateId);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        return query.list();
    }
    
    public List<IfdCertificateProduct> findListFileResultByIdIfdCertificate(Long idIfdCertificate){
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdCertificateProduct t WHERE t.status = :status");
        if(idIfdCertificate != null){
            strBuil.append(" AND t.certificateId = :certificateId ");
        }
        strBuil.append(" order by t.certificateProductId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("certificateId", idIfdCertificate);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE );
        return query.list();
    }

    @Override
    public void saveOrUpdate(IfdCertificateProduct o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null){
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
}
