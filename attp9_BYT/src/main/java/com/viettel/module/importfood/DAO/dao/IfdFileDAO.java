/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.BO.test.TestFile;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.Constant.FileStatus;
import com.viettel.module.importfood.Model.IfdFileModel;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.StringUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class IfdFileDAO extends GenericDAOHibernate<IfdFile, Long> {

    public IfdFileDAO() {
        super(IfdFile.class);
    }

    public List<TestFile> findListTestFile(IfdFile search) {
        List<IfdFile> searchList = new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFile t WHERE t.status = :status");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        return query.list();
    }

    public void deleteByFileId(Long id) {
        StringBuilder hql = new StringBuilder("Update IfdFile f Set f.status = :status"
                + " where f.fileId = :fileId");
        Query query = session.createQuery(hql.toString());
        query.setParameter("status", IfdConstants.OBJECT_STATUS.INACTIVE);
        query.setParameter("fileId", id);
        query.executeUpdate();
    }

    public PagingListModel searchFile(IfdFile search, int start, int take) {
        List listParam = new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFile t WHERE t.status = ?");
        StringBuilder strCountBuil = new StringBuilder(" SELECT COUNT(t.fileId) FROM IfdFile t WHERE t.status = ?");
        StringBuilder sql = new StringBuilder();
        listParam.add(IfdConstants.OBJECT_STATUS.ACTIVE);
        if (search != null) {
            if (search.getFileCode() != null) {
                sql.append(" AND t.fileCode like ? escape '/'");
                listParam.add(search.getFileCode());
            }
            if (search.getGoodsOwnerName() != null) {
                sql.append(" AND t.goodsOwnerName like ? escape '/'");
                listParam.add(search.getGoodsOwnerName());
            }
            if (search.getFileStatus() != null) {
                sql.append(" AND t.fileStatus = ? ");
                listParam.add(search.getFileStatus());
            }
            if (search.getComingDateFrom() != null) {
                sql.append(" AND t.comingDateFrom >= ? ");
                listParam.add(search.getComingDateFrom());
            }
            if (search.getComingDateTo() != null) {
                sql.append(" AND t.comingDateTo <= ? ");
                listParam.add(DateTimeUtils.addOneDay(search.getComingDateTo()));
            }
            if (search.getCheckMethod() != null) {
                sql.append(" AND t.checkMethod = ? ");
                listParam.add(search.getCheckMethod());
            }

        }
        sql.append(" order by t.fileId desc ");
        strBuil.append(sql);
        strCountBuil.append(sql);
        Query query = session.createQuery(strBuil.toString());
        Query queryCount = session.createQuery(strCountBuil.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            queryCount.setParameter(i, listParam.get(i));
        }
        query.setFirstResult(start);
        query.setMaxResults(take);
        List lst = query.list();
        Long count = (Long) queryCount.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    public PagingListModel searchFile(IfdFile search, int start, int take, String searchStatus) {
        List listParam = new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFile t WHERE t.status = ?");
        StringBuilder strCountBuil = new StringBuilder(" SELECT COUNT(t.fileId) FROM IfdFile t WHERE t.status = ?");
        StringBuilder sql = new StringBuilder();
        listParam.add(IfdConstants.OBJECT_STATUS.ACTIVE);

        if (!searchStatus.toLowerCase().equals("all")) {
            if (searchStatus.toLowerCase().equals("waiting")) {
                sql.append(" AND (t.fileStatus = ? OR t.fileStatus = ? OR t.fileStatus = ? OR t.fileStatus = ?)");
                listParam.add(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatus());
                listParam.add(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatus());
                listParam.add(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatus());
                listParam.add(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus());
            }
        }

        if (search != null) {
            if (search.getFileCode() != null) {
                sql.append(" AND t.fileCode like ? escape '/'");
                listParam.add(search.getFileCode());
            }
            if (search.getGoodsOwnerName() != null) {
                sql.append(" AND t.goodsOwnerName like ? escape '/'");
                listParam.add(search.getGoodsOwnerName());
            }
            if (search.getFileStatus() != null) {
                sql.append(" AND t.fileStatus = ? ");
                listParam.add(search.getFileStatus());
            }
            if (search.getComingDateFrom() != null) {
                sql.append(" AND t.comingDateFrom >= ? ");
                listParam.add(search.getComingDateFrom());
            }
            if (search.getComingDateTo() != null) {
                sql.append(" AND t.comingDateTo <= ? ");
                listParam.add(DateTimeUtils.addOneDay(search.getComingDateTo()));
            }
            if (search.getCheckMethod() != null) {
                sql.append(" AND t.checkMethod = ? ");
                listParam.add(search.getCheckMethod());
            }
            if (search.getDocumentType() != null && !"".equals(search.getDocumentType())) {
                sql.append(" AND t.documentType = ? ");
                listParam.add(search.getDocumentType());
            }

        }
        sql.append(" order by t.fileId desc ");
        strBuil.append(sql);
        strCountBuil.append(sql);
        Query query = session.createQuery(strBuil.toString());
        Query queryCount = session.createQuery(strCountBuil.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            queryCount.setParameter(i, listParam.get(i));
        }
        query.setFirstResult(start);
        query.setMaxResults(take);
        List<IfdFile> lst = query.list();

        List<IfdFileModel> lstModel = new ArrayList<>();
        for (IfdFile object : lst) {
            IfdFileModel objectModel = new IfdFileModel();
            objectModel = object.convertToIfdFileModel(object);
            lstModel.add(objectModel);
        }
        Long count = (Long) queryCount.uniqueResult();
        PagingListModel pagingListModel = new PagingListModel(lstModel, count);
        return pagingListModel;
    }

    public PagingListModel searchFile(IfdFile search, int start, int take, List<Long> lstStatus, String deptCode, List<Long> listFileId) {
//        List listParam = new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFile t WHERE t.status = :status");
        StringBuilder strCountBuil = new StringBuilder(" SELECT COUNT(t.fileId) FROM IfdFile t WHERE t.status = :status");
        StringBuilder sql = new StringBuilder();
//        listParam.add(IfdConstants.OBJECT_STATUS.ACTIVE);
        if (deptCode != null && null == listFileId) {
            sql.append(" AND t.checkDeptCode = :checkDeptCode");
        } else if (deptCode != null && listFileId != null && !listFileId.isEmpty()) {
            sql.append(" AND (t.checkDeptCode = :checkDeptCode OR t.fileId in (:listFileId))");
        }
        if (lstStatus != null && !lstStatus.isEmpty()) {
            sql.append(" AND t.fileStatus in (:lstFileStatus)");
        }
//        if (listFileId != null && !listFileId.isEmpty()) {
//            sql.append(" OR t.fileId in (:listFileId)");
//        }

        if (search != null) {
            if (search.getFileCode() != null && search.getFileCode().trim().length() > 0) {
                sql.append(" AND lower(t.fileCode) like :fileCode escape '/' ");
            }

            if (search.getGoodsOwnerName() != null && search.getGoodsOwnerName().trim().length() > 0) {
                sql.append(" AND lower(t.goodsOwnerName) like :goodsOwnerName escape '/'");
            }
            if (search.getFileStatus() != null && search.getFileStatus() > -1L) {
                sql.append(" AND t.fileStatus = :fileStatus ");
            }
            if (search.getComingDateFrom() != null) {
                sql.append(" AND t.createDate >= :comingDateFrom ");
            }
            if (search.getComingDateTo() != null) {
                sql.append(" AND t.createDate < :comingDateTo ");
            }
            if (search.getDocumentType() != null && !"".equals(search.getDocumentType())) {
                sql.append(" AND t.documentType = :documentType ");
            }

        }
        sql.append(" order by t.fileId desc ");
        strBuil.append(sql);
        strCountBuil.append(sql);
        Query query = session.createQuery(strBuil.toString());
        Query queryCount = session.createQuery(strCountBuil.toString());

        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        queryCount.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);

        if (lstStatus != null && !lstStatus.isEmpty()) {
            query.setParameterList("lstFileStatus", lstStatus);
            queryCount.setParameterList("lstFileStatus", lstStatus);
        }
        if (deptCode != null) {
            query.setParameter("checkDeptCode", deptCode);
            queryCount.setParameter("checkDeptCode", deptCode);
        }
        if (deptCode != null && null == listFileId) {
            query.setParameter("checkDeptCode", deptCode);
            queryCount.setParameter("checkDeptCode", deptCode);
        } else if (deptCode != null && listFileId != null && !listFileId.isEmpty()) {
            query.setParameter("checkDeptCode", deptCode);
            queryCount.setParameter("checkDeptCode", deptCode);
            query.setParameterList("listFileId", listFileId);
            queryCount.setParameterList("listFileId", listFileId);
        }
        if (search != null) {
            if (search.getFileCode() != null && search.getFileCode().trim().length() > 0) {
                query.setParameter("fileCode", StringUtils.toLikeString(search.getFileCode()));
                queryCount.setParameter("fileCode", StringUtils.toLikeString(search.getFileCode()));
            }
            if (search.getGoodsOwnerName() != null && search.getGoodsOwnerName().trim().length() > 0) {
                query.setParameter("goodsOwnerName", StringUtils.toLikeString(search.getGoodsOwnerName()));
                queryCount.setParameter("goodsOwnerName", StringUtils.toLikeString(search.getGoodsOwnerName()));
            }
            if (search.getFileStatus() != null && search.getFileStatus() > -1L) {
                query.setParameter("fileStatus", search.getFileStatus());
                queryCount.setParameter("fileStatus", search.getFileStatus());
            }
            if (search.getComingDateFrom() != null) {
                Date startDate = DateTimeUtils.setStartTimeOfDate(search.getComingDateFrom());
                query.setParameter("comingDateFrom", startDate);
                queryCount.setParameter("comingDateFrom", startDate);
            }
            if (search.getComingDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(search.getComingDateTo());
                toDate = DateTimeUtils.setStartTimeOfDate(toDate);
                query.setParameter("comingDateTo", toDate);
                queryCount.setParameter("comingDateTo", toDate);
            }
            if (search.getDocumentType() != null && !"".equals(search.getDocumentType())) {
                query.setParameter("documentType", search.getDocumentType());
                queryCount.setParameter("documentType", search.getDocumentType());
            }

        }

        query.setFirstResult(start);
        query.setMaxResults(take);
        List<IfdFile> lst = query.list();

        List<IfdFileModel> lstModel = new ArrayList<>();
        for (IfdFile object : lst) {
            IfdFileModel objectModel = new IfdFileModel();
            objectModel = object.convertToIfdFileModel(object);
            lstModel.add(objectModel);
        }
        Long count = (Long) queryCount.uniqueResult();
        PagingListModel pagingListModel = new PagingListModel(lstModel, count);
        return pagingListModel;
    }

    public IfdFile findByFileCode(String fileCode) {
        IfdFile result = null;
        List listParam = new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFile t WHERE t.status = ?");
        StringBuilder sql = new StringBuilder();
        listParam.add(IfdConstants.OBJECT_STATUS.ACTIVE);
        if (fileCode != null && !"".equals(fileCode)) {
            sql.append(" AND t.fileCode like ? escape '/'");
            listParam.add(fileCode);
        }

        sql.append(" order by t.fileId desc ");
        strBuil.append(sql);
        Query query = session.createQuery(strBuil.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }

        List<IfdFile> lstIfdFile = query.list();
        if (lstIfdFile != null && !lstIfdFile.isEmpty()) {
            result = lstIfdFile.get(0);
        }
        return result;
    }

    /**
     * lấy danh sách hồ sơ theo danh sách trạng thái hồ sơ
     *
     * @param lstFileStatus
     * @return
     */
    public List<IfdFile> findAllByFileStatus(List<Long> lstFileStatus) {
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFile t WHERE t.status = :status");
        StringBuilder sql = new StringBuilder();
        if (lstFileStatus != null && !lstFileStatus.isEmpty()) {
            sql.append(" AND t.fileStatus in (:fileStatus)");
        }

        sql.append(" ORDER by t.createDate ");
        strBuil.append(sql);
        Query query = session.createQuery(strBuil.toString());

        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);

        if (lstFileStatus != null && !lstFileStatus.isEmpty()) {
            query.setParameterList("fileStatus", lstFileStatus);
        }

        List<IfdFile> lstIfdFile = query.list();
        return lstIfdFile;
    }

    @Override
    public Long create(IfdFile file) {
        file.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
        file.setUpdateDate(new Date());
        if (file.getCreateDate() == null) {
            file.setCreateDate(new Date());
        }
        return super.create(file); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveOrUpdate(IfdFile file) {
        if (file != null) {
            file.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            if (file.getCreateDate() == null) {
                file.setCreateDate(new Date());
            }
            file.setUpdateDate(new Date());
            super.saveOrUpdate(file);
        }
//        getSession().flush();
    }

}
