/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFileFeeNotice;
import com.viettel.module.importfood.Model.IfdFileFeeNoticeModel;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author quannn
 */
public class IfdFileFeeNoticeDAO extends GenericDAOHibernate<IfdFileFeeNotice,Long> {

    public IfdFileFeeNoticeDAO() {
        super(IfdFileFeeNotice.class);
    }
    
    public void saveAll(List<IfdFileFeeNotice> list){
        for(IfdFileFeeNotice pr: list){
            session.saveOrUpdate(pr);
        }
    }
    
    public List<IfdFileFeeNotice> findAllByFileId(Long fileId){
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileFeeNotice t WHERE t.fileId = :id AND t.status = :status");
        strBuil.append(" order by t.feeNoticeId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", fileId);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        return query.list();
    }
    
    public IfdFileFeeNotice findLatestByFileId(Long fileId){
        IfdFileFeeNotice result = null;
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileFeeNotice t WHERE t.fileId = :fileId AND t.status = :status");
        strBuil.append(" order by t.feeNoticeId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", fileId);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        List<IfdFileFeeNotice> lstResults = query.list();
        if(lstResults != null && !lstResults.isEmpty()){
            result = lstResults.get(0);
        }
        return result;
    }
 
    public IfdFileFeeNoticeModel findLatestModelByFileId(Long fileId){
        IfdFileFeeNoticeModel result = null;
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileFeeNotice t WHERE t.fileId = :fileId AND t.status = :status");
        strBuil.append(" order by t.feeNoticeId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", fileId);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        List<IfdFileFeeNotice> lstResults = query.list();
        IfdFileFeeNotice temp = lstResults.get(0);
        if(lstResults != null && !lstResults.isEmpty()){
            result = temp.convertToIfdFileModel(temp);
        }
        return result;
    }
    
    @Override
    public void saveOrUpdate(IfdFileFeeNotice o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null){
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
}
