/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importfood.BO.IfdFileHistory;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.module.importfood.uitls.StringUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author quannn
 */
public class IfdFileHistoryDAO extends GenericDAOHibernate<IfdFileHistory,Long> {

    public IfdFileHistoryDAO() {
        super(IfdFileHistory.class);
    }
    
    public void saveAll(List<IfdFileHistory> list){
        for(IfdFileHistory pr: list){
            session.saveOrUpdate(pr);
        }
    }
    
    public List<IfdFileHistory> findAllByFileId(Long fileId){
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileHistory t WHERE t.fileId = :id AND t.status = :status");
        strBuil.append(" order by t.fileHistoryId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", fileId);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        return query.list();
    }
    
    public IfdFileHistory findLatestByFileId(Long fileId){
        IfdFileHistory result = null;
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileHistory t WHERE t.fileId = :fileId AND t.status = :status");
        strBuil.append(" order by t.fileHistoryId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", fileId);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        List<IfdFileHistory> lstResults = query.list();
        if(lstResults != null && !lstResults.isEmpty()){
            result = lstResults.get(0);
        }
        return result;
    }
    
    @Override
    public void saveOrUpdate(IfdFileHistory o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null){
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public PagingListModel searchFileHistory(String fileCode, int start, int size) {
        StringBuilder hql = new StringBuilder(" from IfdFileHistory s where s.status = ? ");
        List lstParam = new ArrayList();
        lstParam.add(IfdConstants.OBJECT_STATUS.ACTIVE);
        if (fileCode != null && !"".equals(fileCode)) {
                hql.append(" and lower(s.fileCode) like ? escape '/'");
                lstParam.add(StringUtil.toLikeString(fileCode));
        }
        Query query = session.createQuery("select count(s)" + hql.toString());
        for (int i = 0; i < lstParam.size(); i++) {
            query.setParameter(i, lstParam.get(i));
        }
        long total = (Long) query.uniqueResult();
        if (total == 0l) {
            return new PagingListModel(new ArrayList(), total);
        }
        hql.append(" order by s.createDate desc");
        query = session.createQuery("select s " + hql.toString());
        for (int i = 0; i < lstParam.size(); i++) {
            query.setParameter(i, lstParam.get(i));
        }
        if (start > -1L) {
            query.setFirstResult(start);
            query.setMaxResults(size);
        }
        List<IfdFileHistory> list = query.list();
        PagingListModel model = new PagingListModel(list, total);
        return model;
    }
    
}
