/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.module.importfood.Model.IfdFilePaymentModel;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query; 

/**
 *
 * @author Administrator
 */
public class IfdFilePaymentDAO extends GenericDAOHibernate<IfdFilePayment, Long> {

    public IfdFilePaymentDAO() {
        super(IfdFilePayment.class);
    }

    public List<IfdFilePaymentModel> findAllByFileId(Long idIfdFile) {
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFilePayment t WHERE t.status = :status");
        if (idIfdFile != null) {
            strBuil.append(" AND t.fileId = :fileId");
        }
        strBuil.append(" order by t.paymentId desc ");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        if (idIfdFile != null) {
            query.setParameter("fileId", idIfdFile);
        }
        List<IfdFilePayment> paymentList = query.list();
        List<IfdFilePaymentModel> listModel = new ArrayList();
        
        for(IfdFilePayment payment : paymentList){
            if(payment!=null){
                listModel.add(payment.convertToIfdFileModel(payment));
            }
        }
        
        return listModel;
    }
    
    public IfdFilePayment findLatestByFileId(Long fileId){
        IfdFilePayment result = null;
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFilePayment t WHERE t.fileId = :fileId AND t.status = :status");
        strBuil.append(" order by t.paymentId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", fileId);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        List<IfdFilePayment> lstResults = query.list();
        if(lstResults != null && !lstResults.isEmpty()){
            result = lstResults.get(0);
        }
        return result;
    }

    @Override
    public void saveOrUpdate(IfdFilePayment o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null) {
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
//            getSession().getTransaction().commit();
        }
    }

}
