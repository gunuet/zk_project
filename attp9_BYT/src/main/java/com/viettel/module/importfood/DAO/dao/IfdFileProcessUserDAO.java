/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFileProcessUser;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author DEV
 */
public class IfdFileProcessUserDAO extends GenericDAOHibernate<IfdFileProcessUser, Long> {

    public IfdFileProcessUserDAO() {
        super(IfdFileProcessUser.class);
    }

    public List<IfdFileProcessUser> findListIfdFileProcessUser(Long userId, String roleCode, String deptCode) {
        String strBuil = "SELECT t FROM IfdFileProcessUser t WHERE t.status = :status"
                + " and t.receiveUserId = :receiveUserId"
                + " and t.receiveDeptCode = :receiveDeptCode"
                + " and t.receiveRoleCode = :receiveRoleCode";
        Query query = session.createQuery(strBuil);
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        query.setParameter("receiveUserId", userId);
        query.setParameter("receiveDeptCode", deptCode);
        query.setParameter("receiveRoleCode", roleCode);
        List<IfdFileProcessUser> searchList = query.list();

        return searchList;
    }
}
