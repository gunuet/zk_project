/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Administrator
 */
public class IfdFileProductDAO extends GenericDAOHibernate<IfdFileProduct,Long> {

    public IfdFileProductDAO() {
        super(IfdFileProduct.class);
    }
    
    public void saveAll(List<IfdFileProduct> list){
        for(IfdFileProduct pr: list){
            session.saveOrUpdate(pr);
        }
    }
    
    public void deleteAllById(List<Long> listProduct){
        StringBuilder hql = new StringBuilder("UPDATE IfdFileProduct p SET p.status = :inactive_atatus WHERE 1=1 ");
                
        for(int i = 0; i < listProduct.size(); i++){
            if(i>0){
                hql.append(" AND ");
            }
            hql.append("p.id = :id"+i);
        }
        
        Query query = session.createQuery(hql.toString());
 
        for(int i = 0; i < listProduct.size(); i++){
            query.setParameter("id"+i, listProduct.get(i));
        }
        query.setParameter("inactive_status",  IfdConstants.OBJECT_STATUS.INACTIVE);
        query.executeUpdate();
    }
    
    public void deleteById(Long id){
        StringBuilder hql = new StringBuilder("UPDATE IfdFileProduct p SET p.status = :inactive_status WHERE p.id = :id ");
        Query query = session.createQuery(hql.toString());
        query.setParameter("id", id);
        query.setParameter("inactive_status", IfdConstants.OBJECT_STATUS.INACTIVE );
        query.executeUpdate();
    }
    
    public List<IfdFileProduct> findAllByFileId(Long fileId){
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileProduct t WHERE t.fileId = :fileId AND t.status = :active_status");
        strBuil.append(" order by t.fileProductId desc");
        
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", fileId);
        query.setParameter("active_status", IfdConstants.OBJECT_STATUS.ACTIVE);
        return query.list();
    }
    
    public List<IfdFileProduct> findListFileResultByIdIfdFile(Long idIfdFile){
        List<IfdFileProduct> searchList =  new ArrayList<>();
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileProduct t WHERE t.status = :active_status");
        if(idIfdFile != null){
            strBuil.append(" AND t.fileId = :fileId ");
        }
        strBuil.append(" order by t.fileProductId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("fileId", idIfdFile);
        query.setParameter("active_status", IfdConstants.OBJECT_STATUS.ACTIVE );
        return query.list();
    }
    
//    public List<IfdFileProduct> findListProductByFileId(Long fileId, int take, int start){
//        List<IfdFileProduct> searchList =  new ArrayList<>();
//        Criteria cr  = session.createCriteria(IfdFileProduct.class);
//   
//        cr.add(Restrictions.eq("fileId", fileId ));
//       
//        System.out.println(take + "/" + start);
//        cr.setMaxResults(take);
//        cr.setFirstResult(start);
//        
//       searchList = cr.list();
//        System.out.println(searchList);
//        return searchList;
//    }

    @Override
    public void saveOrUpdate(IfdFileProduct o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null){
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
}
