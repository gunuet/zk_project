/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFileRequestCancel;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class IfdFileRequestCancelDAO extends GenericDAOHibernate<IfdFileRequestCancel, Long> {

    public IfdFileRequestCancelDAO() {
        super(IfdFileRequestCancel.class);
    }

    public List<IfdFileRequestCancel> findListFileRequestCancelByFileId(Long fileId) {
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileRequestCancel t WHERE t.status = :status");
        if (fileId != null) {
            strBuil.append(" AND t.fileId = :fileId");
        }
        strBuil.append(" order by t.fileRequestCancelId desc ");
        Query query = session.createQuery(strBuil.toString());
        
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        
        if (fileId != null) {
            query.setParameter("fileId", fileId);
        }
        return query.list();
    }
    
    public IfdFileRequestCancel findLatestByFileId(Long fileId){
        IfdFileRequestCancel result = null;
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdFileRequestCancel t WHERE t.status = :status");
        if (fileId != null) {
            strBuil.append(" AND t.fileId = :fileId");
        }
        
        strBuil.append(" order by t.fileRequestCancelId desc");
        Query query = session.createQuery(strBuil.toString());
        
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        if (fileId != null) {
            query.setParameter("fileId", fileId);
        }
        List<IfdFileRequestCancel> lstResults = query.list();
        if(lstResults != null && !lstResults.isEmpty()){
            result = lstResults.get(0);
        }
        return result;
    }

    @Override
    public void saveOrUpdate(IfdFileRequestCancel o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null) {
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
        }
    }

}
