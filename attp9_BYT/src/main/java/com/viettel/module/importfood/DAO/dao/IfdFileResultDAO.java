/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdFileResult;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class IfdFileResultDAO extends GenericDAOHibernate<IfdFileResult,Long> {

    public IfdFileResultDAO() {
        super(IfdFileResult.class);
    }
    
    public List<IfdFileResult> findListFileResultByIdIfdFile(Long idIfdFile){
        List<IfdFileResult> searchList =  new ArrayList<>();
        StringBuilder strBuil = new StringBuilder(" SELECT t FROM IfdFileResult t WHERE t.status = :active_status");
        if(idIfdFile != null ){
            strBuil.append(" AND t.fileId = "+idIfdFile);
        }
        strBuil.append(" order by t.fileResultId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("active_status", IfdConstants.OBJECT_STATUS.ACTIVE);
        return query.list();
    }
    
    public List<IfdFileResult> findListByFileIdAndFileStatus(Long fileId, Long fileStatus){
        StringBuilder strBuil = new StringBuilder(" SELECT t FROM IfdFileResult t WHERE t.status = :status");
        if(fileId != null ){
            strBuil.append(" AND t.fileId = :fileId");
        }
        if(fileStatus != null){
            strBuil.append(" AND t.fileStatus = :fileStatus");
        }
        strBuil.append(" order by t.fileResultId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        
        if(fileId != null ){
            query.setParameter("fileId", fileId);
        }
        if(fileStatus != null){
            query.setParameter("fileStatus", fileStatus);
        }
        
        return query.list();
    }

    @Override
    public void saveOrUpdate(IfdFileResult o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null){
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    
    
}
