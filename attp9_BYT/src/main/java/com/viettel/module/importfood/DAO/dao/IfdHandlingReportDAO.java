/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.DAO.dao;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importfood.BO.IfdHandlingReport;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class IfdHandlingReportDAO extends GenericDAOHibernate<IfdHandlingReport,Long> {

    public IfdHandlingReportDAO() {
        super(IfdHandlingReport.class);
    }
    
    public void saveAll(List<IfdHandlingReport> list){
        for(IfdHandlingReport pr: list){
            session.saveOrUpdate(pr);
        }
    }
    
    public List<IfdHandlingReport> findAllByFileId(Long fileId){
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdHandlingReport t WHERE t.status = :status");
        if (fileId != null){
            strBuil.append(" AND t.fileId = :fileId ");
        }
        strBuil.append(" order by t.handlingReportId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        if (fileId != null){
            query.setParameter("fileId", fileId);
        }
        return query.list();
    }
    
    public IfdHandlingReport findLatestByFileId(Long fileId){
        IfdHandlingReport result = null;
        StringBuilder strBuil = new StringBuilder("SELECT t FROM IfdHandlingReport t WHERE t.status = :status");
        if (fileId != null){
            strBuil.append(" AND t.fileId = :fileId ");
        }
        strBuil.append(" order by t.handlingReportId desc");
        Query query = session.createQuery(strBuil.toString());
        query.setParameter("status", IfdConstants.OBJECT_STATUS.ACTIVE);
        if (fileId != null){
            query.setParameter("fileId", fileId);
        }
        List<IfdHandlingReport> list = query.list();
        if (list != null && !list.isEmpty()){
            result = list.get(0);
        }
        return result;
    }
    
    @Override
    public void saveOrUpdate(IfdHandlingReport o) {
        if (o != null) {
            o.setStatus(IfdConstants.OBJECT_STATUS.ACTIVE);
            o.setUpdateDate(new Date());
            if (o.getCreateDate() == null){
                o.setCreateDate(new Date());
            }
            super.saveOrUpdate(o); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
}
