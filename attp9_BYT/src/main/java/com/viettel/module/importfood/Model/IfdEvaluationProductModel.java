/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class IfdEvaluationProductModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long evaluationProductId;

    private Long evaluationId;

    private String productCode;

    private String productName;

    private String productGroupCode;

    private String productGroupName;

    private String manufacturer;

    private String manufacturerAddress;

    private Long productCheckMethod;

    private String checkMethodConfirmNo;

    private String confirmAnnounceNo;

    private Long checkResultCode;
    
    private String checkResultName;

    private String reason;

    private Long handlingMeasuresCode;

    private String handlingMeasuresName;

    private String note;

    private Long status;

    private Date createDate;
    
    private Long createBy;

    private Date updateDate;
    
    private Long updateBy;

    public IfdEvaluationProductModel() {
    }

    public IfdEvaluationProductModel(Long evaluationProductId) {
        this.evaluationProductId = evaluationProductId;
    }

    public Long getEvaluationProductId() {
        return evaluationProductId;
    }

    public void setEvaluationProductId(Long evaluationProductId) {
        this.evaluationProductId = evaluationProductId;
    }

    public Long getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(Long evaluationId) {
        this.evaluationId = evaluationId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductGroupCode() {
        return productGroupCode;
    }

    public void setProductGroupCode(String productGroupCode) {
        this.productGroupCode = productGroupCode;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerAddress() {
        return manufacturerAddress;
    }

    public void setManufacturerAddress(String manufacturerAddress) {
        this.manufacturerAddress = manufacturerAddress;
    }

    public Long getProductCheckMethod() {
        return productCheckMethod;
    }

    public void setProductCheckMethod(Long productCheckMethod) {
        this.productCheckMethod = productCheckMethod;
    }

    public String getCheckMethodConfirmNo() {
        return checkMethodConfirmNo;
    }

    public void setCheckMethodConfirmNo(String checkMethodConfirmNo) {
        this.checkMethodConfirmNo = checkMethodConfirmNo;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo;
    }

    public Long getCheckResultCode() {
        return checkResultCode;
    }

    public void setCheckResultCode(Long checkResultCode) {
        this.checkResultCode = checkResultCode;
    }

    public String getCheckResultName() {
        return checkResultName;
    }

    public void setCheckResultName(String checkResultName) {
        this.checkResultName = checkResultName;
    }   

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getHandlingMeasuresCode() {
        return handlingMeasuresCode;
    }

    public void setHandlingMeasuresCode(Long handlingMeasuresCode) {
        this.handlingMeasuresCode = handlingMeasuresCode;
    }

    public String getHandlingMeasuresName() {
        return handlingMeasuresName;
    }

    public void setHandlingMeasuresName(String handlingMeasuresName) {
        this.handlingMeasuresName = handlingMeasuresName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }


}
