/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
public class IfdFileFeeNoticeModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long feeNoticeId;

    private Long fileId;

    private String fileCode;

    private String deptCode;

    private String deptName;

    private Long totalOfFee;

    private Long accountNumber;

    private String bank;

    private Long numberOfProduct;

    private Long feeDefaut;

    private Long status;

    private Date createDate;

    private Long createBy;

    private Date updateDate;

    private Long updateBy;

    public IfdFileFeeNoticeModel() {
    }

    public IfdFileFeeNoticeModel(Long feeNoticeId) {
        this.feeNoticeId = feeNoticeId;
    }

    public IfdFileFeeNoticeModel(Long feeNoticeId, Long status, Date createDate, Date updateDate) {
        this.feeNoticeId = feeNoticeId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFeeNoticeId() {
        return feeNoticeId;
    }

    public void setFeeNoticeId(Long feeNoticeId) {
        this.feeNoticeId = feeNoticeId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getTotalOfFee() {
        return totalOfFee;
    }

    public void setTotalOfFee(Long totalOfFee) {
        this.totalOfFee = totalOfFee;
    }

    public Long getNumberOfProduct() {
        return numberOfProduct;
    }

    public void setNumberOfProduct(Long numberOfProduct) {
        this.numberOfProduct = numberOfProduct;
    }

    public Long getFeeDefaut() {
        return feeDefaut;
    }

    public void setFeeDefaut(Long feeDefaut) {
        this.feeDefaut = feeDefaut;
    }


    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (feeNoticeId != null ? feeNoticeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileFeeNoticeModel)) {
            return false;
        }
        IfdFileFeeNoticeModel other = (IfdFileFeeNoticeModel) object;
        if ((this.feeNoticeId == null && other.feeNoticeId != null) || (this.feeNoticeId != null && !this.feeNoticeId.equals(other.feeNoticeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.importfood.BO.IfdFileFeeNotice[ feeNoticeId=" + feeNoticeId + " ]";
    }
    
}
