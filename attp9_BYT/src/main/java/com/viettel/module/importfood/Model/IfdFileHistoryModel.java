/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class IfdFileHistoryModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long fileHistoryId;
    private Long fileId;
    private String fileCode;
    private String senderName;
    private String senderDepartment;
    private String receiverName;
    private String receiverDepartment;
    private String content;
    private String note;
    private Long fileResultId;
    private Date endDate;
    private Long fileStatus;
    private String fileStatusName;
    private Long status;
    private Date createDate;
    private Long createBy;
    private Date updateDate;
    private Long updateBy;

    public IfdFileHistoryModel() {
    }

    public IfdFileHistoryModel(Long fileHistoryId) {
        this.fileHistoryId = fileHistoryId;
    }

    public IfdFileHistoryModel(Long fileHistoryId, Long status, Date createDate, Date updateDate) {
        this.fileHistoryId = fileHistoryId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFileHistoryId() {
        return fileHistoryId;
    }

    public void setFileHistoryId(Long fileHistoryId) {
        this.fileHistoryId = fileHistoryId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderDepartment() {
        return senderDepartment;
    }

    public void setSenderDepartment(String senderDepartment) {
        this.senderDepartment = senderDepartment;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverDepartment() {
        return receiverDepartment;
    }

    public void setReceiverDepartment(String receiverDepartment) {
        this.receiverDepartment = receiverDepartment;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getFileResultId() {
        return fileResultId;
    }

    public void setFileResultId(Long fileResultId) {
        this.fileResultId = fileResultId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(Long fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getFileStatusName() {
        return fileStatusName;
    }

    public void setFileStatusName(String fileStatusName) {
        this.fileStatusName = fileStatusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileHistoryId != null ? fileHistoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileHistoryModel)) {
            return false;
        }
        IfdFileHistoryModel other = (IfdFileHistoryModel) object;
        if ((this.fileHistoryId == null && other.fileHistoryId != null) || (this.fileHistoryId != null && !this.fileHistoryId.equals(other.fileHistoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFileHistory[ fileHistoryId=" + fileHistoryId + " ]";
    }
    
}
