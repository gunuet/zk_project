/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Model;

import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFileProduct;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Administrator
 */
public class IfdFileModel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Long fileId;
    private String fileCode;
    private Date fileCreateDate;
    private String fileCreateBy;
    private Date fileModifiedDate;
    private Long checkMethod;
    private String goodsOwnerName;
    private String goodsOwnerAddress;
    private String goodsOwnerPhone;
    private String goodsOwnerFax;
    private String goodsOwnerEmail;
    private String responesiblePersonName;
    private String responesiblePersonAddress;
    private String responesiblePersonPhone;
    private String responesiblePersonFax;
    private String responesiblePersonEmail;
    private String exporterName;
    private String exporterAddress;
    private String exporterPhone;
    private String exporterFax;
    private String exporterEmail;
    private String customDeclarationNo;
    private Date comingDateFrom;
    private Date comingDateTo;
    private String exporterGateCode;
    private String exporterGateName;
    private String importerGateCode;
    private String importerGateName;
    private Date checkTimeFrom;
    private Date checkTimeTo;
    private String checkPlace;
    private Long checkDeptId;
    private String checkDeptCode;
    private String checkDeptName;
    private Long fileStatus;
    private String fileStatusName;
    private Long status;
    private Long isDelete;
    private Date createDate;
    private Long createBy;
    private Date updateDate;
    private Long updateBy;
    private Date signDate;
    private String signName;
    private String documentType;
    
    private List<IfdFileProduct> lstProduct;
    
    private List<IfdAttachment> lstAttach;
    
    //chỉ có dành cho view
    private String commingDateFromTo;

    public IfdFileModel() {
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Date getFileCreateDate() {
        return fileCreateDate;
    }

    public void setFileCreateDate(Date fileCreateDate) {
        this.fileCreateDate = fileCreateDate;
    }

    public String getFileCreateBy() {
        return fileCreateBy;
    }

    public void setFileCreateBy(String fileCreateBy) {
        this.fileCreateBy = fileCreateBy;
    }

    public Date getFileModifiedDate() {
        return fileModifiedDate;
    }

    public void setFileModifiedDate(Date fileModifiedDate) {
        this.fileModifiedDate = fileModifiedDate;
    }

    public Long getCheckMethod() {
        return checkMethod;
    }

    public void setCheckMethod(Long checkMethod) {
        this.checkMethod = checkMethod;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return goodsOwnerEmail;
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = goodsOwnerEmail;
    }

    public String getResponesiblePersonName() {
        return responesiblePersonName;
    }

    public void setResponesiblePersonName(String responesiblePersonName) {
        this.responesiblePersonName = responesiblePersonName;
    }

    public String getResponesiblePersonAddress() {
        return responesiblePersonAddress;
    }

    public void setResponesiblePersonAddress(String responesiblePersonAddress) {
        this.responesiblePersonAddress = responesiblePersonAddress;
    }

    public String getResponesiblePersonPhone() {
        return responesiblePersonPhone;
    }

    public void setResponesiblePersonPhone(String responesiblePersonPhone) {
        this.responesiblePersonPhone = responesiblePersonPhone;
    }

    public String getResponesiblePersonFax() {
        return responesiblePersonFax;
    }

    public void setResponesiblePersonFax(String responesiblePersonFax) {
        this.responesiblePersonFax = responesiblePersonFax;
    }

    public String getResponesiblePersonEmail() {
        return responesiblePersonEmail;
    }

    public void setResponesiblePersonEmail(String responesiblePersonEmail) {
        this.responesiblePersonEmail = responesiblePersonEmail;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterFax() {
        return exporterFax;
    }

    public void setExporterFax(String exporterFax) {
        this.exporterFax = exporterFax;
    }

    public String getExporterEmail() {
        return exporterEmail;
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = exporterEmail;
    }

    public String getCustomDeclarationNo() {
        return customDeclarationNo;
    }

    public void setCustomDeclarationNo(String customDeclarationNo) {
        this.customDeclarationNo = customDeclarationNo;
    }

    public Date getComingDateFrom() {
        return comingDateFrom;
    }

    public void setComingDateFrom(Date comingDateFrom) {
        this.comingDateFrom = comingDateFrom;
    }

    public Date getComingDateTo() {
        return comingDateTo;
    }

    public void setComingDateTo(Date comingDateTo) {
        this.comingDateTo = comingDateTo;
    }

    public String getExporterGateCode() {
        return exporterGateCode;
    }

    public void setExporterGateCode(String exporterGateCode) {
        this.exporterGateCode = exporterGateCode;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateCode() {
        return importerGateCode;
    }

    public void setImporterGateCode(String importerGateCode) {
        this.importerGateCode = importerGateCode;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public Date getCheckTimeFrom() {
        return checkTimeFrom;
    }

    public void setCheckTimeFrom(Date checkTimeFrom) {
        this.checkTimeFrom = checkTimeFrom;
    }

    public Date getCheckTimeTo() {
        return checkTimeTo;
    }

    public void setCheckTimeTo(Date checkTimeTo) {
        this.checkTimeTo = checkTimeTo;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getCheckDeptCode() {
        return checkDeptCode;
    }

    public void setCheckDeptCode(String checkDeptCode) {
        this.checkDeptCode = checkDeptCode;
    }

    public String getCheckDeptName() {
        return checkDeptName;
    }

    public void setCheckDeptName(String checkDeptName) {
        this.checkDeptName = checkDeptName;
    }

    public Long getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(Long fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getFileStatusName() {
        return fileStatusName;
    }

    public void setFileStatusName(String fileStatusName) {
        this.fileStatusName = fileStatusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(long isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public List<IfdFileProduct> getLstProduct() {
        return lstProduct;
    }

    public void setLstProduct(List<IfdFileProduct> lstProduct) {
        this.lstProduct = lstProduct;
    }

    public List<IfdAttachment> getLstAttach() {
        return lstAttach;
    }

    public void setLstAttach(List<IfdAttachment> lstAttach) {
        this.lstAttach = lstAttach;
    }

    public String getCommingDateFromTo() {
        return commingDateFromTo;
    }

    public void setCommingDateFromTo(String commingDateFromTo) {
        this.commingDateFromTo = commingDateFromTo;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public Long getCheckDeptId() {
        return checkDeptId;
    }

    public void setCheckDeptId(Long checkDeptId) {
        this.checkDeptId = checkDeptId;
    }

}
