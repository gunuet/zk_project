/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Model;

import com.viettel.module.importfood.BO.IfdAttachment;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
public class IfdFilePaymentModel implements Serializable {

    private static final long serialVersionUID = 1l;
    private Long paymentId;
    private Long fileId;
    private String fileCode;
    private Date processDate;
    private String processName;
    private Long paymentType;
    private String paymentTypeName;
    private Double cost;
    private String bankNo;
    private String deptCode;
    private String deptName;
    private Long status;
    private Date createDate;
    private Long createBy;
    private Date updateDate;
    private Long updateBy;
    private String onlPaymentDeptCode;
    private String onlPaymentDeptName;
    private IfdAttachment attach;

    public IfdFilePaymentModel() {
    }

    public IfdFilePaymentModel(Long paymentId) {
        this.paymentId = paymentId;
    }

    public IfdFilePaymentModel(Long paymentId, Long status, Date createDate, Date updateDate) {
        this.paymentId = paymentId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Long getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Long paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }    
    
    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getOnlPaymentDeptCode() {
        return onlPaymentDeptCode;
    }

    public void setOnlPaymentDeptCode(String onlPaymentDeptCode) {
        this.onlPaymentDeptCode = onlPaymentDeptCode;
    }

    public String getOnlPaymentDeptName() {
        return onlPaymentDeptName;
    }

    public void setOnlPaymentDeptName(String onlPaymentDeptName) {
        this.onlPaymentDeptName = onlPaymentDeptName;
    }

    public IfdAttachment getAttach() {
        return attach;
    }

    public void setAttach(IfdAttachment attach) {
        this.attach = attach;
    }
}
