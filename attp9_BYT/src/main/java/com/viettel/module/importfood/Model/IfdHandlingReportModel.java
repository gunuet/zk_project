/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Model;

import com.viettel.module.importfood.BO.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class IfdHandlingReportModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long handlingReportId;
    private Long fileId;
    private String fileCode;
    private String content;
    private Long status;
    private Date createDate;
    private Long createBy;
    private Date updateDate;
    private Long updateBy;
    private List<IfdAttachment> lstAttachs;

    public IfdHandlingReportModel() {
    }

    public IfdHandlingReportModel(Long handlingReportId) {
        this.handlingReportId = handlingReportId;
    }

    public IfdHandlingReportModel(Long handlingReportId, Long status, Date createDate, Date updateDate) {
        this.handlingReportId = handlingReportId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getHandlingReportId() {
        return handlingReportId;
    }

    public void setHandlingReportId(Long handlingReportId) {
        this.handlingReportId = handlingReportId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public List<IfdAttachment> getLstAttachs() {
        return lstAttachs;
    }

    public void setLstAttachs(List<IfdAttachment> lstAttachs) {
        this.lstAttachs = lstAttachs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (handlingReportId != null ? handlingReportId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdHandlingReportModel)) {
            return false;
        }
        IfdHandlingReportModel other = (IfdHandlingReportModel) object;
        if ((this.handlingReportId == null && other.handlingReportId != null) || (this.handlingReportId != null && !this.handlingReportId.equals(other.handlingReportId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdHandlingReport[ handlingReportId=" + handlingReportId + " ]";
    }
    
}
