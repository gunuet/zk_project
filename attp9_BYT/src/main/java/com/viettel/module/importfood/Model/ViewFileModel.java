/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Model;

import com.viettel.module.importfood.BO.IfdFile;
import java.text.SimpleDateFormat;

/**
 *
 * @author Administrator
 */
public class ViewFileModel {

    private Long fileId;

    private String fileCode;

    private String fileCreateDate;

    private String fileCreateBy;

    private String fileModifiedDate;

    private String checkMethod;

    private String goodsOwnerName;

    private String goodsOwnerAddress;

    private String goodsOwnerPhone;

    private String goodsOwnerFax;

    private String goodsOwnerEmail;

    private String responesiblePersonName;

    private String responesiblePersonAddress;

    private String responesiblePersonPhone;

    private String responesiblePersonFax;

    private String responesiblePersonEmail;

    private String exporterName;

    private String exporterAddress;

    private String exporterPhone;

    private String exporterFax;

    private String exporterEmail;

    private String customDeclarationNo;

    private String comingDateFrom;

    private String comingDateTo;

    private String exporterGateName;

    private String importerGateName;

    private String checkTimeFrom;

    private String checkTimeTo;

    private String checkPlace;

    private String checkDeptName;

    private String fileStatusName;

    private String status;

    private String createDate;

    private String createBy;

    private String updateDate;

    private String updateBy;

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileCreateDate() {
        return fileCreateDate;
    }

    public void setFileCreateDate(String fileCreateDate) {
        this.fileCreateDate = fileCreateDate;
    }

    public String getFileCreateBy() {
        return fileCreateBy;
    }

    public void setFileCreateBy(String fileCreateBy) {
        this.fileCreateBy = fileCreateBy;
    }

    public String getFileModifiedDate() {
        return fileModifiedDate;
    }

    public void setFileModifiedDate(String fileModifiedDate) {
        this.fileModifiedDate = fileModifiedDate;
    }

    public String getCheckMethod() {
        return checkMethod;
    }

    public void setCheckMethod(String checkMethod) {
        this.checkMethod = checkMethod;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return goodsOwnerEmail;
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = goodsOwnerEmail;
    }

    public String getResponesiblePersonName() {
        return responesiblePersonName;
    }

    public void setResponesiblePersonName(String responesiblePersonName) {
        this.responesiblePersonName = responesiblePersonName;
    }

    public String getResponesiblePersonAddress() {
        return responesiblePersonAddress;
    }

    public void setResponesiblePersonAddress(String responesiblePersonAddress) {
        this.responesiblePersonAddress = responesiblePersonAddress;
    }

    public String getResponesiblePersonPhone() {
        return responesiblePersonPhone;
    }

    public void setResponesiblePersonPhone(String responesiblePersonPhone) {
        this.responesiblePersonPhone = responesiblePersonPhone;
    }

    public String getResponesiblePersonFax() {
        return responesiblePersonFax;
    }

    public void setResponesiblePersonFax(String responesiblePersonFax) {
        this.responesiblePersonFax = responesiblePersonFax;
    }

    public String getResponesiblePersonEmail() {
        return responesiblePersonEmail;
    }

    public void setResponesiblePersonEmail(String responesiblePersonEmail) {
        this.responesiblePersonEmail = responesiblePersonEmail;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterFax() {
        return exporterFax;
    }

    public void setExporterFax(String exporterFax) {
        this.exporterFax = exporterFax;
    }

    public String getExporterEmail() {
        return exporterEmail;
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = exporterEmail;
    }

    public String getCustomDeclarationNo() {
        return customDeclarationNo;
    }

    public void setCustomDeclarationNo(String customDeclarationNo) {
        this.customDeclarationNo = customDeclarationNo;
    }

    public String getComingDateFrom() {
        return comingDateFrom;
    }

    public void setComingDateFrom(String comingDateFrom) {
        this.comingDateFrom = comingDateFrom;
    }

    public String getComingDateTo() {
        return comingDateTo;
    }

    public void setComingDateTo(String comingDateTo) {
        this.comingDateTo = comingDateTo;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public String getCheckTimeFrom() {
        return checkTimeFrom;
    }

    public void setCheckTimeFrom(String checkTimeFrom) {
        this.checkTimeFrom = checkTimeFrom;
    }

    public String getCheckTimeTo() {
        return checkTimeTo;
    }

    public void setCheckTimeTo(String checkTimeTo) {
        this.checkTimeTo = checkTimeTo;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getCheckDeptName() {
        return checkDeptName;
    }

    public void setCheckDeptName(String checkDeptName) {
        this.checkDeptName = checkDeptName;
    }

    public String getFileStatusName() {
        return fileStatusName;
    }

    public void setFileStatusName(String fileStatusName) {
        this.fileStatusName = fileStatusName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ViewFileModel(IfdFile file){
        this.fileId = file.getFileId();
        this.fileCode = file.getFileCode();
        this.checkDeptName = file.getCheckDeptName();
        this.checkPlace = file.getCheckPlace();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        this.checkTimeFrom = formatDate.format(file.getCheckTimeFrom());
        this.checkTimeTo = formatDate.format(file.getCheckTimeTo());
        this.comingDateFrom = formatDate.format(file.getComingDateFrom());
        this.comingDateTo = formatDate.format(file.getComingDateTo());
        this.exporterAddress = file.getExporterAddress();
        this.exporterEmail = file.getExporterEmail();
        this.exporterFax = file.getExporterFax();
        this.exporterGateName = file.getExporterGateName();
        this.goodsOwnerAddress = file.getGoodsOwnerAddress();
        this.goodsOwnerEmail = file.getGoodsOwnerEmail();
        this.goodsOwnerFax = file.getGoodsOwnerFax();
        this.goodsOwnerName = file.getGoodsOwnerName();
        this.goodsOwnerPhone = file.getGoodsOwnerPhone();
    }
}
