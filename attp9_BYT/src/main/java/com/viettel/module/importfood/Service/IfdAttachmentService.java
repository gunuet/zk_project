/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdCertificate;
import com.viettel.module.importfood.BO.IfdEvaluation;
import com.viettel.module.importfood.DAO.IfdEvaluationDAO;
import com.viettel.module.importfood.DAO.dao.IfdAttachmentDAO;
import com.viettel.module.importfood.DAO.dao.IfdCertificateDAO;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author quannn
 */
public class IfdAttachmentService {
    
    private IfdAttachmentDAO ifdAttachmentDAO;
    
    private IfdEvaluationDAO ifdEvaluationDAO;
    
    private IfdCertificateDAO ifdCertificateDAO;
    /**
     * Lay danh sach dinh kem theo objectType va objectId
     * @param objectType
     * @param objectId
     * @return 
     */
    public List<IfdAttachment> findAllByObjectTypeAndObjectId(Long objectType, Long objectId){
        ifdAttachmentDAO = new IfdAttachmentDAO();
        return ifdAttachmentDAO.findListAttachmentByObjectTypeAndObjectId(objectType, objectId);
    }
    
    public List<IfdAttachment> findAttachmentEvaluationByFileID(Long fileID) {
        ifdAttachmentDAO = new IfdAttachmentDAO();
        ifdEvaluationDAO = new IfdEvaluationDAO();
        Long objectType = IfdConstants.ATTACH_OBJEC_TYPE.EVALUATION;

        IfdEvaluation evaluation = ifdEvaluationDAO.findLatestByIfdFileID(fileID);
        List<IfdAttachment> searchResult = ifdAttachmentDAO.findListAttachmentByObjectTypeAndObjectId(objectType, evaluation.getEvaluationId());

        return searchResult;
    }
    
    public List<IfdAttachment> findAttachmentCertificateByFileID(Long fileID){
        ifdAttachmentDAO = new IfdAttachmentDAO();
        ifdCertificateDAO = new IfdCertificateDAO();
        Long objectType = IfdConstants.ATTACH_OBJEC_TYPE.CERTIFICATE;
        
        IfdCertificate certificate = ifdCertificateDAO.findLatestByFileID(fileID);
        List<IfdAttachment> result = ifdAttachmentDAO.findListAttachmentByObjectTypeAndObjectId(objectType, certificate.getCertificateId());
        return result;
    }
}
