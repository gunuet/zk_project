/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdCertificate;
import com.viettel.module.importfood.BO.IfdCertificateProduct;
import com.viettel.module.importfood.DAO.dao.IfdAttachmentDAO;
import com.viettel.module.importfood.DAO.dao.IfdCertificateDAO;
import com.viettel.module.importfood.DAO.dao.IfdCertificateProductDAO;
import com.viettel.module.importfood.uitls.IfdConstants;
import java.util.List;

/**
 *
 * @author quannn
 */
public class IfdCertificateService {
    
    private IfdCertificateProductDAO ifdCertificateProductDAO;
    
    private IfdAttachmentDAO ifdAttachmentDAO;
    
    private IfdCertificateDAO ifdCertificateDAO;
    
//    public IfdCertificate createIfdCertificate(IfdCertificate file) {
//        IfdCertificate result = null;
//        ifdCertificateProductDAO = new IfdCertificateProductDAO();
//        ifdAttachmentDAO = new IfdAttachmentDAO();
//        ifdCertificateDAO = new IfdCertificateDAO();
//        
//        try {
//            String fileCode = file.getFileCode();
//            ifdCertificateDAO.saveOrUpdate(file);
//
//            IfdCertificate ifdCertificateSaved = ifdCertificateDAO.findByFileCode(file.getFileCode());
//            Long certificateId = ifdCertificateSaved.getCertificateId();
//
//            //save List IfdCertificateProduct
//            List<IfdCertificateProduct> lstProduct = file.getLstProduct();
//            if (lstProduct != null && !lstProduct.isEmpty()) {
//                for (IfdCertificateProduct ifdCertificateProduct : lstProduct) {
//                    ifdCertificateProduct.setCertificateId(certificateId);
//                    ifdCertificateProductDAO.saveOrUpdate(ifdCertificateProduct);
//
//                    List<IfdCertificateProduct> lstProductSaved = ifdCertificateProductDAO.findAllByCertificateId(certificateId);
//                    if (lstProductSaved != null && !lstProductSaved.isEmpty()) {
//                        IfdCertificateProduct latestProductSaved = lstProductSaved.get(0);
//                        Long productId = latestProductSaved.getFileProductId();
//
//                        IfdAttachment attachProduct = latestProductSaved.getAttach();
//                        if (attachProduct != null) {
//                            attachProduct.setObjectId(productId);
//                            attachProduct.setObjectType(IfdConstants.ATTACH_OBJEC_TYPE.FILE_PRODUCT);
//                            ifdAttachmentDAO.saveOrUpdate(attachProduct);
//                        }
//                    }
//
//                }
//            }
//            
//            //save List IfdAttachment
//            List<IfdAttachment> lstAttach = file.getLstAttach();
//            if (lstAttach != null && !lstAttach.isEmpty()) {
//                for (IfdAttachment attachment : lstAttach) {
//                    attachment.setObjectId(certificateId);
//                    attachment.setObjectType(IfdConstants.ATTACH_OBJEC_TYPE.FILE);
//                    ifdAttachmentDAO.saveOrUpdate(attachment);
//                }
//            }
//
//            result = getIfdCertificate(certificateId);
//            HibernateUtil.commitCurrentSessions();
//            return result;
//        } catch (Exception e) {
//            LogUtils.addLogDB(e);
//            HibernateUtil.rollBackCurrentSession();
//            return result;
//        }
//
//    }
    
    /**
     * lấy đầy đủ Certificate theo certificateId
     * @param certificateId
     * @return 
     */
    public IfdCertificate getIfdCertificate(Long certificateId){
        ifdCertificateProductDAO = new IfdCertificateProductDAO();
        ifdAttachmentDAO = new IfdAttachmentDAO();
        ifdCertificateDAO = new IfdCertificateDAO();
        
        IfdCertificate result = ifdCertificateDAO.findById(certificateId);
        if (result != null) {
            List<IfdAttachment> lstAttach = ifdAttachmentDAO.findListAttachmentByObjectTypeAndObjectId(IfdConstants.ATTACH_OBJEC_TYPE.CERTIFICATE, certificateId);
            if (lstAttach != null && !lstAttach.isEmpty()) {
                result.setAttach(lstAttach.get(0));
            }
            
            List<IfdCertificateProduct> lstProduct = ifdCertificateProductDAO.findAllByCertificateId(certificateId);
            if (lstProduct != null && !lstProduct.isEmpty()) {
                result.setLstProduct(lstProduct);
            }
        }
        
        return result;
    }
    
}
