/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdEvaluation;
import com.viettel.module.importfood.BO.IfdEvaluationComment;
import com.viettel.module.importfood.BO.IfdEvaluationProduct;
import com.viettel.module.importfood.DAO.IfdEvaluationCommentDAO;
import com.viettel.module.importfood.DAO.IfdEvaluationDAO;
import com.viettel.module.importfood.DAO.IfdEvaluationProductDAO;

/**
 *
 * @author Administrator
 */
public class IfdEvaluationService {
    private IfdEvaluationDAO ifdEvaluationDAO;
    private IfdEvaluationProductDAO ifdEvaluationProductDAO;
    private IfdEvaluationCommentDAO ifdEvaluationCommentDAO;
    
//    ===================SAVE========================
    public void saveEvaluation(IfdEvaluation evaluationFile){
        ifdEvaluationDAO.save(evaluationFile);
    }
    
    public void saveEvaluationProduct(IfdEvaluationProduct product){
        ifdEvaluationProductDAO.save(product);
    }
    
    public void saveEvaluationComment(IfdEvaluationComment comment){
        ifdEvaluationCommentDAO.save(comment);
    }
    
//    =================FIND LATEST=================
    public IfdEvaluation findLatestEvaluationFileByIfdFileId(Long fileId){
        return ifdEvaluationDAO.findLatestByIfdFileID(fileId);
    }
}
