/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileFeeNotice;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileFeeNoticeDAO;
import com.viettel.module.importfood.Model.IfdFileFeeNoticeModel;
import com.viettel.utils.HibernateUtil;
import com.viettel.utils.LogUtils;

/**
 *
 * @author quannn
 */
public class IfdFileFeeNoticeService {
    
    private IfdFileFeeNoticeDAO ifdFileFeeNoticeDAO;
    
    private IfdFileDAO ifdFileDAO;
    
    /**
     * Create Thông báo phí
     * @param feeNotice
     * @return 
     */
    public IfdFileFeeNotice createIfdFileFeeNotice(IfdFileFeeNotice feeNotice) {
        IfdFileFeeNotice result = null;
        ifdFileFeeNoticeDAO = new IfdFileFeeNoticeDAO();
        ifdFileDAO = new IfdFileDAO();
        
        try {
            String fileCode = feeNotice.getFileCode();
            IfdFile file = ifdFileDAO.findByFileCode(fileCode);
            if (file != null){
                
                feeNotice.setFileId(file.getFileId());
                feeNotice.setFileCode(file.getFileCode());
                ifdFileFeeNoticeDAO.saveOrUpdate(feeNotice);
                result = ifdFileFeeNoticeDAO.findLatestByFileId(file.getFileId());
                
                HibernateUtil.commitCurrentSessions();
            }
            
            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            HibernateUtil.rollBackCurrentSession();
            return result;
        }

    }
    
    /**
     * get bản ghi mới nhất của Thông báo phí theo fileId
     * @param fileId
     * @return 
     */
    public IfdFileFeeNotice findLatestByFileId(Long fileId) {
        IfdFileFeeNotice result = null;
        ifdFileFeeNoticeDAO = new IfdFileFeeNoticeDAO();
        
        try {
            result = ifdFileFeeNoticeDAO.findLatestByFileId(fileId);
            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            return result;
        }

    }
    
    public IfdFileFeeNoticeModel findLatestModelByFileId(Long fileId) {
        IfdFileFeeNoticeModel result = null;
        ifdFileFeeNoticeDAO = new IfdFileFeeNoticeDAO();
        
        try {
            result = ifdFileFeeNoticeDAO.findLatestModelByFileId(fileId);
            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            return result;
        }

    }
}
