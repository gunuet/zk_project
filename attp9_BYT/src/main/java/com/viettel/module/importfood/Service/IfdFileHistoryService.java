/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileHistory;
import com.viettel.module.importfood.Constant.FileStatus;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileHistoryDAO;
import com.viettel.utils.HibernateUtil;
import com.viettel.utils.LogUtils;

/**
 *
 * @author quannn
 */
public class IfdFileHistoryService {
    
    private IfdFileHistoryDAO ifdFileHistoryDAO;
    
    private IfdFileDAO ifdFileDAO;

    /**
     * Create lịch sử xử lý hồ sơ
     *
     * @param fileHistory
     * @return
     */
    public IfdFileHistory createIfdFileHistory(IfdFileHistory fileHistory) {
        IfdFileHistory result = null;
        ifdFileHistoryDAO = new IfdFileHistoryDAO();
        ifdFileDAO = new IfdFileDAO();
        
        try {
            String fileCode = fileHistory.getFileCode();
            IfdFile file = ifdFileDAO.findByFileCode(fileCode);
            if (file != null) {
                // trạng thái của bản tin "Gửi sửa hồ sơ" sẽ lấy là trạng thái hiện tại của hồ sơ đó.
                if (!fileHistory.getFileStatus().equals(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatus())) {
                    file.setFileStatus(fileHistory.getFileStatus());
                    file.setFileStatusName(fileHistory.getFileStatusName());
                    
                    ifdFileDAO.saveOrUpdate(file);
                }
                
                fileHistory.setFileId(file.getFileId());
                fileHistory.setFileCode(file.getFileCode());
                ifdFileHistoryDAO.saveOrUpdate(fileHistory);
                result = ifdFileHistoryDAO.findLatestByFileId(file.getFileId());
                
                HibernateUtil.commitCurrentSessions();
            }
            
            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            HibernateUtil.rollBackCurrentSession();
            return result;
        }
        
    }
    
    /**
     * Search fileHistory theo fileCode
     * @param fileCode
     * @param start
     * @param size
     * @return 
     */
    public PagingListModel searchFileHistory(String fileCode, int start, int size) {
        ifdFileHistoryDAO = new IfdFileHistoryDAO();
        return ifdFileHistoryDAO.searchFileHistory(fileCode, start, size);
    }    
    
}
