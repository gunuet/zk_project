/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.module.importfood.DAO.dao.IfdAttachmentDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdFilePaymentDAO;
import com.viettel.module.importfood.Model.IfdFilePaymentModel;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.HibernateUtil;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author quannn
 */
public class IfdFilePaymentService {
    
    private IfdFilePaymentDAO ifdFilePaymentDAO;
    
    private IfdFileDAO ifdFileDAO;
    
    private IfdAttachmentDAO ifdAttachmentDAO;
    
    /**
     * Create file payment
     * @param feePayment
     * @return 
     */
    public IfdFilePayment createIfdFilePayment(IfdFilePayment feePayment) {
        IfdFilePayment result = null;
        ifdFilePaymentDAO = new IfdFilePaymentDAO();
        ifdFileDAO = new IfdFileDAO();
        ifdAttachmentDAO = new IfdAttachmentDAO();
        
        try {
            String fileCode = feePayment.getFileCode();
            IfdFile file = ifdFileDAO.findByFileCode(fileCode);
            if (file != null){
                
                feePayment.setFileId(file.getFileId());
                feePayment.setFileCode(file.getFileCode());
                ifdFilePaymentDAO.saveOrUpdate(feePayment);
                IfdFilePayment latestPayment = ifdFilePaymentDAO.findLatestByFileId(file.getFileId());
                
                if (latestPayment != null) {
                    IfdAttachment attachPayment = latestPayment.getAttach();
                    if (attachPayment != null) {
                        attachPayment.setObjectId(latestPayment.getPaymentId());
                        attachPayment.setObjectType(IfdConstants.ATTACH_OBJEC_TYPE.FILE_PAYMENT);
                        ifdAttachmentDAO.saveOrUpdate(attachPayment);
                    }
                    result = getIfdFilePayment(latestPayment.getPaymentId());
                    HibernateUtil.commitCurrentSessions();
                }
                
            }
            
            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            HibernateUtil.rollBackCurrentSession();
            return result;
        }

    }
    
    /**
     * get đầy đủ file payment by paymentId
     * @param paymentId
     * @return 
     */
    public IfdFilePayment getIfdFilePayment(Long paymentId){
        ifdAttachmentDAO = new IfdAttachmentDAO();
        ifdFilePaymentDAO = new IfdFilePaymentDAO();
        
        IfdFilePayment result = ifdFilePaymentDAO.findById(paymentId);
        if (result != null) {
            List<IfdAttachment> lstAttach = ifdAttachmentDAO.findListAttachmentByObjectTypeAndObjectId(IfdConstants.ATTACH_OBJEC_TYPE.FILE_PAYMENT, paymentId);
            if (lstAttach != null && !lstAttach.isEmpty()) {
                result.setAttach(lstAttach.get(0));
            }
        }
        
        return result;
    }
    
    /**
     * get list FilePayment by fileId
     * @param fileId
     * @return 
     */
    public List<IfdFilePaymentModel> findAllByFileId(Long fileId){
        List<IfdFilePaymentModel> result = new ArrayList<>();
        ifdFilePaymentDAO = new IfdFilePaymentDAO();
        
        List<IfdFilePaymentModel> lstPayment = ifdFilePaymentDAO.findAllByFileId(fileId);
        if(lstPayment != null && !lstPayment.isEmpty()){
            for (IfdFilePaymentModel ifdFilePayment : lstPayment) {
                IfdFilePayment filePayment = getIfdFilePayment(ifdFilePayment.getPaymentId());
                if (filePayment != null){
                    result.add(filePayment.convertToIfdFileModel(filePayment));
                }
            }
        }
        
        return result;
    }
    
    
}
