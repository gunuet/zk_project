package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.DAO.dao.IfdFileProductDAO;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class IfdFileProductService {
    private IfdFileProductDAO ifdFileProductDAO;
    
    public List<IfdFileProduct> findAllByFileId(Long fileId){
        ifdFileProductDAO = new IfdFileProductDAO();
        return ifdFileProductDAO.findAllByFileId(fileId);
    }
}
