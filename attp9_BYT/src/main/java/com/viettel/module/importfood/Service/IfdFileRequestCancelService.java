/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileRequestCancel;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileRequestCancelDAO;
import com.viettel.utils.HibernateUtil;
import com.viettel.utils.LogUtils;

/**
 *
 * @author quannn
 */
public class IfdFileRequestCancelService {
    
    private IfdFileRequestCancelDAO ifdFileRequestCancelDAO;
    
    private IfdFileDAO ifdFileDAO;
    
    /**
     * create Yêu cầu rút hồ sơ
     * @param requestCancel
     * @return 
     */
    public IfdFileRequestCancel createIfdFileRequestCancel(IfdFileRequestCancel requestCancel) {
        IfdFileRequestCancel result = null;
        ifdFileRequestCancelDAO = new IfdFileRequestCancelDAO();
        ifdFileDAO = new IfdFileDAO();
        
        try {
            String fileCode = requestCancel.getFileCode();
            IfdFile file = ifdFileDAO.findByFileCode(fileCode);
            if (file != null){
                
                requestCancel.setFileId(file.getFileId());
                requestCancel.setFileCode(file.getFileCode());
                ifdFileRequestCancelDAO.saveOrUpdate(requestCancel);
                result = ifdFileRequestCancelDAO.findLatestByFileId(file.getFileId());
                
                HibernateUtil.commitCurrentSessions();
            }
            
            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            HibernateUtil.rollBackCurrentSession();
            return result;
        }

    }
    
    
}
