/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFileResult;
import com.viettel.module.importfood.DAO.dao.IfdFileResultDAO;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.LogUtils;
import java.util.List;

/**
 *
 * @author quannn
 */
public class IfdFileResultService {

    private IfdFileResultDAO ifdFileResultDAO;
    
    private IfdAttachmentService ifdAttachmentService;

    public IfdFileResult findLatestByFileIdAndFileStatus(Long fileId, Long fileStatus) {
        IfdFileResult result = null;
        List<IfdFileResult> lstFileResult = null;
        ifdFileResultDAO = new IfdFileResultDAO();
        ifdAttachmentService = new IfdAttachmentService();

        try {
            lstFileResult = ifdFileResultDAO.findListByFileIdAndFileStatus(fileId, fileStatus);
            if (lstFileResult != null && !lstFileResult.isEmpty()) {
                result = lstFileResult.get(0);
                List<IfdAttachment> lstAttach = ifdAttachmentService.findAllByObjectTypeAndObjectId(IfdConstants.ATTACH_OBJEC_TYPE.FILE_RESULT, result.getFileResultId());
                if (lstAttach != null && !lstAttach.isEmpty()){
                    result.setAttach(lstAttach.get(0));
                }
            }

            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            return result;
        }

    }

}
