/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.DAO.dao.IfdAttachmentDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileProductDAO;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.HibernateUtil;
import com.viettel.utils.LogUtils;
import java.util.List;

/**
 *
 * @author quannn
 */
public class IfdFileService {
    
    private IfdFileProductDAO ifdFileProductDAO;
    
    private IfdAttachmentDAO ifdAttachmentDAO;
    
    private IfdFileDAO ifdFileDAO;
    
    public IfdFile findByFileCode(String fileCode){
        ifdFileDAO = new IfdFileDAO();
        return ifdFileDAO.findByFileCode(fileCode);
    }
    
    public List<IfdFile> findAllByFileStatus(List<Long> lstFileStatus){
        IfdFileDAO ifdFileDAO = new IfdFileDAO();
        List<IfdFile> result = ifdFileDAO.findAllByFileStatus(lstFileStatus);
        return result;
    }
    
    public IfdFile createIfdFile(IfdFile file) {
        IfdFile result = null;
        ifdFileProductDAO = new IfdFileProductDAO();
        ifdAttachmentDAO = new IfdAttachmentDAO();
        ifdFileDAO = new IfdFileDAO();
        
        try {
            String fileCode = file.getFileCode();
            ifdFileDAO.saveOrUpdate(file);

            IfdFile ifdFileSaved = ifdFileDAO.findByFileCode(file.getFileCode());
            Long fileId = ifdFileSaved.getFileId();

            //save List IfdFileProduct
            List<IfdFileProduct> lstProduct = file.getLstProduct();
            if (lstProduct != null && !lstProduct.isEmpty()) {
                for (IfdFileProduct ifdFileProduct : lstProduct) {
                    ifdFileProduct.setFileId(fileId);
                    ifdFileProduct.setFileCode(fileCode);
                    ifdFileProductDAO.saveOrUpdate(ifdFileProduct);

                    List<IfdFileProduct> lstProductSaved = ifdFileProductDAO.findAllByFileId(fileId);
                    if (lstProductSaved != null && !lstProductSaved.isEmpty()) {
                        IfdFileProduct latestProductSaved = lstProductSaved.get(0);
                        Long productId = latestProductSaved.getFileProductId();

                        IfdAttachment attachProduct = latestProductSaved.getAttach();
                        if (attachProduct != null) {
                            attachProduct.setObjectId(productId);
                            attachProduct.setObjectType(IfdConstants.ATTACH_OBJEC_TYPE.FILE_PRODUCT);
                            ifdAttachmentDAO.saveOrUpdate(attachProduct);
                        }
                    }

                }
            }
            
            //save List IfdAttachment
            List<IfdAttachment> lstAttach = file.getLstAttach();
            if (lstAttach != null && !lstAttach.isEmpty()) {
                for (IfdAttachment attachment : lstAttach) {
                    attachment.setObjectId(fileId);
                    attachment.setObjectType(IfdConstants.ATTACH_OBJEC_TYPE.FILE);
                    ifdAttachmentDAO.saveOrUpdate(attachment);
                }
            }

            result = getIfdFile(fileId);
            HibernateUtil.commitCurrentSessions();
            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            HibernateUtil.rollBackCurrentSession();
            return result;
        }

    }
    
    
    public IfdFile getIfdFile(Long fileId){
        ifdFileProductDAO = new IfdFileProductDAO();
        ifdAttachmentDAO = new IfdAttachmentDAO();
        ifdFileDAO = new IfdFileDAO();
        
        IfdFile result = ifdFileDAO.findById(fileId);
        if (result != null) {
            List<IfdAttachment> lstAttach = ifdAttachmentDAO.findListAttachmentByObjectTypeAndObjectId(IfdConstants.ATTACH_OBJEC_TYPE.FILE, fileId);
            if (lstAttach != null && !lstAttach.isEmpty()) {
                result.setLstAttach(lstAttach);
            }
            
            List<IfdFileProduct> lstProduct = ifdFileProductDAO.findAllByFileId(fileId);
            if (lstProduct != null && !lstProduct.isEmpty()) {
                for (IfdFileProduct ifdFileProduct : lstProduct) {
                    Long productId = ifdFileProduct.getFileProductId();
                    List<IfdAttachment> lstAttachProduct = ifdAttachmentDAO.findListAttachmentByObjectTypeAndObjectId(IfdConstants.ATTACH_OBJEC_TYPE.FILE_PRODUCT, productId);
                    if(lstAttachProduct != null && !lstAttachProduct.isEmpty()){
                        ifdFileProduct.setAttach(lstAttachProduct.get(0));
                    }
                    
                }
                result.setLstProduct(lstProduct);
            }
        }
        
        return result;
    }
    
}
