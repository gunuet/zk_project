/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdHandlingReport;
import com.viettel.module.importfood.DAO.dao.IfdAttachmentDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdHandlingReportDAO;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.HibernateUtil;
import com.viettel.utils.LogUtils;
import java.util.List;

/**
 *
 * @author quannn
 */
public class IfdHandlingReportService {

    private IfdHandlingReportDAO ifdHandlingReportDAO;

    private IfdFileDAO ifdFileDAO;

    private IfdAttachmentDAO ifdAttachmentDAO;

    /**
     * Create handling report
     *
     * @param handlingReport
     * @return
     */
    public IfdHandlingReport createIfdHandlingReport(IfdHandlingReport handlingReport) {
        IfdHandlingReport result = null;
        ifdHandlingReportDAO = new IfdHandlingReportDAO();
        ifdFileDAO = new IfdFileDAO();
        ifdAttachmentDAO = new IfdAttachmentDAO();

        try {
            String fileCode = handlingReport.getFileCode();
            IfdFile file = ifdFileDAO.findByFileCode(fileCode);
            if (file != null) {

                handlingReport.setFileId(file.getFileId());
                handlingReport.setFileCode(file.getFileCode());
                ifdHandlingReportDAO.saveOrUpdate(handlingReport);
                IfdHandlingReport latestHandlingReport = ifdHandlingReportDAO.findLatestByFileId(file.getFileId());

                if (latestHandlingReport != null) {
                    List<IfdAttachment> lstAttach = latestHandlingReport.getLstAttachs();
                    if (lstAttach != null && !lstAttach.isEmpty()) {
                        for (IfdAttachment attach : lstAttach) {
                            attach.setObjectId(latestHandlingReport.getHandlingReportId());
                            attach.setObjectType(IfdConstants.ATTACH_OBJEC_TYPE.HANDLING_REPORT);
                            ifdAttachmentDAO.saveOrUpdate(attach);
                        }

                    }
                    result = getIfdHandlingReport(latestHandlingReport.getHandlingReportId());
                    HibernateUtil.commitCurrentSessions();
                }

            }

            return result;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            HibernateUtil.rollBackCurrentSession();
            return result;
        }

    }

    /**
     * get đầy đủ handing report
     *
     * @param handlingReportId
     * @return
     */
    public IfdHandlingReport getIfdHandlingReport(Long handlingReportId) {
        ifdAttachmentDAO = new IfdAttachmentDAO();
        ifdHandlingReportDAO = new IfdHandlingReportDAO();

        IfdHandlingReport result = ifdHandlingReportDAO.findById(handlingReportId);
        if (result != null) {
            List<IfdAttachment> lstAttach = ifdAttachmentDAO.findListAttachmentByObjectTypeAndObjectId(IfdConstants.ATTACH_OBJEC_TYPE.HANDLING_REPORT, handlingReportId);
            if (lstAttach != null && !lstAttach.isEmpty()) {
                result.setLstAttachs(lstAttach);
            }
        }

        return result;
    }

    /**
     * get latest Handling Report by fileId
     *
     * @param fileId
     * @return
     */
    public IfdHandlingReport findLatestByFileId(Long fileId) {
        IfdHandlingReport result = null;
        ifdHandlingReportDAO = new IfdHandlingReportDAO();

        List<IfdHandlingReport> lstHandlingReport = ifdHandlingReportDAO.findAllByFileId(fileId);
        if (lstHandlingReport != null && !lstHandlingReport.isEmpty()) {
            result = getIfdHandlingReport(lstHandlingReport.get(0).getHandlingReportId());
        }

        return result;
    }

}
