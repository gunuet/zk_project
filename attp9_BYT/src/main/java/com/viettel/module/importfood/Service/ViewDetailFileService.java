/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.Service;

import com.viettel.module.importfood.BO.IfdAttachment;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.BO.IfdFileResult;
import com.viettel.module.importfood.DAO.dao.IfdAttachmentDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileDAO;
import com.viettel.module.importfood.DAO.dao.IfdFilePaymentDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileProductDAO;
import com.viettel.module.importfood.DAO.dao.IfdFileResultDAO;
import com.viettel.module.importfood.Model.IfdFilePaymentModel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ViewDetailFileService {

    public IfdFile getIfdFileById(Long idFile) {
        IfdFileDAO fileDao = new IfdFileDAO();
        IfdFile file = fileDao.getById("fileId", idFile);
        return file;
    }

    public List<IfdAttachment> getListAttachmentByIdFile(Long idFile) {
        IfdAttachmentDAO attachDao = new IfdAttachmentDAO();
        List<IfdAttachment> listAttachment = attachDao.findListAttachmentByIdIfdFile(idFile);
        return listAttachment;
    }

    public List<IfdFileResult> getListFileResultByIdFile(Long idFile) {
        IfdFileResultDAO resultDao = new IfdFileResultDAO();
        List<IfdFileResult> listResult = resultDao.findListFileResultByIdIfdFile(idFile);
        return listResult;
    }

    public List<IfdFileProduct> getListProductByIfFile(Long idFile) {
        IfdFileProductDAO productDao = new IfdFileProductDAO();
        List<IfdFileProduct> listProdut = productDao.findListFileResultByIdIfdFile(idFile);
        return listProdut;
    }

    public List<IfdFilePaymentModel> getListPaymentByIfFile(Long idFile) {
        IfdFilePaymentDAO paymentDao = new IfdFilePaymentDAO();
        List<IfdFilePaymentModel> listPayment = paymentDao.findAllByFileId(idFile);
        return listPayment;
    }

    public List<IfdAttachment> getListAttachmentByObjectTypeAndObjectId(Long objectType, Long objectId) {
        IfdAttachmentDAO attachDao = new IfdAttachmentDAO();
        List<IfdAttachment> listAttachment = attachDao.findListAttachmentByObjectTypeAndObjectId(objectType, objectId);
        return listAttachment;
    }
}
