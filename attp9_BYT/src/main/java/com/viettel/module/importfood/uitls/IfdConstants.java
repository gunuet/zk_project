/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.uitls;

/**
 *
 * @author Administrator 16/03/2019
 */
public class IfdConstants {

    /**
     * trạng thái hồ sơ
     */
    public interface FILE_STATUS {

        public static Long NEW_SUBMIT_FILE = 1L; // hồ sơ nộp mới

        public static Long ADDITIONAL_FEES_FILE = 2L; // hồ sơ nộp phí bổ sung

        public static String NEW_SUBMIT_FILE_STR = "Hồ sơ nộp mới";

        public static String ADDITIONAL_FEES_FILE_STR = "Hồ sơ nộp phí bổ sung";

    }

    /**
     * Phương thức kiểm tra
     */
    public interface CHECK_METHOD {

        public static Long NORMAL = 1L; // Kiểm tra thường

        public static Long TIGHT = 2L; // Kiểm tra chặt

        public static String NORMAL_STR = " Kiểm tra thường";

        public static String TIGHT_STR = "Kiểm tra chặt";
    }

    /**
     * Trạng thái đối tượng
     */
    public interface OBJECT_STATUS {

        public static Long ACTIVE = 1L; // Hoạt động

        public static Long INACTIVE = 0L; // Không hoạt động
    }

    /**
     * Loại đối tượng của file đính kèm
     */
    public interface ATTACH_OBJEC_TYPE {

        public static Long FILE = 1L;// File đính kèm hồ sơ 

        public static Long FILE_PAYMENT = 2L;// File đính kèm thông báo nộp phí 

        public static Long FILE_PRODUCT = 3L;// File đính kèm sản phẩm

        public static Long FILE_RESULT = 4L;// File đính kèm kết quả 

        public static Long CERTIFICATE = 5L;// File đính kèm kết quả cấp phép

//        public static Long CERTIFICATE_PRODUCT = 6L;// File đính kèm sản phẩm cấp phép

//        public static Long HANDLING_REPORT_PRODUCT = 7L;// File đính kèm xử lý sản phẩm báo cáo 

        public static Long HANDLING_REPORT = 8L;// File đính kèm báo cáo xử lý ko dat

        public static Long EVALUATION = 9L;//File đính kèm đánh giá kết quả
    }

    public interface PARAMETER {

        public static String VMC_PARENT = "previousVMC";
        
    }
    
    
    
    public interface ATTP_TYPE {

        public static final String TYPE_101 = "101";
        public static final String TYPE_102 = "102";
        public static final String TYPE_103 = "103";
        public static final String TYPE_104 = "104";
        public static final String TYPE_105 = "105";
        public static final String TYPE_106 = "106";
        public static final String TYPE_107 = "107";
        public static final String TYPE_108 = "108";
        public static final String TYPE_109 = "109";
        public static final String TYPE_110 = "110";
    }

    public interface ATTP_FUNCTION {

        public static final String FUNCTION_01 = "01";
        public static final String FUNCTION_02 = "02";
        public static final String FUNCTION_03 = "03";
        public static final String FUNCTION_04 = "04";
        public static final String FUNCTION_05 = "05";
        public static final String FUNCTION_06 = "06";
        public static final String FUNCTION_07 = "07";
        public static final String FUNCTION_08 = "08";
        public static final String FUNCTION_09 = "09";
        public static final String FUNCTION_10 = "10";
        public static final String FUNCTION_11 = "11";
        public static final String FUNCTION_12 = "12";
        public static final String FUNCTION_13 = "13";
        public static final String FUNCTION_14 = "14";
        public static final String FUNCTION_15 = "15";
        public static final String FUNCTION_16 = "16";
        public static final String FUNCTION_17 = "17";
        public static final String FUNCTION_18 = "18";
        public static final String FUNCTION_19 = "19";
        public static final String FUNCTION_20 = "20";
        public static final String FUNCTION_21 = "21";
        public static final String FUNCTION_22 = "22";
        public static final String FUNCTION_23 = "23";
        public static final String FUNCTION_24 = "24";
        public static final String FUNCTION_25 = "25";
        public static final String FUNCTION_99 = "99";
    }
    
    public static final String BYT = "BYT";    
    public static final String APP_NAME = "ATTPWs";
    public static final String MESSAGE_SEPARATOR = "\\{}";
    
    public interface To {
    	public static final String NAME = "NSW";
    	public static final String IDENTITY = "NSW";
    	public static final String COUNTRY_CODE = "VN";
    	public static final String MINISTRY_CODE = "BTC";
    	public static final String ORAGANIZATION_CODE = "TCHQ";
    	public static final String UNIT_CODE = "NSW";
    }
    
    public interface From {
    	public static final String NAME = "BCA";
    	public static final String IDENTITY = "BCA";
    	public static final String COUNTRY_CODE = "VN";
    	public static final String MINISTRY_CODE = "BCA";
    	public static final String ORAGANIZATION_CODE = "C64";
        public static final String UNIT_CODE = "C64";
    }
    
    public interface Version {
    	public static final String SEND_VERSION = "01";
    	public static final String RECEIVE_VERSION = "01";
    }
    
    public interface Function {
    	public static final String FUNC_SUCCESS = "99";
    	public static final String FUNC_ERROR = "00";
    }    
    
    public interface MohProceduce {
    	public static final String ERROR = "00";
        public static final String THONG_THUONG = "BYTE0500009";// Mã thủ tục kiểm tra thông thường
        public static final String CHAT         = "BYTE0500010";// Mã thủ tục kiểm tra chặt
    }
    
    public interface MohFeeDefaut {
        public static final Long DEFAULT = 100000L;// 300k kiểm tra thông thường
        public static final Long THONG_THUONG = 300000L;// 300k kiểm tra thông thường
        public static final Long CHAT         = 1000000L;// 1 triệu kiểm tra chặt
    }

    public interface Error {
    	
    	public static final String ERR00_CODE = "ERR00_CODE";
    	public static final String ERR01_CODE = "ERR01_CODE";
        public static final String ERR02_CODE = "ERR02_CODE";
        public static final String ERR03_CODE = "ERR03_CODE";   
        public static final String ERR04_CODE = "ERR04_CODE";   
        public static final String ERR05_CODE = "ERR05_CODE";   
        
        public static final String ERR00 = "Bản tin không hợp lệ";
        public static final String ERR01 = "Thủ tục chưa được định nghĩa";
        public static final String ERR02 = "Phân tích xml bị lỗi";
        public static final String ERR03 = "Lỗi phân tích xml: không lấy được thủ tục từ xml";
        public static final String ERR04 = "Message Type: Không tồn tại ";
        public static final String ERR05 = "Nội dung bản tin không có dữ liệu, hoặc không hợp lệ.";
        
        public static final String BTNMT6_CODE = "BTNMT6_CODE";
        public static final String BTNMT6 = "Không lưu được dữ liệu!";
                
        public static final String BTNMT01_CODE = "BTNMT01_CODE";
        public static final String BTNMT02_CODE = "BTNMT03_CODE";
        public static final String BTNMT03_CODE = "BTNMT03_CODE";
        public static final String BTNMT04_CODE = "BTNMT04_CODE";
        public static final String BTNMT05_CODE = "BTNMT05_CODE";
        public static final String BTNMT06_CODE = "BTNMT06_CODE";
        public static final String BTNMT07_CODE = "BTNMT07_CODE";
        public static final String BTNMT08_CODE = "BTNMT08_CODE";
        public static final String BTNMT09_CODE = "BTNMT09_CODE";

        public static final String BTNMT01 = "Chưa định nghĩa function tiếp nhận";
        public static final String BTNMT02 = "Function is null";
        public static final String BTNMT03 = "Lỗi phân tích bản tin";
        public static final String BTNMT04 = "Lỗi lưu bản tin";
        public static final String BTNMT05 = "Lưu không thành công";
        public static final String BTNMT06 = "Không lấy được ID Cơ quan xử lý";
        public static final String BTNMT07 = "Function không hợp lệ";
        public static final String BTNMT08 = "Không lưu được dữ liệu file đính kèm";
        public static final String BTNMT09 = "Không lưu được dữ liệu!";
        
        public static final String BCA00_CODE = "BCA00_CODE";
        public static final String BCA01_CODE = "BCA01_CODE";
        public static final String BCA02_CODE = "BCA02_CODE";
        public static final String BCA03_CODE = "BCA03_CODE";
        public static final String BCA04_CODE = "BCA04_CODE";
        public static final String BCA05_CODE = "BCA05_CODE";
        public static final String BCA06_CODE = "BCA06_CODE";
        public static final String BCA07_CODE = "BCA07_CODE";
        public static final String BCA08_CODE = "BCA08_CODE";
        public static final String BCA09_CODE = "BCA09_CODE";
        
        public static final String BCA00 = "Không lưu được dữ liệu!";
        public static final String BCA01 = "Mã hồ sơ đã tồn tại";
        public static final String BCA02 = "Bản tin không phù hợp với trạng thái hồ sơ";
        public static final String BCA03 = "Số giấy phép không tồn tại hoặc không phù hợp với mã hồ sơ";
        public static final String BCA04 = "Lỗi lưu bản tin";
        public static final String BCA05 = "Lưu không thành công";
        public static final String BCA06 = "Không lấy được ID Cơ quan xử lý";
        public static final String BCA07 = "Function không hợp lệ";
        public static final String BCA08 = "Không lưu được dữ liệu file đính kèm";
        public static final String BCA09 = "Không lưu được dữ liệu!";
    }

    public interface RestUrl {

        public static final String URL_UPLOAD = "/file/upload";
        public static final String URL_DOWNLOAD = "/file/download";
    }

    public interface RestMethod {

        public static final String POST = "POST";
        public static final String PUT = "PUT";
        public static final String DELETE = "DELETE";
    }

    public interface System {

//        public static final String NSW = "NSW";
        public static final String NSW_NAME = "Tổng cục Hải quan";
//        public static final String BCA = "BCA";
        public static final String ATTP_NAME = "Cục An toàn thực phẩm";
    }
    
    public interface CDATA {

        public static final String START_TAG = "<![CDATA[";
        public static final String END_TAG = "]]>";
    }
    
    public interface RESPONSETYPE {

        public static final String FUNCTION_01 = "01";
        public static final String TYPE_SUCCESS = "99";
        public static final String ERROR = "00";
    }
            
    public interface ENVELOP_TAG_ENCODE {

        public static final String OPEN_TAG = "&lt;Envelope&gt;";
        public static final String CLOSE_TAG = "&lt;/Envelope&gt;";
    }
    
    public interface TAG_ENCODE {

        public final static String OPEN_TAG = "&lt;";
        public static final String CLOSE_TAG = "&gt;";
        public static final String UNKNOW_TAG = "";
        public static final String ENTER_TAG = "&#13;";
        public static final String AND_TAG = "&";
    }
    
    public interface TAG_NO_ENCODE {

        public final static String OPEN_TAG = "<";
        public static final String CLOSE_TAG = ">";
        public static final String AND_TAG = "&";
    }
    
    
    public interface XSDPREFIX {
        public static final String IFD = "Attp_ifd_";
    }
    
    public static final String XML_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
