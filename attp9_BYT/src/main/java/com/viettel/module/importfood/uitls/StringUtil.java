package com.viettel.module.importfood.uitls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

public final class StringUtil {

    private static final Logger LOG = Logger.getLogger(StringUtil.class);

    public static final String COLON_SEPARATOR = ":";

    public static final String COMMA_SEPARATOR = ",";

    public static final String EMPTY = "";

    public static boolean isBlank(String str) {
        return org.apache.commons.lang.StringUtils.isBlank(str);
    }

    private StringUtil() {
    }

    /**
     * Viết thường ký tự đầu tiên của chuỗi
     *
     * @param string
     * @return
     */
    public static String decapitalize(String string) {
        if (string == null || string.length() == 0) {
            return string;
        }
        char c[] = string.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        return new String(c);
    }

    /**
     * Viết hoa ký tự đầu tiên của chuỗi
     *
     * @param string
     * @return
     */
    public static String ecapitalize(String string) {
        if (string == null || string.length() == 0) {
            return string;
        }
        char c[] = string.toCharArray();
        c[0] = Character.toUpperCase(c[0]);
        return new String(c);
    }

    public static String toString(Reader in) throws IOException {
        final char[] buffer = new char[0x10000];
        StringBuilder out = new StringBuilder();
        int reader;
        do {
            reader = in.read(buffer, 0, buffer.length);
            if (reader > 0) {
                out.append(buffer, 0, reader);
            }
        } while (reader >= 0);
        return out.toString();
    }

    public static String toString(InputStream in) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        in.close();
        return sb.toString();
    }

    public static String removeHTML(String htmlString) {
        // Remove HTML tag from java String
        String noHTMLString = htmlString.replaceAll("\\<.*?\\>", EMPTY);

        // Remove Carriage return from java String
        noHTMLString = noHTMLString.replaceAll("\r", "<br/>");
        // Remove New line from java string and replace html break
        noHTMLString = noHTMLString.replaceAll("\n", " ");
        return noHTMLString;
    }

    public static String deEscapeHTML(String string) {
        if (string == null) {
            return null;
        }
        String result = string;
        List<String[]> codecs = new ArrayList<>();
        codecs.add(new String[]{"&#39;", "'"});
        codecs.add(new String[]{"&quot;", "\""});
        codecs.add(new String[]{"&lt;", "<"});
        codecs.add(new String[]{"&gt;", ">"});
        codecs.add(new String[]{"&amp;", "&"});
        for (int i = 0; i < codecs.size(); i++) {
            while (result.contains(codecs.get(i)[0])) {
                result = result.replace(codecs.get(i)[0], codecs.get(i)[1]);
            }
        }
        return result;
    }

    public static String escapeHTML(String string) {
        if (string == null) {
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder();
        int length = string.length();
        for (int i = 0; i < length; ++i) {
            char c = string.charAt(i);
            switch (c) {
                case '\'':
                    stringBuilder.append("&#39;");
                    break;
                case '"':
                    stringBuilder.append("&quot;");
                    break;
                case '<':
                    stringBuilder.append("&lt;");
                    break;
                case '>':
                    stringBuilder.append("&gt;");
                    break;
                case '&':
                    stringBuilder.append("&amp;");
                    break;
                default:
                    stringBuilder.append(c);
            }
        }
        return stringBuilder.toString();
    }

    public static String escapeSQL(String input) {
        String result = input.trim().replace("/", "//").replace("_", "/_").replace("%", "/%");
        return result;
    }

    public static String toLikeString(String content) {
        return "%" + StringUtil.escapeSQL(content.toLowerCase().trim()) + "%";
    }

    public static boolean compare(String str, String source) {
        return str == null ? source == null : str.equals(source);
    }

    public static <T> String join(T[] array, String cement, boolean isSql) {
        StringBuilder builder = new StringBuilder();

        if (array == null || array.length == 0) {
            return null;
        }
        for (T t : array) {
            builder.append((isSql == true) ? "'" + t.toString() + "'" : t).append(cement);
        }

        builder.delete(builder.length() - cement.length(), builder.length());

        return builder.toString();
    }

    public static String[] split(String input, String delimiter) {
        String[] output = null;
        if (input != null && !"".equals(input)) {
            input = org.apache.commons.lang.StringUtils.strip(input.trim(), delimiter);
            if (input.contains(";")) {
                output = org.apache.commons.lang.StringUtils.split(input, delimiter);
            } else {
                output = new String[1];
                output[0] = input;
            }
        }
        return output;
    }

    public static String slugify(String input) {
        if (input != null && !EMPTY.equals(input)) {
            input = input.trim().replaceAll(" ", "-");
            String tmp = input.replaceAll("[^a-zA-Z0-9-.]", "-");
            return org.apache.commons.lang.StringUtils.strip(tmp, "-");
        }
        return null;
    }

    public static String toUnsignVietnamese(String input) {
        String[] pattern = new String[]{
            "á", "à", "ả", "ã", "ạ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ",
            "Á", "À", "Ả", "Ã", "Ạ", "Ă", "Ắ", "Ằ", "Ẳ", "Ẵ", "Ặ", "Â", "Ấ", "Ầ", "Ẩ", "Ẫ", "Ậ",
            "đ", "Đ",
            "é", "è", "ẻ", "ẽ", "ẹ", "ê", "ế", "ề", "ể", "ễ", "ệ",
            "É", "È", "Ẻ", "Ẽ", "Ẹ", "Ê", "Ế", "Ề", "Ể", "Ễ", "Ệ",
            "í", "ì", "ỉ", "ĩ", "ị",
            "Í", "Ì", "Ỉ", "Ĩ", "Ị",
            "ó", "ò", "ỏ", "õ", "ọ", "ơ", "ớ", "ờ", "ở", "ỡ", "ợ", "ô", "ố", "ồ", "ổ", "ỗ", "ộ",
            "Ó", "Ò", "Ỏ", "Õ", "Ọ", "Ơ", "Ớ", "Ờ", "Ở", "Ỡ", "Ợ", "Ô", "Ố", "Ồ", "Ổ", "Ỗ", "Ộ",
            "ú", "ù", "ủ", "ũ", "ụ", "ư", "ứ", "ừ", "ử", "ữ", "ự",
            "Ú", "Ù", "Ủ", "Ũ", "Ụ", "Ư", "Ứ", "Ừ", "Ử", "Ữ", "Ự",
            "ý", "ỳ", "ỷ", "ỹ", "ỵ",
            "Ý", "Ỳ", "Ỷ", "Ỹ", "Ỵ"
        };

        String[] replacement = new String[]{
            "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
            "d", "D",
            "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
            "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
            "i", "i", "i", "i", "i",
            "I", "I", "I", "I", "I",
            "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
            "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
            "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
            "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
            "y", "y", "y", "y", "y",
            "Y", "Y", "Y", "Y", "Y"
        };

        if (input != null && !EMPTY.equals(input)) {
            for (int i = 0; i < pattern.length; i++) {
                String pt = pattern[i];
                String rpl = replacement[i];
                input = input.replaceAll(pt, rpl);
            }
            return input;
        }
        return EMPTY;
    }

    public static String safeFilename(String filename) {
        if (filename != null && !EMPTY.equals(filename)) {
            filename = toUnsignVietnamese(filename.trim());
            filename = filename.replaceAll("[^a-zA-Z0-9-.]", "-");
            return org.apache.commons.lang.StringUtils.strip(filename, "-");
        }
        return EMPTY;
    }

    public static String formatNumber(String pattern, Number value, Boolean dotToComma) {
        if (pattern != null && value != null) {
            try {
                DecimalFormatSymbols dfs = new DecimalFormatSymbols();
                dfs.setGroupingSeparator(dotToComma ? '.' : ',');
                dfs.setDecimalSeparator(dotToComma ? ',' : '.');
                DecimalFormat df = new DecimalFormat(pattern, dfs);
                df.applyPattern(pattern);
                return df.format(value);
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
        }
        return EMPTY;
    }

    public static String replaceCharacter(String sourceStr) {
        if (sourceStr == null) {
            return null;
        }
        sourceStr = sourceStr.replace(" ", EMPTY);
        sourceStr = sourceStr.replace("<", EMPTY);
        sourceStr = sourceStr.replace(">", EMPTY);
        return sourceStr;
    }

    public static String reverse(String input) {
        if (input == null || input.isEmpty()) {
            return input;
        }
        StringBuilder sb = new StringBuilder();
        for (int index = input.length() - 1; index >= 0; index--) {
            sb.append(input.charAt(index));
        }
        return sb.toString();
    }

    public static boolean checkEmail(String email) {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean b = email.matches(EMAIL_REGEX);
        return b;
    }

    //kiểm tra chuỗi nhập vào là số
    public static boolean checkNumber(CharSequence number) {
        Pattern pattern = Pattern.compile("\\d*");
        Matcher matcher = pattern.matcher(number);

        if (matcher.matches()) {
            return  true;
        } else {
            return false;
        }
        
    }
}
