/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws;

import com.google.gson.Gson;
import com.viettel.core.sys.BO.XmlMessage;
import com.viettel.core.user.model.UserToken;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.util.Date;
import java.text.DecimalFormat;
import java.util.*;
import org.zkoss.zk.ui.Sessions;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.ws.envelope.Envelope;
import com.viettel.utils.LogUtils;
import com.viettel.module.importfood.ws.envelope.*;
import com.viettel.core.sys.DAO.*;
import com.viettel.module.importfood.BO.IfdDept;
import com.viettel.module.importfood.BO.IfdFileFeeNotice;
import com.viettel.module.importfood.BO.IfdFileHistory;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.BO.IfdFileRequestCancel;
import com.viettel.module.importfood.BO.IfdHandlingReport;
import com.viettel.module.importfood.Constant.FileStatus;
import com.viettel.module.importfood.DAO.IfdDeptDAO;
import com.viettel.module.importfood.Service.IfdFileFeeNoticeService;
import com.viettel.module.importfood.Service.IfdFileHistoryService;
import com.viettel.module.importfood.Service.IfdFilePaymentService;
import com.viettel.module.importfood.Service.IfdFileRequestCancelService;
import com.viettel.module.importfood.Service.IfdFileService;
import com.viettel.module.importfood.Service.IfdHandlingReportService;
import com.viettel.module.importfood.uitls.LogUtil;
import com.viettel.module.importfood.ws.entity.IfdAttachmentXml;
import com.viettel.module.importfood.ws.entity.IfdFilePaymentXml;
import com.viettel.module.importfood.ws.entity.IfdFileProductXml;
import com.viettel.module.importfood.ws.entity.IfdFileRequestCancelXml;
import com.viettel.module.importfood.ws.entity.IfdFileXml;
import com.viettel.module.importfood.ws.entity.IfdHandlingReportXml;
import com.viettel.module.importfood.ws.services.EnvelopeHelper;

/**
 *
 * @author quannn
 */
public class NSWReceiveHelper {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NSWReceiveHelper.class);
    private final SimpleDateFormat formatterYear = new SimpleDateFormat("yyyy");
    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private EnvelopeHelper envelopeService;

    private IfdFileService ifdFileService;

    private IfdFileHistoryService ifdFileHistoryService;

    private IfdFileFeeNoticeService ifdFileFeeNoticeService;

    private IfdFilePaymentService ifdFilePaymentService;

    private IfdFileRequestCancelService ifdFileRequestCancelService;
    
    private IfdHandlingReportService ifdHandlingReportService;

    //Convert Object <---> XML 
    public String ObjectToXml(Object obj) {

        String result = "";
        java.io.StringWriter sw = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(obj, sw);
            result = sw.toString();
        } catch (JAXBException ex) {
            LogUtils.addLogDB(ex);
        }
        return result.trim();
    }

    public Envelope xmlToEnvelope(String xml) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Envelope.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            StringReader reader = new StringReader(xml);
            return (Envelope) unmarshaller.unmarshal(reader);
        } catch (JAXBException ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    //Nhận ms từ NSW
    public String receiveMs(String evl) throws ParseException, IOException, Exception {

        Envelope envelopReturn = null;
        com.viettel.module.importfood.ws.envelope.Error error;
        String xml = evl;
        String type = "00";
        String fileCode = "00";
        String errorMsg;

        Header header, headerReceive;
        Content content;
        String documentType = "";
        String function;
        IfdFile fileExist = null;

        envelopeService = new EnvelopeHelper();
        ifdFileService = new IfdFileService();
        ifdFileHistoryService = new IfdFileHistoryService();
        ifdFileFeeNoticeService = new IfdFileFeeNoticeService();
        ifdFilePaymentService = new IfdFilePaymentService();
        ifdFileRequestCancelService = new IfdFileRequestCancelService();
        ifdHandlingReportService = new IfdHandlingReportService();

        try {

            ValidateXSDHelper validator = new ValidateXSDHelper();
            Envelope envelop = xmlToEnvelope(xml);

            type = getType(envelop);
            function = getFunction(envelop);

            fileCode = envelop.getHeader().getSubject().getReference();

            headerReceive = envelop.getHeader();
            content = envelop.getBody().getContent();
            documentType = headerReceive.getSubject().getDocumentType();

            header = envelopeService.createReceiveHeader(fileCode, documentType, type,
                    IfdConstants.Function.FUNC_SUCCESS);
            writeXmlToFile(evl, "receiveMS", envelop);
            // Validate xml
//            errorMsg = validator.validateWithStringXML(xml, IfdConstants.XSDPREFIX.IFD + type + function + ".xsd");
//            if (null == errorMsg) {
            fileExist = ifdFileService.findByFileCode(fileCode);
            boolean isSuccess = false;

//            Envelope isInvalid = validateFileExistStatus(fileExist, type, function, fileCode, header, json);
//            if (null == isInvalid) {
            // switch type
            switch (type) {
                case IfdConstants.ATTP_TYPE.TYPE_101:
                    if (IfdConstants.ATTP_FUNCTION.FUNCTION_01.equals(function)
                            || IfdConstants.ATTP_FUNCTION.FUNCTION_02.equals(function)
                            || IfdConstants.ATTP_FUNCTION.FUNCTION_03.equals(function)) {
                        IfdFileXml receivedFile = content.getIfdFile();
                        if (receivedFile != null) {
                            receivedFile.setFileCode(fileCode);
                            receivedFile.setDocumentType(documentType);
                            IfdFile ifdFile = null;

                            if (function.equals(IfdConstants.ATTP_FUNCTION.FUNCTION_01)) {
                                ifdFile = convertIfdFileBO(receivedFile);
                                if (ifdFile != null) {
                                    ifdFile = ifdFileService.createIfdFile(ifdFile);
                                }
                            } else if (function.equals(IfdConstants.ATTP_FUNCTION.FUNCTION_02)
                                    || function.equals(IfdConstants.ATTP_FUNCTION.FUNCTION_03)) {
                                //update hồ sơ
                            }

                            if (ifdFile != null) {
                                //lưu lịch sử xử lý hồ sơ.
                                IfdFileHistory fileHistory = createFileHistoryBO(ifdFile, fileCode, function, null);
                                fileHistory = ifdFileHistoryService.createIfdFileHistory(fileHistory);
                                if (fileHistory != null) {
                                    //tạo thông tin áp phí.
                                    IfdFileFeeNotice feeNotice = createFileFeeNoticeBO(ifdFile, documentType);
                                    feeNotice = ifdFileFeeNoticeService.createIfdFileFeeNotice(feeNotice);
                                    if (feeNotice != null) {
                                        isSuccess = true;
                                    }
                                }
                            }
                            envelopReturn = createEnvelopReturn(fileCode, IfdConstants.ATTP_TYPE.TYPE_101, header, isSuccess, IfdConstants.Error.BCA00_CODE, IfdConstants.Error.BCA00);

                        } else {
                            error = new com.viettel.module.importfood.ws.envelope.Error();
                            error.setErrorCode(IfdConstants.Error.ERR05_CODE);
                            error.setErrorName(IfdConstants.Error.ERR05);
                            envelopReturn = envelopeService.createEnvelopeError(fileCode, documentType, type, error);
                        }
                    }
                    break;

                case IfdConstants.ATTP_TYPE.TYPE_104:
                    if (IfdConstants.ATTP_FUNCTION.FUNCTION_09.equals(function)
                            || IfdConstants.ATTP_FUNCTION.FUNCTION_16.equals(function)) {
                        IfdFilePaymentXml receivedPayment = content.getIfdFilePaymentXml();
                        if (receivedPayment != null) {
                            receivedPayment.setFileCode(fileCode);
                            IfdFilePayment ifdFilePayment = convertIfdFilePaymentBO(receivedPayment);
                            if (ifdFilePayment != null) {
                                ifdFilePayment = ifdFilePaymentService.createIfdFilePayment(ifdFilePayment);
                            }

                            if (ifdFilePayment != null) {
                                //lưu lịch sử xử lý hồ sơ.
                                IfdFileHistory fileHistory = createFileHistoryBO(fileExist, fileCode, function, null);
                                fileHistory = ifdFileHistoryService.createIfdFileHistory(fileHistory);
                                if (fileHistory != null) {
                                    isSuccess = true;

                                }
                            }
                            envelopReturn = createEnvelopReturn(fileCode, IfdConstants.ATTP_TYPE.TYPE_104, header, isSuccess, IfdConstants.Error.BCA00_CODE, IfdConstants.Error.BCA00);

                        } else {
                            error = new com.viettel.module.importfood.ws.envelope.Error();
                            error.setErrorCode(IfdConstants.Error.ERR05_CODE);
                            error.setErrorName(IfdConstants.Error.ERR05);
                            envelopReturn = envelopeService.createEnvelopeError(fileCode, documentType, type, error);
                        }
                    }
                    break;

                case IfdConstants.ATTP_TYPE.TYPE_109:
                    if (IfdConstants.ATTP_FUNCTION.FUNCTION_14.equals(function)) {
                        IfdFileRequestCancelXml requestCancel = content.getIfdFileRequestCancelXml();
                        if (requestCancel != null) {
                            requestCancel.setFileCode(fileCode);
                            IfdFileRequestCancel ifdFileRequestCancel = convertIfdFileRequestCancelBO(requestCancel);
                            if (ifdFileRequestCancel != null) {
                                ifdFileRequestCancel = ifdFileRequestCancelService.createIfdFileRequestCancel(ifdFileRequestCancel);
                            }

                            if (ifdFileRequestCancel != null) {
                                //lưu lịch sử xử lý hồ sơ.
                                IfdFileHistory fileHistory = createFileHistoryBO(fileExist, fileCode, function, ifdFileRequestCancel.getReason());
                                fileHistory = ifdFileHistoryService.createIfdFileHistory(fileHistory);
                                if (fileHistory != null) {
                                    isSuccess = true;

                                }
                            }
                            envelopReturn = createEnvelopReturn(fileCode, IfdConstants.ATTP_TYPE.TYPE_104, header, isSuccess, IfdConstants.Error.BCA00_CODE, IfdConstants.Error.BCA00);

                        } else {
                            error = new com.viettel.module.importfood.ws.envelope.Error();
                            error.setErrorCode(IfdConstants.Error.ERR05_CODE);
                            error.setErrorName(IfdConstants.Error.ERR05);
                            envelopReturn = envelopeService.createEnvelopeError(fileCode, documentType, type, error);
                        }
                    }
                    break;
                    
                case IfdConstants.ATTP_TYPE.TYPE_107:
                    if (IfdConstants.ATTP_FUNCTION.FUNCTION_12.equals(function)) {
                        IfdHandlingReportXml handlingReport = content.getIfdHandlingReportXml();
                        if (handlingReport != null) {
                            handlingReport.setFileCode(fileCode);
                            IfdHandlingReport ifdHandlingReport = convertIfdHandlingReportBO(handlingReport);
                            if (ifdHandlingReport != null) {
                                ifdHandlingReport = ifdHandlingReportService.createIfdHandlingReport(ifdHandlingReport);
                            }

                            if (ifdHandlingReport != null) {
                                //lưu lịch sử xử lý hồ sơ.
                                IfdFileHistory fileHistory = createFileHistoryBO(fileExist, fileCode, function, ifdHandlingReport.getContent());
                                fileHistory = ifdFileHistoryService.createIfdFileHistory(fileHistory);
                                if (fileHistory != null) {
                                    isSuccess = true;

                                }
                            }
                            envelopReturn = createEnvelopReturn(fileCode, IfdConstants.ATTP_TYPE.TYPE_104, header, isSuccess, IfdConstants.Error.BCA00_CODE, IfdConstants.Error.BCA00);

                        } else {
                            error = new com.viettel.module.importfood.ws.envelope.Error();
                            error.setErrorCode(IfdConstants.Error.ERR05_CODE);
                            error.setErrorName(IfdConstants.Error.ERR05);
                            envelopReturn = envelopeService.createEnvelopeError(fileCode, documentType, type, error);
                        }
                    }
                    break;

                default:
                    error = new com.viettel.module.importfood.ws.envelope.Error();
                    error.setErrorCode(IfdConstants.Error.ERR04_CODE);
                    error.setErrorName(IfdConstants.Error.ERR04);
                    envelopReturn = envelopeService.createEnvelopeError(fileCode, documentType, type, error);
                    break;
            }
//            } else {
//                envelopReturn = isInvalid;
//            }

//            } else {
//                error = new Error();
//                error.setErrorCode(IfdConstants.Error.ERR02_CODE);
//                error.setErrorName(errorMsg);
//                envelopReturn = envelopeService.createEnvelopeError(fileCode, documentType, type, error);
//            }
        } catch (Exception ex) {
            LogUtil.addLog(ex);
            error = new com.viettel.module.importfood.ws.envelope.Error();
            error.setErrorCode(IfdConstants.Error.ERR02_CODE);
            error.setErrorName(IfdConstants.Error.ERR02);
            envelopReturn = envelopeService.createEnvelopeError(fileCode, documentType, type, error);

        }

        String evS = ObjectToXml(envelopReturn);
        writeXmlToFile(evS, "response", envelopReturn);

        return evS;
    }

    private String getFunction(Envelope envelop) {
        return envelop.getHeader().getSubject().getFunction();
    }

    private String getType(Envelope envelop) {
        return envelop.getHeader().getSubject().getType();
    }

    private Envelope createEnvelopReturn(String fileCode, String msgType, Header header, boolean isSuccess, String errorCode, String errorName) {
        envelopeService = new EnvelopeHelper();
        Envelope envelop;
        if (isSuccess) {
            Content content = new Content();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date today = Calendar.getInstance().getTime();
            Success success = new Success();
            success.setReceiveDate(formatter.format(today));
            content.setSuccess(success);
            Body body = envelopeService.createBody(content);
            envelop = envelopeService.createResponse(header, body);
        } else {
            com.viettel.module.importfood.ws.envelope.Error error = new com.viettel.module.importfood.ws.envelope.Error();
            error.setErrorCode(errorCode);
            error.setErrorName(errorName);
            envelop = envelopeService.createEnvelopeError(fileCode, header.getSubject().getDocumentType(), msgType, error);
        }
        return envelop;
    }

    private Long getUserId() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getUserId();
    }

    /**
     * convert Object Xml tò Object BO : IfdFile
     *
     * @param receivedFile
     * @return
     */
    private IfdFile convertIfdFileBO(IfdFileXml receivedFile) {
        IfdFile result = null;
        List<IfdFileProductXml> lstProduct = receivedFile.getLstProduct();
        if (lstProduct != null && !lstProduct.isEmpty()) {
            for (IfdFileProductXml ifdFileProductXml : lstProduct) {
                IfdAttachmentXml attach = new IfdAttachmentXml();
                attach.setAttachmentCode(ifdFileProductXml.getProductAttachmentCode());
                attach.setAttachTypeCode(ifdFileProductXml.getProductAttachTypeCode());
                attach.setAttachTypeName(ifdFileProductXml.getProductAttachTypeName());
                attach.setAttachmentName(ifdFileProductXml.getProductAttachmentName());
                ifdFileProductXml.setAttach(attach);

                ifdFileProductXml.setProductAttachmentCode(null);
                ifdFileProductXml.setProductAttachTypeCode(null);
                ifdFileProductXml.setProductAttachTypeName(null);
                ifdFileProductXml.setProductAttachmentName(null);
            }
        }
        //save IfdFile
        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(receivedFile), IfdFile.class);
        return result;
    }

    /**
     * convert Object Xml to Object BO : IfdFilePayment
     *
     * @param receivedPayment
     * @return
     */
    private IfdFilePayment convertIfdFilePaymentBO(IfdFilePaymentXml receivedPayment) {
        IfdFilePayment result = null;

        IfdAttachmentXml attach = new IfdAttachmentXml();
        attach.setAttachmentCode(receivedPayment.getAttachmentCode());
        attach.setAttachTypeCode(receivedPayment.getAttachTypeCode());
        attach.setAttachTypeName(receivedPayment.getAttachTypeName());
        attach.setAttachmentName(receivedPayment.getAttachmentName());
        receivedPayment.setAttach(attach);

        receivedPayment.setAttachmentCode(null);
        receivedPayment.setAttachTypeCode(null);
        receivedPayment.setAttachTypeName(null);
        receivedPayment.setAttachmentName(null);

        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(receivedPayment), IfdFilePayment.class);
        return result;
    }

    /**
     * convert Object Xml to Object BO : IfdFileRequestCancel
     *
     * @param requestCancel
     * @return
     */
    private IfdFileRequestCancel convertIfdFileRequestCancelBO(IfdFileRequestCancelXml requestCancel) {
        IfdFileRequestCancel result = null;

        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(requestCancel), IfdFileRequestCancel.class);
        return result;
    }
    
    /**
     * convert Object Xml to Object BO : IfdHandlingReport
     * @param handlingReport
     * @return 
     */
    private IfdHandlingReport convertIfdHandlingReportBO(IfdHandlingReportXml handlingReport) {
        IfdHandlingReport result = null;

        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(handlingReport), IfdHandlingReport.class);
        return result;
    }

    /**
     * Tạo lịch sử
     *
     * @param file
     * @param fileCode
     * @param function
     * @return
     */
    private IfdFileHistory createFileHistoryBO(IfdFile file, String fileCode, String function, String content) {
        IfdFileHistory obj = new IfdFileHistory();
        obj.setSenderDepartment(IfdConstants.System.NSW_NAME);
        obj.setReceiverDepartment(IfdConstants.System.ATTP_NAME);
        obj.setFileCode(fileCode);
        obj.setSenderName(file.getFileCreateBy());

        switch (function) {
            case IfdConstants.ATTP_FUNCTION.FUNCTION_01:
                obj.setFileStatus(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatus());
                obj.setFileStatusName(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatusName());
                if (content != null && !"".equals(content)) {
                    obj.setContent(content);
                } else {
                    obj.setContent(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatusName());
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_02:
                obj.setFileStatus(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatus());
                obj.setFileStatusName(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatusName());
                if (content != null && !"".equals(content)) {
                    obj.setContent(content);
                } else {
                    obj.setContent(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatusName());
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_03:
                obj.setFileStatus(FileStatus.HO_SO_GUI_SUA_DOI_BO_SUNG.getStatus());
                obj.setFileStatusName(FileStatus.HO_SO_GUI_SUA_DOI_BO_SUNG.getStatusName());
                if (content != null && !"".equals(content)) {
                    obj.setContent(content);
                } else {
                    obj.setContent(FileStatus.HO_SO_GUI_SUA_DOI_BO_SUNG.getStatusName());
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_09:
                if (file.getFileStatus().equals(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatusName());
                    }
                } else if (file.getFileStatus().equals(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatusName());
                    }
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_16:
                if (file.getFileStatus().equals(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatusName());
                    }
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_08:
                if (file.getFileStatus().equals(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatus())
                        || file.getFileStatus().equals(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatus());
                    obj.setFileStatusName(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatusName());
                    }
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_14:
                if (file.getFileStatus().equals(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatus())
                        || file.getFileStatus().equals(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatus())
                        || file.getFileStatus().equals(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatus())
                        || file.getFileStatus().equals(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus())
                        || file.getFileStatus().equals(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus())
                        || file.getFileStatus().equals(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_DA_RUT.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_DA_RUT.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_DA_RUT.getStatusName());
                    }
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_12:
                obj.setFileStatus(FileStatus.HO_SO_DA_GUI_BAO_CAO_XU_LY.getStatus());
                obj.setFileStatusName(FileStatus.HO_SO_DA_GUI_BAO_CAO_XU_LY.getStatusName());
                if (content != null && !"".equals(content)) {
                    obj.setContent(content);
                } else {
                    obj.setContent(FileStatus.HO_SO_DA_GUI_BAO_CAO_XU_LY.getStatusName());
                }
                break;
            default:
                break;
        }

        return obj;
    }

    /**
     * Tạo thông báo áp phi
     *
     * @param file
     * @param documentType
     * @return
     */
    private IfdFileFeeNotice createFileFeeNoticeBO(IfdFile file, String documentType) {
        IfdFileFeeNotice result = new IfdFileFeeNotice();
        result.setFileId(file.getFileId());
        result.setFileCode(file.getFileCode());
        result.setDeptCode(file.getCheckDeptCode());
        result.setDeptName(file.getCheckDeptName());

        IfdDeptDAO ifdDeptDAO = new IfdDeptDAO();
        IfdDept ifdDept = ifdDeptDAO.findByDeptcode(file.getCheckDeptCode());
        if (ifdDept != null) {
            result.setAccountNumber(ifdDept.getAccountNumber());
            result.setBank(ifdDept.getBank());
        }

        List<IfdFileProduct> lstProduct = file.getLstProduct();
        if (lstProduct != null && !lstProduct.isEmpty()) {
            Long totalOfFee = 0L;
            if (documentType.equals(IfdConstants.MohProceduce.THONG_THUONG)) {
                totalOfFee = IfdConstants.MohFeeDefaut.THONG_THUONG + IfdConstants.MohFeeDefaut.DEFAULT * (lstProduct.size() - 1);
                result.setFeeDefaut(IfdConstants.MohFeeDefaut.THONG_THUONG);
            } else if (documentType.equals(IfdConstants.MohProceduce.CHAT)) {
                totalOfFee = IfdConstants.MohFeeDefaut.CHAT + IfdConstants.MohFeeDefaut.DEFAULT * (lstProduct.size() - 1);
                result.setFeeDefaut(IfdConstants.MohFeeDefaut.CHAT);
            }
            result.setNumberOfProduct(Long.parseLong(lstProduct.size() + ""));
            result.setTotalOfFee(totalOfFee);
        }

        return result;
    }

    private void writeXmlToFile(String evlrs, String sendOrReceive, Envelope envelope) {

        try {
            if (!"true".equals(ResourceBundleUtil.getString("export_service_message_to_file", "config"))) {
                return;
            }
            XmlMessage xmlMessage = new XmlMessage();
            xmlMessage.setCreateDate(new Date());
            if (envelope != null && envelope.getHeader() != null) {
                String type = envelope.getHeader().getSubject().getType();
                String function = envelope.getHeader().getSubject().getFunction();
                String nswFileCode = envelope.getHeader().getSubject().getReference();
                xmlMessage.setType(type);
                xmlMessage.setFunc(function);
                xmlMessage.setNswFileCode(nswFileCode);
                if (envelope.getHeader().getReference() != null) {
                    String msId = envelope.getHeader().getReference().getMessageId();
                    xmlMessage.setMsid(msId);
                }
            }

            if (evlrs != null) {
                if (evlrs.length() < 20000) {
                    xmlMessage.setMessage(sendOrReceive + evlrs);
                } else {
                    xmlMessage.setMessage(sendOrReceive + evlrs.substring(0, 20000));
                }
            }
            XmlMessageDAOHE xmlDAOHE = new XmlMessageDAOHE();
            xmlDAOHE.saveOrUpdate(xmlMessage);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public String FormatnumberDouble(Double s) {
        if (s == null) {
            return ""; //Khong hien thi
        }
        DecimalFormat df = new DecimalFormat("#######################.#");
        return df.format(s);
    }

    public void commit() {
        FilesDAOHE fhe = new FilesDAOHE();
        fhe.commit();
    }

}
