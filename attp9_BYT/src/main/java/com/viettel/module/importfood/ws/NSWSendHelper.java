/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws;

import com.google.gson.Gson;
import com.viettel.core.sys.BO.XmlMessage;
import com.viettel.core.user.model.UserToken;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.DAO.FilesDAOHE;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.util.Date;
import java.text.DecimalFormat;
import java.util.*;
import org.zkoss.zk.ui.Sessions;
import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.ws.envelope.Envelope;
import com.viettel.utils.LogUtils;
import com.viettel.module.importfood.ws.envelope.*;
import com.viettel.core.sys.DAO.*;
import com.viettel.module.importfood.BO.IfdCertificate;
import com.viettel.module.importfood.BO.IfdDept;
import com.viettel.module.importfood.BO.IfdFileFeeNotice;
import com.viettel.module.importfood.BO.IfdFileHistory;
import com.viettel.module.importfood.BO.IfdFilePayment;
import com.viettel.module.importfood.BO.IfdFileProduct;
import com.viettel.module.importfood.BO.IfdFileResult;
import com.viettel.module.importfood.Constant.FileStatus;
import com.viettel.module.importfood.DAO.IfdDeptDAO;
import com.viettel.module.importfood.Service.IfdCertificateService;
import com.viettel.module.importfood.Service.IfdFileFeeNoticeService;
import com.viettel.module.importfood.Service.IfdFileHistoryService;
import com.viettel.module.importfood.Service.IfdFileResultService;
import com.viettel.module.importfood.Service.IfdFileService;
import com.viettel.module.importfood.uitls.LogUtil;
import com.viettel.module.importfood.ws.entity.IfdAttachmentXml;
import com.viettel.module.importfood.ws.entity.IfdCertificateXml;
import com.viettel.module.importfood.ws.entity.IfdFileFeeNoticeXml;
import com.viettel.module.importfood.ws.entity.IfdFilePaymentXml;
import com.viettel.module.importfood.ws.entity.IfdFileProductXml;
import com.viettel.module.importfood.ws.entity.IfdFileResultXml;
import com.viettel.module.importfood.ws.entity.IfdFileXml;
import com.viettel.module.importfood.ws.message.IfdSendMessage;
import com.viettel.module.importfood.ws.services.EnvelopeHelper;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;
import com.viettel.module.importfood.ws.envelope.Error;

/**
 *
 * @author E5420
 */
public class NSWSendHelper {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NSWSendHelper.class);
    private final SimpleDateFormat formatterYear = new SimpleDateFormat("yyyy");
    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private static EnvelopeHelper envelopeService;

    private static IfdFileService ifdFileService;

    private static IfdFileHistoryService ifdFileHistoryService;

    private static IfdFileFeeNoticeService ifdFileFeeNoticeService;

    private static IfdFileResultService ifdFileResultService;
    
    private static IfdCertificateService ifdCertificateService;

    //Convert Object <---> XML 
    public String ObjectToXml(Object obj) {

        String result = "";
        java.io.StringWriter sw = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(obj, sw);
            result = sw.toString();
        } catch (JAXBException ex) {
            LogUtils.addLogDB(ex);
        }
        return result.trim();
    }

    public Envelope xmlToEnvelope(String xml) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Envelope.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            StringReader reader = new StringReader(xml);
            return (Envelope) unmarshaller.unmarshal(reader);
        } catch (JAXBException ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    //Gửi ms đến NSW
    public boolean sendMessage(IfdSendMessage sendMessage) {

        Gson gson = new Gson();
        envelopeService = new EnvelopeHelper();
        ifdFileService = new IfdFileService();
        ifdFileHistoryService = new IfdFileHistoryService();
        ifdFileFeeNoticeService = new IfdFileFeeNoticeService();
        ifdFileResultService = new IfdFileResultService();
        ifdCertificateService = new IfdCertificateService();

        String errorMessage = "";
        boolean isSuccess = false;

        Envelope envelopeSend = null;
        Header header = null;
        Body body = null;
        Content content = new Content();

        try {
            Long fileId = sendMessage.getFileId();

            IfdFile sendFile = ifdFileService.getIfdFile(fileId);

            if (sendFile != null && sendFile.getFileCode() != null && !"".equals(sendFile.getFileCode())) {
                header = envelopeService.createSendHeader(sendFile.getFileCode(), sendFile.getDocumentType(),
                        sendMessage.getType(), sendMessage.getFunction());
                switch (sendMessage.getType()) {
                    case IfdConstants.ATTP_TYPE.TYPE_110:
                        if (IfdConstants.ATTP_FUNCTION.FUNCTION_08.equals(sendMessage.getFunction())) {
                            IfdFileFeeNotice feeNotice = ifdFileFeeNoticeService.findLatestByFileId(fileId);
                            if (feeNotice != null) {
                                IfdFileFeeNoticeXml fileFeeNoticeXml = gson.fromJson(gson.toJson(feeNotice), IfdFileFeeNoticeXml.class);
                                if (fileFeeNoticeXml != null) {
                                    content.setIfdFileFeeNoticeXml(fileFeeNoticeXml);
                                    body = envelopeService.createBody(content);
                                    envelopeSend = envelopeService.createResponse(header, body);

                                    isSuccess = send(envelopeSend, sendMessage.getSignedXml(), sendFile.getFileCode(), sendFile.getDocumentType(),
                                            sendMessage.getType(), sendMessage.getFunction());
                                    if (isSuccess) {
                                        //lưu lịch sử xử lý hồ sơ.
                                        IfdFileHistory fileHistory = createFileHistoryBO(sendFile, sendFile.getFileCode(), sendMessage.getFunction(), null);
                                        fileHistory = ifdFileHistoryService.createIfdFileHistory(fileHistory);
                                        if (fileHistory != null) {
                                            isSuccess = true;
                                        }
                                    }
                                }
                            }

                        }
                        break;

                    case IfdConstants.ATTP_TYPE.TYPE_102:
                        if (IfdConstants.ATTP_FUNCTION.FUNCTION_04.equals(sendMessage.getFunction())
                                || IfdConstants.ATTP_FUNCTION.FUNCTION_05.equals(sendMessage.getFunction())
                                || IfdConstants.ATTP_FUNCTION.FUNCTION_06.equals(sendMessage.getFunction())
                                || IfdConstants.ATTP_FUNCTION.FUNCTION_15.equals(sendMessage.getFunction())) {
                            IfdFileResult fileResult = null;
                            if (IfdConstants.ATTP_FUNCTION.FUNCTION_04.equals(sendMessage.getFunction())) {
                                fileResult = ifdFileResultService.findLatestByFileIdAndFileStatus(fileId, FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus());
                            } else if (IfdConstants.ATTP_FUNCTION.FUNCTION_05.equals(sendMessage.getFunction())) {
                                fileResult = ifdFileResultService.findLatestByFileIdAndFileStatus(fileId, FileStatus.HO_SO_YEU_CAU_SUA_DOI_BO_SUNG.getStatus());
                            } else if (IfdConstants.ATTP_FUNCTION.FUNCTION_06.equals(sendMessage.getFunction())) {
                                fileResult = ifdFileResultService.findLatestByFileIdAndFileStatus(fileId, FileStatus.HO_SO_CHO_PHAN_CONG.getStatus());
                            } else if (IfdConstants.ATTP_FUNCTION.FUNCTION_15.equals(sendMessage.getFunction())) {
                                fileResult = ifdFileResultService.findLatestByFileIdAndFileStatus(fileId, FileStatus.HO_SO_TU_CHOI_CAP_PHEP.getStatus());
                            }

                            if (fileResult != null) {
                                IfdFileResultXml fileFileResultXml = gson.fromJson(gson.toJson(fileResult), IfdFileResultXml.class);
                                if (fileFileResultXml != null) {
                                    content.setIfdFileResultXml(fileFileResultXml);
                                    body = envelopeService.createBody(content);
                                    envelopeSend = envelopeService.createResponse(header, body);

                                    isSuccess = send(envelopeSend, sendMessage.getSignedXml(), sendFile.getFileCode(), sendFile.getDocumentType(),
                                            sendMessage.getType(), sendMessage.getFunction());
                                    if (isSuccess) {
                                        //lưu lịch sử xử lý hồ sơ.
                                        IfdFileHistory fileHistory = createFileHistoryBO(sendFile, sendFile.getFileCode(), sendMessage.getFunction(), fileResult.getProcessContent());
                                        fileHistory = ifdFileHistoryService.createIfdFileHistory(fileHistory);
                                        if (fileHistory != null) {
                                            isSuccess = true;
                                        }
                                    }
                                }
                            }

                        }
                        break;

                    case IfdConstants.ATTP_TYPE.TYPE_103:
                        if (IfdConstants.ATTP_FUNCTION.FUNCTION_07.equals(sendMessage.getFunction())) {
                            IfdCertificate certificate = ifdCertificateService.getIfdCertificate(fileId);
                            if (certificate != null) {
                                IfdCertificateXml certificateXml = gson.fromJson(gson.toJson(certificate), IfdCertificateXml.class);
                                if (certificateXml != null) {
                                    content.setIfdCertificateXml(certificateXml);
                                    body = envelopeService.createBody(content);
                                    envelopeSend = envelopeService.createResponse(header, body);

                                    isSuccess = send(envelopeSend, sendMessage.getSignedXml(), sendFile.getFileCode(), sendFile.getDocumentType(),
                                            sendMessage.getType(), sendMessage.getFunction());
                                    if (isSuccess) {
                                        //lưu lịch sử xử lý hồ sơ.
                                        IfdFileHistory fileHistory = createFileHistoryBO(sendFile, sendFile.getFileCode(), sendMessage.getFunction(), null);
                                        fileHistory = ifdFileHistoryService.createIfdFileHistory(fileHistory);
                                        if (fileHistory != null) {
                                            isSuccess = true;
                                        }
                                    }
                                }
                            }

                        }
                        break;

                    default:
                        break;

                }
            }

        } catch (Exception ex) {
            LogUtil.addLog(ex);
        }

        return isSuccess;
    }

    private boolean send(Envelope envelopeSend, String signedXml, String fileCode, String documentType, String msgType, String msgFunc) {
        Boolean isSuccess = false;
        String xml;

        String debugMode = ResourceBundleUtil.getString("DEBUG_MODE");
        if ("true".equals(debugMode)) {
            xml = ObjectToXml(envelopeSend);
            writeFileMsg(xml, msgType, msgFunc, fileCode, false);
            isSuccess = true;
//            return isSuccess;
        }

        if (signedXml == null || ("".equals(signedXml))) {
            xml = ObjectToXml(envelopeSend);
            writeFileMsg(xml, msgType, msgFunc, fileCode, false);
        } else {
            xml = signedXml;
        }

        writeXmlToFile(xml, "sendMs", envelopeSend);

        String responseStr = "";

        Envelope envl;
        Error error;

        String officeCode = IfdConstants.BYT;

        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            String url = ResourceBundleUtil.getString("GATEWAY_LINK");
            String nameSpace = ResourceBundleUtil.getString("GATEWAY_NAMESPACE");
            String nameSpaceKey = ResourceBundleUtil.getString("GATEWAY_NAMESPACE_KEY");
            String methodTag = ResourceBundleUtil.getString("GATEWAY_MOTHOD_TAG");
            String requestOfficeCode = ResourceBundleUtil.getString("GATEWAY_PAYLOAD_TAG_OFFICECODE");
            String requestDocumentType = ResourceBundleUtil.getString("GATEWAY_PAYLOAD_TAG_DOCUMENTTYPE");
            String requestPayload = ResourceBundleUtil.getString("GATEWAY_PAYLOAD_TAG_DATA");

            SOAPMessage soapMessage = SoapHelper.createSOAPRequest(xml, officeCode, documentType, nameSpace,
                    nameSpaceKey, methodTag, requestOfficeCode, requestDocumentType, requestPayload);

            SOAPMessage soapResponse = soapConnection.call(soapMessage, url);
            if ("true".equals(debugMode)) {
//                xml = convertXmlService.ObjectToXml(envelopeSend);
//                writeFileMsg(xml, msgType, msgFunc, fileCode, false);
                isSuccess = true;
                return isSuccess;
            }
            responseStr = SoapHelper.getSOAPResponse(soapResponse);
            soapConnection.close();
        } catch (Exception ex) {
            LogUtil.addLog(ex);

            if ("true".equals(debugMode)) {
                xml = ObjectToXml(envelopeSend);
                writeFileMsg(xml, msgType, msgFunc, fileCode, false);
                isSuccess = true;
                return isSuccess;
            }

            error = envelopeService.createError(IfdConstants.Error.ERR01_CODE, IfdConstants.Error.ERR01);
            envl = envelopeService.createEnvelopeError(fileCode, documentType, msgType, error);
            responseStr = envl.toString();
        }

        //log response msg
        writeFileMsg(responseStr, msgType, msgFunc, fileCode, true);

        Envelope envelopeReturn = xmlToEnvelope(responseStr);
        //save db response message
        writeXmlToFile(responseStr, "response", envelopeReturn);

        if (checkTypeReturn(envelopeReturn)) {
            isSuccess = true;
        } else {
            List<Error> errors = envelopeReturn.getBody().getContent().getErrorList();
            envelopeSend.getBody().getContent().setErrorList(errors);
        }

        //phục vụ test nghiệp vụ
        if ("true".equals(debugMode)) {
            isSuccess = true;
        }
        return isSuccess;
    }

    private void writeFileMsg(String xml, String msgType, String msgFunc, String fiMaHoSo, boolean isResponse) {
        String dirName = ResourceBundleUtil.getString("debug_folder_msg_output");
        File theDir = new File(dirName);

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            try {
                theDir.mkdir();
            } catch (SecurityException e) {
                LogUtils.addLogDB(e);
            }
        }

        BufferedWriter writer = null;
        try {
            String fileName;
            if (isResponse == false) {
                fileName = dirName + fiMaHoSo + "_" + msgType + "_" + msgFunc + ".xml";
            } else {
                fileName = dirName + fiMaHoSo + "_" + msgType + "_" + msgFunc + "_response.xml";
            }

            writer = new BufferedWriter(new FileWriter(fileName));
//            logger.info("fileName: " + fileName + "\nxml: " + xml);
            writer.write(xml);
        } catch (IOException e) {
            LogUtils.addLogDB(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    LogUtils.addLogDB(e);
                }
            }
        }
    }

    private boolean checkTypeReturn(Envelope envelopeReturn) {
        return envelopeReturn != null && envelopeReturn.getHeader() != null
                && envelopeReturn.getHeader().getSubject() != null && IfdConstants.ATTP_FUNCTION.FUNCTION_99
                .equals(envelopeReturn.getHeader().getSubject().getFunction());
    }

    private String getFunction(Envelope envelop) {
        return envelop.getHeader().getSubject().getFunction();
    }

    private String getType(Envelope envelop) {
        return envelop.getHeader().getSubject().getType();
    }

    private Envelope createEnvelopReturn(String fileCode, String msgType, Header header, boolean isSuccess, String errorCode, String errorName) {
        envelopeService = new EnvelopeHelper();
        Envelope envelop;
        if (isSuccess) {
            Content content = new Content();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date today = Calendar.getInstance().getTime();
            Success success = new Success();
            success.setReceiveDate(formatter.format(today));
            content.setSuccess(success);
            Body body = envelopeService.createBody(content);
            envelop = envelopeService.createResponse(header, body);
        } else {
            com.viettel.module.importfood.ws.envelope.Error error = new com.viettel.module.importfood.ws.envelope.Error();
            error.setErrorCode(errorCode);
            error.setErrorName(errorName);
            envelop = envelopeService.createEnvelopeError(fileCode, header.getSubject().getDocumentType(), msgType, error);
        }
        return envelop;
    }

    private Long getUserId() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getUserId();
    }

    /**
     * convert Object Xml tò Object BO : IfdFile
     *
     * @param receivedFile
     * @return
     */
    private IfdFile convertIfdFileBO(IfdFileXml receivedFile) {
        IfdFile result = null;
        List<IfdFileProductXml> lstProduct = receivedFile.getLstProduct();
        if (lstProduct != null && !lstProduct.isEmpty()) {
            for (IfdFileProductXml ifdFileProductXml : lstProduct) {
                IfdAttachmentXml attach = new IfdAttachmentXml();
                attach.setAttachmentCode(ifdFileProductXml.getProductAttachmentCode());
                attach.setAttachTypeCode(ifdFileProductXml.getProductAttachTypeCode());
                attach.setAttachTypeName(ifdFileProductXml.getProductAttachTypeName());
                attach.setAttachmentName(ifdFileProductXml.getProductAttachmentName());
                ifdFileProductXml.setAttach(attach);

                ifdFileProductXml.setProductAttachmentCode(null);
                ifdFileProductXml.setProductAttachTypeCode(null);
                ifdFileProductXml.setProductAttachTypeName(null);
                ifdFileProductXml.setProductAttachmentName(null);
            }
        }
        //save IfdFile
        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(receivedFile), IfdFile.class);
        return result;
    }

    private IfdFilePayment convertIfdFilePaymentBO(IfdFilePaymentXml receivedPayment) {
        IfdFilePayment result = null;

        IfdAttachmentXml attach = new IfdAttachmentXml();
        attach.setAttachmentCode(receivedPayment.getAttachmentCode());
        attach.setAttachTypeCode(receivedPayment.getAttachTypeCode());
        attach.setAttachTypeName(receivedPayment.getAttachTypeName());
        attach.setAttachmentName(receivedPayment.getAttachmentName());
        receivedPayment.setAttach(attach);

        receivedPayment.setAttachmentCode(null);
        receivedPayment.setAttachTypeCode(null);
        receivedPayment.setAttachTypeName(null);
        receivedPayment.setAttachmentName(null);

        Gson gson = new Gson();
        result = gson.fromJson(gson.toJson(receivedPayment), IfdFilePayment.class);
        return result;
    }

    /**
     * Tạo lịch sử
     *
     * @param file
     * @param fileCode
     * @param function
     * @return
     */
    private IfdFileHistory createFileHistoryBO(IfdFile file, String fileCode, String function, String content) {
        IfdFileHistory obj = new IfdFileHistory();
        obj.setSenderDepartment(IfdConstants.System.NSW_NAME);
        obj.setReceiverDepartment(IfdConstants.System.ATTP_NAME);
        obj.setFileCode(fileCode);
        obj.setSenderName(file.getFileCreateBy());

        switch (function) {
            case IfdConstants.ATTP_FUNCTION.FUNCTION_01:
                obj.setFileStatus(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatus());
                obj.setFileStatusName(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatusName());
                if (content != null && !"".equals(content)) {
                    obj.setContent(content);
                } else {
                    obj.setContent(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatusName());
                }

                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_02:
                obj.setFileStatus(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatus());
                obj.setFileStatusName(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatusName());
                if (content != null && !"".equals(content)) {
                    obj.setContent(content);
                } else {
                    obj.setContent(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatusName());
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_03:
                obj.setFileStatus(FileStatus.HO_SO_GUI_SUA_DOI_BO_SUNG.getStatus());
                obj.setFileStatusName(FileStatus.HO_SO_GUI_SUA_DOI_BO_SUNG.getStatusName());
                if (content != null && !"".equals(content)) {
                    obj.setContent(content);
                } else {
                    obj.setContent(FileStatus.HO_SO_GUI_SUA_DOI_BO_SUNG.getStatusName());
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_09:
                if (file.getFileStatus().equals(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatusName());
                    }

                } else if (file.getFileStatus().equals(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatusName());
                    }

                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_16:
                if (file.getFileStatus().equals(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatusName());
                    }
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_08:
                obj.setFileStatus(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatus());
                obj.setFileStatusName(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatusName());
                if (content != null && !"".equals(content)) {
                    obj.setContent(content);
                } else {
                    obj.setContent(FileStatus.DA_GUI_THONG_BAO_AP_PHI.getStatusName());
                }

                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_04:
                if (file.getFileStatus().equals(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatus())
                        || file.getFileStatus().equals(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatus());
                    obj.setFileStatusName(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.YEU_CAU_NOP_LAI_PHI.getStatusName());
                    }

                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_06:
                if (file.getFileStatus().equals(FileStatus.HO_SO_CHO_XAC_NHAN_PHI.getStatus())
                        || file.getFileStatus().equals(FileStatus.HO_SO_GUI_BO_SUNG_PHI.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_CHO_PHAN_CONG.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_CHO_PHAN_CONG.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_CHO_PHAN_CONG.getStatusName());
                    }
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_05:
                if (file.getFileStatus().equals(FileStatus.HO_SO_CHO_PHE_DUYET.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_YEU_CAU_SUA_DOI_BO_SUNG.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_YEU_CAU_SUA_DOI_BO_SUNG.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_YEU_CAU_SUA_DOI_BO_SUNG.getStatusName());
                    }
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_15:
                if (file.getFileStatus().equals(FileStatus.HO_SO_CHO_PHE_DUYET.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_TU_CHOI_CAP_PHEP.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_TU_CHOI_CAP_PHEP.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_TU_CHOI_CAP_PHEP.getStatusName());
                    }
                }
                break;
            case IfdConstants.ATTP_FUNCTION.FUNCTION_07:
                if (file.getFileStatus().equals(FileStatus.HO_SO_CHO_PHE_DUYET.getStatus())) {
                    obj.setFileStatus(FileStatus.HO_SO_DA_TRA_KET_QUA.getStatus());
                    obj.setFileStatusName(FileStatus.HO_SO_DA_TRA_KET_QUA.getStatusName());
                    if (content != null && !"".equals(content)) {
                        obj.setContent(content);
                    } else {
                        obj.setContent(FileStatus.HO_SO_DA_TRA_KET_QUA.getStatusName());
                    }
                }
                break;

            default:
                break;
        }

        return obj;
    }

    private void writeXmlToFile(String evlrs, String sendOrReceive, Envelope envelope) {

        try {
            if (!"true".equals(ResourceBundleUtil.getString("export_service_message_to_file", "config"))) {
                return;
            }
            XmlMessage xmlMessage = new XmlMessage();
            xmlMessage.setCreateDate(new Date());
            if (envelope != null && envelope.getHeader() != null) {
                String type = envelope.getHeader().getSubject().getType();
                String function = envelope.getHeader().getSubject().getFunction();
                String nswFileCode = envelope.getHeader().getSubject().getReference();
                xmlMessage.setType(type);
                xmlMessage.setFunc(function);
                xmlMessage.setNswFileCode(nswFileCode);
                if (envelope.getHeader().getReference() != null) {
                    String msId = envelope.getHeader().getReference().getMessageId();
                    xmlMessage.setMsid(msId);
                }
            }

            if (evlrs != null) {
                if (evlrs.length() < 20000) {
                    xmlMessage.setMessage(sendOrReceive + evlrs);
                } else {
                    xmlMessage.setMessage(sendOrReceive + evlrs.substring(0, 20000));
                }
            }
            XmlMessageDAOHE xmlDAOHE = new XmlMessageDAOHE();
            xmlDAOHE.saveOrUpdate(xmlMessage);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public String FormatnumberDouble(Double s) {
        if (s == null) {
            return ""; //Khong hien thi
        }
        DecimalFormat df = new DecimalFormat("#######################.#");
        return df.format(s);
    }

    public void commit() {
        FilesDAOHE fhe = new FilesDAOHE();
        fhe.commit();
    }

}
