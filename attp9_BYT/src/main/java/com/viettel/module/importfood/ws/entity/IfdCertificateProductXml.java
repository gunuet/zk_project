/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.entity;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IfdCertificateProductXml implements Serializable {

    @XmlTransient
    private static final long serialVersionUID = 1L;
    @XmlTransient
    private Long certificateProductId;
    @XmlTransient
    private Long certificateId;
    @XmlElement(name = "ProductId")
    private String productCode;
    @XmlElement(name = "ProductName")
    private String productName;
    @XmlElement(name = "ProductGroupCode")
    private String productGroupCode;
    @XmlElement(name = "ProductGroupName")
    private String productGroupName;
    @XmlElement(name = "Manufacturer")
    private String manufacturer;
    @XmlElement(name = "ManufacturerAddress")
    private String manufacturerAddress;
    @XmlElement(name = "ProductCheckMethod")
    private Long productCheckMethod;
    @XmlElement(name = "CheckMethodConfirmNo")
    private String checkMethodConfirmNo;
    @XmlElement(name = "ConfirmAnnounceNo")
    private String confirmAnnounceNo;
    @XmlElement(name = "Pass")
    private Long pass;
    @XmlElement(name = "Reason")
    private String reason;
    @XmlElement(name = "HandlingMeasuresCode")
    private Long handlingMeasuresCode;
    @XmlElement(name = "HandlingMeasuresName")
    private String handlingMeasuresName;
    @XmlElement(name = "Note")
    private String note;
    @XmlTransient
    private Long status;
    @XmlTransient
    private Date createDate;
    @XmlTransient
    private Long createBy;
    @XmlTransient
    private Date updateDate;
    @XmlTransient
    private Long updateBy;

    public IfdCertificateProductXml() {
    }

    public IfdCertificateProductXml(Long certificateProductId) {
        this.certificateProductId = certificateProductId;
    }

    public IfdCertificateProductXml(Long certificateProductId, Long status, Date createDate, Date updateDate) {
        this.certificateProductId = certificateProductId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getCertificateProductId() {
        return certificateProductId;
    }

    public void setCertificateProductId(Long certificateProductId) {
        this.certificateProductId = certificateProductId;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductGroupCode() {
        return productGroupCode;
    }

    public void setProductGroupCode(String productGroupCode) {
        this.productGroupCode = productGroupCode;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturerAddress() {
        return manufacturerAddress;
    }

    public void setManufacturerAddress(String manufacturerAddress) {
        this.manufacturerAddress = manufacturerAddress;
    }

    public Long getProductCheckMethod() {
        return productCheckMethod;
    }

    public void setProductCheckMethod(Long productCheckMethod) {
        this.productCheckMethod = productCheckMethod;
    }

    public String getCheckMethodConfirmNo() {
        return checkMethodConfirmNo;
    }

    public void setCheckMethodConfirmNo(String checkMethodConfirmNo) {
        this.checkMethodConfirmNo = checkMethodConfirmNo;
    }

    public Long getPass() {
        return pass;
    }

    public void setPass(Long pass) {
        this.pass = pass;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getHandlingMeasuresCode() {
        return handlingMeasuresCode;
    }

    public void setHandlingMeasuresCode(Long handlingMeasuresCode) {
        this.handlingMeasuresCode = handlingMeasuresCode;
    }

    public String getHandlingMeasuresName() {
        return handlingMeasuresName;
    }

    public void setHandlingMeasuresName(String handlingMeasuresName) {
        this.handlingMeasuresName = handlingMeasuresName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getConfirmAnnounceNo() {
        return confirmAnnounceNo;
    }

    public void setConfirmAnnounceNo(String confirmAnnounceNo) {
        this.confirmAnnounceNo = confirmAnnounceNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (certificateProductId != null ? certificateProductId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdCertificateProductXml)) {
            return false;
        }
        IfdCertificateProductXml other = (IfdCertificateProductXml) object;
        if ((this.certificateProductId == null && other.certificateProductId != null) || (this.certificateProductId != null && !this.certificateProductId.equals(other.certificateProductId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdCertificateProduct[ certificateProductId=" + certificateProductId + " ]";
    }

}
