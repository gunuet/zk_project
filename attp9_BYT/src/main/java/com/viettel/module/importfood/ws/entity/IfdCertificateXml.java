/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.entity;

import com.viettel.module.importfood.ws.annotations.DateSerialization;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Administrator
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IfdCertificateXml implements Serializable {

    @XmlTransient
    private static final long serialVersionUID = 1L;
    @XmlTransient
    private Long certificateId;
    @XmlTransient
    private Long fileId;
    @XmlElement(name = "FilesNo")
    private Date certifaicteFilesNo;
    @XmlTransient
    private String fileCode;
    @XmlTransient
    private Long checkMethod;
    @XmlTransient
    private Date certificateCreateDate;
    @XmlTransient
    private String certificateCreateBy;
    @XmlElement(name = "GoodsOwnerName")
    private String goodsOwnerName;
    @XmlElement(name = "GoodsOwnerAddress")
    private String goodsOwnerAddress;
    @XmlElement(name = "GoodsOwnerPhone")
    private String goodsOwnerPhone;
    @XmlElement(name = "GoodsOwnerFax")
    private String goodsOwnerFax;
    @XmlElement(name = "GoodsOwnerEmail")
    private String goodsOwnerEmail;
    @XmlElement(name = "ResponsiblePersonName")
    private String responesiblePersonName;
    @XmlElement(name = "ResponsiblePersonAddress")
    private String responesiblePersonAddress;
    @XmlElement(name = "ResponsiblePersonPhone")
    private String responesiblePersonPhone;
    @XmlElement(name = "ResponsiblePersonFax")
    private String responesiblePersonFax;
    @XmlElement(name = "ResponsiblePersonEmail")
    private String responesiblePersonEmail;
    @XmlElement(name = "ExporterName")
    private String exporterName;
    @XmlElement(name = "ExporterAddress")
    private String exporterAddress;
    @XmlElement(name = "ExporterPhone")
    private String exporterPhone;
    @XmlElement(name = "ExporterFax")
    private String exporterFax;
    @XmlElement(name = "ExporterEmail")
    private String exporterEmail;
    @XmlElement(name = "CustomDeclarationNo")
    private String customDeclarationNo;
    @XmlElement(name = "ComingDateFrom")
    @XmlJavaTypeAdapter(DateSerialization.class)
    private Date comingDateFrom;
    @XmlElement(name = "ComingDateTo")
    @XmlJavaTypeAdapter(DateSerialization.class)
    private Date comingDateTo;
    @XmlElement(name = "ExporterGateCode")
    private String exporterGateCode;
    @XmlElement(name = "ExporterGateName")
    private String exporterGateName;
    @XmlElement(name = "ImporterGateCode")
    private String importerGateCode;
    @XmlElement(name = "ImporterGateName")
    private String importerGateName;
    @XmlElement(name = "CheckTimeFrom")
    @XmlJavaTypeAdapter(DateSerialization.class)
    private Date checkTimeFrom;
    @XmlElement(name = "CheckTimeTo")
    @XmlJavaTypeAdapter(DateSerialization.class)
    private Date checkTimeTo;
    @XmlElement(name = "CheckPlace")
    private String checkPlace;
    @XmlElement(name = "DeptCode")
    private String checkDeptCode;
    @XmlElement(name = "DeptName")
    private String checkDeptName;
    @XmlTransient
    private Long certificateStatus;
    @XmlTransient
    private String certificateStatusName;
    @XmlTransient
    private Long status;
    @XmlTransient
    private Long isDelete;
    @XmlTransient
    private Date createDate;
    @XmlTransient
    private Long createBy;
    @XmlTransient
    private Date updateDate;
    @XmlTransient
    private Long updateBy;
    @XmlElement(name = "SignDate")
    @XmlJavaTypeAdapter(DateSerialization.class)
    private Date signDate;
    @XmlElement(name = "SignName")
    private String signName;
    
    @XmlElementWrapper(name = "ProductList")
    @XmlElement(name = "Product")
    private List<IfdCertificateProductXml> lstProduct;

    @XmlElement(name = "Attachment")
    private IfdAttachmentXml attach;

    public IfdCertificateXml() {
    }

    public IfdCertificateXml(Long certificateId) {
        this.certificateId = certificateId;
    }

    public IfdCertificateXml(Long certificateId, Long status, Date createDate, Date updateDate) {
        this.certificateId = certificateId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Date getCertifaicteFilesNo() {
        return certifaicteFilesNo;
    }

    public void setCertifaicteFilesNo(Date certifaicteFilesNo) {
        this.certifaicteFilesNo = certifaicteFilesNo;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Long getCheckMethod() {
        return checkMethod;
    }

    public void setCheckMethod(Long checkMethod) {
        this.checkMethod = checkMethod;
    }

    public Date getCertificateCreateDate() {
        return certificateCreateDate;
    }

    public void setCertificateCreateDate(Date certificateCreateDate) {
        this.certificateCreateDate = certificateCreateDate;
    }

    public String getCertificateCreateBy() {
        return certificateCreateBy;
    }

    public void setCertificateCreateBy(String certificateCreateBy) {
        this.certificateCreateBy = certificateCreateBy;
    }

    public String getGoodsOwnerName() {
        return goodsOwnerName;
    }

    public void setGoodsOwnerName(String goodsOwnerName) {
        this.goodsOwnerName = goodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return goodsOwnerAddress;
    }

    public void setGoodsOwnerAddress(String goodsOwnerAddress) {
        this.goodsOwnerAddress = goodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return goodsOwnerPhone;
    }

    public void setGoodsOwnerPhone(String goodsOwnerPhone) {
        this.goodsOwnerPhone = goodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return goodsOwnerFax;
    }

    public void setGoodsOwnerFax(String goodsOwnerFax) {
        this.goodsOwnerFax = goodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return goodsOwnerEmail;
    }

    public void setGoodsOwnerEmail(String goodsOwnerEmail) {
        this.goodsOwnerEmail = goodsOwnerEmail;
    }

    public String getResponesiblePersonName() {
        return responesiblePersonName;
    }

    public void setResponesiblePersonName(String responesiblePersonName) {
        this.responesiblePersonName = responesiblePersonName;
    }

    public String getResponesiblePersonAddress() {
        return responesiblePersonAddress;
    }

    public void setResponesiblePersonAddress(String responesiblePersonAddress) {
        this.responesiblePersonAddress = responesiblePersonAddress;
    }

    public String getResponesiblePersonPhone() {
        return responesiblePersonPhone;
    }

    public void setResponesiblePersonPhone(String responesiblePersonPhone) {
        this.responesiblePersonPhone = responesiblePersonPhone;
    }

    public String getResponesiblePersonFax() {
        return responesiblePersonFax;
    }

    public void setResponesiblePersonFax(String responesiblePersonFax) {
        this.responesiblePersonFax = responesiblePersonFax;
    }

    public String getResponesiblePersonEmail() {
        return responesiblePersonEmail;
    }

    public void setResponesiblePersonEmail(String responesiblePersonEmail) {
        this.responesiblePersonEmail = responesiblePersonEmail;
    }

    public String getExporterName() {
        return exporterName;
    }

    public void setExporterName(String exporterName) {
        this.exporterName = exporterName;
    }

    public String getExporterAddress() {
        return exporterAddress;
    }

    public void setExporterAddress(String exporterAddress) {
        this.exporterAddress = exporterAddress;
    }

    public String getExporterPhone() {
        return exporterPhone;
    }

    public void setExporterPhone(String exporterPhone) {
        this.exporterPhone = exporterPhone;
    }

    public String getExporterFax() {
        return exporterFax;
    }

    public void setExporterFax(String exporterFax) {
        this.exporterFax = exporterFax;
    }

    public String getExporterEmail() {
        return exporterEmail;
    }

    public void setExporterEmail(String exporterEmail) {
        this.exporterEmail = exporterEmail;
    }

    public String getCustomDeclarationNo() {
        return customDeclarationNo;
    }

    public void setCustomDeclarationNo(String customDeclarationNo) {
        this.customDeclarationNo = customDeclarationNo;
    }

    public Date getComingDateFrom() {
        return comingDateFrom;
    }

    public void setComingDateFrom(Date comingDateFrom) {
        this.comingDateFrom = comingDateFrom;
    }

    public Date getComingDateTo() {
        return comingDateTo;
    }

    public void setComingDateTo(Date comingDateTo) {
        this.comingDateTo = comingDateTo;
    }

    public String getExporterGateCode() {
        return exporterGateCode;
    }

    public void setExporterGateCode(String exporterGateCode) {
        this.exporterGateCode = exporterGateCode;
    }

    public String getExporterGateName() {
        return exporterGateName;
    }

    public void setExporterGateName(String exporterGateName) {
        this.exporterGateName = exporterGateName;
    }

    public String getImporterGateCode() {
        return importerGateCode;
    }

    public void setImporterGateCode(String importerGateCode) {
        this.importerGateCode = importerGateCode;
    }

    public String getImporterGateName() {
        return importerGateName;
    }

    public void setImporterGateName(String importerGateName) {
        this.importerGateName = importerGateName;
    }

    public Date getCheckTimeFrom() {
        return checkTimeFrom;
    }

    public void setCheckTimeFrom(Date checkTimeFrom) {
        this.checkTimeFrom = checkTimeFrom;
    }

    public Date getCheckTimeTo() {
        return checkTimeTo;
    }

    public void setCheckTimeTo(Date checkTimeTo) {
        this.checkTimeTo = checkTimeTo;
    }

    public String getCheckPlace() {
        return checkPlace;
    }

    public void setCheckPlace(String checkPlace) {
        this.checkPlace = checkPlace;
    }

    public String getCheckDeptCode() {
        return checkDeptCode;
    }

    public void setCheckDeptCode(String checkDeptCode) {
        this.checkDeptCode = checkDeptCode;
    }

    public String getCheckDeptName() {
        return checkDeptName;
    }

    public void setCheckDeptName(String checkDeptName) {
        this.checkDeptName = checkDeptName;
    }

    public Long getCertificateStatus() {
        return certificateStatus;
    }

    public void setCertificateStatus(Long certificateStatus) {
        this.certificateStatus = certificateStatus;
    }

    public String getCertificateStatusName() {
        return certificateStatusName;
    }

    public void setCertificateStatusName(String certificateStatusName) {
        this.certificateStatusName = certificateStatusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public IfdAttachmentXml getAttach() {
        return attach;
    }

    public void setAttach(IfdAttachmentXml attach) {
        this.attach = attach;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (certificateId != null ? certificateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdCertificateXml)) {
            return false;
        }
        IfdCertificateXml other = (IfdCertificateXml) object;
        if ((this.certificateId == null && other.certificateId != null) || (this.certificateId != null && !this.certificateId.equals(other.certificateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdCertificate[ certificateId=" + certificateId + " ]";
    }

}
