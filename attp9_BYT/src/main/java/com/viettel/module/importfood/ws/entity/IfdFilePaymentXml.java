/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.entity;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IfdFilePaymentXml implements Serializable {

    @XmlTransient
    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    private Long paymentId;
    
    @XmlTransient
    private Long fileId;
    
    @XmlTransient
    private String fileCode;
    
    @XmlTransient
    private Date processDate;
    
    @XmlTransient
    private String processName;
    
    @XmlElement(name = "PaymentType")
    private Long paymentType;
    
    @XmlTransient
    private Double cost;
    
    @XmlTransient
    private String bankNo;
    
    @XmlTransient
    private String deptCode;
    
    @XmlTransient
    private String deptName;
    
    @XmlTransient
    private Long status;
    
    @XmlTransient
    private Date createDate;
    
    @XmlTransient
    private Long createBy;
    
    @XmlTransient
    private Date updateDate;
    
    @XmlTransient
    private Long updateBy;
    
    @XmlTransient
    private String onlPaymentDeptCode;
    
    @XmlTransient
    private String onlPaymentDeptName;
    
    @XmlTransient
    private IfdAttachmentXml attach;
    
    @XmlElement(name = "AttachmentId")
    private Long attachmentCode ;

    @XmlElement(name = "AttachmentTypeCode")
    private Long attachTypeCode ;
    
    @XmlElement(name = "AttachmentTypeName")
    private String     attachTypeName ;
    
    @XmlElement(name = "AttachmentName")
    private String     attachmentName ;
    
    public IfdFilePaymentXml() {
    }

    public IfdFilePaymentXml(Long paymentId) {
        this.paymentId = paymentId;
    }

    public IfdFilePaymentXml(Long paymentId, Long status, Date createDate, Date updateDate) {
        this.paymentId = paymentId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Long getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Long paymentType) {
        this.paymentType = paymentType;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public IfdAttachmentXml getAttach() {
        return attach;
    }

    public void setAttach(IfdAttachmentXml attach) {
        this.attach = attach;
    }

    public Long getAttachmentCode() {
        return attachmentCode;
    }

    public void setAttachmentCode(Long attachmentCode) {
        this.attachmentCode = attachmentCode;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public Long getAttachTypeCode() {
        return attachTypeCode;
    }

    public void setAttachTypeCode(Long attachTypeCode) {
        this.attachTypeCode = attachTypeCode;
    }

    public String getAttachTypeName() {
        return attachTypeName;
    }

    public void setAttachTypeName(String attachTypeName) {
        this.attachTypeName = attachTypeName;
    }

    public String getOnlPaymentDeptCode() {
        return onlPaymentDeptCode;
    }

    public void setOnlPaymentDeptCode(String onlPaymentDeptCode) {
        this.onlPaymentDeptCode = onlPaymentDeptCode;
    }

    public String getOnlPaymentDeptName() {
        return onlPaymentDeptName;
    }

    public void setOnlPaymentDeptName(String onlPaymentDeptName) {
        this.onlPaymentDeptName = onlPaymentDeptName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paymentId != null ? paymentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFilePaymentXml)) {
            return false;
        }
        IfdFilePaymentXml other = (IfdFilePaymentXml) object;
        if ((this.paymentId == null && other.paymentId != null) || (this.paymentId != null && !this.paymentId.equals(other.paymentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFilePayment[ paymentId=" + paymentId + " ]";
    }
    
}
