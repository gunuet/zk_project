/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.entity;

import com.viettel.module.importfood.ws.annotations.DateSerialization;
import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Administrator
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IfdFileRequestCancelXml implements Serializable {

    @XmlTransient
    private static final long serialVersionUID = 1L;
    @XmlTransient
    private Long fileRequestCancelId;
    @XmlTransient
    private Long fileId;
    @XmlTransient
    private String fileCode;
    @XmlElement(name = "CancelDate")
    @XmlJavaTypeAdapter(DateSerialization.class)
    private Date cancelDate;
    @XmlElement(name = "Reason")
    private String reason;
    @XmlTransient
    private Long status;
    @XmlTransient
    private Date createDate;
    @XmlTransient
    private Long createBy;
    @XmlTransient
    private Date updateDate;
    @XmlTransient
    private Long updateBy;

    public IfdFileRequestCancelXml() {
    }

    public IfdFileRequestCancelXml(Long fileRequestCancelId) {
        this.fileRequestCancelId = fileRequestCancelId;
    }

    public IfdFileRequestCancelXml(Long fileRequestCancelId, Long status, Date createDate, Date updateDate) {
        this.fileRequestCancelId = fileRequestCancelId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFileRequestCancelId() {
        return fileRequestCancelId;
    }

    public void setFileRequestCancelId(Long fileRequestCancelId) {
        this.fileRequestCancelId = fileRequestCancelId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileRequestCancelId != null ? fileRequestCancelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileRequestCancelXml)) {
            return false;
        }
        IfdFileRequestCancelXml other = (IfdFileRequestCancelXml) object;
        if ((this.fileRequestCancelId == null && other.fileRequestCancelId != null) || (this.fileRequestCancelId != null && !this.fileRequestCancelId.equals(other.fileRequestCancelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFileRequestCancel[ fileRequestCancelId=" + fileRequestCancelId + " ]";
    }

}
