/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.entity;

import com.viettel.module.importfood.ws.annotations.DateSerialization;
import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Administrator
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IfdFileResultXml implements Serializable {

    @XmlTransient
    private static final Long serialVersionUID = 1L;
    @XmlTransient
    private Long fileResultId;
    @XmlTransient
    private Long fileId;
    @XmlTransient
    private String fileCode;
    @XmlElement(name = "DeptCode")
    private String deptCode;
    @XmlElement(name = "DeptName", nillable = true)
    private String deptName;
    @XmlElement(name = "ProcessDate")
    @XmlJavaTypeAdapter(DateSerialization.class)
    private Date processDate;
    @XmlElement(name = "ProcessName", nillable = true)
    private String processName;
    @XmlElement(name = "Reason", nillable = true)
    private String processContent;
    @XmlElement(name = "ReceiveNo", nillable = true)
    private String receiveNo;
    @XmlTransient
    private Long fileStatus;
    @XmlTransient
    private String fileStatusName;
    @XmlTransient
    private Long status;
    @XmlTransient
    private Date createDate;
    @XmlTransient
    private Long createBy;
    @XmlTransient
    private Date updateDate;
    @XmlTransient
    private Long updateBy;
    @XmlElement(name = "Attachment", nillable = true)
    private IfdAttachmentXml attach;

    public IfdFileResultXml() {
    }

    public IfdFileResultXml(Long fileResultId) {
        this.fileResultId = fileResultId;
    }

    public IfdFileResultXml(Long fileResultId, Long status, Date createDate, Date updateDate) {
        this.fileResultId = fileResultId;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Long getFileResultId() {
        return fileResultId;
    }

    public void setFileResultId(Long fileResultId) {
        this.fileResultId = fileResultId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessContent() {
        return processContent;
    }

    public void setProcessContent(String processContent) {
        this.processContent = processContent;
    }

    public String getReceiveNo() {
        return receiveNo;
    }

    public void setReceiveNo(String receiveNo) {
        this.receiveNo = receiveNo;
    }

    public Long getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(Long fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getFileStatusName() {
        return fileStatusName;
    }

    public void setFileStatusName(String fileStatusName) {
        this.fileStatusName = fileStatusName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public IfdAttachmentXml getAttach() {
        return attach;
    }

    public void setAttach(IfdAttachmentXml attach) {
        this.attach = attach;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileResultId != null ? fileResultId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IfdFileResultXml)) {
            return false;
        }
        IfdFileResultXml other = (IfdFileResultXml) object;
        if ((this.fileResultId == null && other.fileResultId != null) || (this.fileResultId != null && !this.fileResultId.equals(other.fileResultId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.importfood.BO.IfdFileResult[ fileResultId=" + fileResultId + " ]";
    }
    
}
