package com.viettel.module.importfood.ws.envelope;

import com.viettel.module.importfood.ws.entity.IfdCertificateXml;
import com.viettel.module.importfood.ws.entity.IfdFileFeeNoticeXml;
import com.viettel.module.importfood.ws.entity.IfdFilePaymentXml;
import com.viettel.module.importfood.ws.entity.IfdFileRequestCancelXml;
import com.viettel.module.importfood.ws.entity.IfdFileResultXml;
import com.viettel.module.importfood.ws.entity.IfdFileXml;
import com.viettel.module.importfood.ws.entity.IfdHandlingReportXml;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Content")
public class Content {

    @XmlElement(name = "Receive")
    private Success success;
    
    @XmlElement(name = "ImportedFoodInspection")
    private IfdFileXml ifdFile;
    
    @XmlElement(name = "Payment")
    private IfdFilePaymentXml ifdFilePaymentXml;
    
    @XmlElement(name = "Result")
    private IfdFileResultXml ifdFileResultXml;
    
    @XmlElement(name = "HandlingReport")
    private IfdHandlingReportXml ifdHandlingReportXml;
    
    @XmlElement(name = "Certificate")
    private IfdCertificateXml ifdCertificateXml;
    
    @XmlElement(name = "RequestCancel")
    private IfdFileRequestCancelXml ifdFileRequestCancelXml;
    
    @XmlElement(name = "DebitNote")
    private IfdFileFeeNoticeXml ifdFileFeeNoticeXml;
    
    @XmlElementWrapper(name = "ErrorList")
    @XmlElement(name = "Error")
    private List<Error> ErrorList;
    
    public Content() {
    }

    public List<Error> getErrorList() {
        return ErrorList;
    }

    public void setErrorList(List<Error> ErrorList) {
        this.ErrorList = ErrorList;
    }

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    public IfdFileXml getIfdFile() {
        return ifdFile;
    }

    public void setIfdFile(IfdFileXml ifdFile) {
        this.ifdFile = ifdFile;
    }

    public IfdFilePaymentXml getIfdFilePaymentXml() {
        return ifdFilePaymentXml;
    }

    public void setIfdFilePaymentXml(IfdFilePaymentXml ifdFilePaymentXml) {
        this.ifdFilePaymentXml = ifdFilePaymentXml;
    }

    public IfdFileFeeNoticeXml getIfdFileFeeNoticeXml() {
        return ifdFileFeeNoticeXml;
    }

    public void setIfdFileFeeNoticeXml(IfdFileFeeNoticeXml ifdFileFeeNoticeXml) {
        this.ifdFileFeeNoticeXml = ifdFileFeeNoticeXml;
    }

    public IfdFileResultXml getIfdFileResultXml() {
        return ifdFileResultXml;
    }

    public void setIfdFileResultXml(IfdFileResultXml ifdFileResultXml) {
        this.ifdFileResultXml = ifdFileResultXml;
    }

    public IfdFileRequestCancelXml getIfdFileRequestCancelXml() {
        return ifdFileRequestCancelXml;
    }

    public void setIfdFileRequestCancelXml(IfdFileRequestCancelXml ifdFileRequestCancelXml) {
        this.ifdFileRequestCancelXml = ifdFileRequestCancelXml;
    }

    public IfdHandlingReportXml getIfdHandlingReportXml() {
        return ifdHandlingReportXml;
    }

    public void setIfdHandlingReportXml(IfdHandlingReportXml ifdHandlingReportXml) {
        this.ifdHandlingReportXml = ifdHandlingReportXml;
    }

    public IfdCertificateXml getIfdCertificateXml() {
        return ifdCertificateXml;
    }

    public void setIfdCertificateXml(IfdCertificateXml ifdCertificateXml) {
        this.ifdCertificateXml = ifdCertificateXml;
    }

    
}
