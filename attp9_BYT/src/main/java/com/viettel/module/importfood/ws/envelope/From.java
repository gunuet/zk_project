package com.viettel.module.importfood.ws.envelope;

/**
 *
 * @author PhongNV9
 */
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class From {

    public From() {

    }

    public From(String name, String identity) {
        this.name = name;
        this.identity = identity;
    }

    @XmlElement(name = "name")
    @Size(max = 255)
    private String name;
    @XmlElement(name = "identity")
    @Size(max = 13)
    private String identity;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getIdentity() {
        return identity;
    }

}
