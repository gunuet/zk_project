package com.viettel.module.importfood.ws.message;

import java.util.Date;

public class IfdSendMessage {

    private String type; // kiểu bản tin
    private String function; //chức năng của bản tin
    private Long fileId; // id của hồ sơ
    private String fileCode; // mã của hồ sơ
    private String reason;// Content của lý do
    private Date delayDateTo;// Hạn mới
    private Boolean getXmlNotSend;// Trả về bản tin để ký, không gửi đi
    private String signedXml;//
    private Long certificateId;// id của kết quả cấp phép

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getDelayDateTo() {
        return delayDateTo;
    }

    public void setDelayDateTo(Date delayDateTo) {
        this.delayDateTo = delayDateTo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getGetXmlNotSend() {
        return getXmlNotSend;
    }

    public void setGetXmlNotSend(Boolean getXmlNotSend) {
        this.getXmlNotSend = getXmlNotSend;
    }

    public String getSignedXml() {
        return signedXml;
    }

    public void setSignedXml(String signedXml) {
        this.signedXml = signedXml;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }
    
    
}
