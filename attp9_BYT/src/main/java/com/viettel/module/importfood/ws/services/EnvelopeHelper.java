/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.services;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.module.importfood.ws.envelope.Body;
import com.viettel.module.importfood.ws.envelope.Content;
import com.viettel.module.importfood.ws.envelope.Envelope;
import com.viettel.module.importfood.ws.envelope.Error;
import com.viettel.module.importfood.ws.envelope.From;
import com.viettel.module.importfood.ws.envelope.Header;
import com.viettel.module.importfood.ws.envelope.Reference;
import com.viettel.module.importfood.ws.envelope.Subject;

public class EnvelopeHelper {

    private static final String CLASS_NAME = "EnvelopeHelper";

    private final SimpleDateFormat formatterYear = new SimpleDateFormat("yyyy");
    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");


    public Error createError(String code, String name) {
        try {
            Error error = new Error();
            error.setErrorCode(code);
            error.setErrorName(name);
            return error;
        } catch (Exception ex2) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex2.toString();
            return null;
        }
    }

    public String getDocumentType(String xml) {
        try {
            return getValueFromXml(xml, "/Envelope/Header/Subject/documentType");

        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();
        }
        return null;
    }

    /**
     * Lay gia tri tu xml key co dang /Envelope/Header/Subject/function
     *
     * @param xml
     * @param key
     * @return
     */
    public String getValueFromXml(String xml, String key) {
        String value;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new ByteArrayInputStream(xml.trim()
                    .getBytes(Charset.forName("utf-8"))));
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            value = getValueFromXml(xpath, doc, key);

        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();
            value = null;
        }
        return value;
    }

    /**
     * Lay gia tri tu xpath key co dang /Envelope/Header/Subject/function
     */
    public String getValueFromXml(XPath xpath, Document doc, String key) {
        String value;
        try {
            XPathExpression functionExpr = xpath.compile(key);
            NodeList fucntionNode = (NodeList) functionExpr.evaluate(doc,
                    XPathConstants.NODESET);
            value = fucntionNode.item(0).getTextContent();
        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();
            value = null;
        }
        return value;
    }

    public Envelope createEnvelopeError(String fiMaHoSo, String documentType, String msgType, Error error) {
        try {
            Envelope envl = new Envelope();
            envl.setHeader(createHeaderError(fiMaHoSo, documentType, msgType));
            envl.setBody(createBodyError(error));
            return envl;
        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();
        }
        return null;
    }

    /**
     * Tao header thong bao loi
     *
     * @param xml
     * @return
     */
    public Header createHeaderError(String fiMaHoSo, String documentType, String msgType) {
        Header hd = new Header();
        Date now = new Date();
        UUID uuid = UUID.randomUUID();
        try {
            hd.setReference(new Reference(IfdConstants.Version.RECEIVE_VERSION, uuid.toString()));
            hd.setFrom(new From(IfdConstants.From.NAME, IfdConstants.From.IDENTITY));
            hd.setTo(new From(IfdConstants.To.NAME, IfdConstants.To.IDENTITY));
            hd.setSubject(new Subject(documentType, msgType, IfdConstants.Function.FUNC_ERROR, fiMaHoSo,
                    formatterYear.format(now), fiMaHoSo, formatterYear.format(now), formatterDateTime.format(now)));
        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();

            hd.setReference(new Reference(IfdConstants.Version.RECEIVE_VERSION, uuid.toString()));
            hd.setFrom(new From(IfdConstants.From.NAME, IfdConstants.From.IDENTITY));
            hd.setTo(new From(IfdConstants.To.NAME, IfdConstants.To.IDENTITY));
            hd.setSubject(new Subject(documentType, msgType, IfdConstants.Function.FUNC_ERROR, fiMaHoSo,
                    formatterYear.format(now), fiMaHoSo, formatterYear.format(now), formatterDateTime.format(now)));
        }
        return hd;
    }

    /**
     * Tao body thong bao loi
     *
     * @param error
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public Body createBodyError(Error error) {
        try {
            Body body = new Body();
            Content content = new Content();
            List<Error> errorList = new ArrayList();
            errorList.add(error);
            content.setErrorList(errorList);
            body.setContent(content);
            return body;
        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();
        }
        return null;
    }

    public Error createError(String code, String name, Exception ex) {
        try {
//			String cause = "";
//			if (ex.getCause() != null) {
//				cause = String.valueOf(ex.getCause());
//			}
//			if (cause.length() > 200) {
//				cause = cause.substring(200);
//			}
            Error error = new Error();
            error.setErrorCode(code);
            error.setErrorName(name);
            return error;
        } catch (Exception ex2) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex2.toString();
            return null;
        }
    }

    public Header createReceiveHeader(String fiMaHoSo, String documentType, String msgType, String msgFunc) {
        Header hd = new Header();
        Date now = new Date();
        UUID uuid = UUID.randomUUID();
        try {
            hd.setReference(new Reference(IfdConstants.Version.RECEIVE_VERSION, uuid.toString()));
            hd.setFrom(new From(IfdConstants.From.NAME, IfdConstants.From.IDENTITY));
            hd.setTo(new From(IfdConstants.To.NAME, IfdConstants.To.IDENTITY));
            hd.setSubject(new Subject(documentType, msgType, msgFunc, fiMaHoSo,
                    formatterYear.format(now), fiMaHoSo, formatterYear.format(now), formatterDateTime.format(now)));
        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();

            hd.setReference(new Reference(IfdConstants.Version.RECEIVE_VERSION, uuid.toString()));
            hd.setFrom(new From(IfdConstants.From.NAME, IfdConstants.From.IDENTITY));
            hd.setTo(new From(IfdConstants.To.NAME, IfdConstants.To.IDENTITY));
            hd.setSubject(new Subject(documentType, msgType, IfdConstants.Function.FUNC_ERROR, fiMaHoSo,
                    formatterYear.format(now), fiMaHoSo, formatterYear.format(now), formatterDateTime.format(now)));
        }
        return hd;
    }

    public Body createBody(Content content) {
        try {
            Body body = new Body();
            body.setContent(content);
            body.setSignature("");
            return body;
        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();
        }
        return new Body();
    }

    public Envelope createResponse(Header header, Body body) {
        try {
            Envelope envl = new Envelope();
            envl.setHeader(header);
            envl.setBody(body);
            envl.setSystemSignature("");
            return envl;
        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();
        }
        return new Envelope();
    }

    public Header createSendHeader(String fiMaHoSo, String documentType, String msgType, String msgFunc) {
        Header hd = new Header();
        Date now = new Date();
        UUID uuid = UUID.randomUUID();

        try {
            hd.setReference(new Reference(IfdConstants.Version.SEND_VERSION, uuid.toString()));
            hd.setFrom(new From(IfdConstants.From.NAME, IfdConstants.From.IDENTITY));
            hd.setTo(new From(IfdConstants.To.NAME, IfdConstants.To.IDENTITY));
            hd.setSubject(new Subject(documentType, msgType, msgFunc, fiMaHoSo,
                    formatterYear.format(now), fiMaHoSo, formatterYear.format(now), formatterDateTime.format(now)));
        } catch (Exception ex) {
            String errorInfo = IfdConstants.APP_NAME + IfdConstants.MESSAGE_SEPARATOR + CLASS_NAME
                    + IfdConstants.MESSAGE_SEPARATOR + Thread.currentThread().getStackTrace()[1].getMethodName()
                    + IfdConstants.MESSAGE_SEPARATOR + ex.toString();

            hd.setReference(new Reference(IfdConstants.Version.SEND_VERSION, uuid.toString()));
            hd.setFrom(new From(IfdConstants.From.NAME, IfdConstants.From.IDENTITY));
            hd.setTo(new From(IfdConstants.To.NAME, IfdConstants.To.IDENTITY));
            hd.setSubject(new Subject(documentType, msgType, IfdConstants.Function.FUNC_ERROR, fiMaHoSo,
                    formatterYear.format(now), fiMaHoSo, formatterYear.format(now), formatterDateTime.format(now)));
        }
        return hd;
    }
}
