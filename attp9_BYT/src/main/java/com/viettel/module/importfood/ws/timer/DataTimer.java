/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.timer;

import com.viettel.utils.LogUtils;
import java.util.List;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author Administrator
 */
public class DataTimer {

    public static void startSync() {
        try {
//            //copy pending file AS2, AS3 to pending_folder
//            SendData.genPendingMsg();
//            
            // specify the job' s details..
            JobDetail job = JobBuilder.newJob(TimerJob.class)
                    .withIdentity("TimerJob", "DB")
                    .build();

            Scheduler scheduler = new StdSchedulerFactory().getScheduler();

            TriggerKey triggerKey = TriggerKey.triggerKey("TimerJobTrigger", "DB");
            Trigger oldTrigger = scheduler.getTrigger(triggerKey);

            if (oldTrigger != null) {
                scheduler.start();
                scheduler.scheduleJob(job, oldTrigger);
            } else {
                Trigger trigger = TriggerBuilder.newTrigger()
                        .withIdentity("TimerJobTrigger", "DB")
                        .withSchedule(simpleSchedule()
                                .withIntervalInSeconds(120)
                                .repeatForever())
                        .build();

                scheduler.start();
                scheduler.scheduleJob(job, trigger);
            }
        } catch (SchedulerException e) {
            LogUtils.addLogDB(e);
        }
    }

    public static boolean isJobRunning(Scheduler scheduler, String jobName, String groupName)
            throws SchedulerException {
        List<JobExecutionContext> currentJobs = scheduler.getCurrentlyExecutingJobs();

        for (JobExecutionContext jobCtx : currentJobs) {
            String thisJobName = jobCtx.getJobDetail().getKey().getName();
            String thisGroupName = jobCtx.getJobDetail().getKey().getGroup();
            if (jobName.equalsIgnoreCase(thisJobName) && groupName.equalsIgnoreCase(thisGroupName)) {
                return true;
            }
        }
        return false;
    }

    public static void stopSync() {        
        try {
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            TriggerKey triggerKey = TriggerKey.triggerKey("TimerJobTrigger", "DB");
            if (triggerKey != null) {
                scheduler.unscheduleJob(triggerKey);
                scheduler.shutdown(true);
            }
        } catch (SchedulerException ex) {
            LogUtils.addLogDB(ex);
        }
    }
}
