/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Administrator
 */
public class IfdDataListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.err.println("IfdDataListener.contextInitialized");
        DataTimer.startSync();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        DataTimer.stopSync();
    }
    
}
