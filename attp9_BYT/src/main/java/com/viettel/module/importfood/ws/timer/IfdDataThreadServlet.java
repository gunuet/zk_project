///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.viettel.module.importfood.ws.timer;
//
//import com.viettel.utils.LogUtils;
//import java.io.IOException;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// *
// * @author Administrator
// */
//public class IfdDataThreadServlet extends HttpServlet {
//
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        response.getWriter().print("Start thread.");
//
//        try {
//            System.err.println("IfdDataThreadServlet.doGet");
//            DataTimer.startSync();
//            response.getWriter().print("Success.");
//        } catch (Exception ex) {
//            LogUtils.addLogDB(ex);
//        }
//        response.getWriter().print("End.");
//    }
//}
