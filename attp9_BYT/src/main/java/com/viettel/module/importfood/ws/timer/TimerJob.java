/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.importfood.ws.timer;

import com.viettel.module.importfood.BO.IfdFile;
import com.viettel.module.importfood.Constant.FileStatus;
import com.viettel.module.importfood.Service.IfdFileService;
import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.module.importfood.ws.NSWSendHelper;
import com.viettel.module.importfood.ws.message.IfdSendMessage;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.HibernateUtil;
import com.viettel.utils.LogUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Administrator
 */
public class TimerJob implements Job {

    AtomicReference<Thread> runningThread = new AtomicReference<Thread>();
    AtomicBoolean stopFlag = new AtomicBoolean(false);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {
            // Tìm kiếm các hồ sơ ở trạng thái chờ thông báo phí. 
            IfdFileService ifdFileService = new IfdFileService();
            
            System.out.println(DateTimeUtils.convertDateTimeToString(new Date()) + " com.viettel.module.importfood.ws.timer.TimerJob.execute()");
            List<Long> lstFileStatus = new ArrayList<>();
            lstFileStatus.add(FileStatus.HO_SO_GUI_MOI_CHO_AP_PHI.getStatus());
            lstFileStatus.add(FileStatus.HO_SO_GUI_SUA_CHO_AP_PHI.getStatus());
            
            List<IfdFile> lstFileWaitingNoticeFee = ifdFileService.findAllByFileStatus(lstFileStatus);
            if (lstFileWaitingNoticeFee != null && !lstFileWaitingNoticeFee.isEmpty()){
                
                int currentIndex = 0;
                for (IfdFile ifdFile : lstFileWaitingNoticeFee) {
                    //xử lý gửi áp phí tối đa 10 hồ sơ trong mỗi 30s
                    if (currentIndex >= 10){
                        break;
                    }
                    IfdSendMessage sendMessage = new IfdSendMessage();
                    sendMessage.setFileId(ifdFile.getFileId());
                    sendMessage.setFileCode(ifdFile.getFileCode());
                    sendMessage.setType(IfdConstants.ATTP_TYPE.TYPE_110);
                    sendMessage.setFunction(IfdConstants.ATTP_FUNCTION.FUNCTION_08);
                    
                    NSWSendHelper sendHelper = new NSWSendHelper();
                    sendHelper.sendMessage(sendMessage);
                    currentIndex++;
                }
            }
            
            
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        // Commit
        try {
            HibernateUtil.commitCurrentSessions();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        } finally {
            try {
                HibernateUtil.closeCurrentSessions();
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
        }
    }
}
