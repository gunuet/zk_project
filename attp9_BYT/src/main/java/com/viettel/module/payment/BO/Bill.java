/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author hoang_000
 */
@Entity
@Table(name = "BILL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bill.findAll", query = "SELECT b FROM Bill b"),
    @NamedQuery(name = "Bill.findByBillId", query = "SELECT b FROM Bill b WHERE b.billId = :billId"),
    @NamedQuery(name = "Bill.findByCode", query = "SELECT b FROM Bill b WHERE b.code = :code"),
    @NamedQuery(name = "Bill.findByTotal", query = "SELECT b FROM Bill b WHERE b.total = :total"),
    @NamedQuery(name = "Bill.findByCreateDate", query = "SELECT b FROM Bill b WHERE b.createDate = :createDate"),
    @NamedQuery(name = "Bill.findByCreatorId", query = "SELECT b FROM Bill b WHERE b.creatorId = :creatorId"),
    @NamedQuery(name = "Bill.findByCreatetorName", query = "SELECT b FROM Bill b WHERE b.createtorName = :createtorName"),
    @NamedQuery(name = "Bill.findByStatus", query = "SELECT b FROM Bill b WHERE b.status = :status"),
    @NamedQuery(name = "Bill.findByPaymentDate", query = "SELECT b FROM Bill b WHERE b.paymentDate = :paymentDate"),
    @NamedQuery(name = "Bill.findByPaymentTypeId", query = "SELECT b FROM Bill b WHERE b.paymentTypeId = :paymentTypeId"),
    @NamedQuery(name = "Bill.findByBillSign", query = "SELECT b FROM Bill b WHERE b.billSign = :billSign"),
    @NamedQuery(name = "Bill.findByConfirmDate", query = "SELECT b FROM Bill b WHERE b.confirmDate = :confirmDate"),
    @NamedQuery(name = "Bill.findByConfirmUserId", query = "SELECT b FROM Bill b WHERE b.confirmUserId = :confirmUserId"),
    @NamedQuery(name = "Bill.findByConfirmUserName", query = "SELECT b FROM Bill b WHERE b.confirmUserName = :confirmUserName"),
    @NamedQuery(name = "Bill.findByConfirmContent", query = "SELECT b FROM Bill b WHERE b.confirmContent = :confirmContent"),
    @NamedQuery(name = "Bill.findByKeypayCode", query = "SELECT b FROM Bill b WHERE b.keypayCode = :keypayCode"),
    @NamedQuery(name = "Bill.findByPaymentInfoKeypay", query = "SELECT b FROM Bill b WHERE b.paymentInfoKeypay = :paymentInfoKeypay"),
    @NamedQuery(name = "Bill.findByCreatorAddress", query = "SELECT b FROM Bill b WHERE b.creatorAddress = :creatorAddress")})
public class Bill implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "BILL_SEQ", sequenceName = "BILL_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BILL_SEQ")
    @Column(name = "BILL_ID")
    private Long billId;
    @Size(max = 255)
    @Column(name = "CODE")
    private String code;
    @Column(name = "TOTAL")
    private Long total;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 255)
    @Column(name = "CREATETOR_NAME")
    private String createtorName;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "PAYMENT_DATE")
    @Temporal(TemporalType.DATE)
    private Date paymentDate;
    @Column(name = "PAYMENT_TYPE_ID")
    private Long paymentTypeId;
    @Size(max = 255)
    @Column(name = "BILL_SIGN")
    private String billSign;
    @Column(name = "CONFIRM_DATE")
    @Temporal(TemporalType.DATE)
    private Date confirmDate;
    @Column(name = "CONFIRM_USER_ID")
    private Long confirmUserId;
    @Size(max = 255)
    @Column(name = "CONFIRM_USER_NAME")
    private String confirmUserName;
    @Size(max = 500)
    @Column(name = "CONFIRM_CONTENT")
    private String confirmContent;
    @Size(max = 50)
    @Column(name = "KEYPAY_CODE")
    private String keypayCode;
    @Size(max = 500)
    @Column(name = "PAYMENT_INFO_KEYPAY")
    private String paymentInfoKeypay;
    @Size(max = 300)
    @Column(name = "CREATOR_ADDRESS")
    private String creatorAddress;

    public Bill() {
    }

    public Bill(Long billId) {
        this.billId = billId;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatetorName() {
        return createtorName;
    }

    public void setCreatetorName(String createtorName) {
        this.createtorName = createtorName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Long getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getBillSign() {
        return billSign;
    }

    public void setBillSign(String billSign) {
        this.billSign = billSign;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public Long getConfirmUserId() {
        return confirmUserId;
    }

    public void setConfirmUserId(Long confirmUserId) {
        this.confirmUserId = confirmUserId;
    }

    public String getConfirmUserName() {
        return confirmUserName;
    }

    public void setConfirmUserName(String confirmUserName) {
        this.confirmUserName = confirmUserName;
    }

    public String getConfirmContent() {
        return confirmContent;
    }

    public void setConfirmContent(String confirmContent) {
        this.confirmContent = confirmContent;
    }

    public String getKeypayCode() {
        return keypayCode;
    }

    public void setKeypayCode(String keypayCode) {
        this.keypayCode = keypayCode;
    }

    public String getPaymentInfoKeypay() {
        return paymentInfoKeypay;
    }

    public void setPaymentInfoKeypay(String paymentInfoKeypay) {
        this.paymentInfoKeypay = paymentInfoKeypay;
    }

    public String getCreatorAddress() {
        return creatorAddress;
    }

    public void setCreatorAddress(String creatorAddress) {
        this.creatorAddress = creatorAddress;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (billId != null ? billId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bill)) {
            return false;
        }
        Bill other = (Bill) object;
        if ((this.billId == null && other.billId != null) || (this.billId != null && !this.billId.equals(other.billId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.payment.BO.Bill[ billId=" + billId + " ]";
    }
    
}
