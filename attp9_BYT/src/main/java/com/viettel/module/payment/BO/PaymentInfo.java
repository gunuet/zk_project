/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.BO;


import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "PAYMENT_INFO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentInfo.findAll", query = "SELECT p FROM PaymentInfo p"),
    @NamedQuery(name = "PaymentInfo.findByPaymentInfoId", query = "SELECT p FROM PaymentInfo p WHERE p.paymentInfoId = :paymentInfoId"),
    @NamedQuery(name = "PaymentInfo.findByFileId", query = "SELECT p FROM PaymentInfo p WHERE p.fileId = :fileId")})
public class PaymentInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "PAYMENT_INFO_SEQ", sequenceName = "PAYMENT_INFO_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAYMENT_INFO_SEQ")
    @Column(name = "PAYMENT_INFO_ID")
    private Long paymentInfoId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "NOTE")
    private String note;
    @Column(name = "COST")
    private Long cost;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "FEE_ID")
    private Long feeId;
    @Column(name = "FEE_NAME")
    private String feeName;
    @Column(name = "BILL_ID")
    private Long billId;
    @Column(name = "BILL_CODE")
    private String billCode;
    @Column(name = "PAYMENT_TYPE_ID")
    private Long paymentTypeId;
    @Column(name = "PAYMENT_CONFIRM")
    private String paymentConfirm;
    @Column(name = "PAYMENT_CODE")
    private String paymentCode;
    @Column(name = "DATE_CONFIRM")
    @Temporal(TemporalType.DATE)
    private Date dateConfirm;
    @Column(name = "PAYMENT_DATE")
    @Temporal(TemporalType.DATE)
    private Date paymentDate;
    @Column(name = "PAYMENT_PERSON")
    private String paymentPerson;
    @Column(name = "PAYMENT_ACTION_USER_ID")
    private Long paymentActionUserId;
    @Column(name = "PAYMENT_ACTION_USER")
    private String paymentActionUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.DATE)
    private Date updateDate;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    
    @Column(name = "PHASE")
    private Long phase;

    @Column(name = "DEPT_CODE")
    private String deptCode;
    @Column(name = "DEPT_NAME")
    private String deptName;
    @Column(name = "BANK_NO")
    private String bankNo;

    public PaymentInfo() {
        
    }

    public PaymentInfo(Long paymentInfoId) {
        this.paymentInfoId = paymentInfoId;
    }

    public PaymentInfo(Long paymentInfoId, Long billId, Date paymentDate) {
        this.paymentInfoId = paymentInfoId;
        this.billId = billId;
        this.paymentDate = paymentDate;
    }

     public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
     public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }
     public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }
    public Long getPaymentInfoId() {
        return paymentInfoId;
    }

    public void setPaymentInfoId(Long paymentInfoId) {
        this.paymentInfoId = paymentInfoId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getFeeId() {
        return feeId;
    }

    public void setFeeId(Long feeId) {
        this.feeId = feeId;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public Long getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getPaymentConfirm() {
        return paymentConfirm;
    }

    public void setPaymentConfirm(String paymentConfirm) {
        this.paymentConfirm = paymentConfirm;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public Date getDateConfirm() {
        return dateConfirm;
    }

    public void setDateConfirm(Date dateConfirm) {
        this.dateConfirm = dateConfirm;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentPerson() {
        return paymentPerson;
    }

    public void setPaymentPerson(String paymentPerson) {
        this.paymentPerson = paymentPerson;
    }

    public Long getPaymentActionUserId() {
        return paymentActionUserId;
    }

    public void setPaymentActionUserId(Long paymentActionUserId) {
        this.paymentActionUserId = paymentActionUserId;
    }

    public String getPaymentActionUser() {
        return paymentActionUser;
    }

    public void setPaymentActionUser(String paymentActionUser) {
        this.paymentActionUser = paymentActionUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getPhase() {
        return phase;
    }

    public void setPhase(Long phase) {
        this.phase = phase;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paymentInfoId != null ? paymentInfoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentInfo)) {
            return false;
        }
        PaymentInfo other = (PaymentInfo) object;
        if ((this.paymentInfoId == null && other.paymentInfoId != null) || (this.paymentInfoId != null && !this.paymentInfoId.equals(other.paymentInfoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.voffice.BO.PaymentInfo[ paymentInfoId=" + paymentInfoId + " ]";
    }
    
       public String getPaymentDateStr() {
        if (paymentDate == null) {
            return null;
        }
        return DateTimeUtils.convertDateToString(paymentDate);
    }

    public String getPaymentTypeStr() {
         String sTemp = "";
        if (paymentTypeId == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
            sTemp = getLabelCos("pay_type_code_keypay");
        } else if (paymentTypeId == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
            sTemp = getLabelCos("pay_type_code_chuyenkhoan");
        } else if (paymentTypeId == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
            sTemp = getLabelCos("pay_type_code_tienmat");
        }
        return sTemp;

    }
    
     public String getStatusStr() {
        String statusPaymentName = "";
        if (status != null) {
            if (status == ConstantsPayment.PAYMENT.PAY_NEW) {
                statusPaymentName = getLabelCos("pay_new");
            } else if (status == ConstantsPayment.PAYMENT.PAY_ALREADY) {
                statusPaymentName = getLabelCos("pay_already");
            } else if (status == ConstantsPayment.PAYMENT.PAY_CONFIRMED) {
                statusPaymentName = getLabelCos("pay_confirmed");
            } else if (status == ConstantsPayment.PAYMENT.PAY_REJECTED) {
                statusPaymentName = getLabelCos("pay_rejected");
            }
        }
        return statusPaymentName;
    }
    
    public  String getLabelCos(String key){
        return getLabelCos(key, "");
    }
    
    public  String getLabelCos(String key, String defaultValue){
        try {
            return ResourceBundleUtil.getString(key,"language_COSMETIC_vi");
        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLogDB(ex);
        }
        return defaultValue;
    }
}
