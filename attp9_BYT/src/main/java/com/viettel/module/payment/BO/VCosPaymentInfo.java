/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hoang_000
 */
@Entity
@Table(name = "V_COS_PAYMENT_INFO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCosPaymentInfo.findAll", query = "SELECT v FROM VCosPaymentInfo v"),
    @NamedQuery(name = "VCosPaymentInfo.findByFileId", query = "SELECT v FROM VCosPaymentInfo v WHERE v.fileId = :fileId"),
    @NamedQuery(name = "VCosPaymentInfo.findByFileType", query = "SELECT v FROM VCosPaymentInfo v WHERE v.fileType = :fileType"),
    @NamedQuery(name = "VCosPaymentInfo.findByCreateDate", query = "SELECT v FROM VCosPaymentInfo v WHERE v.createDate = :createDate"),
    @NamedQuery(name = "VCosPaymentInfo.findByModifyDate", query = "SELECT v FROM VCosPaymentInfo v WHERE v.modifyDate = :modifyDate"),
    @NamedQuery(name = "VCosPaymentInfo.findByTaxCode", query = "SELECT v FROM VCosPaymentInfo v WHERE v.taxCode = :taxCode"),
    @NamedQuery(name = "VCosPaymentInfo.findByBusinessName", query = "SELECT v FROM VCosPaymentInfo v WHERE v.businessName = :businessName"),
    @NamedQuery(name = "VCosPaymentInfo.findByBusinessAddress", query = "SELECT v FROM VCosPaymentInfo v WHERE v.businessAddress = :businessAddress"),
    @NamedQuery(name = "VCosPaymentInfo.findByBusinessPhone", query = "SELECT v FROM VCosPaymentInfo v WHERE v.businessPhone = :businessPhone"),
    @NamedQuery(name = "VCosPaymentInfo.findByBusinessFax", query = "SELECT v FROM VCosPaymentInfo v WHERE v.businessFax = :businessFax"),
    @NamedQuery(name = "VCosPaymentInfo.findByIsActive", query = "SELECT v FROM VCosPaymentInfo v WHERE v.isActive = :isActive"),
    @NamedQuery(name = "VCosPaymentInfo.findByCreatorId", query = "SELECT v FROM VCosPaymentInfo v WHERE v.creatorId = :creatorId"),
    @NamedQuery(name = "VCosPaymentInfo.findByCreatorName", query = "SELECT v FROM VCosPaymentInfo v WHERE v.creatorName = :creatorName"),
    @NamedQuery(name = "VCosPaymentInfo.findByCreateDeptId", query = "SELECT v FROM VCosPaymentInfo v WHERE v.createDeptId = :createDeptId"),
    @NamedQuery(name = "VCosPaymentInfo.findByCreateDeptName", query = "SELECT v FROM VCosPaymentInfo v WHERE v.createDeptName = :createDeptName"),
    @NamedQuery(name = "VCosPaymentInfo.findByNswFileCode", query = "SELECT v FROM VCosPaymentInfo v WHERE v.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "VCosPaymentInfo.findByCosFileId", query = "SELECT v FROM VCosPaymentInfo v WHERE v.cosFileId = :cosFileId"),
    @NamedQuery(name = "VCosPaymentInfo.findByCosmeticNo", query = "SELECT v FROM VCosPaymentInfo v WHERE v.cosmeticNo = :cosmeticNo"),
    @NamedQuery(name = "VCosPaymentInfo.findByBrandName", query = "SELECT v FROM VCosPaymentInfo v WHERE v.brandName = :brandName"),
    @NamedQuery(name = "VCosPaymentInfo.findByDocumentTypeCode", query = "SELECT v FROM VCosPaymentInfo v WHERE v.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "VCosPaymentInfo.findByProductName", query = "SELECT v FROM VCosPaymentInfo v WHERE v.productName = :productName"),
    @NamedQuery(name = "VCosPaymentInfo.findByPaymentInfoId", query = "SELECT v FROM VCosPaymentInfo v WHERE v.paymentInfoId = :paymentInfoId"),
    @NamedQuery(name = "VCosPaymentInfo.findByStatusfile", query = "SELECT v FROM VCosPaymentInfo v WHERE v.statusfile = :statusfile"),
    @NamedQuery(name = "VCosPaymentInfo.findByStatuspayment", query = "SELECT v FROM VCosPaymentInfo v WHERE v.statuspayment = :statuspayment"),
    @NamedQuery(name = "VCosPaymentInfo.findByPaymentPerson", query = "SELECT v FROM VCosPaymentInfo v WHERE v.paymentPerson = :paymentPerson"),
    @NamedQuery(name = "VCosPaymentInfo.findByPaymentDate", query = "SELECT v FROM VCosPaymentInfo v WHERE v.paymentDate = :paymentDate"),
    @NamedQuery(name = "VCosPaymentInfo.findByFeeId", query = "SELECT v FROM VCosPaymentInfo v WHERE v.feeId = :feeId"),
    @NamedQuery(name = "VCosPaymentInfo.findByPaymentTypeId", query = "SELECT v FROM VCosPaymentInfo v WHERE v.paymentTypeId = :paymentTypeId"),
    @NamedQuery(name = "VCosPaymentInfo.findByCost", query = "SELECT v FROM VCosPaymentInfo v WHERE v.cost = :cost"),
    @NamedQuery(name = "VCosPaymentInfo.findByPaymentCode", query = "SELECT v FROM VCosPaymentInfo v WHERE v.paymentCode = :paymentCode"),
    @NamedQuery(name = "VCosPaymentInfo.findByPaymentConfirm", query = "SELECT v FROM VCosPaymentInfo v WHERE v.paymentConfirm = :paymentConfirm"),
    @NamedQuery(name = "VCosPaymentInfo.findByPaymentActionUser", query = "SELECT v FROM VCosPaymentInfo v WHERE v.paymentActionUser = :paymentActionUser"),
    @NamedQuery(name = "VCosPaymentInfo.findByBillId", query = "SELECT v FROM VCosPaymentInfo v WHERE v.billId = :billId"),
    @NamedQuery(name = "VCosPaymentInfo.findByBillCode", query = "SELECT v FROM VCosPaymentInfo v WHERE v.billCode = :billCode"),
    @NamedQuery(name = "VCosPaymentInfo.findByDateConfirm", query = "SELECT v FROM VCosPaymentInfo v WHERE v.dateConfirm = :dateConfirm"),
    @NamedQuery(name = "VCosPaymentInfo.findByNote", query = "SELECT v FROM VCosPaymentInfo v WHERE v.note = :note"),
    @NamedQuery(name = "VCosPaymentInfo.findByFeeName", query = "SELECT v FROM VCosPaymentInfo v WHERE v.feeName = :feeName"),
    @NamedQuery(name = "VCosPaymentInfo.findByPhase", query = "SELECT v FROM VCosPaymentInfo v WHERE v.phase = :phase")})
public class VCosPaymentInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.DATE)
    private Date modifyDate;
    @Size(max = 31)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Size(max = 255)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 500)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 31)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 31)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 255)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 255)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Size(max = 31)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COS_FILE_ID")
    private Long cosFileId;
    @Size(max = 31)
    @Column(name = "COSMETIC_NO")
    private String cosmeticNo;
    @Size(max = 81)
    @Column(name = "BRAND_NAME")
    private String brandName;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Size(max = 81)
    @Column(name = "PRODUCT_NAME")
    private String productName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PAYMENT_INFO_ID")
    @Id
    private Long paymentInfoId;
    @Column(name = "STATUSFILE")
    private Long statusfile;
    @Column(name = "STATUSPAYMENT")
    private Long statuspayment;
    @Size(max = 255)
    @Column(name = "PAYMENT_PERSON")
    private String paymentPerson;
    @Column(name = "PAYMENT_DATE")
    @Temporal(TemporalType.DATE)
    private Date paymentDate;
    @Column(name = "FEE_ID")
    private Long feeId;
    @Column(name = "PAYMENT_TYPE_ID")
    private Long paymentTypeId;
    @Column(name = "COST")
    private Long cost;
    @Size(max = 255)
    @Column(name = "PAYMENT_CODE")
    private String paymentCode;
    @Size(max = 255)
    @Column(name = "PAYMENT_CONFIRM")
    private String paymentConfirm;
    @Size(max = 255)
    @Column(name = "PAYMENT_ACTION_USER")
    private String paymentActionUser;
    @Column(name = "BILL_ID")
    private Long billId;
    @Size(max = 255)
    @Column(name = "BILL_CODE")
    private String billCode;
    @Column(name = "DATE_CONFIRM")
    @Temporal(TemporalType.DATE)
    private Date dateConfirm;
    @Size(max = 500)
    @Column(name = "NOTE")
    private String note;
    @Size(max = 255)
    @Column(name = "FEE_NAME")
    private String feeName;
    @Column(name = "PHASE")
    private Long phase;

    public VCosPaymentInfo() {
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getCosFileId() {
        return cosFileId;
    }

    public void setCosFileId(Long cosFileId) {
        this.cosFileId = cosFileId;
    }

    public String getCosmeticNo() {
        return cosmeticNo;
    }

    public void setCosmeticNo(String cosmeticNo) {
        this.cosmeticNo = cosmeticNo;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getPaymentInfoId() {
        return paymentInfoId;
    }

    public void setPaymentInfoId(Long paymentInfoId) {
        this.paymentInfoId = paymentInfoId;
    }

    public Long getStatusfile() {
        return statusfile;
    }

    public void setStatusfile(Long statusfile) {
        this.statusfile = statusfile;
    }

    public Long getStatuspayment() {
        return statuspayment;
    }

    public void setStatuspayment(Long statuspayment) {
        this.statuspayment = statuspayment;
    }

    public String getPaymentPerson() {
        return paymentPerson;
    }

    public void setPaymentPerson(String paymentPerson) {
        this.paymentPerson = paymentPerson;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Long getFeeId() {
        return feeId;
    }

    public void setFeeId(Long feeId) {
        this.feeId = feeId;
    }

    public Long getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getPaymentConfirm() {
        return paymentConfirm;
    }

    public void setPaymentConfirm(String paymentConfirm) {
        this.paymentConfirm = paymentConfirm;
    }

    public String getPaymentActionUser() {
        return paymentActionUser;
    }

    public void setPaymentActionUser(String paymentActionUser) {
        this.paymentActionUser = paymentActionUser;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public Date getDateConfirm() {
        return dateConfirm;
    }

    public void setDateConfirm(Date dateConfirm) {
        this.dateConfirm = dateConfirm;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public Long getPhase() {
        return phase;
    }

    public void setPhase(Long phase) {
        this.phase = phase;
    }
    
}
