/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.cosmetic.BO.TmpBill;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.StringUtils;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.search.SearchBillModel;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class BillDAO extends GenericDAOHibernate<Bill, Long> {

    public BillDAO() {

        super(Bill.class);
    }

    @Override
    public void saveOrUpdate(Bill obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }
        getSession().flush();
    }

    public List getListRequestPayment(Long userId,
            SearchBillModel model, int activePage, int pageSize,
            boolean getSize, boolean bCreatorId) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM Bill d WHERE d.status is not null ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM Bill d WHERE d.status is not null  ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        if (bCreatorId) {
            docHQL.append(" and d.creatorId=? ");
            docParams.add(userId);
        }
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }

            if (model.getCreatetorName() != null && model.getCreatetorName().trim().length() > 0) {
                docHQL.append(" AND lower(d.createtorName) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getCreatetorName()));
            }

            if (model.getTypePayment() != null) {
                docHQL.append(" AND d.paymentTypeId = ?");
                docParams.add(model.getTypePayment());
            }
            if (model.getBillId() != null) {
                docHQL.append(" AND d.billId = ?");
                docParams.add(model.getBillId());
            }
            if (model.getPaymentId() != null) {
                docHQL.append(" AND d.billId IN (SELECT p.billId FROM PaymentInfo p WHERE 1=1 AND p.paymentInfoId = ?)");
                docParams.add(model.getPaymentId());
            }

        }

        docHQL.append(" order by d.billId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        List<Bill> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    public boolean checkExistBill(TmpBill obj) {
        StringBuilder hql = new StringBuilder("select count(b) from Bill b where 1=1 ");
        List lstParams = new ArrayList();
        if (obj.getBillNo() != null) {
            hql.append(" and r.code = ?");
            lstParams.add(obj.getBillNo());
        }

        Query query = session.createQuery(hql.toString());
        for (int i = 0; i < lstParams.size(); i++) {
            query.setParameter(i, lstParams.get(i));
        }
        Long count = (Long) query.uniqueResult();
        return count == 0L;
    }
}
