/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.importOrder.BO.VImportOrderPaymentInfo;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.utils.Constants;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Linhdx
 */
public class PaymentInfoDAO extends GenericDAOHibernate<PaymentInfo, Long> {

    public PaymentInfoDAO() {
        super(PaymentInfo.class);
    }

    @Override
    public void saveOrUpdate(PaymentInfo obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }
        getSession().flush();
    }

    public List<PaymentInfo> getListPaymentBillId(Long BillId) {
        Query query = getSession().createQuery("select a from PaymentInfo a where a.billId = ? AND a.isActive = 1");
        query.setParameter(0, BillId);
        return query.list();
    }

    public List<PaymentInfo> getListPayment(Long fileId, Long phase) {
        Query query = getSession().createQuery("select a from PaymentInfo a where a.fileId = ? and a.phase = ? AND a.isActive = 1");
        query.setParameter(0, fileId);
        query.setParameter(1, phase);
        return query.list();
    }

    public void deletePayment(Long fileId, Long phase) {
        Query query = getSession().createQuery("Update PaymentInfo a  set a.isActive = 0 where a.status = 1 and a.fileId = ? and a.phase = ?");
        query.setParameter(0, fileId);
        query.setParameter(1, phase);
        query.executeUpdate();
    }

    public int checkPayment(Long fileId) {
        Query query = getSession().createQuery("select a from PaymentInfo a "
                + "where a.fileId = ? "
                + "and a.phase = ? AND a.isActive = 1");
        query.setParameter(0, fileId);
        query.setParameter(1, Constants.PAYMENT.PHASE.EVALUATION);
        List lst = query.list();
        if (lst == null || lst.isEmpty()) {
            return Constants.CHECK_VIEW.NOT_VIEW;
        } else {
            return Constants.CHECK_VIEW.VIEW;
        }
    }

    @Override
    public void update(PaymentInfo o) {
        super.update(o); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Session getSession() {
        return super.getSession(); //To change body of generated methods, choose Tools | Templates.
    }

    public List<PaymentInfo> getListPayedPayment(Long fileId, Long phase) {
        Query query = getSession().createQuery("select a from PaymentInfo a where a.fileId = ? and a.phase = ? AND a.isActive = 1");
        query.setParameter(0, fileId);
        query.setParameter(1, phase);
        return query.list();
    }

    public List<PaymentInfo> getListPayment(Long fileId) {
        Query query = getSession().createQuery("select a from PaymentInfo a where a.fileId = ? AND a.isActive = 1");
        query.setParameter(0, fileId);
        return query.list();
    }

    //Thanhdv 9/5
    //Thông tin chi tiết fee phase #1
    public List<PaymentInfo> getListFeePayment(Long fileId) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                " SELECT d FROM PaymentInfo d WHERE d.isActive = 1 And d.phase <>1 AND d.fileId =:fileId ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter("fileId", fileId);
        List<PaymentInfo> listObj = fileQuery.list();

        return listObj;
    }

    public List<PaymentInfo> getListPaymentByStatus(Long fileId, Long status) {
        Query query = getSession().createQuery("select a from PaymentInfo a where a.fileId = ? AND a.status = ? AND a.isActive = 1");
        query.setParameter(0, fileId);
        query.setParameter(1, status);
        return query.list();
    }

    public List<PaymentInfo> getListPaymentByStatus(Long fileId, Long status, Long phase) {
        Query query = getSession().createQuery("select a from PaymentInfo a where a.fileId = ? AND a.status = ? AND a.phase = ? AND a.isActive = 1");
        query.setParameter(0, fileId);
        query.setParameter(1, status);
        query.setParameter(2, phase);
        return query.list();
    }
//     public PaymentInfo getListPaymentInfoID(Long infoId){
//        //Query query = getSession().createQuery("select a from PaymentInfo a where a.paymentInfoId = ? ");
//        //query.setParameter(0, infoId);
//         
//        return findById(infoId);
//    }

    public List<PaymentInfo> getListPaymentAll() {
        Query query = getSession().createQuery("select a from PaymentInfo a where a.status = 1 and a.isActive = 1");
        return query.list();
    }
    
    public List<PaymentInfo> getListPaymentbynswFileCode(String nswFileCode) {
        Query query = getSession().createQuery("select a from PaymentInfo a, Files b where a.fileId = b.fileId and a.isActive = 1 "
                + "and b.isActive = 1 and b.nswFileCode = ?");
        query.setParameter(0, nswFileCode);
        return query.list();
    }
}
