package com.viettel.module.payment.controller;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.module.payment.BO.Bill;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.module.payment.search.SearchBillModel;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.databind.BindingListModelList;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author ChucHV
 */
public class AcceptBillController extends PaymentController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDayBill")
    private Datebox dbFromDayBill;
    @Wire("#incSearchFullForm #dbToDayBill")
    private Datebox dbToDayBill;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #createtorName")
    private Textbox createtorName;
    @Wire("#incSearchFullForm #paymentId")
    private Textbox paymentId;
    @Wire("#incSearchFullForm #billId")
    private Textbox billId;
    @Wire("#incSearchFullForm #lboxTypePayment")
    private Listbox lboxTypePayment;
    @Wire
    private Groupbox gbPaymentInfo, gbPaymentBill;
    @Wire
    private Button btnListPayment, btnListBill, btnListSearch;
    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire
    private Listbox lbListBill;
    @Wire
    private Window rapidAcceptPaymentBillAll;
    private int _totalSize = 0;
    private SearchBillModel lastSearchModel;
    private final Long deptId = getDeptId();
    private Boolean viewPayment = true;
    //private  String cssButtonChoice="background-color: #2b6fc2;border: medium none;border-radius: 2px;color: #ffffff;	display: inline-block;font-size: 11px;font-weight: bold;height: 30px;margin-right: 3px;margin-top: 3px;";

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        onSearch();
    }

    /*
     * isPaging: neu chuyen page thi khong can lay _totalSize
     */
    /**
     *
     * @param userId
     * @param searchBillModel
     * @param isPaging
     */
    private void reloadModel(Long userId, SearchBillModel searchBillModel, boolean isPaging) {

        searchBillModel.setMenuType(Constants.DOCUMENT_MENU.ALL);
        BillDAO objDAOHE = new BillDAO();
        List listRapidTest = objDAOHE.getListRequestPayment(userId,
                searchBillModel, userPagingTop.getActivePage(),
                userPagingTop.getPageSize(), false, false);

        if (!isPaging) {
            List result = objDAOHE.getListRequestPayment(userId,
                    searchBillModel, userPagingTop.getActivePage(),
                    userPagingTop.getPageSize(), true, false);
            if (result.isEmpty()) {
                _totalSize = 0;
            } else {
                _totalSize = ((Long) result.get(0)).intValue();
            }
        }

        userPagingTop.setTotalSize(_totalSize);
        userPagingBottom.setTotalSize(_totalSize);
        BindingListModelList model = new BindingListModelList<>(listRapidTest, true);

        lbListBill.setModel(model);


        //item.setVisible(false);
        //lbList.renderAll();
    }

    @Listen("onAfterRender = #lbListBill")
    public void onAfterRender() {
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(deptId, lastSearchModel, true);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #rapidAcceptPaymentBillAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(deptId, lastSearchModel, false);
        }
    }

    @Listen("onShowFullSearch = #rapidAcceptPaymentBillAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    @Listen("onChangeTime=#rapidAcceptPaymentBillAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            dbFromDayBill.setValue(model.getFromDate());
            dbToDayBill.setValue(model.getToDate());
            onSearch();
        }
    }

    @Listen("onClick=#btnListPayment")
    public void onClickListPayment() {
        gbPaymentInfo.setVisible(true);
        gbPaymentBill.setVisible(false);

    }

    @Listen("onClick=#btnListBill")
    public void onClickListBill() {
        gbPaymentInfo.setVisible(false);
        gbPaymentBill.setVisible(true);
    }

    @Listen("onReLoadPage = #rapidAcceptPaymentBillAll")
    public void onReLoadPage() {
        lastSearchModel = new SearchBillModel();
        lastSearchModel.setCreateDateFrom(dbFromDayBill.getValue());
        lastSearchModel.setCreateDateTo(dbToDayBill.getValue());
        //lastSearchModel.setBillId(billId.getValue());
        lastSearchModel.setCreatetorName(createtorName.getValue());

        if (lboxTypePayment.getSelectedItem() != null) {
            String value = lboxTypePayment.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != -1) {
                    lastSearchModel.setTypePayment(valueL);
                }
            }
        }
        // xy ly tim kiem theo paymentId

        if (paymentId != null) {
            String value;
            Long valueL;
            try {
                value = paymentId.getValue();
            } catch (Exception ex) {
                value = null;
                LogUtils.addLogDB(ex);

            }
            if (value != null) {
                try {
                    valueL = Long.valueOf(value);
                } catch (NumberFormatException ex) {
                    LogUtils.addLogDB(ex);
                    valueL = null;
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    valueL = null;
                }
                if (valueL != null) {
                    lastSearchModel.setPaymentId(valueL);
                }
            }


        }


        if (billId != null) {
            String value;
            Long valueL;
            try {
                value = billId.getValue();
            } catch (Exception ex) {
                value = null;
                LogUtils.addLogDB(ex);

            }
            if (value != null) {
                try {
                    valueL = Long.valueOf(value);
                } catch (NumberFormatException ex) {
                    LogUtils.addLogDB(ex);
                    valueL = null;
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    valueL = null;
                }
                if (valueL != null) {
                    lastSearchModel.setBillId(valueL);
                }
            }


        }
        reloadModel(deptId, lastSearchModel, false);
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        lastSearchModel = new SearchBillModel();
        lastSearchModel.setCreateDateFrom(dbFromDayBill.getValue());
        lastSearchModel.setCreateDateTo(dbToDayBill.getValue());
        //lastSearchModel.setBillId(billId.getValue());
        lastSearchModel.setCreatetorName(createtorName.getValue());

        if (lboxTypePayment.getSelectedItem() != null) {
            String value = lboxTypePayment.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                    lastSearchModel.setTypePayment(valueL);
                }
            }
        }
        // xy ly tim kiem theo paymentId

        if (paymentId != null) {
            String value;
            Long valueL;
            try {
                value = paymentId.getValue();
            } catch (Exception ex) {
                value = null;
                LogUtils.addLogDB(ex);

            }
            if (value != null) {
                try {
                    valueL = Long.valueOf(value);
                } catch (NumberFormatException ex) {
                    LogUtils.addLogDB(ex);
                    valueL = null;
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    valueL = null;
                }
                if (valueL != null) {
                    lastSearchModel.setPaymentId(valueL);
                }
            }


        }
        userPagingTop.setActivePage(0);
        userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        reloadModel(deptId, lastSearchModel, false);
    }

    private void createWindowView(Bill feePaymentInfo, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        // arguments.put("feePaymentInfoId", feePaymentInfo.getPaymentInfoId());
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        Window window = createWindow("windowView", "/Pages/rapidTest/include/paymentConfirm.zul",
                arguments, Window.POPUP);
        window.doModal();

    }

    @Listen("onOpenUpdateBill = #lbListBill")
    public void onOpenUpdateBill(Event event) {
        Bill objBill = (Bill) event.getData();

        Map<String, Object> arguments = new ConcurrentHashMap<>();
        // arguments.put("listVRtPaymentInfo", listVRtPaymentInfo);
        arguments.put("TypeBill", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
        // 2 sửa
        // 1 tao moi
        arguments.put("objBill", objBill);
        arguments.put("parentWindow", rapidAcceptPaymentBillAll);
        Window window = createWindow("windowsEditPaymentBill", "/Pages/module/payment/rapidTestEditBillAll.zul",
                arguments, Window.MODAL);
    window.setMode(Window.Mode.MODAL);
        window.doModal();
    }

    @Listen("onClick = #btnCreate")
    public void onBtnCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        // arguments.put("listVRtPaymentInfo", listVRtPaymentInfo);
        arguments.put("TypeBill", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_MOT_HO_SO);
        // 2 sửa
        // 1 tao moi
        arguments.put("parentWindow", rapidAcceptPaymentBillAll);
        Window window = createWindow("windowView", "/Pages/module/payment/rapidTestEditBillAll.zul",
                arguments, Window.EMBEDDED);
        window.setMode(Window.Mode.EMBEDDED);
        window.doModal();
    }

    @Listen("onSave = #rapidAcceptPaymentBillAll")
    public void onSave(Event event) {

        rapidAcceptPaymentBillAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
    }

    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi
     * windowDocIn len Con cac su kien save, update thi da xu li hien thi
     * windowDocIn trong phuong thuc onSave
     */
    @Listen("onVisible = #rapidAcceptPaymentBillAll")
    public void onVisible() {
        reloadModel(deptId, lastSearchModel, false);
        rapidAcceptPaymentBillAll.setVisible(true);
    }

    @Listen("onOpenViewBill = #lbListBill")
    public void onOpenViewBill(Event event) throws FileNotFoundException {
        Bill obj = (Bill) event.getData();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(obj.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }

    @Listen("onOpenBill = #lbListBill")
    public void onOpenBill(Event event) throws FileNotFoundException {
        Bill objBill = (Bill) event.getData();
        if (objBill.getStatus() == ConstantsPayment.PAYMENT.PAY_NEW) {
            Map<String, Object> arguments = new ConcurrentHashMap<>();
            // arguments.put("listVRtPaymentInfo", listVRtPaymentInfo);
            //arguments.put("TypeBill", 2);
            // 2 sửa
            // 1 tao moi
            arguments.put("objBill", objBill);
            arguments.put("parentWindow", rapidAcceptPaymentBillAll);
            Window window = createWindow("windowsViewAcceptBill", "/Pages/module/payment/ViewAcceptBillAll.zul",
                    arguments, Window.MODAL);
            window.setMode(Window.Mode.MODAL);
            window.doModal();
        } else {
            showNotification(Constants.Notification.BILL_CONFIRM, Constants.Notification.INFO);
        }

    }

    @Listen("onPrintBill = #lbListBill")
    public void onPrintBill(Event event) throws FileNotFoundException {
        Bill objBill = (Bill) event.getData();
        if (objBill.getStatus() == ConstantsPayment.PAYMENT.PAY_NEW) {
            ExportFileDAO exportFileDAO = new ExportFileDAO();
            exportFileDAO.exportBillPDF(objBill);

        } else {
            showNotification(Constants.Notification.BILL_CONFIRM, Constants.Notification.INFO);
        }

    }

    @Listen("onDeleteBill = #lbListBill")
    public void onDeleteBill(Event event) {
        final Bill obj = (Bill) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.BILL);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
            @Override
            public void onEvent(Event event) {
                if (null != event.getName()) {
                    switch (event.getName()) {
                        case Messagebox.ON_OK:
                            // OK is clicked
                            try {
                                AttachDAOHE attDAOHE = new AttachDAOHE();
                                List<Attachs> items = attDAOHE.getByObjectId(obj.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
                                if ((items != null) && (items.size() > 0)) {
                                    Attachs item = items.get(0);
                                    attDAOHE.delete(item);
                                }
                                //xóa bill
                                BillDAO objBillDaohe = new BillDAO();
                                objBillDaohe.delete(obj);
                                onSearch();
                                showNotification(String.format(Constants.Notification.DELETE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.BILL), Constants.Notification.INFO);
                            } catch (Exception e) {
                                showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.BILL), Constants.Notification.ERROR);
                                LogUtils.addLogDB(e);
                            } finally {
                            }
                            break;
                        case Messagebox.ON_NO:
                            break;
                    }
                }
            }
        });
    }

    public boolean isCRUDMenu() {
        return true;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    

    public String getFormatDate(Date mDate) {
        String sTemp = "";
        if (mDate != null) {
            sTemp = CommonFns.formatDate(mDate, "dd/MM/yyyy");
        }
        return sTemp;
    }

    public boolean isAbleToEditAndDelete(Bill obj) {
        if (obj != null) {
            if ((obj.getStatus() != null) && (obj.getStatus() == ConstantsPayment.PAYMENT.PAY_NEW)) {
                return true;
            }
        }
        return false;
    }

 
}
