package com.viettel.module.payment.controller;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.utils.LogUtils;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.module.payment.search.SearchPaymentModel;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.Constants;
import com.viettel.utils.DateTimeUtils;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
/**
 *
 * @author ChucHV
 */
public class AcceptPaymentInfoController extends PaymentController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    @Wire("#incSearchFullForm #dbFromDayPayment")
    private Datebox dbFromDayPayment;
    @Wire("#incSearchFullForm #dbToDayPayment")
    private Datebox dbToDayPayment;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #brandName")
    private Textbox brandName;
    @Wire("#incSearchFullForm #productname")
    private Textbox productname;
    @Wire("#incSearchFullForm #lboxStatusPayment")
    private Listbox lboxStatusPayment;
    @Wire("#incSearchFullForm #lboxStatusFile")
    private Listbox lboxStatusFile;
    @Wire("#incSearchFullForm #lboxOrder")
    private Listbox lboxOrder;
   
  
    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire
    private Listbox lbList;
    @Wire
    private Window rapidAcceptPaymentInfoAll;
    private int _totalSize = 0;
    private SearchPaymentModel lastSearchModel;
    private final Long deptId = getDeptId();
    private Boolean viewPayment = true;
    

   
    //private  String cssButtonChoice="background-color: #2b6fc2;border: medium none;border-radius: 2px;color: #ffffff;	display: inline-block;font-size: 11px;font-weight: bold;height: 30px;margin-right: 3px;margin-top: 3px;";
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
          Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);

        CategoryDAOHE cdhe = new CategoryDAOHE();

        ListModelArray lstModelProcedure;
        List<Category> lstStatus = cdhe.findAllCategorySearchValue(Constants.CATEGORY_TYPE.FILE_STATUS,true);
        
        lstModelProcedure = new ListModelArray(lstStatus);

        lboxStatusFile.setModel(lstModelProcedure);
        
        // trang thai thanh toan

        lstStatus = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.PAYMENT_STATUS,true);
        
        
        lstModelProcedure = new ListModelArray(lstStatus);
        lboxStatusPayment.setModel(lstModelProcedure);
        
        dbFromDayPayment.setValue(DateTimeUtils.addDay(new Date(), -14));
        dbToDayPayment.setValue(new Date());
        onSearch();

    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo); //To change body of generated methods, choose Tools | Templates.
    }

    /*
     * isPaging: neu chuyen page thi khong can lay _totalSize
     */
    List<VCosPaymentInfo> listRapidTest;
    private Users user;

    private void reloadModel(SearchPaymentModel searchModel) {

        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm;
        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(), start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel, getUserId(), getDeptId(), start, take);
        }
        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        listRapidTest=plm.getLstReturn();
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstModel.setMultiple(true);
        lbList.setModel(lstModel);


    }
// cac ham luong

    

    @Listen("onAfterRender = #lbList")
    public void onAfterRender() {
        //lbList.getItemAtIndex(0)
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #rapidAcceptPaymentInfoAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch = #rapidAcceptPaymentInfoAll")
    public void onShowFullSearch() {

        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    @Listen("onShowListPayment = #rapidAcceptPaymentInfoAll")
    public void onShowListPayment() {
    }

    @Listen("onShowListBill = #rapidAcceptPaymentInfoAll")
    public void onShowListBill() {
    }

    @Listen("onChangeTime=#rapidAcceptPaymentInfoAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            dbFromDayPayment.setValue(model.getFromDate());
            dbToDayPayment.setValue(model.getToDate());
            onSearch();
        }
    }

    @Listen("onClick=#btnListSearch")
    public void onClickListSearch() {

        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }

    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        lastSearchModel = new SearchPaymentModel();
        lastSearchModel.setCreateDateFrom(dbFromDayPayment.getValue());
        lastSearchModel.setCreateDateTo(dbToDayPayment.getValue());
        
        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        lastSearchModel.setBrandName(brandName.getValue());
        lastSearchModel.setProductName(productname.getValue());
        
        
        if (lboxStatusPayment.getSelectedItem() != null) {

            String value = lboxStatusPayment.getSelectedItem().getValue();

            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != -1) {
                    lastSearchModel.setStatusPayment(valueL);
                }
            }
        }
        if (lboxStatusFile.getSelectedItem() != null) {
            String value = lboxStatusFile.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != -1) {
                    lastSearchModel.setStatusFile(valueL);
                }
            }
        }
//        if (lboxDocumentTypeCode.getSelectedItem() != null) {
//            String value = lboxDocumentTypeCode.getSelectedItem().getValue();
//            if (value != null) {
//                Long valueL = Long.valueOf(value);
//                if (valueL != -1) {
//                    lastSearchModel.setDocumentTypeCode(valueL);
//                }
//            }
//        }

        if (lboxOrder.getSelectedItem() != null) {
            String value = lboxOrder.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != -1) {
                    lastSearchModel.setTypeOrder(valueL);
                }
            }
        }
        userPagingTop.setActivePage(0);
        userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        reloadModel( lastSearchModel);
    }

    @Listen("onReLoadPage = #rapidAcceptPaymentInfoAll")
    public void onReloadPage() {
        onSearch();
    }

    @Listen("onSave = #rapidAcceptPaymentInfoAll")
    public void onSave(Event event) {

        rapidAcceptPaymentInfoAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
    }

    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi
     * windowDocIn len Con cac su kien save, update thi da xu li hien thi
     * windowDocIn trong phuong thuc onSave
     */
    @Listen("onVisible = #rapidAcceptPaymentInfoAll")
    public void onVisible() {
        reloadModel(lastSearchModel);
        rapidAcceptPaymentInfoAll.setVisible(true);
    }

    @Listen("onOpenView = #lbList")
    public void onOpenView(Event event) throws FileNotFoundException {
        VCosPaymentInfo obj = (VCosPaymentInfo) event.getData();
        PaymentInfoDAO objPaymentDaohe = new PaymentInfoDAO();
        PaymentInfo paymentInfo = objPaymentDaohe.findById(obj.getPaymentInfoId());
        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(paymentInfo.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);

    }

    @Listen("onOpenBill = #lbList")
    public void onOpenBill(Event event) throws FileNotFoundException {
        VCosPaymentInfo obj = (VCosPaymentInfo) event.getData();
        BillDAO objDAOHE = new BillDAO();
        Bill objBill = objDAOHE.findById(obj.getBillId());
        if (objBill != null) {
            Map<String, Object> arguments = new ConcurrentHashMap<>();
            arguments.put("TypeBill", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
            // 2 sửa
            // 1 tao moi
            arguments.put("objBill", objBill);
            arguments.put("parentWindow", rapidAcceptPaymentInfoAll);
            Window window = createWindow("windowsViewPaymentBill", "/Pages/module/payment/ViewBillAll.zul",
                    arguments, Window.MODAL);
            window.setMode(Window.Mode.MODAL);
            window.doModal();
        }
    }

    @Listen("onAccept = #lbList")
    public void onAccept(Event event) {
        final VCosPaymentInfo obj = (VCosPaymentInfo) event.getData();
        BillDAO objDAOHE = new BillDAO();
        Bill objBill = objDAOHE.findById(obj.getBillId());
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("TypeAcceptBill", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
        // 2 sửa
        // 1 tao moi
        arguments.put("objBill", objBill);
        arguments.put("parentWindow", rapidAcceptPaymentInfoAll);
        Window window = createWindow("businessWindow", "/Pages/module/payment/ViewAcceptBillAll.zul",
                arguments, Window.MODAL);
        
        window.doModal();
//        String message = String.format(Constants.Notification.APROVE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
//        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
//                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
//            @Override
//            public void onEvent(Event event) {
//                if (null != event.getName()) {
//                    switch (event.getName()) {
//                        case Messagebox.ON_OK:
//                            // OK is clicked
//                            try {
//                                PaymentInfoDAO objPaymentDaohe = new PaymentInfoDAO();
//                                PaymentInfo paymentInfo = objPaymentDaohe.findById(obj.getPaymentInfoId());
//                                paymentInfo.setStatus(Constants.RAPID_TEST.PAYMENT.PAY_CONFIRMED); // trang thai moi tao
//                                paymentInfo.setDateConfirm(new Date());
//                                paymentInfo.setPaymentActionUser(getUserName());
//                                paymentInfo.setPaymentActionUserId(getUserId());
//                                objPaymentDaohe.saveOrUpdate(paymentInfo);
//                                objPaymentDaohe.flush();
//                                onSearch();
//                                showNotification(String.format(Constants.Notification.APROVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.INFO);
//                            } catch (Exception e) {
//                                showNotification(String.format(Constants.Notification.APROVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
//                                LogUtils.addLogDB(e);
//                            } finally {
//                            }
//                            return;
//                        case Messagebox.ON_NO:
//                            return;
//                    }
//                }
//            }
//        });
    }

    @Listen("onReject = #lbList")
    public void onReject(Event event) {
        final VCosPaymentInfo obj = (VCosPaymentInfo) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("VCosPaymentInfo", obj);
        arguments.put("menuType", Constants.DOCUMENT_MENU.ALL);
        arguments.put("windowsType", 1);
        arguments.put("parentWindow", rapidAcceptPaymentInfoAll);
        Window window = createWindow("windowsRejectPaymentConfirm", "/Pages/module/payment/rejectPaymentConfirm.zul",
                arguments, Window.MODAL);
        window.doModal();
    }

    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(VCosPaymentInfo obj) {
        if ((obj.getStatuspayment() == null) || (obj.getStatuspayment() == ConstantsPayment.PAYMENT.PAY_NEW)) {
            return false;
        } else {
            return true;
        }

    }

    public boolean isAbleAccept(VCosPaymentInfo obj) {
        if (obj != null) {
            if ((obj.getStatuspayment() != null) && (obj.getStatuspayment() == ConstantsPayment.PAYMENT.PAY_ALREADY)) {
                return true;
            }
        }
        return false;

    }
  
    
}
