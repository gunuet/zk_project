package com.viettel.module.payment.controller;

import com.viettel.core.base.DAO.AttachDAO;

import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.module.payment.BO.Bill;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.utils.LogUtils;

/**
 *
 * @author Linhdx
 */
public class ApproveKtController extends PaymentController {

    @Wire
    private Listbox lbListPaymented;
    @Wire
    private Textbox tbPaymentName;
    @Wire
    private Window businessWindow;
    @Wire
    Radiogroup rgTypePayment;
    @Wire
    Radio keypay, banktransfer, immediaacy;
    Bill objBill;
    @Wire
    private Label lbWarning;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    @Wire
    Groupbox gbTypePayment;
    private Window parentWindow;
    Long lsumMoney = 0L;
    // danh cho upload file
    @Wire
    Textbox txtBillNo, txtBillKey;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    Iframe iframeFile;
    @Wire
    private Textbox txtPaymentConfirm;
    @Wire
    Textbox txtValidate;
    private Media media;
    Long typePayment = 0L; // 1 keypay //2 chuyen khoan // 3 truc tiep
    boolean bUploadFile = false, clickDeletefile = false;
    private final Long deptId = getDeptId();
// xu ly luong
    @Wire
    Groupbox gbReject;
    // khai bao xu ly luong

    private String type;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("windowParent");
        objBill = (Bill) arguments.get("objBill");
        type = (String) arguments.get("type");
        lsumMoney = objBill.getTotal();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onAfterRender = #lbListPaymented")
    public void onAfterRender() {
    }
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    ListModelList<VCosPaymentInfo> lbListPaymentedModelList = new ListModelList<VCosPaymentInfo>();

    public ListModelList<VCosPaymentInfo> getListPaymentedModelList() {
        return lbListPaymentedModelList;
    }

    public void setListPaymentedModelList(ListModelList<VCosPaymentInfo> lbListPaymentedModelList) {
        this.lbListPaymentedModelList = lbListPaymentedModelList;
    }
    private List<VCosPaymentInfo> listPaymented;

    private void reloadModel() throws IOException {

        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();

        listPaymented = objDAOHE.getListRequestPaymentBillId(deptId, objBill.getBillId());
        if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
            rgTypePayment.setSelectedItem(keypay);
        } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
            rgTypePayment.setSelectedItem(banktransfer);

        } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
            rgTypePayment.setSelectedItem(immediaacy);
            txtBillKey.setValue(objBill.getBillSign());
            txtBillNo.setValue(objBill.getCode());
        }
        dbDayPayment.setValue(objBill.getPaymentDate());
        tbPaymentName.setValue(objBill.getCreatetorName());

        setTypePayment();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
        if ((items != null) && (items.size() > 0)) {
            try {
                //AMedia amedia = new AMedia(mAttach.getAttachName(), sType, WorkflowAPI.getCtypeFile(sType), is);
                //iframeFile.setContent(amedia);
                txtFileName.setValue(items.get(0).getAttachName());
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
        }
        lbListPaymentedModelList.addAll(listPaymented);
        lbListPaymented.setModel(lbListPaymentedModelList);

    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        reloadModel();
        lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));
        txtValidate.setValue("0");
        if ("0".equals(type)) {
            gbReject.setVisible(true);
        }
    }

    @Listen("onClick = #btnAccept")
    public void onAccept() {

        PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
        PaymentInfo paymentInfo;
        int sizeList = listPaymented.size();
        for (int i = 0; i < sizeList; i++) {

            VCosPaymentInfo app = listPaymented.get(i);
            paymentInfo = objFreePaymentInfo.findById(app.getPaymentInfoId());
            paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED); // trang thai moi tao
            paymentInfo.setDateConfirm(new Date());
            paymentInfo.setPaymentActionUser(getUserName());
            paymentInfo.setPaymentActionUserId(getUserId());
            objFreePaymentInfo.saveOrUpdate(paymentInfo);
            //System.out.print("gia tri"+app.getRapidTestNo());
        }
        BillDAO objBillDAOHE = new BillDAO();
        objBill.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED);
        objBill.setConfirmDate(new Date());
        objBill.setConfirmUserId(getUserId());
        objBill.setConfirmUserName(getUserName());
        objBillDAOHE.saveOrUpdate(objBill);
        objBillDAOHE.flush();
        showNotification(String.format(Constants.Notification.APROVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.BILL), Constants.Notification.INFO);
        businessWindow.onClose();

    }

    @Listen("onClick = #btnReject")
    public void onReject() {
        PaymentInfoDAO objPaymentDaohe = new PaymentInfoDAO();
        int size = listPaymented.size();
        PaymentInfo TempPaymentInfo = null;
        if (size > 0) {
            TempPaymentInfo = objPaymentDaohe.findById(listPaymented.get(0).getPaymentInfoId());
        }
        for (int i = 0; i < size; i++) {
            PaymentInfo paymentInfo = objPaymentDaohe.findById(listPaymented.get(i).getPaymentInfoId());
            paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_REJECTED); // trang thai moi tao
            paymentInfo.setDateConfirm(new Date());
            paymentInfo.setPaymentActionUser(getUserName());
            paymentInfo.setPaymentActionUserId(getUserId());
            paymentInfo.setPaymentConfirm(txtPaymentConfirm.getText().trim());
            //objPaymentDaohe.saveOrUpdate(paymentInfo);
            objPaymentDaohe.getSession().merge(paymentInfo);

        }
        //windowsRejectPaymentConfirm.onClose();
        if (TempPaymentInfo != null) {
            BillDAO objDAOHE = new BillDAO();
            Bill objBill = objDAOHE.findById(TempPaymentInfo.getBillId());
            objBill.setStatus(ConstantsPayment.PAYMENT.PAY_REJECTED);
            objBill.setConfirmDate(new Date());
            objBill.setConfirmUserId(getUserId());
            objBill.setConfirmUserName(getUserName());
            objDAOHE.saveOrUpdate(objBill);
        }

        objPaymentDaohe.getSession().flush();
    }

    @Listen("onClick = #btnCancel")
    public void onCancel() {
        businessWindow.onClose();
        //Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onClosePage = #windowsViewAcceptBill")
    public void onClosePage() {
        businessWindow.onClose();
        if (parentWindow != null) {
            Events.sendEvent("onReLoadPage", parentWindow, null);
        }
        //Events.sendEvent("onVisible", parentWindow, null);
    }

    private void setTypePayment() {
        if (rgTypePayment.getSelectedItem() != null) {
            if ("keypay".equals(rgTypePayment.getSelectedItem().getId())) {
                //gbTypePayment.setVisible(false);
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
            } else if ("immediaacy".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;
                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(true);
                txtBillKey.setVisible(true);
                lbBillNo.setVisible(true);
                lbBillNoRequi.setVisible(true);
                lbBillKey.setVisible(true);
                lbBillKeyRequi.setVisible(true);

            } else if ("banktransfer".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;
                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(false);
                txtBillKey.setVisible(false);
                lbBillNo.setVisible(false);
                lbBillNoRequi.setVisible(false);
                lbBillKey.setVisible(false);
                lbBillKeyRequi.setVisible(false);
            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        setTypePayment();
        //txtSearchDeptName.setValue("Selection Made: "+rgTypePayment.getSelectedItem().getId());
    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
    }

    @Listen("onClick = #btnDownload")
    public void onDownload() throws IOException {

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        if ("1".equals(type)) {

            onAccept();
            txtValidate.setValue("1");
            if (parentWindow != null) {
                Events.sendEvent("onClosePage", parentWindow, null);
            }
        } else {
            if (isValidate()) {
                onReject();
                txtValidate.setValue("1");
                if (parentWindow != null) {
                    Events.sendEvent("onClosePage", parentWindow, null);
                }
            }
        }

    }

    private boolean isValidate() {
        String message;
        if ("".equals(txtPaymentConfirm.getText().trim())) {
            message = "Không được để trống lí do từ chối";
            txtPaymentConfirm.focus();
            setWarningMessage(message);
            return false;
        }
        return true;
    }
}
