package com.viettel.module.payment.controller;

import com.viettel.core.base.DAO.AttachDAO;

import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;

import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.ConvertFileUtils;
import com.viettel.utils.FileUtil;
import com.viettel.module.payment.BO.Bill;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.utils.ResourceBundleUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.*;

/**
 *
 * @author Linhdx
 */
public class EditPaymentBillController extends PaymentController {

    @Wire
    private Listbox lbListPaymented, lbListNotPayment;
    @Wire
    private Textbox tbPaymentName;
    @Wire
    private Window editPaymentBillWindow;
    @Wire
    Radiogroup rgTypePayment;
    @Wire
    Radio keypay, banktransfer, immediaacy;
    Bill objBill;
    @Wire
    private Label lbWarning;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    @Wire
    Groupbox gbTypePayment;
    private Window parentWindow;
    Long lsumMoney = 0L;
    // danh cho upload file
    @Wire
    Textbox txtBillNo, txtBillKey;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    Image imgDelFile;
    @Wire
    Label lbdeletefile;
    @Wire
    Textbox txtValidate;
    private Media media;
    private Attachs attachCurrent;
    int typePayment = 0;
    int typeWindows = 1;
    boolean bUploadFile = false, clickDeletefile = false;
    private final Long userID = getUserId();
    // khai bao xu ly luong
    private VCosPaymentInfo vFileRtfile;
    private com.viettel.core.workflow.BO.Process processCurrent;// process dang duoc xu li
    private List<com.viettel.core.workflow.BO.Process> listProcessCurrent;
    private List<Long> listDocId, listDocType;
    private long FILE_TYPE;
    ;
    private Long DEPT_ID;
    @Wire
    private Div topToolbar;
    private List<Component> listTopActionComp;
    @Wire
    private Div bottomToolbar;
    private List<Component> listBottomActionComp;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("windowParent");

//        objBill = null;
//        lsumMoney = 0L;
//        typeWindows = ConstantsPayment.PAYMENT.PAYMENT_TAO_MOI;
//        listPaymented = new ArrayList<>();
//        listPaymented.clear();
//        listPaymented = (List<VCosPaymentInfo>) arguments.get("listVCosPaymentInfo");
//        int sizePaymentInfo = listPaymented.size();
//        for (int i = 0; i < sizePaymentInfo; i++) {
//            lsumMoney = lsumMoney + listPaymented.get(i).getCost();
//            if(listPaymented.get(i).getBillId()!=null){
//                typeWindows = ConstantsPayment.PAYMENT.PAYMENT_CHINH_SUA;
//                objBill=(new BillDAO()).findById(listPaymented.get(i).getBillId());
//            }
//        }
        typeWindows = (int) arguments.get("TypeBill");
        if (typeWindows == ConstantsPayment.PAYMENT.PAYMENT_TAO_MOI) { //them moi
            objBill = null;
            lsumMoney = 0L;
        } else if (typeWindows == ConstantsPayment.PAYMENT.PAYMENT_CHINH_SUA) { //chinh sua
            objBill = (Bill) arguments.get("objBill");
            lsumMoney = objBill.getTotal();

        }
        // listPaymented=

        return super.doBeforeCompose(page, parent, compInfo);

    }

    // khoi tao luong
    private void initThread() {
        Components.removeAllChildren(topToolbar);

        //Component tempComponet=listTopButton.get(0).getFirstChild();
//        try {
//            int sizeButton = listTopButton.size();
//            if (sizeButton > 2) {
//                for (int i = 2; i < sizeButton; i++) {
//                    Component tempComponent = listTopButton.get(i);
//                    if (tempComponent != null) {
//                        topToolbar.removeChild(tempComponent);
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            LogUtils.addLogDB(ex);
//        }
//        
        listTopActionComp = new ArrayList<>();
        listBottomActionComp = new ArrayList<>();
        listProcessCurrent = new ArrayList<>();
        listDocId = new ArrayList<>();
        listDocType = new ArrayList<>();
        if (listPaymented.size() > 0) {
            vFileRtfile = listPaymented.get(0);
            processCurrent = WorkflowAPI.getInstance().
                    getCurrentProcess(vFileRtfile.getFileId(), vFileRtfile.getFileType(),
                            vFileRtfile.getStatusfile(), getUserId());
            FILE_TYPE = vFileRtfile.getFileType();
            DEPT_ID = Constants.CUC_QLD_ID;;
            addAllNextActions();
            loadActionsToToolbar();
        }
    }
// cac ham xu ly luong

    private void addAllNextActions() {
        Long docStatus = vFileRtfile.getStatusfile();
        Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;
        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(
                docType, docStatus, deptId);

        for (NodeToNode action : actions) {
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
    }

    private Button createButtonForAction(NodeToNode action) {
        // check if action is RETURN_PREVIOUS but parent process came from another node
        if (processCurrent != null
                && action.getType() == Constants.NODE_ASSOCIATE_TYPE.RETURN_PREVIOUS
                && action.getNextId() != processCurrent.getPreviousNodeId()) {
            return null;
        }
        // check if receiverId is diferent to current user id
        // find all process having current status of document
        List<com.viettel.core.workflow.BO.Process> listCurrentProcess = WorkflowAPI.getInstance()
                .findAllCurrentProcess(vFileRtfile.getFileId(), vFileRtfile.getFileType(),
                        vFileRtfile.getStatusfile());
        Long userId = getUserId();
        Long creatorId = vFileRtfile.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return null;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                for (com.viettel.core.workflow.BO.Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                if (!needToProcess) {
                    return null;
                }
            }
        }

        Button btn = new Button(action.getAction());

        final String actionName = action.getAction();
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action

        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = vFileRtfile.getStatusfile();
        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, false));

        EventListener<Event> event = new EventListener<Event>() {
            @Override
            public void onEvent(Event t) throws Exception {
                // xu ly khi payment luong
//                Set<Listitem> listSelectedItem = lbList.getSelectedItems();
//                String message;
//                List<VCosPaymentInfo> listVCosPaymentInfo;
//                listVCosPaymentInfo = new ArrayList<VCosPaymentInfo>();

                List<VCosPaymentInfo> listVCosPaymentInfo = new ArrayList();
                if (1 <= listPaymented.size()) {
                    for (VCosPaymentInfo item : listPaymented) {

                        listVCosPaymentInfo.add(item);
                        // them list process
                        listProcessCurrent.add((WorkflowAPI.getInstance().
                                getCurrentProcess(item.getFileId(), item.getFileType(),
                                        item.getStatusfile(), getUserId())));
                        listDocId.add(item.getFileId());
                        listDocType.add(item.getFileType());
                        //System.out.print("gia tri"+app.getRapidTestNo());
                    }

                    Bill tempBill = new Bill();
                    tempBill.setCreatetorName(tbPaymentName.getText());
                    tempBill.setCreateDate(new Date());
                    tempBill.setPaymentDate(dbDayPayment.getValue());
                    tempBill.setTotal(lsumMoney);
                    tempBill.setCreatorId(getUserId());
                    tempBill.setStatus(1L);
                    tempBill.setPaymentTypeId(Long.valueOf(typePayment));
                    // neu la chuyen khoan them ma hoa don ky hieu hoa don
                    if (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                        objBill.setCode(txtBillNo.getText().trim());
                        objBill.setBillSign(txtBillKey.getText().trim());
                    }
                    Map<String, Object> data = new ConcurrentHashMap<>();
                    //data.put("typeProcess",Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);    
                    data.put("listVCosPaymentInfo", listVCosPaymentInfo);
                    data.put("menuType", Constants.DOCUMENT_MENU.ALL);
                    data.put("windowParent", editPaymentBillWindow);
                    data.put("typeProcess", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
                    data.put("fileId", vFileRtfile.getFileId());
                    data.put("tempLongId", idLongPayment);
                    data.put("tempBill", tempBill);

                    data.put("docStatus", status);
                    data.put("nextId", nextId);
                    data.put("previousId", prevId);
                    // xu li process
                    data.put("lstAvailableNDU", lstNDUs); //nut tiep theo la chung
                    data.put("actionId", actionId);
                    data.put("actionName", actionName);
                    data.put("actionType", actionType);

                    if (processCurrent != null) {
                        data.put("process", processCurrent);
                    }
                    data.put("docId", vFileRtfile.getFileId());
                    data.put("docType", vFileRtfile.getFileType());

                    data.put("listProcess", listProcessCurrent);
                    data.put("listDocId", listDocId);
                    data.put("listDocType", listDocType);
                    createWindow("businessWindow", formName, data, Window.MODAL);
                    //createWindowView(listVCosPaymentInfo, Constants.DOCUMENT_MENU.ALL,
                    //      rapidDetailPaymentInfoAll);
                    // rapidTestPaymentInfoAll.setVisible(false);
                } else {
                    String message;
                    message = "\n Bạn vui lòng chọn hồ sơ";
                    showNotification(message, Constants.Notification.INFO);
                    return;
                }
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    // Hien thi cac action tren thanh toolbar top va bottom
    public void loadActionsToToolbar() {
        for (Component comp : listTopActionComp) {
            topToolbar.appendChild(comp);
        }
        for (Component comp : listBottomActionComp) {
            bottomToolbar.appendChild(comp);
        }

    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    @Listen("onDeletePayment = #lbListPaymented")
    public void onDeletePayment(Event event) {
        VCosPaymentInfo objPaymentInfo = (VCosPaymentInfo) event.getData();

        // cach nay hoi chuoi
//       listPaymented.remove(objPaymentInfo);
//        BindingListModelList model = new BindingListModelList<>(listPaymented, true);
//        lbListPaymented.setModel(model);
//        
//        listNotPayment.add(objPaymentInfo);
//         model = new BindingListModelList<>(listNotPayment, true);
//        lbListNotPayment.setModel(model);
        listPaymented.remove(objPaymentInfo);
        listNotPayment.add(objPaymentInfo);
        lbListPaymentedModelList.remove(objPaymentInfo);
        lbListNotPaymentModelList.add(objPaymentInfo);

        // update lai trang thai tien
        lsumMoney = lsumMoney - objPaymentInfo.getCost();
        //txtSumMoney.setValue(lsumMoney.toString());
        //sumMoney.setValue("Tổng tiền: " + lsumMoney);
        lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));
    }

    @Listen("onAddPayment = #lbListNotPayment")
    public void onAddPayment(Event event) {

        VCosPaymentInfo objPaymentInfo = (VCosPaymentInfo) event.getData();

//        listPaymented.add(objPaymentInfo);
//        
//        BindingListModelList model = new BindingListModelList<>(listPaymented, true);
//        
//        lbListPaymented.setModel(model);
//        
//        listNotPayment.remove(objPaymentInfo);
//        model = new BindingListModelList<>(listNotPayment, true);
//        lbListNotPayment.setModel(model);
        //Listitem s = lbListNotPayment.getSelectedItem();
        //s.getChildren().remove(objPaymentInfo);
        //lbListNotPayment.appendChild((Component) objPaymentInfo);
        //lbListPaymented.addItemToSelection(lbListNotPayment.getItemAtIndex(0));
//               if (s == null)
//                    Messagebox.show("Select an item first");
//                else
//                    s.setParent(lbListPaymented);
//        listPaymented.add(objPaymentInfo);
//        ListModelArray lstModel = new ListModelArray(listPaymented);
//        lbListPaymented.getItems().clear();
//        lbListPaymented.clearSelection();
//        lbListPaymented.setModel(lstModel);
//        
//        
//       
//        listNotPayment.remove(objPaymentInfo);
//        lstModel= new ListModelArray(listNotPayment);
//         lbListNotPayment.getItems().clear();
//        lbListNotPayment.clearSelection();
//        lbListNotPayment.setModel(lstModel);
//    lbListNotPayment.removeChild((Component) objPaymentInfo);
//    BindUtils.postNotifyChange(null, null, lbListNotPayment, "*");
        listPaymented.add(objPaymentInfo);
        listNotPayment.remove(objPaymentInfo);
        lbListPaymentedModelList.add(objPaymentInfo);
        lbListNotPaymentModelList.remove(objPaymentInfo);
        //lbListPaymentedModelList.notify();
        // update lai trang thai tien
        lsumMoney = lsumMoney + objPaymentInfo.getCost();
        //txtSumMoney.setValue(lsumMoney.toString());
        lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));

    }
    ListModelList<VCosPaymentInfo> lbListPaymentedModelList = new ListModelList<VCosPaymentInfo>();
    ListModelList<VCosPaymentInfo> lbListNotPaymentModelList = new ListModelList<VCosPaymentInfo>();

    public ListModelList<VCosPaymentInfo> getListPaymentedModelList() {
        return lbListPaymentedModelList;
    }

    public void setListPaymentedModelList(ListModelList<VCosPaymentInfo> lbListPaymentedModelList) {
        this.lbListPaymentedModelList = lbListPaymentedModelList;
    }

    public ListModelList<VCosPaymentInfo> getListNotPaymentModelList() {
        return lbListNotPaymentModelList;
    }

    public void setListNotPaymentModelList(ListModelList<VCosPaymentInfo> lbListNotPaymentModelList) {
        this.lbListNotPaymentModelList = lbListNotPaymentModelList;
    }
    private List<VCosPaymentInfo> listPaymented, listNotPayment;
    private Long idLongPayment[];

    private void reloadModel() {

        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
        if (typeWindows == ConstantsPayment.PAYMENT.PAYMENT_CHINH_SUA) {

            listPaymented = objDAOHE.getListRequestPaymentBillId(userID, objBill.getBillId());

            if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
                rgTypePayment.setSelectedItem(keypay);
            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
                rgTypePayment.setSelectedItem(banktransfer);

            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                rgTypePayment.setSelectedItem(immediaacy);
                txtBillKey.setValue(objBill.getBillSign());
                txtBillNo.setValue(objBill.getCode());
            }
            dbDayPayment.setValue(objBill.getPaymentDate());
            tbPaymentName.setValue(objBill.getCreatetorName());

            setTypePayment();

            AttachDAOHE attDAOHE = new AttachDAOHE();
            List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
            if ((items != null) && (items.size() > 0)) {
                String path = items.get(0).getFullPathFile();
                File f = new File(path);
                try {
                    media = new AMedia(f, null, null);
                } catch (FileNotFoundException ex) {
                    LogUtils.addLogDB(ex);
                }
                txtFileName.setValue(items.get(0).getAttachName());
                imgDelFile.setVisible(true);
                lbdeletefile.setVisible(true);
            }

//            temp=listPaymented.
//            for(int i=0;i<temp.size();i++){
//            temp.add(listPaymented.get(i).clo);
//            }
        } else if (typeWindows == 1) {

            listPaymented = new ArrayList<VCosPaymentInfo>();
        }
        lbListPaymentedModelList.addAll(listPaymented);
        lbListPaymented.setModel(lbListPaymentedModelList);
        listNotPayment = objDAOHE.getListRequestPaymentNotPayment(userID);
        lbListNotPaymentModelList.addAll(listNotPayment);
        lbListNotPayment.setModel(lbListNotPaymentModelList);

    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        reloadModel();
        int size = listPaymented.size();
        idLongPayment = new Long[size];
        for (int i = 0; i < size; i++) {
            idLongPayment[i] = listPaymented.get(i).getPaymentInfoId();
        }
        txtValidate.setValue("0");
        lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));
        initThread();
    }

    @Listen("onClick = #btnAccept")
    public void onAccept() {

        if (isValidate()) {

            Date date = new Date();
            // update trong bang bill
            BillDAO objBillDAOHE = new BillDAO();
            if (typeWindows == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_MOT_HO_SO) {

                Bill objBill = new Bill();
                objBill.setCreatetorName(tbPaymentName.getText());
                objBill.setCreateDate(date);
                objBill.setPaymentDate(dbDayPayment.getValue());
                objBill.setTotal(lsumMoney);
                objBill.setCreatorId(getUserId());
                objBill.setStatus(1L);
                objBill.setPaymentTypeId(Long.valueOf(typePayment));
                // neu la chuyen khoan them ma hoa don ky hieu hoa don
                if (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                    objBill.setCode(txtBillNo.getText().trim());
                    objBill.setBillSign(txtBillKey.getText().trim());
                }
                objBillDAOHE.saveOrUpdate(objBill);
                Long idBill = objBill.getBillId();
                //update trong paymentInfo
                PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
                PaymentInfo paymentInfo;
                for (int i = 0; i < listPaymented.size(); i++) {
                    paymentInfo = objFreePaymentInfo.findById(listPaymented.get(i).getPaymentInfoId());
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_ALREADY);
                    paymentInfo.setBillId(idBill);
                    paymentInfo.setPaymentTypeId(Long.valueOf(typePayment));
                    objFreePaymentInfo.saveOrUpdate(paymentInfo);
                }
                //update attach File

                try {
                    AttachDAO attDAO = new AttachDAO();
                    attDAO.saveFileAttach(media, idBill, Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY, null);

                } catch (IOException ex) {
                    LogUtils.addLogDB(ex);
                }
            } else if (typeWindows == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO) {

                objBill.setCreatetorName(tbPaymentName.getText());
                objBill.setCreateDate(date);
                objBill.setPaymentDate(dbDayPayment.getValue());
                objBill.setTotal(lsumMoney);
                objBill.setCreatorId(getUserId());
                objBill.setStatus(1L);
                objBill.setPaymentTypeId(Long.valueOf(typePayment));
                // neu la chuyen khoan them ma hoa don ky hieu hoa don
                if (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                    objBill.setCode(txtBillNo.getText().trim());
                    objBill.setBillSign(txtBillKey.getText().trim());
                }
                objBillDAOHE.saveOrUpdate(objBill);
                Long idBill = objBill.getBillId();
                //update trong paymentInfo
                PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
                int size = idLongPayment.length;
                for (int i = 0; i < size; i++) {
                    PaymentInfo paymentInfo = objFreePaymentInfo.findById(idLongPayment[i]);
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_NEW);
                    paymentInfo.setBillId(null);
                    paymentInfo.setPaymentTypeId(null);
                    paymentInfo.setDateConfirm(null);
                    paymentInfo.setPaymentActionUser(null);
                    paymentInfo.setPaymentActionUserId(null);
                    paymentInfo.setPaymentConfirm(null);
                    objFreePaymentInfo.getSession().merge(paymentInfo);
                }
                size = listPaymented.size();
                for (int i = 0; i < size; i++) {
                    PaymentInfo paymentInfo = objFreePaymentInfo.findById(listPaymented.get(i).getPaymentInfoId());
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_ALREADY);
                    paymentInfo.setBillId(idBill);
                    paymentInfo.setPaymentTypeId(Long.valueOf(typePayment));
                    paymentInfo.setUpdateDate(date);
                    paymentInfo.setPaymentDate(dbDayPayment.getValue());
                    if (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                        paymentInfo.setBillCode(txtBillNo.getText().trim());
                        paymentInfo.setPaymentCode(txtBillKey.getText().trim());
                    }
                    objFreePaymentInfo.getSession().merge(paymentInfo);
                }
                objFreePaymentInfo.getSession().flush();
                //update attach File
                if (bUploadFile) {
                    try {
                        AttachDAO attDAO = new AttachDAO();
                        AttachDAOHE attDAOHE = new AttachDAOHE();
                        //xoa file cu
                        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
                        attDAOHE.deleteAttach(items.get(0));
                        //insert file moi
                        attDAO.saveFileAttach(media, idBill, Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY, null);

                    } catch (IOException ex) {
                        LogUtils.addLogDB(ex);
                    }
                }

            }
            txtValidate.setValue("1");
            if (parentWindow != null) {
                Events.sendEvent("onReloadPage", parentWindow, null);
            }

            editPaymentBillWindow.onClose();

        }

        //modal
        // Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onClick = #btnReject")
    public void onReject() {

        editPaymentBillWindow.onClose();
        //Events.sendEvent("onVisible", parentWindow, null);

    }

    private void setTypePayment() {
        if (rgTypePayment.getSelectedItem() != null) {
            if ("keypay".equals(rgTypePayment.getSelectedItem().getId())) {
                gbTypePayment.setVisible(false);
                typePayment = 1;
            } else if ("immediaacy".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = 2;
                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(true);
                txtBillKey.setVisible(true);
                lbBillNo.setVisible(true);
                lbBillNoRequi.setVisible(true);
                lbBillKey.setVisible(true);
                lbBillKeyRequi.setVisible(true);

            } else if ("banktransfer".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = 3;
                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(false);
                txtBillKey.setVisible(false);
                lbBillNo.setVisible(false);
                lbBillNoRequi.setVisible(false);
                lbBillKey.setVisible(false);
                lbBillKeyRequi.setVisible(false);
            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        setTypePayment();
        //txtSearchDeptName.setValue("Selection Made: "+rgTypePayment.getSelectedItem().getId());
    }

    private boolean isValidate() {
        String message;
        if ("".equals(tbPaymentName.getText().trim())) {
            message = "Bạn vui lòng nhập tên người thanh toán";
            tbPaymentName.focus();
            setWarningMessage(message);
            return false;
        }
        if (typePayment == 0) {
            message = "Bạn vui lòng chọn hình thức thanh toán";
            setWarningMessage(message);
            return false;
        }

        if (typePayment == 2) {

            if ("".equals(txtBillNo.getText().trim())) {
                message = "Bạn vui lòng nhập mã hóa đơn";
                txtBillNo.focus();
                setWarningMessage(message);
                return false;
            }
            if ("".equals(txtBillKey.getText().trim())) {
                message = "Bạn vui lòng nhập ký hiệu hóa đơn";
                txtBillKey.focus();
                setWarningMessage(message);
                return false;
            }
        }
        if ((typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) || (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT)) {
            if (dbDayPayment.getValue() == null) {
                message = "Bạn vui lòng nhập ngày thanh toán";
                setWarningMessage(message);
                dbDayPayment.setFocus(true);
                return false;
            } else {
                Date mDate = new Date();
                if (dbDayPayment.getValue().after(mDate)) {
                    message = "Ngày thanh toán phải nhỏ hơn ngày hiện tại";
                    setWarningMessage(message);
                    dbDayPayment.setFocus(true);
                    return false;
                }
            }

            if ((typeWindows == 1) && (media == null)) {
                message = "Bạn vui lòng nhập file đính kèm hóa đơn";
                setWarningMessage(message);
                return false;
            }
            if (typeWindows == 2) {
                if ((clickDeletefile) && (media == null)) {
                    message = "Bạn vui lòng nhập file đính kèm hóa đơn";
                    setWarningMessage(message);
                    return false;
                }

            }
            if ("".equals(dbDayPayment.getValue().toString())) {
                message = "Bạn vui lòng nhập ngày thanh toán";

                setWarningMessage(message);
                return false;
            }
        }

        return true;

    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
    }

    @Listen("onUpload = #btnUpload")
    public void onUpload(UploadEvent evt) throws IOException {

        media = evt.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            	String sExt = ResourceBundleUtil.getString("extend_file", "config");
	                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
            return;
        }

        if (attachCurrent != null) {
            Messagebox.show("Bạn có đồng ý thay thế tệp biểu mẫu cũ?", "Thông báo", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event evt) throws InterruptedException {
                    if (Messagebox.ON_OK.equals(evt.getName())) {
                        txtFileName.setValue(media.getName());
                        imgDelFile.setVisible(true);
                        lbdeletefile.setVisible(true);
                        String fullPath = attachCurrent.getAttachPath() + attachCurrent.getAttachName();
                        //xóa file cũ nếu có
                        File f = new File(fullPath);
                        if (f.exists()) {
                            ConvertFileUtils.deleteFile(fullPath);
                        }
                        AttachDAOHE attachDAOHE = new AttachDAOHE();
                        attachDAOHE.delete(attachCurrent);
                    }
                }
            });
        } else {
            bUploadFile = true;
            clickDeletefile = false;
            txtFileName.setValue(media.getName());
            imgDelFile.setVisible(true);
            lbdeletefile.setVisible(true);
        }

    }

    @Listen("onClick = #imgDelFile")
    public void onClickDelete(Event ev) {
        clickDeletefile = true;
        bUploadFile = false;
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
        lbdeletefile.setVisible(false);
    }

    @Listen("onClick = #lbdeletefile")
    public void onClicklbDelete(Event ev) {
        clickDeletefile = true;
        bUploadFile = false;
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
        lbdeletefile.setVisible(false);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        onAccept();
        txtValidate.setValue("1");

    }
}
