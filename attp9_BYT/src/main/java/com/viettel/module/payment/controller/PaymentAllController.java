package com.viettel.module.payment.controller;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.cosmetic.BO.CosFile;
import com.viettel.module.cosmetic.BO.VFileCosfile;
import com.viettel.module.cosmetic.DAO.CosmeticDAO;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.model.SearchModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author Linhdx
 */
public class PaymentAllController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #dbFromDayModify")
    private Datebox dbFromDayModify;
    @Wire("#incSearchFullForm #dbToDayModify")
    private Datebox dbToDayModify;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #lboxStatus")
    private Listbox lboxStatus;
    @Wire("#incSearchFullForm #tbCosmeticBrand")
    private Textbox tbCosmeticBrand;
    @Wire("#incSearchFullForm #tbCosmeticProductName")
    private Textbox tbCosmeticProductName;
    @Wire("#incSearchFullForm #tbCosmeticCompanyName")
    private Textbox tbCosmeticCompanyName;
    @Wire("#incSearchFullForm #tbCosmeticCompanyAddress")
    private Textbox tbCosmeticCompanyAddress;
    @Wire("#incSearchFullForm #tbCosmeticDistributePersonName")
    private Textbox tbCosmeticDistributePersonName;
    @Wire("#incSearchFullForm #tbBusinessTaxCode")
    private Textbox tbBusinessTaxCode;
    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    private Window PaymentAll;
    private CosmeticFileModel lastSearchModel;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    //private long DEPT_ID;
    Long UserId = getUserId();
    CategoryDAOHE dao = new CategoryDAOHE();
    //  Long RoleId = Long.parseLong(dao.LayTruong("RoleUserDept", "userId", UserId.toString(), "roleId"));
    private Users user;
    private String sPositionType;
    private Constants constants = new Constants();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        lastSearchModel = new CosmeticFileModel();
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        FILE_TYPE_STR = (String) arguments.get("filetype");
        String menuTypeStr = (String) arguments.get("menuType");
        sPositionType = (String) arguments.get("PositionType");
        String statusStr = (String) arguments.get("status");
        Long statusL = null;
        try {
            if (statusStr != null) {
                statusL = Long.parseLong(statusStr);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        if (statusL != null) {
            lastSearchModel.setStatus(statusL);
        }

        lastSearchModel.setMenuTypeStr(menuTypeStr);
//        try {
//            DEPT_ID = Long.parseLong(deptIdStr);
//            DEPT_ID = 3300L;
//        } catch (Exception e) {
//            DEPT_ID = -1l;
//        }

        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);
        CategoryDAOHE cdhe = new CategoryDAOHE();

        List lstStatusAll = cdhe.getSelectCategoryByType(Constants.CATEGORY_TYPE.FILE_STATUS, "name");

        dbFromDay.setValue(null);
        lastSearchModel.setModifyDateFrom(null);
        dbToDay.setValue(new Date());
        lastSearchModel.setModifyDateTo(new Date());

        //sap xep lstStatusAll theo value
        //bubbleSort(lstStatusAll, lstStatusAll.size());
        ListModelArray lstModelProcedure = new ListModelArray(lstStatusAll);
        lboxStatus.setModel(lstModelProcedure);
        onSearch();
    }

    private List<Long> convert(String str) {
        List<Long> lstLong = new ArrayList<>();
        if (str != null) {
            String[] statusLst = str.split(";");
            for (String statusStr : statusLst) {
                try {
                    if (statusStr != null) {
                        Long statusL = Long.parseLong(statusStr);
                        lstLong.add(statusL);
                    }

                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }

            }
        }
        return lstLong;
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        //validate search

        if (dbFromDay != null && dbToDay != null) {
            if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                showNotification("Thời gian từ ngày không được lớn hơn đến ngày", Constants.Notification.WARNING, 2000);
                return;
            }
        }
        if (dbFromDayModify != null && dbToDayModify != null) {
            if (dbFromDayModify.getValue().compareTo(dbToDayModify.getValue()) > 0) {
                showNotification("Thời gian từ ngày không được lớn hơn đến ngày", Constants.Notification.WARNING, 2000);
                return;
            }
        }

        lastSearchModel.setCreateDateFrom(dbFromDay == null ? null : dbFromDay.getValue());
        lastSearchModel.setCreateDateTo(dbToDay == null ? null : dbToDay.getValue());
        lastSearchModel.setModifyDateFrom(dbFromDayModify == null ? null : dbFromDayModify.getValue());
        lastSearchModel.setModifyDateTo(dbToDayModify == null ? null : dbToDayModify.getValue());
        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        lastSearchModel.setBrandName(tbCosmeticBrand.getValue());
        lastSearchModel.setProductName(tbCosmeticProductName.getValue());
        lastSearchModel.setBusinessName(tbCosmeticCompanyName == null ? null : tbCosmeticCompanyName.getValue());
        lastSearchModel.setBusinessAddress(tbCosmeticCompanyAddress == null ? null : tbCosmeticCompanyAddress.getValue());
        lastSearchModel.setDistributePersonName(tbCosmeticDistributePersonName == null ? null : tbCosmeticDistributePersonName.getValue());
        lastSearchModel.setTaxCode(tbBusinessTaxCode == null ? null : tbBusinessTaxCode.getValue());
        if (lboxStatus.getSelectedItem() != null) {
            try {
                Object value = lboxStatus.getSelectedItem().getValue();
                if (value != null && value instanceof String) {
                    Long valueL = Long.valueOf((String) value);
                    if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                        lastSearchModel.setStatus(valueL);
                    }
                } else {
                    lastSearchModel.setStatus(null);
                }
            } catch (NumberFormatException e) {
                LogUtils.addLogDB(e);
            }

        }

//        if (lboxDocumentTypeCode.getSelectedItem() != null) {
//            String value = lboxDocumentTypeCode.getSelectedItem().getValue();
//            if (value != null) {
//                Long valueL = Long.valueOf(value);
//                if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
//                    lastSearchModel.setDocumentTypeCode(valueL);
//                }
//            }
//        }
        userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        reloadModel(lastSearchModel);

    }

    private void reloadModel(SearchModel searchModel) {
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm;
        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();

        plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel, getUserId(), getDeptId(), start, take);

        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbList.setModel(lstModel);

    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #PaymentAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch=#PaymentAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    @Listen("onChangeTime=#PaymentAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            if (dbFromDay != null) {
                dbFromDay.setValue(model.getFromDate());
            }
            if (dbToDay != null) {
                dbToDay.setValue(model.getToDate());
            }
            if (dbFromDayModify != null) {
                dbFromDayModify.setValue(model.getFromDate());
            }
            if (dbToDayModify != null) {
                dbToDayModify.setValue(model.getToDate());
            }

            onSearch();
        }
    }

//    private Map<String, Object> setParam(Map<String, Object> arguments) {
//        arguments.put("parentWindow", PaymentAll);
//        arguments.put("filetype", FILE_TYPE_STR);
//        arguments.put("procedureId", FILE_TYPE);
//
//        arguments.put("deptId", DEPT_ID);
//        return arguments;
//
//    }
//    @Listen("onOpenCreate=#PaymentAll")
//    public void onOpenCreate() {
//        Map<String, Object> arguments = new ConcurrentHashMap();
//        arguments.put("CRUDMode", "CREATE");
//        setParam(arguments);
//        Window window = createWindow("windowCRUD",
//                "/Pages/cosmetic/cosmeticCRUD.zul", arguments,
//                Window.EMBEDDED);
//        window.setMode(Window.Mode.EMBEDDED);
////        PaymentAll.setVisible(false);
//    }
    @Listen("onOpenView =#incList #lbList")
    public void onOpenView(Event event) {
        VFileCosfile obj = (VFileCosfile) event.getData();
        createWindowView(obj.getFileId(), obj.getFileType(), Constants.DOCUMENT_MENU.ALL,
                PaymentAll);
        // PaymentAll.setVisible(false);
    }

    @Listen("onOpenUpdate =#incList #lbList")
    public void onOpenUpdate(Event event) {
        VFileCosfile obj = (VFileCosfile) event.getData();
        createWindowEdit(obj.getFileId(), obj.getFileType(), Constants.DOCUMENT_MENU.ALL,
                PaymentAll);
        // PaymentAll.setVisible(false);
    }

    @Listen("onRefresh =#PaymentAll")
    public void onRefresh(Event event) {
        //     onOpenView(event);
    }

    @Listen("onSelectedProcess = #PaymentAll")
    public void onSelectedProcess(Event event) {
        PaymentAll.setVisible(false);

    }

    private void createWindowView(Long id, Long idType, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("docId", id);
        arguments.put("docType", idType);
        arguments.put("menuType", menuType);
        arguments.put("windowParent", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        createWindow("viewPaymentWindow", "/Pages/module/payment/viewPaymentSingle.zul",
                arguments, Window.MODAL);
    }

    private void createWindowEdit(Long id, Long idType, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("docId", id);
        arguments.put("docType", idType);
        arguments.put("menuType", menuType);
        arguments.put("windowParent", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        createWindow("viewPaymentWindow", "/Pages/module/payment/EditPaymentSingleBusiness.zul",
                arguments, Window.MODAL);
    }

    @Listen("onSave = #PaymentAll")
    public void onSave(Event event) {
        PaymentAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
    }

    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi
     * windowDocIn len Con cac su kien save, update thi da xu li hien thi
     * windowDocIn trong phuong thuc onSave
     */
    @Listen("onVisible =  #PaymentAll")
    public void onVisible() {
        reloadModel(lastSearchModel);
        PaymentAll.setVisible(true);
    }

    @Listen("onDownLoadGiayPhep = #incList #lbList")
    public void onDownLoadGiayPhep(Event event) throws FileNotFoundException, IOException {
        VFileCosfile obj = (VFileCosfile) event.getData();
        Long fileid = obj.getFileId();
        CosmeticDAO cosDao = new CosmeticDAO();
        CategoryDAOHE catDaohe = new CategoryDAOHE();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        //co giay phep
        if (check == 1) {
            AttachDAOHE attachDaoHe = new AttachDAOHE();
            Long cospermit = Long.parseLong(catDaohe.LayTruongPermitId(fileid));

            Attachs attachs = attachDaoHe.getByObjectIdAndAttachCat(cospermit, Constants.OBJECT_TYPE.COSMETIC_PERMIT);
            //Nếu có file
            if (attachs != null) {
//                attDAO.downloadFileAttach(attachs);

                String path = attachs.getFullPathFile();
                File f = new File(path);
                //neu ton tai
                if (f.exists()) {
                    File tempFile = FileUtil.createTempFile(f, attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                } //Khong se tao file 
                else {
                    String folderPath = ResourceBundleUtil.getString("dir_upload");
                    FileUtil.mkdirs(folderPath);
                    String separator = ResourceBundleUtil.getString("separator");
                    folderPath += separator + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
                    File fd = new File(folderPath);
                    if (!fd.exists()) {
                        fd.mkdirs();
                    }
                    f = new File(attachs.getFullPathFile());
                    if (f.exists()) {
                    } else {
                        f.createNewFile();
                    }
                    File tempFile = FileUtil.createTempFile(f, attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                }
            } //chua có file sẽ tạo trên hệ thông
            else {
                //showNotification("File không còn tồn tại trên hệ thống!");
            }
        }
        //se bo sung truong hop sua goi bo sung
        //Kiem tra bang permit
    }

    @Listen("onViewFlow = #lbList")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul", args, Window.MODAL);
    }

    @Listen("onDelete = #incList #lbList")
    public void onDelete(Event event) {
        final VFileCosfile obj = (VFileCosfile) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {
                                        if (isAbleToDelete(obj)) {
                                            FilesDAOHE objDAOHE = new FilesDAOHE();
                                            objDAOHE.delete(obj.getFileId());
                                            onSearch();
                                            showNotification(String.format(Constants.Notification.DELETE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.INFO);
                                        } else {
                                            showNotification("Bạn không có quyền xóa");
                                        }

                                    } catch (Exception e) {
                                        showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                        LogUtils.addLogDB(e);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    @Listen("onClick=#incSearchFullForm #btnReset")
    public void doResetForm() {
        tbCosmeticBrand.setValue("");
        tbCosmeticCompanyAddress.setValue("");
        tbCosmeticCompanyName.setValue("");
        tbCosmeticProductName.setValue("");
        tbCosmeticDistributePersonName.setValue("");
        tbNSWFileCode.setValue("");
        if (tbBusinessTaxCode != null) {
            tbBusinessTaxCode.setValue("");
        }
        if (dbFromDay != null) {
            dbFromDay.setValue(null);
        }
        if (dbToDay != null) {
            dbToDay.setValue(new Date());
        }
        if (dbFromDayModify != null) {
            dbFromDayModify.setValue(null);
        }
        if (dbToDayModify != null) {
            dbToDayModify.setValue(new Date());
        }
        lboxStatus.setSelectedIndex(0);

        onSearch();
    }

//    @Listen("onCopy = #incList #lbList")
//    public void onCopy(Event event) {
//
////        VFileCosfile obj = (VFileCosfile) event.getData();
////        Map<String, Object> arguments = new ConcurrentHashMap<>();
////
////        Files files = createFile(obj);
////        CosFile cosFile = createCosFile(obj);
////
////        arguments.put("CRUDMode", "COPY");
////        arguments.put("files", files);
////        arguments.put("cosFile", cosFile);
////        setParam(arguments);
////        createWindow("windowCRUDRapidTest", "/Pages/module/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
//        
//        
//        VFileCosfile obj = (VFileCosfile) event.getData();
//        Map<String, Object> arguments = new ConcurrentHashMap<>();
//        arguments.put("id", obj.getFileId());
//        arguments.put("CRUDMode", "COPY");
//       // setParam(arguments);
//        createWindow("windowCRUDRapidTest", "/Pages/module/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
//        
//        PaymentAll.setVisible(false);
//
//    }
//    private Files createFile(VFileCosfile obj) {
//        Date dateNow = new Date();
//        Files files = new Files();
//        files.setCreateDate(obj.getCreateDate());
//        files.setModifyDate(obj.getModifyDate());
//        files.setCreatorId(obj.getCreatorId());
//        files.setCreatorName(obj.getCreatorName());
//        files.setCreateDeptId(obj.getCreateDeptId());
//        files.setCreateDeptName(obj.getCreateDeptName());
//        files.setIsActive(Constants.Status.ACTIVE);
//
//        // viethd 29/01/2015
//        // set status of file is INIT
//        files.setStatus(Constants.PROCESS_STATUS.INITIAL);
//        files.setFileType(FILE_TYPE);
//        CategoryDAOHE che = new CategoryDAOHE();
//        Category cat = che.findById(FILE_TYPE);
//        if (cat != null) {
//            String fileTypeName = cat.getName();
//            files.setFileTypeName(fileTypeName);
//        }
//        List<Flow> flows = WorkflowAPI.getInstance().getFlowByDeptNObject(DEPT_ID, FILE_TYPE);
//        files.setFlowId(flows.get(0).getFlowId());
//        return files;
//    }
    private CosFile createCosFile(VFileCosfile obj) {
        CosFile cosFile = new CosFile();
        cosFile.setFileId(obj.getFileId());

        cosFile.setDocumentTypeCode(obj.getDocumentTypeCode());
        cosFile.setBrandName(obj.getBrandName());
        cosFile.setProductName(obj.getProductName());
        //cosFile.setProductType(tbProductType.getValue());
        cosFile.setIntendedUse(obj.getIntendedUse());
//                cosFile.setProductPresentation(tbProductPresentation.getValue());
        cosFile.setDistributorName(obj.getDistributorName());
        cosFile.setDistributorAddress(obj.getDistributorAddress());
        cosFile.setDistributorPhone(obj.getDistributorPhone());
        cosFile.setDistributorFax(obj.getDistributorFax());
        cosFile.setDistributorRegisNumber(obj.getDistributorRegisNumber());
        cosFile.setDistributePersonName(obj.getDistributePersonName());
        cosFile.setDistributePersonPhone(obj.getDistributePersonPhone());
        cosFile.setDistributePersonEmail(obj.getDistributePersonEmail());
        cosFile.setDistributePersonPosition(obj.getDistributePersonPosition());
        cosFile.setImporterName(obj.getImporterName());
        cosFile.setImporterAddress(obj.getImporterAddress());
        cosFile.setImporterPhone(obj.getImporterPhone());
        cosFile.setImporterFax(obj.getImporterFax());
        cosFile.setOtherProductPresentation(obj.getProductPresentation());
        cosFile.setOtherProductType(obj.getOtherProductType());
        cosFile.setIngreConfirm1(obj.getIngreConfirm1());
        cosFile.setIngreConfirm2(obj.getIngreConfirm2());

        cosFile.setSignPlace(obj.getSignPlace());
        cosFile.setSignName(obj.getSignName());
        cosFile.setSignDate(obj.getSignDate());
        cosFile.setProductPresentation(obj.getProductPresentation());
        cosFile.setProductType(obj.getProductType());

        return cosFile;
    }

    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(VFileCosfile obj) {
        if (user.getUserId() != null && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.INITIAL)) {
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean isAbleToModify(VFileCosfile obj) {
        if (user.getUserId() != null && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.SENT_DISPATCH))
                    || (Objects.equals(obj.getStatus(), Constants.PROCESS_STATUS.SENT_RETURN))) {
                return true;
            }
            return false;
        }
        return false;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public int CheckGiayPhep(Long fileid) {
        CosmeticDAO cosDao = new CosmeticDAO();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        return check;
    }

//    public int checkDispath(Long fileid) {
//        CosmeticDAO cosDao = new CosmeticDAO();
//        int check = cosDao.checkDispath(fileid, getUserId());
//        return check;
//    }
//    public boolean CheckView(String name) {
//        if (name.equals("Edit")) {
//            if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDC)) {
//                return false;
//            } else if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDP)) {
//                return false;
//            } else {
//                return true;
//            }
//        }
//        if (name.equals("Delete")) {
//            if (RoleId.equals(Constants_Cos.ROLE_ID.QLD_LDC)) {
//                return false;
//            }
//            return true;
//        }
//        return true;
//    }
//    @Listen("onViewSDBS = #incList #lbList")
//    public void onViewSDBS(Event event) throws FileNotFoundException, IOException {
//        VFileCosfile obj = (VFileCosfile) event.getData();
//        Map<String, Object> arguments = new ConcurrentHashMap<>();
//        arguments.put("id", obj.getFileId());
//        arguments.put("CRUDMode", "UPDATE");
//        setParam(arguments);
//        createWindow("windowViewSDBS", "/Pages/module/cosmetic/viewDispatch.zul", arguments, Window.POPUP);
//
//    }
    public Constants getConstants() {
        return constants;
    }

    public void setConstants(Constants constants) {
        this.constants = constants;
    }
}
