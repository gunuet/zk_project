package com.viettel.module.payment.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.databind.BindingListModelList;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.*;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.util.Objects;

/**
 *
 * @author Linhdx
 */
public class PaymentConfirmController extends PaymentController {

    @Wire
    private Listbox lbListDetail;
    @Wire
    private Textbox tbPaymentName;
    @Wire
    private Window businessWindow;
    @Wire
    Radiogroup rgTypePayment;
    List<VCosPaymentInfo> listVCosPaymentInfo;
    @Wire("#lbWarning")
    private Label lbWarning;
    @Wire("#toplbWarning")
    private Label toplbWarning;
    @Wire("#sumMoney")
    private Label sumMoney;
    @Wire
    Groupbox gbTypePayment;
    private Window parentWindow;
    Long lsumMoney = 0L;
    // danh cho upload file
    @Wire
    Textbox txtBillNo, txtBillKey;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    Image imgDelFile;
    @Wire
    Label lbdeletefile;
    @Wire
    Textbox txtValidate;
    private Media media;
    private Attachs attachCurrent;
    Long typePayment = 0L;// kieu thanh toan 1//keypay //2 chuyen khoan //3 tien mat
    int noPayment = 0;
    @Wire
    Radio keypay, banktransfer, immediaacy;
    // khai bao luong
    protected com.viettel.core.workflow.BO.Process process;
    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    private Long idLongPayment[] = null;

    //String sTypeView="";
    // 1 gửi đơn
    // 2 gui nhieu
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("windowParent");
        listVCosPaymentInfo = new ArrayList<>();
        listVCosPaymentInfo.clear();
        //sTypeView=(String) arguments.get("typeView");
//        VCosPaymentInfoDAO vCosDao = new VCosPaymentInfoDAO();

//        if(sTypeView.equals("1")){
//            listVCosPaymentInfo=vCosDao.getListFileId((Long)arguments.get("fileId"));
//        }
//        else{
        listVCosPaymentInfo = (List<VCosPaymentInfo>) arguments.get("listVCosPaymentInfo");
        int sizePaymentInfo = listVCosPaymentInfo.size();
        for (int i = 0; i < sizePaymentInfo; i++) {
            lsumMoney = lsumMoney + listVCosPaymentInfo.get(i).getCost();
        }
        noPayment = (int) arguments.get("typeProcess");
        idLongPayment = (Long[]) arguments.get("tempLongId");
        objBill = (Bill) arguments.get("tempBill");
        //}
        // FeePaymentInfoDAOHE objDAOHE = new FeePaymentInfoDAOHE();
        //feePaymentInfo = objDAOHE.findById(feePaymentInfoId);
        return super.doBeforeCompose(page, parent, compInfo);
    }
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    Bill objBill = null;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        BindingListModelList model = new BindingListModelList<>(listVCosPaymentInfo, true);
        model.setMultiple(true);
        lbListDetail.setModel(model);
        lbListDetail.renderAll();
        lbListDetail.setSizedByContent(true);
        sumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));
        txtValidate.setValue("0");
        if (objBill != null) {
            if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
                rgTypePayment.setSelectedItem(keypay);
            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
                rgTypePayment.setSelectedItem(banktransfer);

            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                rgTypePayment.setSelectedItem(immediaacy);
                txtBillKey.setValue(objBill.getBillSign());
                txtBillNo.setValue(objBill.getCode());
            }
            dbDayPayment.setValue(objBill.getPaymentDate());
            tbPaymentName.setValue(objBill.getCreatetorName());

            setTypePayment();

            AttachDAOHE attDAOHE = new AttachDAOHE();
            List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
            if ((items != null) && (items.size() > 0)) {
                String path = items.get(0).getFullPathFile();
                File f = new File(path);
                try {
                    media = new AMedia(f, null, null);
                } catch (FileNotFoundException ex) {

                    LogUtils.addLogDB(ex);
                }
                txtFileName.setValue(items.get(0).getAttachName());
                imgDelFile.setVisible(true);
                lbdeletefile.setVisible(true);
            }
        }
    }

    private void setTypePayment() {
        if (rgTypePayment.getSelectedItem() != null) {
            if ("keypay".equals(rgTypePayment.getSelectedItem().getId())) {
                gbTypePayment.setVisible(false);

            } else if ("immediaacy".equals(rgTypePayment.getSelectedItem().getId())) {

                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(true);
                txtBillKey.setVisible(true);
                lbBillNo.setVisible(true);
                lbBillNoRequi.setVisible(true);
                lbBillKey.setVisible(true);
                lbBillKeyRequi.setVisible(true);

            } else if ("banktransfer".equals(rgTypePayment.getSelectedItem().getId())) {

                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(false);
                txtBillKey.setVisible(false);
                lbBillNo.setVisible(false);
                lbBillNoRequi.setVisible(false);
                lbBillKey.setVisible(false);
                lbBillKeyRequi.setVisible(false);
            }
        }
    }

    @Listen("onClick = #btnAccept")
    public void onAccept() {

        if (isValidate()) {
            // lay ngay hien tai
//            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            // update trong bang bill
            BillDAO objBillDAOHE = new BillDAO();
            Bill objBill = new Bill();
            objBill.setCreatetorName(tbPaymentName.getText().trim());
            objBill.setCreateDate(date);
            objBill.setPaymentDate(dbDayPayment.getValue());
            objBill.setTotal(lsumMoney);
            objBill.setCreatorId(getUserId());
            objBill.setStatus(ConstantsPayment.PAYMENT.PAY_NEW);
            objBill.setPaymentTypeId(Long.valueOf(typePayment));
            // neu la chuyen khoan them ma hoa don ky hieu hoa don
            if (Objects.equals(typePayment, ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT)) {
                objBill.setCode(txtBillNo.getText().trim());
                objBill.setBillSign(txtBillKey.getText().trim());
            }
            objBillDAOHE.saveOrUpdate(objBill);
            Long idBill = objBill.getBillId();
            //update trong paymentInfo
            PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
            PaymentInfo paymentInfo;
            int sizeList = listVCosPaymentInfo.size();
            for (int i = 0; i < sizeList; i++) {
                paymentInfo = objFreePaymentInfo.findById(listVCosPaymentInfo.get(i).getPaymentInfoId());
                paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_ALREADY);
                paymentInfo.setBillId(idBill);
                paymentInfo.setUpdateDate(date);
                paymentInfo.setPaymentDate(dbDayPayment.getValue());
                //paymentInfo.setPaymentActionUserId(getUserId());
                //paymentInfo.setPaymentActionUser(getUserName());
                paymentInfo.setPaymentTypeId(Long.valueOf(typePayment));
                // neu la chuyen khoan them ma hoa don ky hieu hoa don
                if (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                    paymentInfo.setBillCode(txtBillNo.getText().trim());
                    paymentInfo.setPaymentCode(txtBillKey.getText().trim());
                }
                //objFreePaymentInfo.getSession().saveOrUpdate(paymentInfo);
                objFreePaymentInfo.getSession().merge(paymentInfo);
                //objFreePaymentInfo.update(paymentInfo);
                //objFreePaymentInfo.getSession().flush();
                //objFreePaymentInfo.mrge
            }
            //update attach File
            try {
                AttachDAO attDAO = new AttachDAO();
                attDAO.saveFileAttach(media, idBill, Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY, null);
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
            if (parentWindow != null) {
                if (noPayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO) {
                    Events.sendEvent("onReloadPage", parentWindow, null);
                } else if (noPayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_MOT_HO_SO) {
                    Events.sendEvent("onVisible", parentWindow, null);
                }
            }

            if (idLongPayment != null) {

                int size = idLongPayment.length;
                for (int i = 0; i < size; i++) {
                    paymentInfo = objFreePaymentInfo.findById(idLongPayment[i]);
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_NEW);
                    paymentInfo.setBillId(null);
                    paymentInfo.setPaymentTypeId(null);
                    paymentInfo.setDateConfirm(null);
                    paymentInfo.setPaymentActionUser(null);
                    paymentInfo.setPaymentActionUserId(null);
                    paymentInfo.setPaymentConfirm(null);
                    objFreePaymentInfo.getSession().merge(paymentInfo);
                }
            }

            showNotification("Bạn đã thanh toán thành công. ",
                    Constants.Notification.INFO);
            txtValidate.setValue("1");
            Events.sendEvent("onReloadPage", parentWindow, null);

            // xu ly luong
//            List<NodeDeptUser> lstChoosenUser = getListChoosedNDU();
//            NodeToNode action = (new NodeToNodeDAOHE()).getActionById(actionId);
//            Long processParentId = null;
//            if (process != null) {
//                processParentId = process.getProcessId();
//            }
//            String note = txtNote.getText();
//            WorkflowAPI.getInstance().
//                    sendDocToListNDUs(docId, docType, action, note, processParentId, lstChoosenUser);
//
//            ConfirmPaymentInfoAll.onClose();
//            showNotification("Bạn đã thanh toán thành công. ",
//                    Constants.Notification.INFO);
        }
    }

//    public List<NodeDeptUser> getListChoosedNDU() {
//        List list = new ArrayList<>();
//        if (lbNodeDeptUser != null) {
//            for (Listitem item : lbNodeDeptUser.getItems()) {
//                if (!item.isDisabled()) {
//                    list.add((NodeDeptUser) item.getValue());
//                }
//            }
//        }
//        return list;
//    }
    @Listen("onClick = #btnReject")
    public void onReject() {

        businessWindow.onClose();
        //Events.sendEvent("onVisible", parentWindow, null);

    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        if ("keypay".equals(rgTypePayment.getSelectedItem().getId())) {
            gbTypePayment.setVisible(false);
            typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
        } else if ("immediaacy".equals(rgTypePayment.getSelectedItem().getId())) {
            typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;
            gbTypePayment.setVisible(true);
            txtBillNo.setVisible(true);
            txtBillKey.setVisible(true);
            lbBillNo.setVisible(true);
            lbBillNoRequi.setVisible(true);
            lbBillKey.setVisible(true);
            lbBillKeyRequi.setVisible(true);
        } else if ("banktransfer".equals(rgTypePayment.getSelectedItem().getId())) {
            typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;
            gbTypePayment.setVisible(true);
            txtBillNo.setVisible(false);
            txtBillKey.setVisible(false);
            lbBillNo.setVisible(false);
            lbBillNoRequi.setVisible(false);
            lbBillKey.setVisible(false);
            lbBillKeyRequi.setVisible(false);
        }
    }

    private boolean isValidate() {
        String message;
        if ("".equals(tbPaymentName.getText().trim())) {
            message = "Bạn vui lòng nhập tên người thanh toán";
            tbPaymentName.focus();
            setWarningMessage(message);
            return false;
        }
        if (typePayment == 0) {
            message = "Bạn vui lòng chọn hình thức thanh toán";
            setWarningMessage(message);
            return false;
        }
        if (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
            if ("".equals(txtBillNo.getText().trim())) {
                message = "Bạn vui lòng nhập mã hóa đơn";
                txtBillNo.focus();
                setWarningMessage(message);
                return false;
            }
            if ("".equals(txtBillKey.getText().trim())) {
                message = "Bạn vui lòng nhập ký hiệu hóa đơn";
                txtBillKey.focus();
                setWarningMessage(message);
                return false;
            }
        }
        if ((typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) || (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT)) {
            if (dbDayPayment.getValue() == null) {
                message = "Bạn vui lòng nhập ngày thanh toán";
                setWarningMessage(message);
                dbDayPayment.setFocus(true);
                return false;
            } else {
                Date mDate = new Date();
                if (dbDayPayment.getValue().after(mDate)) {
                    message = "Ngày thanh toán phải nhỏ hơn ngày hiện tại";
                    setWarningMessage(message);
                    dbDayPayment.setFocus(true);
                    return false;
                }
            }
            if (media == null) {
                message = "Bạn vui lòng nhập file đính kèm hóa đơn";
                setWarningMessage(message);
                return false;
            }
            if ("".equals(dbDayPayment.getValue().toString())) {
                message = "Bạn vui lòng nhập ngày thanh toán";

                setWarningMessage(message);
                return false;
            }
        }
        return true;
    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
        toplbWarning.setValue(message);
    }

    @Listen("onUpload = #btnUpload")
    public void onUpload(UploadEvent evt) throws IOException {

        media = evt.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            showNotification("Định dạng file không được phép tải lên",
                    Constants.Notification.WARNING);
            return;
        }
        if (attachCurrent != null) {
            Messagebox.show("Bạn có đồng ý thay thế tệp biểu mẫu cũ?", "Thông báo", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event evt) throws InterruptedException {
                    if (Messagebox.ON_OK.equals(evt.getName())) {
                        txtFileName.setValue(media.getName());
                        imgDelFile.setVisible(true);
                        lbdeletefile.setVisible(true);
                        String fullPath = attachCurrent.getAttachPath() + attachCurrent.getAttachName();
                        //xóa file cũ nếu có
                        File f = new File(fullPath);
                        if (f.exists()) {
                            ConvertFileUtils.deleteFile(fullPath);
                        }
                        AttachDAOHE attachDAOHE = new AttachDAOHE();
                        attachDAOHE.delete(attachCurrent);
                    }
                }
            });
        } else {
            txtFileName.setValue(media.getName());
            imgDelFile.setVisible(true);
            lbdeletefile.setVisible(true);
        }
    }

    @Listen("onClick = #imgDelFile")
    public void onClickDelete(Event ev) {
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
        lbdeletefile.setVisible(false);
    }

    @Listen("onClick = #lbdeletefile")
    public void onClicklbDelete(Event ev) {
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
        lbdeletefile.setVisible(false);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        onAccept();

    }

    public String getDocumentType(Long documentTypeCode) {

        return WorkflowAPI.getDocumentTypeCode(documentTypeCode);
    }
}
