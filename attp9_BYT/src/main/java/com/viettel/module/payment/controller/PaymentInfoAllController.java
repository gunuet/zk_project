package com.viettel.module.payment.controller;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;

import com.viettel.module.payment.BO.Bill;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;

import com.viettel.module.payment.search.SearchPaymentModel;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.Button;
import com.viettel.core.workflow.BO.Process;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.DateTimeUtils;
import org.zkoss.zk.ui.Components;
import org.zkoss.zul.Div;
import org.zkoss.zul.ListModelArray;

/**
 *
 * @author ChucHV
 */
public class PaymentInfoAllController extends PaymentController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDayPayment")
    private Datebox dbFromDayPayment;
    @Wire("#incSearchFullForm #dbToDayPayment")
    private Datebox dbToDayPayment;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #brandName")
    private Textbox brandName;
    @Wire("#incSearchFullForm #productname")
    private Textbox productname;
    @Wire("#incSearchFullForm #lboxStatusPayment")
    private Listbox lboxStatusPayment;
    @Wire("#incSearchFullForm #lboxStatusFile")
    private Listbox lboxStatusFile;
    @Wire("#incSearchFullForm #lboxOrder")
    private Listbox lboxOrder;
    //@Wire
    //private Button btnListPayment, btnListBill, btnListSearch;
    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire
    private Listbox lbList;
    @Wire
    private Window rapidDetailPaymentInfoAll;
    private SearchPaymentModel lastSearchModel;
    //private Boolean viewPayment = true;
    //@Wire
    //private Spreadsheet spreadsheet;
    //private  String cssButtonChoice="background-color: #2b6fc2;border: medium none;border-radius: 2px;color: #ffffff;	display: inline-block;font-size: 11px;font-weight: bold;height: 30px;margin-right: 3px;margin-top: 3px;";
    // khai bao xu ly luong
    private VCosPaymentInfo vFileRtfile;
    private Process processCurrent;// process dang duoc xu li
    private List<Process> listProcessCurrent;
    private List<Long> listDocId, listDocType;
    private long FILE_TYPE;
    ;
    private Long DEPT_ID;
    @Wire
    private Div topToolbar;
    private List<Component> listTopActionComp;
    @Wire
    private Div bottomToolbar;
    private List<Component> listBottomActionComp;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);

        CategoryDAOHE cdhe = new CategoryDAOHE();
        List<Category> lstStatus;
        ListModelArray lstModelProcedure;
        lstStatus = cdhe.findAllCategorySearchValue(Constants.CATEGORY_TYPE.FILE_STATUS, false);

        lstModelProcedure = new ListModelArray(lstStatus);
        lboxStatusFile.setModel(lstModelProcedure);
        // trang thai thanh toan
        lstStatus = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.PAYMENT_STATUS, true);
        lstModelProcedure = new ListModelArray(lstStatus);
        lboxStatusPayment.setModel(lstModelProcedure);

        dbFromDayPayment.setValue(DateTimeUtils.addDay(new Date(), -14));
        dbToDayPayment.setValue(new Date());

        //ten loai phi
//        lstStatus = cdhe.findAllCategorySearchValue(Constants.CATEGORY_TYPE.FEE);
//        for (Category category : lstStatus) {
//            String sValue = category.getValue();
//            //LogUtils.addLog("gia tri" + sValue);
//        }
//        lstModelProcedure = new ListModelArray(lstStatus);
//        lboxNameCost.setModel(lstModelProcedure);
        onSearch();

    }

    private void initThread() {
        Components.removeAllChildren(topToolbar);

        //Component tempComponet=listTopButton.get(0).getFirstChild();
//        try {
//            int sizeButton = listTopButton.size();
//            if (sizeButton > 2) {
//                for (int i = 2; i < sizeButton; i++) {
//                    Component tempComponent = listTopButton.get(i);
//                    if (tempComponent != null) {
//                        topToolbar.removeChild(tempComponent);
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            LogUtils.addLogDB(ex);
//        }
//        
        listTopActionComp = new ArrayList<>();
        listBottomActionComp = new ArrayList<>();
        listProcessCurrent = new ArrayList<>();
        listDocId = new ArrayList<>();
        listDocType = new ArrayList<>();
        if (listRapidTest.size() > 0) {
            vFileRtfile = listRapidTest.get(0);
            processCurrent = WorkflowAPI.getInstance().
                    getCurrentProcess(vFileRtfile.getFileId(), vFileRtfile.getFileType(),
                            vFileRtfile.getStatusfile(), getUserId());
            FILE_TYPE = vFileRtfile.getFileType();
            DEPT_ID = Constants.CUC_QLD_ID;;
            addAllNextActions();
            loadActionsToToolbar();
        }
    }
// cac ham xu ly luong

    private void addAllNextActions() {
        Long docStatus = vFileRtfile.getStatusfile();
        Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;
        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(
                docType, docStatus, deptId);

        for (NodeToNode action : actions) {
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
    }

    private Button createButtonForAction(NodeToNode action) {
        // check if action is RETURN_PREVIOUS but parent process came from another node
        if (processCurrent != null
                && action.getType() == Constants.NODE_ASSOCIATE_TYPE.RETURN_PREVIOUS
                && action.getNextId() != processCurrent.getPreviousNodeId()) {
            return null;
        }
        // check if receiverId is diferent to current user id
        // find all process having current status of document
        List<com.viettel.core.workflow.BO.Process> listCurrentProcess = WorkflowAPI.getInstance()
                .findAllCurrentProcess(vFileRtfile.getFileId(), vFileRtfile.getFileType(),
                        vFileRtfile.getStatusfile());
        Long userId = getUserId();
        Long creatorId = vFileRtfile.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return null;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                for (com.viettel.core.workflow.BO.Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                if (!needToProcess) {
                    return null;
                }
            }
        }

        Button btn = new Button(action.getAction());

        final String actionName = action.getAction();
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action

        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = vFileRtfile.getStatusfile();
        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, false));

        EventListener<Event> event = new EventListener<Event>() {
            @Override
            public void onEvent(Event t) throws Exception {
                // xu ly khi payment luong
                Set<Listitem> listSelectedItem = lbList.getSelectedItems();
                String message;
                List<VCosPaymentInfo> listVCosPaymentInfo;
                listVCosPaymentInfo = new ArrayList<VCosPaymentInfo>();

                if (1 <= listSelectedItem.size()) {

                    for (Listitem item : listSelectedItem) {
                        VCosPaymentInfo app = item.getValue();
                        listVCosPaymentInfo.add(app);
                        // them list process
                        listProcessCurrent.add((WorkflowAPI.getInstance().
                                getCurrentProcess(app.getFileId(), app.getFileType(),
                                        app.getStatusfile(), getUserId())));
                        listDocId.add(app.getFileId());
                        listDocType.add(app.getFileType());
                        //System.out.print("gia tri"+app.getRapidTestNo());
                    }
                    Map<String, Object> data = new ConcurrentHashMap<>();
                    //data.put("typeProcess",Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);    
                    data.put("listVCosPaymentInfo", listVCosPaymentInfo);
                    data.put("menuType", Constants.DOCUMENT_MENU.ALL);
                    data.put("windowParent", rapidDetailPaymentInfoAll);
                    data.put("typeProcess", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
                    data.put("fileId", vFileRtfile.getFileId());
                    data.put("docStatus", status);
                    data.put("nextId", nextId);
                    data.put("previousId", prevId);
                    // xu li process
                    data.put("lstAvailableNDU", lstNDUs); //nut tiep theo la chung
                    data.put("actionId", actionId);
                    data.put("actionName", actionName);
                    data.put("actionType", actionType);

                    if (processCurrent != null) {
                        data.put("process", processCurrent);
                    }
                    data.put("docId", vFileRtfile.getFileId());
                    data.put("docType", vFileRtfile.getFileType());

                    data.put("listProcess", listProcessCurrent);
                    data.put("listDocId", listDocId);
                    data.put("listDocType", listDocType);
                    createWindow("businessWindow", formName, data, Window.MODAL);
                    //createWindowView(listVCosPaymentInfo, Constants.DOCUMENT_MENU.ALL,
                    //      rapidDetailPaymentInfoAll);
                    // rapidTestPaymentInfoAll.setVisible(false);
                } else {
                    message = "\n Bạn vui lòng chọn hồ sơ";
                    showNotification(message, Constants.Notification.INFO);
                    return;
                }
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    // Hien thi cac action tren thanh toolbar top va bottom
    public void loadActionsToToolbar() {
        for (Component comp : listTopActionComp) {
            topToolbar.appendChild(comp);
        }
        for (Component comp : listBottomActionComp) {
            bottomToolbar.appendChild(comp);
        }

    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo); //To change body of generated methods, choose Tools | Templates.
    }

    /*
     * isPaging: neu chuyen page thi khong can lay _totalSize
     */
    List<VCosPaymentInfo> listRapidTest;
    private Users user;

    private void reloadModel(SearchPaymentModel searchModel) {

        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm;
        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(), start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel, getUserId(), getDeptId(), start, take);
        }
        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        listRapidTest = plm.getLstReturn();
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstModel.setMultiple(true);
        lbList.setModel(lstModel);

//        searchModel.setMenuType(Constants.DOCUMENT_MENU.ALL);
//        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
//        listRapidTest = objDAOHE.getListRequestPayment(deptId,
//                searchModel, userPagingTop.getActivePage(),
//                userPagingTop.getPageSize(), false);
//        
//        if (!isPaging) {
//            List result = objDAOHE.getListRequestPayment(deptId,
//                    searchModel, userPagingTop.getActivePage(),
//                    userPagingTop.getPageSize(), true);
//            if (result.isEmpty()) {
//                _totalSize = 0;
//            } else {
//                _totalSize = ((Long) result.get(0)).intValue();
//            }
//        }
//        
//        userPagingTop.setTotalSize(_totalSize);
//        userPagingBottom.setTotalSize(_totalSize);
//        //LogUtils.addLog("listRapidTest" + listRapidTest.size());
//        BindingListModelList model = new BindingListModelList<>(listRapidTest, true);
//        //ListModelArray lstModelProvince = new ListModelArray(listRapidTest);
//        model.setMultiple(true);
//        lbList.setModel(model);
    }

    @Listen("onAfterRender = #lbList")
    public void onAfterRender() {

        List<Listitem> items = lbList.getItems();

        for (Listitem item : items) {
            VCosPaymentInfo app = item.getValue();
            if ((app.getStatuspayment() == ConstantsPayment.PAYMENT.PAY_ALREADY) || (app.getStatuspayment() == ConstantsPayment.PAYMENT.PAY_CONFIRMED)) {

                item.setDisabled(true);
            }

        }
        //btnListPayment.setClass("btnClick");
        //btnListBill.setClass("btnAction");

    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #rapidDetailPaymentInfoAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch = #rapidDetailPaymentInfoAll")
    public void onShowFullSearch() {

        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    @Listen("onShowListPayment = #rapidDetailPaymentInfoAll")
    public void onShowListPayment() {
    }

    @Listen("onShowListBill = #rapidDetailPaymentInfoAll")
    public void onShowListBill() {
    }

    @Listen("onChangeTime=#rapidDetailPaymentInfoAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            dbFromDayPayment.setValue(model.getFromDate());
            dbToDayPayment.setValue(model.getToDate());
            onSearch();
        }
    }

//    @Listen("onClick=#btnListPayment")
//    public void onClickListPayment() {
//        gbPaymentInfo.setVisible(true);
//        gbPaymentBill.setVisible(false);
//        fullSearchGbx.setVisible(true);
//        //btnListPayment.setStyle("background-color: #FFFF00;border:1px solid brown;");
//        btnListPayment.setClass("btnClick");
//        btnListBill.setClass("btnAction");
//        onSearch();
//    }
//
//    @Listen("onClick=#btnListBill")
//    public void onClickListBill() {
//        gbPaymentInfo.setVisible(false);
//        fullSearchGbx.setVisible(false);
//        gbPaymentBill.setVisible(true);
//        //btnListBill.setStyle("background-color: #FFFF00;border:1px solid brown;");
//        btnListBill.setClass("btnClick");
//        btnListPayment.setClass("btnAction");
//        Events.sendEvent("onReLoad", rapidDetailPaymentBillAll, null);
//    }
    @Listen("onClick=#btnListSearch")
    public void onClickListSearch() {

        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }

    }

//    @Listen("onClick=#btnExport")
//    public void onExport() {
//
//        try {
//            //long start = System.currentTimeMillis();
// 
//            // 1) Load DOCX into WordprocessingMLPackage
//            InputStream is = new FileInputStream(new File(
//                    "E:/bieumauthongbaosuadoibosung.docx"));
//            XWPFDocument document = new XWPFDocument(is);
// 
//            // 2) Prepare Pdf options
//            PdfOptions options = PdfOptions.create();
// 
//            // 3) Convert XWPFDocument to Pdf
//            OutputStream out = new FileOutputStream(new File(
//                    "E:/HelloWorld.pdf"));
//            PdfConverter.getInstance().convert(document, out, options);
//            
// 
//        } catch (Throwable e) {
//            LogUtils.addLogDB(e);
//        }
//    }
    @Listen("onClick=#btnViewPaymentt")
    public void btnViewPaymentt() {
        btnViewPayment();
    }

    @Listen("onClick=#btnViewPayment")
    public void btnViewPayment() {
        Set<Listitem> listSelectedItem = lbList.getSelectedItems();
        String message;
        List<VCosPaymentInfo> listVCosPaymentInfo;
        listVCosPaymentInfo = new ArrayList<VCosPaymentInfo>();

        if (1 <= listSelectedItem.size()) {

            for (Listitem item : listSelectedItem) {
                VCosPaymentInfo app = item.getValue();
                listVCosPaymentInfo.add(app);
                //System.out.print("gia tri"+app.getRapidTestNo());
            }

            createWindowViewPagePayment(listVCosPaymentInfo, Constants.DOCUMENT_MENU.ALL,
                    rapidDetailPaymentInfoAll);

            // rapidTestPaymentInfoAll.setVisible(false);
        } else {
            message = "\n Bạn vui lòng chọn hồ sơ";
            showNotification(message, Constants.Notification.INFO);
            return;
        }
    }

    @Listen("onClick=#btnPaymentt")
    public void onPaymentt() {
        onPayment();
    }

    @Listen("onClick=#btnPayment")
    public void onPayment() {
        Set<Listitem> listSelectedItem = lbList.getSelectedItems();
        String message;
        List<VCosPaymentInfo> listVCosPaymentInfo;
        listVCosPaymentInfo = new ArrayList<VCosPaymentInfo>();

        if (1 <= listSelectedItem.size()) {

            for (Listitem item : listSelectedItem) {
                VCosPaymentInfo app = item.getValue();
                listVCosPaymentInfo.add(app);
                //System.out.print("gia tri"+app.getRapidTestNo());
            }

            createWindowView(listVCosPaymentInfo, Constants.DOCUMENT_MENU.ALL,
                    rapidDetailPaymentInfoAll);

            // rapidTestPaymentInfoAll.setVisible(false);
        } else {
            message = "\n Bạn vui lòng chọn hồ sơ";
            showNotification(message, Constants.Notification.INFO);
            return;
        }

        //LogUtils.addLog("on payment");
        //Messagebox.show("vao day");  
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);

        lastSearchModel = new SearchPaymentModel();

        lastSearchModel.setCreateDateFrom(dbFromDayPayment.getValue());
        lastSearchModel.setCreateDateTo(dbToDayPayment.getValue());

        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        lastSearchModel.setBrandName(brandName.getValue());
        lastSearchModel.setProductName(productname.getValue());

        if (lboxStatusPayment.getSelectedItem() != null) {

            String value = lboxStatusPayment.getSelectedItem().getValue();

            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != -1) {
                    lastSearchModel.setStatusPayment(valueL);
                }
            }
        }
        if (lboxStatusFile.getSelectedItem() != null) {
            String value = lboxStatusFile.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != -1) {
                    lastSearchModel.setStatusFile(valueL);
                }
            }
        }
//        if (lboxDocumentTypeCode.getSelectedItem() != null) {
//            String value = lboxDocumentTypeCode.getSelectedItem().getValue();
//            if (value != null) {
//                Long valueL = Long.valueOf(value);
//                if (valueL != -1) {
//                    lastSearchModel.setDocumentTypeCode(valueL);
//                }
//            }
//        }

        if (lboxOrder.getSelectedItem() != null) {
            String value = lboxOrder.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != -1) {
                    lastSearchModel.setTypeOrder(valueL);
                }
            }
        }

        reloadModel(lastSearchModel);
        initThread();
    }

    @Listen("onReloadPage = #rapidDetailPaymentInfoAll")
    public void onReloadPage() {
        onSearch();
    }

    private void createWindowView(List<VCosPaymentInfo> listVCosPaymentInfo, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("listVCosPaymentInfo", listVCosPaymentInfo);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("feePaymentType", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
        Window window = createWindow("ConfirmPaymentInfoAll", "/Pages/module/payment/detailpaymentConfirm.zul",
                arguments, Window.MODAL);
        window.doModal();
    }

    private void createWindowViewPagePayment(List<VCosPaymentInfo> listVCosPaymentInfo, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("listVCosPaymentInfo", listVCosPaymentInfo);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("feePaymentType", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
        Window window = createWindow("windowsViewPagePayment", "/Pages/module/payment/viewPagePayment.zul",
                arguments, Window.MODAL);
        window.doModal();
    }

//    @Listen("onClick = #onPayment")
//    public void onPayment() {
//         Messagebox.show("vao day");  
//    }
    @Listen("onSave = #rapidDetailPaymentInfoAll")
    public void onSave(Event event) {

        rapidDetailPaymentInfoAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
    }

    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi
     * windowDocIn len Con cac su kien save, update thi da xu li hien thi
     * windowDocIn trong phuong thuc onSave
     */
    @Listen("onVisible = #rapidDetailPaymentInfoAll")
    public void onVisible() {
        reloadModel(lastSearchModel);
        rapidDetailPaymentInfoAll.setVisible(true);
    }

    @Listen("onOpenView = #lbList")
    public void onOpenView(Event event) throws FileNotFoundException {
        VCosPaymentInfo obj = (VCosPaymentInfo) event.getData();
        PaymentInfoDAO objPaymentDaohe = new PaymentInfoDAO();
        PaymentInfo paymentInfo = objPaymentDaohe.findById(obj.getPaymentInfoId());
        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(paymentInfo.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);

    }

    @Listen("onOpenBill = #lbList")
    public void onOpenBill(Event event) throws FileNotFoundException {
        VCosPaymentInfo obj = (VCosPaymentInfo) event.getData();
        BillDAO objDAOHE = new BillDAO();
        Bill objBill = objDAOHE.findById(obj.getBillId());
        if (objBill.getClass() != null) {
            Map<String, Object> arguments = new ConcurrentHashMap<>();
            // arguments.put("listVCosPaymentInfo", listVCosPaymentInfo);
            arguments.put("TypeBill", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
            // 2 sửa
            // 1 tao moi
            arguments.put("objBill", objBill);
            arguments.put("parentWindow", rapidDetailPaymentInfoAll);
            Window window = createWindow("windowsViewPaymentBill", "/Pages/module/payment/ViewBillAll.zul",
                    arguments, Window.MODAL);
            window.setMode(Window.Mode.MODAL);
            window.doModal();
        } else {
            showNotification(String.format(Constants.Notification.NOT_EXIST_WARNING, Constants.DOCUMENT_TYPE_NAME.BILL), Constants.Notification.INFO);
        }
    }

    @Listen("onRePayment = #lbList")
    public void onRePayment(Event event) throws FileNotFoundException {

        VCosPaymentInfo obj = (VCosPaymentInfo) event.getData();
        BillDAO objDAOHE = new BillDAO();
        Bill objBill = objDAOHE.findById(obj.getBillId());
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        // arguments.put("listVCosPaymentInfo", listVCosPaymentInfo);
        arguments.put("TypeBill", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
        // 2 sửa
        // 1 tao moi
        arguments.put("objBill", objBill);
        arguments.put("parentWindow", rapidDetailPaymentInfoAll);
        Window window = createWindow("windowsEditPaymentBill", "/Pages/module/payment/EditBillAll.zul",
                arguments, Window.MODAL);
        window.setMode(Window.Mode.MODAL);
        window.doModal();

    }

    @Listen("onUpdate = #lbList")
    public void onUpdate(Event event) {
        final VCosPaymentInfo obj = (VCosPaymentInfo) event.getData();
        BillDAO objDAOHE = new BillDAO();
        Bill objBill = objDAOHE.findById(obj.getBillId());
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        // arguments.put("listVCosPaymentInfo", listVCosPaymentInfo);
        arguments.put("TypeBill", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
        // 2 sửa
        // 1 tao moi
        arguments.put("objBill", objBill);
        arguments.put("parentWindow", rapidDetailPaymentInfoAll);
        Window window = createWindow("windowsEditPaymentBill", "/Pages/module/payment/EditBillAll.zul",
                arguments, Window.MODAL);
        window.setMode(Window.Mode.MODAL);
        window.doModal();

    }

    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(VCosPaymentInfo obj) {
        if ((obj.getStatuspayment() != null) && (obj.getStatuspayment() == ConstantsPayment.PAYMENT.PAY_ALREADY)) {
            return true;
        } else {
            return false;
        }

    }

    public boolean isAbleRePayment(VCosPaymentInfo obj) {

        if ((obj.getStatuspayment() != null) && (obj.getStatuspayment() == ConstantsPayment.PAYMENT.PAY_REJECTED)) {
            return true;
        } else {
            return false;
        }

    }

    public boolean isAbleView(VCosPaymentInfo obj) {

        if ((obj.getStatuspayment() == null) || (obj.getStatuspayment() == ConstantsPayment.PAYMENT.PAY_NEW)) {
            return false;
        } else {
            return true;
        }

    }

}
