/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.module.payment.BO.Bill;
import com.viettel.utils.Constants;
import com.viettel.module.payment.BO.VRtPaymentInfo;
import com.viettel.utils.LogUtils;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.utils.ConstantsPayment;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zkplus.databind.BindingListModelList;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author giangpn
 */
@SuppressWarnings({"rawtypes", "serial"})
public class RejectPaymentConfirmController extends BaseComposer {

    @WireVariable
    private String recordMode;
    Window templateCRUD; // autowired
    @Wire
    Window windowsRejectPaymentConfirm; // autowired
    @Wire("#lbWarning")
    private Label lbWarning;
    @Wire
    private Textbox txtPaymentConfirm;
    Groupbox gbTemplateContent;
    protected Listbox lboxStatus;
    private Window parentWindow;
    //private Template templateSelected;
    private AnnotateDataBinder binder;
    private BindingListModelList<Category> catList;
    List<VRtPaymentInfo> listVRtPaymentInfo;
    VRtPaymentInfo objPaymentInfo;
    int windowsType = 0;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        //setRecordMode((String) execution.getArg().get("recordMode"));
        //setTemplateSelected((Template) execution.getArg().get("selectedRecord"));
        windowsType = (int) arguments.get("windowsType");
        if (windowsType == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_MOT_HO_SO) {
            objPaymentInfo = (VRtPaymentInfo) arguments.get("VRtPaymentInfo");
        } else if (windowsType == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO) {
            listVRtPaymentInfo = (List<VRtPaymentInfo>) arguments.get("listVRtPaymentInfo");
        }
        setParentWindow((Window) arguments.get("parentWindow"));

    }

    public void onCreate$docCRUD(Event event) {
        this.binder = (AnnotateDataBinder) event.getTarget().getAttribute(
                "binder", true);
        this.binder.loadAll();
    }

    @Listen("onClick=#btnSave")
    public void btnSave(Event event) {
        //  binder.saveAll();
        if (isValidate()) {
            PaymentInfoDAO objPaymentDaohe = new PaymentInfoDAO();
            if (windowsType == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_MOT_HO_SO) {
                try {

                    PaymentInfo paymentInfo = objPaymentDaohe.findById(objPaymentInfo.getPaymentInfoId());
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_REJECTED); // trang thai moi tao
                    paymentInfo.setDateConfirm(new Date());
                    paymentInfo.setPaymentActionUser(getUserName());
                    paymentInfo.setPaymentActionUserId(getUserId());
                    paymentInfo.setPaymentConfirm(txtPaymentConfirm.getText().trim());
                    objPaymentDaohe.getSession().merge(paymentInfo);
                    objPaymentDaohe.getSession().flush();

                    if (parentWindow != null) {
                        Events.sendEvent("onReLoadPage", parentWindow, null);
                    }


                    BillDAO objDAOHE = new BillDAO();
                    Bill objBill = objDAOHE.findById(objPaymentInfo.getBillId());
                    objBill.setStatus(ConstantsPayment.PAYMENT.PAY_REJECTED);
                    objBill.setConfirmDate(new Date());
                    objBill.setConfirmUserId(getUserId());
                    objBill.setConfirmUserName(getUserName());
                    objDAOHE.saveOrUpdate(objBill);
                    windowsRejectPaymentConfirm.onClose();
                    showNotification(String.format(Constants.Notification.REJECT_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.INFO);
                } catch (Exception e) {
                    showNotification(String.format(Constants.Notification.REJECT_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                    LogUtils.addLogDB(e);
                } finally {
                }


            } else if (windowsType == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO) {
                int size = listVRtPaymentInfo.size();
                PaymentInfo TempPaymentInfo = null;
                if (size > 0) {
                    TempPaymentInfo = objPaymentDaohe.findById(listVRtPaymentInfo.get(0).getPaymentInfoId());
                }
                for (int i = 0; i < size; i++) {
                    PaymentInfo paymentInfo = objPaymentDaohe.findById(listVRtPaymentInfo.get(i).getPaymentInfoId());
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_REJECTED); // trang thai moi tao
                    paymentInfo.setDateConfirm(new Date());
                    paymentInfo.setPaymentActionUser(getUserName());
                    paymentInfo.setPaymentActionUserId(getUserId());
                    paymentInfo.setPaymentConfirm(txtPaymentConfirm.getText().trim());
                    //objPaymentDaohe.saveOrUpdate(paymentInfo);
                    objPaymentDaohe.getSession().merge(paymentInfo);

                }
                //windowsRejectPaymentConfirm.onClose();
                if(TempPaymentInfo!=null){
                BillDAO objDAOHE = new BillDAO();
                Bill objBill = objDAOHE.findById(TempPaymentInfo.getBillId());
                objBill.setStatus(ConstantsPayment.PAYMENT.PAY_REJECTED);
                objBill.setConfirmDate(new Date());
                objBill.setConfirmUserId(getUserId());
                objBill.setConfirmUserName(getUserName());
                objDAOHE.saveOrUpdate(objBill);
                }
                
                objPaymentDaohe.getSession().flush();
                if (parentWindow != null) {
                    Events.sendEvent("onClosePage", parentWindow, null);
                }
                windowsRejectPaymentConfirm.onClose();
                showNotification(String.format(Constants.Notification.APROVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.BILL), Constants.Notification.INFO);
            }


        } else {
            return;
        }
    }

    @Listen("onClick=#btnCancel")
    public void btnCancel(Event event) {
        windowsRejectPaymentConfirm.onClose();

    }

    private boolean isValidate() {
        String message;
        if ("".equals(txtPaymentConfirm.getText().trim())) {
            message = "Không được để trống lí do từ chối";
            txtPaymentConfirm.focus();
            setWarningMessage(message);
            return false;
        }
        return true;

    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public BindingListModelList<Category> getCatList() {
        return catList;
    }

    public void setCatList(BindingListModelList<Category> catList) {
        this.catList = catList;
    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
    }
}
