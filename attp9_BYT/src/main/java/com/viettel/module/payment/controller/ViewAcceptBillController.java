package com.viettel.module.payment.controller;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.module.payment.BO.Bill;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.LogUtils;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class ViewAcceptBillController extends PaymentController {

    @Wire
    private Listbox lbListPaymented;
    @Wire
    private Textbox tbPaymentName;
    @Wire
    private Window windowViewAcceptBill;
    @Wire
    Radiogroup rgTypePayment;
    @Wire
    Radio keypay, banktransfer, immediaacy;
    Bill objBill;
    @Wire
    private Label lbWarning;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    @Wire
    Groupbox gbTypePayment;
    private Window parentWindow;
    Long lsumMoney = 0L;
    // danh cho upload file
    @Wire
    Textbox txtBillNo, txtBillKey;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    Iframe iframeFile;
    private Media media;
    Long typePayment = 0L;
    boolean bUploadFile = false, clickDeletefile = false;
    private final Long deptId = getDeptId();
// xu ly luong
    // khai bao xu ly luong
    private VCosPaymentInfo vFileRtfile;
    private com.viettel.core.workflow.BO.Process processCurrent;// process dang duoc xu li
    private List<com.viettel.core.workflow.BO.Process> listProcessCurrent;
    private List<Long> listDocId, listDocType;
    private long FILE_TYPE;
    ;
    private Long DEPT_ID;
    @Wire
    private Div topToolbar;
    private List<Component> listTopActionComp;
    @Wire
    private Div bottomToolbar;
    private List<Component> listBottomActionComp;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        objBill = (Bill) arguments.get("objBill");
        lsumMoney = objBill.getTotal();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        reloadModel();
        lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));
        initThread();

    }

    @Listen("onAfterRender = #lbListPaymented")
    public void onAfterRender() {
    }
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    ListModelList<VCosPaymentInfo> lbListPaymentedModelList = new ListModelList<VCosPaymentInfo>();

    public ListModelList<VCosPaymentInfo> getListPaymentedModelList() {
        return lbListPaymentedModelList;
    }

    private void initThread() {
        Components.removeAllChildren(topToolbar);
        listTopActionComp = new ArrayList<>();
        listBottomActionComp = new ArrayList<>();
        listProcessCurrent = new ArrayList<>();
        listDocId = new ArrayList<>();
        listDocType = new ArrayList<>();
        if (listPaymented.size() > 0) {
            vFileRtfile = listPaymented.get(0);
            processCurrent = WorkflowAPI.getInstance().
                    getCurrentProcess(vFileRtfile.getFileId(), vFileRtfile.getFileType(),
                            vFileRtfile.getStatusfile(), getUserId());
            FILE_TYPE = vFileRtfile.getFileType();
//            if (getDeptId() == null) {
//                DEPT_ID = Constants.CUC_QLD_ID;
//            } else {
//                DEPT_ID = getDeptId();
//            }
            DEPT_ID = Constants.CUC_QLD_ID;
            addAllNextActions();
            loadActionsToToolbar();
        }

    }

    private void addAllNextActions() {
        Long docStatus = vFileRtfile.getStatusfile();
        Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;
        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(
                docType, docStatus, deptId);

        for (NodeToNode action : actions) {
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
    }

    private Button createButtonForAction(NodeToNode action) {
        // check if action is RETURN_PREVIOUS but parent process came from another node
        if (processCurrent != null
                && action.getType() == Constants.NODE_ASSOCIATE_TYPE.RETURN_PREVIOUS
                && action.getNextId() != processCurrent.getPreviousNodeId()) {
            return null;
        }
        // check if receiverId is diferent to current user id
        // find all process having current status of document
        List<com.viettel.core.workflow.BO.Process> listCurrentProcess = WorkflowAPI.getInstance()
                .findAllCurrentProcess(vFileRtfile.getFileId(), vFileRtfile.getFileType(),
                        vFileRtfile.getStatusfile());
        Long userId = getUserId();
        Long creatorId = vFileRtfile.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return null;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                for (com.viettel.core.workflow.BO.Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                if (!needToProcess) {
                    return null;
                }
            }
        }

        Button btn = new Button(action.getAction());

        final String actionName = action.getAction();
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action

        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = vFileRtfile.getStatusfile();
        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, false));

        EventListener<Event> event = new EventListener<Event>() {
            @Override
            public void onEvent(Event t) throws Exception {
                // xu ly khi payment luong
                String message;
                if (1 <= listPaymented.size()) {

                    for (VCosPaymentInfo app : listPaymented) {
                        // them list process
                        listProcessCurrent.add((WorkflowAPI.getInstance().
                                getCurrentProcess(app.getFileId(), app.getFileType(),
                                        app.getStatusfile(), getUserId())));
                        listDocId.add(app.getFileId());
                        listDocType.add(app.getFileType());
                        //System.out.print("gia tri"+app.getRapidTestNo());
                    }
                    Map<String, Object> data = new ConcurrentHashMap<>();
                    //data.put("typeProcess",Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);    

                    data.put("TypeBill", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
                    // 2 sửa
                    // 1 tao moi
                    data.put("objBill", objBill);
                    data.put("windowParent", windowViewAcceptBill);

                    data.put("fileId", vFileRtfile.getFileId());
                    data.put("docStatus", status);
                    data.put("nextId", nextId);
                    data.put("previousId", prevId);

                    // xu li process
                    data.put("lstAvailableNDU", lstNDUs); //nut tiep theo la chung
                    data.put("actionId", actionId);
                    data.put("actionName", actionName);
                    data.put("actionType", actionType);

                    if (processCurrent != null) {
                        data.put("process", processCurrent);
                    }
                    data.put("docId", vFileRtfile.getFileId());
                    data.put("docType", vFileRtfile.getFileType());

                    data.put("listProcess", listProcessCurrent);
                    data.put("listDocId", listDocId);
                    data.put("listDocType", listDocType);
                    createWindow("businessWindow", formName, data, Window.MODAL);
                    //createWindowView(listVCosPaymentInfo, Constants.DOCUMENT_MENU.ALL,
                    //      rapidDetailPaymentInfoAll);
                    // rapidTestPaymentInfoAll.setVisible(false);
                } else {
                    message = "\n Bạn vui lòng chọn hồ sơ";
                    showNotification(message, Constants.Notification.INFO);
                    return;
                }
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    // Hien thi cac action tren thanh toolbar top va bottom
    public void loadActionsToToolbar() {
        for (Component comp : listTopActionComp) {
            topToolbar.appendChild(comp);
        }
        for (Component comp : listBottomActionComp) {
            bottomToolbar.appendChild(comp);
        }

    }

    public void setListPaymentedModelList(ListModelList<VCosPaymentInfo> lbListPaymentedModelList) {
        this.lbListPaymentedModelList = lbListPaymentedModelList;
    }
    private List<VCosPaymentInfo> listPaymented;

    private void reloadModel() throws IOException {

        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();

        listPaymented = objDAOHE.getListRequestPaymentBillId(deptId, objBill.getBillId());
        if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
            rgTypePayment.setSelectedItem(keypay);
        } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
            rgTypePayment.setSelectedItem(banktransfer);
            txtBillKey.setValue(objBill.getBillSign());
            txtBillNo.setValue(objBill.getCode());
        } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
            rgTypePayment.setSelectedItem(immediaacy);
        }
        dbDayPayment.setValue(objBill.getPaymentDate());
        tbPaymentName.setValue(objBill.getCreatetorName());

        setTypePayment();

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
        if ((items != null) && (items.size() > 0)) {
            try {
//                Attachs mAttach = items.get(0);
//                String path = mAttach.getFullPathFile();
//                File f = new File(path);
//                String sType = mAttach.getAttachName().substring(mAttach.getAttachName().lastIndexOf(".") + 1);
//
//                byte[] buffer = new byte[(int) f.length()];
//                FileInputStream fs;
//                fs = new FileInputStream(f);
//
//                int size = fs.read(buffer);
//                fs.close();
//                ByteArrayInputStream is = new ByteArrayInputStream(buffer);
//                //AMedia amedia = new AMedia(mAttach.getAttachName(), sType, WorkflowAPI.getCtypeFile(sType), is);
//                //iframeFile.setContent(amedia);
                txtFileName.setValue(items.get(0).getAttachName());
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
        }
        lbListPaymentedModelList.addAll(listPaymented);
        lbListPaymented.setModel(lbListPaymentedModelList);

    }

    @Listen("onClick = #btnAccept")
    public void onAccept() {

        PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
        PaymentInfo paymentInfo;
        int sizeList = listPaymented.size();
        for (int i = 0; i < sizeList; i++) {

            VCosPaymentInfo app = listPaymented.get(i);
            paymentInfo = objFreePaymentInfo.findById(app.getPaymentInfoId());
            paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED); // trang thai moi tao
            paymentInfo.setDateConfirm(new Date());
            paymentInfo.setPaymentActionUser(getUserName());
            paymentInfo.setPaymentActionUserId(getUserId());
            objFreePaymentInfo.saveOrUpdate(paymentInfo);
            //System.out.print("gia tri"+app.getRapidTestNo());
        }
        BillDAO objBillDAOHE = new BillDAO();
        objBill.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED);
        objBill.setConfirmDate(new Date());
        objBill.setConfirmUserId(getUserId());
        objBill.setConfirmUserName(getUserName());
        objBillDAOHE.saveOrUpdate(objBill);
        objBillDAOHE.flush();
        showNotification(String.format(Constants.Notification.APROVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.BILL), Constants.Notification.INFO);
        windowViewAcceptBill.onClose();
        if (parentWindow != null) {
            Events.sendEvent("onReLoadPage", parentWindow, null);
        }
    }

    @Listen("onClick = #btnReject")
    public void onReject() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("listVCosPaymentInfo", listPaymented);
        arguments.put("menuType", Constants.DOCUMENT_MENU.ALL);
        arguments.put("windowsType", ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_NHIEU_HO_SO);
        arguments.put("parentWindow", windowViewAcceptBill);
        Window window = createWindow("windowsRejectPaymentConfirm", "/Pages/module/payment/rejectPaymentConfirm.zul",
                arguments, Window.MODAL);
        window.doModal();
    }

    @Listen("onClick = #btnCancel")
    public void onCancel() {
        windowViewAcceptBill.onClose();
        //Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onClosePage = #windowViewAcceptBill")
    public void onClosePage() {
        windowViewAcceptBill.onClose();
        if (parentWindow != null) {
            Events.sendEvent("onReLoadPage", parentWindow, null);
        }
        //Events.sendEvent("onVisible", parentWindow, null);
    }

    private void setTypePayment() {
        if (rgTypePayment.getSelectedItem() != null) {
            if ("keypay".equals(rgTypePayment.getSelectedItem().getId())) {
                //gbTypePayment.setVisible(false);
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
            } else if ("banktransfer".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;
                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(true);
                txtBillKey.setVisible(true);
                lbBillNo.setVisible(true);
                lbBillNoRequi.setVisible(true);
                lbBillKey.setVisible(true);
                lbBillKeyRequi.setVisible(true);

            } else if ("immediaacy".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;
                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(false);
                txtBillKey.setVisible(false);
                lbBillNo.setVisible(false);
                lbBillNoRequi.setVisible(false);
                lbBillKey.setVisible(false);
                lbBillKeyRequi.setVisible(false);
            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        setTypePayment();
        //txtSearchDeptName.setValue("Selection Made: "+rgTypePayment.getSelectedItem().getId());
    }

    private boolean isValidate() {
        String message;
        if ("".equals(tbPaymentName.getText().trim())) {
            message = "Bạn vui lòng nhập tên người thanh toán";
            tbPaymentName.focus();
            setWarningMessage(message);
            return false;
        }
        if (typePayment == 0) {
            message = "Bạn vui lòng chọn hình thức thanh toán";
            setWarningMessage(message);
            return false;
        }

        if (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {

            if ("".equals(txtBillNo.getText().trim())) {
                message = "Bạn vui lòng nhập mã hóa đơn";
                txtBillNo.focus();
                setWarningMessage(message);
                return false;
            }
            if ("".equals(txtBillKey.getText().trim())) {
                message = "Bạn vui lòng nhập ký hiệu hóa đơn";
                txtBillKey.focus();
                setWarningMessage(message);
                return false;
            }
        }
        if ((typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) || (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT)) {
            if (dbDayPayment.getValue() == null) {
                message = "Bạn vui lòng nhập ngày thanh toán";
                setWarningMessage(message);
                dbDayPayment.setFocus(true);
                return false;
            }

            if ((clickDeletefile) && (media == null)) {
                message = "Bạn vui lòng nhập file đính kèm hóa đơn";
                setWarningMessage(message);
                return false;
            }

            if ("".equals(dbDayPayment.getValue().toString())) {
                message = "Bạn vui lòng nhập ngày thanh toán";

                setWarningMessage(message);
                return false;
            }
        }

        return true;

    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
    }

    @Listen("onClick = #btnDownload")
    public void onDownload() throws IOException {

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }
}
