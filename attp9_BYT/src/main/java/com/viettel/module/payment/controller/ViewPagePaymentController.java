package com.viettel.module.payment.controller;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.payment.BO.VRtPaymentInfo;
import java.util.List;
import java.util.Map;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.databind.BindingListModelList;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class ViewPagePaymentController extends BaseComposer {

    @Wire
    private Listbox lbListDetail;
    @Wire
    private Textbox tbPaymentName;
    @Wire
    private Window windowsViewPagePayment;
    @Wire
    Radiogroup rgTypePayment;
    List<VRtPaymentInfo> listVRtPaymentInfo;
    @Wire("#lbWarning")
    private Label lbWarning;
    @Wire("#sumMoney")
    private Label sumMoney;
    @Wire
    private Textbox txtCompanyName, txtCompanyAddress;
    private Window parentWindow;
    Long lsumMoney = 0L;
    int typePayment = 0;// kieu thanh toan 1//keypay //2 chuyen khoan //3 tien mat
    int noPayment = 0;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        listVRtPaymentInfo = (List<VRtPaymentInfo>) arguments.get("listVRtPaymentInfo");
        for (int i = 0; i < listVRtPaymentInfo.size(); i++) {
            lsumMoney = lsumMoney + listVRtPaymentInfo.get(i).getCost();
        }
        noPayment = (int) arguments.get("feePaymentType");
        // FeePaymentInfoDAOHE objDAOHE = new FeePaymentInfoDAOHE();
        //feePaymentInfo = objDAOHE.findById(feePaymentInfoId);
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        BindingListModelList model = new BindingListModelList<>(listVRtPaymentInfo, true);
        model.setMultiple(true);
        lbListDetail.setModel(model);
        lbListDetail.renderAll();
        lbListDetail.setSizedByContent(true);
        sumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));
        txtCompanyName.setValue(listVRtPaymentInfo.get(0).getBusinessName());
        txtCompanyAddress.setValue(listVRtPaymentInfo.get(0).getBusinessAddress());
    }

    @Listen("onClick = #btnReject")
    public void onReject() {

        windowsViewPagePayment.onClose();
        //Events.sendEvent("onVisible", parentWindow, null);

    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
    }

    public String getDocumentType(Long documentTypeCode) {

        return WorkflowAPI.getDocumentTypeCode(documentTypeCode);
    }
}
