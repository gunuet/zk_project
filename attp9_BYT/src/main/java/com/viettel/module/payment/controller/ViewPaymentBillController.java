package com.viettel.module.payment.controller;

import com.viettel.core.base.DAO.AttachDAO;

import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class ViewPaymentBillController extends PaymentController {

    @Wire
    private Listbox lbListPaymented;
    @Wire
    private Textbox tbPaymentName;
    @Wire
    private Window windowsViewPaymentBill;
    @Wire
    Radiogroup rgTypePayment;
    @Wire
    Radio keypay, banktransfer, immediaacy;
    Bill objBill;
    @Wire
    private Label lbWarning;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    @Wire
    Groupbox gbTypePayment;
    private Window parentWindow;
    Long lsumMoney = 0L;
    // danh cho upload file
    @Wire
    Textbox txtBillNo, txtBillKey;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    Iframe iframeFile;
    private Media media;
    Long typePayment = 0L;
    int typeWindows = 1;
    boolean bUploadFile = false, clickDeletefile = false;
    private final Long deptId = getDeptId();

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        typeWindows = (int) arguments.get("TypeBill");
        if (typeWindows == ConstantsPayment.PAYMENT.PAYMENT_TAO_MOI) { //them moi
            objBill = null;
            lsumMoney = 0L;
        } else if (typeWindows == ConstantsPayment.PAYMENT.PAYMENT_CHINH_SUA) { //chinh sua

            objBill = (Bill) arguments.get("objBill");
            try {
                lsumMoney = objBill.getTotal();
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
            //rgTypePayment.setSelectedItem();   
        }

        //txtSumMoney.setValue(lsumMoney.toString());
        // FeePaymentInfoDAOHE objDAOHE = new FeePaymentInfoDAOHE();
        //feePaymentInfo = objDAOHE.findById(feePaymentInfoId);
        return super.doBeforeCompose(page, parent, compInfo);

    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    ListModelList<VCosPaymentInfo> lbListPaymentedModelList = new ListModelList<VCosPaymentInfo>();

    public ListModelList<VCosPaymentInfo> getListPaymentedModelList() {
        return lbListPaymentedModelList;
    }

    public void setListPaymentedModelList(ListModelList<VCosPaymentInfo> lbListPaymentedModelList) {
        this.lbListPaymentedModelList = lbListPaymentedModelList;
    }
    private List<VCosPaymentInfo> listPaymented;

    private void reloadModel() throws IOException {

        VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
        if (typeWindows == ConstantsPayment.PAYMENT.PAYMENT_CHINH_SUA) {
            listPaymented = objDAOHE.getListRequestPaymentBillId(deptId, objBill.getBillId());
            if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
                rgTypePayment.setSelectedItem(keypay);
            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
                rgTypePayment.setSelectedItem(banktransfer);
                txtBillKey.setValue(objBill.getBillSign());
                txtBillNo.setValue(objBill.getCode());
            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                rgTypePayment.setSelectedItem(immediaacy);
            }
            dbDayPayment.setValue(objBill.getPaymentDate());
            tbPaymentName.setValue(objBill.getCreatetorName());

            setTypePayment();

            AttachDAOHE attDAOHE = new AttachDAOHE();
            List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
            if ((items != null) && (items.size() > 0)) {
                try {
//                    Attachs mAttach = items.get(0);
//                    String path = mAttach.getFullPathFile();
//                    File f = new File(path);
//                    String sType = mAttach.getAttachName().substring(mAttach.getAttachName().lastIndexOf(".") + 1);
//
//                    byte[] buffer = new byte[(int) f.length()];
//                    FileInputStream fs;
//                    fs = new FileInputStream(f);
//
//                    fs.read(buffer);
//                    fs.close();
//                    ByteArrayInputStream is = new ByteArrayInputStream(buffer);
//                    AMedia amedia = new AMedia(mAttach.getAttachName(), sType, WorkflowAPI.getCtypeFile(sType), is);
//                    //iframeFile.setContent(amedia);
                    txtFileName.setValue(items.get(0).getAttachName());
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                }
            }

        } else if (typeWindows == 1) {
            listPaymented = new ArrayList<VCosPaymentInfo>();
        }
        lbListPaymentedModelList.addAll(listPaymented);

//        BindingListModelList model = new BindingListModelList<>(listPaymented, true);
//        lbListPaymented.setModel(model);
        lbListPaymented.setModel(lbListPaymentedModelList);

    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        reloadModel();

        lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney, "###,###,###"));

    }

    @Listen("onClick = #btnReject")
    public void onReject() {

        windowsViewPaymentBill.onClose();
        //Events.sendEvent("onVisible", parentWindow, null);

    }

    private void setTypePayment() {
        if (rgTypePayment.getSelectedItem() != null) {
            if ("keypay".equals(rgTypePayment.getSelectedItem().getId())) {
                //gbTypePayment.setVisible(false);
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
            } else if ("banktransfer".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;
                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(true);
                txtBillKey.setVisible(true);
                lbBillNo.setVisible(true);
                lbBillNoRequi.setVisible(true);
                lbBillKey.setVisible(true);
                lbBillKeyRequi.setVisible(true);

            } else if ("immediaacy".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;
                gbTypePayment.setVisible(true);
                txtBillNo.setVisible(false);
                txtBillKey.setVisible(false);
                lbBillNo.setVisible(false);
                lbBillNoRequi.setVisible(false);
                lbBillKey.setVisible(false);
                lbBillKeyRequi.setVisible(false);
            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        setTypePayment();
        //txtSearchDeptName.setValue("Selection Made: "+rgTypePayment.getSelectedItem().getId());
    }

    @Listen("onClick = #btnDownload")
    public void onDownload() throws IOException {

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }
}
