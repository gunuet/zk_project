package com.viettel.module.payment.controller.single;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.utils.Constants;
import com.viettel.utils.ConvertFileUtils;
import com.viettel.utils.FileUtil;
import com.viettel.module.payment.BO.Bill;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.module.payment.parent.PaymentController;
import static com.viettel.module.payment.parent.PaymentController.numberToString;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author quynhhv1 Doanh nghiep thanh toan 1 ho so
 */
public class PaymentSingleBusinessController extends PaymentController {

    @Wire
    private Textbox txtPaymentName;
    @Wire
    private Textbox txtPaymentAddress;
    @Wire
    private Window businessWindow;
    @Wire
    Radiogroup rgTypePayment;
    @Wire("#lbWarning")
    private Label lbWarning;
    @Wire("#toplbWarning")
    private Label toplbWarning;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    @Wire("#lbtextMoney")
    private Label lbtextMoney;
    @Wire
    Textbox txtBillNo, txtBillKey;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    Image imgDelFile;
    @Wire
    Label lbdeletefile;
    @Wire
    Textbox txtValidate;
    @Wire
    private Label lbbookNumber, lbMahoso, lbTensp;
    @Wire
    Radio radioKeypay, radioBanktransfer, radioImmediaacy;
    private Media media;
    private Attachs attachCurrent;
    Long typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;// kieu thanh toan 1//keypay //2 chuyen khoan //3 tien mat
    int noPayment = 0;
    private Window parentWindow;
    Long lsumMoney = 0L;
    Long docId = null;
    Long docType = null;
    //Bill objBill = null;
    List<VCosPaymentInfo> listVCosPaymentInfo;
    VCosPaymentInfo vCosPaymentInfo;
    BookDocument mBook;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        listVCosPaymentInfo = new ArrayList<>();
        listVCosPaymentInfo.clear();
        //sTypeView=(String) arguments.get("typeView");
        VCosPaymentInfoDAO vCosDao = new VCosPaymentInfoDAO();
        docId = (Long) arguments.get("docId");
        docType = (Long) arguments.get("docType");

        listVCosPaymentInfo = vCosDao.getListFileId(docId);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onClick = #onPrint")
    public void onPrint(Event e) {
        // createWindowViewPrint();
        ExportFileDAO exportFileDAO = new ExportFileDAO();
        // giangnh20, su dung ham nay de dau phan cach nghin la dau . thay vi dau ,
        String formatedNumberText = formatNumber(lsumMoney, "###,##0");
        if (!"".equals(formatedNumberText)) {
            formatedNumberText += " đồng";
        }
        exportFileDAO.exportPhieuBaoThu(mBook.getCreateDate(), vCosPaymentInfo.getBusinessName().trim(), vCosPaymentInfo.getBusinessAddress().trim(),
                formatedNumberText, numberToString(lsumMoney),
                lbTensp.getValue().trim(), lbMahoso.getValue().trim(), lbbookNumber.getValue().trim(), mBook.getCreatorName(), "", true);
    }

    private void createWindowViewPrint() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("docId", docId);
        arguments.put("docType", docType);
        arguments.put("windowParent", businessWindow);
        Window window = createWindow("businessWindowPrint", "/Pages/module/payment/paymentSingleBusinessPrint.zul",
                arguments, Window.MODAL);
        window.doModal();

    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        BookDocumentDAOHE bookDao = new BookDocumentDAOHE();
        if ((docId != null) && (docType != null)) {
            mBook = bookDao.getBookInFromDocumentId(docId, docType);
            if (mBook != null) {
                lbbookNumber.setValue(mBook.getBookNumber().toString());
            } else {
                lbbookNumber.setValue("");
            }
        }

//        if (objBill != null) {
//            if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
//                rgTypePayment.setSelectedItem(radioKeypay);
//            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
//                rgTypePayment.setSelectedItem(radioBanktransfer);
//            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
//                rgTypePayment.setSelectedItem(radioImmediaacy);
//               
//            }
//            txtBillKey.setValue(objBill.getBillSign());
//            txtBillNo.setValue(objBill.getCode());
//            dbDayPayment.setValue(objBill.getPaymentDate());
//            txtPaymentName.setValue(objBill.getCreatetorName());
//            AttachDAOHE attDAOHE = new AttachDAOHE();
//            List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
//            if ((items != null) && (items.size() > 0)) {
//                String path = items.get(0).getFullPathFile();
//                File f = new File(path);
//                try {
//                    media = new AMedia(f, null, null);
//                } catch (FileNotFoundException ex) {
//
//                    Logger.getLogger(EditPaymentBillController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                txtFileName.setValue(items.get(0).getAttachName());
//                imgDelFile.setVisible(true);
//                lbdeletefile.setVisible(true);
//            }
//        }
        if (listVCosPaymentInfo.size() > 0) {
            vCosPaymentInfo = listVCosPaymentInfo.get(0);
            lsumMoney = vCosPaymentInfo.getCost();
            lbMahoso.setValue(vCosPaymentInfo.getNswFileCode());
            lbTensp.setValue(vCosPaymentInfo.getProductName());
            txtPaymentAddress.setValue(vCosPaymentInfo.getBusinessAddress());
        }
//        lbSumMoney.setValue(CommonFns.formatNumber(lsumMoney, "###,###,###") + " đồng ");
        // giangnh20, su dung ham nay de dau phan cach nghin la dau . thay vi dau ,
        String formatedMoneyText = formatNumber(lsumMoney, "###,##0");
        lbSumMoney.setValue("".equals(formatedMoneyText) ? "" : formatedMoneyText + " đồng");
        txtValidate.setValue("0");
        dbDayPayment.setValue(new Date());
        lbtextMoney.setValue(numberToString(lsumMoney));
    }

    public void onAccept() {
        if (isValidate()) {
            // lay ngay hien tai
            Date date = new Date();
            // update trong bang bill
            BillDAO objBillDAOHE = new BillDAO();
            Bill objBill = new Bill();
            objBill.setCreatetorName(txtPaymentName.getText().trim());
            objBill.setCreateDate(date);
            objBill.setPaymentDate(dbDayPayment.getValue());
            objBill.setTotal(lsumMoney);
            objBill.setCreatorId(getUserId());
            objBill.setStatus(ConstantsPayment.PAYMENT.PAY_ALREADY);
            objBill.setPaymentTypeId(Long.valueOf(typePayment));
            objBill.setCreatorAddress(txtPaymentAddress.getText().trim());
            objBillDAOHE.saveOrUpdate(objBill);
            Long idBill = objBill.getBillId();
            //update trong paymentInfo
            PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
            PaymentInfo paymentInfo;
            int sizeList = listVCosPaymentInfo.size();
            for (int i = 0; i < sizeList; i++) {
                paymentInfo = objFreePaymentInfo.findById(listVCosPaymentInfo.get(i).getPaymentInfoId());
                paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_ALREADY);
                paymentInfo.setBillId(idBill);
                paymentInfo.setUpdateDate(date);
                paymentInfo.setPaymentDate(dbDayPayment.getValue());
                paymentInfo.setPaymentTypeId(Long.valueOf(typePayment));
                objFreePaymentInfo.getSession().merge(paymentInfo);
            }
            //update attach File
            try {
                AttachDAO attDAO = new AttachDAO();
                attDAO.saveFileAttach(media, idBill, Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY, null);
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }

            showNotification("Bạn đã thanh toán thành công. ",
                    Constants.Notification.INFO);
            txtValidate.setValue("1");
            if (parentWindow != null) {
                Events.sendEvent("onReloadPage", parentWindow, null);
            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        switch (rgTypePayment.getSelectedItem().getId()) {
            case "radioKeypay":
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
                break;
            case "radioImmediaacy":
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;
                break;
            case "radioBanktransfer":
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;
                break;
        }
    }

    private boolean isValidate() {
        String message;
        if (typePayment == 0) {
            message = "Bạn vui lòng chọn hình thức thanh toán";
            setWarningMessage(message);
            return false;
        }
        if ("".equals(txtPaymentName.getText().trim())) {
            message = "Bạn vui lòng nhập tên người thanh toán";
            txtPaymentName.focus();
            setWarningMessage(message);
            return false;
        }

        if ((typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) || (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT)) {
            if (dbDayPayment.getValue() == null) {
                message = "Bạn vui lòng nhập ngày thanh toán";
                setWarningMessage(message);
                dbDayPayment.setFocus(true);
                return false;
            } else {
                Date mDate = new Date();
                if (dbDayPayment.getValue().after(mDate)) {
                    message = "Ngày thanh toán phải nhỏ hơn ngày hiện tại";
                    setWarningMessage(message);
                    dbDayPayment.setFocus(true);
                    return false;
                }
            }

            if ("".equals(dbDayPayment.getValue().toString())) {
                message = "Bạn vui lòng nhập ngày thanh toán";
                setWarningMessage(message);
                return false;
            }
        }
        return true;
    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
        toplbWarning.setValue(message);
    }

    @Listen("onUpload = #btnUpload")
    public void onUpload(UploadEvent evt) throws IOException {

        media = evt.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            String sExt = ResourceBundleUtil.getString("extend_file", "config");
            showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                    Constants.Notification.WARNING);
            return;
        }
        if (attachCurrent != null) {
            Messagebox.show("Bạn có đồng ý thay thế tệp biểu mẫu cũ?", "Thông báo", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event evt) throws InterruptedException {
                    if (Messagebox.ON_OK.equals(evt.getName())) {
                        txtFileName.setValue(media.getName());
                        imgDelFile.setVisible(true);
                        lbdeletefile.setVisible(true);
                        String fullPath = attachCurrent.getAttachPath() + attachCurrent.getAttachName();
                        //xóa file cũ nếu có
                        File f = new File(fullPath);
                        if (f.exists()) {
                            ConvertFileUtils.deleteFile(fullPath);
                        }
                        AttachDAOHE attachDAOHE = new AttachDAOHE();
                        attachDAOHE.delete(attachCurrent);
                    }
                }
            });
        } else {
            txtFileName.setValue(media.getName());
            imgDelFile.setVisible(true);
            lbdeletefile.setVisible(true);
        }
    }

    @Listen("onClick = #imgDelFile")
    public void onClickDelete(Event ev) {
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
        lbdeletefile.setVisible(false);
    }

    @Listen("onClick = #lbdeletefile")
    public void onClicklbDelete(Event ev) {
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
        lbdeletefile.setVisible(false);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        onAccept();
    }
}
