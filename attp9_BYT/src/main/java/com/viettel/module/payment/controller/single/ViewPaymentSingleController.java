package com.viettel.module.payment.controller.single;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.utils.Constants;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.VCosPaymentInfoDAO;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author quynhhv1 Doanh nghiep thanh toan 1 ho so
 */
public class ViewPaymentSingleController extends PaymentController {

    @Wire
    private Label txtPaymentName;
    @Wire
    private Label txtPaymentAddress;
    @Wire
    private Window viewPaymentWindow;
    @Wire("#lbWarning")
    private Label lbWarning;
    @Wire("#toplbWarning")
    private Label toplbWarning;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    @Wire("#lbtextMoney")
    private Label lbtextMoney;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;
    @Wire
    Label txtFileName;
    @Wire
    Textbox txtValidate;
    @Wire
    private Label lbbookNumber, lbMahoso, lbTensp, typePaymentZul;
    Long typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;// kieu thanh toan 1//keypay //2 chuyen khoan //3 tien mat
    int noPayment = 0;
    private Window parentWindow;
    Long lsumMoney = 0L;
    Long docId = null;
    Long docType = null;
    Bill objBill = null;
    List<VCosPaymentInfo> listVCosPaymentInfo;
    VCosPaymentInfo vCosPaymentInfo;
    private Desktop desktop;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("windowParent");
        listVCosPaymentInfo = new ArrayList<>();
        listVCosPaymentInfo.clear();
        //sTypeView=(String) arguments.get("typeView");
        VCosPaymentInfoDAO vCosDao = new VCosPaymentInfoDAO();
        docId = (Long) arguments.get("docId");
        docType = (Long) arguments.get("docType");

        listVCosPaymentInfo = vCosDao.getListFileId(docId);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        BookDocumentDAOHE bookDao = new BookDocumentDAOHE();
        if ((docId != null) && (docType != null)) {
            BookDocument mBook = bookDao.getBookInFromDocumentId(docId, docType);
            if (mBook != null) {
                lbbookNumber.setValue(mBook.getBookNumber().toString());
            } else {
                lbbookNumber.setValue("");
            }
        }

        if (listVCosPaymentInfo.size() > 0) {
            vCosPaymentInfo = listVCosPaymentInfo.get(0);
            lsumMoney = vCosPaymentInfo.getCost();
            lbMahoso.setValue(vCosPaymentInfo.getNswFileCode());
            lbTensp.setValue(vCosPaymentInfo.getProductName());
            txtPaymentAddress.setValue(vCosPaymentInfo.getBusinessAddress());


            BillDAO mBillDao = new BillDAO();
            objBill = mBillDao.findById(vCosPaymentInfo.getBillId());

            if (objBill != null) {
                if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
                    typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
                } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
                    typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;
                } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                    typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;

                }
                AttachDAOHE attDAOHE = new AttachDAOHE();
                List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
                if ((items != null) && (items.size() > 0)) {
                    txtFileName.setValue(items.get(0).getAttachName());

                }

                typePaymentZul.setValue(getStatusPayment(typePayment));
                dbDayPayment.setValue(objBill.getPaymentDate());
                txtPaymentName.setValue(objBill.getCreatetorName());
            }
        }
        lbSumMoney.setValue(CommonFns.formatNumber(lsumMoney, "###,###,###") + " đồng ");
        txtValidate.setValue("0");
        lbtextMoney.setValue(numberToString(lsumMoney));
    }

    @Listen("onClick = #btnReject")
    public void onReject() {
        viewPaymentWindow.onClose();
    }
    
    @Listen("onClick = #btnDownload")
    public void onDownload() throws IOException {
        if(objBill==null){
              showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }
}
