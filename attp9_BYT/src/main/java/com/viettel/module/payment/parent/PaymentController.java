/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.parent;

import com.viettel.core.workflow.*;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.utils.LogUtils;
import java.text.NumberFormat;
import java.util.Date;
import org.zkoss.xel.fn.CommonFns;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;

/**
 *
 * @author duv
 */
public class PaymentController extends BusinessController {

    private String[] NumberString = new String[]{"không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười", "trăm", "nghìn", "triệu", "tỷ"};
    private String[] SubNumberString = new String[]{"mốt", "lăm", "lẻ", "mươi"};

    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        
    }

    public String viewCreaterAccept(VCosPaymentInfo obj) {
        String temp = "";
        if ((obj.getPaymentActionUser() != null) && (!obj.getPaymentActionUser().isEmpty())) {
            temp = temp + obj.getPaymentActionUser();
        }
        if ((obj.getDateConfirm() != null)) {
            temp = temp + " (" + getFormatDate(obj.getDateConfirm()) + ")";
        }
        if ((obj.getPaymentConfirm() != null) && (!obj.getPaymentConfirm().isEmpty())) {
            temp = temp + " \"" + obj.getPaymentConfirm() + "\"";
        }
        return temp;
    }

    public String viewnswFileCodeAndStatus(VCosPaymentInfo obj) {
        String temp = "";
        if ((obj.getNswFileCode() != null) && (!obj.getNswFileCode().isEmpty()) && (!"null".equals(obj.getNswFileCode()))) {
            temp = temp + obj.getNswFileCode();
        }
        if ((obj.getStatusfile() != null)) {
            if (!"null".equals(obj.getStatusfile())) {
                temp = temp + " (" + WorkflowAPI.getStatusName(obj.getStatusfile()) + ")";
            } else {
                temp = temp + WorkflowAPI.getStatusName(obj.getStatusfile());
            }
        }
        return temp;
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    public String getStatusPayment(Long status) {
        String statusPaymentName = "";
        if (status != null) {
            if (status == ConstantsPayment.PAYMENT.PAY_NEW) {
                statusPaymentName = getLabelCos("pay_new");
            } else if (status == ConstantsPayment.PAYMENT.PAY_ALREADY) {
                statusPaymentName = getLabelCos("pay_already");
            } else if (status == ConstantsPayment.PAYMENT.PAY_CONFIRMED) {
                statusPaymentName = getLabelCos("pay_confirmed");
            } else if (status == ConstantsPayment.PAYMENT.PAY_REJECTED) {
                statusPaymentName = getLabelCos("pay_rejected");
            }
        }
        return statusPaymentName;
    }

    public String getStatusBill(Long status) {

        return getStatusPayment(status);
    }

    public String getDocumentType(Long documentTypeCode) {

        return WorkflowAPI.getDocumentTypeCode(documentTypeCode);
    }

    public String getFormatDate(Date mDate) {
        String sTemp = "";
        if (mDate != null) {
            sTemp = CommonFns.formatDate(mDate, "dd/MM/yyyy");
        }
        return sTemp;
    }

    public String getPaymentType(Long paymentType) {
        String sTemp = "";
        if (paymentType == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
            sTemp = getLabelCos("pay_type_code_keypay");
        } else if (paymentType == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
            sTemp = getLabelCos("pay_type_code_chuyenkhoan");
        } else if (paymentType == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
            sTemp = getLabelCos("pay_type_code_tienmat");
        }
        return sTemp;
    }

    public static String numberToStringMoney(String number) {
// test number = "1234567890";
        if (number == null || number.trim().length() == 0) {
            return "";
        }
        int t, l = number.length();
        t = l - 1;
        String[] dv = new String[]{"nghìn", "triệu", "tỷ", "nghìn tỷ", "triệu tỷ"};
        String[] tl = new String[]{"mười", "trăm"};
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < l; i++) {
            buffer.append(number.charAt(i)).append(" ");
            int k = (t - 1) / 3;
            if (k >= 0 && t > 0) {
                if (t % 3 == 0) {
                    buffer.append(dv[k]).append(" ");
                } else {
                    buffer.append(tl[(t % 3) - 1]).append(" ");
                }
                --t;
            }
        }
        number = buffer.toString();
// truong hop dc biet
        number = number.replaceAll("mười 0", "mười").replaceAll("1 mười 5", "mười lăm").
                replaceAll("1 mười", "mười").replaceAll("0 mười", "lẻ");
// thay so
        number = number.replaceAll("1", "một").replaceAll("2", "hai").replaceAll("3", "ba").
                replaceAll("4", "bốn").replaceAll("5", "năm").replaceAll("6", "sáu").
                replaceAll("7", "bảy").replaceAll("8", "tám").replaceAll("9", "chín").replaceAll("0", "không");
        LogUtils.addLog("\n\n " + number + "\n\n");
        return number;
    }

    public static String numberToString(Long number) {
        String sNumber = formatNumberForRead(number);
        // Tao mot bien tra ve
        String sReturn = "";
        // Tim chieu dai cua chuoi
        int iLen = sNumber.length();
        // Lat nguoc chuoi nay lai
        String sNumber1 = "";
        for (int i = iLen - 1; i >= 0; i--) {
            sNumber1 += sNumber.charAt(i);
        }
        // Tao mot vong lap de doc so
        // Tao mot bien nho vi tri cua sNumber
        int iRe = 0;
        do {
            // Tao mot bien cat tam
            String sCut;
            if (iLen > 3) {
                sCut = sNumber1.substring((iRe * 3), (iRe * 3) + 3);
                sReturn = Read(sCut, iRe) + sReturn;
                iRe++;
                iLen -= 3;
            } else {
                sCut = sNumber1.substring((iRe * 3), (iRe * 3) + iLen);
                sReturn = Read(sCut, iRe) + sReturn;
                break;
            }
        } while (true);
        if (sReturn.length() > 1) {
            sReturn = sReturn.substring(0, 1).toUpperCase() + sReturn.substring(1);
        }
        sReturn = sReturn + "đồng";
        return sReturn;
    }

    // Khoi tao ham Read
    public static String Read(String sNumber, int iPo) {
        // Tao mot bien tra ve
        String sReturn = "";
        // Tao mot bien so
        String sPo[] = {"", "ngàn" + " ",
            "triệu" + " ", "tỷ" + " "};
        String sSo[] = {"không" + " ", "một" + " ",
            "hai" + " ", "ba" + " ",
            "bốn" + " ", "năm" + " ",
            "sáu" + " ", "bảy" + " ",
            "tám" + " ", "chín" + " "};
        String sDonvi[] = {"", "mươi" + " ",
            "trăm" + " "};
        // Tim chieu dai cua chuoi
        int iLen = sNumber.length();
        // Tao mot bien nho vi tri doc
        int iRe = 0;
        // Tao mot vong lap de doc so
        for (int i = 0; i < iLen; i++) {
            String sTemp = "" + sNumber.charAt(i);
            int iTemp = Integer.parseInt(sTemp);
            // Tao mot bien ket qua
            String sRead = "";
            // Kiem tra xem so nhan vao co = 0 hay ko
            if (iTemp == 0) {
                switch (iRe) {
                    case 0:
                        break;// Khong lam gi ca
                    case 1: {
                        if (Integer.parseInt("" + sNumber.charAt(0)) != 0) {
                            sRead = "lẻ" + " ";
                        }
                        break;
                    }
                    case 2: {
                        if (Integer.parseInt("" + sNumber.charAt(0)) != 0
                                && Integer.parseInt("" + sNumber.charAt(1)) != 0) {
                            sRead = "không trăm" + " ";
                        }
                        break;
                    }
                }
            } else if (iTemp == 1) {
                switch (iRe) {
                    case 1:
                        sRead = "mười" + " ";
                        break;
                    default:
                        sRead = "một" + " " + sDonvi[iRe];
                        break;
                }
            } else if (iTemp == 5) {
                switch (iRe) {
                    case 0: {
                        if (sNumber.length() <= 1) {
                            sRead = "năm" + " ";
                        } else if (Integer.parseInt("" + sNumber.charAt(1)) != 0) {
                            sRead = "lăm" + " ";
                        } else {
                            sRead = "năm" + " ";
                        }
                        break;
                    }
                    default:
                        sRead = sSo[iTemp] + sDonvi[iRe];
                }
            } else {
                sRead = sSo[iTemp] + sDonvi[iRe];
            }

            sReturn = sRead + sReturn;
            iRe++;
        }
        if (sReturn.length() > 0) {
            sReturn += sPo[iPo];
        }

        return sReturn;
    }

    public static String formatNumberForRead(double number) {
        NumberFormat nf = NumberFormat.getInstance();
        String temp = nf.format(number);
        String strReturn = "";
        int slen = temp.length();
        for (int i = 0; i < slen; i++) {
            if (".".equals(String.valueOf(temp.charAt(i)))) {
                break;
            } else if (Character.isDigit(temp.charAt(i))) {
                strReturn += String.valueOf(temp.charAt(i));
            }
        }
        return strReturn;

    }

}
