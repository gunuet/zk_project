/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.search;

import com.viettel.module.rapidtest.DAO.*;
import com.viettel.core.base.DAO.BaseComposer;
import org.joda.time.LocalDate;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.DateConstraint;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author ChucHV
 */
public class PaymentBillSearch extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    private Datebox dbFromDayBill, dbToDayBill;
    @Wire
    private Textbox billId, createtorName;
    @Wire
    private Groupbox fullSearchGbx;

    @SuppressWarnings({"unchecked", "rawtypes"})

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
        dbFromDayBill.setConstraint(new DateConstraint(dbToDayBill, "before"));
        dbToDayBill.setConstraint(new DateConstraint(dbFromDayBill, "after"));
        
    }

    @Listen("onChange = #dbFromDayBill, #dbToDayBill")
    public void onChangeDate(InputEvent event) {
         LocalDate fromDate=null,toDate=null;
        if(dbFromDayBill.getValue()!=null)
         fromDate = LocalDate.fromDateFields(dbFromDayBill.getValue());
        
        if(dbToDayBill.getValue()!=null)
         toDate = LocalDate.fromDateFields(dbToDayBill.getValue());
        
        if((fromDate!=null)&&(toDate!=null))
        if (toDate.isBefore(fromDate)) {
            if (event.getTarget().equals(dbFromDayBill)) {
                throw new WrongValueException(event.getTarget(),
                        "Giá trị trong trường Từ ngày không được sau trường Đến ngày");
            } else if (event.getTarget().equals(dbToDayBill)) {
                throw new WrongValueException(event.getTarget(),
                        "Giá trị trong trường Đến ngày không được trước trường Từ ngày");
            }
        }
    }
}
