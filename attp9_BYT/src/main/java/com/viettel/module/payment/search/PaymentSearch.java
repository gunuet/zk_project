/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.search;

import com.viettel.module.rapidtest.DAO.*;
import com.viettel.core.base.DAO.BaseComposer;
import org.joda.time.LocalDate;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.DateConstraint;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author ChucHV
 */
public class PaymentSearch extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    private Datebox dbFromDayPayment, dbToDayPayment;
    @Wire
    private Textbox tbNSWFileCode, tbRapidTestNo;
    @Wire
    private Listbox lboxStatus,lboxDocumentTypeCode,lboxOrder;
    
    @Wire
    private Groupbox fullSearchGbx;

    @SuppressWarnings({"unchecked", "rawtypes"})

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
        dbFromDayPayment.setConstraint(new DateConstraint(dbToDayPayment, "before"));
        dbToDayPayment.setConstraint(new DateConstraint(dbFromDayPayment, "after"));
        
    }

    @Listen("onChange = #dbFromDayPayment, #dbToDayPayment")
    public void onChangeDate(InputEvent event) {
         LocalDate fromDate=null,toDate=null;
        if(dbFromDayPayment.getValue()!=null)
         fromDate = LocalDate.fromDateFields(dbFromDayPayment.getValue());
        
        if(dbToDayPayment.getValue()!=null)
         toDate = LocalDate.fromDateFields(dbToDayPayment.getValue());
        
        if((fromDate!=null)&&(toDate!=null))
        if (toDate.isBefore(fromDate)) {
            if (event.getTarget().equals(dbFromDayPayment)) {
                throw new WrongValueException(event.getTarget(),
                        "Giá trị trong trường Từ ngày không được sau trường Đến ngày");
            } else if (event.getTarget().equals(dbToDayPayment)) {
                throw new WrongValueException(event.getTarget(),
                        "Giá trị trong trường Đến ngày không được trước trường Từ ngày");
            }
        }
    }
}
