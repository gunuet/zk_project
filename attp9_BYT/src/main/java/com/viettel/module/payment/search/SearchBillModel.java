/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.search;

import java.util.Date;

/**
 *
 * @author ChucHV
 */
public class SearchBillModel {
     private int menuType;
   
    private Date createDateFrom;
    private Date createDateTo;
    private Long billId;
    private String createtorName;
    private Long typePayment;
    private Long paymentId=null;

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }
    
    
    private Long TypeOrder;

    public Long getTypeOrder() {
        return TypeOrder;
    }

    public void setTypeOrder(Long TypeOrder) {
        this.TypeOrder = TypeOrder;
    }

    public SearchBillModel() {
    }

    public int getMenuType() {
        return menuType;
    }

    public void setMenuType(int menuType) {
        this.menuType = menuType;
    }

    public Date getCreateDateFrom() {
        return createDateFrom;
    }

    public void setCreateDateFrom(Date createDateFrom) {
        this.createDateFrom = createDateFrom;
    }

    public Date getCreateDateTo() {
        return createDateTo;
    }

    public void setCreateDateTo(Date createDateTo) {
        this.createDateTo = createDateTo;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getCreatetorName() {
        return createtorName;
    }

    public void setCreatetorName(String createtorName) {
        this.createtorName = createtorName;
    }

    public Long getTypePayment() {
        return typePayment;
    }

    public void setTypePayment(Long typePayment) {
        this.typePayment = typePayment;
    }

   

}
