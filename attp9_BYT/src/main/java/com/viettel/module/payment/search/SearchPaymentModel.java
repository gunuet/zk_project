/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.payment.search;

import java.util.Date;

/**
 *
 * @author ChucHV
 */
public class SearchPaymentModel {

    private Long bookId;
    private int menuType;
    
    private Date createDateFrom;
    private Date createDateTo;
    private String nSWFileCode;
    private String brandName;
    private String productName;
    private Long statusFile;
    private Long statusPayment;
    private Long TypeOrder;
    
    private Long documentTypeCode;
    private Long nameCost;
    
    
    
    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
   

    public Long getStatusFile() {
        return statusFile;
    }

    public void setStatusFile(Long statusFile) {
        this.statusFile = statusFile;
    }

    public Long getNameCost() {
        return nameCost;
    }

    public void setNameCost(Long nameCost) {
        this.nameCost = nameCost;
    }
    

    public Long getTypeOrder() {
        return TypeOrder;
    }

    public void setTypeOrder(Long TypeOrder) {
        this.TypeOrder = TypeOrder;
    }

    public SearchPaymentModel() {
    }

    public int getMenuType() {
        return menuType;
    }

    public void setMenuType(int menuType) {
        this.menuType = menuType;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Date getCreateDateFrom() {
        return createDateFrom;
    }

    public void setCreateDateFrom(Date createDateFrom) {
        this.createDateFrom = createDateFrom;
    }

    public Date getCreateDateTo() {
        return createDateTo;
    }

    public void setCreateDateTo(Date createDateTo) {
        this.createDateTo = createDateTo;
    }

    public String getnSWFileCode() {
        return nSWFileCode;
    }

    public void setnSWFileCode(String nSWFileCode) {
        this.nSWFileCode = nSWFileCode;
    }

    public String getRapidTestNo() {
        return brandName;
    }

    public void setRapidTestNo(String rapidTestNo) {
        this.brandName = rapidTestNo;
    }

    public Long getStatusPayment() {
        return statusPayment;
    }

    public void setStatusPayment(Long statusPayment) {
        this.statusPayment = statusPayment;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }
}
