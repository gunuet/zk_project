package com.viettel.module.qrcode;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.importOrder.BO.VFIofIopP;
import com.viettel.module.importOrder.Controller.QrCodeController;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.importOrder.DAO.VFIofIopPDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

/**
 *
 * @author
 */
public class QrCodePageController extends BaseComposer {

    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #txtFileCode")
    private Textbox txtFileCode;
    @Wire("#incSearchFullForm #txtBusinessName")
    private Textbox txtBusinessName;
    @Wire("#incSearchFullForm #txtTaxCode")
    private Textbox txtTaxCode;
    @Wire("#incSearchFullForm #txtReceiveNo")
    private Textbox txtReceiveNo;
    @Wire("#incSearchFullForm #txtConfirmAnnounceNo")
    private Textbox txtConfirmAnnounceNo;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    Paging userPagingBottom;

    VFIofIopP searchForm;

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
        this.getPage().setTitle(ResourceBundleUtil.getString("title"));
        searchForm = new VFIofIopP();
        onSearch();
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {
        userPagingBottom.setActivePage(0);
        search(0);
    }

    private void search(int actp) {
        int take = userPagingBottom.getPageSize();
        int start = actp * userPagingBottom.getPageSize();
        PagingListModel plm;
        searchForm.setFileCode(txtFileCode.getValue());
        searchForm.setTaxCode(txtTaxCode.getValue().trim());
        searchForm.setBusinessName(txtBusinessName.getValue().trim());
        searchForm.setReceiveNo(txtReceiveNo.getValue().trim());
        searchForm.setConfirmAnnounceNo(txtConfirmAnnounceNo.getValue().trim());
        VFIofIopPDAO fDAO = new VFIofIopPDAO();
        plm = fDAO.searchDataPermited(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbList.setModel(lstModel);
        lbList.setMultiple(true);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        search(userPagingBottom.getActivePage());
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public String getLabel(String key) {
        try {
            return ResourceBundleUtil.getString(key, "language_XNN_vi");

        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLogDB(ex);
        }
        return "";
    }
//hien thi danh sach tim kiem

    @Listen("onShowFullSearch=#importOrderAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
        }
    }
//tao qr code

    @Listen("onClick = #incSearchFullForm #btnCreateQrCode")
    public void createQrCode() {
        VFIofIopPDAO vFIofIopPDAO = new VFIofIopPDAO();
        List<VFIofIopP> lstFiles = vFIofIopPDAO.findProductNotHaveQrCode();
        QrCodeController qcc = new QrCodeController();
        int count = 0;
        if (lstFiles != null && !lstFiles.isEmpty()) {
            for (int i = 0; i < lstFiles.size(); i++) {
                qcc.createQrCodeByVFIofIopP(lstFiles.get(i));
                count++;
            }
        }
        showNotification(String.format(getLabelCos("qrcode_dataothanhcong"), count), Constants.Notification.INFO);
    }
//tai qr code

    @Listen("onDownloadQrCode =#incList #lbList")
    public void onDownloadQrCode(Event event) {
        VFIofIopP obj = (VFIofIopP) event.getData();

        QrCodeController qrcodeconsole = new QrCodeController();
        qrcodeconsole.onDownloadQrCodeByProductId(obj.getProductId());
    }
//ch eck ton tai qr code

    public int CheckHaveQrCode(Long productId) {
        if (!"admin".equals(getUserName())) {
            return Constants.CHECK_VIEW.NOT_VIEW;
        }
        ImportOrderProductDAO iopDao = new ImportOrderProductDAO();
        return iopDao.checkHaveQrCode(productId);
    }
}
