/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Linhdx
 */
@Entity
@Table(name = "PROCEDURE_FEE_SUBPROCEDURE")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "ProcedureFeeSubprocedure.findAll", query = "SELECT p FROM ProcedureFeeSubprocedure p"),
		@NamedQuery(name = "ProcedureFeeSubprocedure.findById", query = "SELECT p FROM ProcedureFeeSubprocedure p WHERE p.id = :id") })
public class ProcedureFeeSubprocedure implements Serializable {
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name = "PROCEDURE_FEE_SUB_SEQ", sequenceName = "PROCEDURE_FEE_SUB_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROCEDURE_FEE_SUB_SEQ")
	@Column(name = "ID")
	private Long id;
	@Column(name = "SUB_PROCEDURE_ID")
	private Long subProcedureId;
	@Column(name = "PROCEDURE_ID")
	private Long procedureId;
	@Column(name = "IS_ACTIVE")
	private Long isActive;
	@Column(name = "FEE_ID")
	private Long feeId;
	@Column(name = "COST")
	private Long cost;
	@Column(name = "PHASE")
	private Long phase;
	@Column(name = "PROCEDURE_NAME")
	private String procedureName;
	@Column(name = "FEE_NAME")
	private String feeName;
	@Column(name = "SUB_PROCEDURE_NAME")
	private String subProcedureName;

	public ProcedureFeeSubprocedure() {
	}

	public ProcedureFeeSubprocedure(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSubProcedureId() {
		return subProcedureId;
	}

	public void setSubProcedureId(Long subProcedureId) {
		this.subProcedureId = subProcedureId;
	}

	public Long getProcedureId() {
		return procedureId;
	}

	public void setProcedureId(Long procedureId) {
		this.procedureId = procedureId;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	public Long getFeeId() {
		return feeId;
	}

	public void setFeeId(Long feeId) {
		this.feeId = feeId;
	}

	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}

	public Long getPhase() {
		return phase;
	}

	public void setPhase(Long phase) {
		this.phase = phase;
	}

	public String getProcedureName() {
		return procedureName;
	}

	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}

	public String getFeeName() {
		return feeName;
	}

	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}

	public String getSubProcedureName() {
		return subProcedureName;
	}

	public void setSubProcedureName(String subProcedureName) {
		this.subProcedureName = subProcedureName;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof ProcedureFeeSubprocedure)) {
			return false;
		}
		ProcedureFeeSubprocedure other = (ProcedureFeeSubprocedure) object;
		return !((this.id == null && other.id != null) || (this.id != null && !this.id
				.equals(other.id)));
	}

	@Override
	public String toString() {
		return "com.viettel.module.rapidtest.BO.ProcedureFeeSubprocedure[ id="
				+ id + " ]";
	}

}
