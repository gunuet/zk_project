/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Linhdx
 */
@Entity
@Table(name = "RT_ADDITIONAL_REQUEST")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "RtAdditionalRequest.findAll", query = "SELECT r FROM RtAdditionalRequest r"),
		@NamedQuery(name = "RtAdditionalRequest.findByAdditionalRequestId", query = "SELECT r FROM RtAdditionalRequest r WHERE r.additionalRequestId = :additionalRequestId"),
		@NamedQuery(name = "RtAdditionalRequest.findByCreatorId", query = "SELECT r FROM RtAdditionalRequest r WHERE r.creatorId = :creatorId"),
		@NamedQuery(name = "RtAdditionalRequest.findByCreateDate", query = "SELECT r FROM RtAdditionalRequest r WHERE r.createDate = :createDate"),
		@NamedQuery(name = "RtAdditionalRequest.findByFileId", query = "SELECT r FROM RtAdditionalRequest r WHERE r.fileId = :fileId"),
		@NamedQuery(name = "RtAdditionalRequest.findByCreateDeptId", query = "SELECT r FROM RtAdditionalRequest r WHERE r.createDeptId = :createDeptId"),
		@NamedQuery(name = "RtAdditionalRequest.findByCreateDeptName", query = "SELECT r FROM RtAdditionalRequest r WHERE r.createDeptName = :createDeptName"),
		@NamedQuery(name = "RtAdditionalRequest.findByVersion", query = "SELECT r FROM RtAdditionalRequest r WHERE r.version = :version"),
		@NamedQuery(name = "RtAdditionalRequest.findByCommentType", query = "SELECT r FROM RtAdditionalRequest r WHERE r.commentType = :commentType"),
		@NamedQuery(name = "RtAdditionalRequest.findByStatus", query = "SELECT r FROM RtAdditionalRequest r WHERE r.status = :status"),
		@NamedQuery(name = "RtAdditionalRequest.findByAttachId", query = "SELECT r FROM RtAdditionalRequest r WHERE r.attachId = :attachId"),
		@NamedQuery(name = "RtAdditionalRequest.findByIsActive", query = "SELECT r FROM RtAdditionalRequest r WHERE r.isActive = :isActive") })
public class RtAdditionalRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name = "RT_ADDITIONAL_REQUEST_SEQ", sequenceName = "RT_ADDITIONAL_REQUEST_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RT_ADDITIONAL_REQUEST_SEQ")
	@Column(name = "ADDITIONAL_REQUEST_ID")
	private Long additionalRequestId;
	@Column(name = "CREATOR_ID")
	private Long creatorId;
	@Column(name = "CREATOR_NAME")
	private String creatorName;
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.DATE)
	private Date createDate;
	@Column(name = "FILE_ID")
	private Long fileId;
	@Column(name = "CREATE_DEPT_ID")
	private Long createDeptId;
	@Column(name = "CREATE_DEPT_NAME")
	private String createDeptName;
	@Column(name = "VERSION")
	private Long version;
	@Column(name = "CONTENT")
	private String content;
	@Column(name = "COMMENT_TYPE")
	private Long commentType;
	@Column(name = "STATUS")
	private Long status;
	@Column(name = "ATTACH_ID")
	private Long attachId;
	@Column(name = "IS_ACTIVE")
	private Long isActive;

	public RtAdditionalRequest() {
	}

	public RtAdditionalRequest(Long additionalRequestId) {
		this.additionalRequestId = additionalRequestId;
	}

	public Long getAdditionalRequestId() {
		return additionalRequestId;
	}

	public void setAdditionalRequestId(Long additionalRequestId) {
		this.additionalRequestId = additionalRequestId;
	}

	public Long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getCreateDeptId() {
		return createDeptId;
	}

	public void setCreateDeptId(Long createDeptId) {
		this.createDeptId = createDeptId;
	}

	public String getCreateDeptName() {
		return createDeptName;
	}

	public void setCreateDeptName(String createDeptName) {
		this.createDeptName = createDeptName;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getCommentType() {
		return commentType;
	}

	public void setCommentType(Long commentType) {
		this.commentType = commentType;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getAttachId() {
		return attachId;
	}

	public void setAttachId(Long attachId) {
		this.attachId = attachId;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (additionalRequestId != null ? additionalRequestId.hashCode()
				: 0);
		return hash;
	}
        
        @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RtAdditionalRequest)) {
            return false;
        }
        RtAdditionalRequest other = (RtAdditionalRequest) object;
        if ((this.additionalRequestId == null && other.additionalRequestId != null) || (this.additionalRequestId != null && !this.additionalRequestId.equals(other.additionalRequestId))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "com.viettel.module.rapidtest.BO.RtAdditionalRequest[ additionalRequestId="
				+ additionalRequestId + " ]";
	}

}
