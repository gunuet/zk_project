/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.rapidtest.BO;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author E5420
 */
@Entity
@Table(name = "RT_ASSAY")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "RtAssay.findAll", query = "SELECT r FROM RtAssay r"),
		@NamedQuery(name = "RtAssay.findById", query = "SELECT r FROM RtAssay r WHERE r.id = :id"),
		@NamedQuery(name = "RtAssay.findByName", query = "SELECT r FROM RtAssay r WHERE r.name = :name"),
		@NamedQuery(name = "RtAssay.findByCode", query = "SELECT r FROM RtAssay r WHERE r.code = :code"),
		@NamedQuery(name = "RtAssay.findByAddress", query = "SELECT r FROM RtAssay r WHERE r.address = :address"),
		@NamedQuery(name = "RtAssay.findByPhone", query = "SELECT r FROM RtAssay r WHERE r.phone = :phone"),
		@NamedQuery(name = "RtAssay.findByFax", query = "SELECT r FROM RtAssay r WHERE r.fax = :fax"),
		// @NamedQuery(name = "RtAssay.findByCreatedDate", query =
		// "SELECT r FROM RtAssay r WHERE r.createdDate = :createdDate"),
		@NamedQuery(name = "RtAssay.findByIsActive", query = "SELECT r FROM RtAssay r WHERE r.isActive = :isActive"),
		@NamedQuery(name = "RtAssay.findByCreatedBy", query = "SELECT r FROM RtAssay r WHERE r.createdBy = :createdBy") })
public class RtAssay implements Serializable {
	private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@SequenceGenerator(name = "RT_ASSAY_SEQ", sequenceName = "RT_ASSAY_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RT_ASSAY_SEQ")
	@Basic(optional = false)
	@NotNull
	@Column(name = "ID")
	private Long id;
	@Size(max = 200)
	@Column(name = "NAME")
	private String name;
	@Size(max = 100)
	@Column(name = "CODE")
	private String code;
	@Size(max = 255)
	@Column(name = "ADDRESS")
	private String address;
	// @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
	// message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the
	// field contains phone or fax number consider using this annotation to
	// enforce field validation
	@Size(max = 50)
	@Column(name = "PHONE")
	private String phone;
	// @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
	// message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the
	// field contains phone or fax number consider using this annotation to
	// enforce field validation
	@Size(max = 50)
	@Column(name = "FAX")
	private String fax;
	// @Column(name = "CREATED_DATE")
	// @Temporal(TemporalType.DATE)
	// private Date createdDate;
	@Column(name = "IS_ACTIVE")
	private Long isActive;
	@Column(name = "CREATED_BY")
	private Long createdBy;

	public RtAssay() {
	}

	public RtAssay(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	/*
	 * public Date getCreatedDate() { return createdDate; }
	 * 
	 * public void setCreatedDate(Date createdDate) { this.createdDate =
	 * createdDate; }
	 */

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof RtAssay)) {
			return false;
		}
		RtAssay other = (RtAssay) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.viettel.module.rapidtest.BO.RtAssay[ id=" + id + " ]";
	}

}
