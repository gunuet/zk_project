/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * 
 * @author Tichnv
 */
@Entity
@Table(name = "RT_COUNCIL")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "RtCouncil.findAll", query = "SELECT r FROM RtCouncil r"),
		@NamedQuery(name = "RtCouncil.findById", query = "SELECT r FROM RtCouncil r WHERE r.id = :id"),
		@NamedQuery(name = "RtCouncil.findByName", query = "SELECT r FROM RtCouncil r WHERE r.name = :name"),
		@NamedQuery(name = "RtCouncil.findByRole", query = "SELECT r FROM RtCouncil r WHERE r.role = :role"),
		@NamedQuery(name = "RtCouncil.findByPosition", query = "SELECT r FROM RtCouncil r WHERE r.position = :position"),
		@NamedQuery(name = "RtCouncil.findByCreatedDate", query = "SELECT r FROM RtCouncil r WHERE r.createdDate = :createdDate"),
		@NamedQuery(name = "RtCouncil.findByIsActive", query = "SELECT r FROM RtCouncil r WHERE r.isActive = :isActive"),
		@NamedQuery(name = "RtCouncil.findByCreatedBy", query = "SELECT r FROM RtCouncil r WHERE r.createdBy = :createdBy"),
		@NamedQuery(name = "RtCouncil.findByCode", query = "SELECT r FROM RtCouncil r WHERE r.code = :code") })
public class RtCouncil implements Serializable {
	private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@SequenceGenerator(name = "RT_COUNCIL_SEQ", sequenceName = "RT_COUNCIL_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RT_COUNCIL_SEQ")
	@Basic(optional = false)
	@NotNull
	@Column(name = "ID")
	private Long id;
	@Size(max = 255)
	@Column(name = "NAME")
	private String name;
	@Size(max = 255)
	@Column(name = "ROLE")
	private String role;
	@Size(max = 255)
	@Column(name = "POSITION")
	private String position;
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	@Column(name = "IS_ACTIVE")
	private Long isActive;
	@Column(name = "CREATED_BY")
	private Long createdBy;
	@Size(max = 30)
	@Column(name = "CODE")
	private String code;
	@Column(name = "ORDER_BY")
	private Long orderBy;

	public RtCouncil() {
	}

	public RtCouncil(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof RtCouncil)) {
			return false;
		}
		RtCouncil other = (RtCouncil) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.viettel.module.rapidtest.BO.RtCouncil[ id=" + id + " ]";
	}

	public Long getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Long orderBy) {
		this.orderBy = orderBy;
	}

}
