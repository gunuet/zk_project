/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.viettel.utils.Constants;

/**
 * 
 * @author Linhdx
 */
@Entity
@Table(name = "RT_EVALUATION_RECORD")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "RtEvaluationRecord.findAll", query = "SELECT r FROM RtEvaluationRecord r"),
		@NamedQuery(name = "RtEvaluationRecord.findByEvaluationRecordId", query = "SELECT r FROM RtEvaluationRecord r WHERE r.evaluationRecordId = :evaluationRecordId"),
		@NamedQuery(name = "RtEvaluationRecord.findByCreateDate", query = "SELECT r FROM RtEvaluationRecord r WHERE r.createDate = :createDate"),
		@NamedQuery(name = "RtEvaluationRecord.findByBusinessName", query = "SELECT r FROM RtEvaluationRecord r WHERE r.businessName = :businessName"),
		@NamedQuery(name = "RtEvaluationRecord.findByBusinessAddress", query = "SELECT r FROM RtEvaluationRecord r WHERE r.businessAddress = :businessAddress"),
		@NamedQuery(name = "RtEvaluationRecord.findByProductName", query = "SELECT r FROM RtEvaluationRecord r WHERE r.productName = :productName"),
		@NamedQuery(name = "RtEvaluationRecord.findByMainContent", query = "SELECT r FROM RtEvaluationRecord r WHERE r.mainContent = :mainContent"),
		@NamedQuery(name = "RtEvaluationRecord.findByLegal", query = "SELECT r FROM RtEvaluationRecord r WHERE r.legal = :legal"),
		@NamedQuery(name = "RtEvaluationRecord.findByLegalContent", query = "SELECT r FROM RtEvaluationRecord r WHERE r.legalContent = :legalContent"),
		@NamedQuery(name = "RtEvaluationRecord.findByUserId", query = "SELECT r FROM RtEvaluationRecord r WHERE r.userId = :userId"),
		@NamedQuery(name = "RtEvaluationRecord.findByUserName", query = "SELECT r FROM RtEvaluationRecord r WHERE r.userName = :userName"),
		@NamedQuery(name = "RtEvaluationRecord.findByFileId", query = "SELECT r FROM RtEvaluationRecord r WHERE r.fileId = :fileId"),
		@NamedQuery(name = "RtEvaluationRecord.findByFileType", query = "SELECT r FROM RtEvaluationRecord r WHERE r.fileType = :fileType"),
		@NamedQuery(name = "RtEvaluationRecord.findByStatus", query = "SELECT r FROM RtEvaluationRecord r WHERE r.status = :status"),
		@NamedQuery(name = "RtEvaluationRecord.findByIsActive", query = "SELECT r FROM RtEvaluationRecord r WHERE r.isActive = :isActive"),
		@NamedQuery(name = "RtEvaluationRecord.findByProcessId", query = "SELECT r FROM RtEvaluationRecord r WHERE r.processId = :processId") })
public class RtEvaluationRecord implements Serializable {

	private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@SequenceGenerator(name = "RT_EVALUATION_RECORD_SEQ", sequenceName = "RT_EVALUATION_RECORD_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RT_EVALUATION_RECORD_SEQ")
	@Column(name = "EVALUATION_RECORD_ID")
	private Long evaluationRecordId;
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.DATE)
	private Date createDate;
	@Size(max = 255)
	@Column(name = "BUSINESS_NAME")
	private String businessName;
	@Size(max = 500)
	@Column(name = "BUSINESS_ADDRESS")
	private String businessAddress;
	@Size(max = 500)
	@Column(name = "PRODUCT_NAME")
	private String productName;
	@Size(max = 2000)
	@Column(name = "MAIN_CONTENT")
	private String mainContent;
	@Column(name = "LEGAL")
	private Long legal;
	@Size(max = 2000)
	@Column(name = "LEGAL_CONTENT")
	private String legalContent;
	@Column(name = "USER_ID")
	private Long userId;
	@Size(max = 250)
	@Column(name = "USER_NAME")
	private String userName;
	@Column(name = "FILE_ID")
	private Long fileId;
	@Column(name = "FILE_TYPE")
	private Long fileType;
	@Column(name = "STATUS")
	private Long status;
	@Column(name = "IS_ACTIVE")
	private Long isActive;
	@Column(name = "PROCESS_ID")
	private Long processId;

	@Column(name = "ATTACH_ID")
	private Long attachId;

	public RtEvaluationRecord() {
	}

	public RtEvaluationRecord(Long evaluationRecordId) {
		this.evaluationRecordId = evaluationRecordId;
	}

	public Long getEvaluationRecordId() {
		return evaluationRecordId;
	}

	public void setEvaluationRecordId(Long evaluationRecordId) {
		this.evaluationRecordId = evaluationRecordId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getMainContent() {
		return mainContent;
	}

	public void setMainContent(String mainContent) {
		this.mainContent = mainContent;
	}

	public Long getLegal() {
		return legal;
	}

	public void setLegal(Long legal) {
		this.legal = legal;
	}

	public String getLegalContent() {
		return legalContent;
	}

	public void setLegalContent(String legalContent) {
		this.legalContent = legalContent;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getFileType() {
		return fileType;
	}

	public void setFileType(Long fileType) {
		this.fileType = fileType;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	public Long getProcessId() {
		return processId;
	}

	public void setProcessId(Long processId) {
		this.processId = processId;
	}

	public Long getAttachId() {
		return attachId;
	}

	public void setAttachId(Long attachId) {
		this.attachId = attachId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (evaluationRecordId != null ? evaluationRecordId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof RtEvaluationRecord)) {
			return false;
		}
		RtEvaluationRecord other = (RtEvaluationRecord) object;
		if ((this.evaluationRecordId == null && other.evaluationRecordId != null)
				|| (this.evaluationRecordId != null && !this.evaluationRecordId
						.equals(other.evaluationRecordId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.viettel.module.rapidtest.BO.RtEvaluationRecord[ evaluationRecordId="
				+ evaluationRecordId + " ]";
	}

	public void copy(RtEvaluationRecord obj) {
		status = obj.getStatus();
		createDate = obj.getCreateDate();
		businessName = obj.getBusinessName();
		businessAddress = obj.getBusinessAddress();
		productName = obj.getProductName();
		mainContent = obj.getMainContent();
		legal = obj.getLegal();
		legalContent = obj.getLegalContent();
		fileId = obj.getFileId();
		fileType = obj.getFileType();
		processId = obj.getProcessId();

	}

	public String getStaffEvaluationValue() {
		String staffEvaluationValue = "";
		if (Constants.EVALUTION.FILE_NEED_ADD.equals(status)) {
			staffEvaluationValue = "Chuyên viên KL :SDBS";
		} else if (Constants.EVALUTION.FILE_OK.equals(status)) {
			staffEvaluationValue = "Chuyên viên KL :Đạt";
		}
		return staffEvaluationValue;
	}

	public String getStaffEvaluationContent() {
		return mainContent;
	}

}
