/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nguyen Van Trung
 */
@Entity
@Table(name = "RT_FILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RtFile.findAll", query = "SELECT r FROM RtFile r"),
    @NamedQuery(name = "RtFile.findByRtFileId", query = "SELECT r FROM RtFile r WHERE r.rtFileId = :rtFileId"),
    @NamedQuery(name = "RtFile.findByFileId", query = "SELECT r FROM RtFile r WHERE r.fileId = :fileId"),
    @NamedQuery(name = "RtFile.findByNswFileCode", query = "SELECT r FROM RtFile r WHERE r.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "RtFile.findByDocumentTypeCode", query = "SELECT r FROM RtFile r WHERE r.documentTypeCode = :documentTypeCode"),
    @NamedQuery(name = "RtFile.findByRapidTestChangeNo", query = "SELECT r FROM RtFile r WHERE r.rapidTestChangeNo = :rapidTestChangeNo"),
    @NamedQuery(name = "RtFile.findByRapidTestName", query = "SELECT r FROM RtFile r WHERE r.rapidTestName = :rapidTestName"),
    @NamedQuery(name = "RtFile.findByRapidTestCode", query = "SELECT r FROM RtFile r WHERE r.rapidTestCode = :rapidTestCode"),
    @NamedQuery(name = "RtFile.findByCirculatingRapidTestNo", query = "SELECT r FROM RtFile r WHERE r.circulatingRapidTestNo = :circulatingRapidTestNo"),
    @NamedQuery(name = "RtFile.findByDateIssue", query = "SELECT r FROM RtFile r WHERE r.dateIssue = :dateIssue"),
    @NamedQuery(name = "RtFile.findByContents", query = "SELECT r FROM RtFile r WHERE r.contents = :contents"),
    @NamedQuery(name = "RtFile.findByAttachmentsInfo", query = "SELECT r FROM RtFile r WHERE r.attachmentsInfo = :attachmentsInfo"),
    @NamedQuery(name = "RtFile.findBySignPlace", query = "SELECT r FROM RtFile r WHERE r.signPlace = :signPlace"),
    @NamedQuery(name = "RtFile.findBySignDate", query = "SELECT r FROM RtFile r WHERE r.signDate = :signDate"),
    @NamedQuery(name = "RtFile.findBySignName", query = "SELECT r FROM RtFile r WHERE r.signName = :signName"),
    @NamedQuery(name = "RtFile.findBySignedData", query = "SELECT r FROM RtFile r WHERE r.signedData = :signedData"),
    @NamedQuery(name = "RtFile.findByPlaceOfManufacture", query = "SELECT r FROM RtFile r WHERE r.placeOfManufacture = :placeOfManufacture"),
    @NamedQuery(name = "RtFile.findByPropertiesTests", query = "SELECT r FROM RtFile r WHERE r.propertiesTests = :propertiesTests"),
    @NamedQuery(name = "RtFile.findByOperatingPrinciples", query = "SELECT r FROM RtFile r WHERE r.operatingPrinciples = :operatingPrinciples"),
    @NamedQuery(name = "RtFile.findByTargetTesting", query = "SELECT r FROM RtFile r WHERE r.targetTesting = :targetTesting"),
    @NamedQuery(name = "RtFile.findByRangeOfApplications", query = "SELECT r FROM RtFile r WHERE r.rangeOfApplications = :rangeOfApplications"),
    @NamedQuery(name = "RtFile.findByLimitDevelopment", query = "SELECT r FROM RtFile r WHERE r.limitDevelopment = :limitDevelopment"),
    @NamedQuery(name = "RtFile.findByPrecision", query = "SELECT r FROM RtFile r WHERE r.precision = :precision"),
    @NamedQuery(name = "RtFile.findByDescription", query = "SELECT r FROM RtFile r WHERE r.description = :description"),
    @NamedQuery(name = "RtFile.findByPakaging", query = "SELECT r FROM RtFile r WHERE r.pakaging = :pakaging"),
    @NamedQuery(name = "RtFile.findByShelfLife", query = "SELECT r FROM RtFile r WHERE r.shelfLife = :shelfLife"),
    @NamedQuery(name = "RtFile.findByStorageConditions", query = "SELECT r FROM RtFile r WHERE r.storageConditions = :storageConditions"),
    @NamedQuery(name = "RtFile.findByOtherInfos", query = "SELECT r FROM RtFile r WHERE r.otherInfos = :otherInfos"),
    @NamedQuery(name = "RtFile.findByDateEffect", query = "SELECT r FROM RtFile r WHERE r.dateEffect = :dateEffect"),
    @NamedQuery(name = "RtFile.findByExtensionNo", query = "SELECT r FROM RtFile r WHERE r.extensionNo = :extensionNo"),
    @NamedQuery(name = "RtFile.findByRapidTestNo", query = "SELECT r FROM RtFile r WHERE r.rapidTestNo = :rapidTestNo"),
    @NamedQuery(name = "RtFile.findByCirculatingExtensionNo", query = "SELECT r FROM RtFile r WHERE r.circulatingExtensionNo = :circulatingExtensionNo")})
public class RtFile implements Serializable {

    private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
    // consider using these annotations to enforce field validation
    @SequenceGenerator(name = "RT_FILE_SEQ", sequenceName = "RT_FILE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RT_FILE_SEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "RT_FILE_ID")
    private Long rtFileId;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Size(max = 31)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Size(max = 31)
    @Column(name = "RAPID_TEST_CHANGE_NO")
    private String rapidTestChangeNo;
    @Size(max = 255)
    @Column(name = "RAPID_TEST_NAME")
    private String rapidTestName;
    @Size(max = 255)
    @Column(name = "RAPID_TEST_CODE")
    private String rapidTestCode;
    @Size(max = 255)
    @Column(name = "CIRCULATING_RAPID_TEST_NO")
    private String circulatingRapidTestNo;
    @Column(name = "DATE_ISSUE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateIssue;
    @Size(max = 500)
    @Column(name = "CONTENTS")
    private String contents;
    @Size(max = 255)
    @Column(name = "ATTACHMENTS_INFO")
    private String attachmentsInfo;
    @Size(max = 255)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date signDate;
    @Size(max = 255)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Size(max = 2000)
    @Column(name = "SIGNED_DATA")
    private String signedData;
    @Size(max = 255)
    @Column(name = "PLACE_OF_MANUFACTURE")
    private String placeOfManufacture;
    @Column(name = "PROPERTIES_TESTS")
    private Long propertiesTests;
    @Size(max = 255)
    @Column(name = "OPERATING_PRINCIPLES")
    private String operatingPrinciples;
    @Size(max = 255)
    @Column(name = "TARGET_TESTING")
    private String targetTesting;
    @Size(max = 255)
    @Column(name = "RANGE_OF_APPLICATIONS")
    private String rangeOfApplications;
    @Size(max = 255)
    @Column(name = "LIMIT_DEVELOPMENT")
    private String limitDevelopment;
    @Size(max = 255)
    @Column(name = "PRECISION")
    private String precision;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 255)
    @Column(name = "PAKAGING")
    private String pakaging;
    @Column(name = "SHELF_LIFE")
    private String shelfLife;
    @Size(max = 255)
    @Column(name = "STORAGE_CONDITIONS")
    private String storageConditions;
    @Size(max = 255)
    @Column(name = "OTHER_INFOS")
    private String otherInfos;
    @Column(name = "DATE_EFFECT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEffect;
    @Column(name = "EXTENSION_NO")
    private Long extensionNo;
    @Size(max = 31)
    @Column(name = "RAPID_TEST_NO")
    private String rapidTestNo;
    @Size(max = 31)
    @Column(name = "CIRCULATING_EXTENSION_NO")
    private String circulatingExtensionNo;
    // Bổ sung chỉ tiêu tt
    @Column(name = "MANUFACTURE")
    private String manufacture;
    @Column(name = "ADDRESS_OF_MANUFACTURE")
    private String addressOfManufacture;
    @Column(name = "STATE_OF_MANUFACTURE")
    private String stateOfManufacture;
    @Column(name = "NAME_OF_STATE")
    private String nameOfState;
    @Column(name = "SIGN_PLACE_NAME")
    private String signPlaceName;
    // Bổ sung giấy phép
    @Column(name = "CIRCULATING_NO")
    private String circulatingNo;
    @Column(name = "SIGN_CIRCULATING_DATE")
    private String signCirculatingDate;
    @Size(max = 5)
    @Column(name = "SHELF_LIFE_MONTH")
    private Long shelfLifeMonth;

    @Column(name = "WITHDRAW")
    private String withdraw;

    public RtFile() {
    }

    public RtFile(Long rtFileId) {
        this.rtFileId = rtFileId;
    }

    public String getSignCirculatingDate() {
        return signCirculatingDate;
    }

    public void setSignCirculatingDate(String signCirculatingDate) {
        this.signCirculatingDate = signCirculatingDate;
    }

    public String getCirculatingNo() {
        return circulatingNo;
    }

    public void setCirculatingNo(String circulatingNo) {
        this.circulatingNo = circulatingNo;
    }

    public String getSignPlaceName() {
        return signPlaceName;
    }

    public void setSignPlaceName(String signPlaceName) {
        this.signPlaceName = signPlaceName;
    }

    public String getNameOfState() {
        return nameOfState;
    }

    public void setNameOfState(String nameOfState) {
        this.nameOfState = nameOfState;
    }

    public String getStateOfManufacture() {
        return stateOfManufacture;
    }

    public void setStateOfManufacture(String stateOfManufacture) {
        this.stateOfManufacture = stateOfManufacture;
    }

    public String getAddressOfManufacture() {
        return addressOfManufacture;
    }

    public void setAddressOfManufacture(String addressOfManufacture) {
        this.addressOfManufacture = addressOfManufacture;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public Long getRtFileId() {
        return rtFileId;
    }

    public void setRtFileId(Long rtFileId) {
        this.rtFileId = rtFileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getRapidTestChangeNo() {
        return rapidTestChangeNo;
    }

    public void setRapidTestChangeNo(String rapidTestChangeNo) {
        this.rapidTestChangeNo = rapidTestChangeNo;
    }

    public String getRapidTestName() {
        return rapidTestName;
    }

    public void setRapidTestName(String rapidTestName) {
        this.rapidTestName = rapidTestName;
    }

    public String getRapidTestCode() {
        return rapidTestCode;
    }

    public void setRapidTestCode(String rapidTestCode) {
        this.rapidTestCode = rapidTestCode;
    }

    public String getCirculatingRapidTestNo() {
        return circulatingRapidTestNo;
    }

    public void setCirculatingRapidTestNo(String circulatingRapidTestNo) {
        this.circulatingRapidTestNo = circulatingRapidTestNo;
    }

    public Date getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getAttachmentsInfo() {
        return attachmentsInfo;
    }

    public void setAttachmentsInfo(String attachmentsInfo) {
        this.attachmentsInfo = attachmentsInfo;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignedData() {
        return signedData;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public String getPlaceOfManufacture() {
        return placeOfManufacture;
    }

    public void setPlaceOfManufacture(String placeOfManufacture) {
        this.placeOfManufacture = placeOfManufacture;
    }

    public Long getPropertiesTests() {
        return propertiesTests;
    }

    public void setPropertiesTests(Long propertiesTests) {
        this.propertiesTests = propertiesTests;
    }

    public String getOperatingPrinciples() {
        return operatingPrinciples;
    }

    public void setOperatingPrinciples(String operatingPrinciples) {
        this.operatingPrinciples = operatingPrinciples;
    }

    public String getTargetTesting() {
        return targetTesting;
    }

    public void setTargetTesting(String targetTesting) {
        this.targetTesting = targetTesting;
    }

    public String getRangeOfApplications() {
        return rangeOfApplications;
    }

    public void setRangeOfApplications(String rangeOfApplications) {
        this.rangeOfApplications = rangeOfApplications;
    }

    public String getLimitDevelopment() {
        return limitDevelopment;
    }

    public void setLimitDevelopment(String limitDevelopment) {
        this.limitDevelopment = limitDevelopment;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPakaging() {
        return pakaging;
    }

    public void setPakaging(String pakaging) {
        this.pakaging = pakaging;
    }

    public String getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(String shelfLife) {
        this.shelfLife = shelfLife;
    }

    public String getStorageConditions() {
        return storageConditions;
    }

    public void setStorageConditions(String storageConditions) {
        this.storageConditions = storageConditions;
    }

    public String getOtherInfos() {
        return otherInfos;
    }

    public void setOtherInfos(String otherInfos) {
        this.otherInfos = otherInfos;
    }

    public Date getDateEffect() {
        return dateEffect;
    }

    public void setDateEffect(Date dateEffect) {
        this.dateEffect = dateEffect;
    }

    public Long getExtensionNo() {
        return extensionNo;
    }

    public void setExtensionNo(Long extensionNo) {
        this.extensionNo = extensionNo;
    }

    public String getRapidTestNo() {
        return rapidTestNo;
    }

    public void setRapidTestNo(String rapidTestNo) {
        this.rapidTestNo = rapidTestNo;
    }

    public String getCirculatingExtensionNo() {
        return circulatingExtensionNo;
    }

    public void setCirculatingExtensionNo(String circulatingExtensionNo) {
        this.circulatingExtensionNo = circulatingExtensionNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rtFileId != null ? rtFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof RtFile)) {
            return false;
        }
        RtFile other = (RtFile) object;
        if ((this.rtFileId == null && other.rtFileId != null)
                || (this.rtFileId != null && !this.rtFileId
                .equals(other.rtFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.module.rapidtest.BO.RtFile[ rtFileId=" + rtFileId
                + " ]";
    }

    public Long getShelfLifeMonth() {
        return shelfLifeMonth;
    }

    public void setShelfLifeMonth(Long shelfLifeMonth) {
        this.shelfLifeMonth = shelfLifeMonth;
    }

    public String getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(String withdraw) {
        this.withdraw = withdraw;
    }

}
