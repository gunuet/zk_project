/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Linhdx
 */
@Entity
@Table(name = "RT_FILE1")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "RtFileOld.findAll", query = "SELECT r FROM RtFileOld r"),
		@NamedQuery(name = "RtFileOld.findById", query = "SELECT r FROM RtFileOld r WHERE r.id = :id"),
		@NamedQuery(name = "RtFileOld.findByNswFileCode", query = "SELECT r FROM RtFileOld r WHERE r.nswFileCode = :nswFileCode"),
		@NamedQuery(name = "RtFileOld.findByStatusCode", query = "SELECT r FROM RtFileOld r WHERE r.statusCode = :statusCode"),
		@NamedQuery(name = "RtFileOld.findByDocumentTypeCode", query = "SELECT r FROM RtFileOld r WHERE r.documentTypeCode = :documentTypeCode"),
		@NamedQuery(name = "RtFileOld.findByRapidTestChangeNo", query = "SELECT r FROM RtFileOld r WHERE r.rapidTestChangeNo = :rapidTestChangeNo"),
		@NamedQuery(name = "RtFileOld.findByRapidTestName", query = "SELECT r FROM RtFileOld r WHERE r.rapidTestName = :rapidTestName"),
		@NamedQuery(name = "RtFileOld.findByRapidTestCode", query = "SELECT r FROM RtFileOld r WHERE r.rapidTestCode = :rapidTestCode"),
		@NamedQuery(name = "RtFileOld.findByCirculatingRapidTestNo", query = "SELECT r FROM RtFileOld r WHERE r.circulatingRapidTestNo = :circulatingRapidTestNo"),
		@NamedQuery(name = "RtFileOld.findByDateIssue", query = "SELECT r FROM RtFileOld r WHERE r.dateIssue = :dateIssue"),
		@NamedQuery(name = "RtFileOld.findByContents", query = "SELECT r FROM RtFileOld r WHERE r.contents = :contents"),
		@NamedQuery(name = "RtFileOld.findByAttachmentsInfo", query = "SELECT r FROM RtFileOld r WHERE r.attachmentsInfo = :attachmentsInfo"),
		@NamedQuery(name = "RtFileOld.findBySignPlace", query = "SELECT r FROM RtFileOld r WHERE r.signPlace = :signPlace"),
		@NamedQuery(name = "RtFileOld.findBySignDate", query = "SELECT r FROM RtFileOld r WHERE r.signDate = :signDate"),
		@NamedQuery(name = "RtFileOld.findBySignName", query = "SELECT r FROM RtFileOld r WHERE r.signName = :signName"),
		@NamedQuery(name = "RtFileOld.findBySignedData", query = "SELECT r FROM RtFileOld r WHERE r.signedData = :signedData"),
		@NamedQuery(name = "RtFileOld.findByPlaceOfManufacture", query = "SELECT r FROM RtFileOld r WHERE r.placeOfManufacture = :placeOfManufacture"),
		@NamedQuery(name = "RtFileOld.findByPropertiesTests", query = "SELECT r FROM RtFileOld r WHERE r.propertiesTests = :propertiesTests"),
		@NamedQuery(name = "RtFileOld.findByOperatingPrinciples", query = "SELECT r FROM RtFileOld r WHERE r.operatingPrinciples = :operatingPrinciples"),
		@NamedQuery(name = "RtFileOld.findByTargetTesting", query = "SELECT r FROM RtFileOld r WHERE r.targetTesting = :targetTesting"),
		@NamedQuery(name = "RtFileOld.findByRangeOfApplications", query = "SELECT r FROM RtFileOld r WHERE r.rangeOfApplications = :rangeOfApplications"),
		@NamedQuery(name = "RtFileOld.findByLimitDevelopment", query = "SELECT r FROM RtFileOld r WHERE r.limitDevelopment = :limitDevelopment"),
		@NamedQuery(name = "RtFileOld.findByPrecision", query = "SELECT r FROM RtFileOld r WHERE r.precision = :precision"),
		@NamedQuery(name = "RtFileOld.findByDescription", query = "SELECT r FROM RtFileOld r WHERE r.description = :description"),
		@NamedQuery(name = "RtFileOld.findByPakaging", query = "SELECT r FROM RtFileOld r WHERE r.pakaging = :pakaging"),
		@NamedQuery(name = "RtFileOld.findByShelfLife", query = "SELECT r FROM RtFileOld r WHERE r.shelfLife = :shelfLife"),
		@NamedQuery(name = "RtFileOld.findByStorageConditions", query = "SELECT r FROM RtFileOld r WHERE r.storageConditions = :storageConditions"),
		@NamedQuery(name = "RtFileOld.findByOtherInfos", query = "SELECT r FROM RtFileOld r WHERE r.otherInfos = :otherInfos"),
		@NamedQuery(name = "RtFileOld.findByDateEffect", query = "SELECT r FROM RtFileOld r WHERE r.dateEffect = :dateEffect"),
		@NamedQuery(name = "RtFileOld.findByExtensionNo", query = "SELECT r FROM RtFileOld r WHERE r.extensionNo = :extensionNo"),
		@NamedQuery(name = "RtFileOld.findByRapidTestNo", query = "SELECT r FROM RtFileOld r WHERE r.rapidTestNo = :rapidTestNo"),
		@NamedQuery(name = "RtFileOld.findByCirculatingExtensionNo", query = "SELECT r FROM RtFileOld r WHERE r.circulatingExtensionNo = :circulatingExtensionNo"),
		@NamedQuery(name = "RtFileOld.findByFileId", query = "SELECT r FROM RtFileOld r WHERE r.fileId = :fileId") })
public class RtFileOld implements Serializable {
	private static final long serialVersionUID = 1L;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@SequenceGenerator(name = "RT_FILE_OLD_SEQ", sequenceName = "RT_FILE_OLD_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RT_FILE_OLD_SEQ")
	@Column(name = "ID")
	private Long id;
	@Size(max = 31)
	@Column(name = "NSW_FILE_CODE")
	private String nswFileCode;
	@Column(name = "STATUS_CODE")
	private Long statusCode;
	@Column(name = "DOCUMENT_TYPE_CODE")
	private Long documentTypeCode;
	@Size(max = 31)
	@Column(name = "RAPID_TEST_CHANGE_NO")
	private String rapidTestChangeNo;
	@Size(max = 255)
	@Column(name = "RAPID_TEST_NAME")
	private String rapidTestName;
	@Size(max = 255)
	@Column(name = "RAPID_TEST_CODE")
	private String rapidTestCode;
	@Size(max = 255)
	@Column(name = "CIRCULATING_RAPID_TEST_NO")
	private String circulatingRapidTestNo;
	@Column(name = "DATE_ISSUE")
	@Temporal(TemporalType.DATE)
	private Date dateIssue;
	@Size(max = 500)
	@Column(name = "CONTENTS")
	private String contents;
	@Size(max = 255)
	@Column(name = "ATTACHMENTS_INFO")
	private String attachmentsInfo;
	@Size(max = 255)
	@Column(name = "SIGN_PLACE")
	private String signPlace;
	@Column(name = "SIGN_DATE")
	@Temporal(TemporalType.DATE)
	private Date signDate;
	@Size(max = 255)
	@Column(name = "SIGN_NAME")
	private String signName;
	@Size(max = 2000)
	@Column(name = "SIGNED_DATA")
	private String signedData;
	@Size(max = 255)
	@Column(name = "PLACE_OF_MANUFACTURE")
	private String placeOfManufacture;
	@Column(name = "PROPERTIES_TESTS")
	private Long propertiesTests;
	@Size(max = 510)
	@Column(name = "OPERATING_PRINCIPLES")
	private String operatingPrinciples;
	@Size(max = 510)
	@Column(name = "TARGET_TESTING")
	private String targetTesting;
	@Size(max = 255)
	@Column(name = "RANGE_OF_APPLICATIONS")
	private String rangeOfApplications;
	@Size(max = 255)
	@Column(name = "LIMIT_DEVELOPMENT")
	private String limitDevelopment;
	@Size(max = 255)
	@Column(name = "PRECISION")
	private String precision;
	@Size(max = 255)
	@Column(name = "DESCRIPTION")
	private String description;
	@Size(max = 255)
	@Column(name = "PAKAGING")
	private String pakaging;
	@Size(max = 255)
	@Column(name = "SHELF_LIFE")
	private String shelfLife;
	@Size(max = 255)
	@Column(name = "STORAGE_CONDITIONS")
	private String storageConditions;
	@Size(max = 255)
	@Column(name = "OTHER_INFOS")
	private String otherInfos;
	@Column(name = "DATE_EFFECT")
	@Temporal(TemporalType.DATE)
	private Date dateEffect;
	@Column(name = "EXTENSION_NO")
	private Long extensionNo;
	@Size(max = 31)
	@Column(name = "RAPID_TEST_NO")
	private String rapidTestNo;
	@Size(max = 31)
	@Column(name = "CIRCULATING_EXTENSION_NO")
	private String circulatingExtensionNo;
	@Size(max = 20)
	@Column(name = "FILE_ID")
	private Long fileId;
	@Column(name = "EFFECTIVE")
	private Long effective;

	public RtFileOld() {
	}

	public RtFileOld(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNswFileCode() {
		return nswFileCode;
	}

	public void setNswFileCode(String nswFileCode) {
		this.nswFileCode = nswFileCode;
	}

	public Long getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Long statusCode) {
		this.statusCode = statusCode;
	}

	public Long getDocumentTypeCode() {
		return documentTypeCode;
	}

	public void setDocumentTypeCode(Long documentTypeCode) {
		this.documentTypeCode = documentTypeCode;
	}

	public String getRapidTestChangeNo() {
		return rapidTestChangeNo;
	}

	public void setRapidTestChangeNo(String rapidTestChangeNo) {
		this.rapidTestChangeNo = rapidTestChangeNo;
	}

	public String getRapidTestName() {
		return rapidTestName;
	}

	public void setRapidTestName(String rapidTestName) {
		this.rapidTestName = rapidTestName;
	}

	public String getRapidTestCode() {
		return rapidTestCode;
	}

	public void setRapidTestCode(String rapidTestCode) {
		this.rapidTestCode = rapidTestCode;
	}

	public String getCirculatingRapidTestNo() {
		return circulatingRapidTestNo;
	}

	public void setCirculatingRapidTestNo(String circulatingRapidTestNo) {
		this.circulatingRapidTestNo = circulatingRapidTestNo;
	}

	public Date getDateIssue() {
		return dateIssue;
	}

	public void setDateIssue(Date dateIssue) {
		this.dateIssue = dateIssue;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getAttachmentsInfo() {
		return attachmentsInfo;
	}

	public void setAttachmentsInfo(String attachmentsInfo) {
		this.attachmentsInfo = attachmentsInfo;
	}

	public String getSignPlace() {
		return signPlace;
	}

	public void setSignPlace(String signPlace) {
		this.signPlace = signPlace;
	}

	public Date getSignDate() {
		return signDate;
	}

	public void setSignDate(Date signDate) {
		this.signDate = signDate;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}

	public String getSignedData() {
		return signedData;
	}

	public void setSignedData(String signedData) {
		this.signedData = signedData;
	}

	public String getPlaceOfManufacture() {
		return placeOfManufacture;
	}

	public void setPlaceOfManufacture(String placeOfManufacture) {
		this.placeOfManufacture = placeOfManufacture;
	}

	public Long getPropertiesTests() {
		return propertiesTests;
	}

	public void setPropertiesTests(Long propertiesTests) {
		this.propertiesTests = propertiesTests;
	}

	public String getOperatingPrinciples() {
		return operatingPrinciples;
	}

	public void setOperatingPrinciples(String operatingPrinciples) {
		this.operatingPrinciples = operatingPrinciples;
	}

	public String getTargetTesting() {
		return targetTesting;
	}

	public void setTargetTesting(String targetTesting) {
		this.targetTesting = targetTesting;
	}

	public String getRangeOfApplications() {
		return rangeOfApplications;
	}

	public void setRangeOfApplications(String rangeOfApplications) {
		this.rangeOfApplications = rangeOfApplications;
	}

	public String getLimitDevelopment() {
		return limitDevelopment;
	}

	public void setLimitDevelopment(String limitDevelopment) {
		this.limitDevelopment = limitDevelopment;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPakaging() {
		return pakaging;
	}

	public void setPakaging(String pakaging) {
		this.pakaging = pakaging;
	}

	public String getShelfLife() {
		return shelfLife;
	}

	public void setShelfLife(String shelfLife) {
		this.shelfLife = shelfLife;
	}

	public String getStorageConditions() {
		return storageConditions;
	}

	public void setStorageConditions(String storageConditions) {
		this.storageConditions = storageConditions;
	}

	public String getOtherInfos() {
		return otherInfos;
	}

	public void setOtherInfos(String otherInfos) {
		this.otherInfos = otherInfos;
	}

	public Date getDateEffect() {
		return dateEffect;
	}

	public void setDateEffect(Date dateEffect) {
		this.dateEffect = dateEffect;
	}

	public Long getExtensionNo() {
		return extensionNo;
	}

	public void setExtensionNo(Long extensionNo) {
		this.extensionNo = extensionNo;
	}

	public String getRapidTestNo() {
		return rapidTestNo;
	}

	public void setRapidTestNo(String rapidTestNo) {
		this.rapidTestNo = rapidTestNo;
	}

	public String getCirculatingExtensionNo() {
		return circulatingExtensionNo;
	}

	public void setCirculatingExtensionNo(String circulatingExtensionNo) {
		this.circulatingExtensionNo = circulatingExtensionNo;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getEffective() {
		return effective;
	}

	public void setEffective(Long effective) {
		this.effective = effective;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof RtFileOld)) {
			return false;
		}
		RtFileOld other = (RtFileOld) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.viettel.module.rapidtest.BO.RtFileOld[ id=" + id + " ]";
	}

}
