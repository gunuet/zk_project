/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Linhdx
 */
@Entity
@Table(name = "RTPERMIT")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "RtPermit.findAll", query = "SELECT p FROM Permit p"),
		@NamedQuery(name = "RtPermit.findByPermitId", query = "SELECT p FROM Permit p WHERE p.permitId = :permitId"),
		@NamedQuery(name = "RtPermit.findByFileId", query = "SELECT p FROM Permit p WHERE p.fileId = :fileId"),
		@NamedQuery(name = "RtPermit.findByReceiveNo", query = "SELECT p FROM Permit p WHERE p.receiveNo = :receiveNo"),
		@NamedQuery(name = "RtPermit.findByReceiveDate", query = "SELECT p FROM Permit p WHERE p.receiveDate = :receiveDate"),
		@NamedQuery(name = "RtPermit.findByReceiveDeptName", query = "SELECT p FROM Permit p WHERE p.receiveDeptName = :receiveDeptName"),
		@NamedQuery(name = "RtPermit.findBySignerName", query = "SELECT p FROM Permit p WHERE p.signerName = :signerName"),
		@NamedQuery(name = "RtPermit.findBySignDate", query = "SELECT p FROM Permit p WHERE p.signDate = :signDate"),
		@NamedQuery(name = "RtPermit.findByBusinessName", query = "SELECT p FROM Permit p WHERE p.businessName = :businessName"),
		@NamedQuery(name = "RtPermit.findByBusinessId", query = "SELECT p FROM Permit p WHERE p.businessId = :businessId"),
		@NamedQuery(name = "RtPermit.findByTelephone", query = "SELECT p FROM Permit p WHERE p.telephone = :telephone"),
		@NamedQuery(name = "RtPermit.findByFax", query = "SELECT p FROM Permit p WHERE p.fax = :fax"),
		@NamedQuery(name = "RtPermit.findByEmail", query = "SELECT p FROM Permit p WHERE p.email = :email"),
		@NamedQuery(name = "RtPermit.findByEffectiveDate", query = "SELECT p FROM Permit p WHERE p.effectiveDate = :effectiveDate"),
		@NamedQuery(name = "RtPermit.findByIsActive", query = "SELECT p FROM Permit p WHERE p.isActive = :isActive"),
		@NamedQuery(name = "RtPermit.findByAttachId", query = "SELECT p FROM Permit p WHERE p.attachId = :attachId") })
public class RtPermit implements Serializable {
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name = "RT_PERMIT_SEQ", sequenceName = "RT_PERMIT_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RT_PERMIT_SEQ")
	@Column(name = "PERMIT_ID")
	private Long permitId;
	@Column(name = "FILE_ID")
	private Long fileId;
	@Column(name = "RECEIVE_NO")
	private String receiveNo;
	@Column(name = "RECEIVE_DATE")
	@Temporal(TemporalType.DATE)
	private Date receiveDate;
	@Column(name = "RECEIVE_DEPT_NAME")
	private String receiveDeptName;
	@Column(name = "SIGNER_NAME")
	private String signerName;
	@Column(name = "SIGN_DATE")
	@Temporal(TemporalType.DATE)
	private Date signDate;
	@Column(name = "BUSINESS_NAME")
	private String businessName;
	@Column(name = "BUSINESS_ID")
	private Long businessId;
	@Column(name = "TELEPHONE")
	private String telephone;
	// @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
	// message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the
	// field contains phone or fax number consider using this annotation to
	// enforce field validation
	@Column(name = "FAX")
	private String fax;
	// @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
	// message="Invalid email")//if the field contains email address consider
	// using this annotation to enforce field validation
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "PRODUCT_NAME")
	private String productName;
	@Column(name = "MANUFACTURE_NAME")
	private String manufactureName;
	@Column(name = "MANUFACTURE_ADD")
	private String manufactureAdd;
	@Column(name = "NATION_NAME")
	private String nationName;
	@Column(name = "MATCHING_TARGET")
	private String matchingTarget;
	@Column(name = "EFFECTIVE_DATE")
	@Temporal(TemporalType.DATE)
	private Date effectiveDate;
	@Column(name = "IS_ACTIVE")
	private Long isActive;
	@Column(name = "ATTACH_ID")
	private Long attachId;
	@Column(name = "STATUS")
	private Long status;

	public RtPermit() {
	}

	public RtPermit(Long permitId) {
		this.permitId = permitId;
	}

	public Long getPermitId() {
		return permitId;
	}

	public void setPermitId(Long permitId) {
		this.permitId = permitId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getReceiveNo() {
		return receiveNo;
	}

	public void setReceiveNo(String receiveNo) {
		this.receiveNo = receiveNo;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getReceiveDeptName() {
		return receiveDeptName;
	}

	public void setReceiveDeptName(String receiveDeptName) {
		this.receiveDeptName = receiveDeptName;
	}

	public String getSignerName() {
		return signerName;
	}

	public void setSignerName(String signerName) {
		this.signerName = signerName;
	}

	public Date getSignDate() {
		return signDate;
	}

	public void setSignDate(Date signDate) {
		this.signDate = signDate;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getManufactureName() {
		return manufactureName;
	}

	public void setManufactureName(String manufactureName) {
		this.manufactureName = manufactureName;
	}

	public String getManufactureAdd() {
		return manufactureAdd;
	}

	public void setManufactureAdd(String manufactureAdd) {
		this.manufactureAdd = manufactureAdd;
	}

	public String getNationName() {
		return nationName;
	}

	public void setNationName(String nationName) {
		this.nationName = nationName;
	}

	public String getMatchingTarget() {
		return matchingTarget;
	}

	public void setMatchingTarget(String matchingTarget) {
		this.matchingTarget = matchingTarget;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	public Long getAttachId() {
		return attachId;
	}

	public void setAttachId(Long attachId) {
		this.attachId = attachId;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (permitId != null ? permitId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof RtPermit)) {
			return false;
		}
		RtPermit other = (RtPermit) object;
		if ((this.permitId == null && other.permitId != null)
				|| (this.permitId != null && !this.permitId
						.equals(other.permitId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.viettel.module.rapidtest.BO.Permit[ permitId=" + permitId
				+ " ]";
	}

}
