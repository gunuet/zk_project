/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author E5420
 */
@Entity
@Table(name = "RT_TARGET_TESTING")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "RtTargetTesting.findAll", query = "SELECT r FROM RtTargetTesting r"),
		@NamedQuery(name = "RtTargetTesting.findById", query = "SELECT r FROM RtTargetTesting r WHERE r.id = :id"),
		@NamedQuery(name = "RtTargetTesting.findByName", query = "SELECT r FROM RtTargetTesting r WHERE r.name = :name"),
		@NamedQuery(name = "RtTargetTesting.findByRangeOfApplications", query = "SELECT r FROM RtTargetTesting r WHERE r.rangeOfApplications = :rangeOfApplications"),
		@NamedQuery(name = "RtTargetTesting.findByLimitDevelopment", query = "SELECT r FROM RtTargetTesting r WHERE r.limitDevelopment = :limitDevelopment"),
		@NamedQuery(name = "RtTargetTesting.findByPrecision", query = "SELECT r FROM RtTargetTesting r WHERE r.precision = :precision"),
		@NamedQuery(name = "RtTargetTesting.findByCreatedBy", query = "SELECT r FROM RtTargetTesting r WHERE r.createdBy = :createdBy"),
		@NamedQuery(name = "RtTargetTesting.findByCreatedDate", query = "SELECT r FROM RtTargetTesting r WHERE r.createdDate = :createdDate"),
		@NamedQuery(name = "RtTargetTesting.findByFileId", query = "SELECT r FROM RtTargetTesting r WHERE r.fileId = :fileId"),
		@NamedQuery(name = "RtTargetTesting.findByIsActive", query = "SELECT r FROM RtTargetTesting r WHERE r.isActive = :isActive") })
public class RtTargetTesting implements Serializable {
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name = "RT_TARGET_TESTING_SEQ", sequenceName = "RT_TARGET_TESTING_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RT_TARGET_TESTING_SEQ")
	@Column(name = "ID")
	private Long id;
	@Size(max = 500)
	@Column(name = "NAME")
	private String name;
	@Size(max = 250)
	@Column(name = "RANGE_OF_APPLICATIONS")
	private String rangeOfApplications;
	@Size(max = 50)
	@Column(name = "LIMIT_DEVELOPMENT")
	private String limitDevelopment;
	@Size(max = 50)
	@Column(name = "PRECISIONS")
	private String precision;
	@Column(name = "CREATED_BY")
	private Long createdBy;
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	@Column(name = "FILE_ID")
	private Long fileId;
	@Column(name = "IS_ACTIVE")
	private Long isActive;

	public RtTargetTesting() {
	}

	public RtTargetTesting(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRangeOfApplications() {
		return rangeOfApplications;
	}

	public void setRangeOfApplications(String rangeOfApplications) {
		this.rangeOfApplications = rangeOfApplications;
	}

	public String getLimitDevelopment() {
		return limitDevelopment;
	}

	public void setLimitDevelopment(String limitDevelopment) {
		this.limitDevelopment = limitDevelopment;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof RtTargetTesting)) {
			return false;
		}
		RtTargetTesting other = (RtTargetTesting) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.viettel.module.rapidtest.BO.RtTargetTesting[ id=" + id
				+ " ]";
	}

}
