/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.BO;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author viettel
 */
@Entity
@Table(name = "TEMPLATE")
@XmlRootElement
@NamedQueries({
		@NamedQuery(name = "Template.findAll", query = "SELECT t FROM Template t"),
		@NamedQuery(name = "Template.findByTemplateId", query = "SELECT t FROM Template t WHERE t.templateId = :templateId"),
		@NamedQuery(name = "Template.findByTemplateName", query = "SELECT t FROM Template t WHERE t.templateName = :templateName"),
		@NamedQuery(name = "Template.findByAttachId", query = "SELECT t FROM Template t WHERE t.attachId = :attachId"),
		@NamedQuery(name = "Template.findByDeptId", query = "SELECT t FROM Template t WHERE t.deptId = :deptId"),
		@NamedQuery(name = "Template.findByDeptName", query = "SELECT t FROM Template t WHERE t.deptName = :deptName") })
public class Template implements Serializable {

	private static final Long serialVersionUID = 1L;
	@SequenceGenerator(name = "TEMPLATE_SEQ", sequenceName = "TEMPLATE_SEQ")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEMPLATE_SEQ")
	@Column(name = "TEMPLATE_ID")
	private Long templateId;
	@Column(name = "TEMPLATE_NAME")
	private String templateName;
	@Column(name = "ATTACH_ID")
	private Long attachId;
	@Basic(optional = false)
	@Column(name = "DEPT_ID")
	private Long deptId;
	@Basic(optional = false)
	@Column(name = "PROCEDURE_ID")
	private Long procedureId;
	@Column(name = "PROCEDURE_NAME")
	private String procedureName;

	@Column(name = "TEMPLATE_TYPE_ID")
	private Long templateTypeId;
	@Column(name = "TEMPLATE_TYPE_NAME")
	private String templateTypeName;

	@Column(name = "DEPT_NAME")
	private String deptName;
	@Column(name = "IS_ACTIVE")
	private Long isActive;

	public Template() {
	}

	public Template(Long templateId) {
		this.templateId = templateId;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public Long getAttachId() {
		return attachId;
	}

	public void setAttachId(Long attachId) {
		this.attachId = attachId;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public Long getProcedureId() {
		return procedureId;
	}

	public void setProcedureId(Long procedureId) {
		this.procedureId = procedureId;
	}

	public String getProcedureName() {
		return procedureName;
	}

	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}

	public Long getTemplateTypeId() {
		return templateTypeId;
	}

	public void setTemplateTypeId(Long templateTypeId) {
		this.templateTypeId = templateTypeId;
	}

	public String getTemplateTypeName() {
		return templateTypeName;
	}

	public void setTemplateTypeName(String templateTypeName) {
		this.templateTypeName = templateTypeName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (templateId != null ? templateId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Template)) {
			return false;
		}
		Template other = (Template) object;
		return !((this.templateId == null && other.templateId != null) || (this.templateId != null && !this.templateId
				.equals(other.templateId)));
	}

	@Override
	public String toString() {
		return "com.viettel.voffice.BO.Document.Template[ templateId="
				+ templateId + " ]";
	}

}
