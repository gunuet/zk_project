/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.BO;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.viettel.utils.Constants;
import com.viettel.utils.DateTimeUtils;

/**
 *
 * @author Linhdx
 */
@Entity
@Table(name = "V_FILE_RTFILE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VFileRtfile.findAll", query = "SELECT v FROM VFileRtfile v"),
    @NamedQuery(name = "VFileRtfile.findById", query = "SELECT v FROM VFileRtfile v WHERE v.id = :id"),
    @NamedQuery(name = "VFileRtfile.findByNswFileCode", query = "SELECT v FROM VFileRtfile v WHERE v.nswFileCode = :nswFileCode"),
    @NamedQuery(name = "VFileRtfile.findByCreateDeptName", query = "SELECT v FROM VFileRtfile v WHERE v.createDeptName = :createDeptName")})
public class VFileRtfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "FILE_ID")
    private Long fileId;

    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Column(name = "FILE_TYPE_NAME")
    private String fileTypeName;
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Column(name = "FILE_NAME")
    private String fileName;

    @Size(max = 31)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Size(max = 31)
    @Column(name = "RAPID_TEST_CHANGE_NO")
    private String rapidTestChangeNo;
    @Size(max = 255)
    @Column(name = "RAPID_TEST_NAME")
    private String rapidTestName;
    @Size(max = 255)
    @Column(name = "RAPID_TEST_CODE")
    private String rapidTestCode;
    @Size(max = 255)
    @Column(name = "CIRCULATING_RAPID_TEST_NO")
    private String circulatingRapidTestNo;
    @Column(name = "DATE_ISSUE")
    @Temporal(TemporalType.DATE)
    private Date dateIssue;
    @Size(max = 500)
    @Column(name = "CONTENTS")
    private String contents;
    @Size(max = 255)
    @Column(name = "ATTACHMENTS_INFO")
    private String attachmentsInfo;
    @Size(max = 255)
    @Column(name = "SIGN_PLACE")
    private String signPlace;
    @Column(name = "SIGN_DATE")
    @Temporal(TemporalType.DATE)
    private Date signDate;
    @Size(max = 255)
    @Column(name = "SIGN_NAME")
    private String signName;
    @Size(max = 2000)
    @Column(name = "SIGNED_DATA")
    private String signedData;
    @Size(max = 255)
    @Column(name = "PLACE_OF_MANUFACTURE")
    private String placeOfManufacture;
    @Size(max = 510)
    @Column(name = "OPERATING_PRINCIPLES")
    private String operatingPrinciples;
    @Size(max = 510)
    @Column(name = "TARGET_TESTING")
    private String targetTesting;
    @Size(max = 255)
    @Column(name = "RANGE_OF_APPLICATIONS")
    private String rangeOfApplications;
    @Size(max = 255)
    @Column(name = "LIMIT_DEVELOPMENT")
    private String limitDevelopment;
    @Size(max = 255)
    @Column(name = "PRECISION")
    private String precision;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 255)
    @Column(name = "PAKAGING")
    private String pakaging;
    @Size(max = 255)
    @Column(name = "SHELF_LIFE")
    private String shelfLife;
    @Size(max = 255)
    @Column(name = "STORAGE_CONDITIONS")
    private String storageConditions;
    @Size(max = 255)
    @Column(name = "OTHER_INFOS")
    private String otherInfos;
    @Column(name = "DATE_EFFECT")
    @Temporal(TemporalType.DATE)
    private Date dateEffect;
    @Size(max = 31)
    @Column(name = "RAPID_TEST_NO")
    private String rapidTestNo;
    @Size(max = 31)
    @Column(name = "CIRCULATING_EXTENSION_NO")
    private String circulatingExtensionNo;
    @Size(max = 20)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.DATE)
    private Date modifyDate;
    @Size(max = 31)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Size(max = 255)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 15)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 15)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 255)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;

    // RT_FILE_ID
    @Column(name = "ID")
    private Long id;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "DOCUMENT_TYPE_CODE")
    private Long documentTypeCode;
    @Column(name = "PROPERTIES_TESTS")
    private Long propertiesTests;
    @Column(name = "EXTENSION_NO")
    private Long extensionNo;
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;

    @Column(name = "IS_TEMP")
    private Long isTemp;
    @Column(name = "VERSION")
    private Long version;
    @Column(name = "PARENT_FILE_ID")
    private Long parentFileId;
    @Column(name = "SHELF_LIFE_MONTH")
    private Long shelfLifeMonth;
    @Column(name = "FINISH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;

    @Column(name = "WITHDRAW")
    private String withdraw;

    public VFileRtfile() {
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(Long documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getRapidTestChangeNo() {
        return rapidTestChangeNo;
    }

    public void setRapidTestChangeNo(String rapidTestChangeNo) {
        this.rapidTestChangeNo = rapidTestChangeNo;
    }

    public String getRapidTestName() {
        return rapidTestName;
    }

    public void setRapidTestName(String rapidTestName) {
        this.rapidTestName = rapidTestName;
    }

    public String getRapidTestCode() {
        return rapidTestCode;
    }

    public void setRapidTestCode(String rapidTestCode) {
        this.rapidTestCode = rapidTestCode;
    }

    public String getCirculatingRapidTestNo() {
        return circulatingRapidTestNo;
    }

    public void setCirculatingRapidTestNo(String circulatingRapidTestNo) {
        this.circulatingRapidTestNo = circulatingRapidTestNo;
    }

    public Date getDateIssue() {
        return dateIssue;
    }

    public void setDateIssue(Date dateIssue) {
        this.dateIssue = dateIssue;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getAttachmentsInfo() {
        return attachmentsInfo;
    }

    public void setAttachmentsInfo(String attachmentsInfo) {
        this.attachmentsInfo = attachmentsInfo;
    }

    public String getSignPlace() {
        return signPlace;
    }

    public void setSignPlace(String signPlace) {
        this.signPlace = signPlace;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignedData() {
        return signedData;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public String getPlaceOfManufacture() {
        return placeOfManufacture;
    }

    public void setPlaceOfManufacture(String placeOfManufacture) {
        this.placeOfManufacture = placeOfManufacture;
    }

    public Long getPropertiesTests() {
        return propertiesTests;
    }

    public void setPropertiesTests(Long propertiesTests) {
        this.propertiesTests = propertiesTests;
    }

    public String getOperatingPrinciples() {
        return operatingPrinciples;
    }

    public void setOperatingPrinciples(String operatingPrinciples) {
        this.operatingPrinciples = operatingPrinciples;
    }

    public String getTargetTesting() {
        return targetTesting;
    }

    public void setTargetTesting(String targetTesting) {
        this.targetTesting = targetTesting;
    }

    public String getRangeOfApplications() {
        return rangeOfApplications;
    }

    public void setRangeOfApplications(String rangeOfApplications) {
        this.rangeOfApplications = rangeOfApplications;
    }

    public String getLimitDevelopment() {
        return limitDevelopment;
    }

    public void setLimitDevelopment(String limitDevelopment) {
        this.limitDevelopment = limitDevelopment;
    }

    public String getPrecision() {
        return precision;
    }

    public void setPrecision(String precision) {
        this.precision = precision;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPakaging() {
        return pakaging;
    }

    public void setPakaging(String pakaging) {
        this.pakaging = pakaging;
    }

    public String getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(String shelfLife) {
        this.shelfLife = shelfLife;
    }

    public String getStorageConditions() {
        return storageConditions;
    }

    public void setStorageConditions(String storageConditions) {
        this.storageConditions = storageConditions;
    }

    public String getOtherInfos() {
        return otherInfos;
    }

    public void setOtherInfos(String otherInfos) {
        this.otherInfos = otherInfos;
    }

    public Date getDateEffect() {
        return dateEffect;
    }

    public void setDateEffect(Date dateEffect) {
        this.dateEffect = dateEffect;
    }

    public Long getExtensionNo() {
        return extensionNo;
    }

    public void setExtensionNo(Long extensionNo) {
        this.extensionNo = extensionNo;
    }

    public String getRapidTestNo() {
        return rapidTestNo;
    }

    public void setRapidTestNo(String rapidTestNo) {
        this.rapidTestNo = rapidTestNo;
    }

    public String getCirculatingExtensionNo() {
        return circulatingExtensionNo;
    }

    public void setCirculatingExtensionNo(String circulatingExtensionNo) {
        this.circulatingExtensionNo = circulatingExtensionNo;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public String getSignDateStr() {
        if (signDate == null) {
            return null;
        }
        return DateTimeUtils.convertDateToString(signDate);
    }

    public String getDateIssueStr() {
        if (dateIssue == null) {
            return null;
        }
        return DateTimeUtils.convertDateToString(dateIssue);
    }

    public String getDateEffectStr() {
        if (dateEffect == null) {
            return null;
        }
        return DateTimeUtils.convertDateToString(dateEffect);
    }

    public String getShelfLifeStr() {
        if (shelfLife == null) {
            return null;
        }
        return shelfLife;
    }

    public String getDocumentTypeCodeToStr() {
        String documentTypeCodeStr = "";
        if (Objects.equals(documentTypeCode,
                Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI)) {
            documentTypeCodeStr = Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI_STR;
        } else if (Objects.equals(documentTypeCode,
                Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN)) {
            documentTypeCodeStr = Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN_STR;
        } else if (Objects.equals(documentTypeCode,
                Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG)) {
            documentTypeCodeStr = Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG_STR;
        }
        return documentTypeCodeStr;

    }

    public String getPropertiesTestsStr() {
        String propertiesTestsStr = "";
        if (Objects.equals(propertiesTests, Constants.RAPID_TEST.DINH_TINH)) {
            propertiesTestsStr = Constants.RAPID_TEST.DINH_TINH_STR;
        } else if (Objects.equals(propertiesTests,
                Constants.RAPID_TEST.BAN_DINH_TINH)) {
            propertiesTestsStr = Constants.RAPID_TEST.BAN_DINH_TINH_STR;
        } else if (Objects.equals(propertiesTests,
                Constants.RAPID_TEST.DINH_LUONG)) {
            propertiesTestsStr = Constants.RAPID_TEST.DINH_LUONG_STR;
            ;
        }
        return propertiesTestsStr;

    }

    /**
     * @return the fileType
     */
    public Long getFileType() {
        return fileType;
    }

    /**
     * @param fileType the fileType to set
     */
    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    /**
     * @return the fileTypeName
     */
    public String getFileTypeName() {
        return fileTypeName;
    }

    /**
     * @param fileTypeName the fileTypeName to set
     */
    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    /**
     * @return the fileCode
     */
    public String getFileCode() {
        return fileCode;
    }

    /**
     * @param fileCode the fileCode to set
     */
    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the isTemp
     */
    public Long getIsTemp() {
        return isTemp;
    }

    /**
     * @param isTemp the isTemp to set
     */
    public void setIsTemp(Long isTemp) {
        this.isTemp = isTemp;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the parentFileId
     */
    public Long getParentFileId() {
        return parentFileId;
    }

    /**
     * @param parentFileId the parentFileId to set
     */
    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public Long getShelfLifeMonth() {
        return shelfLifeMonth;
    }

    public void setShelfLifeMonth(Long shelfLifeMonth) {
        this.shelfLifeMonth = shelfLifeMonth;
    }

    /**
     * @return the finishDate
     */
    public Date getFinishDate() {
        return finishDate;
    }

    /**
     * @param finishDate the finishDate to set
     */
    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(String withdraw) {
        this.withdraw = withdraw;
    }
}
