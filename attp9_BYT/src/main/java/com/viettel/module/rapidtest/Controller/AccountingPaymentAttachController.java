package com.viettel.module.rapidtest.Controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.DAO.HolidaysManagementDAOHE;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.BO.VRtPaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.controller.ViewAcceptBillController;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.module.rapidtest.DAO.VRtPaymentinfoDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.utils.LogUtils;

/**
 *
 * @author quynhhv1 kế toán xác nhận 1 hồ sơ
 */
public class AccountingPaymentAttachController extends PaymentController {

    @Wire
    private Textbox txtPaymentName;
    @Wire
    private Window businessWindow;
    @Wire
    Radiogroup rgTypePayment;
    @Wire
    Radio radioKeypay, radioBanktransfer, radioImmediaacy;
    @Wire
    private Label lbWarning;
    @Wire
    private Label lbWarningUnder;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    private Window parentWindow;
    Long lsumMoney = 0L;
    // danh cho upload file
    @Wire
    Datebox dbDayPayment;
    @Wire
    Datebox dbReturnDate;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbPhase;
    @Wire
    Textbox txtFileName;
    @Wire
    private Textbox txtPaymentConfirm;
    @Wire
    Textbox txtValidate, txtMessage;
    @Wire
    private Label lbMahoso;
    @Wire
    private Label lbTensp;
    @Wire
    private Groupbox gbDSFileKhaoNghiem;
    @Wire
    Groupbox gbReject;
    @Wire("#lbtextMoney")
    private Label lbtextMoney;
    Bill objBill = null;
    Long typePayment = 0L; // 1 radioKeypay //2 chuyen khoan // 3 truc tiep
    boolean bUploadFile = false, clickDeletefile = false;
    Long docId = null;
    Long docType = null;
    private List<VRtPaymentInfo> listPaymented;
    VRtPaymentInfo vPaymentInfo = null;
    BookDocument mBook;
    // private String type;

    @Wire
    private Listbox fileListbox, lbRapidTestFileType;
    @Wire
    private Vlayout flist;

    private Long categoryCode;
    private Long attachFileId = 0L;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("windowParent");
        mBook = new BookDocument();
        docId = (Long) arguments.get("docId");
        docType = (Long) arguments.get("docType");
        // lsumMoney = objBill.getTotal();
        categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.FILE_KET_QUA_KHAO_NGHIEM;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onAfterRender = #lbListPaymented")
    public void onAfterRender() {
    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    private void reloadModel() throws IOException {

        // VCosPaymentInfoDAO objDAOHE = new VCosPaymentInfoDAO();
        VRtPaymentinfoDAO objDAO = new VRtPaymentinfoDAO();

        listPaymented = objDAO.getListFileIdPhase(docId,
                Long.valueOf(lbPhase.getValue()));
        if (listPaymented.size() > 0) {
            vPaymentInfo = listPaymented.get(0);

            lsumMoney = vPaymentInfo.getCost();
            String formatedMoneyText = formatNumber(lsumMoney, "###,##0");
            lbSumMoney.setValue("".equals(formatedMoneyText) ? ""
                    : formatedMoneyText + " đồng");
            lbMahoso.setValue(vPaymentInfo.getNswFileCode());
            // lbPaymentActionUser.setValue(vPaymentInfo.getPaymentActionUser());
            // lbNgayTiepNhan.setValue(sdf.format(vPaymentInfo.getCreateDate()));
            // lbTensp.setValue(VCosPaymentInfo.getProductName());
            // txtPaymentAddress.setValue(vCosPaymentInfo.getBusinessAddress());
            lbtextMoney.setValue("(Viết bằng chữ): "
                    + numberToString(lsumMoney));

            BillDAO objBillDAOHE = new BillDAO();
            objBill = objBillDAOHE.findById(vPaymentInfo.getBillId());

            if (objBill != null) {
                if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
                    rgTypePayment.setSelectedItem(radioKeypay);
                } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
                    rgTypePayment.setSelectedItem(radioBanktransfer);

                } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                    rgTypePayment.setSelectedItem(radioImmediaacy);
//                    txtBillNo.setValue(objBill.getCode());
                }
                dbDayPayment.setValue(objBill.getPaymentDate());
                txtPaymentName.setValue(objBill.getCreatetorName());
//                txtPaymentAddress.setValue(objBill.getCreatorAddress());

                AttachDAOHE attDAOHE = new AttachDAOHE();
                List<Attachs> items = attDAOHE.getByObjectId(
                        objBill.getBillId(),
                        Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
                if ((items != null) && (items.size() > 0)) {
                    try {
                        // AMedia amedia = new AMedia(mAttach.getAttachName(),
                        // sType, WorkflowAPI.getCtypeFile(sType), is);
                        // iframeFile.setContent(amedia);
                        txtFileName.setValue(items.get(0).getAttachName());
                    } catch (Exception ex) {
                        LogUtils.addLogDB(ex);
                    }
                }
            }

            setTypePayment();
        }
    }

    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            reloadModel();
            fillFileListbox(docId);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        // BookDocumentDAOHE bookDao = new BookDocumentDAOHE();
        // if ((docId != null) && (docType != null)) {
        // BookDocument mBook = bookDao.getBookInFromDocumentId(docId, docType);
        // if (mBook != null) {
        // lbBookNumber.setValue(mBook.getBookNumber().toString());
        // lbNgayTiepNhan.setValue(new
        // SimpleDateFormat("dd-MM-yyyy").format(mBook.getCreateDate()));
        // lbPaymentActionUser.setValue(mBook.getCreatorName());
        // } else {
        // lbBookNumber.setValue("");
        // }
        // }
        // lbSumMoney.setValue("Tổng tiền: " + CommonFns.formatNumber(lsumMoney,
        // "###,###,###"));
        // txtValidate.setValue("0");
        // txtBillNo.setFocus(true);

    }

    @Listen("onClick = #btnAccept")
    public void onAccept() {
        if (isValidate()) {
            PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
            PaymentInfo paymentInfo;
            int sizeList = listPaymented.size();
            for (int i = 0; i < sizeList; i++) {
                VRtPaymentInfo app = listPaymented.get(i);
                paymentInfo = objFreePaymentInfo.findById(app
                        .getPaymentInfoId());
                paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED); // trang
                // thai
                // moi
                // tao
                paymentInfo.setDateConfirm(new Date());
                paymentInfo.setPaymentActionUser(getUserFullName());
                paymentInfo.setPaymentActionUserId(getUserId());
                paymentInfo.setNote(txtPaymentConfirm.getText().trim());
                objFreePaymentInfo.saveOrUpdate(paymentInfo);
                // System.out.print("gia tri"+app.getRapidTestNo());
            }
            BillDAO objBillDAOHE = new BillDAO();

            if (objBill != null) {
                objBill.setStatus(ConstantsPayment.PAYMENT.PAY_CONFIRMED);
                objBill.setConfirmDate(new Date());
                objBill.setConfirmUserId(getUserId());
                objBill.setConfirmUserName(getUserFullName());
//                objBill.setCode(txtBillNo.getValue().trim());

                objBillDAOHE.saveOrUpdate(objBill);
                objBillDAOHE.flush();
                showNotification(String.format(
                        Constants.Notification.APROVE_SUCCESS,
                        Constants.DOCUMENT_TYPE_NAME.BILL),
                        Constants.Notification.INFO);
            }

            FilesDAOHE filesDAOHE = new FilesDAOHE();
            Files files = filesDAOHE.findById(docId);
            files.setStartDate(new Date());
            files.setNumDayProcess(Constants.WARNING.NUM_DAY_PROCESS);
            HolidaysManagementDAOHE holidaysManagementDAOHE = new HolidaysManagementDAOHE();
            Date deadline = holidaysManagementDAOHE.getDeadline(
                    files.getStartDate(), files.getNumDayProcess());
            files.setDeadline(deadline);
            filesDAOHE.saveOrUpdate(files);
        }

    }

    @Listen("onClosePage = #windowsViewAcceptBill")
    public void onClosePage() {
        businessWindow.onClose();
        if (parentWindow != null) {
            Events.sendEvent("onReLoadPage", parentWindow, null);
        }
        // Events.sendEvent("onVisible", parentWindow, null);
    }

    private void setTypePayment() {
        if (rgTypePayment.getSelectedItem() != null) {
            if ("keypay".equals(rgTypePayment.getSelectedItem().getId())) {
                // gbTypePayment.setVisible(false);
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
            } else if ("immediaacy".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;

            } else if ("banktransfer".equals(rgTypePayment.getSelectedItem().getId())) {
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;

            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        setTypePayment();

    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
        lbWarningUnder.setValue(message);
        lbWarning.setVisible(true);
        lbWarningUnder.setVisible(true);
    }

    @Listen("onClick = #btnDownload")
    public void onDownload() throws IOException {

        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(),
                Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {

        if (isValidate()) {
            onAccept();

            saveMainContent();

            sendMS();

            txtValidate.setValue("1");

            businessWindow.onClose();

            if (parentWindow != null) {
                Events.sendEvent("onClosePage", parentWindow, null);
            }

        }
    }

    private boolean isValidate() {
        String message;

        if (dbReturnDate.getValue() == null) {
            message = "Không được để trống ngày trả hồ sơ";
            dbReturnDate.focus();
            setWarningMessage(message);
            txtValidate.setValue("0");
            return false;
        }
        if(txtPaymentConfirm.getValue() == null){
            message = "Không được để trống nội dung";
            txtPaymentConfirm.focus();
            setWarningMessage(message);
            txtValidate.setValue("0");
            return false;
        }
        if(txtPaymentConfirm.getValue() != null && "".equals(txtPaymentConfirm.getValue().trim())){
            message = "Không được để trống nội dung";
            txtPaymentConfirm.focus();
            setWarningMessage(message);
            txtValidate.setValue("0");
            return false;
        }

        return true;
    }

    private void fillFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttachAll> lstRapidTestAttach = rDaoHe
                .findRapidTestAllAttachByCode(fileId, categoryCode);
        this.fileListbox.setModel(new ListModelArray(lstRapidTestAttach));
        if (lstRapidTestAttach.size() == 0) {
            gbDSFileKhaoNghiem.setVisible(false);
        }
        // onSelect();//An hien form
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
        md.setFileId(docId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_RT.SendMs_03);
        md.setPhase(0l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

    private void saveMainContent() {
        EvaluationModel evaluationModel = new EvaluationModel();
        evaluationModel.setUserId(getUserId());
        evaluationModel.setUserName(getUserName());
        evaluationModel.setDeptId(getDeptId());
        evaluationModel.setDeptName(getDeptName());
        evaluationModel.setReturnDate(dbReturnDate.getValue());
        evaluationModel.setResultEvaluation(new Long(0));
        evaluationModel.setContent(txtPaymentConfirm.getText());
        // evaluationModel.setResultEvaluationStr(note);
        String formContent = new Gson().toJson(evaluationModel);

        // set data insert DB
        EvaluationRecord evaluationRecord = new EvaluationRecord();

        evaluationRecord.setFormContent(formContent);
        evaluationRecord
                .setEvalType(Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.HENTRAKQ);
        evaluationRecord.setFileId(docId);
        evaluationRecord.setCreatorId(getUserId());
        evaluationRecord.setCreateDate(new Date());
        evaluationRecord.setCreatorName(getUserFullName());
        evaluationRecord.setIsActive(1L);
        EvaluationRecordDAO evaluationRecordDAO = new EvaluationRecordDAO();
        evaluationRecordDAO.saveOrUpdate(evaluationRecord);
    }
}
