package com.viettel.module.rapidtest.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.Controller.include.CosEvaluationSignPermitController;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.module.rapidtest.DAO.VFileRtflieDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
//import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.module.rapidtest.model.ExportModel;
import com.viettel.module.rapidtest.model.RtManagerSignModel;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;

/**
 *
 * @author Linhdx
 */
public class AdditionalRequestTpController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate, txtMessage;
    @Wire
    private Listbox finalFileListbox;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    @Wire
    private Textbox txtMainContent;
    private Textbox txtNote = (Textbox) Path
            .getComponent("/windowProcessing/txtNote");
    @Wire
    private Label lbNote;

    private Permit permit;

    private List listBook;
    private Long docType;
    private String bCode;
    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();
    private String fileSignOut = "";
    private Long rtSignType;
    List<VFileRtAttachAll> lstRapidTestAttach;
    private Long evalType = 1L;

    /**
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");
        bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.GIAY_YEU_CAU_SUA_DOI_BO_SUNG;

        rtSignType = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_SDBS;
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillFinalFileListbox(fileId);
        txtValidate.setValue("0");

        // load content
        EvaluationRecord evaluationRecord = new EvaluationRecordDAO()
                .findByFileIdAndEvalTypeMax(fileId);
        if (evaluationRecord != null) {
            String formContent = evaluationRecord.getFormContent();
            Gson gson = new Gson();
            EvaluationModel evaluationModel = gson.fromJson(formContent,
                    EvaluationModel.class);

            if (evaluationModel != null) {
                txtMainContent.setValue(evaluationModel.getContent());
                lbNote.setValue(evaluationModel.getResultEvaluationStr());
            }
        }

    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void saveContent() {
        // get user infor
        String note = txtNote.getValue();
        String mainContentValue = txtMainContent.getValue();

        // create json for formContent
        EvaluationModel evaluationModel = new EvaluationModel();
        evaluationModel.setUserId(getUserId());
        evaluationModel.setUserName(getUserName());
        evaluationModel.setDeptId(getDeptId());
        evaluationModel.setDeptName(getDeptName());
        evaluationModel.setContent(mainContentValue);
        evaluationModel.setResultEvaluation(new Long(0));
        evaluationModel.setResultEvaluationStr(note);
        String formContent = new Gson().toJson(evaluationModel);

        // set data insert DB
        EvaluationRecord evaluationRecord = new EvaluationRecord();
        evaluationRecord.setMainContent(mainContentValue);
        evaluationRecord.setFormContent(formContent);
        evaluationRecord
                .setEvalType(Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.SDBS);
        evaluationRecord.setFileId(fileId);
        evaluationRecord.setCreatorId(getUserId());
        evaluationRecord.setCreatorName(getUserFullName());
        evaluationRecord.setCreateDate(new Date());
        EvaluationRecordDAO evaluationRecordDAO = new EvaluationRecordDAO();
        evaluationRecordDAO.saveOrUpdate(evaluationRecord);
    }

    private boolean isValidatedData() {
        if (lstRapidTestAttach.size() == 0) {
            setWarningMessage("Chưa ký văn bản");
            return false;
        }
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            onApproveFile();

            sendMS();
            updateAttachFileSigned();

            txtValidate.setValue("1");

        } catch (WrongValueException e) {
            LogUtils.addLogDB(e);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    public void updateAttachFileSigned() {
        // get addRequest lastest
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO
                .findAllActiveByFileId(fileId);
        // get file attach
        if (lstAdd != null && lstAdd.size() > 0) {
            AdditionalRequest additionalRequest = lstAdd.get(0);
            AttachDAOHE rDaoHe = new AttachDAOHE();

            List<Attachs> lstAttach = rDaoHe
                    .getByObjectIdAndAttachCatAndAttachType(
                            additionalRequest.getAdditionalRequestId(),
                            Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE,
                            Constants.CATEGORY_TYPE.RAPID_TEST_ATT_TYPE);
            for (Attachs att : lstAttach) {
                Attachs att_new = att;
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att_new);
                }
            }
        }

    }

    /**
     *
     * @param event
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) {
        String signature = event.getData().toString();

        /* set outputPath of PDF file */
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
        String filePath = resourceBundle.getString("signPdf");
        // if not existing then to create new forder
        if (!(new File(filePath)).exists()) {
            FileUtil.mkdirs(filePath);
        }

        // set name file
        String outputFileName = "_signed_ThongBaoSuaDoiBoSung"
                + (new Date()).getTime() + ".pdf";
        fileSignOut = filePath + outputFileName;

        Session session = Sessions.getCurrent();
        SignPdfFile signPdfFile = (SignPdfFile) session
                .getAttribute("PDFSignature");

        try {
            signPdfFile.insertSignature(signature, fileSignOut);
            try {
                onSignPermit();
                fileSignOut = "";
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);
            }
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }

        // try {
        // onSignPermit();
        // fileSignOut = "";
        // } catch (Exception ex) {
        // Logger.getLogger(CosEvaluationSignPermitController.class.getName())
        // .log(Level.SEVERE, null, ex);
        // }
    }

    /**
     *
     * @throws Exception
     */
    public void onSignPermit() throws Exception {
        ExportFileDAO exportDao = new ExportFileDAO();
        RtManagerSignModel model = new RtManagerSignModel(fileId,
                Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE, rtSignType);
        model.setSendNo(permit.getReceiveNo());
        model.setSignedDate(permit.getSignDate());
        model.setCosmeticPermitId(fileId);
        exportDao.updateAttachSignFile(model, fileSignOut);

        showNotification("Ký số thành công!", Constants.Notification.INFO);
        fillFinalFileListbox(fileId);
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        // save content
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        saveContent();
        // get fdf file path
        String pdfPath = actionPrepareSign();
        // insert data into pdf
        actionSignCA(event, pdfPath);
    }

    public String actionPrepareSign() {
        String fileToSign = "";
        try {
            fileToSign = onApproveFileSign();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    private void onApproveFile() throws Exception {
        createAddRequest();
    }

    private void createAddRequest() {
        AdditionalRequest additionalRequest;
        AdditionalRequestDAO additionalRequestDAO = new AdditionalRequestDAO();
        List<AdditionalRequest> lstAdd = additionalRequestDAO
                .findAllActiveByFileId(fileId);

        if (lstAdd.size() > 0) {
            additionalRequest = lstAdd.get(0);
        } else {
            EvaluationRecord evaluationRecord = new EvaluationRecordDAO()
                    .findByFileIdAndEvalTypeMax(fileId);
            String fromContent = evaluationRecord.getFormContent();
            EvaluationModel evaluationModel = new Gson().fromJson(fromContent,
                    EvaluationModel.class);

            additionalRequest = new AdditionalRequest();
            additionalRequest.setFileId(fileId);
            additionalRequest.setIsActive(new Long(Constants.CHECK_VIEW.VIEW));
            additionalRequest.setCreatorId(getUserId());
            additionalRequest.setCreatorName(getUserFullName());
            additionalRequest.setCreateDate(new Date());
            additionalRequest.setContent(evaluationModel.getContent());

        }
        additionalRequestDAO.saveOrUpdate(additionalRequest);
    }

    /**
     * Phe duyet ho so, kem CKS
     *
     * @return @throws Exception
     */
    public String onApproveFileSign() throws Exception {
        clearWarningMessage();
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);

        if (lstPermit.size() > 0) {// Da co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
        }
        permitDAO.saveOrUpdate(permit);
        Long bookNumber = putInBook(permit);// Vao so
        String receiveNo = getPermitNo(bookNumber);
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        VFileRtflieDAO rtDao = new VFileRtflieDAO();
        VFileRtfile rt = rtDao.findById(fileId);

        ExportModel model = new ExportModel();
        model.setSendNo(permit.getReceiveNo());
        model.setBusinessName(rt.getBusinessName());
        model.setSignDate(permit.getSignDate());
        String text_content = txtMainContent.getText();
        // model.setContent(evaluationModel.getContent());
        model.setContent(text_content);
        model.setLeaderSinged(getUserFullName());
        model.setRapidTestName(rt.getRapidTestName());
        model.setRapidTestCode(rt.getRapidTestCode());
        model.setNswFileCode(rt.getNswFileCode());

        ExportFileDAO exp = new ExportFileDAO();
        return exp.exportIDFNoSignXnn(model, false);
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Permit Permit) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(Permit.getPermitId(),
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    private String getPermitNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yy"); // Just the year,
        // with 2 digits
        String year = sdf.format(Calendar.getInstance().getTime());
        permitNo += "/" + year;
        permitNo += "/ATTP-KN";
        return permitNo;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        SignPdfFile pdfSig = new SignPdfFile();// pdfSig = new SignPdfFile();
        String base64Hash;
        String certSerial = x509Cert.getSerialNumber().toString(16);

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");
        FileUtil.mkdirs(filePath);
        String outputFileFinalName = "_" + (new Date()).getTime() + ".pdf";
        String outPutFileFinal = filePath + outputFileFinalName;
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String linkImageSign = folderPath + separator
                    + caur.get(0).getSignature();
            String linkImageStamp = folderPath + separator
                    + caur.get(0).getStamper();
            Pdf pdfProcess = new Pdf();
            // chen chu ki
            try {
                pdfProcess.insertImageAll(fileToSign, outPutFileFinal,
                        linkImageSign, linkImageStamp, null);
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
            // chen CKS
            if (pdfProcess.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }

            base64Hash = pdfSig.createHash(outPutFileFinal,
                    new Certificate[]{x509Cert});

            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64HASH.setValue(base64Hash);
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");

        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    private void fillFinalFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        lstRapidTestAttach = rDaoHe.findRapidTestAllLastAttachByCode(fileId,
                rtSignType);
        this.finalFileListbox.setModel(new ListModelArray(lstRapidTestAttach));
    }

    private Permit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        // Attachs att = attDAOHE.findById(attachId); findByAttachId
        Attachs att = attDAOHE.findByAttachId(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    // // download signed pdf
    // @Listen("onDownloadFinalFile = #finalFileListbox")
    // public void onDownloadFinalFile(Event event) throws FileNotFoundException
    // {
    // Attachs attachs = (Attachs) event.getData();
    // AttachDAO attDAO = new AttachDAO();
    // attDAO.downloadFileAttach(attachs);
    // }
    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();

        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);

        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(att);
        fillFinalFileListbox(fileId);
    }

    private void setWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_RT.SendMs_04);
        md.setPhase(0l);
        md.setFeeUpdate(false);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

}
