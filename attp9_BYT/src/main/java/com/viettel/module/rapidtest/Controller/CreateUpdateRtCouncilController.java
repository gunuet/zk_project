package com.viettel.module.rapidtest.Controller;

import java.io.IOException;
import java.util.Date;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.module.rapidtest.BO.RtCouncil;
import com.viettel.module.rapidtest.DAO.RtCouncilDAO;
import com.viettel.utils.Constants;

/**
 * 
 * @author THANHDV
 */
public class CreateUpdateRtCouncilController extends BaseComposer {
	//
	// @Wire
	// Textbox tbId, tbName, tbDescription;
	@Wire
	Datebox dbTimeDate;
	@Wire
	Listbox lbDayType;
	@Wire
	Window createUpdate;
	@Wire
	Textbox tbrtCouncilName, tbrole, tbrtPosition, tbcode, tbId, tborderBy;

	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		loadInfoToForm();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Listen("onClick=#btnSave")
	public void onSave() throws IOException {
		RtCouncil rtCouncil = new RtCouncil();
		if (tbId.getValue() != null && !tbId.getValue().isEmpty()) {
			rtCouncil.setId(Long.parseLong(tbId.getValue()));
		}

		if (tbrtCouncilName.getValue() != null
				&& tbrtCouncilName.getValue().trim().length() == 0) {
			showNotification("Phải nhập tên thành viên hội đồng ",
					Constants.Notification.ERROR);
			tbrtCouncilName.focus();
			return;
		} else {
			rtCouncil.setName(tbrtCouncilName.getValue());
		}

		if (tbrole.getValue() != null && tbrole.getValue().trim().length() == 0) {
			showNotification("Phải nhập vai trò thành viên",
					Constants.Notification.ERROR);
			tbrole.focus();
			return;
		} else {
			rtCouncil.setRole(tbrole.getValue());
		}
		if (tbrtPosition.getValue() != null
				&& tbrtPosition.getValue().trim().length() == 0) {
			showNotification("Phải nhập vị trí thành viên",
					Constants.Notification.ERROR);
			tbrtPosition.focus();
			return;
		} else {
			rtCouncil.setPosition(tbrtPosition.getValue());
		}
		if (tbcode.getValue() != null && tbcode.getValue().trim().length() == 0) {
			showNotification("Phải nhập mã số", Constants.Notification.ERROR);
			tbcode.focus();
			return;
		} else {
			rtCouncil.setCode(tbcode.getValue());
		}
		if (tborderBy.getValue() != null && !tborderBy.getValue().isEmpty()) {
			rtCouncil.setOrderBy(Long.parseLong(tborderBy.getValue()));
		} else {
			showNotification("Phải nhập thứ tự", Constants.Notification.ERROR);
			tborderBy.focus();
			return;
		}

		rtCouncil.setIsActive(1L);
		rtCouncil.setCreatedBy(getUserId());
		rtCouncil.setCreatedDate(new Date());
		RtCouncilDAO rtCouncilDAO = new RtCouncilDAO();
		rtCouncilDAO.saveOrUpdate(rtCouncil);
		if (tbId.getValue() != null && !tbId.getValue().isEmpty()) {
			// update thì detach window
			createUpdate.detach();
		} else {
			// them moi thi clear window
			loadInfoToForm();
		}
		showNotification("Lưu thành công", Constants.Notification.INFO);

		Window parentWnd = (Window) Path.getComponent("/managertCouncilWindow");
		Events.sendEvent(new Event("onReload", parentWnd, null));
	}

	public void loadInfoToForm() {
		Long id = (Long) Executions.getCurrent().getArg().get("id");
		if (id != null) {
			RtCouncilDAO objhe = new RtCouncilDAO();
			RtCouncil rs = objhe.findById(id);
			if (rs.getId() != null) {
				tbId.setValue(rs.getId().toString());
			}
			if (rs.getName() != null) {
				tbrtCouncilName.setValue(rs.getName().toString());
			}
			if (rs.getCode() != null) {
				tbcode.setValue(rs.getCode().toString());
			}
			if (rs.getPosition() != null) {
				tbrtPosition.setValue(rs.getPosition().toString());
			}

			if (rs.getRole() != null) {
				tbrole.setValue(rs.getRole().toString());
			}
			if (rs.getOrderBy() != null) {
				tborderBy.setValue(rs.getOrderBy().toString());
			}
		} else {
			tbrtCouncilName.setValue("");
			tbcode.setValue("");
			tbrtPosition.setValue("");
			tbrole.setValue("");
			tborderBy.setValue("");

		}
	}

	@Override
	public String getConfigLanguageFile() {
		return "language_COSMETIC_vi";
	}
}
