package com.viettel.module.rapidtest.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.BO.VRtPaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.parent.PaymentController;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.module.rapidtest.DAO.VRtPaymentinfoDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.model.include.EvaluationAttachFileModel;
import com.viettel.utils.*;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;

/**
 *
 * @author quynhhv1 Doanh nghiep thanh toan 1 ho so
 */
public class PaymentRtBusinessAttachController extends PaymentController {

    @Wire
    private Textbox txtPaymentName;
    @Wire
    private Textbox txtPaymentAddress;
    @Wire
    private Window businessWindow;
    @Wire
    Radiogroup rgTypePayment;
    @Wire("#lbWarning")
    private Label lbWarning;
    @Wire("#toplbWarning")
    private Label toplbWarning;
    @Wire("#lbSumMoney")
    private Label lbSumMoney;
    @Wire("#lbtextMoney")
    private Label lbtextMoney;
    @Wire
    Textbox txtBillNo, txtBillKey;
    @Wire
    Datebox dbDayPayment;
    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;
    @Wire
    Textbox txtFileName;
    @Wire
    Image imgDelFile;
    @Wire
    Label lbdeletefile;
    @Wire
    Textbox txtValidate;
    @Wire
    private Label lbMahoso, lbTensp, lbPhase;
    @Wire
    Radio radioKeypay, radioBanktransfer, radioImmediaacy;
    private Media media;
    private Attachs attachCurrent;
    Long typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;// kieu
    // thanh
    // toan
    // 1//keypay
    // //2
    // chuyen
    // khoan
    // //3
    // tien
    // mat
    int noPayment = 0;
    private Window parentWindow;
    Long lsumMoney = 0L;
    Long docId = null;
    Long docType = null;
    Bill objBill = null;
    List<VRtPaymentInfo> listVIdfPaymentInfo;
    VRtPaymentInfo vIdfPaymentInfo = null;
    // BookDocument mBook;

    private Long attachFileId = 0L;
    private ListModelList<EvaluationAttachFileModel> listFileModel;
    private EvaluationAttachFileModel attachFileModel;
    List<VFileRtAttachAll> lstRapidTestAttach;
    private Long categoryCode;
    @Wire
    private List listRapidTestFileType;
    @Wire
    private List<Media> listMedia;
    @Wire
    private Listbox fileListbox, lbRapidTestFileType, lbAssay;
    @Wire
    private Vlayout flist;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        // mBook = new BookDocument();
        listVIdfPaymentInfo = new ArrayList<>();
        listVIdfPaymentInfo.clear();
        // sTypeView=(String) arguments.get("typeView");
        docId = (Long) arguments.get("docId");
        // docId = (Long) arguments.get("fileId");
        docType = (Long) arguments.get("docType");

        listMedia = new ArrayList();
        attachFileModel = new EvaluationAttachFileModel();
        listFileModel = new ListModelList<EvaluationAttachFileModel>();
        categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.FILE_KET_QUA_KHAO_NGHIEM;

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onClick = #onPrint")
    public void onPrint(Event e) throws IOException {
        AttachDAOHE attDAOHE = new AttachDAOHE();
        List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(),
                Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        if ((items == null) || (items.size() == 0)) {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
            return;
        }
        Attachs item = items.get(0);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(item);
    }

    private void createWindowViewPrint() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("docId", docId);
        arguments.put("docType", docType);
        arguments.put("windowParent", businessWindow);
        Window window = createWindow("businessWindowPrint",
                "/Pages/module/payment/paymentSingleBusinessPrint.zul",
                arguments, Window.MODAL);
        window.doModal();

    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }

        VRtPaymentinfoDAO vIDfDao = new VRtPaymentinfoDAO();
        listVIdfPaymentInfo = vIDfDao.getListFileIdPhase(docId,
                Long.valueOf(lbPhase.getValue()));

        BillDAO objBillDAOHE = new BillDAO();
        if (listVIdfPaymentInfo.size() > 0) {
            VRtPaymentInfo vPaymentInfo = listVIdfPaymentInfo.get(0);
            objBill = objBillDAOHE.findById(vPaymentInfo.getBillId());
        }

        /*
         * BookDocumentDAOHE bookDao = new BookDocumentDAOHE(); if ((docId !=
         * null) && (docType != null)) { mBook =
         * bookDao.getBookInFromDocumentId(docId, docType); if (mBook != null) {
         * lbbookNumber.setValue(mBook.getBookNumber().toString()); } else {
         * lbbookNumber.setValue(""); } }
         */
        if (objBill != null) {
            if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY) {
                rgTypePayment.setSelectedItem(radioKeypay);
            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN) {
                rgTypePayment.setSelectedItem(radioBanktransfer);
            } else if (objBill.getPaymentTypeId() == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT) {
                rgTypePayment.setSelectedItem(radioImmediaacy);

            }
            // txtBillKey.setValue(objBill.getBillSign());
            txtBillNo.setValue(objBill.getCode());
            dbDayPayment.setValue(objBill.getPaymentDate());
            txtPaymentName.setValue(objBill.getCreatetorName());

            AttachDAOHE attDAOHE = new AttachDAOHE();
            List<Attachs> items = attDAOHE.getByObjectId(objBill.getBillId(),
                    Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);
            if ((items != null) && (items.size() > 0)) {



                /*
                 * try { media = new AMedia(f, null, null); } catch
                 * (FileNotFoundException ex) {
                 * 
                 * Logger.getLogger(PaymentRtBusinessAttachController.class.getName
                 * ()).log(Level.SEVERE, null, ex); }
                 */

                txtFileName.setValue(items.get(0).getAttachName());
                imgDelFile.setVisible(true);
                lbdeletefile.setVisible(true);
            }
        }

        if (listVIdfPaymentInfo.size() > 0) {
            vIdfPaymentInfo = listVIdfPaymentInfo.get(0);
            lsumMoney = vIdfPaymentInfo.getCost();
            lbMahoso.setValue(vIdfPaymentInfo.getNswFileCode());
            // lbNameOfficerFee.setValue(vIdfPaymentInfo.getPaymentActionUser());
            // lbDateFee.setValue(sdf.format(vIdfPaymentInfo.getCreateDate()));
            txtPaymentAddress.setValue(vIdfPaymentInfo.getBusinessAddress());
            txtPaymentName.setValue(vIdfPaymentInfo.getBusinessName());
        }
        // lbSumMoney.setValue(CommonFns.formatNumber(lsumMoney, "###,###,###")
        // + " đồng ");
        // giangnh20, su dung ham nay de dau phan cach nghin la dau . thay vi
        // dau ,
        String formatedMoneyText = formatNumber(lsumMoney, "###,##0");
        lbSumMoney.setValue("".equals(formatedMoneyText) ? ""
                : formatedMoneyText + " đồng");
        txtValidate.setValue("0");
        dbDayPayment.setValue(new Date());
        lbtextMoney.setValue(numberToString(lsumMoney));

        fillFileListbox(docId);
    }

    public void onAccept() {
        if (isValidate()) {
            // lay ngay hien tai
            Date date = new Date();
            // update trong bang bill
            BillDAO objBillDAOHE = new BillDAO();

            if (objBill == null) {
                objBill = new Bill();
            }

            objBill.setCreatetorName(txtPaymentName.getText().trim());
            objBill.setCreateDate(date);
            objBill.setPaymentDate(dbDayPayment.getValue());
            objBill.setTotal(lsumMoney);
            objBill.setCreatorId(getUserId());
            objBill.setStatus(ConstantsPayment.PAYMENT.PAY_ALREADY);
            objBill.setPaymentTypeId(Long.valueOf(typePayment));
            objBill.setCode(txtBillNo.getValue().trim());
            objBill.setCreatorAddress(txtPaymentAddress.getText().trim());
            objBillDAOHE.saveOrUpdate(objBill);
            Long idBill = objBill.getBillId();
            // update trong paymentInfo
            PaymentInfoDAO objFreePaymentInfo = new PaymentInfoDAO();
            PaymentInfo paymentInfo;
            int sizeList = listVIdfPaymentInfo.size();
            for (int i = 0; i < sizeList; i++) {
                paymentInfo = objFreePaymentInfo.findById(listVIdfPaymentInfo
                        .get(i).getPaymentInfoId());
                paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_ALREADY);
                paymentInfo.setBillId(idBill);
                paymentInfo.setUpdateDate(date);
                paymentInfo.setPaymentDate(dbDayPayment.getValue());
                paymentInfo.setPaymentTypeId(Long.valueOf(typePayment));
                objFreePaymentInfo.getSession().merge(paymentInfo);
            }
            // update attach File
            try {
                AttachDAO attDAO = new AttachDAO();

                if (media != null) {
                    attDAO.saveFileAttach(media, idBill,
                            Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY,
                            null);
                }

                /*
                 * AttachDAO base = new AttachDAO(); for
                 * (EvaluationAttachFileModel file : listFileModel) {
                 * base.saveFileAttachDes(file.getFileContent(),
                 * file.getObjId(), file.getObjType(), file.getFileType(),
                 * file.getAttachDes()); }
                 */
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }

            txtValidate.setValue("1");

            showNotification("Bạn đã thanh toán và upload file thành công. ",
                    Constants.Notification.INFO);

            if (parentWindow != null) {
                Events.sendEvent("onReloadPage", parentWindow, null);
            }
        }
    }

    @Listen("onCheck = #rgTypePayment")
    public void onCheck(Event e) {
        switch (rgTypePayment.getSelectedItem().getId()) {
            case "radioKeypay":
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_KEYPAY;
                break;
            case "radioImmediaacy":
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT;
                break;
            case "radioBanktransfer":
                typePayment = ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN;
                break;
        }
    }

    private boolean isValidate() {
        String message;
        if (typePayment == 0) {
            message = "Bạn vui lòng chọn hình thức thanh toán";
            setWarningMessage(message);
            return false;
        }
        if ("".equals(txtPaymentName.getText().trim())) {
            message = "Bạn vui lòng nhập tên người thanh toán";
            txtPaymentName.focus();
            setWarningMessage(message);
            return false;
        }

        if ((typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN)
                || (typePayment == ConstantsPayment.PAYMENT.FEE_PAYMENT_TYPE_CODE_TIENMAT)) {
            if (dbDayPayment.getValue() == null) {
                message = "Bạn vui lòng nhập ngày thanh toán";
                setWarningMessage(message);
                dbDayPayment.setFocus(true);
                return false;
            } else {
                Date mDate = new Date();
                if (dbDayPayment.getValue().after(mDate)) {
                    message = "Ngày thanh toán phải nhỏ hơn ngày hiện tại";
                    setWarningMessage(message);
                    dbDayPayment.setFocus(true);
                    return false;
                }
            }

            if ("".equals(dbDayPayment.getValue().toString())) {
                message = "Bạn vui lòng nhập ngày thanh toán";
                setWarningMessage(message);
                return false;
            }

            if ("".equals(txtBillNo.getValue().toString())) {
                message = "Bạn vui lòng số biên lai";
                setWarningMessage(message);
                txtBillNo.setFocus(true);
                return false;
            }

            if (fileListbox.getItemCount() == 0) {
                message = "Chưa nhập file kết quả khảo nghiệm";
                lbAssay.focus();
                setWarningMessage(message);
                return false;
            }
        }
        return true;
    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
        toplbWarning.setValue(message);
    }

    @Listen("onUpload = #btnUpload")
    public void onUpload(UploadEvent evt) throws IOException {

        media = evt.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            showNotification("Định dạng file không được phép tải lên",
                    Constants.Notification.WARNING);
            return;
        }
        if (attachCurrent != null) {
            Messagebox.show("Bạn có đồng ý thay thế tệp biểu mẫu cũ?",
                    "Thông báo", Messagebox.OK | Messagebox.CANCEL,
                    Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                        public void onEvent(Event evt)
                        throws InterruptedException {
                            if (Messagebox.ON_OK.equals(evt.getName())) {
                                txtFileName.setValue(media.getName());
                                imgDelFile.setVisible(true);
                                lbdeletefile.setVisible(true);
                                String fullPath = attachCurrent.getAttachPath()
                                + attachCurrent.getAttachName();
                                // xóa file cũ nếu có
                                File f = new File(fullPath);
                                if (f.exists()) {
                                    ConvertFileUtils.deleteFile(fullPath);
                                }
                                AttachDAOHE attachDAOHE = new AttachDAOHE();
                                attachDAOHE.delete(attachCurrent);
                            }
                        }
                    });
        } else {
            txtFileName.setValue(media.getName());
            imgDelFile.setVisible(true);
            lbdeletefile.setVisible(true);
        }
    }

    @Listen("onClick = #imgDelFile")
    public void onClickDelete(Event ev) {
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
        lbdeletefile.setVisible(false);
    }

    @Listen("onClick = #lbdeletefile")
    public void onClicklbDelete(Event ev) {
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
        lbdeletefile.setVisible(false);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        onAccept();
    }

    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        categoryDAOHE = new CategoryDAOHE();
        listRapidTestFileType = categoryDAOHE.findAllCategoryByCode(
                Constants.CATEGORY_TYPE.RAPID_TEST_FILE_TYPE,
                categoryCode.toString());
        lstModel = new ListModelList(listRapidTestFileType);
        return lstModel;

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    @Listen("onClick=#btnCreate")
    public void onCreateRapidTestFileType() throws IOException, Exception {

        if (listMedia.isEmpty()) {
            showNotification("Chọn tệp tải lên trước khi thêm mới!",
                    Constants.Notification.WARNING);

            return;
        }

        AttachDAO base = new AttachDAO();

        for (Media media : listMedia) {
            attachFileModel = new EvaluationAttachFileModel();
            attachFileId = attachFileId + 1;
            attachFileModel.setAttachID(attachFileId);
            attachFileModel.setFileContent(media);
            attachFileModel.setFileName(media.getName());
            attachFileModel.setFileType(Long
                    .valueOf((String) lbRapidTestFileType.getSelectedItem()
                            .getValue()));
            attachFileModel.setFileTypeName(lbRapidTestFileType
                    .getSelectedItem().getLabel());
            attachFileModel.setObjId(docId);
            attachFileModel.setAttachDes(lbAssay.getSelectedItem().getLabel());
            attachFileModel
                    .setObjType(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
            // listFileModel.add(attachFileModel);

            base.saveFileAttachDes(attachFileModel.getFileContent(),
                    attachFileModel.getObjId(), attachFileModel.getObjType(),
                    attachFileModel.getFileType(),
                    attachFileModel.getAttachDes());
        }

        // fileListbox.setModel(listFileModel);
        if (listMedia.size() > 0) {
            listMedia.clear();
        }
        flist.getChildren().clear();

        fillFileListbox(docId);
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        rDaoHe.delete(obj.getAttachId());
        // listFileModel.remove(obj);
        fillFileListbox(docId);
    }

    private void fillFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        lstRapidTestAttach = rDaoHe.findRapidTestAllAttachByCode(fileId,
                categoryCode);
        this.fileListbox.setModel(new ListModelArray(lstRapidTestAttach));
        // onSelect();//An hien form
    }

    @Listen("onUpload = #btnAttach")
    public void onUploadAttach(UploadEvent event)
            throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();

        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                	String sExt = ResourceBundleUtil.getString("extend_file", "config");
	                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("z-valign-left");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listMedia.remove(media);
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    public ListModelList fillBasicAssay() {
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        obj = objDAO.findMaxByFileIdAndEvalType(docId,
                Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
        if (obj != null) {
            Gson gson = new Gson();
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(),
                    EvaluationModel.class);
            return new ListModelList(evModel.getEvaluationBasicAssay());
        } else {
            return null;
        }
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }
}
