/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.Controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;

/**
 *
 * @author giangpn
 */
public class PublicFileManageImportCRUDController extends BaseComposer {

    @Wire
    Listbox lbProcedure, lbFee, lbSub;
    @Wire
    Listbox lbFileType;
    @Wire
    Paging userPagingBottom;

    @Wire
    Textbox attachDes;

    @Wire
    Window PublicFileManagerCRUD;
    Attachs searchForm, attach;

    @Wire
    Button btnAttach;
    @Wire
    Vlayout flist;
    List<Media> listMedia;
    private List<Attachs> listFileAttach;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        loadBeforeToForm();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);

        CategoryDAOHE cdhe = new CategoryDAOHE();
        List lstFileType = cdhe.findAllCategorySearch(
                Constants.IMPORT_TYPE.IMPORT_FILE_TYPE, true, "--Chọn--");
        ListModelArray lstModel = new ListModelArray(lstFileType);
        listMedia = new ArrayList();
        lbFileType.setModel(lstModel);
        lbFileType.renderAll();
        loadAferToForm();
    }

    public void loadAferToForm() {
        Long id = (Long) Executions.getCurrent().getArg().get("id");
        if (id != null) {
            AttachDAOHE attachsDAOHE = new AttachDAOHE();
            listFileAttach = attachsDAOHE.getByObjectId(id,
                    Constants.OBJECT_TYPE.IMPORT_FILE_TYPE);
            loadFileAttach(listFileAttach);
            attach = attachsDAOHE.findById(id);
            if (attach.getAttachType() != null) {
                for (int i = 0; i < lbFileType.getListModel().getSize(); i++) {
                    Category ct = (Category) lbFileType.getListModel()
                            .getElementAt(i);
                    if (attach.getAttachType().equals(ct.getCategoryId())) {
                        lbFileType.setSelectedIndex(i);
                        break;
                    }
                }
            }
        } else {
            attach = new Attachs();
            lbFileType.setSelectedIndex(0);
        }
    }

    public void loadBeforeToForm() {
        Long id = (Long) Executions.getCurrent().getArg().get("id");
        if (id != null) {
            AttachDAOHE objhe = new AttachDAOHE();
            attach = objhe.findById(id);

        } // them mới
        else {
            attach = new Attachs();
        }
    }

    @Listen("onClick=#btnCreate")
    public void onUpdate(Event ev) throws IOException {
        AttachDAO base = new AttachDAO();
        AttachDAOHE obj = new AttachDAOHE();
        // validate Loai ho so
        int idx = lbFileType.getSelectedItem().getIndex();
        if (idx == 0) {
            showNotification("Bạn phải chọn loại hồ sơ",
                    Constants.Notification.INFO);
            return;
        }
        // Neu ton tai trong co su lieu
        if (obj.CheckTypePublicFile(
                Long.parseLong(lbFileType.getSelectedItem().getValue()
                        .toString()), getUserId(),
                Constants.OBJECT_TYPE.IMPORT_FILE_TYPE, attach.getAttachId())) {
            showNotification(
                    "Lưu không thành công! Loại hồ sơ đã được lưu! Xin vui lòng chọn sửa!",
                    Constants.Notification.INFO, 2000);
            return;
        }

        if ((listMedia != null && listMedia.size() > 0)) {
            for (Media media : listMedia) {
                base.saveFileAttach(
                        media,
                        attach.getAttachId(),
                        attach,
                        getUserId(),
                        Constants.OBJECT_TYPE.IMPORT_FILE_TYPE,
                        Long.parseLong(lbFileType.getSelectedItem().getValue()
                                .toString()), attachDes.getText());
            }
        } else if (attach.getAttachId() != null) {
            base.saveFileAttach(
                    null,
                    attach.getAttachId(),
                    attach,
                    getUserId(),
                    Constants.OBJECT_TYPE.IMPORT_FILE_TYPE,
                    Long.parseLong(lbFileType.getSelectedItem().getValue()
                            .toString()), attachDes.getText());
        } else {
            showNotification("Phải chọn tệp đính kèm!",
                    Constants.Notification.INFO);
            return;
        }

        showNotification("Lưu thành công", Constants.Notification.INFO);
        PublicFileManagerCRUD.detach();
        Window parentWnd = (Window) Path.getComponent("/publicFileManage");
        Events.sendEvent(new Event("onReload", parentWnd, null));

    }

    @Listen("onDelete=#lbProcedureFeeSub")
    public void onDelete(Event ev) throws IOException {
        // ProcedureFeeSubprocedure rs = (ProcedureFeeSubprocedure)
        // lbProcedureFeeSub.getSelectedItem().getValue();
        // ProcedureFeeSubDAOHE pfshe = new ProcedureFeeSubDAOHE();
        // pfshe.delete(rs.getId());
        //
        // onSearch();
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        flist.getChildren().clear();
        // final Media media = event.getMedia();
        final Media media = event.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            String sExt = ResourceBundleUtil.getString("extend_file", "config");
            showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                    Constants.Notification.WARNING);
            return;
        } else {
            // luu file vao danh sach file
            listMedia.clear();
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            // A rm = new A("Xóa");
            // rm.addEventListener(Events.ON_CLICK,
            // new org.zkoss.zk.ui.event.EventListener() {
            // @Override
            // public void onEvent(Event event) throws Exception {
            // hl.detach();
            // // xoa file khoi danh sach file
            // listMedia.remove(media);
            // }
            // });
            // hl.appendChild(rm);
            // flist= new Vlayout();
            flist.appendChild(hl);
        }
    }

    public Attachs getAttach() {
        return attach;
    }

    private void loadFileAttach(List<Attachs> listFileAttach) {
        if (listFileAttach != null) {
            for (final Attachs attach : listFileAttach) {
                // layout hien thi ten file va nut "Xoa"
                final Hlayout hl = new Hlayout();
                hl.setSpacing("6px");
                hl.setClass("newFile");
                hl.appendChild(new Label(attach.getAttachName()));
                // A rm = new A("Xóa");
                // rm.addEventListener(Events.ON_CLICK,
                // new org.zkoss.zk.ui.event.EventListener() {
                // @Override
                // public void onEvent(Event event) throws Exception {
                // hl.detach();
                // // Set attach deactive
                // attach.setIsActive(Constants.Status.INACTIVE);
                // AttachDAOHE daoHE = new AttachDAOHE();
                // daoHE.saveOrUpdate(attach);
                // }
                // });
                // hl.appendChild(rm);
                flist.appendChild(hl);
            }

        }
    }

}
