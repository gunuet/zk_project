/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.module.rapidtest.DAOHE.ProcedureFeeSubDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.model.AttachCategoryModel;

/**
 * 
 * @author giangpn
 */
public class PublicFileManageImportController extends BaseComposer {

	@Wire
	Listbox fileListbox, lbFee, lbSub;
	@Wire
	Listbox lbFileType;
	@Wire
	Paging userPagingBottom;
	Attachs searchForm;

	@Wire
	Button btnAttach;
	@Wire
	Vlayout flist;

	@Wire
	Textbox txtattachName;
	List<Media> listMedia;

	@Override
	public void doAfterCompose(Component window) throws Exception {
		super.doAfterCompose(window);

		CategoryDAOHE cdhe = new CategoryDAOHE();
		List lstFileType = cdhe
				.findAllCategorySearch(Constants.IMPORT_TYPE.IMPORT_FILE_TYPE);

		ListModelArray lstModel = new ListModelArray(lstFileType);
		lbFileType.setModel(lstModel);
		listMedia = new ArrayList();
		onSearch();
	}

	@Listen("onClick=#btnSearch")
	public void onSearch() {userPagingBottom.setActivePage(0);
		searchForm = new Attachs();
		if (lbFileType.getSelectedItem() != null
				&& lbFileType.getSelectedItem().getIndex() != 0) {
			Long attachType = Long.parseLong(lbFileType.getSelectedItem()
					.getValue().toString());
			searchForm.setAttachType(attachType);
		}
		if (!txtattachName.getValue().matches("\\s*")) {
			searchForm.setAttachName(txtattachName.getValue());
		}
		// Fill danh sach loai danh muc
		fillDataToList();
	}

	@Listen("onReload=#publicFileManage")
	public void onReload() {
		onSearch();
	}

	private void fillDataToList() {
		AttachDAOHE obj = new AttachDAOHE();

		int take = userPagingBottom.getPageSize();
		int start = userPagingBottom.getActivePage()
				* userPagingBottom.getPageSize();
		// PagingListModel plm = objhe.search(searchForm, start, take);

		PagingListModel plm = obj.searchPublicFileImport(searchForm, start,
				take, getUserId());
		userPagingBottom.setTotalSize(plm.getCount());
		if (plm.getCount() == 0) {
			userPagingBottom.setVisible(false);
		} else {
			userPagingBottom.setVisible(true);
		}

		ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
		fileListbox.setModel(lstModel);
	}

	@Listen("onPaging = #userPagingBottom")
	public void onPaging(Event event) {
		fillDataToList();
	}

	@Listen("onClick=#btnCreate")
	public void onCreate() throws IOException {
		Window window = (Window) Executions.createComponents(
				"/Pages/rapidTest/PublicFileManageImportCRUD.zul", null, null);
		window.doModal();
	}

	@Listen("onEdit= #fileListbox")
	public void onEdit(Event ev) throws IOException {
		AttachCategoryModel pfs = (AttachCategoryModel) fileListbox
				.getSelectedItem().getValue();
		Map args = new ConcurrentHashMap();
		args.put("id", pfs.getAttach().getAttachId());
		Window window = (Window) Executions.createComponents(
				"/Pages/rapidTest/PublicFileManageImportCRUD.zul", null, args);
		window.doModal();
	}

	@Listen("onDownloadFile = #fileListbox")
	public void onDownloadFile(Event event) throws FileNotFoundException {
		AttachCategoryModel obj = (AttachCategoryModel) fileListbox
				.getSelectedItem().getValue();
		Long attachId = obj.getAttach().getAttachId();
		AttachDAOHE attDAOHE = new AttachDAOHE();
		Attachs att = attDAOHE.findById(attachId);

		String path = obj.getAttach().getFullPathFile();
		File f = new File(path);
		if (f.exists()) {
			Filedownload.save(f, att.getAttachName());
		} else {
			showNotification("File không còn tồn tại trên hệ thống",
					Constants.Notification.INFO);
		}

	}

	@Listen("onDeleteFile = #fileListbox")
	public void onDeleteFile(Event event) {
		Messagebox.show("Bạn có chắc chắn muốn xóa không?", "Xác nhận",
				Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener() {
					@Override
					public void onEvent(Event event) {
						if (null != event.getName()) {
							switch (event.getName()) {
							case Messagebox.ON_OK:
								// OK is clicked
								try {
									AttachCategoryModel obj = (AttachCategoryModel) fileListbox
											.getSelectedItem().getValue();
									// Long fileId = obj.getObjectId();
									RapidTestAttachDAOHE rDAOHE = new RapidTestAttachDAOHE();
									rDAOHE.delete(obj.getAttach().getAttachId());
									fillDataToList();
									showNotification("Xóa thành công",
											Constants.Notification.INFO);
								} catch (Exception e) {
									// showNotification(String.format(Constants.Notification.DELETE_ERROR,
									// Constants.DOCUMENT_TYPE_NAME.FILE),
									// Constants.Notification.ERROR);
									LogUtils.addLogDB(e);
								} finally {
								}
								break;
							case Messagebox.ON_NO:
								break;
							}
						}
					}
				});

	}
}
