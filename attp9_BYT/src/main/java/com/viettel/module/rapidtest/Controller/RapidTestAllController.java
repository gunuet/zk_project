package com.viettel.module.rapidtest.Controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.model.SearchModel;

import java.text.SimpleDateFormat;

/**
 *
 * @author Linhdx
 */
public class RapidTestAllController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #dbFromDayModify")
    private Datebox dbFromDayModify;
    @Wire("#incSearchFullForm #dbToDayModify")
    private Datebox dbToDayModify;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    @Wire("#incSearchFullForm #lboxStatus")
    private Listbox lboxStatus;
    @Wire("#incSearchFullForm #tbRapidTestNo")
    private Textbox tbRapidTestNo;
    @Wire("#incSearchFullForm #lboxDocumentTypeCode")
    private Listbox lboxDocumentTypeCode;
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire("#incList #lbList")
    private Listbox lbList;
    @Wire
    private Window rapidTestAll;
    private SearchModel lastSearchModel;
    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;
    Long UserId = getUserId();
    CategoryDAOHE dao = new CategoryDAOHE();
    // Long RoleId = Long.parseLong(dao.LayTruong("RoleUserDept", "userId",
    // UserId.toString(), "roleId"));
    private Users user;
    private String sPositionType;
    private Constants constants = new Constants();
    String sBoSung = Constants.PROCESS_STATUS.SENT_RETURN.toString();
    String statusStr;
    // list cac checkbox đã check trong list
    List<VFileRtfile> itemsCheckedList, lstCosmetic;
    // list cac checkbox đã check trong 1 trang
    List<Listitem> itemsChecksedPage;
    String menuTypeStr;

    // luong
    private VFileRtfile VFileRtfile;
    private Files files;
    private SearchModel sm;
    private List<Category> lstStatusAll = new ArrayList<>();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        itemsCheckedList = new ArrayList<VFileRtfile>();
        lastSearchModel = new CosmeticFileModel();
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        FILE_TYPE_STR = (String) arguments.get("filetype");
        String deptIdStr = (String) arguments.get("deptid");
        menuTypeStr = (String) arguments.get("menuType");
        sPositionType = (String) arguments.get("PositionType");
        statusStr = (String) arguments.get("status");
        Long statusL = null;
        try {
            if (statusStr != null) {
                statusL = Long.parseLong(statusStr);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        if (statusL != null) {
            lastSearchModel.setStatus(statusL);
        }

        lastSearchModel.setMenuTypeStr(menuTypeStr);
        try {
            DEPT_ID = Long.parseLong(deptIdStr);
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            DEPT_ID = -1l;
        }
        FILE_TYPE = WorkflowAPI.getInstance().getProcedureTypeIdByCode(
                FILE_TYPE_STR);

        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);
        CategoryDAOHE cdhe = new CategoryDAOHE();
        RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
        //     List lstStatusAll = cdhe.getSelectCategoryByType(
        //             Constants.CATEGORY_TYPE.FILE_STATUS, "name");
        List lstStatus;
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            lstStatus = objDAOHE.findListStatusByCreatorId(getUserId());
            dbFromDay.setValue(null);
            lastSearchModel.setCreateDateFrom(null);
            dbToDay.setValue(new Date());
            lastSearchModel.setCreateDateTo(new Date());
        } else {
            lstStatus = objDAOHE.findListStatusByReceiverAndDeptId(
                    lastSearchModel, getUserId(), getDeptId());
            dbFromDayModify.setValue(null);
            lastSearchModel.setModifyDateFrom(null);
            dbToDayModify.setValue(new Date());
            lastSearchModel.setModifyDateTo(new Date());
        }
        if (lstStatus != null && !lstStatus.isEmpty()) {
            lstStatusAll = cdhe.findStatusByVal(lstStatus.toArray());
            ListModelArray lstModelProcedure = new ListModelArray(lstStatusAll);
            lboxStatus.setModel(lstModelProcedure);

            // set multi check
            onSearch();
        }
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        // validate search
        // neu an nut search reset lại itemsCheckedList
        itemsCheckedList = new ArrayList<VFileRtfile>();
        if (dbFromDay != null && dbToDay != null && dbFromDay.getValue() != null && dbToDay.getValue() != null) {
            if (dbFromDay.getValue().compareTo(dbToDay.getValue()) > 0) {
                showNotification(
                        "Thời gian từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                return;
            }
        }
        if (dbFromDayModify != null && dbToDayModify != null && dbFromDayModify.getValue() != null && dbToDayModify.getValue() != null) {
            if (dbFromDayModify.getValue().compareTo(dbToDayModify.getValue()) > 0) {
                showNotification(
                        "Thời gian từ ngày không được lớn hơn đến ngày",
                        Constants.Notification.WARNING, 2000);
                return;
            }
        }

        lastSearchModel.setCreateDateFrom(dbFromDay == null ? null : dbFromDay
                .getValue());
        lastSearchModel.setCreateDateTo(dbToDay == null ? null : dbToDay
                .getValue());
        lastSearchModel.setModifyDateFrom(dbFromDayModify == null ? null
                : dbFromDayModify.getValue());
        lastSearchModel.setModifyDateTo(dbToDayModify == null ? null
                : dbToDayModify.getValue());
        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        lastSearchModel.setRapidTestNo(tbRapidTestNo.getValue());
        // lastSearchModel.setBrandName(tbCosmeticBrand.getValue());
        // lastSearchModel.setProductName(tbCosmeticProductName.getValue());
        // lastSearchModel.setBusinessName(tbCosmeticCompanyName == null ? null
        // : tbCosmeticCompanyName.getValue());
        // lastSearchModel.setBusinessAddress(tbCosmeticCompanyAddress == null ?
        // null : tbCosmeticCompanyAddress.getValue());
        // lastSearchModel.setDistributePersonName(tbCosmeticDistributePersonName
        // == null ? null : tbCosmeticDistributePersonName.getValue());
        // lastSearchModel.setTaxCode(tbBusinessTaxCode == null ? null :
        // tbBusinessTaxCode.getValue());
        // lastSearchModel.setMenuType(tbEquipmentNo.getValue());
        if (lboxStatus.getSelectedItem() != null) {
            try {
                Object value = lboxStatus.getSelectedItem().getValue();
                if (value != null && value instanceof String) {
                    Long valueL = Long.valueOf((String) value);
                    if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                        lastSearchModel.setStatus(valueL);
                    }
                } else {
                    lastSearchModel.setStatus(null);
                }
            } catch (NumberFormatException e) {
                LogUtils.addLogDB(e);
            }

        }
        if (statusStr != null) {
            if (statusStr.equals(sBoSung)) {
                lastSearchModel.setStatus(Long.parseLong(sBoSung));
            }
        }

        if (lboxDocumentTypeCode.getSelectedItem() != null) {
            String value = lboxDocumentTypeCode.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                    lastSearchModel.setDocumentTypeCode(valueL);
                } else {
                    lastSearchModel.setDocumentTypeCode(null);
                }
            }
        }

        reloadModel(lastSearchModel);
    }

    public String getSendDate(VFileRtfile f) {
        SimpleDateFormat formatterDateTime = new SimpleDateFormat("dd-MM-yyyy");
        String rs = formatterDateTime.format(f.getCreateDate());
        if (sm != null) {
            ProcessDAOHE pDAO = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process p = pDAO.getLastByUserId(f.getFileId(), UserId);
            if (p != null) {
                rs = formatterDateTime.format(p.getSendDate());
            }
        }
        return rs;
    }

    private void reloadModel(SearchModel searchModel) {
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage()
                * userPagingBottom.getPageSize();
        PagingListModel plm;
        RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(),
                    start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel,
                    getUserId(), getDeptId(), start, take);
            sm = searchModel;
        }

        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstCosmetic = plm.getLstReturn();
        lstModel.setMultiple(true);

        // itemsCheckedList set selected
        lbList.setModel(lstModel);
        lbList.renderAll();
        List<Listitem> listItem = lbList.getItems();
        Set<Listitem> itemselected = new HashSet();
        for (Listitem itemlb : listItem) {
            for (int i = 0; i < itemsCheckedList.size(); i++) {
                if (itemsCheckedList.get(i).getFileId()
                        .equals(((VFileRtfile) itemlb.getValue()).getFileId())) {
                    itemselected.add(itemlb);
                    break;
                }
            }
        }
        lbList.setSelectedItems(itemselected);
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #rapidTestAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch=#rapidTestAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            // Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    // Show popup khi an nut thao tac
    @Listen("onShowActionMulti=#rapidTestAll")
    public void onShowActionMulti() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("lstCosmetic", itemsCheckedList);
        Window window = createWindow("MultiProcess",
                "/Pages/module/cosmetic/viewMultiProcess.zul", arguments,
                Window.MODAL);
        window.setMode(Window.Mode.MODAL);
    }

    @Listen("onChangeTime=#rapidTestAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            if (dbFromDay != null) {
                dbFromDay.setValue(model.getFromDate());
            }
            if (dbToDay != null) {
                dbToDay.setValue(model.getToDate());
            }
            if (dbFromDayModify != null) {
                dbFromDayModify.setValue(model.getFromDate());
            }
            if (dbToDayModify != null) {
                dbToDayModify.setValue(model.getToDate());
            }

            onSearch();
        }
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", rapidTestAll);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("procedureId", FILE_TYPE);

        arguments.put("deptId", DEPT_ID);
        return arguments;

    }

    @Listen("onOpenCreate=#rapidTestAll")
    public void onOpenCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("CRUDMode", "CREATE");
        setParam(arguments);
        Window window = createWindow("windowCRUD",
                "/Pages/cosmetic/cosmeticCRUD.zul", arguments, Window.EMBEDDED);
        window.setMode(Window.Mode.EMBEDDED);
        // rapidTestAll.setVisible(false);
    }

    @Listen("onSelect =#incList #lbList")
    public void onSelect(Event event) {
        Listbox lb = (Listbox) event.getTarget();
        List<Listitem> li = lb.getItems();
        Set<Listitem> liselect = lb.getSelectedItems();
        // loai bo nhưng item trong lb co trong itemsCheckedList
        for (Listitem itemlb : li) {
            for (VFileRtfile items : itemsCheckedList) {
                if (((VFileRtfile) itemlb.getValue()).getFileId().equals(
                        items.getFileId())) {
                    itemsCheckedList.remove(items);
                    break;
                }
            }
        }
        // add item check vao itemsCheckedList
        for (Listitem item : liselect) {
            itemsCheckedList.add((VFileRtfile) item.getValue());
        }
    }

    @Listen("onOpenView = #incList #lbList")
    public void onOpenView(Event event) {
        VFileRtfile obj = (VFileRtfile) event.getData();
        createWindowView(obj.getFileId(), Constants.DOCUMENT_MENU.ALL,
                rapidTestAll);
        rapidTestAll.setVisible(false);

    }

    @Listen("onRefresh =#rapidTestAll")
    public void onRefresh(Event event) {
        onOpenView(event);
    }

    @Listen("onSelectedProcess = #rapidTestAll")
    public void onSelectedProcess(Event event) {
        rapidTestAll.setVisible(false);
    }

    private void createWindowView(Long id, int menuType, Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView", "/Pages/rapidTest/viewRapidTest.zul",
                arguments, Window.EMBEDDED);
    }

    @Listen("onSave = #rapidTestAll")
    public void onSave(Event event) {
        rapidTestAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
    }

    @Listen("onVisible =  #rapidTestAll")
    public void onVisible() {
        reloadModel(lastSearchModel);
        rapidTestAll.setVisible(true);
    }

    @Listen("onOpenUpdate = #incList #lbList")
    public void onOpenUpdate(Event event) {
        VFileRtfile obj = (VFileRtfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowCRUDRapidTest",
                "/Pages/rapidTest/rapidTestCRUD.zul", arguments,
                Window.EMBEDDED);
        rapidTestAll.setVisible(false);
    }
    
    @Listen("onWithdraw = #incList #lbList")
    public void onWithdraw(Event event) {
        VFileRtfile obj = (VFileRtfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("parentWindow", rapidTestAll);
        setParam(arguments);
        createWindow("windowWithdraw",
                "/Pages/rapidTest/withDraw.zul", arguments,
                Window.POPUP);
        //rapidTestAll.setVisible(false);
    }

    @Listen("onDownLoadGiayPhep = #incList #lbList")
    public void onDownLoadGiayPhep(Event event) throws FileNotFoundException,
            IOException {
        VFileRtfile obj = (VFileRtfile) event.getData();
        Long fileid = obj.getFileId();
        RapidTestDAOHE cosDao = new RapidTestDAOHE();
        CategoryDAOHE catDaohe = new CategoryDAOHE();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        // co giay phep
        if (check == 1) {
            AttachDAOHE attachDaoHe = new AttachDAOHE();
            Long cospermit = Long.parseLong(catDaohe.LayTruongPermitId(fileid));

            Attachs attachs = attachDaoHe.getByObjectIdAndAttachCat(cospermit,
                    Constants.OBJECT_TYPE.COSMETIC_PERMIT);
            // Nếu có file
            if (attachs != null) {
                // attDAO.downloadFileAttach(attachs);

                String path = attachs.getFullPathFile();
                File f = new File(path);
                // neu ton tai
                if (f.exists()) {
                    File tempFile = FileUtil.createTempFile(f,
                            attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                } // Khong se tao file
                else {
                    String folderPath = ResourceBundleUtil
                            .getString("dir_upload");
                    FileUtil.mkdirs(folderPath);
                    String separator = ResourceBundleUtil
                            .getString("separator");
                    folderPath += separator
                            + Constants_Cos.OBJECT_TYPE_STR.PERMIT_STR;
                    File fd = new File(folderPath);
                    if (!fd.exists()) {
                        fd.mkdirs();
                    }
                    f = new File(attachs.getFullPathFile());
                    if (f.exists()) {
                    } else {
                        f.createNewFile();
                    }
                    File tempFile = FileUtil.createTempFile(f,
                            attachs.getAttachName());
                    Filedownload.save(tempFile, path);
                }
            } // chua có file sẽ tạo trên hệ thông
            else {
                showNotification("File không còn tồn tại trên hệ thống!");
            }
        }
    }

    @Listen("onViewFlow = #lbList")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    @Listen("onDelete = #incList #lbList")
    public void onDelete(Event event) {
        final VFileRtfile obj = (VFileRtfile) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {
                                        if (isAbleToDelete(obj)) {
                                            FilesDAOHE objDAOHE = new FilesDAOHE();
                                            objDAOHE.delete(obj.getFileId());
                                            onSearch();
                                            showNotification(
                                                    String.format(
                                                            Constants.Notification.DELETE_SUCCESS,
                                                            Constants.DOCUMENT_TYPE_NAME.FILE),
                                                    Constants.Notification.INFO);
                                        } else {
                                            showNotification("Bạn không có quyền xóa");
                                        }

                                    } catch (Exception e) {
                                        showNotification(
                                                String.format(
                                                        Constants.Notification.DELETE_ERROR,
                                                        Constants.DOCUMENT_TYPE_NAME.FILE),
                                                Constants.Notification.ERROR);
                                        LogUtils.addLogDB(e);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    @Listen("onClick=#incSearchFullForm #btnReset")
    public void doResetForm() {
        tbNSWFileCode.setValue("");
        if (dbFromDay != null) {
            dbFromDay.setValue(null);
        }
        if (dbToDay != null) {
            dbToDay.setValue(new Date());
        }
        if (dbFromDayModify != null) {
            dbFromDayModify.setValue(null);
        }
        if (dbToDayModify != null) {
            dbToDayModify.setValue(new Date());
        }
        lboxStatus.setSelectedIndex(0);

        onSearch();
    }

    @Listen("onCopy = #incList #lbList")
    public void onCopy(Event event) {

        VFileRtfile obj = (VFileRtfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "COPY");
        setParam(arguments);
        createWindow("windowCRUDRapidTest",
                "/Pages/rapidTest/rapidTestCRUD.zul", arguments,
                Window.EMBEDDED);
        rapidTestAll.setVisible(false);

    }

    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(VFileRtfile obj) {
        if (user.getUserId() != null
                && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(),
                    Constants.PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.VT_RETURN))) {
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean isAbleToModify(VFileRtfile obj) {
        if (user.getUserId() != null
                && user.getUserId().equals(obj.getCreatorId())) {
            if (Objects.equals(obj.getStatus(),
                    Constants.PROCESS_STATUS.INITIAL)
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.SENT_DISPATCH))
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.SENT_RT_SDBS1))
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.SENT_RETURN))
                    || (Objects.equals(obj.getStatus(),
                            Constants.PROCESS_STATUS.VT_RETURN))) {
                return true;
            }
            return false;
        }
        return false;
    }
    
    
    public boolean isAbleToWidthdraw(VFileRtfile obj) {
       
            if (Objects.equals(obj.getStatus(),
                    Constants.PROCESS_STATUS.FINISH_RAPID)) {
                return true;
            }
            return false;
        
    }

    public String getStatus(Long status) {
        for (Category c : lstStatusAll) {
            if (status.toString().equals(c.getValue())) {
                return c.getName();
            }
        }
        return WorkflowAPI.getStatusName(status);
    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    public int CheckGiayPhep(Long fileid) {
        RapidTestDAOHE cosDao = new RapidTestDAOHE();
        int check = cosDao.CheckGiayPhep(fileid, getUserId());
        return check;
    }

    public int checkDispathSDBS(Long fileid) {
        RapidTestDAOHE cosDao = new RapidTestDAOHE();
        int check = cosDao.checkDispathSDBS(fileid, getUserId());
        return check;
    }

    @Listen("onViewSDBS = #incList #lbList")
    public void onViewSDBS(Event event) throws FileNotFoundException,
            IOException {
        VFileRtfile obj = (VFileRtfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowViewSDBS",
                "/Pages/module/importreq/flow/viewDispatch.zul", arguments,
                Window.MODAL);
    }

    public Constants getConstants() {
        return constants;
    }

    public void setConstants(Constants constants) {
        this.constants = constants;
    }

    public Long getOldDeadlineWarning(VFileRtfile f) {
        if (f.getFinishDate() != null) {
            return Constants.WARNING.DEADLINE_ON;
        }
        Long value = Constants.WARNING.DEADLINE_ON;
        ProcessDAOHE pDAO = new ProcessDAOHE();
        com.viettel.core.workflow.BO.Process p = pDAO.getLastByUserId(
                f.getFileId(), UserId);
        if (p != null) {
            Date sendDate = p.getSendDate();

            Date exceptedDate = new Date();

            if (sendDate != null
                    && menuTypeStr.equals(Constants.MENU_TYPE.WAITPROCESS_STR)) {
                SimpleDateFormat formatterDateTime = new SimpleDateFormat(
                        "yyyyMMdd");
                if (Long.valueOf(formatterDateTime.format(exceptedDate)) > Long
                        .valueOf(formatterDateTime.format(sendDate))) {
                    value = Constants.WARNING.DEADLINE_MISS;
                }

            }
        }

        return value;
    }
    
    public Long isWithdraw(VFileRtfile f) {
        if (f.getFinishDate() != null) {
            return Constants.WARNING.DEADLINE_ON;
        }
        Long value = Constants.WARNING.DEADLINE_ON;
        ProcessDAOHE pDAO = new ProcessDAOHE();
        com.viettel.core.workflow.BO.Process p = pDAO.getLastByUserId(
                f.getFileId(), UserId);
        if (p != null) {
            Date sendDate = p.getSendDate();

            Date exceptedDate = new Date();

            if (sendDate != null
                    && menuTypeStr.equals(Constants.MENU_TYPE.WAITPROCESS_STR)) {
                SimpleDateFormat formatterDateTime = new SimpleDateFormat(
                        "yyyyMMdd");
                if (Long.valueOf(formatterDateTime.format(exceptedDate)) > Long
                        .valueOf(formatterDateTime.format(sendDate))) {
                    value = Constants.WARNING.DEADLINE_MISS;
                }

            }
        }

        return value;
    }
}
