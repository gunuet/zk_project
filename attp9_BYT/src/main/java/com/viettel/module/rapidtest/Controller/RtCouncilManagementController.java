/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.Controller;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.rapidtest.BO.RtCouncil;
import com.viettel.module.rapidtest.DAO.RtCouncilDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import org.zkoss.zk.ui.util.Clients;

/**
 *
 * @author hungpv32
 */
public class RtCouncilManagementController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 8732384265737189129L;
    @Wire
    Datebox dbDay;
    @Wire
    Window managertCouncilWindow;
    @Wire
    Listbox lbManageHolidays, lbArrange;
    @Wire
    Paging userPagingBottom;
    RtCouncil rtsearch;
    @Wire
    Textbox tbrtcouncilName, tbrtcouncilrole, tbrtCouncilPosition,
            tbrtcouncilCode;

    @Override
    public void doAfterCompose(Component window) {
        try {
            super.doAfterCompose(window);
            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo); // To change body
        // of generated
        // methods,
        // choose Tools
        // | Templates.
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {
            userPagingBottom.setActivePage(0);
            rtsearch = new RtCouncil();
            if (tbrtcouncilName.getValue() != null
                    && !equals("".equals(tbrtcouncilName.getValue()))) {
                rtsearch.setName(tbrtcouncilName.getValue());
            }
            if (tbrtcouncilrole.getValue() != null
                    && !("".equals(tbrtcouncilrole.getValue()))) {
                rtsearch.setRole(tbrtcouncilrole.getValue());
            }
            if (tbrtCouncilPosition.getValue() != null
                    && !"".equals(tbrtCouncilPosition.getValue())) {
                rtsearch.setPosition(tbrtCouncilPosition.getValue());
            }
            if (tbrtcouncilCode.getValue() != null
                    && !"".equals(tbrtcouncilCode.getValue())) {
                rtsearch.setCode(tbrtcouncilCode.getValue());
            }
            // Fill danh sach loai danh muc
            fillDataToList();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    private void fillDataToList() {
        RtCouncilDAO objhe = new RtCouncilDAO();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage()
                * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(rtsearch, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }
        @SuppressWarnings({"unchecked", "rawtypes"})
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbManageHolidays.setModel(lstModel);
    }

    @Listen("onReload=#managertCouncilWindow")
    public void onReload() {
        onSearch();
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException {
        Window window = (Window) Executions.createComponents(
                "/Pages/module/rapidtest/rtcouncil/createUpdateRtCouncil.zul",
                null, null);
        window.doModal();

    }

    @Listen("onEdit=#lbManageHolidays")
    public void onUpdate(Event ev) throws IOException {
        RtCouncil abo = (RtCouncil) lbManageHolidays.getSelectedItem()
                .getValue();
        Map args = new ConcurrentHashMap();
        args.put("id", abo.getId());
        Window window = (Window) Executions.createComponents(
                "/Pages/module/rapidtest/rtcouncil/createUpdateRtCouncil.zul",
                null, args);
        window.doModal();
    }

    @SuppressWarnings("unchecked")
    @Listen("onDelete=#lbManageHolidays")
    public void doDeleteItem(ForwardEvent evt) {
        String message = String.format(Constants.Notification.DELETE_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {
                                        RtCouncil rs = (RtCouncil) lbManageHolidays
                                        .getSelectedItem().getValue();
                                        RtCouncilDAO daohe = new RtCouncilDAO();
                                        daohe.delete(rs.getId());
                                        onSearch();
                                    } catch (Exception e) {
                                        showNotification(
                                                String.format(
                                                        Constants.Notification.DELETE_ERROR,
                                                        Constants.DOCUMENT_TYPE_NAME.FILE),
                                                Constants.Notification.ERROR);
                                        LogUtils.addLogDB(e);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });

    }

    @Override
    public String getConfigLanguageFile() {
        return "language_COSMETIC_vi";
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }
}
