package com.viettel.module.rapidtest.Controller;

import com.viettel.core.base.DAO.BaseComposer;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Textbox;

import com.viettel.module.rapidtest.BO.RtFile;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
//import com.viettel.module.cosmetic.Model.FilesModel;
import com.viettel.utils.Constants;
import com.viettel.ws.Helper_QTest;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class WithdrawController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long fileId;
    @Wire
    private Textbox txtReason;

    private Window parentWindow;

    /**
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("id");
        parentWindow = (Window) arguments.get("parentWindow");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        if (txtReason.getValue() != null && txtReason.getValue().trim().length() == 0) {
            showNotification("Lý do không thể để trống", Constants.Notification.ERROR);
            txtReason.focus();
            return;
        }
        if (txtReason.getValue().trim().length() > 500) {
            showNotification("Lý do không thể vượt quá 500 ký tự", Constants.Notification.ERROR);
            txtReason.focus();
            return;
        }
        RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
        RtFile rtFile = objDAOHE.findByFileId(fileId);
        rtFile.setWithdraw(txtReason.getValue().trim());
        objDAOHE.saveOrUpdate(rtFile);
        Helper_QTest helperQ = new Helper_QTest();
        helperQ.sendMs_31(fileId);

        showNotification("Thu hồi thành công", Constants.Notification.IMP_SUCCESS);
        Events.sendEvent("onVisible", parentWindow, null);

    }

}
