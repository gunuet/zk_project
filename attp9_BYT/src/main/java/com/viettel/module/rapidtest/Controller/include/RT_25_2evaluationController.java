package com.viettel.module.rapidtest.Controller.include;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.Controller.include.CosEvaluationSignPermitController;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.rapidtest.BO.RtCouncil;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.module.rapidtest.DAO.RtCouncilDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.model.SignExportModel;
import com.viettel.newsignature.plugin.SignPdfFile;
import com.viettel.newsignature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;

/**
 *
 * @author ThanhTM
 */
public class RT_25_2evaluationController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private List listRapidTestFileType;
    @Wire
    private List<Media> listMedia;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtCertSerial, txtBase64Hash, txtValidate, txtMessage, txtFinishDate;
    @Wire
    private Listbox fileListbox, fileListboxCA, lbRapidTestFileType, councilList;
    @Wire
    private Vlayout flist;
    
    private Long attachFileId = 0L;
    private boolean showCouncilList = false;
    private ListModelList<RtCouncil> listRtCouncil;
    
    private ListModelList<VFileRtAttachAll> lmlAttach, lmlSigned;
    
    private UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute("userToken");
    private ListModelList<SignExportModel> listSignExport;

    /**
     * thanhtm Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        
        return super.doBeforeCompose(page, parent, compInfo);
    }
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillFileListbox(fileId);
        if (checkCouncilList() == 1) {
            fillRtCouncil();
        }
        lmlSigned = new ListModelList<VFileRtAttachAll>();
        txtValidate.setValue("0");
    }
    
    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }
    
    private void fillRtCouncil() {
        RtCouncilDAO dao = new RtCouncilDAO();
        List<RtCouncil> lstRtCouncil = dao.getRtCouncilActiveList();
        if (lstRtCouncil.size() > 0) {
            listRtCouncil = new ListModelList<RtCouncil>(lstRtCouncil);
            listRtCouncil.setMultiple(true);
            this.councilList.setModel(listRtCouncil);
        }
        
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        obj = objDAO.findMaxByFileIdAndEvalType(fileId, Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION_COUNCIL);
        if (obj != null) {
            Gson gson = new Gson();
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            if (evModel.getEvaluationRtCouncil() != null) {
                councilList.setModel(evModel.getEvaluationRtCouncil());
            }
        }
    }
    
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            //LogUtils.addLog("Tham dinh ho so xet nghiem nhanh:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        if (fileListboxCA != null && lmlSigned.size() == 0) {
            showWarningMessage("Chưa có file quyết định nào được ký!");
            fileListboxCA.focus();
            return false;
        }
        
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            
            AttachDAO base = new AttachDAO();
            for (VFileRtAttachAll file : lmlSigned) {
                base.saveFileAttachPdfAfterSign(file.getAttachName(), file.getObjectId(), file.getAttachCat(), file.getAttachType(), file.getAttachPath());
            }
            
            sendMS();
            txtValidate.setValue("1");
            txtFinishDate.setValue("1");

            
        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }
    
    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
        md.setFileId(fileId);
        md.setPhase(1L);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_RT.SendMs_05);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }
    
    private void fillFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttachAll> lstRapidTestAttach = rDaoHe.findRapidTestAllAttachByCodeList(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES);
        lmlAttach = new ListModelList(lstRapidTestAttach);
        this.fileListbox.setModel(lmlAttach);
    }
    
    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findByAttachId(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
        
    }
    
    public int checkCouncilList() {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        if (!showCouncilList) {
            return Constants.CHECK_VIEW.NOT_VIEW;
        }
        return Constants.CHECK_VIEW.VIEW;
    }
    
    public int checkViewCouncilList() {
        return Constants.CHECK_VIEW.NOT_VIEW;
    }
    
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        try {
            clearWarningMessage();
            
            listSignExport = new ListModelList<SignExportModel>();
            for (VFileRtAttachAll file : lmlAttach) {
                actionSignCA(event, actionPrepareSign(file), file);
            }
            Clients.evalJavaScript("signAndSubmit();");
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }
    
    public String actionPrepareSign(VFileRtAttachAll file) throws Exception {
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(file.getAttachId());
        String fileOut = att.getFullPathFile();
        return fileOut;
    }
    
    public void actionSignCA(Event event, String fileToSign,
            VFileRtAttachAll file) throws Exception {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        String certSerial = x509Cert.getSerialNumber().toString(16);
        
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
        AttachDAO base = new AttachDAO();
        String outputFileFinalName = "_temp_" + base.getFileName(fileToSign);
        String signFileFinalName = "_signed_" + base.getFileName(fileToSign);
        String outPutFileFinal = filePath + outputFileFinalName;
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");// not
            String linkImageStamp = folderPath + separator
                    + caur.get(0).getStamper();
            // fileSignOut = outPutFileFinal;
            Pdf pdfProcess = new Pdf();
            
            String fieldName = tk.getUserFullName();
            
            SignPdfFile pdfSig = pdfProcess.createHash(fileToSign, outPutFileFinal,
                    new Certificate[]{x509Cert}, linkImageStamp, fieldName);
            if (pdfSig == null) {
                showNotification("Ký số không thành công!");
                return;
            }
            txtBase64Hash.setValue(pdfProcess.getBase64Hash());
            txtCertSerial.setValue(certSerial);
            
            SignExportModel sem = new SignExportModel();
            sem.setCertSerial(certSerial);
            sem.setBase64Hash(pdfProcess.getBase64Hash());
            sem.setFilePath(filePath);
            sem.setSignFileFinalName(signFileFinalName);
            sem.setPdfSig(pdfSig);
            sem.setFile(file);
            sem.setOutPutFileFinal(outPutFileFinal);
            listSignExport.add(sem);
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }
    
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) {
        try {
            String data = event.getData().toString();
            for (SignExportModel sem : listSignExport) {
                String filePath = sem.getFilePath();
                FileUtil.mkdirs(filePath);
                String outputFileName = sem.getSignFileFinalName();
                String outputFile = filePath + outputFileName;
                String signature = data;
                SignPdfFile pdfSig;
                pdfSig = sem.getPdfSig();
                
                try {
                    // pdfSig.insertSignature(signature, c);
                    pdfSig.insertSignature(signature, sem.getOutPutFileFinal(),
                            outputFile);
                } catch (IOException ex) {
                    LogUtils.addLogDB(ex);
                    showNotification("Ký số không thành công");
                    return;
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    showNotification("Ký số không thành công");
                    return;
                }
                
                fillSignedFileListbox(sem.getFilePath(),
                        sem.getSignFileFinalName(), sem.getFile());
            }
            
            showNotification("Ký số thành công!", Constants.Notification.INFO);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
    }
    
    private void fillSignedFileListbox(String filePath, String fileName,
            VFileRtAttachAll file) {
        file.setAttachPath(filePath);
        file.setAttachName(fileName);
        file.setAttachCat(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        lmlSigned.add(file);
        this.fileListboxCA.setModel(lmlSigned);
    }
    
    @Listen("onDownloadCAFile = #fileListboxCA")
    public void onDownloadCAFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFile(obj.getAttachPath() + obj.getAttachName(), obj.getAttachName());
    }
    
    @Listen("onDeleteFile = #fileListboxCA")
    public void onDeleteFile(Event event) {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        lmlSigned.remove(obj);
    }
}
