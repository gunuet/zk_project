package com.viettel.module.rapidtest.Controller.include;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.rapidtest.BO.RtTargetTesting;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAO.RtTargetTestingDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;

/**
 *
 * @author ThanhTM
 */
public class RT_3_4_EvaluationController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Listbox lbAppraisal, lbListOfTargetTesting, finalFileListbox, signedFileListbox, fileListbox;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbEvalType;
    private Long fileId;
    @Wire
    private Textbox txtValidate;
    private Textbox txtNote = (Textbox) Path.getComponent("/windowProcessing/txtNote");

    @Wire
    private Groupbox gbRapidCRUD1, gbRapidCRUD2, gbRapidCRUD3;
    @Wire
    private Tabbox tb;

   // private ListModelList<RtAssay> categoryModel;
   // private ListModelList<RtAssay> selectedCategoryModel;
    private ListModelList<RtTargetTesting> rtTargetTestingMode;

    private RapidTestDAOHE vFileRtfileDAOHE;
    private VFileRtfile vFileRtfile;

    /**
     * thanhtm Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        vFileRtfileDAOHE = new RapidTestDAOHE();
        vFileRtfile = vFileRtfileDAOHE.findViewByFileId(fileId);
        //categoryModel = new ListModelList<RtAssay>();
        //selectedCategoryModel = new ListModelList<RtAssay>();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
       // fillSelectedBasicAssay();
       // fillBasicAssay();
        fillFileListbox(fileId);
        fillFinalFileListbox(fileId);
        Long documentTypeCode = vFileRtfile.getDocumentTypeCode();
        if (documentTypeCode != null) {
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(true);
                gbRapidCRUD2.setVisible(false);
                gbRapidCRUD3.setVisible(false);
                RtTargetTestingDAO objTargetTesting = new RtTargetTestingDAO();
                List lsTargetTesting = objTargetTesting.getByFileId(fileId);
                rtTargetTestingMode = new ListModelList(lsTargetTesting);
                rtTargetTestingMode.setMultiple(true);
                lbListOfTargetTesting.setModel(rtTargetTestingMode);
                lbListOfTargetTesting.renderAll();

            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(false);
                gbRapidCRUD2.setVisible(true);
                gbRapidCRUD3.setVisible(false);
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(false);
                gbRapidCRUD2.setVisible(false);
                gbRapidCRUD3.setVisible(true);
            } else {
                tb.setVisible(false);
            }

        }
        txtValidate.setValue("0");
    }

    void fillHoSo() {
        Gson gson = new Gson();
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListFileId(fileId);
        if (lstEvaluationRecord.size() > 0) {
            obj = lstEvaluationRecord.get(0);
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            List<Listitem> listItem = lbAppraisal.getItems();
            for (Listitem listItem1 : listItem) {
                if ((String) listItem1.getLabel() != evModel.getResultEvaluationStr()) {
                } else {
                    lbAppraisal.setSelectedItem(listItem1);
                    break;
                }
            }
            txtNote.setValue(obj.getMainContent());
        }
    }

/*   void fillSelectedBasicAssay() {
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        obj = objDAO.findMaxByFileIdAndEvalType(fileId, Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.BASIC_ASSAY);
        selectedCategoryModel = new ListModelList<RtAssay>();
        if (obj != null) {
            Gson gson = new Gson();
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            if (selectedCategoryModel != null) {
                selectedCategoryModel = evModel.getEvaluationBasicAssay();
            }

            if (selectedCategoryModel == null) {
                selectedCategoryModel = new ListModelList<RtAssay>();
            }

        }
        if (selectedCategoryModel != null) {
            selectedCategoryModel.setMultiple(true);
        }
        if (lbSelectedList != null && selectedCategoryModel != null) {
            lbSelectedList.setModel(selectedCategoryModel);
        }
    }*/

    /*void fillBasicAssay() {
        RtAssayDAO objCategory = new RtAssayDAO();
        List lsCategory = objCategory.getRtAssayActiveList();
        categoryModel = new ListModelList(lsCategory);

        if (selectedCategoryModel != null) {
            for (RtAssay objSelected : selectedCategoryModel) {
                for (RtAssay obj : categoryModel) {
                    if (obj.getId() == objSelected.getId()) {
                        categoryModel.remove(obj);
                        break;
                    }
                }
            }
        }
        categoryModel.setMultiple(true);
        lbList.setModel(categoryModel);
    } */

    private void fillFileListbox(Long vFileRtfileId) {
        try {
            RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
            List<VFileRtAttach> lstRapidTestAttach = rDaoHe.findRapidTestAttach(vFileRtfileId);
            this.fileListbox.setModel(new ListModelArray(lstRapidTestAttach));
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void fillFinalFileListbox(Long fileId) {
        try {
            AttachDAOHE rDaoHe = new AttachDAOHE();
            List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                    Constants.OBJECT_TYPE.RAPID_TEST_HO_SO_GOC);
            this.finalFileListbox.setModel(new ListModelArray(lstAttach));

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            //LogUtils.addLog("Tham dinh ho so xet nghiem nhanh:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            Gson gson = new Gson();
            String reson = txtNote.getValue().trim();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setContent(reson);
            evaluModel.setResultEvaluationStr((String) lbAppraisal.getSelectedItem().getLabel());
           // evaluModel.setEvaluationBasicAssay(selectedCategoryModel);
            String jsonEvaluation = gson.toJson(evaluModel);

            EvaluationRecord obj = new EvaluationRecord();
            obj.setMainContent(reson);
            if (Long.valueOf((String) lbAppraisal.getSelectedItem().getValue()) == 1L) {
                obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHDAT);
            } else {
                obj.setStatus(Constants.FILE_STATUS_CODE.STATUS_CV_THAMDINHYCSDBS);
            }
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setEvalType(Long.valueOf(lbEvalType.getValue()));
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);
            txtValidate.setValue("1");


        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

   /* @Listen("onClick = #chooseBtn")
    public void chooseItem() {
        Set<RtAssay> set = categoryModel.getSelection();
        selectedCategoryModel.addAll(set);
        categoryModel.removeAll(set);
    }

    @Listen("onClick = #removeBtn")
    public void unchooseItem() {
        Set<RtAssay> set = selectedCategoryModel.getSelection();
        categoryModel.addAll(set);
        selectedCategoryModel.removeAll(set);
    }

    @Listen("onClick = #chooseAllBtn")
    public void chooseAllItem() {
        selectedCategoryModel.addAll(categoryModel);
        categoryModel.clear();
    }

    @Listen("onClick = #removeAllBtn")
    public void unchooseAll() {
        categoryModel.addAll(selectedCategoryModel);
        selectedCategoryModel.clear();
    } */

    public VFileRtfile getvFileRtfile() {
        return vFileRtfile;
    }

    public void setvFileRtfile(VFileRtfile vFileRtfile) {
        this.vFileRtfile = vFileRtfile;
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }
}
