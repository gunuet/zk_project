package com.viettel.module.rapidtest.Controller.include;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.module.rapidtest.DAO.RtAssayDAO;
import com.viettel.module.rapidtest.DAO.VFileRtflieDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.model.ExportModel;
import com.viettel.module.rapidtest.model.RtManagerSignModel;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.ws.SendEmailSms;

/**
 *
 * @author ThanhTM
 */
public class RT_5_1_EvaluationAcceptController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbRtSignType;
    private Long fileId;
    @Wire
    private Textbox txtValidate, txtMessage;
    @Wire
    private Listbox lbListResult, lbList, lbSelectedList;

    private ListModelList<RtAssay> categoryModel;
    private ListModelList<RtAssay> selectedCategoryModel;

    @Wire
    private Listbox finalFileListbox;
    @Wire
    Textbox txtCertSERIAL, txtBase64HASH;
    private String fileSignOut = "";
    private Permit permit;
    private Long rtSignType;
    List<VFileRtAttachAll> lstRapidTestAttach;
    private List listBook;
    private String bCode;
    private Long docType;

    /**
     * thanhtm Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        categoryModel = new ListModelList<RtAssay>();
        selectedCategoryModel = new ListModelList<RtAssay>();
        bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.GIAY_YEU_CAU_THANH_TOAN;
        docType = (Long) arguments.get("docType");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        rtSignType = Long.valueOf(lbRtSignType.getValue());
        fillHoSo();
        fillSelectedBasicAssay();
        fillBasicAssay();
        fillFinalFileListbox(fileId);
        txtValidate.setValue("0");
    }

    void fillHoSo() {
        Gson gson = new Gson();
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId,
                Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
        if (lstEvaluationRecord.size() > 0) {
            for (int i = 0; i < lstEvaluationRecord.size(); i++) {
                obj = lstEvaluationRecord.get(i);
                EvaluationModel evModel = gson.fromJson(obj.getFormContent(),
                        EvaluationModel.class);

                Listitem li = new Listitem();
                Listcell lcIndex = new Listcell(Integer.toString(i + 1));
                li.appendChild(lcIndex);
                Listcell lcResultEvaluation = new Listcell(
                        evModel.getResultEvaluationStr());
                li.appendChild(lcResultEvaluation);
                Listcell lcResonRequest = new Listcell(evModel.getContent());
                li.appendChild(lcResonRequest);
                lbListResult.appendChild(li);
                Listcell lcUserName = new Listcell(evModel.getUserName());
                li.appendChild(lcUserName);
                lbListResult.appendChild(li);
                Listcell lcDate = new Listcell(lstEvaluationRecord.get(i)
                        .getCreateDate().toString());
                li.appendChild(lcDate);
                lbListResult.appendChild(li);
            }
        }
    }

    void fillSelectedBasicAssay() {
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        obj = objDAO.findMaxByFileIdAndEvalType(fileId,
                Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
        selectedCategoryModel = new ListModelList<RtAssay>();
        if (obj != null) {
            Gson gson = new Gson();
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(),
                    EvaluationModel.class);
//            if (selectedCategoryModel != null) {
                selectedCategoryModel = evModel.getEvaluationBasicAssay();
//            }

        }
        selectedCategoryModel.setMultiple(true);
        lbSelectedList.setModel(selectedCategoryModel);
    }

    void fillBasicAssay() {
        RtAssayDAO objCategory = new RtAssayDAO();
        List lsCategory = objCategory.getRtAssayActiveList();
        categoryModel = new ListModelList(lsCategory);
        for (RtAssay objSelected : selectedCategoryModel) {
            for (RtAssay obj : categoryModel) {
                if (obj.getId() == objSelected.getId()) {
                    categoryModel.remove(obj);
                    break;
                }
            }
        }
        categoryModel.setMultiple(true);
        lbList.setModel(categoryModel);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            // LogUtils.addLog("Tham dinh ho so xet nghiem nhanh:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (lstRapidTestAttach.size() == 0) {
            showWarningMessage("Chưa ký văn bản");
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            Gson gson = new Gson();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setEvaluationBasicAssay(selectedCategoryModel);
            String jsonEvaluation = gson.toJson(evaluModel);

            EvaluationRecord obj = new EvaluationRecord();
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setEvalType(Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);

            sendMail();

            sendMS();

            txtValidate.setValue("1");

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    @Listen("onClick = #chooseBtn")
    public void chooseItem() {
        Set<RtAssay> set = categoryModel.getSelection();
        selectedCategoryModel.addAll(set);
        categoryModel.removeAll(set);
    }

    @Listen("onClick = #removeBtn")
    public void unchooseItem() {
        Set<RtAssay> set = selectedCategoryModel.getSelection();
        categoryModel.addAll(set);
        selectedCategoryModel.removeAll(set);
    }

    @Listen("onClick = #chooseAllBtn")
    public void chooseAllItem() {
        selectedCategoryModel.addAll(categoryModel);
        categoryModel.clear();
    }

    @Listen("onClick = #removeAllBtn")
    public void unchooseAll() {
        categoryModel.addAll(selectedCategoryModel);
        selectedCategoryModel.clear();
    }

    /**
     *
     * @param event
     */
    @Listen("onSign = #businessWindow")
    public void onSign(Event event) throws Exception {
        String signature = event.getData().toString();

        /* set outputPath of PDF file */
        ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
        String filePath = resourceBundle.getString("signPdf");
        // if not existing then to create new forder
        if (!(new File(filePath)).exists()) {
            FileUtil.mkdirs(filePath);
        }

        // set name file
        String outputFileName = "_signed_ThongBaoNopPhiThamDinh"
                + (new Date()).getTime() + ".pdf";
        fileSignOut = filePath + outputFileName;

        Session session = Sessions.getCurrent();
        SignPdfFile signPdfFile = (SignPdfFile) session
                .getAttribute("PDFSignature");

        signPdfFile.insertSignature(signature, fileSignOut);
        LogUtils.addLog("Signed file: " + fileSignOut);

        try {
            onSignPermit();
            fileSignOut = "";
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     *
     * @throws Exception
     */
    public void onSignPermit() throws Exception {
        ExportFileDAO exportDao = new ExportFileDAO();
        RtManagerSignModel model = new RtManagerSignModel(fileId,
                Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE, rtSignType);
        model.setSendNo(permit.getReceiveNo());
        model.setSignedDate(permit.getSignDate());
        model.setCosmeticPermitId(fileId);
        exportDao.updateAttachSignFile(model, fileSignOut);

        showNotification("Ký số thành công!", Constants.Notification.INFO);
        fillFinalFileListbox(fileId);
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) throws Exception {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        // save content
        // saveContent();
        // get fdf file path
        String pdfPath = actionPrepareSign();
        // insert data into pdf
        actionSignCA(event, pdfPath);
    }

    public String actionPrepareSign() {
        String fileToSign = "";
        try {
            fileToSign = onApproveFileSign();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    /**
     * Phe duyet ho so, kem CKS
     *
     * @return @throws Exception
     */
    public String onApproveFileSign() throws Exception {
        clearWarningMessage();
        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(fileId);

        if (lstPermit.size() > 0) {// Da co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
        }
        permitDAO.saveOrUpdate(permit);
        Long bookNumber = putInBook(permit);// Vao so
        String receiveNo = getPermitNo(bookNumber);
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        ListModelList<RtAssay> list = new ListModelList<RtAssay>();
        EvaluationRecordDAO dao = new EvaluationRecordDAO();
        EvaluationRecord evaluationRecord = dao.findMaxByFileIdAndEvalType(
                fileId, Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
        if (evaluationRecord != null) {
            String formContent = evaluationRecord.getFormContent();
            if (formContent != null) {
                Gson gson = new Gson();
                EvaluationModel evModel = gson.fromJson(
                        evaluationRecord.getFormContent(),
                        EvaluationModel.class);
                list = evModel.getEvaluationBasicAssay();
            }
        }

        ExportFileDAO expDAO = new ExportFileDAO();
        VFileRtflieDAO rtDao = new VFileRtflieDAO();
        VFileRtfile rt = rtDao.findById(fileId);

        ExportModel model = new ExportModel();
        model.setSendNo(permit.getReceiveNo());
        model.setBusinessName(rt.getBusinessName());
        model.setSignDate(permit.getSignDate());
        model.setLeaderSinged(getUserFullName());
        model.setRapidTestName(rt.getRapidTestName());
        model.setRapidTestCode(rt.getRapidTestCode());
        model.setNswFileCode(rt.getNswFileCode());
        model.setList(list);

        return expDAO.exportFileHSDKXNN1(model, false);
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Permit Permit) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(Permit.getPermitId(),
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    private String getPermitNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yy"); // Just the year,
        // with 2 digits
        String year = sdf.format(Calendar.getInstance().getTime());
        permitNo += "/" + year;
        permitNo += "/ATTP-KN";
        return permitNo;
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        SignPdfFile pdfSig = new SignPdfFile();// pdfSig = new SignPdfFile();
        String base64Hash;
        String certSerial = x509Cert.getSerialNumber().toString(16);

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");
        FileUtil.mkdirs(filePath);
        String outputFileFinalName = "_" + (new Date()).getTime() + ".pdf";
        String outPutFileFinal = filePath + outputFileFinalName;
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String linkImageSign = folderPath + separator
                    + caur.get(0).getSignature();
            String linkImageStamp = folderPath + separator
                    + caur.get(0).getStamper();
            Pdf pdfProcess = new Pdf();
            // chen chu ki
            try {
                pdfProcess.insertImageAll(fileToSign, outPutFileFinal,
                        linkImageSign, linkImageStamp, null);
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
            // chen CKS
            if (pdfProcess.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }

            base64Hash = pdfSig.createHash(outPutFileFinal,
                    new Certificate[]{x509Cert});

            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64HASH.setValue(base64Hash);
            txtCertSERIAL.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitDepartmentLeadershipFile();");
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    private void fillFinalFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        lstRapidTestAttach = rDaoHe.findRapidTestAllLastAttachByCode(fileId,
                rtSignType);
        this.finalFileListbox.setModel(new ListModelArray(lstRapidTestAttach));
    }

    private Permit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();

        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);

        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(att);
        fillFinalFileListbox(fileId);
    }

    private void sendMS() {
        Gson gson = new Gson();
        MessageModel md = new MessageModel();
        md.setCode(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
        md.setFileId(fileId);
        md.setFunctionName(Constants.FUNCTION_MESSAGE_RT.SendMs_30);
        md.setPhase(0l);
        md.setFeeUpdate(true);
        String jsonMd = gson.toJson(md);
        txtMessage.setValue(jsonMd);
    }

    private void sendMail() {
        try {
            VFileRtflieDAO rtDao = new VFileRtflieDAO();
            VFileRtfile rt = rtDao.findById(fileId);

            UserDAOHE userDAOHE = new UserDAOHE();
            Users user = userDAOHE.getUserById(rt.getCreatorId());

            if (user.getEmail() != null) {

                ListModelList<RtAssay> list = new ListModelList<RtAssay>();
                EvaluationRecordDAO dao = new EvaluationRecordDAO();
                EvaluationRecord evaluationRecord = dao
                        .findMaxByFileIdAndEvalType(
                                fileId,
                                Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
                if (evaluationRecord != null) {
                    String formContent = evaluationRecord.getFormContent();
                    if (formContent != null) {
                        Gson gson = new Gson();
                        EvaluationModel evModel = gson.fromJson(
                                evaluationRecord.getFormContent(),
                                EvaluationModel.class);
                        list = evModel.getEvaluationBasicAssay();
                    }
                }

                SendEmailSms ses = new SendEmailSms();
                String msge = "Kính gửi: "
                        + rt.getBusinessName()
                        + "<br//>    Cục An toàn thực phẩm đã nhận được hồ sơ đề nghị đăng ký lưu hành bộ xét nghiệm nhanh:"
                        + "<br//>Tên Bộ xét nghiệm nhanh: "
                        + rt.getRapidTestName()
                        + "<br//>Ký mã hiệu: "
                        + rt.getRapidTestCode()
                        + "<br//>Của công ty: "
                        + rt.getBusinessName()
                        + "<br//>Mã hồ sơ: "
                        + rt.getNswFileCode()
                        + "<br//>Theo quy định tại Thông tư số 11/2014/TT-BYT ngày 18/03/2014 của Bộ Y tế quy định"
                        + "<br//>quản lý bộ xét nghiệm nhanh thực phẩm, Cục An toàn thực phẩm có ý kiến như sau:"
                        + "<br//>Công ty phải tiến hành khảo nghiệm bộ xét nghiệm nhanh, việc khảo nghiệm phải được thực hiện độc"
                        + "<br//>lập tại 3 cơ sở kiểm nghiệm. Quý Công ty có thể lựa chọn đơn vị khảo nghiệm trong danh sách kèm theo."
                        + "<br//>Chi phí khảo nghiệm Quý công ty chi trả theo thỏa thuận với đơn vị khảo nghiệm được lựa chọn."
                        + "<br//>Sau khi có kết quả khảo nghiệm đạt yêu cầu, Quý Công thực hiện nộp Phí thẩm định xét duyệt"
                        + "<br//>hồ sơ đăng ký lưu hành đối với bộ xét nghiệm nhanh nêu trên, chi tiết như sau:"
                        + "<br//>1.	Số phí phải nộp: 3.000.000đ (ba triệu đồng chẵn)"
                        + "<br//>2.	Tên đơn vị thụ hưởng:    Cục An toàn thực phẩm"
                        + "<br//>3.	Tài khoản thụ hưởng: 05.115.133.01"
                        + "<br//>Tại Ngân hàng: .Công  thương việt nam Viettin bank"
                        + "<br//>Trong phần nội dung chuyển tiền đề nghị ghi rõ: Nộp phí thẩm định xét duyệt hồ sơ đăng ký lưu hành bộ xét nghiệm nhanh mã số............. (ghi đầy đủ mã hồ sơ)"
                        + "<br//>    Sau khi hoàn tất đóng phí đề nghị công ty gửi chứng từ chuyển tiền (file điện tử) lên hệ thống xử lý ............... và nộp 02 mẫu Bộ xét nghiệm nhanh về"
                        + "<br//>Cục An toàn thực phẩm (135 Núi Trúc, Ba Đình, Hà Nội) để Hồ sơ được thẩm xét"
                        + "<br//>    Cục An toàn thực phẩm thông báo để Công ty biết và thực hiện./."
                        + "<br//>" + "<br//>Trân trọng. Cục An toàn thực phẩm."
                        + "<br//>" + "<br//>" + "<br//>"
                        + " DANH SÁCH CÁC CƠ SỞ KIỂM NGHIỆM";

                for (int i = 0; i < list.getSize(); i++) {
                    msge = msge + "<br//>" + list.get(i).getName() + "     "
                            + list.get(i).getAddress();
                }

                ses.sendEmailManual(
                        "Nop phi tham dinh HS DK test nhanh ", user.getEmail(),
                        msge);
            }
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        } finally {
        }
    }
}
