package com.viettel.module.rapidtest.Controller.include;

import com.google.gson.Gson;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.module.rapidtest.DAO.RtAssayDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

/**
 *
 * @author ThanhTM
 */
public class evaluationAcceptController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Listbox lbListResult, lbList, lbSelectedList;
    
    private ListModelList<RtAssay> categoryModel;
    private ListModelList<RtAssay> selectedCategoryModel;
    /**
     * thanhtm Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        categoryModel = new ListModelList<RtAssay>();
        selectedCategoryModel = new ListModelList<RtAssay>();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillHoSo();
        fillSelectedBasicAssay();
        fillBasicAssay();
        txtValidate.setValue("0");
    }

    void fillHoSo(){
        Gson gson = new Gson();
        List<EvaluationRecord> lstEvaluationRecord;
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        lstEvaluationRecord = objDAO.getListByFileIdAndEvalType(fileId,Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
        if (lstEvaluationRecord.size() > 0) {
        	for (int i = 0; i< lstEvaluationRecord.size(); i++) {
	            obj = lstEvaluationRecord.get(i);
	            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
	            
	            Listitem li = new Listitem();
	            Listcell lcIndex = new Listcell(Integer.toString(i+1));
	            li.appendChild(lcIndex);
	            Listcell lcResultEvaluation = new Listcell(evModel.getResultEvaluationStr());
	            li.appendChild(lcResultEvaluation);
	            Listcell lcResonRequest = new Listcell(evModel.getContent());
	            li.appendChild(lcResonRequest);
	            lbListResult.appendChild(li);
	            Listcell lcUserName = new Listcell(evModel.getUserName());
	            li.appendChild(lcUserName);
	            lbListResult.appendChild(li);
	            Listcell lcDate= new Listcell(lstEvaluationRecord.get(i).getCreateDate().toString());
	            li.appendChild(lcDate);
	            lbListResult.appendChild(li);
        	}
        }
    }
    
    void fillSelectedBasicAssay(){
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        obj = objDAO.findMaxByFileIdAndEvalType(fileId, Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
        selectedCategoryModel = new ListModelList<RtAssay>();
        if (obj != null) {
            Gson gson = new Gson();
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
//            if (selectedCategoryModel!=null) {            	
                selectedCategoryModel = evModel.getEvaluationBasicAssay();
//            }

        }
        selectedCategoryModel.setMultiple(true);
        lbSelectedList.setModel(selectedCategoryModel);
    }

    void fillBasicAssay(){
    	RtAssayDAO objCategory = new RtAssayDAO();
    	List lsCategory = objCategory.getRtAssayActiveList();
    	categoryModel = new ListModelList(lsCategory);
		for (RtAssay objSelected : selectedCategoryModel) {
	    	for (RtAssay obj : categoryModel) {
    	    	if (obj.getId() == objSelected.getId()) {
    	    		categoryModel.remove(obj);
    	    		break;
    	    	}
        	}
    	}
    	categoryModel.setMultiple(true);
    	lbList.setModel(categoryModel);
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
                //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            Gson gson = new Gson();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setEvaluationBasicAssay(selectedCategoryModel);
            String jsonEvaluation = gson.toJson(evaluModel);
            
            EvaluationRecord obj = new EvaluationRecord();
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setEvalType(Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);
            
            txtValidate.setValue("1");
        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    @Listen("onClick = #chooseBtn")
    public void chooseItem() {
    	Set<RtAssay> set = categoryModel.getSelection();
        selectedCategoryModel.addAll(set);
        categoryModel.removeAll(set);
    }
 
    @Listen("onClick = #removeBtn")
    public void unchooseItem() {
    	Set<RtAssay> set = selectedCategoryModel.getSelection();
        categoryModel.addAll(set);
        selectedCategoryModel.removeAll(set);
    }
 
    @Listen("onClick = #chooseAllBtn")
    public void chooseAllItem() {
        selectedCategoryModel.addAll(categoryModel);
        categoryModel.clear();
    }
 
    @Listen("onClick = #removeAllBtn")
    public void unchooseAll() {
    	categoryModel.addAll(selectedCategoryModel);
    	selectedCategoryModel.clear();
    }

}
