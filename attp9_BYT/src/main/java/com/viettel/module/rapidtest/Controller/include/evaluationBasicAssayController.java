package com.viettel.module.rapidtest.Controller.include;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.module.rapidtest.DAO.RtAssayDAO;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;

/**
 *
 * @author ThanhTM
 */
public class evaluationBasicAssayController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private Listbox lbList, lbSelectedList;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate;

    private ListModelList<RtAssay> categoryModel;
    private ListModelList<RtAssay> selectedCategoryModel;
    /**
     * thanhtm Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillData();
        txtValidate.setValue("0");
    }

    private void fillData() {
    	RtAssayDAO objCategory = new RtAssayDAO();
    	List lsCategory = objCategory.getRtAssayActiveList();
    	lsCategory.remove(0);
    	categoryModel = new ListModelList(lsCategory);
    	categoryModel.setMultiple(true);
    	lbList.setModel(categoryModel);
    	//lbList.renderAll();
    	lbSelectedList.setModel(selectedCategoryModel = new ListModelList<RtAssay>());
    	selectedCategoryModel.setMultiple(true);
    }
    
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
    	if (selectedCategoryModel.size()==0) {
    		showWarningMessage("Danh sách cơ sở khảo nghiệm chỉ định chưa được chọn!");
            lbSelectedList.focus();
            return false;
    	}
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
                //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }
            Gson gson = new Gson();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            evaluModel.setEvaluationBasicAssay(selectedCategoryModel);
            String jsonEvaluation = gson.toJson(evaluModel);
            
            EvaluationRecord obj = new EvaluationRecord();
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            obj.setEvalType(Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.BASIC_ASSAY);
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);
            txtValidate.setValue("1");
            

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    @Listen("onClick = #chooseBtn")
    public void chooseItem() {
    	Set<RtAssay> set = categoryModel.getSelection();
        selectedCategoryModel.addAll(set);
        categoryModel.removeAll(set);
    }
 
    @Listen("onClick = #removeBtn")
    public void unchooseItem() {
    	Set<RtAssay> set = selectedCategoryModel.getSelection();
        categoryModel.addAll(set);
        selectedCategoryModel.removeAll(set);
    }
 
    @Listen("onClick = #chooseAllBtn")
    public void chooseAllItem() {
        selectedCategoryModel.addAll(categoryModel);
        categoryModel.clear();
    }
 
    @Listen("onClick = #removeAllBtn")
    public void unchooseAll() {
    	categoryModel.addAll(selectedCategoryModel);
    	selectedCategoryModel.clear();
    }
    
}
