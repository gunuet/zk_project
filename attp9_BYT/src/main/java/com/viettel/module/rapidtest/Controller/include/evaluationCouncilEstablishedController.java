package com.viettel.module.rapidtest.Controller.include;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Vlayout;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.rapidtest.BO.RtCouncil;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.module.rapidtest.DAO.RtCouncilDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.module.rapidtest.model.include.EvaluationAttachFileModel;
import com.viettel.utils.Constants;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author ThanhTM
 */
public class evaluationCouncilEstablishedController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private List listRapidTestFileType;
    @Wire
    private List<Media> listMedia;
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbPhase;
    private Long fileId;
    private String categoryCode;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate, txtResult;
    private Textbox txtNote = (Textbox) Path
            .getComponent("/windowProcessing/txtNote");
    @Wire
    private Listbox fileListbox, lbRapidTestFileType, fileReturnListbox,
            councilList, lbPosition;
    @Wire
    private Vlayout flist;
    @Wire
    private Datebox dbMeetingDate;
    @Wire
    private Timebox tbMeetingTime;
    @Wire
    private Groupbox gbAttachFile;
    @Wire
    private Button btnMake;
    private EvaluationAttachFileModel attachFileModel;

    private Long attachFileId = 0L;

    private ListModelList<EvaluationAttachFileModel> listFileModel;
    private ListModelList<VFileRtAttachAll> listFileReturnModel;
    private ListModelList<VFileRtAttachAll> listFileReturnDeleteModel;
    private ListModelList<RtCouncil> listRtCouncil;

    private String filePath = "";
    private String fileName = "";
    private boolean showCouncilList = false;
    private boolean showViewResult = false;
    private PermitDAO permitDAO = new PermitDAO();
    private Permit permit;
    private List listBook;
    private Long docType;
    private String bCode = "";
    private List listSignPos;

    /**
     * thanhtm Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        docType = Constants.OBJECT_TYPE.RAPID_TEST_PERMIT;
        categoryCode = arguments.get("code").toString();
        switch (categoryCode) {
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_VALUES;
                showCouncilList = true;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_VALUES;
                showViewResult = true;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES;
                break;

            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_VALUES;
                showCouncilList = true;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES;
                break;

            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES;
                break;
        }
        listMedia = new ArrayList();
        attachFileModel = new EvaluationAttachFileModel();
        listFileModel = new ListModelList<EvaluationAttachFileModel>();
        listFileReturnModel = new ListModelList<VFileRtAttachAll>();
        listFileReturnDeleteModel = new ListModelList<VFileRtAttachAll>();
        listRtCouncil = new ListModelList<RtCouncil>();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillReturnFileListbox(fileId);
        if (checkCouncilList() == 1) {
            fillRtCouncil();
        }
        txtValidate.setValue("0");
    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        lbRapidTestFileType.setSelectedIndex(0);
        visibleControl();
        return selectedItem;
    }

    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        categoryDAOHE = new CategoryDAOHE();
        listRapidTestFileType = categoryDAOHE.findAllCategoryByCodeString(
                Constants.CATEGORY_TYPE.RAPID_TEST_FILE_TYPE, categoryCode);
        lstModel = new ListModelList(listRapidTestFileType);
        return lstModel;

    }

    public ListModelList getPositionModel(int type) {
        UserDAOHE userDAOHE;
        ListModelList lstModel;
        userDAOHE = new UserDAOHE();
        listSignPos = userDAOHE.getUserByDeptPosID(
                Constants.DEPARTMENT.CUC_ATTP, Constants.POSITION.NGUOI_KY);
        lstModel = new ListModelList(listSignPos);
        return lstModel;

    }

    public int getSelectedIndexInPositionModel(int type) {
        int selectedItem = 0;
        lbPosition.setSelectedIndex(0);
        return selectedItem;
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            // System.out.println("Tham dinh ho so xet nghiem nhanh:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onSelect=#lbRapidTestFileType")
    public void onSelectFileType() {
        try {
            visibleControl();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void visibleControl() {
        if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG) {
            gbAttachFile.setVisible(true);
            btnMake.setVisible(false);
            return;
        }
        if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG) {
            gbAttachFile.setVisible(true);
            btnMake.setVisible(false);
            return;
        }
        gbAttachFile.setVisible(false);
        btnMake.setVisible(true);
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        if (listFileModel.size() == 0) {
            showWarningMessage("Chưa có file nào được chọn!");
            fileListbox.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            // Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedData()) {
                return;
            }

            Gson gson = new Gson();
            EvaluationModel evaluModel = new EvaluationModel();
            evaluModel.setUserId(getUserId());
            evaluModel.setUserName(getUserName());
            evaluModel.setDeptId(getDeptId());
            evaluModel.setDeptName(getDeptName());
            ListModelList<RtCouncil> lst = new ListModelList<RtCouncil>();
            for (RtCouncil council : listRtCouncil.getSelection()) {
                lst.add(council);
            }
            evaluModel.setEvaluationRtCouncil(lst);
            if (checkCouncilList() == 1) {
                evaluModel.setMeetingDate(dbMeetingDate.getValue());
            }
            String jsonEvaluation = gson.toJson(evaluModel);

            EvaluationRecord obj = new EvaluationRecord();
            obj.setFormContent(jsonEvaluation);
            obj.setCreatorId(getUserId());
            obj.setCreatorName(getUserFullName());
            obj.setFileId(fileId);
            obj.setCreateDate(new Date());
            obj.setIsActive(Constants.Status.ACTIVE);
            // obj.setEvalType(Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION_COUNCIL);
            obj.setEvalType(Long.parseLong(lbPhase.getValue()));
            EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
            objDAO.saveOrUpdate(obj);

            AttachDAO base = new AttachDAO();
            for (EvaluationAttachFileModel file : listFileModel) {
                if ("".equals(file.getFilePath())) {
                    base.saveFileAttach(file.getFileContent(), file.getObjId(),
                            file.getObjType(), file.getFileType());
                } else {
                    base.saveFileAttachPdfAfterSign("_" + file.getFileName(),
                            file.getObjId(), file.getObjType(),
                            file.getFileType(), "");
                }
            }
            RapidTestAttachDAOHE rDAOHE = new RapidTestAttachDAOHE();
            for (VFileRtAttachAll file : listFileReturnDeleteModel) {
                rDAOHE.delete(file.getAttachId());
            }
            txtValidate.setValue("1");

        } catch (WrongValueException e) {
            LogUtils.addLogDB(e);
            showNotification(String.format(Constants.Notification.SAVE_ERROR,
                    Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        EvaluationAttachFileModel obj = (EvaluationAttachFileModel) event
                .getData();
        listFileModel.remove(obj);
    }

    private void fillReturnFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttachAll> lstRapidTestAttach = rDaoHe
                .findRapidTestAllLastAttachByCodeList(fileId, categoryCode);
        if (lstRapidTestAttach.size() > 0) {
            listFileReturnModel = new ListModelList<VFileRtAttachAll>(
                    lstRapidTestAttach);
            this.fileReturnListbox.setModel(listFileReturnModel);
        }

        // onSelect();//An hien form
    }

    private void fillRtCouncil() {
        RtCouncilDAO dao = new RtCouncilDAO();
        List<RtCouncil> lstRtCouncil = dao.getRtCouncilActiveList();
        if (lstRtCouncil.size() > 0) {
            listRtCouncil = new ListModelList<RtCouncil>(lstRtCouncil);
            listRtCouncil.setMultiple(true);
            this.councilList.setModel(listRtCouncil);
        }

        // onSelect();//An hien form
    }

    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();

        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                	String sExt = ResourceBundleUtil.getString("extend_file", "config");
	                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("z-valign-left");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listMedia.remove(media);
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    public static boolean validFileType(String fileType)
            throws UnsupportedEncodingException {
        boolean result = false;
        String sExt = ResourceBundleUtil.getString("extend_file", "config");
        String[] fileTypes = sExt.split(",");
        for (String s : fileTypes) {
            if (s.toLowerCase().equals(fileType.toLowerCase())) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Listen("onClick=#btnCreate")
    public void onCreateRapidTestFileType() throws IOException, Exception {

        if (listMedia.isEmpty()) {
            showNotification("Chọn tệp tải lên trước khi thêm mới!",
                    Constants.Notification.WARNING);

            return;
        }
        for (EvaluationAttachFileModel file : listFileModel) {
            if (file.getFileType() == Long.valueOf((String) lbRapidTestFileType
                    .getSelectedItem().getValue())) {
                showNotification(
                        "Loại file bạn chọn đã tồn tại trong danh sách tải lên!",
                        Constants.Notification.WARNING);

                return;
            }
        }
        for (VFileRtAttachAll file : listFileReturnModel) {
            if (file.getAttachType() == Long
                    .valueOf((String) lbRapidTestFileType.getSelectedItem()
                            .getValue())) {
                showNotification(
                        "Loại file bạn chọn đã tồn tại trong danh sách file trả về!",
                        Constants.Notification.WARNING);

                return;
            }
        }
        /*
         * if ((listMedia.size()>1) || (listFileModel.size()>0)) {
         * showNotification("Chỉ được phép tải lên 01 file!",
         * Constants.Notification.WARNING);
         * 
         * return; }
         */
        for (Media media : listMedia) {
            attachFileModel = new EvaluationAttachFileModel();
            attachFileId = attachFileId + 1;
            attachFileModel.setAttachID(attachFileId);
            attachFileModel.setFileContent(media);
            attachFileModel.setFileName(media.getName());
            attachFileModel.setFileType(Long
                    .valueOf((String) lbRapidTestFileType.getSelectedItem()
                            .getValue()));
            attachFileModel.setFileTypeName(lbRapidTestFileType
                    .getSelectedItem().getLabel());
            attachFileModel.setObjId(fileId);
            attachFileModel
                    .setObjType(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
            attachFileModel.setFilePath("");
            ;
            listFileModel.add(attachFileModel);
        }

        fileListbox.setModel(listFileModel);
        if (listMedia.size() > 0) {
            listMedia.clear();
        }
        flist.getChildren().clear();
    }

    @Listen("onClick=#btnMake")
    public void onCreatePdfFile() throws IOException, Exception {

        RapidTestDAOHE vFileRtfileDAOHE = new RapidTestDAOHE();
        VFileRtfile vFileRtfile = vFileRtfileDAOHE.findViewByFileId(fileId);
        Long documentTypeCode = vFileRtfile.getDocumentTypeCode();
        if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
            // Them moi XNN
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG) {
                if (councilList.getSelectedCount() == 0) {
                    showNotification("Chưa chọn thành viên hội đồng!",
                            Constants.Notification.WARNING);
                    councilList.focus();
                    return;
                }
                filePath = "/WEB-INF/template/BMquyetdinhthanhlaphoidong.docx";
                fileName = "BMquyetdinhthanhlaphoidong";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.QUYET_DINH_LAP_HOI_DONG;
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN) {
                if ("".equals(txtResult.getText().trim())) {
                    showNotification("Chưa nhập lý do!",
                            Constants.Notification.WARNING);
                    txtResult.focus();
                    return;
                }
                filePath = "/WEB-INF/template/BMkhongchophepluuhanhXNN.docx";
                fileName = "BMkhongchophepluuhanhXNN";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.KHONG_PHAI_BO_XNN;
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG) {
                if (councilList.getSelectedCount() == 0) {
                    showNotification("Chưa chọn thành viên hội đồng!",
                            Constants.Notification.WARNING);
                    councilList.focus();
                    return;
                }
                filePath = "/WEB-INF/template/BMgiaymoihophoidong.docx";
                fileName = "BMgiaymoihophoidong";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.GIAY_MOI_HOP_HOI_DONG;
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH) {
                filePath = "/WEB-INF/template/BMgiaychungnhanXNN.docx";
                fileName = "BMgiaychungnhanXNN";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.GIAY_CHUNG_NHAN_XNN;
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG) {
                filePath = "";
                fileName = "";
            }
            if ("".equals(filePath)) {
                showNotification("Mẫu file bạn chọn không tồn tại!",
                        Constants.Notification.WARNING);

                return;
            }
            ExportFileDAO exp = new ExportFileDAO();
            String permitNo = onSignPermit(vFileRtfile);

            UserDAOHE userDAOHE;
            userDAOHE = new UserDAOHE();
            Users signUsers = userDAOHE.getUserById((Long) lbPosition
                    .getSelectedItem().getValue());
            String f = "";
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN) {
                if (checkViewResult() == 1) {
                    f = exp.exportBMkhongchophepluuhanhXNN(vFileRtfile, false,
                            filePath, permitNo, txtResult.getText(), fileName,
                            signUsers);
                }
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG) {
                ListModelList<RtCouncil> lst = new ListModelList<RtCouncil>();
                for (RtCouncil council : listRtCouncil.getSelection()) {
                    lst.add(council);
                }
                f = exp.exportBMquyetdinhthanhlaphoidong(vFileRtfile, false,
                        filePath, permitNo, fileName, lst, signUsers);
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH) {
                f = exp.exportBMgiaychungnhanXNN(vFileRtfile, false, filePath,
                        permitNo, txtNote.getText(), fileName, signUsers);
            }
            AttachDAO base = new AttachDAO();

            if (!"".equals(f)) {
                attachFileModel = new EvaluationAttachFileModel();
                attachFileId = attachFileId + 1;
                attachFileModel.setAttachID(attachFileId);
                attachFileModel.setFileContent(null);
                attachFileModel.setFileName(base.getFileName(f));
                attachFileModel.setFileType(Long
                        .valueOf((String) lbRapidTestFileType.getSelectedItem()
                                .getValue()));
                attachFileModel.setFileTypeName(lbRapidTestFileType
                        .getSelectedItem().getLabel());
                attachFileModel.setObjId(fileId);
                attachFileModel
                        .setObjType(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
                attachFileModel.setFilePath(f);
                listFileModel.add(attachFileModel);
            }
            if (checkCouncilList() == 1) {
                if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                        .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG) {
                    String meetingDate = Integer.toString(tbMeetingTime.getValue()
                            .getHours())
                            + " giờ "
                            + Integer.toString(tbMeetingTime.getValue()
                                    .getMinutes());
                    meetingDate = meetingDate
                            + ", "
                            + "ngày "
                            + Integer.toString(dbMeetingDate.getValue().getDate())
                            + " tháng "
                            + Integer
                            .toString(dbMeetingDate.getValue().getMonth() + 1)
                            + " năm "
                            + Integer
                            .toString(dbMeetingDate.getValue().getYear() + 1900);
                    for (RtCouncil council : listRtCouncil.getSelection()) {
                        f = exp.exportBMgiaymoihophoidong(vFileRtfile, false,
                                filePath, permitNo, council.getCode() + "_"
                                + fileName, meetingDate, council.getName(),
                                signUsers);
                        if (!"".equals(f)) {
                            attachFileModel = new EvaluationAttachFileModel();
                            attachFileId = attachFileId + 1;
                            attachFileModel.setAttachID(attachFileId);
                            attachFileModel.setFileContent(null);
                            attachFileModel.setFileName(base.getFileName(f));
                            attachFileModel.setFileType(Long
                                    .valueOf((String) lbRapidTestFileType
                                            .getSelectedItem().getValue()));
                            attachFileModel.setFileTypeName(lbRapidTestFileType
                                    .getSelectedItem().getLabel());
                            attachFileModel.setObjId(fileId);
                            attachFileModel
                                    .setObjType(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
                            attachFileModel.setFilePath(f);
                            listFileModel.add(attachFileModel);
                        }
                    }
                }
            }
            fileListbox.setModel(listFileModel);
        }

        if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
            // Thay doi XNN
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG) {
                if (councilList.getSelectedCount() == 0) {
                    showNotification("Chưa chọn thành viên hội đồng!",
                            Constants.Notification.WARNING);
                    councilList.focus();
                    return;
                }
                filePath = "/WEB-INF/template/BMquyetdinhthanhlaphoidong_Thaydoi.docx";
                fileName = "BMquyetdinhthanhlaphoidong_Thaydoi";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.QUYET_DINH_LAP_HOI_DONG;
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN) {
                filePath = "/WEB-INF/template/BMkhongchophepluuhanhXNN_Thaydoi.docx";
                fileName = "BMkhongchophepluuhanhXNN_Thaydoi";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.KHONG_PHAI_BO_XNN;
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG) {
                if (councilList.getSelectedCount() == 0) {
                    showNotification("Chưa chọn thành viên hội đồng!",
                            Constants.Notification.WARNING);
                    councilList.focus();
                    return;
                }
                filePath = "/WEB-INF/template/BMgiaymoihophoidong_Thaydoi.docx";
                fileName = "BMgiaymoihophoidong_Thaydoi";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.GIAY_MOI_HOP_HOI_DONG;
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH) {
                filePath = "/WEB-INF/template/BMgiaychungnhanXNN_Thaydoi.docx";
                fileName = "BMgiaychungnhanXNN_Thaydoi";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.GIAY_CHUNG_NHAN_XNN;
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG) {
                filePath = "";
                fileName = "";
            }
            if ("".equals(filePath)) {
                showNotification("Mẫu file bạn chọn không tồn tại!",
                        Constants.Notification.WARNING);

                return;
            }
            ExportFileDAO exp = new ExportFileDAO();
            String permitNo = onSignPermit(vFileRtfile);

            UserDAOHE userDAOHE;
            userDAOHE = new UserDAOHE();
            Users signUsers = userDAOHE.getUserById((Long) lbPosition
                    .getSelectedItem().getValue());
            String f = "";
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN) {
                EvaluationRecord obj;
                EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
                obj = objDAO
                        .findMaxByFileIdAndEvalType(
                                fileId,
                                Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION_COUNCIL_MEETING_DATE);
                String meetingDate = "";
                if (obj != null) {
                    Gson gson = new Gson();
                    EvaluationModel evModel = gson.fromJson(
                            obj.getFormContent(), EvaluationModel.class);
                    if (evModel.getMeetingDate() != null) {
                        meetingDate = DateTimeUtils.convertDateToString(evModel
                                .getMeetingDate());
                    }
                }
                f = exp.exportBMkhongchophepluuhanhXNN_thaydoi(vFileRtfile,
                        false, filePath, permitNo, "",
                        fileName, signUsers, meetingDate);
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG) {
                ListModelList<RtCouncil> lst = new ListModelList<RtCouncil>();
                for (RtCouncil council : listRtCouncil.getSelection()) {
                    lst.add(council);
                }
                f = exp.exportBMquyetdinhthanhlaphoidong(vFileRtfile, false,
                        filePath, permitNo, fileName, lst, signUsers);
            }
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH) {
                EvaluationRecord obj;
                EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
                obj = objDAO
                        .findMaxByFileIdAndEvalType(
                                fileId,
                                Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION_COUNCIL_MEETING_DATE);
                String meetingDate = "";
                if (obj != null) {
                    Gson gson = new Gson();
                    EvaluationModel evModel = gson.fromJson(obj.getFormContent(),
                            EvaluationModel.class);
                    if (evModel.getMeetingDate() != null) {
                        meetingDate = DateTimeUtils.convertDateToString(evModel
                                .getMeetingDate());
                    }
                }
                f = exp.exportBMgiaychungnhanXNN_thaydoi(vFileRtfile, false,
                        filePath, permitNo, txtNote.getText(), fileName, signUsers,
                        meetingDate);
            }
            AttachDAO base = new AttachDAO();

            if (!"".equals(f)) {
                attachFileModel = new EvaluationAttachFileModel();
                attachFileId = attachFileId + 1;
                attachFileModel.setAttachID(attachFileId);
                attachFileModel.setFileContent(null);
                attachFileModel.setFileName(base.getFileName(f));
                attachFileModel.setFileType(Long
                        .valueOf((String) lbRapidTestFileType.getSelectedItem()
                                .getValue()));
                attachFileModel.setFileTypeName(lbRapidTestFileType
                        .getSelectedItem().getLabel());
                attachFileModel.setObjId(fileId);
                attachFileModel
                        .setObjType(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
                attachFileModel.setFilePath(f);
                listFileModel.add(attachFileModel);
            }
            if (checkCouncilList() == 1) {
                if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                        .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG) {
                    String meetingDate = Integer.toString(tbMeetingTime.getValue()
                            .getHours())
                            + " giờ "
                            + Integer.toString(tbMeetingTime.getValue()
                                    .getMinutes());
                    meetingDate = meetingDate
                            + ", "
                            + "ngày "
                            + Integer.toString(dbMeetingDate.getValue().getDate())
                            + " tháng "
                            + Integer
                            .toString(dbMeetingDate.getValue().getMonth() + 1)
                            + " năm "
                            + Integer
                            .toString(dbMeetingDate.getValue().getYear() + 1900);
                    for (RtCouncil council : listRtCouncil.getSelection()) {
                        f = exp.exportBMgiaymoihophoidong(vFileRtfile, false,
                                filePath, permitNo, council.getCode() + "_"
                                + fileName, meetingDate, council.getName(),
                                signUsers);
                        if (!"".equals(f)) {
                            attachFileModel = new EvaluationAttachFileModel();
                            attachFileId = attachFileId + 1;
                            attachFileModel.setAttachID(attachFileId);
                            attachFileModel.setFileContent(null);
                            attachFileModel.setFileName(base.getFileName(f));
                            attachFileModel.setFileType(Long
                                    .valueOf((String) lbRapidTestFileType
                                            .getSelectedItem().getValue()));
                            attachFileModel.setFileTypeName(lbRapidTestFileType
                                    .getSelectedItem().getLabel());
                            attachFileModel.setObjId(fileId);
                            attachFileModel
                                    .setObjType(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
                            attachFileModel.setFilePath(f);
                            listFileModel.add(attachFileModel);
                        }
                    }
                }
            }
            fileListbox.setModel(listFileModel);
        }

        if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
            // Gia han XNN
            ExportFileDAO exp = new ExportFileDAO();

            UserDAOHE userDAOHE;
            userDAOHE = new UserDAOHE();
            Users signUsers = userDAOHE.getUserById((Long) lbPosition
                    .getSelectedItem().getValue());
            String f = "";
            if (Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue()
                    .toString()) == Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN) {
                filePath = "/WEB-INF/template/BMkhongchophepluuhanhXNN_Giahan.docx";
                fileName = "BMkhongchophepluuhanhXNN_Giahan";
                bCode = Constants.EVALUTION.BOOK_TYPE.RAPID_TEST.KHONG_PHAI_BO_XNN;
                String permitNo = onSignPermit(vFileRtfile);
                f = exp.exportBMkhongchophepluuhanhXNN_giahan(vFileRtfile, false,
                        filePath, permitNo, "", fileName,
                        signUsers);
            }
            AttachDAO base = new AttachDAO();
            if (!"".equals(f)) {
                attachFileModel = new EvaluationAttachFileModel();
                attachFileId = attachFileId + 1;
                attachFileModel.setAttachID(attachFileId);
                attachFileModel.setFileContent(null);
                attachFileModel.setFileName(base.getFileName(f));
                attachFileModel.setFileType(Long
                        .valueOf((String) lbRapidTestFileType.getSelectedItem()
                                .getValue()));
                attachFileModel.setFileTypeName(lbRapidTestFileType
                        .getSelectedItem().getLabel());
                attachFileModel.setObjId(fileId);
                attachFileModel
                        .setObjType(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
                attachFileModel.setFilePath(f);
                listFileModel.add(attachFileModel);
            }
            fileListbox.setModel(listFileModel);
        }
    }

    public int checkReturnFile() {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; // Khong hien thi
        }
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttachAll> lstRapidTestAttach = rDaoHe
                .findRapidTestAllAttachByCodeList(fileId, categoryCode);
        if (lstRapidTestAttach.size() == 0) {
            return Constants.CHECK_VIEW.NOT_VIEW;
        }
        return Constants.CHECK_VIEW.VIEW;
    }

    public int checkCouncilList() {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; // Khong hien thi
        }
        if (!showCouncilList) {
            return Constants.CHECK_VIEW.NOT_VIEW;
        }
        return Constants.CHECK_VIEW.VIEW;
    }

    public int checkViewResult() {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; // Khong hien thi
        }
        if (!showViewResult) {
            return Constants.CHECK_VIEW.NOT_VIEW;
        }
        return Constants.CHECK_VIEW.VIEW;
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws IOException {
        EvaluationAttachFileModel obj = (EvaluationAttachFileModel) event
                .getData();
        if ("".equals(obj.getFilePath())) {
            Filedownload.save(obj.getFileContent());
        } else {
            AttachDAO attDAO = new AttachDAO();
            attDAO.downloadFile(obj.getFilePath(), obj.getFileName());
        }

    }

    @Listen("onDownloadReturnFile = #fileReturnListbox")
    public void onDownloadReturnFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findByAttachId(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    @Listen("onDeleteReturnFile = #fileReturnListbox")
    public void onDeleteReturnFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        listFileReturnDeleteModel.add(obj);
        listFileReturnModel.remove(obj);
    }

    public String onSignPermit(VFileRtfile file) throws Exception {
        permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(file
                .getFileId());

        if (lstPermit.size() > 0) {// Da co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
        }
        permitDAO.saveOrUpdate(permit);
        Long bookNumber = putInBook(permit);// Vao so
        String receiveNo = getPermitNo(bookNumber);
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);
        return receiveNo;
        // fillFinalFileListbox(fileId);
    }

    private Permit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Permit Permit) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);// Lay so dau tien
        BookDocument bookDocument = createBookDocument(Permit.getPermitId(),
                book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    private String getPermitNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yy"); // Just the year,
        // with 2 digits
        String year = sdf.format(Calendar.getInstance().getTime());
        permitNo += "/" + year;
        // permitNo += "/CBMP-QLD";
        return permitNo;
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }
}
