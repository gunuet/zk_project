package com.viettel.module.rapidtest.Controller.include;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.Controller.include.CosEvaluationSignPermitController;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.evaluation.Model.MessageModel;
import com.viettel.module.importDevice.Controller.include.DepartmentLeadershipController;
import com.viettel.module.rapidtest.BO.RtCouncil;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.module.rapidtest.DAO.RtCouncilDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.model.include.EvaluationAttachFileModel;
import com.viettel.newsignature.plugin.SignPdfFile;
import com.viettel.newsignature.utils.CertUtils;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.module.rapidtest.model.SignExportModel;
import com.viettel.utils.LogUtils;

/**
 *
 * @author ThanhTM
 */
public class evaluationSignCouncilEstablishedController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire
    private List listRapidTestFileType;
    @Wire
    private List<Media> listMedia;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private Long fileId;
    private String categoryCode;
    private Long signedCode;
    @Wire
    private Textbox tbResonRequest;
    @Wire
    private Textbox txtValidate;
    @Wire
    private Listbox fileListbox, fileListboxCA, councilList;
    @Wire
    private Vlayout flist;
    @Wire
    Textbox txtCertSerial, txtBase64Hash, txtMessage;

    private Long attachFileId = 0L;

    private ListModelList<VFileRtAttachAll> lmlAttach, lmlSigned;

    private String fileSignOut = "";

    private Permit permit;

    private List listBook;
    private Long docType;
    private String bCode;
    private boolean showCouncilList = false;
    private ListModelList<RtCouncil> listRtCouncil;
    private ListModelList<SignExportModel> listSignExport;
    private PermitDAO permitDAO = new PermitDAO();
    private String signPath = "";
    private String signFile = "";

    private UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute("userToken");

    /**
     * thanhtm Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        categoryCode = arguments.get("code").toString();
        switch (categoryCode) {
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_VALUES;
                showCouncilList = true;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES;
                break;

            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_VALUES;
                showCouncilList = true;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES;
                break;

            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_VALUES;
                break;
            case Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY:
                categoryCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES;
                break;
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        fillFileListbox(fileId);
        if (checkCouncilList() == 1) {
            fillRtCouncil();
        }
        lmlSigned = new ListModelList<VFileRtAttachAll>();
        listSignExport = new ListModelList<SignExportModel>();
        txtValidate.setValue("0");
    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            //LogUtils.addLog("Tham dinh ho so xet nghiem nhanh:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        if (lmlAttach.size() == 0) {
            showWarningMessage("Chưa có file quyết định nào!");
            fileListbox.focus();
            return false;
        }

        return true;
    }

    private boolean isValidatedSignData() {
        if (lmlSigned.size() == 0) {
            showWarningMessage("Chưa có file quyết định nào được ký!");
            fileListboxCA.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * vnnt Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu khong dong y thi yeu cau nhap comment va luu comment
            if (!isValidatedSignData()) {
                return;
            }

            AttachDAO base = new AttachDAO();
            for (VFileRtAttachAll file : lmlSigned) {
                //onSignPermit(file);

                base.saveFileAttachPdfAfterSign(file.getAttachName(), file.getObjectId(), file.getAttachCat(), file.getAttachType(), file.getAttachPath());
            }
//     	   if ((categoryCode.equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_KEY))||(categoryCode.equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_KEY))) {
//           	sendMS();
//    	   }

            //updateAttachFileSigned();
            txtValidate.setValue("1");
            

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    private void fillFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttachAll> lstRapidTestAttach = rDaoHe.findRapidTestAllAttachByCodeList(fileId, categoryCode);
        lmlAttach = new ListModelList(lstRapidTestAttach);
        this.fileListbox.setModel(lmlAttach);
        //onSelect();//An hien form
    }

    private void fillRtCouncil() {
        RtCouncilDAO dao = new RtCouncilDAO();
        List<RtCouncil> lstRtCouncil = dao.getRtCouncilActiveList();
        if (lstRtCouncil.size() > 0) {
            listRtCouncil = new ListModelList<RtCouncil>(lstRtCouncil);
            listRtCouncil.setMultiple(true);
            this.councilList.setModel(listRtCouncil);
        }

        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        obj = objDAO.findMaxByFileIdAndEvalType(fileId, Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION_COUNCIL_MEETING_DATE);
        if (obj != null) {
            Gson gson = new Gson();
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            if (evModel.getEvaluationRtCouncil() != null) {
                councilList.setModel(evModel.getEvaluationRtCouncil());
            }
        }
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    @Listen("onDownloadCAFile = #fileListboxCA")
    public void onDownloadCAFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFile(obj.getAttachPath() + obj.getAttachName(), obj.getAttachName());
    }

    @Listen("onDeleteFile = #fileListboxCA")
    public void onDeleteFile(Event event) {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        lmlSigned.remove(obj);
    }

    @Listen("onSign = #businessWindow")
    public void onSign(Event event) {
        try {
            String data = event.getData().toString();
            for (SignExportModel sem : listSignExport) {
                String filePath = sem.getFilePath();
                FileUtil.mkdirs(filePath);
                String outputFileName = sem.getSignFileFinalName();
                String outputFile = filePath + outputFileName;
                String signature = data;
                SignPdfFile pdfSig;
                pdfSig = sem.getPdfSig();

                try {
                    //pdfSig.insertSignature(signature, c);
                    pdfSig.insertSignature(signature, sem.getOutPutFileFinal(), outputFile);
                } catch (IOException ex) {
                    LogUtils.addLogDB(ex);
                    showNotification("Ký số không thành công");
                    return;
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    showNotification("Ký số không thành công");
                    return;
                }
                fillSignedFileListbox(sem.getFilePath(), sem.getSignFileFinalName(), sem.getFile());
            }
            showNotification("Ký số thành công!", Constants.Notification.INFO);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
    }

    @Listen("onSign1 = #businessWindow")
    public void onSign1(Event event) {
        String data = event.getData().toString();

        //ResourceBundle rb = ResourceBundle.getBundle("config");
        //String filePath = rb.getString("signPdf");
        String filePath = signPath;
        FileUtil.mkdirs(filePath);
        //String outputFileName = "_signed_" + (new Date()).getTime() + ".pdf";
        String outputFileName = signFile;
        String outputFile = filePath + outputFileName;
        fileSignOut = outputFile;
        String signature = data;
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");

        String outputFileFinal = session.getAttribute("outputFileFinal").toString();

        try {
            //pdfSig.insertSignature(signature, outputFile);
            pdfSig.insertSignature(signature, outputFileFinal, fileSignOut);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        /*       try {
         //onSignPermit();
         fileSignOut = "";
         showNotification("Ký số thành công!", Constants.Notification.INFO);
         } catch (Exception ex) {
         Logger.getLogger(CosEvaluationSignPermitController.class.getName()).log(Level.SEVERE, null, ex);
         }*/
    }

    public String onSignPermit(VFileRtAttachAll file) throws Exception {
        permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(file.getObjectId());

        if (lstPermit.size() > 0) {//Da co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
        }
        //permitDAO.saveOrUpdate(permit);
        Long bookNumber = putInBook(permit);//Vao so
        String receiveNo = getPermitNo(bookNumber);
        permit.setReceiveNo(receiveNo);
        //permitDAO.saveOrUpdate(permit);
        return receiveNo;
        //fillFinalFileListbox(fileId);
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId, Constants.OBJECT_TYPE.COSMETIC_PERMIT);
        this.fileListboxCA.setModel(new ListModelArray(lstAttach));
    }

    private void fillSignedFileListbox(String filePath, String fileName, VFileRtAttachAll file) {
        file.setAttachPath(filePath);
        file.setAttachName(fileName);
        signPath = filePath;
        signFile = fileName;
        file.setAttachCat(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);

        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.QUYET_DINH_THANH_LAP_HOI_DONG_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_MOI_LAP_HOI_DONG_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.BIEN_BAN_HOP_HOI_DONG_DAKY;
        }

        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY;
        }

        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY;
        }
        if (file.getAttachType().equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH)) {
            signedCode = Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY;
        }
        file.setAttachType(signedCode);
        lmlSigned.add(file);
        this.fileListboxCA.setModel(lmlSigned);
    }

    /**
     * Onclick Phe duyet ho so, ki CA
     *
     * @param event
     */
    @Listen("onUploadCert = #businessWindow")
    public void onUploadCert(Event event) {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        try {
            listSignExport = new ListModelList<SignExportModel>();
            for (VFileRtAttachAll file : lmlAttach) {
                actionSignCA(event, actionPrepareSign(file), file);
                //actionSignCATest(event, actionPrepareSign(file), file);
            }
            Clients.evalJavaScript("signAndSubmit();");
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public String actionPrepareSign(VFileRtAttachAll file) {
        String fileToSign = "";
        try {
            fileToSign = onApproveFileSign(file);
            return fileToSign;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    public void actionSignCA(Event event, String fileToSign, VFileRtAttachAll file) throws Exception {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
 
        String certSerial = x509Cert.getSerialNumber().toString(16);

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
        AttachDAO base = new AttachDAO();
        //String outputFileFinalName = "_signed_quyet_dinh_lap_hoi_dong_" + (new Date()).getTime() + ".pdf";
        String outputFileFinalName = "_temp_" + base.getFileName(fileToSign);
        String signFileFinalName = "_signed_" + base.getFileName(fileToSign);
        String outPutFileFinal = filePath + outputFileFinalName;
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");//not use binhnt53 u230315
            String linkImageSign = folderPath + separator + caur.get(0).getSignature();
            //fileSignOut = outPutFileFinal;
            Pdf pdfProcess = new Pdf();

            String fieldName = tk.getUserFullName();

            SignPdfFile pdfSig = pdfProcess.createHash(fileToSign, outPutFileFinal, new Certificate[]{x509Cert}, linkImageSign, fieldName);
            if (pdfSig == null) {
                showNotification("Ký số không thành công!");
                return;
            }
            /*try {//chen chu ki
             pdfProcess.insertImageAll(fileToSign, outPutFileFinal, linkImageSign, linkImageStamp ,Constants.RAPID_TEST.KY_DONG_DAU_LANH_DAO_CUC);
             } catch (IOException ex) {
             Logger.getLogger(DepartmentLeadershipController.class.getName()).log(Level.SEVERE, null, ex);
             }
             try {//chen CKS
             base64Hash = pdfSig.createHash(outPutFileFinal, new Certificate[]{x509Cert});
             } catch (Exception ex) {
             Logger.getLogger(DepartmentLeadershipController.class.getName()).log(Level.SEVERE, null, ex);
             }*/

            txtBase64Hash.setValue(pdfProcess.getBase64Hash());
            txtCertSerial.setValue(certSerial);

            /*Session session = Sessions.getCurrent();
             session.setAttribute("certSerial", certSerial);
             session.setAttribute("base64Hash", base64Hash);
             txtBase64Hash.setValue(base64Hash);
             txtCertSerial.setValue(certSerial);
             session.setAttribute("PDFSignature", pdfSig);
             Clients.evalJavaScript("signAndSubmit();");*/
            SignExportModel sem = new SignExportModel();
            sem.setCertSerial(certSerial);
            sem.setBase64Hash(pdfProcess.getBase64Hash());
            sem.setFilePath(filePath);
            sem.setSignFileFinalName(signFileFinalName);
            sem.setPdfSig(pdfSig);
            sem.setFile(file);
            sem.setOutPutFileFinal(outPutFileFinal);
            listSignExport.add(sem);
            //fillSignedFileListbox(filePath, signFileFinalName, file);
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    public void actionSignCATest(Event event, String fileToSign, VFileRtAttachAll file) throws Exception {
        SignPdfFile pdfSig = new SignPdfFile();//pdfSig = new SignPdfFile();        

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
        AttachDAO base = new AttachDAO();
        //String outputFileFinalName = "_signed_quyet_dinh_lap_hoi_dong_" + (new Date()).getTime() + ".pdf";
        String outputFileFinalName = "_temp_" + base.getFileName(fileToSign);
        String signFileFinalName = "_signed_" + base.getFileName(fileToSign);
        String outPutFileFinal = filePath + outputFileFinalName;
        String folderPath = ResourceBundleUtil.getString("dir_upload");
        FileUtil.mkdirs(folderPath);
        String separator = ResourceBundleUtil.getString("separator");//not use binhnt53 u230315
        String linkImageSign = folderPath + separator + "Document\\2015\\5\\18\\CA\\11851\\63306_43251_chuky.jpg";
        String linkImageStamp = folderPath + separator + "Document\\2015\\5\\18\\CA\\11851\\63307_43250_a.png";
        //fileSignOut = outPutFileFinal;
        Pdf pdfProcess = new Pdf();
        try {//chen chu ki
            pdfProcess.insertImageAll(fileToSign, outPutFileFinal, linkImageSign, linkImageStamp, Constants.RAPID_TEST.KY_DONG_DAU_LANH_DAO_CUC);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
        }

        Session session = Sessions.getCurrent();
        session.setAttribute("PDFSignature", pdfSig);
        Clients.evalJavaScript("signAndSubmit();");
        fillSignedFileListbox(filePath, signFileFinalName, file);
    }

    /**
     * Phe duyet ho so, kem CKS
     *
     * @return @throws Exception
     */
    public String onApproveFileSign(VFileRtAttachAll file) throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return "fail";
        }

        //Tao file pdf từ mẫu file docx
       /*ExportModel exportModel = new ExportModel();
         exportModel.setSendNo(permit.getReceiveNo());
         exportModel.setSignDate(permit.getSignDate());
       
         ExportFileDAO exp = new ExportFileDAO();
         return exp.exportIDFNoSign(exportModel, false);*/
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(file.getAttachId());
        String fileOut = att.getFullPathFile();
        return fileOut;

        //base.showNotify("Phê duyệt hồ sơ thành công!");
    }

    private Permit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;

    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(Permit Permit) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);//Lay so dau tien
        BookDocument bookDocument = createBookDocument(Permit.getPermitId(), book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    private String getPermitNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yy"); // Just the year, with 2 digits
        String year = sdf.format(Calendar.getInstance().getTime());
        permitNo += "/" + year;
        //permitNo += "/CBMP-QLD";
        return permitNo;
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    public int checkCouncilList() {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        if (!showCouncilList) {
            return Constants.CHECK_VIEW.NOT_VIEW;
        }
        return Constants.CHECK_VIEW.VIEW;
    }

    public int checkViewCouncilList() {
        return Constants.CHECK_VIEW.NOT_VIEW;
    }

    public void updateAttachFileSigned() {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        if (lmlSigned != null && lmlSigned.size() > 0) {
            for (VFileRtAttachAll att : lmlSigned) {
                Long attachId = att.getAttachId();
                Attachs att_new = rDaoHe.findById(attachId);
                if (att_new.getIsSent() == null || att_new.getIsSent() == 0) {
                    att_new.setIsSent(1L);
                    rDaoHe.saveOrUpdate(att_new);
                }
            }
        }
    }

//	private void sendMS() {
//		Gson gson = new Gson();
//		MessageModel md = new MessageModel();
//		md.setCode(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
//		md.setFileId(fileId);
//	   if (categoryCode.equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_KEY)) {
//		   md.setFunctionName(Constants.FUNCTION_MESSAGE_RT.SendMs_05);
//	   }
//	   if (categoryCode.equals(Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_KEY)) {
//		   md.setFunctionName(Constants.FUNCTION_MESSAGE_RT.SendMs_06);
//	   }		
//		md.setPhase(0l);
//		md.setFeeUpdate(false);
//		String jsonMd = gson.toJson(md);
//		txtMessage.setValue(jsonMd);
//	}
}
