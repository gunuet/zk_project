/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import com.viettel.utils.Constants;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.module.rapidtest.BO.ProcedureFeeSubprocedure;
import com.viettel.module.rapidtest.DAOHE.ProcedureFeeSubDAOHE;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author giangpn
 */
public class ProcedureFeeSubCreateController extends BaseComposer {

    @Wire
    Textbox txtId, txtCost, txtPhase;
    @Wire
    Listbox lbProcedure, lbFee, lbSub;
    @Wire
    Window procedureFeeSubCreateWnd;

    public ProcedureFeeSubCreateController() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        CategoryDAOHE cdhe = new CategoryDAOHE();
        List lstProcedure = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.PROCEDURE);
        ListModelArray lstModelProcedure = new ListModelArray(lstProcedure);
        lbProcedure.setModel(lstModelProcedure);
        lbProcedure.renderAll();

        List lstFee = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.FEE);
        ListModelArray lstModelFee = new ListModelArray(lstFee);
        lbFee.setModel(lstModelFee);
        lbFee.renderAll();

        List lstSub = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.SUB_PROCEDURE);
        ListModelArray lstModelsub = new ListModelArray(lstSub);
        lbSub.setModel(lstModelsub);
        lbSub.renderAll();

        loadInfoToForm();
    }

    public void loadInfoToForm() {
        Long id = (Long) Executions.getCurrent().getArg().get("id");
        if (id != null) {
            ProcedureFeeSubDAOHE objhe = new ProcedureFeeSubDAOHE();
            ProcedureFeeSubprocedure
                    pfs = objhe.findById(id);
            if (pfs.getId() != null) {
                txtId.setValue(pfs.getId().toString());
            }
            if (pfs.getPhase() != null) {
                txtPhase.setValue(pfs.getPhase().toString());
            }
            if (pfs.getCost() != null) {
                txtCost.setValue(pfs.getCost().toString());
            }

            if (pfs.getProcedureId() != null) {
                for (int i = 0; i < lbProcedure.getListModel().getSize(); i++) {
                    Category ct = (Category) lbProcedure.getListModel().getElementAt(i);
                    if (pfs.getProcedureId().equals(ct.getCategoryId())) {
                        lbProcedure.setSelectedIndex(i);
                        break;
                    }
                }
            }
            if (pfs.getFeeId() != null) {
                for (int i = 0; i < lbFee.getListModel().getSize(); i++) {
                    Category ct = (Category) lbFee.getListModel().getElementAt(i);
                    if (pfs.getFeeId().equals(ct.getCategoryId())) {
                        lbFee.setSelectedIndex(i);
                        break;
                    }
                }
            }
            if (pfs.getSubProcedureId() != null) {
                for (int i = 0; i < lbSub.getListModel().getSize(); i++) {
                    Category ct = (Category) lbSub.getListModel().getElementAt(i);
                    if (pfs.getSubProcedureId().equals(ct.getCategoryId())) {
                        lbSub.setSelectedIndex(i);
                        break;
                    }
                }
            }
        } else {
            txtId.setValue("");
            txtPhase.setValue("");
            txtCost.setValue("");
            lbProcedure.setSelectedIndex(0);
            lbFee.setSelectedIndex(0);
            lbSub.setSelectedIndex(0);
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        ProcedureFeeSubprocedure rs = new ProcedureFeeSubprocedure();
        if (lbProcedure.getSelectedItem() != null) {
            int idx = lbProcedure.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setProcedureId((Long) lbProcedure.getSelectedItem().getValue());
                rs.setProcedureName(lbProcedure.getSelectedItem().getLabel());
            } else {
                showNotification("Phải chọn thủ tục", Constants.Notification.ERROR);
                lbProcedure.focus();
                return;
            }
        }
        if (lbFee.getSelectedItem() != null) {
            int idx = lbFee.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setFeeId((Long) lbFee.getSelectedItem().getValue());
                rs.setFeeName(lbFee.getSelectedItem().getLabel());
            } else {
                showNotification("Phải chọn Phí", Constants.Notification.ERROR);
                lbFee.focus();
                return;
            }
        }

        if (lbSub.getSelectedItem() != null) {
            int idx = lbSub.getSelectedItem().getIndex();
            if (idx > 0l) {
                rs.setSubProcedureId((Long) lbSub.getSelectedItem().getValue());
                rs.setSubProcedureName(lbSub.getSelectedItem().getLabel());
            }
        }

        if (txtId.getValue() != null && !txtId.getValue().isEmpty()) {
            rs.setId(Long.parseLong(txtId.getValue()));
        }
        if (txtCost.getValue() != null && txtCost.getValue().trim().length() == 0) {
            showNotification("Phải nhập số tiền", Constants.Notification.ERROR);
            txtCost.focus();
            return;
        } else {
            rs.setCost(Long.parseLong(txtCost.getValue()));
        }
        if (txtPhase.getValue() != null && txtPhase.getValue().trim().length() == 0) {
            showNotification("Phải nhập giai đoạn", Constants.Notification.ERROR);
            txtPhase.focus();
            return;
        } else {
            rs.setPhase(Long.parseLong(txtPhase.getValue()));
        }

        rs.setIsActive(1L);

        ProcedureFeeSubDAOHE rdhe = new ProcedureFeeSubDAOHE();
//        if(rdhe.hasDuplicate(rs)){
//          showNotification("Trùng dữ liệu đã tạo trên hệ thống");
//          return;
//        } 
        rdhe.saveOrUpdate(rs);

        if (txtId.getValue() != null && !txtId.getValue().isEmpty()) {
            //update thì detach window
            procedureFeeSubCreateWnd.detach();
        } else {
            // them moi thi clear window
            loadInfoToForm();
        }
        showNotification("Lưu thành công", Constants.Notification.INFO);

        Window parentWnd = (Window) Path.getComponent("/procedureFeeSubWnd");
        Events.sendEvent(new Event("onReload", parentWnd, null));
    }

}
