/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import com.viettel.utils.Constants;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.rapidtest.BO.ProcedureFeeSubprocedure;
import com.viettel.module.rapidtest.BO.RtCouncil;
import com.viettel.module.rapidtest.DAOHE.ProcedureFeeSubDAOHE;
import com.viettel.utils.LogUtils;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Window;

/**
 *
 * @author giangpn
 */
public class ProcedureFeeSubManageController extends BaseComposer {

    @Wire
    Listbox lbProcedure, lbFee, lbSub;
    @Wire
    Listbox lbProcedureFeeSub;
    @Wire
    Paging userPagingBottom;
    ProcedureFeeSubprocedure searchForm;

    @Override
    public void doAfterCompose(Component window) {
        try {
            super.doAfterCompose(window);

            CategoryDAOHE cdhe = new CategoryDAOHE();
            List lstProcedure = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.PROCEDURE);
            List lstFee = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.FEE);
            List lstSub = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.SUB_PROCEDURE);

            ListModelArray lstModelProcedure = new ListModelArray(lstProcedure);
            lbProcedure.setModel(lstModelProcedure);

            ListModelArray lstModelFee = new ListModelArray(lstFee);
            lbFee.setModel(lstModelFee);

            ListModelArray lstModelSub = new ListModelArray(lstSub);
            lbSub.setModel(lstModelSub);

            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {
            userPagingBottom.setActivePage(0);
            searchForm = new ProcedureFeeSubprocedure();

            if (lbProcedure.getSelectedItem() != null) {
                Long procedureId = (Long) lbProcedure.getSelectedItem().getValue();
                searchForm.setProcedureId(procedureId);
            }
            if (lbFee.getSelectedItem() != null) {
                Long feeId = (Long) lbFee.getSelectedItem().getValue();
                searchForm.setFeeId(feeId);
            }
            if (lbSub.getSelectedItem() != null) {
                Long subId = (Long) lbSub.getSelectedItem().getValue();
                searchForm.setSubProcedureId(subId);
            }
            //Fill danh sach loai danh muc
            fillDataToList();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    @Listen("onReload=#procedureFeeSubWnd")
    public void onReload() {
        onSearch();
    }

    private void fillDataToList() {
        ProcedureFeeSubDAOHE objhe = new ProcedureFeeSubDAOHE();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = objhe.search(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }

        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbProcedureFeeSub.setModel(lstModel);
    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws IOException {
        Window window = (Window) Executions.createComponents(
                "/Pages/admin/procedureFeeSub/procedureFeeSubCreate.zul", null, null);
        window.doModal();
    }

    @Listen("onEdit=#lbProcedureFeeSub")
    public void onUpdate(Event ev) throws IOException {
        ProcedureFeeSubprocedure pfs = (ProcedureFeeSubprocedure) lbProcedureFeeSub.getSelectedItem().getValue();
        Map args = new ConcurrentHashMap();
        args.put("id", pfs.getId());
        Window window = (Window) Executions.createComponents("/Pages/admin/procedureFeeSub/procedureFeeSubCreate.zul", null, args);
        window.doModal();
    }

    @Listen("onDelete=#lbProcedureFeeSub")
    public void onDelete(Event ev) throws IOException {
        Messagebox.show("Bạn có chắc muốn xóa thông tin phí này?", "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {

                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    ProcedureFeeSubprocedure rs = (ProcedureFeeSubprocedure) lbProcedureFeeSub.getSelectedItem().getValue();
                                    deleteProcedureFeeSub(rs);
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
        lbProcedureFeeSub.clearSelection();
    }

    public void deleteProcedureFeeSub(ProcedureFeeSubprocedure rs) {
        ProcedureFeeSubDAOHE pfshe = new ProcedureFeeSubDAOHE();
        pfshe.delete(rs.getId());
        onSearch();
    }
}
