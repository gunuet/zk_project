/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.model.AttachCategoryModel;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

/**
 *
 * @author giangpn
 */
public class PublicFileManageListController extends BaseComposer {

   @Wire 
   Window PublicFileManagerList;
   @Wire
   Listbox lstPublicFile;
   
    private Window parentWindow;
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
       
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component window) throws Exception {
        super.doAfterCompose(window);
         Execution execution = Executions.getCurrent();
        parentWindow = (Window) execution.getArg().get("parentWindow");
       loadAferToForm();
      
    }

    public void loadAferToForm() {
        AttachDAOHE obj = new AttachDAOHE();
        List lst = obj.getListProFile(getUserId());
        ListModelArray lstModel = new ListModelArray(lst);
        lstModel.setMultiple(true);
        lstPublicFile.setModel(lstModel);
    }

    public void loadBeforeToForm() {
     
    }
    @Listen("onClick = #btnSave")
    public void onSaveDoc() {
        List drSend = new ArrayList();
        Set<Listitem> ls = lstPublicFile.getSelectedItems();
        if (ls.size() > 0) {
            for (Listitem item : ls) {
                AttachCategoryModel Process = item.getValue();
                drSend.add(Process);
            }

            Map<String, Object> args = new ConcurrentHashMap<>();
            args.put("documentReceiveProcess", drSend);
            Events.sendEvent(new Event("onChooseProFile", parentWindow, args));
            PublicFileManagerList.onClose();
        } else {
            showNotification("Bạn chưa chọn hồ sơ");
        }
    }
}
