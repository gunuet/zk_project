package com.viettel.module.rapidtest.DAO;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.model.SearchModel;

import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author Linhdx
 */
public class RapidTestAllController extends BaseComposer {

    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;

    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;

    @Wire("#incSearchFullForm #tbRapidTestNo")
    private Textbox tbRapidTestNo;

    @Wire("#incSearchFullForm #lboxStatus")
    private Listbox lboxStatus;

    @Wire("#incSearchFullForm #lboxDocumentTypeCode")
    private Listbox lboxDocumentTypeCode;

    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire
    private Listbox lbList;
    @Wire
    private Window rapidTestAll;
    private SearchModel lastSearchModel;

    private String FILE_TYPE_STR;
    private long FILE_TYPE;
    private long DEPT_ID;

    private Users user;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        lastSearchModel = new SearchModel();
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        FILE_TYPE_STR = (String) arguments.get("filetype");
        String deptId2 = (String) arguments.get("deptid");
        String menuTypeStr = (String) arguments.get("menuType");
        lastSearchModel.setMenuTypeStr(menuTypeStr);
        
        
        try {
            DEPT_ID = Long.parseLong(deptId2);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            DEPT_ID = -1l;
        }
        FILE_TYPE = WorkflowAPI.getInstance().getProcedureTypeIdByCode(FILE_TYPE_STR);

        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);

        CategoryDAOHE cdhe = new CategoryDAOHE();
        List lstStatus = cdhe.findAllCategorySearch(Constants.CATEGORY_TYPE.FILE_STATUS);
        ListModelArray lstModelProcedure = new ListModelArray(lstStatus);
        lboxStatus.setModel(lstModelProcedure);
        onSearch();
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);

        
        lastSearchModel.setCreateDateFrom(dbFromDay.getValue());
        lastSearchModel.setCreateDateTo(dbToDay.getValue());
        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        lastSearchModel.setRapidTestNo(tbRapidTestNo.getValue());

        if (lboxStatus.getSelectedItem() != null) {
            try {
                String value = lboxStatus.getSelectedItem().getValue();
                if (value != null) {
                    Long valueL = Long.valueOf(value);
                    if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                        lastSearchModel.setStatus(valueL);
                    }
                }
            } catch (Exception ex) {
                LogUtils.addLogDB(ex);

            }

        }

        if (lboxDocumentTypeCode.getSelectedItem() != null) {
            String value = lboxDocumentTypeCode.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if (valueL != Constants.COMBOBOX_HEADER_VALUE) {
                    lastSearchModel.setDocumentTypeCode(valueL);
                }
            }
        }

        reloadModel(lastSearchModel);

    }

    private void reloadModel(SearchModel searchModel) {
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm;
        RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(), start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel, getUserId(), getDeptId(), start, take);
        }
        userPagingBottom.setTotalSize(plm.getCount());
        userPagingTop.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
            userPagingTop.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
            userPagingTop.setVisible(true);
        }

        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lbList.setModel(lstModel);

    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(lastSearchModel);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #rapidTestAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(lastSearchModel);
        }
    }

    @Listen("onShowFullSearch=#rapidTestAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    @Listen("onChangeTime=#rapidTestAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            dbFromDay.setValue(model.getFromDate());
            dbToDay.setValue(model.getToDate());
            onSearch();
        }
    }

    private Map<String, Object> setParam(Map<String, Object> arguments) {
        arguments.put("parentWindow", rapidTestAll);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("procedureId", FILE_TYPE);
        arguments.put("deptid", DEPT_ID);
        return arguments;

    }

    @Listen("onOpenCreate=#rapidTestAll")
    public void onOpenCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("CRUDMode", "CREATE");
        setParam(arguments);
        Window window = createWindow("windowCRUD",
                "/Pages/rapidTest/rapidTestCRUD.zul", arguments,
                Window.EMBEDDED);
        window.setMode(Window.Mode.EMBEDDED);
        //rapidTestAll.setVisible(false);
    }

    @Listen("onOpenView = #lbList")
    public void onOpenView(Event event) {
        VFileRtfile obj = (VFileRtfile) event.getData();
        createWindowView(obj.getFileId(), Constants.DOCUMENT_MENU.ALL,
                rapidTestAll);
        rapidTestAll.setVisible(false);

    }

    @Listen("onSelectedProcess = #rapidTestAll")
    public void onSelectedProcess(Event event) {
        rapidTestAll.setVisible(false);

    }

    private void createWindowView(Long id, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id);
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        arguments.put("filetype", FILE_TYPE_STR);
        arguments.put("deptid", DEPT_ID);
        createWindow("windowView", "/Pages/rapidTest/viewRapidTest.zul",
                arguments, Window.EMBEDDED);
    }

    @Listen("onSave = #rapidTestAll")
    public void onSave(Event event) {
        rapidTestAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
    }

    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi
     * windowDocIn len Con cac su kien save, update thi da xu li hien thi
     * windowDocIn trong phuong thuc onSave
     */
    @Listen("onVisible = #rapidTestAll")
    public void onVisible() {
        reloadModel(lastSearchModel);
        rapidTestAll.setVisible(true);
    }

    @Listen("onOpenUpdate = #lbList")
    public void onOpenUpdate(Event event) {
        VFileRtfile obj = (VFileRtfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "UPDATE");
        setParam(arguments);
        createWindow("windowCRUDRapidTest", "/Pages/rapidTest/rapidTestCRUD.zul", arguments, Window.EMBEDDED);
        rapidTestAll.setVisible(false);
    }

    @Listen("onViewFlow = #lbList")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul", args, Window.MODAL);
    }

    @Listen("onDelete = #lbList")
    public void onDelete(Event event) {
        final VFileRtfile obj = (VFileRtfile) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM, Constants.DOCUMENT_TYPE_NAME.FILE);
        Messagebox.show(message, "XÃ¡c nháº­n", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {
                                        FilesDAOHE objDAOHE = new FilesDAOHE();
                                        objDAOHE.delete(obj.getFileId());
                                        onSearch();
                                        showNotification(String.format(Constants.Notification.DELETE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.INFO);
                                    } catch (Exception e) {
                                        showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                        LogUtils.addLogDB(e);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }

    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(VFileRtfile obj) {
        return user.getUserId() != null && user.getUserId().equals(obj.getCreatorId());
    }

    public boolean isAbleToModify(VFileRtfile obj) {
        return user.getUserId() != null && user.getUserId().equals(obj.getCreatorId());
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }
    
     public int CheckGiayPhep(Long fileid) {
        //RapidTestDAOHE rptDao = new RapidTestDAOHE();
        int check = 0;//rptDao.CheckGiayPhep(fileid, getUserId());
        return check;
    }
     
    public int checkDispathSDBS(Long fileid) {
        //RapidTestDAOHE rptDao = new RapidTestDAOHE();
        int check = 0;//cosDao.checkDispathSDBS(fileid, getUserId());
        return check;
    }
    
    @Listen("onCopy = #lbList")
    public void onCopy(Event event) {

        VFileRtfile obj = (VFileRtfile) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getFileId());
        arguments.put("CRUDMode", "COPY");
        setParam(arguments);
        createWindow("windowCRUDRapidTest", "/Pages/rapidTest/rapidTestCRUD.zul", arguments, Window.EMBEDDED);
        rapidTestAll.setVisible(false);

    }

    @Listen("onRefresh = #rapidTestAll")
    public void onRefresh(Event event) {
        onOpenView(event);
    }

}
