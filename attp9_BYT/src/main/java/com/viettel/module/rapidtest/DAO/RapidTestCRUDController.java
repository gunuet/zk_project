package com.viettel.module.rapidtest.DAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.sys.model.CategorySearchForm;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.module.Pdf.Pdf;
import com.viettel.module.cosmetic.BO.CaUser;
import com.viettel.module.cosmetic.DAO.CaUserDAO;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.payment.utils.ConstantsPayment;
import com.viettel.module.rapidtest.BO.RtFile;
import com.viettel.module.rapidtest.BO.RtTargetTesting;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.signature.plugin.SignPdfFile;
import com.viettel.signature.utils.CertUtils;
import com.viettel.utils.*;
import com.viettel.voffice.BO.Business;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.BO.VFeeProcedure;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.BusinessDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.voffice.DAOHE.VFeeProcedureDAOHE;
import com.viettel.voffice.model.AttachCategoryModel;

/**
 *
 * @author Linhdx
 */
public class RapidTestCRUDController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final int SAVE = 1;
    private final int SAVE_CLOSE = 2;
    private final int SAVE_COPY = 3;
    private final int RAPID_TEST_FILE_TYPE = 1;

    private long FILE_TYPE;
    private long DEPT_ID;

    @Wire
    private Vlayout flist, fListImportExcel;
    private List<Media> listMedia, listFileExcel;
    private List listRapidTestFileType;
    private ListModelList<RtTargetTesting> rtTargetTestingMode;
    private ListModelList<RtTargetTesting> rtDeleteTargetTestingMode;

    // label for validating data
    @Wire
    private Label lbTopWarning, lbBottomWarning, lbImportExcelWarning;

    // Ý kiến lãnh đạo
    @Wire
    private Window windowCRUDRapidTest;
    private Window parentWindow;
    private String crudMode;

    private String isSave;
    private ListModel model;
    private RtFile rtFile;
    private Files files;
    private RtTargetTesting rtTargetTesting;
    private Long originalFilesId;
    private Long originalRtFileId;
    private Long originalFileType;
    Long documentTypeCode;
    @Wire
    Textbox txtCertSerial, txtBase64Hash;
    @Wire
    private Textbox tbRapidTestNo, tbRapidTestName, tbRapidTestCode,
            tbPlaceOfManufacture, tbOperatingPrinciples, tbTargetTesting,
            tbRangeOfApplications, tbLimitDevelopment, tbPrecision,
            tbDescription, tbPakaging, tbStorageConditions, tbOtherInfos,
            tbAttachmentsInfo, tbSignPlace, tbSignName, t2bRapidTestChangeNo,
            t2bRapidTestName, t2bRapidTestCode, t2bCirculatingRapidTestNo,
            t2bContens, t2bAttachmentsInfo, t2bSignPlace, t2bSignName,
            t3bCirculatingExtensionNo, t3bRapidTestName, t3bRapidTestCode,
            t3bCirculatingRapidTestNo, t3bExtensionNo, t3bAttachmentsInfo,
            t3bSignPlace, t3bSignName;
    @Wire
    private Intbox ibShelfLife;
    @Wire
    private Datebox dbSignDate, d2bDateIssue, d2bSignDate, d3bDateIssue,
            d3bDateEffect, d3bSignDate;
    @Wire
    private Listbox lboxDocumentTypeCode,// loai ho so
            lboxPropertiesTests, lbRapidTestFileType, // loai tep dinh kem
            fileListbox, finalFileListbox, lbListOfTargetTesting;
    @Wire
    private Groupbox gbRapidCRUD1, gbRapidCRUD2, gbRapidCRUD3;

    @Wire
    private Tabbox tb;

    @Wire
    private Tabpanel mainpanel;

    private String fileSignOut = "";

    private RapidTestDAOHE vFileRtfileDAOHE = new RapidTestDAOHE();

    private VFileRtfile vFileRtfile;

    private Long rtTargetTestingId = 1L;

    @Wire
    private RtTargetTesting selected;

    private Permit permit;
    private List listBook;
    private Long docType;
    private String bCode;
    private RtTargetTesting itemSelected = null;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();

        parentWindow = (Window) arguments.get("parentWindow");
        crudMode = (String) arguments.get("CRUDMode");
        listMedia = new ArrayList();
        switch (crudMode) {
            case "CREATE":
                DEPT_ID = Long.parseLong(arguments.get("deptId").toString());
                FILE_TYPE = WorkflowAPI.getInstance().getProcedureTypeIdByCode(
                        arguments.get("procedureId").toString());
                documentTypeCode = Long.parseLong(arguments.get("documentTypeCode")
                        .toString());
                files = new Files();
                rtFile = new RtFile();
                rtTargetTestingMode = new ListModelList<RtTargetTesting>();
                rtDeleteTargetTestingMode = new ListModelList<RtTargetTesting>();
                rtTargetTesting = new RtTargetTesting();
                loadBusinessInfo();
                break;
            case "UPDATE":
                Long id = (Long) arguments.get("id");
                FilesDAOHE objFile = new FilesDAOHE();
                files = objFile.findById(id);
                RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
                rtFile = objDAOHE.findByFileId(id);
                vFileRtfileDAOHE = new RapidTestDAOHE();
                vFileRtfile = vFileRtfileDAOHE.findViewByFileId(id);
                originalFilesId = rtFile.getFileId();
                originalFileType = files.getFileType();
                originalRtFileId = rtFile.getRtFileId();
                break;
            case "COPY":
                id = (Long) arguments.get("id");
                FilesDAOHE fileDAOHE = new FilesDAOHE();
                Files filesOld = fileDAOHE.findById(id);
                files = copy(filesOld);
                objDAOHE = new RapidTestDAOHE();
                RtFile rtFileOld = objDAOHE.findByFileId(id);
                rtFile = copy(rtFileOld);

                originalFilesId = filesOld.getFileId();
                originalFileType = filesOld.getFileType();
                originalRtFileId = rtFileOld.getRtFileId();
                break;
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }

    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        tb.setVisible(false);

        loadItemForEdit();
        setVisible(documentTypeCode);
        if ("UPDATE".equals(crudMode) || "COPY".equals(crudMode)) {
            if ("UPDATE".equals(crudMode)) {
                fillFileListbox(originalFilesId);
                fillFinalFileListbox(originalFilesId);
            }
            RtTargetTestingDAO objTargetTesting = new RtTargetTestingDAO();
            List lsTargetTesting = objTargetTesting.getByFileId(originalFilesId);
            rtTargetTestingMode = new ListModelList(lsTargetTesting);
            rtTargetTestingMode.setMultiple(true);
            lbListOfTargetTesting.setModel(rtTargetTestingMode);
            lbListOfTargetTesting.renderAll();
            rtDeleteTargetTestingMode = new ListModelList<RtTargetTesting>();
        }

    }

    private void loadBusinessInfo() {
        Long userId = getUserId();
        BusinessDAOHE bhe = new BusinessDAOHE();
        Business business = bhe.findByUserId(userId);
        if (business != null) {
            files.setBusinessId(business.getBusinessId());
            files.setBusinessName(business.getBusinessName());
            files.setBusinessAddress(business.getBusinessAddress());
            files.setBusinessPhone(business.getBusinessTelephone());
            files.setBusinessFax(business.getBusinessFax());
            files.setTaxCode(business.getBusinessTaxCode());
        }
    }

    private void loadItemForEdit() {
        if (rtFile.getDocumentTypeCode() != null) {
            documentTypeCode = rtFile.getDocumentTypeCode();
        }

        Long propertiesTests = rtFile.getPropertiesTests();
        if (propertiesTests != null) {
            if (Constants.RAPID_TEST.DINH_TINH.equals(propertiesTests)) {
                lboxPropertiesTests.setSelectedIndex(1);
            } else if (Constants.RAPID_TEST.BAN_DINH_TINH
                    .equals(propertiesTests)) {
                lboxPropertiesTests.setSelectedIndex(2);
            } else if (Constants.RAPID_TEST.DINH_LUONG.equals(propertiesTests)) {

            }

        }
    }

    public void onSelectStatus() {

        gbRapidCRUD1.setVisible(true);
        gbRapidCRUD2.setVisible(false);
        gbRapidCRUD3.setVisible(false);
    }

    public void setVisible(Long documentTypeCode) {
        if (documentTypeCode != null) {
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI
                    .equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(true);
                gbRapidCRUD2.setVisible(false);
                gbRapidCRUD3.setVisible(false);
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG
                    .equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(false);
                gbRapidCRUD2.setVisible(true);
                gbRapidCRUD3.setVisible(false);
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN
                    .equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(false);
                gbRapidCRUD2.setVisible(false);
                gbRapidCRUD3.setVisible(true);
            } else {
                tb.setVisible(false);
            }

        }
    }

    /**
     * linhdx Hàm load danh sach trong dropdown control
     *
     * @param type
     * @return
     */
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case RAPID_TEST_FILE_TYPE:
                categoryDAOHE = new CategoryDAOHE();
                listRapidTestFileType = categoryDAOHE
                        .findAllCategory(Constants.CATEGORY_TYPE.RAPID_TEST_FILE_TYPE);
                lstModel = new ListModelList(listRapidTestFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    /**
     * linhdx Ham tao doi tuong tu form de luu lai
     *
     * @return
     */
    private RtFile createRtFile() {
        if ("COPY".equals(crudMode)) {
            rtFile.setFileId(null);
            rtFile.setRtFileId(null);
            rtFile.setNswFileCode(null);
        }
        rtFile.setFileId(files.getFileId());
        if (documentTypeCode != null) {
            rtFile.setDocumentTypeCode(documentTypeCode);
            // rtFile.setStatusCode(Constants.RAPID_TEST_STATUS.NEW);
            if (!"UPDATE".equals(crudMode)) {
                String nswFileCode = getAutoNswFileCode(documentTypeCode);
                rtFile.setNswFileCode(nswFileCode);
            }
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI
                    .equals(documentTypeCode)) {
                rtFile.setRapidTestNo(tbRapidTestNo.getValue().trim());
                rtFile.setRapidTestName(tbRapidTestName.getValue().trim());
                rtFile.setRapidTestCode(tbRapidTestCode.getValue().trim());
                rtFile.setPlaceOfManufacture(tbPlaceOfManufacture.getValue()
                        .trim());
                rtFile.setOperatingPrinciples(tbOperatingPrinciples.getValue()
                        .trim());
                /*rtFile.setTargetTesting(tbTargetTesting.getValue().trim());
                 rtFile.setRangeOfApplications(tbRangeOfApplications.getValue()
                 .trim());
                 rtFile.setLimitDevelopment(tbLimitDevelopment.getValue().trim());
                 rtFile.setPrecision(tbPrecision.getValue().trim());*/
                rtFile.setDescription(tbDescription.getValue().trim());
                rtFile.setPakaging(tbPakaging.getValue().trim());
                rtFile.setStorageConditions(tbStorageConditions.getValue()
                        .trim());
                rtFile.setOtherInfos(tbOtherInfos.getValue().trim());
                rtFile.setAttachmentsInfo(tbAttachmentsInfo.getValue().trim());
                rtFile.setSignPlace(tbSignPlace.getValue().trim());
                rtFile.setSignName(tbSignName.getValue().trim());
                rtFile.setShelfLifeMonth(Long.valueOf(ibShelfLife.getValue()));
                rtFile.setSignDate(dbSignDate.getValue());
                String proTestStr = lboxPropertiesTests.getSelectedItem()
                        .getValue();
                rtFile.setPropertiesTests(Long.valueOf(proTestStr));
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG
                    .equals(documentTypeCode)) {
                rtFile.setRapidTestNo(t2bRapidTestChangeNo.getValue().trim());
                rtFile.setRapidTestName(t2bRapidTestName.getValue().trim());
                rtFile.setRapidTestCode(t2bRapidTestCode.getValue().trim());
                rtFile.setCirculatingRapidTestNo(t2bCirculatingRapidTestNo
                        .getValue().trim());
                rtFile.setContents(t2bContens.getValue().trim());
                rtFile.setAttachmentsInfo(t2bAttachmentsInfo.getValue().trim());
                rtFile.setSignPlace(t2bSignPlace.getValue().trim());
                rtFile.setSignName(t2bSignName.getValue().trim());

                rtFile.setDateIssue(d2bDateIssue.getValue());
                rtFile.setSignDate(d2bSignDate.getValue());
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN
                    .equals(documentTypeCode)) {
                rtFile.setCirculatingExtensionNo(t3bCirculatingExtensionNo
                        .getValue().trim());
                rtFile.setRapidTestName(t3bRapidTestName.getValue().trim());
                rtFile.setRapidTestCode(t3bRapidTestCode.getValue().trim());
                rtFile.setCirculatingRapidTestNo(t3bCirculatingRapidTestNo
                        .getValue().trim());
                String value = t3bExtensionNo.getValue().trim();
                if (!"".equals(value)) {
                    rtFile.setExtensionNo(Long.parseLong(value));
                }

                rtFile.setAttachmentsInfo(t3bAttachmentsInfo.getValue().trim());
                rtFile.setSignPlace(t3bSignPlace.getValue().trim());
                rtFile.setSignName(t3bSignName.getValue().trim());
                rtFile.setDateIssue(d3bDateIssue.getValue());
                rtFile.setDateEffect(d3bDateEffect.getValue());
                rtFile.setSignDate(d3bSignDate.getValue());
            }

        }

        return rtFile;
    }

    private Files createFile() throws Exception {
        Date dateNow = new Date();
        files.setCreateDate(dateNow);
        files.setModifyDate(dateNow);
        files.setCreatorId(getUserId());
        files.setCreatorName(getUserFullName());
        files.setCreateDeptId(getDeptId());
        files.setCreateDeptName(getDeptName());
        files.setIsActive(Constants.Status.ACTIVE);

        if ("CREATE".equals(crudMode)) {
            // viethd 29/01/2015
            // set status of file is INIT
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
            files.setFileType(FILE_TYPE);
            CategoryDAOHE che = new CategoryDAOHE();
            Category cat = che.findById(FILE_TYPE);
            if (cat != null) {
                String fileTypeName = cat.getName();
                files.setFileTypeName(fileTypeName);
            }
            List<Flow> flows = WorkflowAPI.getInstance().getFlowByDeptNObject(
                    DEPT_ID, FILE_TYPE);
            files.setFlowId(flows.get(0).getFlowId());
        }
        if ("COPY".equals(crudMode)) {
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
        }
        return files;
    }

    private Files createFileUpdate() throws Exception {
        Date dateNow = new Date();
        files.setModifyDate(dateNow);
        return files;
    }

    private RtTargetTesting createRtTargetTesting(
            RtTargetTesting createRtTargetTesting) {
        Date dateNow = new Date();
        createRtTargetTesting.setId(null);
        createRtTargetTesting.setCreatedBy(getUserId());
        createRtTargetTesting.setCreatedDate(dateNow);
        createRtTargetTesting.setIsActive(Constants.Status.ACTIVE);
        createRtTargetTesting.setName(createRtTargetTesting.getName());
        createRtTargetTesting.setRangeOfApplications(createRtTargetTesting
                .getRangeOfApplications());
        createRtTargetTesting.setLimitDevelopment(createRtTargetTesting
                .getLimitDevelopment());
        createRtTargetTesting
                .setPrecision(createRtTargetTesting.getPrecision());
        createRtTargetTesting.setFileId(rtFile.getFileId());
        return createRtTargetTesting;
    }

    private RtTargetTesting deleteRtTargetTesting(
            RtTargetTesting createRtTargetTesting) {
        Date dateNow = new Date();
        createRtTargetTesting.setCreatedBy(getUserId());
        createRtTargetTesting.setCreatedDate(dateNow);
        createRtTargetTesting.setIsActive(Constants.Status.INACTIVE);
        return createRtTargetTesting;
    }

    private void updateRtTargetTesting() {
        boolean isCommit = false;
        RtTargetTestingDAO obj = new RtTargetTestingDAO();
        if (rtTargetTestingMode.size() > 0) {
            for (RtTargetTesting li : rtTargetTestingMode) {
                if ("COPY".equals(crudMode)) {
                    li.setId(null);
                    li.setFileId(null);
                }

                if (li.getId() == null) {
                    obj.saveOrUpdateNotCommit(createRtTargetTesting(li));
                } else {
                    if (checkRtTargetTesting(li)) {
                        long idTemp = li.getId();
                        obj.saveOrUpdateNotCommit(createRtTargetTesting(li));
                        rtDeleteTargetTestingMode.add(obj.findById(idTemp));
                    }
                }
            }
            isCommit = true;
        }

        if (rtDeleteTargetTestingMode.size() > 0) {
            for (RtTargetTesting li : rtDeleteTargetTestingMode) {
                obj.saveOrUpdateNotCommit(deleteRtTargetTesting(li));
            }
            isCommit = true;
        }

        if (isCommit) {
            obj.doCommit();
        }
    }

    private boolean checkRtTargetTesting(RtTargetTesting item) {
        RtTargetTestingDAO obj = new RtTargetTestingDAO();
        RtTargetTesting inDb = obj.findById(item.getId());
        if ((!item.getName().equals(inDb.getName()))
                || (!item.getLimitDevelopment().equals(inDb.getLimitDevelopment()))
                || (!item.getPrecision().equals(inDb.getPrecision()))
                || (!item.getRangeOfApplications().equals(inDb.getRangeOfApplications()))) {
            return true;
        }
        return false;
    }

    public String getAutoNswFileCode(Long documentTypeCode) {
        CategorySearchForm form = new CategorySearchForm();
        form.setType(Constants.RAPID_TEST.TYPE_CONFIG);
        form.setCode(Constants.RAPID_TEST.CODE_CONFIG);
        CategoryDAOHE obj = new CategoryDAOHE();
        List<Category> lstCategory = obj.findCategory(form);
        String autoNumber = "000001";
        if (lstCategory != null && lstCategory.size() > 0) {
            Category ca = lstCategory.get(0);
            autoNumber = ca.getValue();
            ca.setValue(String.valueOf(Long.valueOf(autoNumber) + 1));
            obj.saveOrUpdate(ca);// Tang chi so len
        }
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

    private boolean isValidatedData() {

        if (documentTypeCode != null) {

            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI
                    .equals(documentTypeCode)) {
                if (tbRapidTestName.getText().matches("\\s*")) {
                    showWarningMessage("Tên bộ xét nghiệm nhanh không được để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbRapidTestName.focus();
                    return false;
                }
                if (tbRapidTestCode.getText().matches("\\s*")) {
                    showWarningMessage("Ký hiệu (Mã hiệu) hồ sơ không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbRapidTestCode.focus();
                    return false;
                }

                if (tbPlaceOfManufacture.getText().matches("\\s*")) {
                    showWarningMessage("Nơi sản xuất không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbPlaceOfManufacture.focus();
                    return false;
                }

                if (lboxPropertiesTests.getSelectedItem() == null
                        || Constants.COMBOBOX_HEADER_VALUE_STRING
                        .equals((String) lboxPropertiesTests
                                .getSelectedItem().getValue())) {
                    showWarningMessage("Tính chất xét nghiệm không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    lboxPropertiesTests.focus();
                    return false;
                }

                if (tbOperatingPrinciples.getText().matches("\\s*")) {
                    showWarningMessage("Nguyên lý hoạt động không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbOperatingPrinciples.focus();
                    return false;
                }
                if (lbListOfTargetTesting == null) {
                    showWarningMessage("Yêu cầu nhập thông tin chỉ tiêu xét nghiệm");
                    tb.setSelectedPanel(mainpanel);
                    lbListOfTargetTesting.focus();
                    return false;
                }

                if (tbDescription.getText().matches("\\s*")) {
                    showWarningMessage("Mô tả cấu tạo bộ xét nghiệm nhanh không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbDescription.focus();
                    return false;
                }
                if (tbPakaging.getText().matches("\\s*")) {
                    showWarningMessage("Quy cách đóng gói không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbPakaging.focus();
                    return false;
                }
                if (ibShelfLife.getText().matches("\\s*")) {
                    showWarningMessage("Thời hạn sử dụng không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    ibShelfLife.focus();
                    return false;
                }
                if (tbStorageConditions.getText().matches("\\s*")) {
                    showWarningMessage("Điều kiện bảo quản không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbStorageConditions.focus();
                    return false;
                }
                if (tbSignName.getText().matches("\\s*")) {
                    showWarningMessage("Người ký không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    tbSignName.focus();
                    return false;
                }

            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG
                    .equals(documentTypeCode)) {
                if (t2bRapidTestName.getText().matches("\\s*")) {
                    showWarningMessage("Tên bộ xét nghiệm nhanh không được để trống");
                    tb.setSelectedPanel(mainpanel);
                    t2bRapidTestName.focus();
                    return false;
                }
                if (t2bRapidTestCode.getText().matches("\\s*")) {
                    showWarningMessage("Ký hiệu(Mã hiệu) không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    t2bRapidTestCode.focus();
                    return false;
                }

                if (t2bCirculatingRapidTestNo.getText().matches("\\s*")) {
                    showWarningMessage("Số đăng ký lưu hành đã cấp không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    t2bCirculatingRapidTestNo.focus();
                    return false;
                }

                if (d2bDateIssue.getValue() == null) {
                    showWarningMessage("Ngày cấp không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    d2bDateIssue.focus();
                    return false;
                }

                if (t2bContens.getText().matches("\\s*")) {
                    showWarningMessage("Nội dung đề nghị thay đổi không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    t2bContens.focus();
                    return false;
                }
                if (t2bSignName.getText().matches("\\s*")) {
                    showWarningMessage("Người ký không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    t2bSignName.focus();
                    return false;
                }
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN
                    .equals(documentTypeCode)) {
                if (t3bRapidTestName.getText().matches("\\s*")) {
                    showWarningMessage("Tên bộ xét nghiệm nhanh không được để trống");
                    tb.setSelectedPanel(mainpanel);
                    t3bRapidTestName.focus();
                    return false;
                }
                if (t3bRapidTestCode.getText().matches("\\s*")) {
                    showWarningMessage("Ký hiệu(Mã hiệu) không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    t3bRapidTestCode.focus();
                    return false;
                }
                if (t3bCirculatingRapidTestNo.getText().matches("\\s*")) {
                    showWarningMessage("Số đăng ký lưu hành đã cấp không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    t3bCirculatingRapidTestNo.focus();
                    return false;
                }
                if (d3bDateIssue.getValue() == null) {
                    showWarningMessage("Ngày cấp không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    d3bDateIssue.focus();
                    return false;
                }
                if (d3bDateEffect.getValue() == null) {
                    showWarningMessage("Có hiệu lực đến không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    d3bDateEffect.focus();
                    return false;
                }
                if (t3bExtensionNo.getText().matches("\\s*")) {
                    showWarningMessage("Xin gia hạn lần thứ không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    t3bExtensionNo.focus();
                    return false;
                }
                if (t3bSignName.getText().matches("\\s*")) {
                    showWarningMessage("Người ký không thể để trống");
                    tb.setSelectedPanel(mainpanel);
                    t3bSignName.focus();
                    return false;
                }
            }

        }
        return true;
    }

    public boolean validateData() {
        if (documentTypeCode != null) {

            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI
                    .equals(documentTypeCode)) {
                String msg;
                if ((msg = ValidatorUtil.validateTextbox(tbRapidTestNo, false,
                        20)) != null) {
                    showWarningMessage(String.format(msg, "Số/Ký hiệu"));
                    tb.setSelectedPanel(mainpanel);
                    tbRapidTestNo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbRapidTestName, true,
                        250)) != null) {
                    showWarningMessage(String.format(msg, "Tên bộ xét nghiệm"));
                    tb.setSelectedPanel(mainpanel);
                    tbRapidTestName.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbRapidTestCode, true,
                        250)) != null) {
                    showWarningMessage(String.format(msg, "Ký hiệu(Mã hiệu)"));
                    tb.setSelectedPanel(mainpanel);
                    tbRapidTestCode.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbPlaceOfManufacture,
                        true, 250)) != null) {
                    showWarningMessage(String.format(msg, "Nơi sản xuất"));
                    tb.setSelectedPanel(mainpanel);
                    tbPlaceOfManufacture.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbOperatingPrinciples,
                        true, 2000)) != null) {
                    showWarningMessage(String
                            .format(msg, "Nguyên lý hoạt động"));
                    tb.setSelectedPanel(mainpanel);
                    tbOperatingPrinciples.focus();
                    return false;
                }
                if (lbListOfTargetTesting == null) {
                    showWarningMessage(String.format(msg,
                            "Yêu cầu nhập thông tin chỉ tiêu xét nghiệm"));
                    tb.setSelectedPanel(mainpanel);
                    lbListOfTargetTesting.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbDescription, true,
                        250)) != null) {
                    showWarningMessage(String.format(msg,
                            "Mô tả cấu tạo bộ xét nghiệm nhanh"));
                    tb.setSelectedPanel(mainpanel);
                    tbDescription.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbPakaging, true, 250)) != null) {
                    showWarningMessage(String.format(msg, "Quy cách đóng gói"));
                    tb.setSelectedPanel(mainpanel);
                    tbPakaging.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbStorageConditions,
                        true, 250)) != null) {
                    showWarningMessage(String.format(msg, "Điều kiện bảo quản"));
                    tb.setSelectedPanel(mainpanel);
                    tbStorageConditions.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbOtherInfos, false,
                        500)) != null) {
                    showWarningMessage(String.format(msg,
                            "Các thông tin khác (nếu có)"));
                    tb.setSelectedPanel(mainpanel);
                    tbOtherInfos.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbAttachmentsInfo,
                        false, 250)) != null) {
                    showWarningMessage(String.format(msg, "Hồ sơ kèm theo"));
                    tb.setSelectedPanel(mainpanel);
                    tbAttachmentsInfo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil
                        .validateTextbox(tbSignPlace, false, 12)) != null) {
                    showWarningMessage(String.format(msg, "Nơi ký"));
                    tb.setSelectedPanel(mainpanel);
                    tbSignPlace.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbSignName, true, 100)) != null) {
                    showWarningMessage(String.format(msg, "Người ký"));
                    tb.setSelectedPanel(mainpanel);
                    tbSignName.focus();
                    return false;
                }
                if (ibShelfLife.getValue() <= 0) {
                    showWarningMessage("Vui lòng nhập số dương");
                    tb.setSelectedPanel(mainpanel);
                    ibShelfLife.focus();
                    return false;
                }
            }
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG
                    .equals(documentTypeCode)) {
                String msg;
                if ((msg = ValidatorUtil.validateTextbox(t2bRapidTestChangeNo,
                        false, 20)) != null) {
                    showWarningMessage(String.format(msg, "Số/Ký hiệu"));
                    tb.setSelectedPanel(mainpanel);
                    t2bRapidTestChangeNo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t2bRapidTestName,
                        true, 250)) != null) {
                    showWarningMessage(String.format(msg, "Tên bộ xét nghiệm"));
                    tb.setSelectedPanel(mainpanel);
                    t2bRapidTestName.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t2bRapidTestCode,
                        true, 250)) != null) {
                    showWarningMessage(String.format(msg, "Ký hiệu(Mã hiệu)"));
                    tb.setSelectedPanel(mainpanel);
                    t2bRapidTestCode.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(
                        t2bCirculatingRapidTestNo, true, 50)) != null) {
                    showWarningMessage(String.format(msg,
                            "Số đăng ký lưu hành đã cấp"));
                    tb.setSelectedPanel(mainpanel);
                    t2bCirculatingRapidTestNo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t2bContens, true, 500)) != null) {
                    showWarningMessage(String.format(msg,
                            "Nội dung đề nghị thay đổi"));
                    tb.setSelectedPanel(mainpanel);
                    t2bContens.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t2bAttachmentsInfo,
                        false, 250)) != null) {
                    showWarningMessage(String.format(msg, "Hồ sơ kèm theo"));
                    tb.setSelectedPanel(mainpanel);
                    t2bAttachmentsInfo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t2bSignPlace, false,
                        12)) != null) {
                    showWarningMessage(String.format(msg, "Nơi ký"));
                    tb.setSelectedPanel(mainpanel);
                    t2bSignPlace.focus();
                    return false;
                }
                if ((msg = ValidatorUtil
                        .validateTextbox(t2bSignName, true, 100)) != null) {
                    showWarningMessage(String.format(msg, "Người ký"));
                    tb.setSelectedPanel(mainpanel);
                    t2bSignName.focus();
                    return false;
                }
            }
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN
                    .equals(documentTypeCode)) {
                String msg;
                if ((msg = ValidatorUtil.validateTextbox(
                        t3bCirculatingExtensionNo, false, 20)) != null) {
                    showWarningMessage(String.format(msg, "Số/Ký hiệu"));
                    tb.setSelectedPanel(mainpanel);
                    t3bCirculatingExtensionNo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t3bRapidTestName,
                        true, 250)) != null) {
                    showWarningMessage(String.format(msg, "Tên bộ xét nghiệm"));
                    tb.setSelectedPanel(mainpanel);
                    t3bRapidTestName.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t3bRapidTestCode,
                        true, 250)) != null) {
                    showWarningMessage(String.format(msg, "Ký hiệu(Mã hiệu)"));
                    tb.setSelectedPanel(mainpanel);
                    t3bRapidTestCode.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(
                        t3bCirculatingRapidTestNo, true, 50)) != null) {
                    showWarningMessage(String.format(msg,
                            "Số đăng ký lưu hành đã cấp"));
                    tb.setSelectedPanel(mainpanel);
                    t3bCirculatingRapidTestNo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t3bExtensionNo, true,
                        2, "^[0-9]+[0-9\\.]*$")) != null) {
                    showWarningMessage(String
                            .format(msg, "Xin gia hạn lần thứ"));
                    tb.setSelectedPanel(mainpanel);
                    t3bExtensionNo.focus();
                    return false;
                }
                if (Long.parseLong(t3bExtensionNo.getValue().trim()) <= 0) {
                    showWarningMessage("Thông tin gia hạn lần thứ phải > 0");
                    tb.setSelectedPanel(mainpanel);
                    t3bExtensionNo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t3bAttachmentsInfo,
                        false, 250)) != null) {
                    showWarningMessage(String.format(msg,
                            "Hồ sơ xin gia hạn bao gồm"));
                    tb.setSelectedPanel(mainpanel);
                    t3bAttachmentsInfo.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(t3bSignPlace, false,
                        12)) != null) {
                    showWarningMessage(String.format(msg, "Nơi ký"));
                    tb.setSelectedPanel(mainpanel);
                    t3bSignPlace.focus();
                    return false;
                }
                if ((msg = ValidatorUtil
                        .validateTextbox(t3bSignName, true, 100)) != null) {
                    showWarningMessage(String.format(msg, "Người ký"));
                    tb.setSelectedPanel(mainpanel);
                    t3bSignName.focus();
                    return false;
                }
                if (d3bDateIssue.getValue().after(d3bDateEffect.getValue())) {
                    showWarningMessage("Ngày có hiệu lực không được sớm hơn ngày cấp");
                    tb.setSelectedPanel(mainpanel);
                    d3bDateEffect.focus();
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validateTargetTestingData() {
        if (documentTypeCode != null) {

            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI
                    .equals(documentTypeCode)) {
                String msg;
                if ((msg = ValidatorUtil.validateTextbox(tbTargetTesting, true,
                        500)) != null) {
                    showWarningMessage(String
                            .format(msg, "Chỉ tiêu xét nghiệm"));
                    tb.setSelectedPanel(mainpanel);
                    tbTargetTesting.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbRangeOfApplications,
                        true, 250)) != null) {
                    showWarningMessage(String.format(msg, "Phạm vi ứng dụng"));
                    tb.setSelectedPanel(mainpanel);
                    tbRangeOfApplications.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbLimitDevelopment,
                        true, 50)) != null) {
                    showWarningMessage(String.format(msg, "Giới hạn phát hiện"));
                    tb.setSelectedPanel(mainpanel);
                    tbLimitDevelopment.focus();
                    return false;
                }
                if ((msg = ValidatorUtil.validateTextbox(tbPrecision, false, 50)) != null) {
                    showWarningMessage(String.format(msg,
                            "Độ chính xác (hoặc sai số)"));
                    tb.setSelectedPanel(mainpanel);
                    tbPrecision.focus();
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isValidatedFile() {
        Long valueFileType = Long.valueOf((String) lbRapidTestFileType
                .getSelectedItem().getValue());
        Long headerValue = (Long) Constants.COMBOBOX_HEADER_VALUE;
        if (lbRapidTestFileType.getSelectedItem() == null
                || headerValue.equals(valueFileType)) {
            showWarningMessage("Loại tệp đính kèm không thể để trống");
            lbRapidTestFileType.focus();
            return false;
        }

        if (listMedia == null || listMedia.isEmpty()) {
            showWarningMessage("Chưa chọn đính kèm");
            return false;
        }
        if (listMedia.size() > 1) {
            showWarningMessage("Chỉ được chọn 1 file đính kèm");
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSave(SAVE);
                break;
            case KeyEvent.F7:
                onSave(SAVE_CLOSE);
                break;
            case KeyEvent.F8:
                onSave(SAVE_COPY);
                break;
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave(int typeSave) throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }

        try {
            if (null != crudMode) {
                if (validateData()) {
                    // Save to
                    createObject();
                    VFeeProcedureDAOHE vFeeFileDAOHE = new VFeeProcedureDAOHE();
                    List<VFeeProcedure> lstVFeeProcedure = vFeeFileDAOHE
                            .getListFee(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
                    switch (crudMode) {
                        case "CREATE": {
                            Long fileId = rtFile.getFileId();
                            if (lstVFeeProcedure.size() > 0) {
                                PaymentInfoDAO feePaymentInfoDAOHE = new PaymentInfoDAO();
                                List lst = feePaymentInfoDAOHE
                                        .getListPayment(fileId);
                                if (lst.isEmpty()) {
                                    // Neu chua co ban ghi thanh toan thi luu lai
                                    for (VFeeProcedure obj : lstVFeeProcedure) {

                                        PaymentInfo feePaymentInfo = new PaymentInfo();
                                        feePaymentInfo.setFileId(fileId);
                                        feePaymentInfo.setFeeId(obj.getFeeId());
                                        feePaymentInfo.setFeeName(obj.getName());
                                        feePaymentInfo
                                                .setIsActive(Constants.Status.ACTIVE);
                                        feePaymentInfo
                                                .setStatus(Constants.RAPID_TEST.PAYMENT.PAY_NEW);
                                        feePaymentInfo.setPhase(obj.getPhase());
                                        String value = obj.getCost();
                                        Long valueL = 0L;
                                        try {
                                            valueL = Long.valueOf(value);
                                        } catch (Exception ex) {
                                            LogUtils.addLogDB(ex);
                                        }
                                        feePaymentInfo.setCost(valueL);
                                        feePaymentInfoDAOHE
                                                .saveOrUpdate(feePaymentInfo);

                                    }
                                }

                            }
                            //resetControl();
                            showWarningMessage("");
                            showNotification(String.format(
                                    Constants.Notification.SAVE_SUCCESS,
                                    Constants.DOCUMENT_TYPE_NAME.FILE),
                                    Constants.Notification.INFO);
                            break;
                        }
                        case "UPDATE":
                            createFileUpdate();
                            showWarningMessage("");
                            createPayment();
                            showNotification(String.format(
                                    Constants.Notification.UPDATE_SUCCESS,
                                    Constants.DOCUMENT_TYPE_NAME.FILE),
                                    Constants.Notification.INFO);
                            break;
                        case "COPY":
                            if ("COPY".equals(crudMode)) {
                                crudMode = "UPDATE";
                            }
                            //Reset lai cac gia tri moi
                            originalFilesId = files.getFileId();
                            originalFileType = files.getFileType();
                            originalRtFileId = rtFile.getRtFileId();
                            showWarningMessage("");
                            createPayment();
                            showNotification(String.format(
                                    Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                                    Constants.Notification.INFO);

                    }
                }
            }

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private void createPayment() {
        VFeeProcedureDAOHE vFeeFileDAOHE = new VFeeProcedureDAOHE();
        List<VFeeProcedure> lstVFeeProcedure = vFeeFileDAOHE
                .getListFee(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
        Long fileId = files.getFileId();
        if (lstVFeeProcedure.size() > 0) {
            PaymentInfoDAO paymentInfoDAOHE = new PaymentInfoDAO();
            List lst = paymentInfoDAOHE.getListPayment(fileId);
            if (lst.isEmpty()) {
                // Neu chua co ban ghi thanh toan thi luu lai
                for (VFeeProcedure obj : lstVFeeProcedure) {

                    PaymentInfo paymentInfo = new PaymentInfo();
                    paymentInfo.setFileId(fileId);
                    paymentInfo.setFeeId(obj.getFeeId());
                    paymentInfo.setFeeName(obj.getName());
                    paymentInfo.setIsActive(Constants.Status.ACTIVE);
                    paymentInfo.setStatus(ConstantsPayment.PAYMENT.PAY_NEW);
                    paymentInfo.setPhase(obj.getPhase());
                    String value = obj.getCost();
                    Long valueL = 0L;
                    try {
                        valueL = Long.valueOf(value);
                    } catch (Exception ex) {
                        LogUtils.addLogDB(ex);
                    }
                    paymentInfo.setCost(valueL);
                    paymentInfoDAOHE.saveOrUpdate(paymentInfo);

                }
            }

        }
    }

    private void createObject() throws Exception {
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        files = createFile();
        filesDAOHE.saveOrUpdate(files);
        RapidTestDAOHE rtFileDAOHE = new RapidTestDAOHE();
        rtFile = createRtFile();
        rtFileDAOHE.saveOrUpdate(rtFile);
        updateRtTargetTesting();
        crudMode = "UPDATE";
    }

    @Listen("onClick=#btnCreate")
    public void onCreateRapidTestFileType() throws IOException, Exception {

        if (listMedia.isEmpty()) {
            showNotification("Chọn tệp tải lên trước khi thêm mới!",
                    Constants.Notification.WARNING);

            return;
        }

        if (lbRapidTestFileType.getSelectedIndex() < 1) {
            showNotification("Chưa chọn loại tệp đính kèm!",
                    Constants.Notification.WARNING);
            lbRapidTestFileType.focus();
            return;
        }

        if (saveObject() == false) {
            return;
        }
        Long rtFileFileType = Long.valueOf((String) lbRapidTestFileType
                .getSelectedItem().getValue());
        AttachDAO base = new AttachDAO();

        for (Media media : listMedia) {
            base.saveFileAttach(media, files.getFileId(),
                    Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE, rtFileFileType);
        }

        fillFileListbox(files.getFileId());
        if ((listMedia != null) && (listMedia.size() > 0)) {
            listMedia.clear();
        }
        flist.getChildren().clear();
    }

    @Listen("onClick=#btnCreateProFile")
    public void onCreateProFile() throws IOException, Exception {
        if (lbRapidTestFileType.getSelectedItem() == null || "-1".equals(lbRapidTestFileType.getSelectedItem().getValue().toString())) {
            showNotification("Chọn loại tệp trước khi thêm từ hồ sơ dùng chung !!!");
            return;
        }
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", windowCRUDRapidTest);
        Window window = (Window) Executions.createComponents(
                "/Pages/rapidTest/PublicFileManageList.zul", null, arguments);
        window.doModal();
    }

    private void fillFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttach> lstRapidTestAttach = rDaoHe
                .findRapidTestAttach(fileId);
        this.fileListbox.setModel(new ListModelArray(lstRapidTestAttach));
        // onSelect();//An hien form
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long fileId = obj.getObjectId();
        RapidTestAttachDAOHE rDAOHE = new RapidTestAttachDAOHE();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(fileId);
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);

    }

    /**
     * Xu ly su kien upload file
     *
     * @param event
     */
    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                	String sExt = ResourceBundleUtil.getString("extend_file", "config");
	                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("z-valign-left");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listMedia.remove(media);
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    /**
     * Dong cua so khi o dang popup
     */
    @Listen("onClose = #windowCRUDRapidTest")
    public void onClose() {
        try {
            if (windowCRUDRapidTest != null) {
                windowCRUDRapidTest.detach();
            }
            Events.sendEvent("onVisible", parentWindow, null);
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
    }

    @Listen("onVisible = #windowCRUDRapidTest")
    public void onVisible() {
        windowCRUDRapidTest.setVisible(true);
    }

    // su kien chon tu ho so dung chung
    @Listen("onChooseProFile = #windowCRUDRapidTest")
    public void onChooseProFile(Event event) throws IOException, Exception {
        if (saveObject() == false) {
            return;
        }
        Map<String, Object> args = (Map<String, Object>) event.getData();
        List list = (List) args.get("documentReceiveProcess");
        if (list == null || list.isEmpty()) {
            return;
        }
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        for (int idx = 0; idx < list.size(); idx++) {
            AttachCategoryModel Process = (AttachCategoryModel) list.get(idx);
            Attachs attach = new Attachs();
            attach.setAttachPath(Process.getAttach().getAttachPath());
            String fileName = (Process.getAttach().getAttachName());
            attach.setAttachName(fileName);
            attach.setIsActive(Constants.Status.ACTIVE);
            attach.setObjectId(files.getFileId());
            attach.setCreatorId(getUserId());
            attach.setCreatorName(Process.getAttach().getCreatorName());
            attach.setDateCreate(new Date());
            attach.setModifierId(getUserId());
            attach.setDateModify(new Date());
            attach.setAttachCat(Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
            attach.setAttachType(Long.parseLong(lbRapidTestFileType.getSelectedItem().getValue().toString()));
            attach.setAttachTypeName(lbRapidTestFileType.getSelectedItem().getLabel());

            attach.setAttachDes(Process.getAttach().getAttachDes());
            attachDAOHE.saveOrUpdate(attach);
        }
        fillFileListbox(files.getFileId());

    }

    public ListModel getModel() {
        return model;
    }

    public void setModel(ListModel model) {
        this.model = model;
    }

    public RtFile getRtFile() {
        return rtFile;
    }

    public void setRtFile(RtFile rtFile) {
        this.rtFile = rtFile;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public String getIsSave() {
        return isSave;
    }

    public void setIsSave(String isSave) {
        this.isSave = isSave;
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public boolean saveObject() throws IOException, Exception {
        // neu chua co fileID thi them moi
        if (files.getFileId() == null) {
            if (!isValidatedData()) {
                return false;
            }
            // linhdx
            // Luu thong tin ho so truoc khi tao
            createObject();
        }
        return true;
    }

    public int checkViewProcess() {
        return checkViewProcess(files.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; // Khong hien thi
        }
        return Constants.CHECK_VIEW.VIEW;
    }

    public int checkDispathSDBS() {
        return checkDispathSDBS(files.getFileId());
    }

    public int checkDispathSDBS(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; // Khong hien thi
        }
        RapidTestDAOHE rtDao = new RapidTestDAOHE();
        int check = rtDao.checkDispathRT(fileId, getUserId());
        return check;

    }

    public int checkBooked() {
        return checkBooked(files.getFileId(), files.getFileType());
    }

    public int checkBooked(Long fileId, Long fileType) {
        if (fileId == null || fileType == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; // Khong hien thi
        }
        BookDocumentDAOHE dao = new BookDocumentDAOHE();
        int check = dao.checkBookedRapidTest(fileId, fileType);
        return check;
    }

    // them moi ho so tu file excel
    /**
     * Xu ly su kien upload file
     *
     * @param event
     */
    @Listen("onUpload = #btnImportExcel")
    public void onUploadExcel(UploadEvent event)
            throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        if ((listFileExcel != null) && (listFileExcel.size() > 0)) {
            showNotification("Chỉ được phép chọn 1 file import!",
                    Constants.Notification.WARNING);
            return;
        }
        listFileExcel = new ArrayList<Media>();
        final Media[] medias = event.getMedias();
        if (medias.length > 1) {
            showNotification("Chỉ được phép chọn 1 file import!",
                    Constants.Notification.WARNING);
            return;
        }

        for (final Media media : medias) {
            String extFile = media.getFormat();
            if (!("xlsx".equals(extFile.toLowerCase()) || "xls".equals(extFile
                    .toLowerCase()))) {
                showNotification("Định dạng file không được phép tải lên",
                        Constants.Notification.WARNING);
                continue;
            }

            // luu file vao danh sach file
            listFileExcel.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("z-valign-left");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listFileExcel.remove(media);
                        }
                    });
            hl.appendChild(rm);
            fListImportExcel.appendChild(hl);
        }
    }

    @Listen("onDownloadFile = #lbDownloadExcelTemplate")
    public void onDownloadFile() throws FileNotFoundException {
        String folderPath = Executions.getCurrent().getDesktop().getWebApp()
                .getRealPath(Constants.UPLOAD.ATTACH_PATH);
        String path = folderPath + "\\BM_Xet_nghiem_nhanh.xlsx";
        File f = new File(path);
//        if (f != null) {
            if (f.exists()) {
                File tempFile = FileUtil.createTempFile(f, f.getName());
                Filedownload.save(tempFile, path);
            } else {
                Clients.showNotification(
                        "File không còn tồn tại trên hệ thống!",
                        Constants.Notification.INFO, null, "middle_center",
                        1500);
            }
//        } else {
//            Clients.showNotification("File không còn tồn tại trên hệ thống!",
//                    Constants.Notification.INFO, null, "middle_center", 1500);
//        }
    }

    @Listen("onClick=#btnCreateFromImport")
    public void onCreateFromImportExcel(Event event) throws IOException,
            Exception {
        if (listFileExcel == null || listFileExcel.isEmpty()) {
            lbImportExcelWarning
                    .setValue("Chọn tệp tải lên trước khi thêm mới!");
            return;
        }
        lbImportExcelWarning.setValue(null);
        for (Media media : listFileExcel) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");
            String fileName = media.getName();
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            folderPath += separator + dateFormat.format(date);
            File fd = new File(folderPath);
            if (!fd.exists()) {
                // tạo forlder
                fd.mkdirs();
            }
            File f = new File(folderPath + separator + fileName);
            if (f.exists()) {
            } else {
                f.createNewFile();
            }
            // save to hard disk
            InputStream inputStream;
            OutputStream outputStream;
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(f));
            resetControl();
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI
                    .equals(documentTypeCode)) {
                // thong tin ve ho so xet nghiem nhanh
                XSSFSheet sheet = wb.getSheetAt(0);
                XSSFSheet sheetTargetTesting = wb.getSheetAt(3);
                tbRapidTestNo.setValue(sheet.getRow(1).getCell(1).toString()
                        .trim());
                tbRapidTestName.setValue(sheet.getRow(1).getCell(3).toString()
                        .trim());
                tbRapidTestCode.setValue(sheet.getRow(2).getCell(1).toString()
                        .trim());
                tbPlaceOfManufacture.setValue(sheet.getRow(2).getCell(3)
                        .toString().trim());
                for (int i = 0; i < lboxPropertiesTests.getItemCount(); i++) {
                    if (lboxPropertiesTests
                            .getItemAtIndex(i)
                            .getLabel()
                            .equals(sheet.getRow(3).getCell(1).toString()
                                    .trim())) {
                        lboxPropertiesTests.setSelectedIndex(i);
                        break;
                    }
                }
                tbOperatingPrinciples.setValue(sheet.getRow(3).getCell(3)
                        .toString().trim());
                for (int i = 2; i < Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_ROW_INDEX; i++) {
                    if ((!"".equals(sheetTargetTesting.getRow(i).getCell(Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_NAME_COL).toString().trim()))
                            && (!"".equals(sheetTargetTesting.getRow(i).getCell(Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_RANGE_COL).toString().trim()))
                            && (!"".equals(sheetTargetTesting.getRow(i).getCell(Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_LIMIT_COL).toString().trim()))
                            && (!"".equals(sheetTargetTesting.getRow(i).getCell(Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_PRECISION_COL).toString().trim()))) {
                        RtTargetTesting rtImport = new RtTargetTesting();
                        rtImport.setName(sheetTargetTesting.getRow(i).getCell(Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_NAME_COL).toString().trim());
                        rtImport.setRangeOfApplications(sheetTargetTesting.getRow(i).getCell(Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_RANGE_COL).toString().trim());
                        rtImport.setLimitDevelopment(sheetTargetTesting.getRow(i).getCell(Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_LIMIT_COL).toString().trim());
                        rtImport.setPrecision(sheetTargetTesting.getRow(i).getCell(Constants.IMPORT_EXCEL.RAPID_TEST_TARGET_TESTING_PRECISION_COL).toString().trim());
                        rtTargetTestingMode.add(rtImport);
                    }
                }
                if (rtTargetTestingMode.size() > 0) {
                    lbListOfTargetTesting.setModel(rtTargetTestingMode);
                    lbListOfTargetTesting.renderAll();
                }
                /*tbTargetTesting.setValue(sheet.getRow(4).getCell(1).toString()
                 .trim());
                 tbRangeOfApplications.setValue(sheet.getRow(4).getCell(3)
                 .toString().trim());
                 tbLimitDevelopment.setValue(sheet.getRow(5).getCell(1)
                 .toString().trim());
                 tbPrecision.setValue(sheet.getRow(5).getCell(3).toString()
                 .trim());*/
                tbDescription.setValue(sheet.getRow(5).getCell(1).toString()
                        .trim());
                tbPakaging.setValue(sheet.getRow(5).getCell(3).toString()
                        .trim());
                if (!"".equals(sheet.getRow(6).getCell(1).toString())) {
                    int value = (int) Double.parseDouble(sheet.getRow(6)
                            .getCell(1).toString().trim());
                    ibShelfLife.setValue(value);
                } else {
                    ibShelfLife.setValue(0);
                }

                tbStorageConditions.setValue(sheet.getRow(6).getCell(3)
                        .toString().trim());
                tbOtherInfos.setValue(sheet.getRow(7).getCell(1).toString()
                        .trim());
                tbAttachmentsInfo.setValue(sheet.getRow(7).getCell(3)
                        .toString().trim());
                tbSignPlace.setValue(sheet.getRow(9).getCell(1).toString()
                        .trim());
                if (!"".equals(sheet.getRow(9).getCell(3).toString())) {
                    dbSignDate.setValue(new Date(sheet.getRow(9).getCell(3)
                            .toString()));
                }
                tbSignName.setValue(sheet.getRow(10).getCell(1).toString()
                        .trim());
            }

            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG
                    .equals(documentTypeCode)) {
                // thong tin ve ho so bo sung xet nghiem nhanh
                XSSFSheet sheet = wb.getSheetAt(1);
                t2bRapidTestChangeNo.setValue(sheet.getRow(1).getCell(1)
                        .toString().trim());
                t2bRapidTestName.setValue(sheet.getRow(1).getCell(3).toString()
                        .trim());
                t2bRapidTestCode.setValue(sheet.getRow(2).getCell(1).toString()
                        .trim());

                t2bCirculatingRapidTestNo.setValue(sheet.getRow(3).getCell(1)
                        .toString().trim());
                if (!"".equals(sheet.getRow(3).getCell(3).toString())) {
                    d2bDateIssue.setValue(new Date(sheet.getRow(3).getCell(3)
                            .toString()));
                }
                t2bContens.setValue(sheet.getRow(4).getCell(1).toString()
                        .trim());
                t2bAttachmentsInfo.setValue(sheet.getRow(4).getCell(3)
                        .toString().trim());
                t2bSignPlace.setValue(sheet.getRow(6).getCell(1).toString()
                        .trim());
                if (!"".equals(sheet.getRow(6).getCell(3).toString())) {
                    d2bSignDate.setValue(new Date(sheet.getRow(6).getCell(3)
                            .toString()));
                }
                t2bSignName.setValue(sheet.getRow(7).getCell(1).toString()
                        .trim());
            }

            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN
                    .equals(documentTypeCode)) {
                // thong tin ve ho so gia han xet nghiem nhanh
                XSSFSheet sheet = wb.getSheetAt(2);
                t3bCirculatingExtensionNo.setValue(sheet.getRow(1).getCell(1)
                        .toString().trim());
                t3bRapidTestName.setValue(sheet.getRow(1).getCell(3).toString()
                        .trim());
                t3bRapidTestCode.setValue(sheet.getRow(2).getCell(1).toString()
                        .trim());

                t3bCirculatingRapidTestNo.setValue(sheet.getRow(3).getCell(1)
                        .toString().trim());
                if (!"".equals(sheet.getRow(3).getCell(3).toString())) {
                    d3bDateIssue.setValue(new Date(sheet.getRow(3).getCell(3)
                            .toString().trim()));
                }
                if (!"".equals(sheet.getRow(4).getCell(1).toString())) {
                    d3bDateEffect.setValue(new Date(sheet.getRow(4).getCell(1)
                            .toString()));
                }
                if (!"".equals(sheet.getRow(4).getCell(3).toString().trim())) {
                    Long value = (long) Double.parseDouble(sheet.getRow(4)
                            .getCell(3).toString().trim());
                    t3bExtensionNo.setValue(value.toString());
                }
                t3bAttachmentsInfo.setValue(sheet.getRow(5).getCell(1)
                        .toString().trim());
                t3bSignPlace.setValue(sheet.getRow(7).getCell(1).toString()
                        .trim());
                if (!"".equals(sheet.getRow(7).getCell(3).toString())) {
                    d3bSignDate.setValue(new Date(sheet.getRow(7).getCell(3)
                            .toString()));
                }
                t3bSignName.setValue(sheet.getRow(8).getCell(1).toString()
                        .trim());
            }

            tb.setSelectedPanel(mainpanel);
            inputStream.close();
            outputStream.close();
        }
        return;
    }

    private void resetControl() {
        if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI
                .equals(documentTypeCode)) {
            // thong tin ve ho so xet nghiem nhanh
            tbRapidTestNo.setValue("");
            tbRapidTestName.setValue("");
            tbRapidTestCode.setValue("");
            tbPlaceOfManufacture.setValue("");
            lboxPropertiesTests.setSelectedIndex(-1);
            tbOperatingPrinciples.setValue("");
            clearTargetTesting();
            tbDescription.setValue("");
            tbPakaging.setValue("");
            ibShelfLife.setValue(0);
            tbStorageConditions.setValue("");
            tbOtherInfos.setValue("");
            tbAttachmentsInfo.setValue("");
            tbSignPlace.setValue("");
            dbSignDate.setValue(null);
            tbSignName.setValue("");
            rtTargetTestingMode.clear();
            lbListOfTargetTesting.setModel(rtTargetTestingMode);
            lbListOfTargetTesting.renderAll();
        }

        if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG
                .equals(documentTypeCode)) {
            // thong tin ve ho so bo sung xet nghiem nhanh
            t2bRapidTestChangeNo.setValue("");
            t2bRapidTestName.setValue("");
            t2bRapidTestCode.setValue("");

            t2bCirculatingRapidTestNo.setValue("");
            d2bDateIssue.setValue(null);
            t2bContens.setValue("");
            t2bAttachmentsInfo.setValue("");
            t2bSignPlace.setValue("");
            d2bSignDate.setValue(null);
            t2bSignName.setValue("");
        }

        if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN
                .equals(documentTypeCode)) {
            // thong tin ve ho so gia han xet nghiem nhanh
            t3bCirculatingExtensionNo.setValue("");
            t3bRapidTestName.setValue("");
            t3bRapidTestCode.setValue("");

            t3bCirculatingRapidTestNo.setValue("");
            d3bDateIssue.setValue(null);
            d3bDateEffect.setValue(null);
            t3bExtensionNo.setValue("");
            t3bAttachmentsInfo.setValue("");
            t3bSignPlace.setValue("");
            d3bSignDate.setValue(null);
            t3bSignName.setValue("");
        }
    }

    @Listen("onDeleteFinalFile = #finalFileListbox")
    public void onDeleteFinalFile(Event event) {
        Attachs obj = (Attachs) event.getData();
        Long fileId = obj.getObjectId();
        AttachDAOHE rDAOHE = new AttachDAOHE();
        rDAOHE.deleteAttach(obj);
        fillFinalFileListbox(fileId);
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    /**
     * Đinh kem ho so goc
     */
    @Listen("onClick = #btnOpenAttachFileFinal")
    public void onCreateAttachFileFinal() throws IOException, Exception {

        if (saveObject() == false) {
            return;
        }
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("CRUDMode", "CREATE");
        arguments.put("fileId", files.getFileId());
        arguments.put("cosFileId", rtFile.getRtFileId());
        arguments.put("parentWindow", windowCRUDRapidTest);
        Window window = createWindow("wdAttachFinalFileCRUD",
                "/Pages/rapidTest/attachFinalFileCRUD.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onLoadFinalFile = #windowCRUDRapidTest")
    public void onLoadFinalFile() {
        fillFinalFileListbox(files.getFileId());
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.RAPID_TEST_HO_SO_GOC);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    @Listen("onUploadCert = #windowCRUDRapidTest")
    public void onUploadCert(Event event) throws Exception {
        if (event.getData() == null || event.getData().toString().startsWith("ERROR")) {
            return;
        }
        String base64Certificate = event.getData().toString();
        X509Certificate x509Cert = com.viettel.newsignature.utils.CertUtils.getX509Cert(base64Certificate);
        WorkflowAPI w = new WorkflowAPI();
        if (!w.checkCA(x509Cert)) {
            showNotification("Chữ ký số không hợp lệ");
            return;
        }
        try {
            actionSignCA(event, actionPrepareSign());
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public String actionPrepareSign() {
        String fileToSign = "";
        try {
            fileToSign = createFileToSign();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return fileToSign;
    }

    public String createFileToSign() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return "fail";
        }

        ExportFileDAO exp = new ExportFileDAO();
        return exp.exportRapidTest(vFileRtfile, false, (List) rtTargetTestingMode);
    }

    public void actionSignCA(Event event, String fileToSign) throws Exception {
        String data = event.getData().toString();
        String base64Certificate = data;
        X509Certificate x509Cert = CertUtils.getX509Cert(base64Certificate);
        SignPdfFile pdfSig = new SignPdfFile();// pdfSig = new SignPdfFile();
        String base64Hash;
        String certSerial = x509Cert.getSerialNumber().toString(16);

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signTemp");
        FileUtil.mkdirs(filePath);
        String outputFileFinalName = "_" + (new Date()).getTime() + ".pdf";
        String outPutFileFinal = filePath + outputFileFinalName;
        // String linkImage = rb.getString("signImage");
        // String linkImageSign = linkImage + "232.png";
        // String linkImageStamp = linkImage + "attpStamp.png";
        CaUserDAO ca = new CaUserDAO();
        List<CaUser> caur = ca.findCaBySerial(certSerial, 1L, getUserId());
        if (caur != null && !caur.isEmpty()) {
            String folderPath = ResourceBundleUtil.getString("dir_upload");
            FileUtil.mkdirs(folderPath);
            String separator = ResourceBundleUtil.getString("separator");// not
            // use
            // binhnt53
            // u230315
            String linkImageSign = folderPath + separator
                    + caur.get(0).getSignature();
            String linkImageStamp = folderPath + separator
                    + caur.get(0).getStamper();
            // fileSignOut = outPutFileFinal;
            Pdf pdfProcess = new Pdf();
            try {// chen chu ki
                String sToFind = Constants.REPLATE_CHARACTER_WHEN_FIND_LOCATION_TO_SIGN.LOCATION_BUSINESS;
                pdfProcess.insertImageAll(fileToSign, outPutFileFinal,
                        linkImageSign, linkImageStamp, sToFind);
            } catch (IOException ex) {
                LogUtils.addLogDB(ex);
            }
            if (pdfProcess.getPageNumber() == -1) {
                showNotification("Ký số không thành công!");
                LogUtils.addLog("Exception " + "Ký số không thành công" + new Date());
                return;
            }

            base64Hash = pdfSig.createHash(outPutFileFinal,
                    new Certificate[]{x509Cert});

            Session session = Sessions.getCurrent();
            session.setAttribute("certSerial", certSerial);
            session.setAttribute("base64Hash", base64Hash);
            txtBase64Hash.setValue(base64Hash);
            txtCertSerial.setValue(certSerial);
            session.setAttribute("PDFSignature", pdfSig);
            Clients.evalJavaScript("signAndSubmitFinalFileRapidTest();");
        } else {
            showNotification("Chữ ký số chưa được đăng ký !!!");
        }
    }

    @Listen("onSign = #windowCRUDRapidTest")
    public void onSign(Event event) {
        String data = event.getData().toString();

        ResourceBundle rb = ResourceBundle.getBundle("config");
        String filePath = rb.getString("signPdf");
        FileUtil.mkdirs(filePath);
        // String inputFileName = "_" + (new Date()).getTime() + ".pdf";//NOT
        // ƯSE BINHNT53 230315
        String outputFileName = "_signed_" + (new Date()).getTime() + ".pdf";
        // String inputFile = filePath + inputFileName;//NOT ƯSE BINHNT53 230315
        String outputFile = filePath + outputFileName;
        fileSignOut = outputFile;
        String signature = data;
        SignPdfFile pdfSig;
        Session session = Sessions.getCurrent();
        pdfSig = (SignPdfFile) session.getAttribute("PDFSignature");

        try {
            pdfSig.insertSignature(signature, outputFile);
        } catch (IOException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
        try {
            onSignFinalFile(outputFile);
            fileSignOut = "";
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Ký số không thành công");
            return;
        }
    }

    /**
     *
     * @throws Exception
     */
    public void onSignFinalFile(String fileName) throws Exception {

        PermitDAO permitDAO = new PermitDAO();
        List<Permit> lstPermit = permitDAO.findAllPermitActiveByFileId(files.getFileId());

        if (lstPermit.size() > 0) {//Da co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            permit = new Permit();
            createPermit();
        }
        permitDAO.saveOrUpdate(permit);
        Long bookNumber = putInBook(permit);//Vao so
        String receiveNo = getPermitNo(bookNumber);
        permit.setReceiveNo(receiveNo);
        permitDAO.saveOrUpdate(permit);

        AttachDAO base = new AttachDAO();
        base.saveFileAttachPdfSign(fileName, files.getFileId(), Constants.OBJECT_TYPE.RAPID_TEST_HO_SO_GOC, null);

        showNotification("Ký số thành công!", Constants.Notification.INFO);
        fillFinalFileListbox(files.getFileId());

    }

    private Permit createPermit() throws Exception {
        permit.setFileId(files.getFileId());
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;

    }

    private Long putInBook(Permit Permit) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);//Lay so dau tien
        BookDocument bookDocument = createBookDocument(Permit.getPermitId(), book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    private String getPermitNo(Long bookNumber) {
        String permitNo = "";
        if (bookNumber != null) {
            permitNo += String.valueOf(bookNumber);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yy"); // Just the year, with 2 digits
        String year = sdf.format(Calendar.getInstance().getTime());
        permitNo += "/" + year;
        permitNo += "/CBMP-QLD";
        return permitNo;
    }

    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    /**
     * ThanhTM
     *
     */
    @Listen("onClick = #btnAddAssembler")
    public void onSaveTargetTesting() throws Exception {
        clearWarningMessage();
        if (!validateTargetTestingData()) {
            return;
        }

        try {

            clearWarningMessage();
            if (!validateTargetTestingData()) {
                return;
            }

            rtTargetTesting = new RtTargetTesting();
            if (itemSelected != null) {
                rtTargetTesting = itemSelected;
            }

            // rtTargetTestingId = rtTargetTestingId + 1;
            // rtTargetTesting.setId(rtTargetTestingId);
            rtTargetTesting.setName(tbTargetTesting.getValue());
            rtTargetTesting.setRangeOfApplications(tbRangeOfApplications
                    .getValue());
            rtTargetTesting.setLimitDevelopment(tbLimitDevelopment.getValue());
            rtTargetTesting.setPrecision(tbPrecision.getValue());
            if (itemSelected == null) {
                rtTargetTestingMode.add(rtTargetTesting);
            }
            lbListOfTargetTesting.setModel(rtTargetTestingMode);
            lbListOfTargetTesting.renderAll();
            itemSelected = null;
            clearTargetTesting();
        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onDeleteTargetTesting = #lbListOfTargetTesting")
    public void deleteTargetTesting(Event event) {
        RtTargetTesting obj = (RtTargetTesting) event.getData();
        if (obj.getId() != null) {
            rtDeleteTargetTestingMode.add(obj);
        }
        rtTargetTestingMode.remove(lbListOfTargetTesting.getSelectedIndex());

        clearTargetTesting();
    }

    @Listen("onEditTargetTesting = #lbListOfTargetTesting")
    public void onEditTargetTesting(Event event) {
        itemSelected = (RtTargetTesting) lbListOfTargetTesting.getSelectedItem().getValue();
        if (itemSelected != null) {
            tbTargetTesting.setValue(itemSelected.getName());
            tbRangeOfApplications.setValue(itemSelected.getRangeOfApplications());
            tbLimitDevelopment.setValue(itemSelected.getLimitDevelopment());
            tbPrecision.setValue(itemSelected.getPrecision());
        } else {
            clearTargetTesting();
        }
    }

    private void clearTargetTesting() {
        tbTargetTesting.setValue("");
        tbRangeOfApplications.setValue("");
        tbLimitDevelopment.setValue("");
        tbPrecision.setValue("");
    }

    @Listen("onSelect = #lbListOfTargetTesting")
    public void onSelect(Event event) {
        clearTargetTesting();
    }

    private Files copy(Files old) {
        Files newObj = new Files();
        newObj.setBusinessAddress(old.getBusinessAddress());
        newObj.setBusinessFax(old.getBusinessFax());
        newObj.setBusinessId(old.getBusinessId());
        newObj.setBusinessName(old.getBusinessName());
        newObj.setBusinessPhone(old.getBusinessPhone());
        newObj.setCreateDate(new Date());
        newObj.setCreateDeptId(old.getCreateDeptId());
        newObj.setCreateDeptName(old.getCreateDeptName());
        newObj.setCreatorId(old.getCreatorId());
        newObj.setCreatorName(old.getCreatorName());
        newObj.setFileCode(old.getFileCode());
        newObj.setFileName(old.getFileName());
        newObj.setFileType(old.getFileType());
        newObj.setFileTypeName(old.getFileTypeName());
        newObj.setFlowId(old.getFlowId());
        newObj.setIsActive(old.getIsActive());
        newObj.setIsTemp(old.getIsTemp());
        newObj.setTaxCode(old.getTaxCode());
        return newObj;
    }

    private RtFile copy(RtFile old) {
        RtFile newObj = new RtFile();
        newObj.setAddressOfManufacture(old.getAddressOfManufacture());
        newObj.setAttachmentsInfo(old.getAttachmentsInfo());
        newObj.setCirculatingExtensionNo(old.getCirculatingExtensionNo());
        newObj.setCirculatingNo(old.getCirculatingNo());
        newObj.setCirculatingRapidTestNo(old.getCirculatingRapidTestNo());
        newObj.setContents(old.getContents());
        newObj.setDateEffect(old.getDateEffect());
        newObj.setDateIssue(old.getDateIssue());
        newObj.setDescription(old.getDescription());
        newObj.setDocumentTypeCode(old.getDocumentTypeCode());
        newObj.setExtensionNo(old.getExtensionNo());
        newObj.setLimitDevelopment(old.getLimitDevelopment());
        newObj.setManufacture(old.getManufacture());
        newObj.setNameOfState(old.getNameOfState());
        newObj.setOperatingPrinciples(old.getOperatingPrinciples());
        newObj.setOtherInfos(old.getOtherInfos());
        newObj.setPakaging(old.getPakaging());
        newObj.setPlaceOfManufacture(old.getPlaceOfManufacture());
        newObj.setPrecision(old.getPrecision());
        newObj.setPropertiesTests(old.getPropertiesTests());
        newObj.setRangeOfApplications(old.getRangeOfApplications());
        newObj.setRapidTestChangeNo(old.getRapidTestChangeNo());
        newObj.setRapidTestCode(old.getRapidTestCode());
        newObj.setRapidTestName(old.getRapidTestName());
        newObj.setRapidTestNo(old.getRapidTestNo());
        newObj.setShelfLife(old.getShelfLife());
        newObj.setShelfLifeMonth(old.getShelfLifeMonth());
        newObj.setSignCirculatingDate(old.getSignCirculatingDate());
        newObj.setSignDate(old.getSignDate());
        newObj.setSignedData(old.getSignedData());
        newObj.setSignName(old.getSignName());
        newObj.setSignPlace(old.getSignPlace());
        newObj.setSignPlaceName(old.getSignPlaceName());
        newObj.setStateOfManufacture(old.getStateOfManufacture());
        newObj.setStorageConditions(old.getStorageConditions());
        newObj.setTargetTesting(old.getTargetTesting());
        return newObj;
    }

}
