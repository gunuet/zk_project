/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BO.Process;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.DocumentReceive;
import com.viettel.voffice.BO.Home.Notify;
import com.viettel.voffice.DAOHE.NotifyDAOHE;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author ChucHV
 */
public class RapidTestProcess extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire("#incSelectObjectsToSendProcess #wdSelectObjectsToSend #grbListNDU")
    protected Groupbox grbListNDU;
    @Wire
    protected Groupbox grbDeadline;
    @Wire
    protected Datebox dbDeadline;
    @Wire("#incSelectObjectsToSendProcess #wdSelectObjectsToSend #cbListNDU")
    protected Caption cbListNDU;
    @Wire
    protected Caption ctDeadline, cbProcessContent;
    @Wire
    protected Label lbBottomWarning, lbTopWarning;
    @Wire
    protected Window wdSendProcess;
    @Wire
    protected Textbox tbcomment;

    @Wire
    protected Include incSelectObjectsToSendProcess;
    @Wire("#incSelectObjectsToSendProcess #wdSelectObjectsToSend #lbNodeDeptUser")
    protected Listbox lbNodeDeptUser;
    @Wire("#incSelectObjectsToSendProcess #wdSelectObjectsToSend")
    protected Window wdSelectObjectsToSend;

    // Attach file
    @Wire
    protected Vlayout flist;
    protected List<Media> listMedia;
    @Wire
    protected Button btnSend, btnChoose;

    protected Window windowParent;
    protected Process processCurrent;// process dang duoc xu li
    protected String actionName;
    protected Long actionType;
    protected DocumentReceive documentReceive;

    protected List<NodeToNode> listNodeToNode;
    protected List<NodeDeptUser> listAvailableNDU;
    protected List<Long> listUserToSend;
    protected List<Long> listDeptToSend;

    public void saveProcess() {
        processCurrent.setProcessId(1000L);
        processCurrent.setObjectId(1000L);
        processCurrent.setObjectType(Constants.OBJECT_TYPE.YCNK_FILE);
        listUserToSend = new ArrayList<>();

        listUserToSend.add(1L);
        listUserToSend.add(2L);

        listDeptToSend = new ArrayList<>();
        listDeptToSend.add(10L);
        listDeptToSend.add(20L);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected Notify sendNotify(
            Process process,
            List<Long> listDeptToSend, List<Long> listUserToSend) {
        NotifyDAOHE notifyDAOHE = new NotifyDAOHE();
        Notify notify = new Notify();
        notify.setObjectId(process.getObjectId());
        notify.setObjectType(process.getObjectType());
        notify.setContent(tbcomment.getText());
        notify.setSendUserId(getUserId());
        notify.setSendUserName(getUserFullName());
        notify.setSendTime(new Date());
        notify.setStatus(Constants.Status.ACTIVE);

        if (listDeptToSend != null && !listDeptToSend.isEmpty()) {
            StringBuilder builder = new StringBuilder(";");
            for (Long deptId : listDeptToSend) {
                builder.append(deptId).append(";");
            }
            notify.setMultiDept(builder.toString());
        }

        if (listUserToSend != null && !listUserToSend.isEmpty()) {
            StringBuilder builder = new StringBuilder(";");
            for (Long userId : listUserToSend) {
                builder.append(userId).append(";");
            }
            notify.setMultiUser(builder.toString());
        }

        notifyDAOHE.saveOrUpdate(notify);

        EventQueue eq = EventQueues.lookup("onLoadListNotify",
                EventQueues.DESKTOP, true);
        eq.publish(new Event("NO NAME"));

        return notify;
    }

 

    // Xu li su kien khi user upload file
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {

            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                	String sExt = ResourceBundleUtil.getString("extend_file", "config");
	                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa"
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            hl.detach();
                            // xoa file khoi danh sach file
                            listMedia.remove(media);
                        }
                    });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

}
