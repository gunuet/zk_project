/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import com.viettel.core.workflow.BO.Process;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Home.Notify;
import java.io.IOException;
import java.util.ArrayList;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.Listen;
/**
 *
 * @author ChucHV
 */
public class RapidTestProcessController extends RapidTestProcess {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        listMedia = new ArrayList<>();
        processCurrent = new Process();
    }


    @Listen("onClick = #btnSend")
    public void onSendProcess() throws IOException {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
        try {
            saveProcess();//Luu thong tin Process

            showNotification(actionName + " thành công",
                    Constants.Notification.INFO);

            Events.sendEvent("onAfterSendProcess", windowParent, null);
            wdSendProcess.onClose();
        } catch (Exception ex) {
            showNotification("Chuyển xử lý thất bại",
                    Constants.Notification.ERROR);
            LogUtils.addLogDB(ex);
            
        }
    }

}
