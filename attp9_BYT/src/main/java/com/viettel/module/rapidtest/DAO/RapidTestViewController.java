package com.viettel.module.rapidtest.DAO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.BO.Process;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.rapidtest.BO.RtFile;
import com.viettel.module.rapidtest.BO.RtTargetTesting;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;

/**
 *
 * @author ChucHV
 */
public class RapidTestViewController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 8261140945077364743L;
    @Wire
    private Listbox fileListbox;

    @Wire
    private Div divToolbarBottom;
    private List<Component> listTopActionComp;
    @Wire
    private Div divToolbarTop;
    private List<Component> listBottomActionComp;

    @Wire
    private Window windowView;
    private Window parentWindow;

    private VFileRtfile vFileRtfile;
    private VFileRtfile a;
    private Process processCurrent;// process dang duoc xu li
    private Integer menuType;

    @Wire
    private Groupbox gbRapidCRUD1, gbRapidCRUD2, gbRapidCRUD3;
    @Wire
    private Tabbox tb;
    private RapidTestDAOHE vFileRtfileDAOHE;

    private long FILE_TYPE;
    ;
    private Long DEPT_ID;
    private Long fileId;
    private RtFile rtFile;
    private ListModelList<RtTargetTesting> rtTargetTestingMode;
    @Wire
    private Listbox lbListOfTargetTesting, lbList, finalFileListbox, signedFileListbox;

    List<NodeToNode> lstNextAction;

    private String codeList = Constants.RAPID_TEST.NHOM_BIEU_MAU.HIEN_THI_CHO_DOANH_NGHIEP_VALUES;

    @SuppressWarnings("unchecked")
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map<String, Object>) Executions
                .getCurrent().getArg();
        fileId = (Long) arguments.get("id");
        vFileRtfileDAOHE = new RapidTestDAOHE();
        vFileRtfile = vFileRtfileDAOHE.findViewByFileId(fileId);
        rtFile = vFileRtfileDAOHE.findByFileId(fileId);
        DEPT_ID = (Long) arguments.get("deptid");
        FILE_TYPE = vFileRtfile.getFileType();
        rtTargetTestingMode = new ListModelList<RtTargetTesting>();
        processCurrent = WorkflowAPI.getInstance().
                getCurrentProcess(vFileRtfile.getFileId(), vFileRtfile.getFileType(),
                        vFileRtfile.getStatus(), getUserId());
        parentWindow = (Window) arguments.get("parentWindow");
        menuType = (Integer) arguments.get("menuType");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        listTopActionComp = new ArrayList<>();
        listBottomActionComp = new ArrayList<>();
        Long documentTypeCode = vFileRtfile.getDocumentTypeCode();
        fillFileListbox(rtFile.getFileId());
        fillFinalFileListbox(rtFile.getFileId());
        fillSignedFileListbox(rtFile.getFileId());
        fillBasicAssay();
        if (documentTypeCode != null) {
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(true);
                gbRapidCRUD2.setVisible(false);
                gbRapidCRUD3.setVisible(false);
                RtTargetTestingDAO objTargetTesting = new RtTargetTestingDAO();
                List lsTargetTesting = objTargetTesting.getByFileId(rtFile
                        .getFileId());
                rtTargetTestingMode = new ListModelList(lsTargetTesting);
                rtTargetTestingMode.setMultiple(true);
                lbListOfTargetTesting.setModel(rtTargetTestingMode);
                lbListOfTargetTesting.renderAll();

            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(false);
                gbRapidCRUD2.setVisible(true);
                gbRapidCRUD3.setVisible(false);
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
                tb.setVisible(true);
                gbRapidCRUD1.setVisible(false);
                gbRapidCRUD2.setVisible(false);
                gbRapidCRUD3.setVisible(true);
            } else {
                tb.setVisible(false);
            }

        }
        if (menuType != null && menuType == Constants.DOCUMENT_MENU.VIEW) {
            return;
        }

        addAllNextActions();
        if (menuType != null) {
            loadActionsToToolbar(menuType, vFileRtfile, processCurrent, windowView);
        }
    }

    private void fillFileListbox(Long vFileRtfileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttach> lstRapidTestAttach = rDaoHe.findRapidTestAttach(vFileRtfileId);
        this.fileListbox.setModel(new ListModelArray(lstRapidTestAttach));
    }

    private void fillFinalFileListbox(Long fileId) {
        AttachDAOHE rDaoHe = new AttachDAOHE();
        List<Attachs> lstAttach = rDaoHe.getByObjectIdAndType(fileId,
                Constants.OBJECT_TYPE.RAPID_TEST_HO_SO_GOC);
        this.finalFileListbox.setModel(new ListModelArray(lstAttach));
    }

    @Listen("onClose = #windowView")
    public void onClose() {
        windowView.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onVisible = #windowView")
    public void onVisible(Event event) {
        windowView.setVisible(true);
    }

    // Hien thi cac action tren thanh toolbar top va bottom
    public void loadActionsToToolbar(int menuType,
            final VFileRtfile vFileRtfile,
            final Process currentProcess, final Window currentWindow) {

        for (Component comp : listTopActionComp) {
            divToolbarTop.appendChild(comp);
        }
        for (Component comp : listBottomActionComp) {
            divToolbarBottom.appendChild(comp);
        }

    }

    @Listen("onViewAttachFile = #attachListBox")
    public void onViewAttachFile(Event event) {
        // try {
        Attachs attach = (Attachs) event.getData();
        java.io.File file = new java.io.File(attach.getAttachPath()
                + attach.getAttachId());
        if (file.exists()) {
            Filedownload.save(attach.getAttachPath() + attach.getAttachId(),
                    null, attach.getAttachName());
        } else {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.WARNING);
        }

    }

    @Listen("onAfterSendProcess = #windowView")
    public void onAfterSendProcess(Event event) {

    }

    @Listen("onAfterRetrieving = #windowView")
    public void onAfterRetrieving(Event event) {
        onAfterSendProcess(event);
    }

    @Listen("onAfterReturning = #windowView")
    public void onAfterReturning(Event event) {
        onAfterSendProcess(event);
    }

    @SuppressWarnings("unchecked")
    @Listen("onPuttingInBook = #windowView")
    public void onAfterPuttingInBook(Event event) throws Exception {
        Map<String, Object> arguments = (Map<String, Object>) event.getData();
        vFileRtfile.setId((Long) arguments.get("id"));
        windowView.setVisible(true);
        onAfterSendProcess(event);
        reloadView();
    }

    // Sau khi tạo hồ sơ
    @Listen("onAfterCreatingFile = #windowView")
    public void onAfterCreatingFile() {
        windowView.setVisible(true);
    }

    private void reloadView() throws Exception {
        // load lai tat ca thong tin cua van ban hien thi tren form View

    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

    public VFileRtfile getvFileRtfile() {
        return vFileRtfile;
    }

    public void setvFileRtfile(VFileRtfile vFileRtfile) {
        this.vFileRtfile = vFileRtfile;
    }

    @Listen("onViewFlow = #windowView")
    public void onViewFlow(Event event) {
        Map args = new ConcurrentHashMap();
        Long docId = vFileRtfile.getFileId();
        Long docType = FILE_TYPE;
        args.put("objectId", docId);
        args.put("objectType", docType);
        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    // Mở popup xử lý nghiệp vụ
    // Trường hợp khởi tạo luồng
    @Listen("onActiveFlow = #windowView")
    public void onActiveFlow() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("docId", vFileRtfile.getId());
        arguments.put("docType", vFileRtfile.getDocumentTypeCode());
        Long status = vFileRtfile.getStatus();
        if (status == null) {
            status = 0l;
        }
        arguments.put("docStatus", status);

        // form nghiep vu tuong ung voi action
        String formName;
        if (status == 3 || status == 0) {
            formName = "/Pages/module/rapidtest/index.zul";
        } else if (status == 4) {
            formName = "/Pages/module/rapidtest/approveDoc.zul";
        } else {
            formName = "/Pages/module/rapidtest/processDoc.zul";
        }
        createWindow("windowActiveFlow", formName, arguments, Window.MODAL);
    }

    private Map<String, Object> setArgument() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("CRUDMode", "CREATE");
        arguments.put("fileId", vFileRtfile.getFileId());
        arguments.put("parentWindow", windowView);
        return arguments;
    }

    /**
     * Đinh kem ho so goc
     */
    @Listen("onCreateAttachFileFinal = #windowView")
    public void onCreateAttachFileFinal() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdAttachFinalFileCRUD",
                "/Pages/rapidTest/include/attachFinalFileCRUD.zul", arguments,
                Window.POPUP);
    }

    /**
     * Download ho so goc
     *
     * @throws java.io.FileNotFoundException
     */
    @Listen("onDownloadAttachFileFinal = #windowView")
    public void onDownloadAttachFileFinal() throws FileNotFoundException {
        AttachDAOHE attachDAOHE = new AttachDAOHE();
        List<Attachs> listAttach = attachDAOHE.getByObjectIdAndType(vFileRtfile.getId(), Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        if (listAttach != null && listAttach.size() > 0) {
            Attachs att = listAttach.get(0);
            String path = att.getAttachPath() + File.separator + att.getAttachId();
            File f = new File(path);
            if (f.exists()) {
                File tempFile = FileUtil.createTempFile(f, att.getAttachName());
                Filedownload.save(tempFile, null);
            } else {
                showNotification("File không còn tồn tại trên hệ thống");
            }
        }
    }

    /**
     * Thanh toan ho so Hien thi danh sach cac loai phi can thanh toan
     */
    @Listen("onPayment = #windowView")
    public void onPayment() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdPayment",
                "/Pages/rapidTest/include/paymentAll.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    /**
     * Ke toan xac nhan thanh toan
     */
    @Listen("onPaymentConfirm = #windowView")
    public void onPaymentConfirm() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdPaymentConfirm",
                "/Pages/rapidTest/include/paymentConfirm.zul", arguments,
                Window.POPUP);
    }

    /**
     * Van thu tiep nhan(vao so)
     */
    @Listen("onPutInBookOk = #windowView")
    public void onPutInBookOk() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdPutInBook",
                "/Pages/rapidTest/include/putInBookOk.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onPutInBookNok = #windowView")
    public void onPutInBookNok() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdPutInBook",
                "/Pages/rapidTest/include/putInBookNok.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onEvaluation = #windowView")
    public void onEvaluation() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdEvaluation",
                "/Pages/rapidTest/include/evaluationCV.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onEvaluationCv = #windowView")
    public void onEvaluationCv() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdEvaluation",
                "/Pages/rapidTest/include/evaluationCv.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onEvaluationTP = #windowView")
    public void onEvaluationTP() {
        Map<String, Object> arguments = setArgument();
        arguments.put("userEvaluationType", Constants.EVALUTION.USER_EVALUATION_TYPE.TP);
        Window window = createWindow("wdEvaluationTP",
                "/Pages/rapidTest/include/evaluationTP.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onApproveEvaluation = #windowView")
    public void onApproveEvaluation() {
        Map<String, Object> arguments = setArgument();
        arguments.put("userEvaluationType", Constants.EVALUTION.USER_EVALUATION_TYPE.CT);
        Window window = createWindow("wdEvaluationCT",
                "/Pages/rapidTest/include/evaluationCT.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onProvideNumberDispatch = #windowView")
    public void onProvideNumberDispatch() {
        Map<String, Object> arguments = setArgument();
        arguments.put("userEvaluationType", Constants.EVALUTION.USER_EVALUATION_TYPE.VT);
        Window window = createWindow("wdEvaluationVT",
                "/Pages/rapidTest/include/evaluationVT.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    @Listen("onProvideNumberPermit = #windowView")
    public void onProvideNumberPermit() {
        Map<String, Object> arguments = setArgument();
        arguments.put("userEvaluationType", Constants.EVALUTION.USER_EVALUATION_TYPE.VT);
        Window window = createWindow("wdEvaluationVT",
                "/Pages/rapidTest/include/evaluationVT.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    /**
     * Đong y phe duyet
     */
    @Listen("onApprove = #windowView")
    public void onApprove() {
        Map<String, Object> arguments = setArgument();
        Window window = createWindow("wdApprove",
                "/Pages/rapidTest/include/approve.zul", arguments,
                Window.POPUP);
        window.doModal();
    }

    /**
     * Ky duyet Voi lanh dao phong la ky nhay Voi lanh dao cuc la ky duyet
     */
    @Listen("onSignCA = #windowView")
    public void onSignCA() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdSignCA",
                "/Pages/rapidTest/include/approve.zul", arguments,
                Window.POPUP);
    }

    /**
     * Tu choi
     */
    @Listen("onReject = #windowView")
    public void onReject() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdReject",
                "/Pages/rapidTest/include/approve.zul", arguments,
                Window.POPUP);
    }

    /**
     * Gui doanh nghiep yeu cau bo sung ho so
     */
    @Listen("onSendCommentForAddition = #windowView")
    public void onSendCommentForAddition() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdSendCommentForAddition",
                "/Pages/rapidTest/include/sendCommentForAddition.zul", arguments,
                Window.POPUP);
    }

    /**
     * Doanh nghiep gui yeu cau kiem tra
     */
    @Listen("onSendRequestToCheck = #windowView")
    public void onSendRequestToCheck() {
        Map<String, Object> arguments = setArgument();
        createWindow("wdSendRequestToCheck",
                "/Pages/rapidTest/include/sendRequestToCheck.zul", arguments,
                Window.POPUP);
    }

//    @Listen("onExportSDBS = #windowView")
//    public void onExportSDBS() {
//        Map<String, Object> arguments = setArgument();
//        int typeExport = Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP;
//        ExportFileDAO export = new ExportFileDAO();
//        export.exportDataCvSdbs(typeExport);
//        
//        
//    }
    private void addAllNextActions() {
        Long docId = vFileRtfile.getFileId();
        Long userId = getUserId();
        //Long docType = FILE_TYPE;
        Long deptId = DEPT_ID;

        if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(vFileRtfile.getDocumentTypeCode())) {
            FILE_TYPE = Constants.DOC_TYPE_CODE_BOSUNG;
        }

        if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(vFileRtfile.getDocumentTypeCode())) {
            FILE_TYPE = Constants.DOC_TYPE_CODE_GIAHAN;
        }

        List<Process> lstProcess = WorkflowAPI.getInstance().getProcess(docId, FILE_TYPE, userId);
        List<Process> lstAllProcess = WorkflowAPI.getInstance().getAllProcessNotFinish(docId, FILE_TYPE);
        if ((lstAllProcess == null || lstAllProcess.isEmpty()) && vFileRtfile.getStatus() != Constants.FILE_STATUS_CODE.STATUS_MOITAO) {
            return;
        }

        List<Long> lstStatus = new ArrayList();
        //linhdx tim nhung trang thai chua hoan thanh xu ly de tim action tiep theo
        for (Process obj : lstProcess) {
            if (obj.getFinishDate() == null) {
                lstStatus.add(obj.getStatus());
            }
        }

        //linhdx Co nguoi xu ly roi (Doanh nghiep da nop ho so) ma khong co 
        //trang thai nao nguoi do can xu ly thi them flag NO_NEED_PROCESS
        //de bao khong can xu ly
        if (lstStatus.isEmpty() && lstAllProcess != null && !lstAllProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }

        //linhdx da co ho so gui toi nguoi do ma khong co ho so nao o trang thai
        //can xu ly thi add flag NO_NEED_PROCESS de bao la khong phai xu ly nua
        if (lstStatus.isEmpty() && !lstProcess.isEmpty()) {
            lstStatus.add(Constants.PROCESS_STATUS.NO_NEED_PROCESS);
        }

        //Luu y: Neu de lstStatus == null thi ham findAvaiableNextActions se coi nhu la 
        //chua co xu ly va se tim node dau tien de gui
        List<NodeToNode> actions = WorkflowAPI.getInstance().findAvaiableNextActions(
                FILE_TYPE, lstStatus, deptId);
        lstNextAction = actions;//linhdx

        if (actions != null && actions.size() > 0) {
            NodeToNode action = actions.get(0);
            Button temp = createButtonForAction(action);
            if (temp != null) {
                listTopActionComp.add(temp);
            }
        }
//         for (NodeToNode action : actions) {
//             Button temp = createButtonForAction(action);
//             if (temp != null) {
//                 listTopActionComp.add(temp);
//             }
//         }

    }

    // viethd3: create new button for a processing action
    private Button createButtonForAction(NodeToNode action) {
        if (validateUserRight(action, processCurrent, vFileRtfile)) {
            return null;
        }

        //Button btn = new Button(action.getAction());
        //final String actionName = action.getAction();
        Button btn = new Button("Xử lý hồ sơ");
        final String actionName = "Xử lý hồ sơ";
        //final String actionName = "Xem thông tin xử lý";
        final Long actionId = action.getId();
        final Long actionType = action.getType();
        final Long nextId = action.getNextId();
        final Long prevId = action.getPreviousId();
        // form nghiep vu tuong ung voi action
        final String formName = WorkflowAPI.PROCESSING_GENERAL_PAGE;
        final Long status = vFileRtfile.getStatus();
        final List<NodeToNode> lstNextAction1 = lstNextAction;

        final List<NodeDeptUser> lstNDUs = new ArrayList<>();
        lstNDUs.addAll(WorkflowAPI.getInstance().findNDUsByNodeId(nextId, false));

        EventListener<Event> event = new EventListener<Event>() {
            @Override
            public void onEvent(Event t) throws Exception {
                Map<String, Object> data = new ConcurrentHashMap<>();
                data.put("fileId", vFileRtfile.getFileId());
                data.put("docId", vFileRtfile.getFileId());
                data.put("docType", FILE_TYPE);
                data.put("docStatus", status);
                data.put("actionId", actionId);

                data.put("actionName", actionName);
                data.put("actionType", actionType);
                data.put("nextId", nextId);
                data.put("previousId", prevId);
                if (processCurrent != null) {
                    data.put("process", processCurrent);
                }
                data.put("lstAvailableNDU", lstNDUs);
                data.put("parentWindow", windowView);
                data.put("lstNextAction", lstNextAction1);
                createWindow("windowComment", formName, data, Window.MODAL);
            }
        };

        btn.addEventListener(Events.ON_CLICK, event);
        return btn;
    }

    @Listen("onRefresh=#windowView")
    public void onRefresh() {
        windowView.detach();

        RapidTestDAOHE vFileRtfileDAO = new RapidTestDAOHE();
        VFileRtfile vFileRtfile1 = vFileRtfileDAO.findViewByFileId(fileId);
        Events.sendEvent("onRefresh", parentWindow, vFileRtfile1);
        LogUtils.addLog("on refresh view window!");
    }

    public boolean validateUserRight(NodeToNode action, Process processCurrent,
            VFileRtfile vFileRtfile) {
        // check if action is RETURN_PREVIOUS but parent process came from another node
        //linhdx comment 20150405
//        if (processCurrent != null
//                && action.getType() == Constants.NODE_ASSOCIATE_TYPE.RETURN_PREVIOUS
//                && action.getNextId() != processCurrent.getPreviousNodeId()) {
//            return true;
//        }
        // check if receiverId is diferent to current user id
        // find all process having current status of document
        List<Process> listCurrentProcess = WorkflowAPI.getInstance().findAllCurrentProcess(vFileRtfile.getFileId(), vFileRtfile.getFileType(),
                vFileRtfile.getStatus());
        Long userId = getUserId();
        Long creatorId = vFileRtfile.getCreatorId();
        if (listCurrentProcess.isEmpty()) {
            if (!userId.equals(creatorId)) {
                return true;
            }
        } else {
            if (processCurrent == null) {
                boolean needToProcess = false;
                for (Process p : listCurrentProcess) {
                    if (p.getReceiveUserId().equals(userId)) {
                        needToProcess = true;
                    }
                }
                if (!needToProcess) {
                    return true;
                }
            }
        }
        return false;
    }

    @Listen("onExportFile = #windowView")
    public void onExportFile() {
        ExportFileDAO exp = new ExportFileDAO();
        exp.exportRapidTest(vFileRtfile, true, (List) rtTargetTestingMode);
    }

    public int checkViewProcess() {
        return checkViewProcess(vFileRtfile.getFileId());
    }

    public int checkViewProcess(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        return Constants.CHECK_VIEW.VIEW;
    }

    public int checkDispathSDBS() {
        return checkDispathSDBS(vFileRtfile.getFileId());
    }

    public int checkDispathSDBS(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        RapidTestDAOHE rtDao = new RapidTestDAOHE();
        int check = rtDao.checkDispathRT(fileId, getUserId());
        return check;

    }

    public int checkBooked() {
        return checkBooked(vFileRtfile.getFileId(), vFileRtfile.getFileType());
    }

    public int checkBooked(Long fileId, Long fileType) {
        if (fileId == null || fileType == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        BookDocumentDAOHE dao = new BookDocumentDAOHE();
        int check = dao.checkBookedRapidTest(fileId, fileType);
        return check;
    }

    public int checkPayment() {
        return checkPayment(vFileRtfile.getFileId());
    }

    public int checkPayment(Long fileId) {
        if (fileId == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        PaymentInfoDAO dao = new PaymentInfoDAO();
        int check = dao.checkPayment(fileId);
        return check;
    }

    public int checkViewAssay() {
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        EvaluationRecord obj;
        obj = objDAO.findMaxByFileIdAndEvalType(vFileRtfile.getFileId(), Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
        if (obj != null) {
            Gson gson = new Gson();
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            if (evModel.getEvaluationBasicAssay() == null) {
                return 0;
            }
        } else {
            return 0;
        }
        return 1;
    }

    void fillBasicAssay() {
        EvaluationRecord obj;
        EvaluationRecordDAO objDAO = new EvaluationRecordDAO();
        obj = objDAO.findMaxByFileIdAndEvalType(vFileRtfile.getFileId(), Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
        if (obj != null) {
            Gson gson = new Gson();
            EvaluationModel evModel = gson.fromJson(obj.getFormContent(), EvaluationModel.class);
            if (evModel.getEvaluationBasicAssay() != null) {
                lbList.setModel(evModel.getEvaluationBasicAssay());
            }
        }
    }

    @Listen("onDownloadFinalFile = #finalFileListbox")
    public void onDownloadFinalFile(Event event) throws FileNotFoundException {
        Attachs obj = (Attachs) event.getData();
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(obj);

    }

    public int checkSignedFile() {
        if (vFileRtfile.getFileId() == null) {
            return Constants.CHECK_VIEW.NOT_VIEW; //Khong hien thi
        }
        RapidTestAttachDAOHE dao = new RapidTestAttachDAOHE();
        int check = dao.checkSignedFile(vFileRtfile.getFileId(), codeList);
        return check;
    }

    private void fillSignedFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttachAll> lstRapidTestSignFile = rDaoHe.findRapidTestAllLastAttachByCodeList(fileId, codeList);
        if (lstRapidTestSignFile.size() > 0) {
            this.signedFileListbox.setModel(new ListModelArray(lstRapidTestSignFile));
        }
    }

    @Listen("onDownloadSignedFile = #signedFileListbox")
    public void onDownloadSignedFile(Event event) throws FileNotFoundException {
        VFileRtAttachAll obj = (VFileRtAttachAll) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findByAttachId(attachId);
        AttachDAO attDAO = new AttachDAO();
        attDAO.downloadFileAttach(att);
    }

}
