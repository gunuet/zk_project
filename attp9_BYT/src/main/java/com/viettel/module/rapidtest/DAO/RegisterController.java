package com.viettel.module.rapidtest.DAO;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.sys.DAO.PlaceDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_XNN;
import com.viettel.voffice.BO.Register;
import com.viettel.core.sys.DAO.RegisterDAOHE;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.ValidatorUtil;
import com.viettel.voffice.model.PublicFunctionModel;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.batik.dom.util.HashTable;
import org.zkforge.bwcaptcha.Captcha;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

/**
 *
 * @author nghiepnc
 */
public class RegisterController extends BaseComposer {

    private Register register;
    @Wire
    private Textbox tbBusiness_name_vi, tbBusiness_name_ENG, tbBusiness_name_alias, tbBusiness_tax_code, tbBusiness_license, tbgoverning_body, tbBusiness_add, tbBusiness_telephone, tbBusiness_fax,
            tbManager_email, tbBusiness_website, tbUser_full_name, tbUser_mobile, tbUserEmail, txtNamThanhLap, txtCaptchaId;
    @Wire
    private Listbox tbBusiness_type_name, tbBusiness_province, tbBusiness_district, tbBusiness_town, tbUser_type;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    private HashTable hasLabel;
    @Wire
    Captcha capcha;
    @Wire
    Vbox vboxCaptchaId;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        Session ss = Sessions.getCurrent();
        if (ss.getAttribute("countCaptcha2") == null) {
            ss.setAttribute("countCaptcha2", 0);
        }
        hasLabel = new HashTable();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        this.getPage().setTitle(ResourceBundleUtil.getString("title"));
        capcha.setBgColor(16777215);
        capcha.setCaptchars(Constants.getCaptchars());
        if (Integer.valueOf(Sessions.getCurrent().getAttribute("countCaptcha2").toString()) >= 2) {
            vboxCaptchaId.setVisible(true);
            Sessions.getCurrent().setAttribute("isVisibleCaptcha", true);
        }
        CategoryDAOHE cdhe = new CategoryDAOHE();
        PlaceDAOHE phe = new PlaceDAOHE();
        //cb tinh

        List lstProvince = phe.findPlaceSearchBycode(Constants_XNN.PLACE.PROVINCE, Constants.PLACE.VNCODE);
        ListModelArray lstModelProvince = new ListModelArray(lstProvince);
        tbBusiness_province.setModel(lstModelProvince);
        tbBusiness_province.renderAll();
        tbBusiness_province.setSelectedIndex(0);

        //list huyen
        List lstdistrict = phe.findAllPlaceSearch(Constants_XNN.PLACE.DISTRICT, -1L);
        ListModelArray lstModelDistrict = new ListModelArray(lstdistrict);
        tbBusiness_district.setModel(lstModelDistrict);
        tbBusiness_district.renderAll();
        tbBusiness_district.setSelectedIndex(0);

        //list xa
        List lsttown = phe.findAllPlaceSearch(Constants_XNN.PLACE.VILLAGE, -1L);
        ListModelArray lstModelTown = new ListModelArray(lsttown);
        tbBusiness_town.setModel(lstModelTown);
        tbBusiness_town.renderAll();
        tbBusiness_town.setSelectedIndex(0);

        //cb loai hinh
        //dua ra cóntaint
        List lstObjects = cdhe.getSelectCategoryByType("BUSINESS_TYPE", "value");
        ListModelArray lstPosition = new ListModelArray(lstObjects);
        tbBusiness_type_name.setModel(lstPosition);
        tbBusiness_type_name.renderAll();
        tbBusiness_type_name.setSelectedIndex(0);

        lstObjects = cdhe.getSelectCategoryByType("USER_TYPE", "value");
        lstPosition = new ListModelArray(lstObjects);
        tbUser_type.setModel(lstPosition);
        tbUser_type.renderAll();
        tbUser_type.setSelectedIndex(0);

    }

    @Listen("onClick = #btnAction")
    public void onSave(int typeSave) throws Exception {
        
        Session ss = Sessions.getCurrent();
        if (Long.valueOf(ss.getAttribute("countCaptcha2").toString()) == 0) {
            ss.setAttribute("timeStart", new Date());
            ss.setAttribute("timeEnd", new Date());
            ss.setAttribute("isVisibleCaptcha", false);
        }
        if (Long.valueOf(ss.getAttribute("countCaptcha2").toString()) == 2) {
            ss.setAttribute("timeEnd", new Date());
        }
        Long timeStart = ((Date) ss.getAttribute("timeStart")).getTime();
        Long timeEnd = ((Date) ss.getAttribute("timeEnd")).getTime();
        Long time = timeEnd - timeStart;

//        if (isValidatedData()) {
        if (Integer.valueOf(ss.getAttribute("countCaptcha2").toString()) > 2
                && capcha != null
                && capcha.getValue() != null
                && !capcha.getValue().equals(txtCaptchaId.getValue())
                && (Boolean) ss.getAttribute("isVisibleCaptcha")) {
            lbTopWarning.setVisible(true);
            lbTopWarning.setValue("Nhập lại mã Captcha");
            txtCaptchaId.setValue(null);
            capcha.randomValue();
            Long userId = -1L;
            LogUtils.addLog(null, userId, null, Constants.ACTION.TYPE.REGISTER, Constants.ACTION.NAME.CAPTCHA_LOG,
                    Constants.OBJECT_TYPE.REGISTER, null, Constants.OBJECT_TYPE.REGISTER, null, ss.getRemoteAddr());
            return;
        } else if (Integer.valueOf(ss.getAttribute("countCaptcha2").toString()) > 2
                && capcha != null
                && capcha.getValue() != null
                && capcha.getValue().equals(txtCaptchaId.getValue())) {
            ss.setAttribute("countCaptcha2", -1);
            vboxCaptchaId.setVisible(false);
            txtCaptchaId.setValue(null);
//                lbTopWarning.setVisible(false);
//                createObject();
        }

        if (!isValidatedData()) {
            if (time > 10 * 1000 && Long.valueOf(ss.getAttribute("countCaptcha2").toString()) <= 2) {
                ss.setAttribute("countCaptcha2", -1);
                ss.setAttribute("isVisibleCaptcha", false);
            }
            if (Long.valueOf(ss.getAttribute("countCaptcha2").toString()) <= 2) {
                ss.setAttribute("countCaptcha2", Long.valueOf(ss.getAttribute("countCaptcha2").toString()) + 1);
            }
            if (Long.valueOf(ss.getAttribute("countCaptcha2").toString()) > 2
                    && time <= 10 * 1000) {
                capcha.randomValue();
                vboxCaptchaId.setVisible(true);
                ss.setAttribute("isVisibleCaptcha", true);
            }
        } else {
            createObject();
        }
    }

    private boolean isValidatedData() {

        String errorMsg;
        if (PublicFunctionModel.validateBlank(tbBusiness_name_vi.getText())) {
            showWarningMessage("Tên tiếng việt không thể để trống!");
            tbBusiness_name_vi.focus();
            return false;
        }
        if (tbBusiness_name_vi.getText().trim().length() > 250) {
            showWarningMessage("Tên tiếng việt không được nhiều hơn 250 ký tự!");
            tbBusiness_name_vi.focus();
            return false;
        }
        if (!PublicFunctionModel.validateBlank(tbBusiness_name_ENG.getText())) {
            if (tbBusiness_name_ENG.getText().trim().length() > 250) {
                showWarningMessage("Tên tiếng anh không được nhiều hơn 250 ký tự!");
                tbBusiness_name_ENG.focus();
                return false;
            }
        }
        if (!PublicFunctionModel.validateBlank(tbBusiness_name_alias.getText())) {
            if (tbBusiness_name_alias.getText().trim().length() > 50) {
                showWarningMessage("Tên viết tắt không được nhiều hơn 50 ký tự!");
                tbBusiness_name_alias.focus();
                return false;
            }
        }
        if (PublicFunctionModel.validateBlank(tbBusiness_tax_code.getText())) {
            showWarningMessage("Mã số thuế không thể để trống!");
            tbBusiness_tax_code.focus();
            return false;
        }
        if (tbBusiness_tax_code.getText().trim().length() > 15) {
            showWarningMessage("Mã số thuế không được nhiều hơn 15 ký tự!");
            tbBusiness_tax_code.focus();
            return false;
        }
        if (PublicFunctionModel.validateNumber(tbBusiness_tax_code.getText())) {
            showWarningMessage("Mã số thuế phải là số!");
            tbBusiness_tax_code.focus();
            return false;
        }
        if ("-1".equals(tbBusiness_type_name.getSelectedItem().getValue().toString())) {
            showWarningMessage("Bạn phải chọn loại hình!");
            tbBusiness_type_name.focus();
            return false;
        }
        if (tbBusiness_license.getText().matches("\\s*")) {
            showWarningMessage("Số đăng ký KD không thể để trống!");
            tbBusiness_license.focus();
            return false;
        }
        if (tbBusiness_license.getText().trim().length() > 15) {
            showWarningMessage("Số đăng ký KD không được nhiều hơn 15 ký tự!");
            tbBusiness_license.focus();
            return false;
        }
        if (PublicFunctionModel.validateNumber(tbBusiness_license.getText())) {
            showWarningMessage("Số đăng ký kinh doanh phải là số!");
            tbBusiness_license.focus();
            return false;
        }
        if ("-1".equals(tbBusiness_province.getSelectedItem().getValue().toString())) {
            showWarningMessage("Bạn phải chọn Tỉnh/Thành phố!");
            tbBusiness_province.focus();
            return false;
        }
        if (tbBusiness_district.getSelectedItem() != null) {
            int idx = tbBusiness_district.getSelectedItem().getIndex();
            if (idx <= 0) {
                showWarningMessage("Bạn phải chọn Quận/Huyện!");
                tbBusiness_district.focus();
                return false;
            }
        } else {
            showWarningMessage("Bạn phải chọn Quận/Huyện!");
            tbBusiness_district.focus();
            return false;
        }

        if ("-1".equals(tbBusiness_town.getSelectedItem().getValue().toString())) {
            showWarningMessage("Bạn phải chọn Xã/Phường/Thị trấn!");
            tbBusiness_town.focus();
            return false;
        }
        if (tbBusiness_add.getText().matches("\\s*")) {
            showWarningMessage("Địa chỉ chi tiết không thể để trống!");
            tbBusiness_add.focus();
            return false;
        }
        if (tbBusiness_add.getText().trim().length() > 250) {
            showWarningMessage("Địa chỉ chi tiết không được nhiều hơn 250 ký tự!");
            tbBusiness_add.focus();
            return false;
        }

        if ((errorMsg = ValidatorUtil.validateTextbox(tbBusiness_telephone, true, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            showWarningMessage(String.format(errorMsg, "Điện thoại doanh nghiệp"));
            tbBusiness_telephone.focus();
            return false;
        }
//        if (PublicFunctionModel.validateNumber(tbBusiness_telephone.getText())) {
//            showWarningMessage("Điện thoại doanh nghiệp phải là số");
//            tbBusiness_telephone.focus();
//            return false;
//        }

        if ((errorMsg = ValidatorUtil.validateTextbox(tbBusiness_fax, false, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            showWarningMessage(String.format(errorMsg, "Số fax"));
            tbBusiness_fax.focus();
            return false;
        }

        if (txtNamThanhLap.getText() != null) {
            if (PublicFunctionModel.validateNumber(txtNamThanhLap.getText())) {
                showWarningMessage("Năm thành lập phải là số!");
                txtNamThanhLap.focus();
                return false;
            }
        }

        if (txtNamThanhLap.getText() != null) {
            Integer year = Calendar.getInstance().get(Calendar.YEAR);
            if (Integer.parseInt(txtNamThanhLap.getText()) > year) {
                showWarningMessage("Năm thành lập không thể lớn năm hiện tại");
                txtNamThanhLap.focus();
                return false;
            }
        }

        if (tbManager_email.getText().matches("\\s*")) {
            showWarningMessage("Email không thể để trống!");
            tbManager_email.focus();
            return false;
        }
        if (tbManager_email.getText().trim().length() > 50) {
            showWarningMessage("Email không được nhiều hơn 50 ký tự!");
            tbManager_email.focus();
            return false;
        }

        if (PublicFunctionModel.validateEmail(tbManager_email.getText())) {
            showWarningMessage("Email không đúng định dạng!");
            tbManager_email.focus();
            return false;
        }

        if (tbUser_full_name.getText().matches("\\s*")) {
            showWarningMessage("Họ và tên người đại diện không thể để trống!");
            tbUser_full_name.focus();
            return false;
        }
        if (tbUser_full_name.getText().trim().length() > 250) {
            showWarningMessage("Họ và tên người đại diện không được nhiều hơn 250 ký tự!");
            tbUser_full_name.focus();
            return false;
        }

        if (tbUserEmail.getText().matches("\\s*")) {
            showWarningMessage("Email không thể để trống!");
            tbUserEmail.focus();
            return false;
        }

        if (PublicFunctionModel.validateEmail(tbUserEmail.getText())) {
            showWarningMessage("Email không đúng định dạng!");
            tbUserEmail.focus();
            return false;
        }

        if ("-1".equals(tbUser_type.getSelectedItem().getValue().toString())) {
            showWarningMessage("Bạn phải chọn chức vụ người đại diện!");
            tbBusiness_province.focus();
            return false;
        }

        if ((errorMsg = ValidatorUtil.validateTextbox(tbUser_mobile, false, 15, ValidatorUtil.PATTERN_CHECK_PHONENUMBER)) != null) {
            showWarningMessage(String.format(errorMsg, "Số di động người đại diện"));
            tbUser_mobile.focus();
            return false;
        }

        RegisterDAOHE registerDAOHE = new RegisterDAOHE();

        if (registerDAOHE.checkBusinessTaxCode(tbBusiness_tax_code.getText())) {
            showWarningMessage("Mã số thuế đã đăng ký trong hệ thống!");
            tbBusiness_tax_code.focus();
            return false;
        }

        return true;
    }

    //<editor-fold defaultstate="collapsed" desc="function">
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
//        lbBottomWarning.setValue(message);
    }

    protected void clearMessage() {
        tbBusiness_name_vi.setText("");
        tbBusiness_name_ENG.setText("");
        tbBusiness_name_alias.setText("");
        tbBusiness_tax_code.setText("");
        tbBusiness_license.setText("");
        tbgoverning_body.setText("");
        tbBusiness_add.setText("");
        tbBusiness_telephone.setText("");
        tbBusiness_fax.setText("");
        tbManager_email.setText("");
        tbBusiness_website.setText("");
        tbUser_full_name.setText("");
        tbUser_mobile.setText("");
        txtNamThanhLap.setText("");
        tbUserEmail.setText("");
        tbUser_type.setSelectedIndex(0);
        tbBusiness_type_name.setSelectedIndex(0);
        tbBusiness_province.setSelectedIndex(0);
        tbBusiness_district.setSelectedIndex(0);
        tbBusiness_town.setSelectedIndex(0);

    }

    private void createObject() throws Exception {
        RegisterDAOHE registerDAOHE = new RegisterDAOHE();
        CategoryDAOHE cdhe = new CategoryDAOHE();
        String sLoaiHinh = cdhe.LayTruongCategory_Name(Long.valueOf(tbBusiness_type_name.getSelectedItem().getValue().toString()));
        String sChucVu = cdhe.LayTruongCategory_Name(Long.valueOf(tbUser_type.getSelectedItem().getValue().toString()));
        register = new Register();
        if (tbBusiness_name_vi.getValue() != null) {
            register.setBusinessNameVi(tbBusiness_name_vi.getValue().trim());
        }
        if (tbBusiness_name_ENG.getValue() != null) {
            register.setBusinessNameEng(tbBusiness_name_ENG.getValue().trim());
        }
        if (tbBusiness_name_alias.getValue() != null) {
            register.setBusinessNameAlias(tbBusiness_name_alias.getValue().trim());
        }
        if (tbBusiness_tax_code.getValue() != null) {
            register.setBusinessTaxCode(tbBusiness_tax_code.getValue().trim());
        }

        register.setBusinessTypeId(Long.parseLong(tbBusiness_type_name.getSelectedItem().getValue().toString()));
        if (sLoaiHinh != null) {
            register.setBusinessTypeName(sLoaiHinh.trim());
        }
        if (tbBusiness_license.getValue() != null) {
            register.setBusinessLicense(tbBusiness_license.getValue().trim());
        }
        if (tbgoverning_body.getValue() != null) {
            register.setGoverningBody(tbgoverning_body.getValue().trim());
        }
        register.setBusinessProvince(tbBusiness_province.getSelectedItem().getValue().toString());
        register.setBusinessDistrict(tbBusiness_district.getSelectedItem().getValue().toString());
        register.setBusinessTown(tbBusiness_town.getSelectedItem().getValue().toString());
        if (tbBusiness_add.getValue() != null) {
            register.setBusinessAdd(tbBusiness_add.getValue().trim());
        }
        if (tbBusiness_telephone.getValue() != null) {
            register.setBusinessTelephone(tbBusiness_telephone.getValue().trim());
        }
        if (tbManager_email.getValue() != null) {
            register.setManageEmail(tbManager_email.getValue().trim());
        }
        if (tbBusiness_fax.getValue() != null) {
            register.setBusinessFax(tbBusiness_fax.getValue().trim());
        }

        if (tbBusiness_website.getValue() != null) {
            register.setBusinessWebsite(tbBusiness_website.getValue().trim());
        }
        if (tbUser_full_name.getValue() != null) {
            register.setUserFullName(tbUser_full_name.getValue().trim());
        }
        if (tbUser_mobile.getValue() != null) {
            register.setUserMobile(tbUser_mobile.getValue().trim());
        }
        if (tbUserEmail.getValue() != null) {
            register.setUserEmail(tbUserEmail.getValue().trim());
        }
        if (txtNamThanhLap.getValue() != null) {
            register.setBusinessEstablishYear(txtNamThanhLap.getValue().trim());
        }

        register.setPosId(Long.parseLong(tbUser_type.getSelectedItem().getValue().toString()));
        register.setPosName(sChucVu != null ? sChucVu.trim() : null);
        register.setDateCreate(new Date());
        //trang thai chua duyet
        register.setStatus(Constants_XNN.Status.INACTIVE);
        try {
            registerDAOHE.saveOrUpdate(register);
            showWarningMessage(Constants_XNN.Notification.REGISTER_SUCCESS);
            clearMessage();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

        }
    }

    public void onSelectProvince() {
        if (tbBusiness_province.getSelectedItem() != null) {
            int idx = tbBusiness_province.getSelectedItem().getIndex();
            PlaceDAOHE phe = new PlaceDAOHE();
            if (idx > 0l) {
                Long id = (Long) tbBusiness_province.getSelectedItem().getValue();
                List lstProvince = phe.findAllPlaceSearch(Constants_XNN.PLACE.DISTRICT, id);
                ListModelArray lstModelProvince = new ListModelArray(lstProvince);
                tbBusiness_district.setModel(lstModelProvince);
                tbBusiness_district.renderAll();
                tbBusiness_town.renderAll();
                tbBusiness_town.setSelectedIndex(0);
                tbBusiness_district.setSelectedIndex(0);
            } //an nut chon
            else {
                List lstProvince = phe.findAllPlaceSearch(Constants_XNN.PLACE.DISTRICT, -1L);
                ListModelArray lstModelProvince = new ListModelArray(lstProvince);
                tbBusiness_district.setModel(lstModelProvince);
                tbBusiness_district.renderAll();

                List lstTown = phe.findAllPlaceSearch(Constants_XNN.PLACE.VILLAGE, -1L);
                ListModelArray lstModelXa = new ListModelArray(lstTown);
                tbBusiness_town.setModel(lstModelXa);
                tbBusiness_town.renderAll();
                tbBusiness_town.setSelectedIndex(0);
                tbBusiness_district.setSelectedIndex(0);
            }
        }
    }

    public void onSelectDistrict() {
        if (tbBusiness_district.getSelectedItem() != null) {
            int idx = tbBusiness_district.getSelectedItem().getIndex();
            PlaceDAOHE phe = new PlaceDAOHE();
            if (idx > 0l) {
                Long id = (Long) tbBusiness_district.getSelectedItem().getValue();
                List lstProvince = phe.findAllPlaceSearch(Constants_XNN.PLACE.VILLAGE, id);
                ListModelArray lstModelProvince = new ListModelArray(lstProvince);
                tbBusiness_town.setModel(lstModelProvince);
                tbBusiness_town.renderAll();
                tbBusiness_town.setSelectedIndex(0);
            } else {
                List lstProvince = phe.findAllPlaceSearch(Constants_XNN.PLACE.VILLAGE, -1L);
                ListModelArray lstModelProvince = new ListModelArray(lstProvince);
                tbBusiness_town.setModel(lstModelProvince);
                tbBusiness_town.renderAll();
                tbBusiness_town.setSelectedIndex(0);
            }
        }
    }

    public String getLabel(String key) {
        try {
            return ResourceBundleUtil.getString(key, "language_XNN_vi");

        } catch (UnsupportedEncodingException ex) {
            LogUtils.addLogDB(ex);
        }
        return "";
    }
    //</editor-fold>
}
