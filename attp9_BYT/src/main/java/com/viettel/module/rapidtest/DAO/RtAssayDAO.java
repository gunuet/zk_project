/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.utils.Constants;

/**
 *
 * @author E5420
 */
public class RtAssayDAO extends
        GenericDAOHibernate<RtAssay, Long> {

    public RtAssayDAO() {
        super(RtAssay.class);
    }

    @Override
    public void saveOrUpdate(RtAssay assay) {
        if (assay != null) {
            super.saveOrUpdate(assay);
            getSession().getTransaction().commit();
        }
    }

    public void saveOrUpdateNotCommit(RtAssay assay) {
        if (assay != null) {
            super.saveOrUpdate(assay);
        }
    }

    public void doCommit() {
        getSession().getTransaction().commit();
    }

    @Override
    public RtAssay findById(Long id) {
        Query query = getSession().getNamedQuery("RtAssay.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (RtAssay) result.get(0);
        }
    }

    @Override
    public void delete(RtAssay assay) {
        assay.setIsActive(0L);
        getSession().saveOrUpdate(assay);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<RtAssay> getRtAssayList() {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtAssay d WHERE d.isActive is null OR d.isActive = 0 ");
        return query.list();
    }

    public List<RtAssay> getRtAssayActiveList() {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtAssay d WHERE d.isActive = 1 order by d.id");
        return query.list();
    }
}
