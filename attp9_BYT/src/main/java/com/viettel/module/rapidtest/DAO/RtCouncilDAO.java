/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.rapidtest.BO.RtCouncil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;

/**
 *
 * @author E5420
 */
public class RtCouncilDAO extends
        GenericDAOHibernate<RtCouncil, Long> {

    public RtCouncilDAO() {
        super(RtCouncil.class);
    }

    @Override
    public void saveOrUpdate(RtCouncil council) {
        if (council != null) {
            super.saveOrUpdate(council);
            getSession().getTransaction().commit();
        }
    }

    public void saveOrUpdateNotCommit(RtCouncil council) {
        if (council != null) {
            super.saveOrUpdate(council);
        }
    }

    public void doCommit() {
        getSession().getTransaction().commit();
    }

    @Override
    public RtCouncil findById(Long id) {
        Query query = getSession().getNamedQuery("RtCouncil.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (RtCouncil) result.get(0);
        }
    }

    @Override
    public void delete(RtCouncil council) {
        council.setIsActive(0L);
        getSession().saveOrUpdate(council);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<RtCouncil> getRtCouncilList() {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtCouncil d WHERE d.isActive is null OR d.isActive = 0 ");
        return query.list();
    }

    public List<RtCouncil> getRtCouncilActiveList() {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtCouncil d WHERE d.isActive = 1 order by d.orderBy");
        return query.list();
    }
    
    public PagingListModel search(RtCouncil searchForm, int start, int take) {
		List listParam = new ArrayList();
		try {
			StringBuilder strBuf = new StringBuilder(
					"select r from RtCouncil r where r.isActive = 1 ");
			StringBuilder strCountBuf = new StringBuilder(
					"select count(r) from RtCouncil r where r.isActive = 1 ");
			StringBuilder hql = new StringBuilder();
			if (searchForm != null) {
				if (searchForm.getName() != null
						&& !"".equals(searchForm.getName())) {
					hql.append(" and lower(r.name) like ? escape '/' ");
					listParam
							.add(StringUtils.toLikeString(searchForm.getName()));
				}
				if (searchForm.getRole() != null
						&& !"".equals(searchForm.getRole())) {
					hql.append(" and lower(r.role) like ? escape '/' ");
					listParam
							.add(StringUtils.toLikeString(searchForm.getRole()));
				}
				if (searchForm.getPosition() != null
						&& !"".equals(searchForm.getPosition())) {
					hql.append(" and lower(r.position) like ? escape '/' ");
					listParam.add(StringUtils.toLikeString(searchForm
							.getPosition()));
				}
				if (searchForm.getCode() != null
						&& !"".equals(searchForm.getCode())) {
					hql.append(" and lower(r.code) like ? escape '/' ");
					listParam
							.add(StringUtils.toLikeString(searchForm.getCode()));
				}
			}

			strBuf.append(hql);
			strCountBuf.append(hql);

			Query query = session.createQuery(strBuf.toString());
			Query countQuery = session.createQuery(strCountBuf.toString());

			for (int i = 0; i < listParam.size(); i++) {
				query.setParameter(i, listParam.get(i));
				countQuery.setParameter(i, listParam.get(i));
			}

			query.setFirstResult(start);
			if (take < Integer.MAX_VALUE) {
				query.setMaxResults(take);
			}
			List lst = query.list();
			Long count = (Long) countQuery.uniqueResult();
			PagingListModel model = new PagingListModel(lst, count);
			return model;
		} catch (Exception ex) {
			LogUtils.addLogDB(ex);
			return null;
		}
	}

    public void delete(Long id) {
		RtCouncil obj = findById(id);
		obj.setIsActive(0l);
		update(obj);
	}
    
    public boolean hasDuplicate(RtCouncil rs) {
		StringBuilder hql = new StringBuilder(
				"select count(r) from RtCouncil r where r.isActive = 1 ");
		List lstParams = new ArrayList();
		if (rs.getId() != null && rs.getId() > 0L) {
			hql.append(" and r.RtCouncil <> ?");
			lstParams.add(rs.getId());
		}

		Query query = session.createQuery(hql.toString());
		for (int i = 0; i < lstParams.size(); i++) {
			query.setParameter(i, lstParams.get(i));
		}
		Long count = (Long) query.uniqueResult();
		return count > 0L;
	}
}
