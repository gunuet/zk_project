/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.rapidtest.BO.RtTargetTesting;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;

/**
 *
 * @author E5420
 */
public class RtTargetTestingDAO extends
        GenericDAOHibernate<RtTargetTesting, Long> {

    public RtTargetTestingDAO() {
        super(RtTargetTesting.class);
    }

    @Override
    public void saveOrUpdate(RtTargetTesting targettesting) {
        if (targettesting != null) {
            super.saveOrUpdate(targettesting);
            getSession().getTransaction().commit();
        }
    }

    public void saveOrUpdateNotCommit(RtTargetTesting targettesting) {
        if (targettesting != null) {
            super.saveOrUpdate(targettesting);
        }
    }
	
    public void doCommit() {
        getSession().getTransaction().commit();
    }

    @Override
    public RtTargetTesting findById(Long id) {
        Query query = getSession().getNamedQuery("RtTargetTesting.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (RtTargetTesting) result.get(0);
        }
    }

    @Override
    public void delete(RtTargetTesting targettesting) {
        targettesting.setIsActive(0L);
        getSession().saveOrUpdate(targettesting);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<RtTargetTesting> getRtTargetTestingList(Long fileId) {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtTargetTesting d WHERE d.fileId = :fileId and (d.isActive is null OR d.isActive = '0') ");
        query.setParameter("fileId", fileId);
        return query.list();
    }

    public List<RtTargetTesting> getByFileId(Long fileId) {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtTargetTesting d WHERE d.fileId = :fileId and d.isActive = '1' ");
        query.setParameter("fileId", fileId);
        return query.list();
    }

    public RtTargetTesting getListByFileId(Long fileId) {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtTargetTesting d WHERE d.fileId = :fileId and d.isActive = '1' ");
        query.setParameter("fileId", fileId);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (RtTargetTesting) result.get(0);
        }
    }

    public void deleteByFileId(Long fileId) {
        Query query = getSession().createQuery("UPDATE  RtTargetTesting set isActive = 0 WHERE fileId = :fileId");
        query.setParameter("fileId", fileId);
        query.executeUpdate();
    }

    public void setIsTempTargetTesting(Long newfileId, Long oldfileId) {
        Query query = getSession()
                .createQuery(
                        "UPDATE RtTargetTesting SET fileId = :newfileId WHERE fileId = :oldfileId ");
        query.setParameter("newfileId", newfileId);
        query.setParameter("oldfileId", oldfileId);
        query.executeUpdate();
    }

}
