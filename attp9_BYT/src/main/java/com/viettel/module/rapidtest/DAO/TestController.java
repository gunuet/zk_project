package com.viettel.module.rapidtest.DAO;

import com.viettel.core.base.DAO.AttachDAO;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.sys.model.CategorySearchForm;
import com.viettel.core.workflow.DAO.FlowDAOHE;
import com.viettel.module.rapidtest.BO.RtFile;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.utils.Constants;
import com.viettel.utils.FileUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.voffice.BO.VFeeProcedure;

import com.viettel.voffice.BO.Ycnk.YcnkProduct;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestAttachDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ProcedureConfig;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.VFeeProcedureDAOHE;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class TestController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final int SAVE = 1;
    private final int SAVE_CLOSE = 2;
    private final int SAVE_COPY = 3;
    private final int RAPID_TEST_FILE_TYPE = 1;

    @Wire
    private Vlayout flist;
    @Wire
    private Button btnAttach;
    private List<Media> listMedia;
    private List listRapidTestFileType;
    // label for validating data
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    // Ý kiến lãnh đạo
    @Wire
    private Window windowCRUDRapidTest;
    private Window parentWindow;
    private String crudMode;

    private String isSave;
    @SuppressWarnings("rawtypes")
    private ListModel model;
    private List listDeptFlow;
    private RtFile rtFile;
    private Files files;
    private Attachs rtFileAttach;
    @Wire
    private Textbox tbRapidTestNo, tbRapidTestName, tbRapidTestCode, tbPlaceOfManufacture,
            tbOperatingPrinciples, tbTargetTesting, tbRangeOfApplications,
            tbLimitDevelopment, tbPrecision, tbDescription, tbPakaging,
            tbStorageConditions, tbOtherInfos, tbAttachmentsInfo,
            tbSignPlace, tbSignName,
            t2bRapidTestChangeNo, t2bRapidTestName, t2bRapidTestCode, t2bCirculatingRapidTestNo,
            t2bContens, t2bAttachmentsInfo, t2bSignPlace, t2bSignName,
            t3bCirculatingRapidTestExtensionNo, t3bRapidTestName,
            t3bRapidTestCode, t3bCirculatingRapidTestNo, t3bExtensionNo,
            t3bAttachmentsInfo, t3bSignPlace, t3bSignName;

    @Wire
    private Datebox dbSignDate,
            d2bDateIssue, d2bSignDate,
            d3bDateIssue, d3bDateEffect, d3bSignDate, dbShelfLife;
    @Wire
    private Listbox lboxDocumentTypeCode//loai ho so
            , lboxPropertiesTests,
            lbRapidTestFileType //loai tep dinh kem
            , fileListbox;
    private List<Attachs> listAttach = new ArrayList<>();
    @Wire
    private Groupbox gbRapidCRUD1, gbRapidCRUD2, gbRapidCRUD3;
    @Wire
    private Tabbox tb;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        FlowDAOHE flowDAOHE = new FlowDAOHE();
        //listDeptFlow = flowDAOHE.getDeptFlow(getDeptId(), Constants.OBJECT_TYPE.YCNK_FILE, null);
        listDeptFlow = flowDAOHE.getFlowByDeptNObject(getDeptId(), Constants.OBJECT_TYPE.YCNK_FILE);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        crudMode = (String) arguments.get("CRUDMode");
        listMedia = new ArrayList();
        switch (crudMode) {
            case "CREATE":// Tao moi 
                files = new Files();
                rtFile = new RtFile();
                break;
            case "UPDATE":// Sua 
                Long id = (Long) arguments.get("id");
                FilesDAOHE objFile = new FilesDAOHE();
                files = objFile.findById(id);
                RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
                rtFile = objDAOHE.findByFileId(id);
                break;
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        tb.setVisible(false);
        onSelectStatus();
        loadItemForEdit();
        if ("UPDATE".equals(crudMode)) {
            fillFileListbox(rtFile.getRtFileId());
            lboxDocumentTypeCode.setDisabled(true);
        }

    }

    private void loadItemForEdit() {
        Long documentTypeCode = rtFile.getDocumentTypeCode();
        if (documentTypeCode != null) {
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
                lboxDocumentTypeCode.setSelectedIndex(1);
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
                lboxDocumentTypeCode.setSelectedIndex(2);
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
                lboxDocumentTypeCode.setSelectedIndex(3);
            }

        }
        Long propertiesTests = rtFile.getPropertiesTests();
        if (propertiesTests != null) {
            if (Constants.RAPID_TEST.DINH_TINH.equals(propertiesTests)) {
                lboxPropertiesTests.setSelectedIndex(1);
            } else if (Constants.RAPID_TEST.BAN_DINH_TINH.equals(propertiesTests)) {
                lboxPropertiesTests.setSelectedIndex(2);
            } else if (Constants.RAPID_TEST.DINH_LUONG.equals(propertiesTests)) {

            }

        }
    }

    public void onSelectStatus() {

        gbRapidCRUD1.setVisible(true);
        gbRapidCRUD2.setVisible(false);
        gbRapidCRUD3.setVisible(false);
    }

    @Listen("onSelect = #lboxDocumentTypeCode")
    public void onSelect() {
        Listitem selectedItem = lboxDocumentTypeCode.getSelectedItem();
        if (selectedItem != null) {
            Long documentTypeCode = Long.valueOf((String) selectedItem.getValue());
            if (documentTypeCode != null) {
                if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
                    tb.setVisible(true);
                    gbRapidCRUD1.setVisible(true);
                    gbRapidCRUD2.setVisible(false);
                    gbRapidCRUD3.setVisible(false);
                } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
                    tb.setVisible(true);
                    gbRapidCRUD1.setVisible(false);
                    gbRapidCRUD2.setVisible(true);
                    gbRapidCRUD3.setVisible(false);
                } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
                    tb.setVisible(true);
                    gbRapidCRUD1.setVisible(false);
                    gbRapidCRUD2.setVisible(false);
                    gbRapidCRUD3.setVisible(true);
                } else {
                    tb.setVisible(false);
                }

            }
        }
    }

    /**
     * linhdx Hàm load danh sach trong dropdown control
     *
     * @param type
     * @return
     */
    public ListModelList getListBoxModel(int type) {
        CategoryDAOHE categoryDAOHE;
        ListModelList lstModel;
        switch (type) {
            case RAPID_TEST_FILE_TYPE:
                categoryDAOHE = new CategoryDAOHE();
                listRapidTestFileType = categoryDAOHE.findAllCategoryByTypeAndDept(
                        Constants.CATEGORY_TYPE.RAPID_TEST_FILE_TYPE, getDeptId());
                Category catFirst = new Category();
                catFirst.setCategoryId(-1L);
                catFirst.setName("-------------Chọn------------");
                listRapidTestFileType.add(0, catFirst);
                lstModel = new ListModelList(listRapidTestFileType);
                return lstModel;
        }
        return new ListModelList();

    }

    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        return selectedItem;
    }

    /**
     * linhdx Ham tao doi tuong tu form de luu lai
     *
     * @return
     */
    private RtFile createRtFile() {
        rtFile.setFileId(files.getFileId());
        Long documentTypeCode = Long.valueOf((String) lboxDocumentTypeCode.getSelectedItem().getValue());
        if (documentTypeCode != null) {
            rtFile.setDocumentTypeCode(documentTypeCode);
            //rtFile.setStatusCode(Constants.RAPID_TEST_STATUS.NEW);
            String nswFileCode = getAutoNswFileCode(documentTypeCode);
            rtFile.setNswFileCode(nswFileCode);
            if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI.equals(documentTypeCode)) {
                rtFile.setRapidTestNo(tbRapidTestNo.getValue());
                rtFile.setRapidTestName(tbRapidTestName.getValue());
                rtFile.setRapidTestCode(tbRapidTestCode.getValue());
                rtFile.setPlaceOfManufacture(tbPlaceOfManufacture.getValue());
                rtFile.setOperatingPrinciples(tbOperatingPrinciples.getValue());
                rtFile.setTargetTesting(tbTargetTesting.getValue());
                rtFile.setRangeOfApplications(tbRangeOfApplications.getValue());
                rtFile.setLimitDevelopment(tbLimitDevelopment.getValue());
                rtFile.setPrecision(tbPrecision.getValue());
                rtFile.setDescription(tbDescription.getValue());
                rtFile.setPakaging(tbPakaging.getValue());
                rtFile.setStorageConditions(tbStorageConditions.getValue());
                rtFile.setOtherInfos(tbOtherInfos.getValue());
                rtFile.setAttachmentsInfo(tbAttachmentsInfo.getValue());
                rtFile.setSignPlace(tbSignPlace.getValue());
                rtFile.setSignName(tbSignName.getValue());
                rtFile.setShelfLife(dbShelfLife.getValue().toString());
                rtFile.setSignDate(dbSignDate.getValue());
                String proTestStr = lboxPropertiesTests.getSelectedItem().getValue();
                rtFile.setPropertiesTests(Long.valueOf(proTestStr));
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG.equals(documentTypeCode)) {
                rtFile.setRapidTestChangeNo(t2bRapidTestChangeNo.getValue());
                rtFile.setRapidTestName(t2bRapidTestName.getValue());
                rtFile.setRapidTestCode(t2bRapidTestCode.getValue());
                rtFile.setCirculatingRapidTestNo(t2bCirculatingRapidTestNo.getValue());
                rtFile.setContents(t2bContens.getValue());
                rtFile.setAttachmentsInfo(t2bAttachmentsInfo.getValue());
                rtFile.setSignPlace(t2bSignPlace.getValue());
                rtFile.setSignName(t2bSignName.getValue());

                rtFile.setDateIssue(d2bDateIssue.getValue());
                rtFile.setSignDate(d2bSignDate.getValue());
            } else if (Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN.equals(documentTypeCode)) {
                rtFile.setCirculatingExtensionNo(t3bCirculatingRapidTestExtensionNo.getValue());
                rtFile.setRapidTestName(t3bRapidTestName.getValue());
                rtFile.setRapidTestCode(t3bRapidTestCode.getValue());
                rtFile.setCirculatingRapidTestNo(t3bCirculatingRapidTestNo.getValue());

                //rtFile.setExtensionNo();
                rtFile.setAttachmentsInfo(t3bAttachmentsInfo.getValue());
                rtFile.setSignPlace(t3bSignPlace.getValue());
                rtFile.setSignName(t3bSignName.getValue());
                rtFile.setDateIssue(d3bDateIssue.getValue());
                rtFile.setDateEffect(d3bDateEffect.getValue());
                rtFile.setSignDate(d3bSignDate.getValue());
            }

        }

        return rtFile;
    }

    private Files createFile() throws Exception {
        Date dateNow = new Date();
        files.setCreateDate(dateNow);
        files.setModifyDate(dateNow);
        files.setCreatorId(getUserId());
        files.setCreatorName(getUserFullName());
        files.setCreateDeptId(getDeptId());
        files.setCreateDeptName(getDeptName());
        files.setIsActive(Constants.Status.ACTIVE);

        // viethd 29/01/2015
        // set status of file is INIT
        files.setStatus(Constants.PROCESS_STATUS.INITIAL);
        files.setFileType(ProcedureConfig.PROCEDURE_RAPID_TEST_KIT);
        return files;
    }

    private Files createFileUpdate() throws Exception {
        Date dateNow = new Date();
        files.setModifyDate(dateNow);
        return files;
    }

    public String getAutoNswFileCode(Long documentTypeCode) {
        CategorySearchForm form = new CategorySearchForm();
        form.setType(Constants.RAPID_TEST.TYPE_CONFIG);
        form.setCode(Constants.RAPID_TEST.CODE_CONFIG);
        CategoryDAOHE obj = new CategoryDAOHE();
        List<Category> lstCategory = obj.findCategory(form);
        String autoNumber = "000001";
        if (lstCategory != null && lstCategory.size() > 0) {
            Category ca = lstCategory.get(0);
            autoNumber = ca.getValue();
            ca.setValue(String.valueOf(Long.valueOf(autoNumber) + 1));
            obj.saveOrUpdate(ca);//Tang chi so len
        }
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        int len = String.valueOf(autoNumber).length();
        if (len < 6) {
            for (int a = len; a < 6; a++) {
                autoNumber = "0" + autoNumber;
            }
        }
        return documentTypeCode + year.toString() + autoNumber;

    }

    // Load danh sach cac file dinh kem cua van ban den
    // va hien thi tren giao dien
    private void loadFileAttach(List<Attachs> listFileAttach) {
        if (listFileAttach != null) {
            for (final Attachs attach : listFileAttach) {
                // layout hien thi ten file va nut "Xoa"
                final Hlayout hl = new Hlayout();
                hl.setSpacing("6px");
                hl.setClass("newFile");
                hl.appendChild(new Label(attach.getAttachName()));
                A rm = new A("Xóa");
                rm.addEventListener(Events.ON_CLICK,
                        new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        hl.detach();
                        // Xoa file khoi he thong file
                        // tam thoi khong can

                        // Set attach deactive
                        attach.setIsActive(Constants.Status.INACTIVE);
                        AttachDAOHE daoHE = new AttachDAOHE();
                        daoHE.saveOrUpdate(attach);
                    }
                });
                hl.appendChild(rm);
                flist.appendChild(hl);
            }
        }
    }

    private void resetObject() {
        rtFile = new RtFile();

    }

    private void resetForm() {
        crudMode = "CREATE";

    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbRapidTestNo.getText().matches("\\s*")) {
            showWarningMessage("Số hồ sơ không thể để trống");
            tbRapidTestNo.focus();
            return false;
        }
        if (tbRapidTestName.getText().matches("\\s*")) {
            showWarningMessage("Tên hồ sơ không thể để trống");
            tbRapidTestName.focus();
            return false;
        }
        if (tbRapidTestCode.getText().matches("\\s*")) {
            showWarningMessage("Ký hiệu (Mã hiệu) hồ sơ không thể để trống");
            tbRapidTestCode.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSave(SAVE);
                break;
            case KeyEvent.F7:
                onSave(SAVE_CLOSE);
                break;
            case KeyEvent.F8:
                onSave(SAVE_COPY);
                break;
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @param typeSave
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave(int typeSave) throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }

//        if (listDeptFlow == null) {
//            showNotification("Đơn vị chưa có luồng định nghĩa",
//                    Constants.Notification.WARNING);
//            return;
//        }
        try {
            if (null != crudMode) {
                // Save to 
                createObject();
                VFeeProcedureDAOHE vFeeFileDAOHE = new VFeeProcedureDAOHE();
                List<VFeeProcedure> lstVFeeProcedure = vFeeFileDAOHE.getListFee(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
                switch (crudMode) {
                    case "CREATE": {
                        Long fileId = rtFile.getFileId();
                        if (lstVFeeProcedure.size() > 0) {
                            PaymentInfoDAO feePaymentInfoDAOHE = new PaymentInfoDAO();
                            List lst = feePaymentInfoDAOHE.getListPayment(fileId);
                            if (lst.isEmpty()) {
                                //Neu co ban ghi thanh toan thi khong luu nua
                                for (VFeeProcedure obj : lstVFeeProcedure) {

                                    PaymentInfo feePaymentInfo = new PaymentInfo();
                                    feePaymentInfo.setFileId(fileId);
                                    feePaymentInfo.setFeeId(obj.getFeeId());
                                    feePaymentInfo.setFeeName(obj.getName());
                                    feePaymentInfo.setIsActive(Constants.Status.ACTIVE);
                                    String value = obj.getCost();
                                    Long valueL = 0L;
                                    try {
                                        valueL = Long.valueOf(value);
                                    } catch (Exception ex) {
                                        LogUtils.addLogDB(ex);
                                    }
                                    feePaymentInfo.setCost(valueL);
                                    feePaymentInfoDAOHE.saveOrUpdate(feePaymentInfo);

                                }
                            }

                        }

                        showNotification(String.format(
                                Constants.Notification.SAVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                                Constants.Notification.INFO);
                        break;
                    }
                    case "UPDATE":
                        createFileUpdate();
                        showNotification(String.format(
                                Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                                Constants.Notification.INFO);
                        break;

                }

            }

            //resetObject();
            crudMode = "CREATE";
            isSave = "1";
            switch (typeSave) {
                case SAVE:
                    resetForm();
                    break;
                case SAVE_CLOSE:
                    //Events.sendEvent("onSave", parentWindow, null);
                    //windowCRUDRapidTest.onClose();

                    break;
                case SAVE_COPY:
                    break;
            }
        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            if (null != crudMode) {
                switch (crudMode) {
                    case "CREATE":
                        showNotification(String.format(
                                Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                                Constants.Notification.ERROR);
                        break;
                    case "UPDATE":
                        showNotification(String.format(
                                Constants.Notification.UPDATE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                                Constants.Notification.ERROR);
                        break;
                }
            }
        }
    }

    private void createObject() throws Exception {
        FilesDAOHE filesDAOHE = new FilesDAOHE();
        files = createFile();
        filesDAOHE.saveOrUpdate(files);
        RapidTestDAOHE rtFileDAOHE = new RapidTestDAOHE();
        rtFile = createRtFile();
        rtFileDAOHE.saveOrUpdate(rtFile);
    }

    @Listen("onClick=#btnCreate")
    public void onCreateRapidTestFileType() throws IOException, Exception {

        //Neu chua luu thong tin ho so thi luu lai truoc khi cho phep tao san pham
        RapidTestDAOHE rtFileDAOHE = new RapidTestDAOHE();
        if (files.getFileId() == null) {
            if (!isValidatedData()) {
                return;
            }
            //linhdx
            //Luu thong tin ho so truoc khi tao

            createObject();
        }
        Long rtFileFileType = Long.valueOf((String) lbRapidTestFileType.getSelectedItem().getValue());
        AttachDAO base = new AttachDAO();
        for (Media media : listMedia) {
            base.saveFileAttach(media, files.getFileId(), Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE, rtFileFileType);
        }

        rtFileDAOHE.flush();
        fillFileListbox(files.getFileId());
    }

    private void fillFileListbox(Long fileId) {
        RapidTestAttachDAOHE rDaoHe = new RapidTestAttachDAOHE();
        List<VFileRtAttach> lstRapidTestAttach = rDaoHe.findRapidTestAttach(fileId);
        this.fileListbox.setModel(new ListModelArray(lstRapidTestAttach));
        onSelect();//An hien form
    }

    @Listen("onDeleteFile = #fileListbox")
    public void onDeleteFile(Event event) {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long fileId = obj.getObjectId();
        RapidTestAttachDAOHE rDAOHE = new RapidTestAttachDAOHE();
        rDAOHE.delete(obj.getAttachId());
        fillFileListbox(fileId);
    }

    @Listen("onDownloadFile = #fileListbox")
    public void onDownloadFile(Event event) throws FileNotFoundException {
        VFileRtAttach obj = (VFileRtAttach) event.getData();
        Long attachId = obj.getAttachId();
        AttachDAOHE attDAOHE = new AttachDAOHE();
        Attachs att = attDAOHE.findById(attachId);

        String path = att.getAttachPath()
                + att.getAttachId();
        File f = new File(path);
        if (f.exists()) {
            Filedownload.save(f, att.getAttachName());
        } else {
            showNotification("File không còn tồn tại trên hệ thống",
                    Constants.Notification.INFO);
        }
    }

    /**
     * Xu ly su kien upload file
     *
     * @param event
     */
    @Listen("onUpload = #btnAttach")
    public void onUpload(UploadEvent event) throws UnsupportedEncodingException {
        // final Media media = event.getMedia();
        final Media[] medias = event.getMedias();
        for (final Media media : medias) {
            String extFile = media.getName().replace("\"", "");
            if (!FileUtil.validFileType(extFile)) {
                String sExt = ResourceBundleUtil.getString("extend_file", "config");
                showNotification("Định dạng file không được phép tải lên (" + sExt + ")",
                        Constants.Notification.WARNING);
                return;
            }

            // luu file vao danh sach file
            listMedia.add(media);

            // layout hien thi ten file va nut "Xóa";
            final Hlayout hl = new Hlayout();
            hl.setSpacing("6px");
            hl.setClass("newFile");
            hl.appendChild(new Label(media.getName()));
            A rm = new A("Xóa");
            rm.addEventListener(Events.ON_CLICK,
                    new org.zkoss.zk.ui.event.EventListener() {
                @Override
                public void onEvent(Event event) throws Exception {
                    hl.detach();
                    // xoa file khoi danh sach file
                    listMedia.remove(media);
                }
            });
            hl.appendChild(rm);
            flist.appendChild(hl);
        }
    }

    /**
     * Dong cua so khi o dang popup
     */
    @Listen("onClose = #windowCRUDRapidTest")
    public void onClose() {
        windowCRUDRapidTest.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onVisible = #windowCRUDRapidTest")
    public void onVisible() {

        windowCRUDRapidTest.setVisible(true);
    }

    public ListModel getModel() {
        return model;
    }

    public void setModel(ListModel model) {
        this.model = model;
    }

    public RtFile getRapidTest() {
        return rtFile;
    }

    public boolean isCRUDProductMenu() {
        return true;
    }

    public boolean isAbleToDeleteProduct(YcnkProduct ycnkProduct) {
        return true;
    }

    public boolean isAbleToModifyProduct(YcnkProduct ycnkProduct) {
        return true;
    }

    public String getIsSave() {
        return isSave;
    }

    public void setIsSave(String isSave) {
        this.isSave = isSave;
    }

}
