/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import java.util.List;

import org.hibernate.Query;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.module.rapidtest.BO.VFileRtfile;

/**
 *
 * @author E5420
 */
public class VFileRtflieDAO extends
        GenericDAOHibernate<VFileRtfile, Long> {

    public VFileRtflieDAO() {
        super(VFileRtfile.class);
    }

    @Override
    public void saveOrUpdate(VFileRtfile assay) {
        if (assay != null) {
            super.saveOrUpdate(assay);
            getSession().getTransaction().commit();
        }
    }

    public void saveOrUpdateNotCommit(VFileRtfile assay) {
        if (assay != null) {
            super.saveOrUpdate(assay);
        }
    }

    public void doCommit() {
        getSession().getTransaction().commit();
    }

    @Override
    public VFileRtfile findById(Long id) {
        Query query = getSession().getNamedQuery("VFileRtfile.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (VFileRtfile) result.get(0);
        }
    }

    @Override
    public void delete(VFileRtfile assay) {
        assay.setIsActive(0L);
        getSession().saveOrUpdate(assay);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<RtAssay> getRtAssayList() {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtAssay d WHERE d.isActive is null OR d.isActive = 0 ");
        return query.list();
    }

    public List<RtAssay> getRtAssayActiveList() {
        Query query = getSession()
                .createQuery(
                        "SELECT d FROM RtAssay d WHERE d.isActive = 1 order by d.id");
        return query.list();
    }
  
}
