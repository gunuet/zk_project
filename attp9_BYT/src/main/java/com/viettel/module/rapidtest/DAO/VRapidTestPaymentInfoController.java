package com.viettel.module.rapidtest.DAO;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.module.rapidtest.BO.RtFile;
import com.viettel.module.rapidtest.BO.RtFileOld;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.Attachs;

import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.voffice.model.DocumentProcess;
import com.viettel.voffice.model.SearchModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.databind.BindingListModelList;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.PagingEvent;

/**
 *
 * @author ChucHV
 */
public class VRapidTestPaymentInfoController extends BaseComposer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Wire("#incSearchFullForm #dbFromDay")
    private Datebox dbFromDay;
    @Wire("#incSearchFullForm #dbToDay")
    private Datebox dbToDay;
    @Wire("#incSearchFullForm #fullSearchGbx")
    private Groupbox fullSearchGbx;
    
    @Wire("#incSearchFullForm #tbNSWFileCode")
    private Textbox tbNSWFileCode;
    
    @Wire("#incSearchFullForm #tbRapidTestNo")
    private Textbox tbRapidTestNo;
    
    @Wire("#incSearchFullForm #lboxStatus")
    private Listbox lboxStatus;
    
    @Wire("#incSearchFullForm #lboxDocumentTypeCode")
    private Listbox lboxDocumentTypeCode;
    
    // End search form
    @Wire
    private Paging userPagingTop, userPagingBottom;
    @Wire
    private Listbox lbList;
    @Wire
    private Window rapidTestAll;
    private int _totalSize = 0;
    private SearchModel lastSearchModel;
    private final Long deptId = getDeptId();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        onSearch();
    }

    /*
     * isPaging: neu chuyen page thi khong can lay _totalSize
     */
    private void reloadModel(Long deptId,SearchModel searchModel, boolean isPaging) {
        searchModel.setMenuType(Constants.DOCUMENT_MENU.ALL);
        RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
        List listRapidTest = objDAOHE.getList(deptId,
                searchModel, userPagingTop.getActivePage(),
                userPagingTop.getPageSize(), false); 

        if (!isPaging) {
            List result = objDAOHE.getList(deptId,
                    searchModel, userPagingTop.getActivePage(),
                    userPagingTop.getPageSize(), true);
            if (result.isEmpty()) {
                _totalSize = 0;
            } else {
                _totalSize = ((Long) result.get(0)).intValue();
            }
        }
        userPagingTop.setTotalSize(_totalSize);
        userPagingBottom.setTotalSize(_totalSize);
        lbList.setModel(new BindingListModelList<>(listRapidTest, true));

    }

    @Listen("onAfterRender = #lbList")
    public void onAfterRender() {
    }

    @Listen("onPaging = #userPagingTop, #userPagingBottom")
    public void onPaging(Event event) {
        final PagingEvent pe = (PagingEvent) event;
        if (userPagingTop.equals(pe.getTarget())) {
            userPagingBottom.setActivePage(userPagingTop.getActivePage());
        } else {
            userPagingTop.setActivePage(userPagingBottom.getActivePage());
        }
        reloadModel(deptId, lastSearchModel, true);
    }

    /*
     * Xu li su kien tim kiem simple
     */
    @Listen("onSearchFullText = #rapidTestAll")
    public void onSearchFullText(Event event) {
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            reloadModel(deptId, lastSearchModel, false);
        }
    }

    @Listen("onShowFullSearch=#rapidTestAll")
    public void onShowFullSearch() {
        if (fullSearchGbx.isVisible()) {
            fullSearchGbx.setVisible(false);
        } else {
            fullSearchGbx.setVisible(true);
            //Events.sendEvent("onLoadBookIn", fullSearchGbx, null);
        }
    }

    @Listen("onChangeTime=#rapidTestAll")
    public void onChangeTime(Event e) {
        String data = e.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);
        if (model != null) {
            dbFromDay.setValue(model.getFromDate());
            dbToDay.setValue(model.getToDate());
            onSearch();
        }
    }

    @Listen("onClick = #incSearchFullForm #btnSearch")
    public void onSearch() {userPagingBottom.setActivePage(0); 	userPagingTop.setActivePage(0);
        lastSearchModel = new SearchModel();
        lastSearchModel.setCreateDateFrom(dbFromDay.getValue());
        lastSearchModel.setCreateDateTo(dbToDay.getValue());
        lastSearchModel.setnSWFileCode(tbNSWFileCode.getValue());
        lastSearchModel.setRapidTestNo(tbRapidTestNo.getValue());
        
        if (lboxStatus.getSelectedItem() != null) {
            String value = lboxStatus.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if(valueL != -1){
                    lastSearchModel.setStatus(valueL);
                }
            }
        }
        
        if (lboxDocumentTypeCode.getSelectedItem() != null) {
            String value = lboxDocumentTypeCode.getSelectedItem().getValue();
            if (value != null) {
                Long valueL = Long.valueOf(value);
                if(valueL != -1){
                    lastSearchModel.setDocumentTypeCode(valueL);
                }
            }
        }
       
        
       
        
        reloadModel(deptId, lastSearchModel, false);
        
    }

    @Listen("onOpenCreate=#rapidTestAll")
    public void onOpenCreate() {
        Map<String, Object> arguments = new ConcurrentHashMap();
        arguments.put("CRUDMode", "CREATE");
        arguments.put("parentWindow", rapidTestAll);
        Window window = createWindow("windowCRUD",
                "/Pages/rapidTest/rapidTestCRUD.zul", arguments,
                Window.EMBEDDED);
        window.setMode(Window.Mode.EMBEDDED);
        rapidTestAll.setVisible(false);
    }

    @Listen("onOpenView = #lbList")
    public void onOpenView(Event event) {
        RtFileOld obj = (RtFileOld) event.getData();
        createWindowView(obj.getId(), Constants.DOCUMENT_MENU.ALL,
                rapidTestAll);
        rapidTestAll.setVisible(false);

    }

    @Listen("onSelectedProcess = #rapidTestAll")
    public void onSelectedProcess(Event event) {
        rapidTestAll.setVisible(false);

    }

    private void createWindowView(Long id, int menuType,
            Window parentWindow) {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", id); 
        arguments.put("menuType", menuType);
        arguments.put("parentWindow", parentWindow);
        createWindow("windowView", "/Pages/rapidTest/viewRapidTest.zul",
                arguments, Window.EMBEDDED);
    }

    @Listen("onDownloadAttach=#lbList")
    public void onDownloadAttach(Event event) throws FileNotFoundException {
        DocumentProcess docProcess = (DocumentProcess) event.getData();
        AttachDAOHE adhe = new AttachDAOHE();
        List<Attachs> lstAttachs = adhe.getByObjectIdAndType(docProcess
                .getDocumentReceive().getDocumentReceiveId(),
                Constants.OBJECT_TYPE.DOCUMENT_RECEIVE);
        if (lstAttachs == null || lstAttachs.isEmpty()) {
            showNotification("Không có file đính kèm",
                    Constants.Notification.INFO);
        } else if (lstAttachs.size() == 1) {
            String path = lstAttachs.get(0).getAttachPath() + File.separator
                    + lstAttachs.get(0).getAttachId();
            File f = new File(path);
            if (f.exists()) {
                Filedownload.save(lstAttachs.get(0).getAttachPath()
                        + lstAttachs.get(0).getAttachId(), null, lstAttachs
                        .get(0).getAttachName());
            } else {
                showNotification("File không còn tồn tại trên hệ thống",
                        Constants.Notification.INFO);
            }
        } else {
            Map<String, Object> args = new ConcurrentHashMap<>();
            args.put("objectId", docProcess.getDocumentReceive()
                    .getDocumentReceiveId());
            args.put("objectType", Constants.OBJECT_TYPE.DOCUMENT_RECEIVE);
            createWindow("downloadWnd", "/Pages/common/downloadSelect.zul",
                    args, Window.MODAL);
        }
    }

    @Listen("onSave = #rapidTestAll")
    public void onSave(Event event) {
        
        rapidTestAll.setVisible(true);
        try {
            onSearch();
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
    }
    /**
     * Khi cac window Create, Update, View dong thi gui su kien hien thi  
     * windowDocIn len 
     * Con cac su kien save, update thi da xu li hien thi 
     * windowDocIn trong phuong thuc onSave
     */

    @Listen("onVisible = #rapidTestAll")
    public void onVisible() {
        reloadModel(deptId, lastSearchModel, false);
        rapidTestAll.setVisible(true);
    }

    @Listen("onOpenUpdate = #lbList")
    public void onOpenUpdate(Event event) {
        RtFileOld obj = (RtFileOld) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("id", obj.getId());
        arguments.put("CRUDMode", "UPDATE");
        arguments.put("parentWindow", rapidTestAll);
        createWindow("windowCRUDRapidTest", "/Pages/rapidTest/rapidTestCRUD.zul",
                arguments, Window.EMBEDDED);
        rapidTestAll.setVisible(false);
    }
    
    @Listen("onViewFlow = #lbList")
    public void onViewFlow(Event event) {
        
        Map args = new ConcurrentHashMap();

        createWindow("flowConfigDlg", "/Pages/admin/flow/flowCurrentView.zul",
                args, Window.MODAL);
    }

    @Listen("onDelete = #lbList")
    public void onDelete(Event event) {
        final RtFile obj = (RtFile) event.getData();
        String message = String.format(Constants.Notification.DELETE_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.FILE);

        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    try {
                                        RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
                                        objDAOHE.delete(obj);
                                        onSearch();
                                        showNotification(String.format(Constants.Notification.DELETE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.INFO);
                                    } catch (Exception e) {
                                        showNotification(String.format(Constants.Notification.DELETE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE), Constants.Notification.ERROR);
                                        LogUtils.addLogDB(e);
                                    } finally {
                                    }
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });
    }



    public boolean isCRUDMenu() {
        return true;
    }

    public boolean isAbleToDelete(RtFileOld obj) {
        return true;
    }

    public boolean isAbleToModify(RtFileOld obj) {
        return true;
    }
}
