/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.cosmetic.Model.CosmeticFileModel;
import com.viettel.module.payment.BO.VRtPaymentInfo;
import com.viettel.module.payment.BO.VCosPaymentInfo;
import com.viettel.module.payment.search.SearchPaymentModel;
import com.viettel.utils.Constants;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.model.SearchModel;

/**
 *
 * @author Linhdx
 */
public class VRtPaymentinfoDAO extends GenericDAOHibernate<VRtPaymentInfo, Long> {

    public VRtPaymentinfoDAO() {
        super(VRtPaymentInfo.class);
    }

    public List getListFileId(Long fileID) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1   ");

        docHQL.append(" AND d.fileId = ? ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, fileID);
        List<VRtPaymentInfo> listObj;

        listObj = fileQuery.list();

        return listObj;
    }
    
    public List getListFileIdPhase(Long fileID, Long phase) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1   ");

        docHQL.append(" AND d.fileId = ? ");
        docHQL.append(" AND d.phase = ? ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, fileID);
        fileQuery.setParameter(1, phase);
        List<VRtPaymentInfo> listObj;

        listObj = fileQuery.list();

        return listObj;
    }

    public List getList(Long deptId,
            SearchPaymentModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VCosPaymentInfo d WHERE d.isActive = 1  ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VCosPaymentInfo d WHERE  d.isActive = 1  ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();

        docHQL.append(" AND d.status = ? ");
        docParams.add(Constants.RAPID_TEST.PAYMENT.PAY_ALREADY);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.cosFileId) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatusPayment() != null) {
                docHQL.append(" AND d.statuspayment = ?");
                docParams.add(model.getStatusPayment());
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }
        }

        docHQL.append(" order by fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        List<VCosPaymentInfo> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    // payment cho doanh nghiep
    public List getListRequestPayment(Long deptId,
            SearchPaymentModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VCosPaymentInfo d WHERE d.isActive = 1 ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VCosPaymentInfo d WHERE d.isActive = 1  ");
        }
        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        docHQL.append(" AND d.createDeptId = ? ");
        docParams.add(deptId);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.cosFileId) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatusPayment() != null) {
                docHQL.append(" AND d.statuspayment = ?");
                docParams.add(model.getStatusPayment());
            } else {
                docHQL.append(" AND (d.statuspayment = 1 OR d.statuspayment = 4) ");
            }
            if (model.getStatusFile() != null) {
                docHQL.append(" AND d.statusfile = ?");
                docParams.add(model.getStatusFile());
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }

            if (model.getNameCost() != null) {
                docHQL.append(" AND d.phase = ?");
                docParams.add(model.getNameCost());
            }
            //d.status is not null
            if (model.getTypeOrder() != null) {

                if (model.getTypeOrder() == 1L) {
                    docHQL.append(" order by d.rapidTestNo ");
                } else if (model.getTypeOrder() == 2L) {
                    docHQL.append(" order by d.documentTypeCode ");
                } else if (model.getTypeOrder() == 3L) {
                    docHQL.append(" order by d.rapidTestName ");
                } else {
                    docHQL.append(" order by d.fileId desc");
                }

            } else {
                docHQL.append(" order by d.fileId desc");
            }

        }
        Query fileQuery = session.createQuery(docHQL.toString());
        //LogUtils.addLogs(docHQL.toString());
        List<VCosPaymentInfo> listObj;

        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            //LogUtils.addLog("size" + fileQuery.list().size());

            return fileQuery.list();

        } else {
            if (activePage >= 0) {

                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();

        }

        return listObj;

    }

    public List getListRequestAcceptPayment(Long deptId,
            SearchPaymentModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VCosPaymentInfo d WHERE d.isActive = 1   ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VCosPaymentInfo d WHERE d.isActive = 1   ");
        }

        /*
         * model nhất định phải != null
         */

        List docParams = new ArrayList();

        //docHQL.append(" AND d.createDeptId = ? ");
        //docParams.add(deptId);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.cosFileId) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatusPayment() != null) {
                docHQL.append(" AND d.statuspayment = ?");
                docParams.add(model.getStatusPayment());
            } else {
                docHQL.append(" AND (d.statuspayment = 2 OR d.statuspayment = 3 OR d.statuspayment = 4)");
            }
            if (model.getStatusFile() != null) {
                docHQL.append(" AND d.statusfile = ?");
                docParams.add(model.getStatusFile());
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }


            if (model.getTypeOrder() != null) {

                if (model.getTypeOrder() == 1L) {
                    docHQL.append(" order by d.rapidTestNo ");
                } else if (model.getTypeOrder() == 2L) {
                    docHQL.append(" order by d.documentTypeCode ");
                } else if (model.getTypeOrder() == 3L) {
                    docHQL.append(" order by d.rapidTestName ");
                } else {
                    docHQL.append(" order by d.fileId desc");
                }
            } else {
                docHQL.append(" order by d.fileId desc");
            }

        }



        Query fileQuery = session.createQuery(docHQL.toString());
        List<VCosPaymentInfo> listObj;

        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    // danh sach da thanh toan theo payment info id
    public List getListRequestPaymentInfoId(Long deptId,
            Long paymentInfoId) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT d FROM VCosPaymentInfo d WHERE d.isActive = 1   ");

        docHQL.append(" AND d.paymentInfoId = ? ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, paymentInfoId);
        List<VCosPaymentInfo> listObj;

        listObj = fileQuery.list();

        return listObj;

    }

    // danh sach da thanh toan theo id
    public List getListRequestPaymentBillId(Long deptId,
            Long BillId) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT d FROM VCosPaymentInfo d WHERE d.isActive = 1   ");
        docHQL.append(" AND d.billId = ? ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, BillId);

        List<VCosPaymentInfo> listObj;

        listObj = fileQuery.list();

        return listObj;

    }

    // Danh sach chua thanh toan
    public List getListRequestPaymentNotPayment(Long creatorId) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT d FROM VCosPaymentInfo d WHERE d.isActive = 1   ");

        docHQL.append(" AND d.statuspayment = 1 ");
        docHQL.append(" AND d.statusfile = 0 ");
        docHQL.append(" AND d.creatorId = ? ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        fileQuery.setParameter(0, creatorId);
        List<VCosPaymentInfo> listObj;
        listObj = fileQuery.list();

        return listObj;

    }

//
    public List getList(Long deptId,
            SearchModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VCosPaymentInfo d WHERE d.isActive = 1   ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VCosPaymentInfo d WHERE d.isActive = 1   ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        docHQL.append(" AND d.status = ? ");
        docParams.add(Constants.RAPID_TEST.PAYMENT.PAY_ALREADY);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.cosFileId) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatus() != null) {
                docHQL.append(" AND d.status = ?");
                docParams.add(model.getStatus());
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }

        }

        docHQL.append(" order by fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        List<VCosPaymentInfo> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    public List getListRequestPayment(Long deptId,
            SearchModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VCosPaymentInfo d WHERE d.isActive = 1   ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VCosPaymentInfo d WHERE d.isActive = 1   ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        docHQL.append(" AND d.status is not null ");
        //docParams.add(Constants.RAPID_TEST.PAYMENT.PAY_NEW);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.cosFileId) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatus() != null) {
                docHQL.append(" AND d.statusCode = ?");
                docParams.add(model.getStatus());
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }
        }

        docHQL.append(" order by fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        List<VCosPaymentInfo> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    public PagingListModel findFilesByReceiverAndDeptIdKT(SearchPaymentModel searchModel,
            Long receiverId, Long receiveDeptId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(d) ");
        StringBuilder strCountBuf = new StringBuilder("select count(d) ");
        StringBuilder hql = new StringBuilder();
        hql.append(" FROM VCosPaymentInfo d, Process p WHERE"
                + " d.fileId = p.objectId AND "
                + " d.fileType = p.objectType AND "
                + " p.receiveUserId = ? ");
        listParam.add(receiverId);


        //listParam.add(receiverId);
        //listParam.add(receiveDeptId);
        if (searchModel != null) {
            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND d.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND d.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(d.cosFileId) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatusPayment() != null) {
                hql.append(" AND d.statuspayment = ?");
                listParam.add(searchModel.getStatusPayment());
            } else {
                hql.append(" AND (d.statuspayment = 2 OR d.statuspayment = 3 OR d.statuspayment = 4)");
            }
            if (searchModel.getStatusFile() != null) {
                hql.append(" AND d.statusfile = ?");
                listParam.add(searchModel.getStatusFile());
            }
            if (searchModel.getDocumentTypeCode() != null) {
                hql.append(" AND d.documentTypeCode = ?");
                listParam.add(searchModel.getDocumentTypeCode());
            }


            if (searchModel.getTypeOrder() != null) {

                if (searchModel.getTypeOrder() == 1L) {
                    hql.append(" order by d.rapidTestNo ");
                } else if (searchModel.getTypeOrder() == 2L) {
                    hql.append(" order by d.documentTypeCode ");
                } else if (searchModel.getTypeOrder() == 3L) {
                    hql.append(" order by d.rapidTestName ");
                } else {
                    hql.append(" order by d.billId desc");
                }
            } else {
                hql.append(" order by d.billId desc");
            }


        }
        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    public PagingListModel findFilesByReceiverAndDeptId(SearchPaymentModel searchModel,
            Long receiverId, Long receiveDeptId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(d) ");
        StringBuilder strCountBuf = new StringBuilder("select count(d) ");
        StringBuilder hql = new StringBuilder();

        hql.append(" FROM VCosPaymentInfo d, Process p WHERE"
                + " d.fileId = p.objectId AND "
                + " d.fileType = p.objectType AND "
                + " p.receiveUserId = ? ");
        listParam.add(receiverId);


        //listParam.add(receiverId);
        //listParam.add(receiveDeptId);
        if (searchModel != null) {
            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND d.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND d.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(d.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getBrandName() != null && searchModel.getBrandName().trim().length() > 0) {
                hql.append(" AND lower(d.brandName) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getBrandName()));
            }
            if (searchModel.getProductName() != null && searchModel.getProductName().trim().length() > 0) {
                hql.append(" AND lower(d.productName) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getProductName()));
            }
            if (searchModel.getStatusFile() != null) {
                hql.append(" AND d.statusfile = ?");
                listParam.add(searchModel.getStatusFile());
            }

            if (searchModel.getStatusPayment() != null) {
                hql.append(" AND d.statuspayment = ?");
                listParam.add(searchModel.getStatusPayment());
            }

            if (searchModel.getTypeOrder() != null) {

                if (searchModel.getTypeOrder() == 1L) {
                    hql.append(" order by d.nswFileCode asc ");
                } else if (searchModel.getTypeOrder() == 2L) {
                    hql.append(" order by d.nswFileCode desc ");
                } else if (searchModel.getTypeOrder() == 3L) {
                    hql.append(" order by d.brandName asc");
                } else if (searchModel.getTypeOrder() == 4L) {
                    hql.append(" order by d.brandName desc");
                } else if (searchModel.getTypeOrder() == 5L) {
                    hql.append(" order by d.productName asc");
                } else if (searchModel.getTypeOrder() == 6L) {
                    hql.append(" order by d.productName desc");
                } else {
                    hql.append(" order by d.billId asc");
                }
            } else {
                hql.append(" order by d.billId asc");
            }

        }
        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

    public PagingListModel findFilesByCreatorId(SearchPaymentModel searchModel, Long creatorId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) FROM VCosPaymentInfo n WHERE n.isActive = 1  ");
        StringBuilder strCountBuf = new StringBuilder("select count(n) FROM VCosPaymentInfo n WHERE n.isActive = 1  ");
        StringBuilder hql = new StringBuilder();
        hql.append(" AND n.creatorId = ? ");
        listParam.add(creatorId);

        if (searchModel != null) {
            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getBrandName() != null && searchModel.getBrandName().trim().length() > 0) {
                hql.append(" AND lower(n.brandName) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getBrandName()));
            }
            if (searchModel.getProductName() != null && searchModel.getProductName().trim().length() > 0) {
                hql.append(" AND lower(n.productName) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getProductName()));
            }
            if (searchModel.getStatusFile() != null) {
                hql.append(" AND n.statusfile = ?");
                listParam.add(searchModel.getStatusFile());
            } else {
                hql.append(" AND n.statusfile = ?");
                listParam.add(0L);
            }

            if (searchModel.getStatusPayment() != null) {
                hql.append(" AND n.statuspayment = ?");
                listParam.add(searchModel.getStatusPayment());
            }

        }

        if (searchModel.getTypeOrder() != null) {

            if (searchModel.getTypeOrder() == 1L) {
                hql.append(" order by n.nswFileCode asc ");
            } else if (searchModel.getTypeOrder() == 2L) {
                hql.append(" order by n.nswFileCode desc ");
            } else if (searchModel.getTypeOrder() == 3L) {
                hql.append(" order by n.brandName asc");
            } else if (searchModel.getTypeOrder() == 4L) {
                hql.append(" order by n.brandName desc");
            } else if (searchModel.getTypeOrder() == 5L) {
                hql.append(" order by n.productName asc");
            } else if (searchModel.getTypeOrder() == 6L) {
                hql.append(" order by n.productName desc");
            } else {
                hql.append(" order by n.nswFileCode desc");
            }
        } else {
            hql.append(" order by n.nswFileCode desc");
        }
        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }
    // all

    public List findListStatusByReceiverAndDeptId(SearchModel searchModel, Long receiverId, Long receiveDeptId) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n.status) ");
        StringBuilder hql = new StringBuilder();


        hql.append(" FROM VFileCosfile n, Process p WHERE"
                + " n.fileId = p.objectId AND "
                + " n.fileType = p.objectType AND "
                + " n.status = p.status AND "
                + " p.receiveUserId = ? ");
        listParam.add(receiverId);

        //  hql.append(" order by n.fileId desc");
        strBuf.append(hql);
        Query query = session.createQuery(strBuf.toString());
        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
        }
        List lst = query.list();
        return lst;
    }
    
    public PagingListModel findFilesByReceiverAndDeptId(SearchModel searchModel,
            Long receiverId, Long receiveDeptId, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) ");
        StringBuilder strCountBuf = new StringBuilder("select count(distinct n.cosFileId) ");
        StringBuilder hql = new StringBuilder();
      
                    hql.append(" FROM VFileCosfile n, Process p WHERE"
                            + " n.fileId = p.objectId AND "
                            + " n.fileType = p.objectType "); //AND "
                           // + " n.status = p.status AND "
                           // + " p.receiveUserId = ? ");
                  //  listParam.add(receiverId);
                    

        //listParam.add(receiverId);
        //listParam.add(receiveDeptId);
        if (searchModel != null) {

            //todo:fill query
            if (searchModel.getCreateDateFrom() != null) {
                hql.append(" AND n.createDate >= ? ");
                listParam.add(searchModel.getCreateDateFrom());
            }
            if (searchModel.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getModifyDateFrom() != null) {
                hql.append(" AND n.modifyDate >= ? ");
                listParam.add(searchModel.getModifyDateFrom());
            }
            if (searchModel.getModifyDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(searchModel.getModifyDateTo());
                hql.append(" AND n.createDate < ? ");
                listParam.add(toDate);
            }
            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
            }
            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
                hql.append(" AND lower(n.cosmeticNo) like ? escape '/'");
                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
            }
            if (searchModel.getStatus() != null) {
                hql.append(" AND n.status = ?");
                listParam.add(searchModel.getStatus());
            }
            if (searchModel.getDocumentTypeCode() != null) {
                hql.append(" AND n.documentTypeCode = ?");
                listParam.add(searchModel.getDocumentTypeCode());
            }

            if (searchModel instanceof CosmeticFileModel) {
                CosmeticFileModel cosmeticFileModel = (CosmeticFileModel) searchModel;
                if (cosmeticFileModel.getBrandName() != null && cosmeticFileModel.getBrandName().trim().length() > 0) {
                    hql.append(" AND lower(n.brandName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBrandName()));
                }

                if (cosmeticFileModel.getProductName() != null && cosmeticFileModel.getProductName().trim().length() > 0) {
                    hql.append(" AND lower(n.productName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getProductName()));
                }

                if (cosmeticFileModel.getBusinessId() != null) {
                    hql.append(" AND n.businessId = ?");
                    listParam.add(cosmeticFileModel.getBusinessId());
                }

                if (cosmeticFileModel.getBusinessName() != null && cosmeticFileModel.getBusinessName().trim().length() > 0) {
                    hql.append(" AND lower(n.businessName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessName()));
                }

                if (cosmeticFileModel.getBusinessAddress() != null && cosmeticFileModel.getBusinessAddress().trim().length() > 0) {
                    hql.append(" AND lower(n.businessAddress) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getBusinessAddress()));
                }

                if (cosmeticFileModel.getDistributePersonName() != null && cosmeticFileModel.getDistributePersonName().trim().length() > 0) {
                    hql.append(" AND lower(n.distributePersonName) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getDistributePersonName()));
                }

                if (cosmeticFileModel.getIntendedUse() != null && cosmeticFileModel.getIntendedUse().trim().length() > 0) {
                    hql.append(" AND lower(n.intendedUse) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getIntendedUse()));
                }
                if (cosmeticFileModel.getTaxCode()!= null && cosmeticFileModel.getTaxCode().trim().length() > 0) {
                    hql.append(" AND lower(n.taxCode) like ? escape '/'");
                    listParam.add(StringUtils.toLikeString(cosmeticFileModel.getTaxCode()));
                }

            }

        }

        if (null != searchModel.getMenuTypeStr()) {
            switch (searchModel.getMenuTypeStr()) {
                case Constants.MENU_TYPE.WAITPROCESS_STR:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
                case Constants.MENU_TYPE.PROCESSING_STR:
                    hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
                    break;
                case Constants.MENU_TYPE.PROCESSED_STR:
                    hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
                    break;
                case Constants.MENU_TYPE.DEPT_PROCESS_STR:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
                default:
                    hql.append(" order by n.modifyDate ,n.nswFileCode");
                    break;
            }
        } else {
            hql.append(" order by n.modifyDate desc,n.nswFileCode desc");
        }

        strBuf.append(hql);
        strCountBuf.append(hql);

        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }
}
