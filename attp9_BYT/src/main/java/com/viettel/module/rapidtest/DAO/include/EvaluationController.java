package com.viettel.module.rapidtest.DAO.include;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.rapidtest.BO.RtAdditionalRequest;
import com.viettel.module.rapidtest.BO.RtEvaluationRecord;
import com.viettel.module.rapidtest.BO.RtPermit;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.module.rapidtest.DAOHE.EvaluationRecordDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
//import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHEOld;
import com.viettel.module.rapidtest.DAOHE.RtAdditionalRequestDAOHE;
import com.viettel.module.rapidtest.model.ExportModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.DAOHE.PermitDAOHE;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author Linhdx
 */
public class EvaluationController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Wire
    private Label lbTopWarning, lbBottomWarning;

    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();

    @Wire
    private Window windowEvaluation;
    private Long fileId;
    private Long userEvaluationType;
    @Wire
    private Textbox userEvaluationTypeH;
    @Wire
    private Radiogroup rbStatus;
    @Wire
    private Textbox mainContent, legalContent;
    @Wire
    private Listbox lbEffective, lbLegal;
    @Wire
    private Button btnApproveFile, btnApproveDispatch, btnPreviewDispatch;
    @Wire
    private Button btnApproveFile2, btnApproveDispatch2, btnPreviewDispatch2;

    private VFileRtfile vFileRtfile;

    private RtEvaluationRecord obj = new RtEvaluationRecord();
    private RtPermit permit;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        EvaluationRecordDAOHE objDAOHE = new EvaluationRecordDAOHE();
        RtEvaluationRecord object = objDAOHE.getLastEvaluation(fileId);
        if (object != null) {
            obj.copy(object);
        }

        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        vFileRtfile = rapidTestDAOHE.findViewByFileId(fileId);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Listen("onCheck=#windowEvaluationCT #rbStatus")
    public void onCheck() {
        //btnSave.setVisible(false);
        Long select = Long.valueOf((String) rbStatus.getSelectedItem().getValue());
        if (Constants.EVALUTION.EVALUATION_LEADER_APPROVE.APPROVE_FILE.equals(select)) {
            btnApproveDispatch.setVisible(false);
            btnPreviewDispatch.setVisible(false);
            btnApproveFile.setVisible(true);

            btnApproveDispatch2.setVisible(false);
            btnPreviewDispatch2.setVisible(false);
            btnApproveFile2.setVisible(true);

        } else if (Constants.EVALUTION.EVALUATION_LEADER_APPROVE.APPROVE_DISPATCH.equals(select)) {
            btnApproveDispatch.setVisible(true);
            btnPreviewDispatch.setVisible(true);
            btnApproveFile.setVisible(false);

            btnApproveDispatch2.setVisible(true);
            btnPreviewDispatch2.setVisible(true);
            btnApproveFile2.setVisible(false);
        }
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        userEvaluationType = Long.valueOf((String)userEvaluationTypeH.getValue());

        if (Constants.EVALUTION.USER_EVALUATION_TYPE.STAFF.equals(userEvaluationType)) {//chuyen vien
            loadItemForEdit(fileId);
        } else if (Constants.EVALUTION.USER_EVALUATION_TYPE.PP.equals(userEvaluationType)) {//Phong phong
            loadItemForEdit(fileId);
        } else if (Constants.EVALUTION.USER_EVALUATION_TYPE.TP.equals(userEvaluationType)) {//Truong phong
            loadItemForEdit(fileId);
        }

    }

    private void loadItemForEdit(Long fileId) {
        Long confirm = obj.getStatus();
        if (confirm != null) {
            if (Constants.EVALUTION.FILE_OK.equals(confirm)) {
                rbStatus.setSelectedIndex(0);
            } else if (Constants.EVALUTION.FILE_NEED_ADD.equals(confirm)) {
                rbStatus.setSelectedIndex(1);
            }
        }
//        RapidTestDAOHEOld rapidTestDAOHEOld = new RapidTestDAOHEOld();
//        RtFileOld rtFile = rapidTestDAOHEOld.findByFileId(fileId);
//        Long effective = rtFile.getEffective();
//        if (effective != null) {
//            if (Constants.EVALUTION.EFFECTIVE.NO_LIMIT.equals(effective)) {
//                lbEffective.setSelectedIndex(1);
//            } else if (Constants.EVALUTION.EFFECTIVE.YEAR_3.equals(effective)) {
//                lbEffective.setSelectedIndex(2);
//            } else if (Constants.EVALUTION.EFFECTIVE.YEAR_5.equals(effective)) {
//                lbEffective.setSelectedIndex(3);
//            }
//        }

        Long legal = obj.getLegal();
        if (legal != null) {
            if (Constants.EVALUTION.LEGAL.OK.equals(legal)) {
                lbLegal.setSelectedIndex(1);
            } else if (Constants.EVALUTION.LEGAL.NOK.equals(legal)) {
                lbLegal.setSelectedIndex(2);
            } else if (Constants.EVALUTION.LEGAL.NEED_ADD.equals(legal)) {
                lbLegal.setSelectedIndex(3);
            }
        }
    }


    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        if (rbStatus.getSelectedItem() == null) {
            showWarningMessage("Chưa chọn kết quả thẩm định");
            rbStatus.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSave();
                break;

        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        EvaluationRecordDAOHE objDAOHE = new EvaluationRecordDAOHE();
        obj = createEvaluationRecord();
        objDAOHE.saveOrUpdate(obj);

//        //Luu truong effective vao bang rt_file
//        if (lbEffective != null && !"-1".equals((String) lbEffective.getSelectedItem().getValue())) {
//            RapidTestDAOHEOld rapidTestDAOHEOld = new RapidTestDAOHEOld();
//            RtFileOld rtFile = rapidTestDAOHEOld.findByFileId(fileId);
//            rtFile.setEffective(Long.valueOf((String) lbEffective.getSelectedItem().getValue()));
//            rapidTestDAOHEOld.saveOrUpdate(rtFile);
//        }
        showNotification(String.format(
                Constants.Notification.SAVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                Constants.Notification.INFO);

    }

    public void onApproveFile() throws Exception {
        EvaluationRecordDAOHE objDAOHE = new EvaluationRecordDAOHE();
        obj = createApproveObject();
        objDAOHE.saveOrUpdate(obj);
        onSignPermit();
        base.showNotify("Phê duyệt hồ sơ thành công!");
    }

    public void onApproveDispatch() throws Exception {
        onSignDispatchTP();
        base.showNotify("Phê duyệt công văn thành công!");
    }

    public void onPreviewDispath() throws Exception {
        RtAdditionalRequest additionalRequest = new RtAdditionalRequest();
        createAdditionalrequest(additionalRequest);
        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        VFileRtfile vFileRtfile2 = rapidTestDAOHE.findViewByFileId(fileId);
        ExportModel model = setModelObject(additionalRequest, vFileRtfile2);
        ExportFileDAO exportFileDAO = new ExportFileDAO();
        exportFileDAO.exportTempCvSdbs(model);
    }

    public void onProvideNumberDispatch() throws Exception {
        //chuyen luong den doanh nghiep
        RtAdditionalRequestDAOHE additionalRequestDAOHE = new RtAdditionalRequestDAOHE();
        //Tim kiem cac ban ghi cong van Yeu cau bo sung
        List<RtAdditionalRequest> lstAdditionalRequest = additionalRequestDAOHE.findAllActiveByFileId(fileId);
        RtAdditionalRequestDAOHE objDAOHE;
        RtAdditionalRequest additionalRequest;
        if (lstAdditionalRequest.size() > 0) {
            //Co co cong van thi lay ban cu
            additionalRequest = lstAdditionalRequest.get(0);
            additionalRequest.setStatus(Constants.EVALUTION.DISPATCH_STATUS.PROVIDED_NUMBER);
            additionalRequestDAOHE.saveOrUpdate(additionalRequest);
            showNotification("Cấp số công văn thành công",
                    Constants.Notification.INFO);
        } else {
            showNotification("Chưa có công văn",
                    Constants.Notification.INFO);
        }
    }

    @Listen("onSignDispatchTP = #windowEvaluationTP")
    public void onSignDispatchTP() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        RtAdditionalRequestDAOHE additionalRequestDAOHE = new RtAdditionalRequestDAOHE();
        //Tim kiem cac ban ghi cong van Yeu cau bo sung
        List<RtAdditionalRequest> lstAdditionalRequest = additionalRequestDAOHE.findAllActiveByFileId(fileId);
        RtAdditionalRequestDAOHE objDAOHE;
        RtAdditionalRequest additionalRequest;
        if (lstAdditionalRequest.size() > 0) {
            //Co co cong van thi lay ban cu
            additionalRequest = lstAdditionalRequest.get(0);
        } else {
            //Neu chua co cong van thi tao moi
            objDAOHE = new RtAdditionalRequestDAOHE();
            additionalRequest = new RtAdditionalRequest();
            createAdditionalrequest(additionalRequest);
            objDAOHE.saveOrUpdate(additionalRequest);
        }

        //lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi luu vao attach)
        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        VFileRtfile vFileRtfile2 = rapidTestDAOHE.findViewByFileId(fileId);
        ExportModel model = setModelObject(additionalRequest, vFileRtfile2);
        ExportFileDAO exportFileDAO = new ExportFileDAO();
        exportFileDAO.exportDataCvSdbs(model);
        showNotification(String.format(
                Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                Constants.Notification.INFO);
//        downloadFile(additionalRequest);

    }

    public void onSignPermit() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        PermitDAOHE permitDAOHE = new PermitDAOHE();
        //Tim kiem cac ban ghi cong van Yeu cau bo sung
        List<RtPermit> lstPermit = permitDAOHE.findAllActiveByFileId(fileId);

        if (lstPermit.size() > 0) {
            //Co co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            //Neu chua co cong van thi tao moi
            permit = new RtPermit();
            createPermit();
            permitDAOHE.saveOrUpdate(permit);
        }

        //lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi luu vao attach)
        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        VFileRtfile vFileRtfile2 = rapidTestDAOHE.findViewByFileId(fileId);
        ExportModel model = setModelObject(permit, vFileRtfile2);
        ExportFileDAO exportFileDAO = new ExportFileDAO();
        exportFileDAO.exportPermit(model);
        showNotification(String.format(
                Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                Constants.Notification.INFO);
//        downloadFile(additionalRequest);

    }

    private RtEvaluationRecord createEvaluationRecord() throws Exception {
        Date dateNow = new Date();
        obj.setCreateDate(dateNow);
        obj.setStatus(Long.valueOf((String) rbStatus.getSelectedItem().getValue()));
        obj.setFileId(fileId);
        obj.setLegal(Long.valueOf((String) lbLegal.getSelectedItem().getValue()));
        obj.setLegalContent(legalContent.getValue());
        obj.setMainContent(mainContent.getValue());
        obj.setIsActive(Constants.Status.ACTIVE);
        return obj;
    }

    private RtEvaluationRecord createApproveObject() throws Exception {
        Date dateNow = new Date();
        obj.setCreateDate(dateNow);
        obj.setStatus(Constants.EVALUTION.FILE_OK);
        obj.setFileId(fileId);
        obj.setMainContent(mainContent.getValue());
        obj.setIsActive(Constants.Status.ACTIVE);
        return obj;
    }

    private RtPermit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;

    }

    private void createAdditionalrequest(RtAdditionalRequest objRequest) throws Exception {
        Date dateNow = new Date();
        objRequest.setCreateDate(dateNow);
        objRequest.setCreateDeptId(getDeptId());
        objRequest.setCreateDeptName(getDeptName());
        objRequest.setCreatorId(getUserId());
        objRequest.setCreatorName(getUserFullName());

        objRequest.setStatus(Long.valueOf((String) rbStatus.getSelectedItem().getValue()));
        objRequest.setFileId(fileId);
        objRequest.setContent(mainContent.getValue());

        objRequest.setIsActive(Constants.Status.ACTIVE);
    }

    private ExportModel setModelObject(RtAdditionalRequest additionalRequest, VFileRtfile vFileRtfile) throws Exception {
        ExportModel model = new ExportModel();
        //obj.setBusinessName(additionalRequest.geadditionalRequesttb);
        model.setBusinessName(vFileRtfile.getBusinessName());
        model.setContent(additionalRequest.getContent());
        model.setLeaderSinged(getUserFullName());
        model.setObjectId(additionalRequest.getAdditionalRequestId());
        model.setObjectType(Constants.OBJECT_TYPE.RAPID_TEST_SDBS_DISPATCH);
        model.setRolesigner("Lãnh đạo");
        model.setSignDate(new Date());
        model.setSigner(getUserFullName());
        model.setTypeExport(Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP);

        return model;

    }

    private ExportModel setModelObject(RtPermit permit, VFileRtfile vFileRtfile) throws Exception {
        ExportModel model = new ExportModel();
        //obj.setBusinessName(additionalRequest.geadditionalRequesttb);
        model.setBusinessName(vFileRtfile.getBusinessName());
        model.setLeaderSinged(getUserFullName());
        model.setObjectId(permit.getPermitId());
        model.setObjectType(Constants.OBJECT_TYPE.RAPID_TEST_PERMIT);
        model.setRolesigner("Lãnh đạo");
        model.setSignDate(new Date());
        model.setSigner(getUserFullName());
        model.setTypeExport(Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP);

        return model;

    }


    public RtEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(RtEvaluationRecord obj) {
        this.obj = obj;
    }

    public VFileRtfile getvFileRtfile() {
        return vFileRtfile;
    }

    public void setvFileRtfile(VFileRtfile vFileRtfile) {
        this.vFileRtfile = vFileRtfile;
    }

}
