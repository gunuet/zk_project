package com.viettel.module.rapidtest.DAO.include;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.module.rapidtest.BO.RtEvaluationRecord;
import com.viettel.module.rapidtest.BO.RtPermit;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.module.rapidtest.DAOHE.EvaluationRecordDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.module.rapidtest.model.ExportModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAOHE;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author Linhdx
 */
public class EvaluationSignPermitController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Wire
    private Label lbTopWarning, lbBottomWarning;

    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();

    private Long fileId;
    private Long docType;
    private String bCode;
    private List listBook;
    @Wire
    private Textbox mainContent;

    private VFileRtfile vFileRtfile;

    private RtEvaluationRecord obj = new RtEvaluationRecord();
    private RtPermit permit;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_PERMIT;
        docType = (Long) arguments.get("docType");

        EvaluationRecordDAOHE objDAOHE = new EvaluationRecordDAOHE();
        RtEvaluationRecord object = objDAOHE.getLastEvaluation(fileId);
        if (object != null) {
            obj.copy(object);
        }
        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        vFileRtfile = rapidTestDAOHE.findViewByFileId(fileId);
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {
        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;

        }
    }

    
    @Listen("onClick=#btnSubmit")
    public void btnSubmit() {
        try {
            onApproveFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    @Listen("onClick=#btnSign")
    public void btnSign() {
        try {
            onApproveFile();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public void onApproveFile() throws Exception {
        EvaluationRecordDAOHE objDAOHE = new EvaluationRecordDAOHE();
        obj = createApproveObject();
        objDAOHE.saveOrUpdate(obj);
        onSignPermit();
        base.showNotify("Phê duyệt hồ sơ thành công!");
    }

    public void onSignPermit() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        PermitDAOHE permitDAOHE = new PermitDAOHE();
        List<RtPermit> lstPermit = permitDAOHE.findAllActiveByFileId(fileId);

        if (lstPermit.size() > 0) {
            //Co co cong van thi lay ban cu
            permit = lstPermit.get(0);
        } else {
            //Neu chua co cong van thi tao moi
            permit = new RtPermit();
            createPermit();
            permitDAOHE.saveOrUpdate(permit);
        }
        Long bookNumber = putInBook(permit);//Vao so

        permit.setReceiveNo(String.valueOf(bookNumber));
        permitDAOHE.saveOrUpdate(permit);

        //lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi luu vao attach)
        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        VFileRtfile vFileRtfile2 = rapidTestDAOHE.findViewByFileId(fileId);
        String pathTemplate = getPathTemplate(fileId);
        ExportModel model = setModelObject(permit, vFileRtfile2, pathTemplate);

        ExportFileDAO exportFileDAO = new ExportFileDAO();

        exportFileDAO.exportPermit(model);
        showNotification(String.format(
                Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                Constants.Notification.INFO);
//        downloadFile(additionalRequest);

    }

    private String getPathTemplate(Long fileId) {
        //Get Template
        WorkflowAPI wAPI = new WorkflowAPI();
        Flow flow = wAPI.getFlowByFileId(fileId);
        Long deptId = flow.getDeptId();
        Long procedureId = flow.getObjectId();
        TemplateDAOHE the = new TemplateDAOHE();
        String pathTemplate = the.findPathTemplate(deptId, procedureId,
                Constants.PROCEDURE_TEMPLATE_TYPE.PERMIT);
        return pathTemplate;
    }

    /**
     * Vao so giay phep de lay so giay phep
     *
     * @return
     */
    private Long putInBook(RtPermit permit) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        listBook = bookDAOHE.getBookByTypeAndPrefix(docType, bCode);
        if (listBook == null || listBook.size() < 1) {
            return null;
        }
        Books book = (Books) listBook.get(0);//Lay so dau tien
        BookDocument bookDocument = createBookDocument(permit.getPermitId(), book.getBookId());
        if (bookDocument == null || bookDocument.getBookNumber() == null) {
            return null;
        }
        return bookDocument.getBookNumber();
    }

    /**
     * Tao bao ghi trong bang book document
     *
     * @param objectId
     * @param bookId
     * @return
     */
    protected BookDocument createBookDocument(Long objectId, Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bookDocument = new BookDocument();
        bookDocument.setBookId(bookId);
        Long maxBookNumber = bookDocumentDAOHE.getMaxBookNumber(bookId);
        bookDocument.setBookNumber(maxBookNumber);
        bookDocument.setDocumentId(objectId);
        bookDocument.setStatus(Constants.Status.ACTIVE);
        bookDocumentDAOHE.saveOrUpdate(bookDocument);
        updateBookCurrentNumber(bookDocument);

        return bookDocument;
    }

    /**
     * Cap nhat so hien tai trong bang book
     *
     * @param bookDocument
     */
    public void updateBookCurrentNumber(BookDocument bookDocument) {
        BookDAOHE bookDAOHE = new BookDAOHE();
        Books book = bookDAOHE.findById(bookDocument.getBookId());
        book.setCurrentNumber(bookDocument.getBookNumber());
        bookDAOHE.saveOrUpdate(book);
    }

    private RtEvaluationRecord createApproveObject() throws Exception {
        Date dateNow = new Date();
        obj.setCreateDate(dateNow);
        obj.setStatus(Constants.EVALUTION.FILE_OK);
        obj.setFileId(fileId);
        obj.setMainContent(mainContent.getValue());
        obj.setIsActive(Constants.Status.ACTIVE);
        return obj;
    }

    private RtPermit createPermit() throws Exception {
        permit.setFileId(fileId);
        permit.setIsActive(Constants.Status.ACTIVE);
        permit.setStatus(Constants.PERMIT_STATUS.SIGNED);
        permit.setSignDate(new Date());
        permit.setReceiveDate(new Date());
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        permit.setSignerName(tk.getUserFullName());
        permit.setBusinessId(tk.getUserId());
        return permit;

    }

    private ExportModel setModelObject(RtPermit permit, VFileRtfile vFileRtfile, String pathTemplate) throws Exception {
        ExportModel model = new ExportModel();
        //obj.setBusinessName(additionalRequest.geadditionalRequesttb);
        model.setBusinessName(vFileRtfile.getBusinessName());
        model.setLeaderSinged(getUserFullName());
        model.setObjectId(permit.getPermitId());
        model.setObjectType(Constants.OBJECT_TYPE.RAPID_TEST_PERMIT);
        model.setRolesigner("Lãnh đạo");
        model.setSignDate(new Date());
        model.setSigner(getUserFullName());
        model.setRapidTestName(vFileRtfile.getRapidTestName());
        model.setPlaceOfManufacture(vFileRtfile.getPlaceOfManufacture());
        model.setBusinessAddress(vFileRtfile.getBusinessAddress());
        model.setSendNo(permit.getReceiveNo());
        model.setPathTemplate(pathTemplate);
        model.setTypeExport(Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP);

        return model;

    }

    public RtEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(RtEvaluationRecord obj) {
        this.obj = obj;
    }

    public VFileRtfile getvFileRtfile() {
        return vFileRtfile;
    }

    public void setvFileRtfile(VFileRtfile vFileRtfile) {
        this.vFileRtfile = vFileRtfile;
    }

}
