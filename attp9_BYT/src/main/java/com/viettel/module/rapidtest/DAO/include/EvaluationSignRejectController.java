package com.viettel.module.rapidtest.DAO.include;

import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.BusinessController;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.rapidtest.BO.RtEvaluationRecord;
import com.viettel.module.rapidtest.BO.RtAdditionalRequest;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAO.ExportFileDAO;
import com.viettel.module.rapidtest.DAOHE.EvaluationRecordDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.module.rapidtest.DAOHE.RtAdditionalRequestDAOHE;
import com.viettel.core.sys.DAO.TemplateDAOHE;
import com.viettel.module.rapidtest.model.ExportModel;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class EvaluationSignRejectController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Wire
    private Label lbTopWarning, lbBottomWarning;

    private Long fileId;

    @Wire
    private Radiogroup rbStatus;
    @Wire
    private Textbox mainContent;

    private VFileRtfile vFileRtfile;

    private RtEvaluationRecord obj = new RtEvaluationRecord();

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        EvaluationRecordDAOHE objDAOHE = new EvaluationRecordDAOHE();
        RtEvaluationRecord object = objDAOHE.getLastEvaluation(fileId);
        if (object != null) {
            obj.copy(object);
        }

        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        vFileRtfile = rapidTestDAOHE.findViewByFileId(fileId);

        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;

        }
    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            onSignDispatchTP();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @throws java.lang.Exception
     */
    public void onSignDispatchTP() throws Exception {
        clearWarningMessage();
        if (!isValidatedData()) {
            return;
        }
        RtAdditionalRequestDAOHE additionalRequestDAOHE = new RtAdditionalRequestDAOHE();
        //Tim kiem cac ban ghi cong van Yeu cau bo sung
        List<RtAdditionalRequest> lstAdditionalRequest = additionalRequestDAOHE.findAllActiveByFileId(fileId);
        RtAdditionalRequestDAOHE objDAOHE;
        RtAdditionalRequest additionalRequest;
        if (lstAdditionalRequest.size() > 0) {
            //Co co cong van thi lay ban cu
            additionalRequest = lstAdditionalRequest.get(0);
        } else {
            //Neu chua co cong van thi tao moi
            objDAOHE = new RtAdditionalRequestDAOHE();
            additionalRequest = new RtAdditionalRequest();
            createAdditionalrequest(additionalRequest);
            objDAOHE.saveOrUpdate(additionalRequest);
        }

        //lay thong tin ve File va dua vao model de xuat ra file doc(dong thoi luu vao attach)
        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        VFileRtfile vFileRtfile2 = rapidTestDAOHE.findViewByFileId(fileId);
        String pathTemplate = getPathTemplate(fileId);
        ExportModel model = setModelObject(additionalRequest, vFileRtfile2, pathTemplate);
        ExportFileDAO exportFileDAO = new ExportFileDAO();
        exportFileDAO.exportDataCvSdbs(model);
        showNotification(String.format(
                Constants.Notification.UPDATE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                Constants.Notification.INFO);
//        downloadFile(additionalRequest);

    }
    private String getPathTemplate(Long fileId) {
        //Get Template
        WorkflowAPI wAPI = new WorkflowAPI();
        Flow flow = wAPI.getFlowByFileId(fileId);
        Long deptId = flow.getDeptId();
        Long procedureId = flow.getObjectId();

        TemplateDAOHE the = new TemplateDAOHE();
        String pathTemplate = the.findPathTemplate(deptId, procedureId,
                Constants.PROCEDURE_TEMPLATE_TYPE.ADDITIONAL_REQUEST);
        return pathTemplate;
    }

    private void createAdditionalrequest(RtAdditionalRequest objRequest) throws Exception {
        Date dateNow = new Date();
        objRequest.setCreateDate(dateNow);
        objRequest.setCreateDeptId(getDeptId());
        objRequest.setCreateDeptName(getDeptName());
        objRequest.setCreatorId(getUserId());
        objRequest.setCreatorName(getUserFullName());

        //objRequest.setStatus(Long.valueOf((String) rbStatus.getSelectedItem().getValue()));
        objRequest.setFileId(fileId);
        objRequest.setContent(mainContent.getValue());

        objRequest.setIsActive(Constants.Status.ACTIVE);
    }

    private ExportModel setModelObject(RtAdditionalRequest additionalRequest, VFileRtfile vFileRtfile, String pathTemplate) throws Exception {
        ExportModel model = new ExportModel();
        //obj.setBusinessName(additionalRequest.geadditionalRequesttb);
        model.setBusinessName(vFileRtfile.getBusinessName());
        model.setContent(additionalRequest.getContent());
        model.setLeaderSinged(getUserFullName());
        model.setObjectId(additionalRequest.getAdditionalRequestId());
        model.setObjectType(Constants.OBJECT_TYPE.RAPID_TEST_SDBS_DISPATCH);
        model.setRolesigner("Lãnh đạo");
        model.setSignDate(new Date());
        model.setSigner(getUserFullName());
        model.setTypeExport(Constants.RAPID_TEST.ADDITIONAL_REQUEST.TYPE_EXPORT.EX_TEMP);
        model.setPathTemplate(pathTemplate);
        return model;

    }

    public RtEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(RtEvaluationRecord obj) {
        this.obj = obj;
    }

    public VFileRtfile getvFileRtfile() {
        return vFileRtfile;
    }

    public void setvFileRtfile(VFileRtfile vFileRtfile) {
        this.vFileRtfile = vFileRtfile;
    }

}
