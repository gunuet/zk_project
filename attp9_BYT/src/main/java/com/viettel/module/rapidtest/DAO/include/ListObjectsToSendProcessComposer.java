package com.viettel.module.rapidtest.DAO.include;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.BO.NodeDeptUser;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import com.viettel.utils.Constants;

public class ListObjectsToSendProcessComposer extends BaseComposer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3120854764294466129L;

	@Wire
	private Listbox lbNodeDeptUser;
	private List<NodeDeptUser> listNDU;

	private String actionName;

	@Listen("onChangeProcessType = #lbNodeDeptUser")
	public String onChangeProcessType(NodeDeptUser ndu) {
		// Neu la "Xin y kien" thi ko cho thay doi loai xu li
		if (isGetOpinion(actionName)) {
			return loadLabelProcessType(ndu.getProcessType());
		}

		if (ndu.getProcessType() == null) {
			return loadLabelProcessType(Constants.PROCESS_TYPE.COOPERATE);
		} else {
			if (ndu.getProcessType() < Constants.PROCESS_TYPE.RECEIVE_TO_KNOW) {
				ndu.setProcessType(ndu.getProcessType() + 1);
				return loadLabelProcessType(ndu.getProcessType());
			} else {
				ndu.setProcessType(Constants.PROCESS_TYPE.COOPERATE);
				return loadLabelProcessType(Constants.PROCESS_TYPE.COOPERATE);
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Listen("onLoadModel = #lbNodeDeptUser")
	public void onLoadModel(Event event) {
		Map<String, Object> arguments = (Map<String, Object>) event.getData();
		listNDU = (List<NodeDeptUser>) arguments.get("listNDU");
		actionName = (String) arguments.get("actionName");
		ListModelList model = new ListModelList(listNDU);
		lbNodeDeptUser.setModel(model);
		lbNodeDeptUser.renderAll();
	}

	@Listen("onAfterRender = #lbNodeDeptUser")
	public void onAfterRenderListbox() {
		NodeDeptUser ndu;
		for (Listitem item : lbNodeDeptUser.getItems()) {
			ndu = item.getValue();
			if (Objects.equals(ndu.getNodeDeptUserId(), -1L)) {
				item.setDisabled(true);
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void onDeleteDocOut(int index) {
		listNDU.remove(index);
		lbNodeDeptUser.setModel(new ListModelList(listNDU));
		lbNodeDeptUser.renderAll();
	}

	public String loadLabelProcessType(Long processType) {
		if (Constants.PROCESS_TYPE.MAIN.equals(processType)) {
			return "Xử lý chính";
		} else if (Constants.PROCESS_TYPE.COOPERATE.equals(processType)) {
			return "Phối hợp";
		} else if (Constants.PROCESS_TYPE.COMMENT.equals(processType)) {
			return "Xin ý kiến";
		} else if (Constants.PROCESS_TYPE.RECEIVE_TO_KNOW.equals(processType)) {
			return "Nhận để biết";
		}
		return "";
	}

	public boolean isGetOpinion(String actionName) {
		return "xin ý kiến".equals(actionName.toLowerCase());
	}

}
