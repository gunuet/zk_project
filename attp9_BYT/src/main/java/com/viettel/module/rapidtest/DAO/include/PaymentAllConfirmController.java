package com.viettel.module.rapidtest.DAO.include;

import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.utils.LogUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class PaymentAllConfirmController extends PaymentAllController {

  @Override
  public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        fileId = (Long) arguments.get("fileId");
        phase = Long.valueOf((String) arguments.get("phase"));
        reload(fileId);
    }
  /**
   * Lay danh sach cac ho so da thanh toan
   * @param fileId 
   */
    private void reload(Long fileId) {
        PaymentInfoDAO paymentInfoDAOHE = new PaymentInfoDAO();
        lstFeePaymentInfo = paymentInfoDAOHE.getListPayedPayment(fileId, phase);
        lboxFeePaymentInfo.setModel(new ListModelList(lstFeePaymentInfo));
    }

    
    @Listen("onPaymentConfirm = #lboxFeePaymentInfo")
    public void onPaymentConfirm(Event event) {
        PaymentInfo paymentInfo = (PaymentInfo) event.getData();
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("paymentInfo", paymentInfo);
        arguments.put("parentWindow", businessWindow);
        Window window = createWindow("wdPaymentByMoney", "/Pages/rapidTest/include/paymentConfirm.zul",
                arguments, Window.POPUP);
        window.doModal();
    }

   

    
        @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

}
