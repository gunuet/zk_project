package com.viettel.module.rapidtest.DAO.include;

import com.viettel.core.workflow.BusinessController;
import com.viettel.utils.Constants;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

/**
 *
 * @author Linhdx
 */
public class PaymentConfirmController extends BusinessController {
    
    @Wire
    protected Vlayout flist;
    
    @Wire
    protected Textbox note;
    
    @Wire
    protected Datebox paymentDate;
    
    @Wire
    protected Listbox lboxFeePaymentType;
    protected Window parentWindow;
    
    @Wire
    protected Window windowPaymentConfirm;
    
    protected PaymentInfo paymentInfo;
    @SuppressWarnings("rawtypes")
    Long feePaymentType;
    
    Long fileId;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        parentWindow = (Window) arguments.get("parentWindow");
        paymentInfo = (PaymentInfo)arguments.get("paymentInfo");
        
        
//        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
//        parentWindow = (Window) arguments.get("parentWindow");
//        Long paymentInfoId = (Long) arguments.get("paymentInfoId");
//        FeePaymentInfoDAOHE objDAOHE = new FeePaymentInfoDAOHE();
//        paymentInfo = objDAOHE.findById(paymentInfoId);
//        fileId = (Long) arguments.get("fileId");
//        FeePaymentInfoDAOHE paymentInfoDAOHE = new FeePaymentInfoDAOHE();
//        List<PaymentInfo> lstPaymentInfo = paymentInfoDAOHE.getListPayment(fileId);
//        if(lstPaymentInfo !=null && lstPaymentInfo.size() >0){
//            paymentInfo = lstPaymentInfo.get(0);
//        }
        return super.doBeforeCompose(page, parent, compInfo);
    }
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
    }



    

    
    public Boolean isPaymentByBank() {
        return Constants.RAPID_TEST.PAYMENT.FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN.equals(feePaymentType);
    }
    
    @Listen("onClick = #btnAccept")
    public void onAccept() {
        paymentInfo = createObject();
        paymentInfo.setStatus(Constants.RAPID_TEST.PAYMENT.PAY_CONFIRMED);
        PaymentInfoDAO objDAOHE = new PaymentInfoDAO();
        objDAOHE.saveOrUpdate(paymentInfo);
        windowPaymentConfirm.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
        
    }
    
    @Listen("onClick = #btnReject")
    public void onReject() {
        paymentInfo = createObject();
         paymentInfo.setStatus(Constants.RAPID_TEST.PAYMENT.PAY_REJECTED);
        PaymentInfoDAO objDAOHE = new PaymentInfoDAO();
        objDAOHE.saveOrUpdate(paymentInfo);
        windowPaymentConfirm.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
        
    }
    
    private PaymentInfo createObject() {
        paymentInfo.setPaymentConfirm(getUserFullName());
        paymentInfo.setDateConfirm(new Date());
        paymentInfo.setNote(note.getValue());
        return paymentInfo;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

 
    
    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        
    }
    
}
