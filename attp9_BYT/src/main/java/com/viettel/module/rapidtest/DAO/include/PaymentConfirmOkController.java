package com.viettel.module.rapidtest.DAO.include;

import com.viettel.utils.LogUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.select.annotation.Listen;

/**
 *
 * @author Linhdx
 */
public class PaymentConfirmOkController extends PaymentConfirmController {

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
            onAccept();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

}
