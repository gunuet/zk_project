package com.viettel.module.rapidtest.DAO.include;

import com.viettel.core.workflow.BusinessController;
import com.viettel.module.rapidtest.BO.RtAdditionalRequest;
import com.viettel.module.rapidtest.DAOHE.RtAdditionalRequestDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Linhdx
 */
public class PutInBookController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final int BOOK_LISTBOXMODEL = 1;
    private final int STATUS_LISTBOXMODEL = 2;
    @Wire
    private Listbox lbBookIn;
    private List listBookIn;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    @Wire
    private Intbox boxBookNumber;
    private Long fileId;
    private BookDocument bookDocument;
    @Wire
    private Listbox lbStatusReceive;
    @Wire
    private Textbox tbAdditionRequest;
    
    private String bCode;
    private Long docType;

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        bCode = (String) arguments.get("bCode");
        docType = (Long)arguments.get("docType"); 
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public ListModelList getListBoxModel(int type) {
        ListModelList model = null;
        switch (type) {
            case BOOK_LISTBOXMODEL:
                BookDAOHE bookDAOHE = new BookDAOHE();
                listBookIn = bookDAOHE
                        .getBookByTypeAndPrefix(docType,bCode); 
                model = new ListModelList(listBookIn);
                break;
            case STATUS_LISTBOXMODEL:

                break;
        }

        return model;
    }

    /**
     * Lấy id so cu
     *
     * @param type
     * @return
     */
    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        switch (type) {
            case BOOK_LISTBOXMODEL:
                if (bookDocument != null) {
                    for (int i = 0; i < listBookIn.size(); i++) {
                        if (Objects.equals(bookDocument.getBookId(),
                                ((Books) listBookIn.get(i)).getBookId())) {
                            selectedItem = i;
                            break;
                        }
                    }
                }
                break;
        }
        return selectedItem;
    }

    public void onSelectBook() {
        Books book = (Books) listBookIn.get(lbBookIn.getSelectedIndex());
        boxBookNumber.setValue(getMaxBookNumber(book.getBookId()));
    }

    @Listen("onAfterRender = #lbBookIn")
    public void onAfterRenderListBookIn() {
        int selectedItemIndex = getSelectedIndexInModel(BOOK_LISTBOXMODEL);
        List<Listitem> listitems = lbBookIn.getItems();
        if (listitems.isEmpty()) {
            // Hien thi thong bao loi: don vi chua co so
            showWarningMessage("Đơn vị " + getDeptName()
                    + " chưa có sổ hồ sơ.");
            showNotification("Đơn vị " + getDeptName()
                    + " chưa có sổ hồ sơ.",
                    Constants.Notification.WARNING);
        } else {
            lbBookIn.setSelectedIndex(selectedItemIndex);
            Long bookId = lbBookIn.getItemAtIndex(selectedItemIndex).getValue();
            boxBookNumber.setValue(getMaxBookNumber(bookId));
        }
    }

    public Integer getBookNumber(Long fileId, Long fileType) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bd = bookDocumentDAOHE.getBookDocumentFromDocumentId(
                fileId, fileType);
        if (bd != null) {
            return bd.getBookNumber().intValue();
        }
        return null;
    }

    public int getMaxBookNumber(Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        return bookDocumentDAOHE.getMaxBookNumber(bookId).intValue();
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        if (tbAdditionRequest.getText().matches("\\s*")) {
            showWarningMessage("Yêu cầu bổ sung không thể để trống");
            tbAdditionRequest.focus();
            return false;
        }

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @param typeSave
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
            //Neu chon dong y thi luu so
            if (Objects.equals(Long.valueOf((String) lbStatusReceive.getSelectedItem().getValue()), Constants.Status.ACTIVE)) {
                BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
                bookDocument = createBookDocument();
                bookDocument.setStatus(Constants.Status.ACTIVE);
                bookDocumentDAOHE.saveOrUpdate(bookDocument);
                updateBookCurrentNumber();
            } else if (Objects.equals(Long.valueOf((String) lbStatusReceive.getSelectedItem().getValue()), Constants.Status.INACTIVE)) {
                //Neu khong dong y thi yeu cau nhap comment va luu comment
                if (!isValidatedData()) {
                    return;
                }
                String comment = tbAdditionRequest.getValue();
                RtAdditionalRequest obj = new RtAdditionalRequest();
                obj.setContent(comment);
                obj.setCreatorId(getUserId());
                obj.setCreatorName(getUserFullName());
                obj.setFileId(fileId);
                obj.setCreateDate(new Date());
                obj.setIsActive(Constants.Status.ACTIVE);
                RtAdditionalRequestDAOHE objDAOHE = new RtAdditionalRequestDAOHE();
                objDAOHE.saveOrUpdate(obj);
            }
            showNotification(String.format(
                    Constants.Notification.SAVE_SUCCESS, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.INFO);

        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification(String.format(
                    Constants.Notification.SAVE_ERROR, Constants.DOCUMENT_TYPE_NAME.FILE),
                    Constants.Notification.ERROR);
        }
    }

    protected BookDocument createBookDocument() {
        // So van ban *
        Long bookId = lbBookIn.getSelectedItem().getValue();
        if (bookDocument == null) {
            bookDocument = new BookDocument();
        }
        if (bookId != -1) {
            bookDocument.setBookId(bookId);
        }
        // So den *
        bookDocument.setBookNumber(boxBookNumber.getValue().longValue());
        bookDocument.setDocumentId(fileId);
        return bookDocument;
    }

    public void updateBookCurrentNumber() {
        if (getMaxBookNumber(bookDocument.getBookId()) <= bookDocument
                .getBookNumber()) {
            BookDAOHE bookDAOHE = new BookDAOHE();
            Books book = bookDAOHE.findById(bookDocument.getBookId());
            book.setCurrentNumber(bookDocument.getBookNumber());
            bookDAOHE.saveOrUpdate(book);
        }
    }

}
