package com.viettel.module.rapidtest.DAO.include;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.workflow.BusinessController;
import com.viettel.module.rapidtest.BO.RtEvaluationRecord;
import com.viettel.module.rapidtest.BO.RtPermit;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAOHE.EvaluationRecordDAOHE;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Document.BookDocument;
import com.viettel.voffice.BO.Document.Books;
import com.viettel.voffice.DAOHE.BookDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.PermitDAOHE;

/**
 *
 * @author Linhdx
 */
public class PutInBookPermitController extends BusinessController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final int BOOK_LISTBOXMODEL = 1;
    private final int STATUS_LISTBOXMODEL = 2;
    @Wire
    private Listbox lbBookIn;
    private List listBookIn;
    @Wire
    private Label lbTopWarning, lbBottomWarning;
    @Wire
    private Intbox boxBookNumber;
    private Long fileId;
    private BookDocument bookDocument;
    @Wire
    private Listbox lbStatusReceive;
    @Wire
    private Textbox tbAdditionRequest;

    private BaseGenericForwardComposer base = new BaseGenericForwardComposer();

    @Wire
    private Window windowEvaluation;
    private Long userEvaluationType;
    @Wire
    private Textbox userEvaluationTypeH;
    @Wire
    private Radiogroup rbStatus;
    @Wire
    private Textbox mainContent, legalContent;
    @Wire
    private Listbox lbEffective, lbLegal;
    @Wire
    private Button btnApproveFile, btnApproveDispatch, btnPreviewDispatch;
    @Wire
    private Button btnApproveFile2, btnApproveDispatch2, btnPreviewDispatch2;

    private VFileRtfile vFileRtfile;

    private RtEvaluationRecord obj = new RtEvaluationRecord();
    private RtPermit permit;
    private String bCode;//book Code
    private Long docType;
    

    /**
     * linhdx Ham bat dau truoc khi load trang
     *
     * @param page
     * @param parent
     * @param compInfo
     * @return
     */
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
            ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map) Executions.getCurrent().getArg();
        fileId = (Long) arguments.get("fileId");
        bCode = Constants.EVALUTION.BOOK_TYPE.BOOK_PERMIT; 
        docType = (Long)arguments.get("docType"); 
        EvaluationRecordDAOHE objDAOHE = new EvaluationRecordDAOHE();
        RtEvaluationRecord object = objDAOHE.getLastEvaluation(fileId);
        if (object != null) {
            obj.copy(object);
        }
        RapidTestDAOHE rapidTestDAOHE = new RapidTestDAOHE();
        vFileRtfile = rapidTestDAOHE.findViewByFileId(fileId);
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    /**
     * linhdx Ham thuc hien sau khi load form xong
     */
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    @Override
    @Listen("onClick=#btnSubmit")
    public void onSubmit() {
        try {
            LogUtils.addLog("Tiep nhan ho so:" + fileId);
            onSave();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    public ListModelList getListBoxModel(int type) {
        ListModelList model = null;
        switch (type) {
            case BOOK_LISTBOXMODEL:
                BookDAOHE bookDAOHE = new BookDAOHE();
                listBookIn = bookDAOHE
                        .getBookByTypeAndPrefix(docType, bCode);
                model = new ListModelList(listBookIn);
                break;
            case STATUS_LISTBOXMODEL:

                break;
        }

        return model;
    }

    /**
     * Lấy id so cu
     *
     * @param type
     * @return
     */
    public int getSelectedIndexInModel(int type) {
        int selectedItem = 0;
        switch (type) {
            case BOOK_LISTBOXMODEL:
                if (bookDocument != null) {
                    for (int i = 0; i < listBookIn.size(); i++) {
                        if (Objects.equals(bookDocument.getBookId(),
                                ((Books) listBookIn.get(i)).getBookId())) {
                            selectedItem = i;
                            break;
                        }
                    }
                }
                break;
        }
        return selectedItem;
    }

    public void onSelectBook() {
        Books book = (Books) listBookIn.get(lbBookIn.getSelectedIndex());
        boxBookNumber.setValue(getMaxBookNumber(book.getBookId()));
    }

    @Listen("onAfterRender = #lbBookIn")
    public void onAfterRenderListBookIn() {
        int selectedItemIndex = getSelectedIndexInModel(BOOK_LISTBOXMODEL);
        List<Listitem> listitems = lbBookIn.getItems();
        if (listitems.isEmpty()) {
            // Hien thi thong bao loi: don vi chua co so
            showWarningMessage("Đơn vị " + getDeptName()
                    + " chưa có sổ hồ sơ.");
            showNotification("Đơn vị " + getDeptName()
                    + " chưa có sổ hồ sơ.",
                    Constants.Notification.WARNING);
        } else {
            lbBookIn.setSelectedIndex(selectedItemIndex);
            Long bookId = lbBookIn.getItemAtIndex(selectedItemIndex).getValue();
            boxBookNumber.setValue(getMaxBookNumber(bookId));
        }
    }

    public Integer getBookNumber(Long fileId, Long fileType) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        BookDocument bd = bookDocumentDAOHE.getBookDocumentFromDocumentId(
                fileId, fileType);
        if (bd != null) {
            return bd.getBookNumber().intValue();
        }
        return null;
    }

    public int getMaxBookNumber(Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        return bookDocumentDAOHE.getMaxBookNumber(bookId).intValue();
    }

    /**
     * Validate du lieu
     *
     * @return
     */
    private boolean isValidatedData() {

        return true;
    }

    /**
     * Hien thi canh bao
     *
     * @param message
     */
    protected void showWarningMessage(String message) {
        lbTopWarning.setValue(message);
        lbBottomWarning.setValue(message);
    }

    /**
     * Tao canh bao
     */
    protected void clearWarningMessage() {
        lbTopWarning.setValue("");
        lbBottomWarning.setValue("");
    }

    /**
     * Xu ly phim tat
     *
     * @param keyCode
     */
    public void keyEventHandle(int keyCode) throws Exception {
        switch (keyCode) {
            case KeyEvent.F6:
                onSubmit();
                break;
        }
    }

    /**
     * linhdx Xu ly su kien luu
     *
     * @throws java.lang.Exception
     */
    @Listen("onClick = #btnSave, .saveClose")
    public void onSave() throws Exception {
        clearWarningMessage();
        try {
                PermitDAOHE permitDAOHE = new PermitDAOHE();
                RtPermit permit2;
                List<RtPermit> lstPermit = permitDAOHE.findAllActiveByFileId(fileId); 
                if (lstPermit.size() > 0) {
                    //Co co cong van thi lay ban cu
                    permit2 = lstPermit.get(0);
                    permit2.setStatus(Constants.PERMIT_STATUS.PROVIDED_NUMBER);
                    permitDAOHE.saveOrUpdate(permit2); 
                    BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
                    bookDocument = createBookDocument(permit2.getPermitId());
                    bookDocument.setStatus(Constants.Status.ACTIVE);
                    bookDocumentDAOHE.saveOrUpdate(bookDocument);
                    updateBookCurrentNumber();
                    showNotification("Cấp số công văn thành công",
                            Constants.Notification.INFO);
                } else {
                    showNotification("Chưa có công văn",
                            Constants.Notification.INFO);
                }



        } catch (WrongValueException ex) {
            LogUtils.addLogDB(ex);
            showNotification("Có lỗi xảy ra",
                            Constants.Notification.INFO);
        }
    }

    protected BookDocument createBookDocument(Long objectId) {
        // So van ban *
        Long bookId = lbBookIn.getSelectedItem().getValue();
        if (bookDocument == null) {
            bookDocument = new BookDocument();
        }
        if (bookId != -1) {
            bookDocument.setBookId(bookId);
        }
        // So den *
        bookDocument.setBookNumber(boxBookNumber.getValue().longValue());
        bookDocument.setDocumentId(objectId);
        return bookDocument;
    }

    public void updateBookCurrentNumber() {
        if (getMaxBookNumber(bookDocument.getBookId()) <= bookDocument
                .getBookNumber()) {
            BookDAOHE bookDAOHE = new BookDAOHE();
            Books book = bookDAOHE.findById(bookDocument.getBookId());
            book.setCurrentNumber(bookDocument.getBookNumber());
            bookDAOHE.saveOrUpdate(book);
        }
    }

    public VFileRtfile getvFileRtfile() {
        return vFileRtfile;
    }

    public void setvFileRtfile(VFileRtfile vFileRtfile) {
        this.vFileRtfile = vFileRtfile;
    }

    public RtEvaluationRecord getObj() {
        return obj;
    }

    public void setObj(RtEvaluationRecord obj) {
        this.obj = obj;
    }
    
    
    
    

}
