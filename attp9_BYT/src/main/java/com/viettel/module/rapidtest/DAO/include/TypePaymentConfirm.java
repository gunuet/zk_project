/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAO.include;

import com.viettel.core.sys.BO.Category;
import com.viettel.utils.Constants;
import com.viettel.utils.ConvertFileUtils;
import com.viettel.utils.FileUtil;
import com.viettel.voffice.BO.Document.Attachs;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.utils.ResourceBundleUtil;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zkplus.databind.BindingListModelList;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author giangpn
 */
@SuppressWarnings({"rawtypes", "serial"})
public class TypePaymentConfirm extends BaseGenericForwardComposer {

    @WireVariable
    private String recordMode;
    Window templateCRUD; // autowired
    @Wire
    Window TypePaymentConfirm; // autowired

    Window showDeptDlg;

    @Wire
    Textbox txtBillNo, txtBillKey;

    @Wire
    Datebox dbDayPayment;

    @Wire("#lbWarning")
    private Label lbWarning;

    @Wire
    Label lbBillNo, lbBillNoRequi, lbBillKey, lbBillKeyRequi;

    Groupbox gbTemplateContent;
    protected Listbox lboxStatus;
    private Window parentWindow;

    //private Template templateSelected;
    private AnnotateDataBinder binder;
    private BindingListModelList<Category> catList;

    Textbox txtFileName;
    Image imgDelFile;
    private Media media;
    private Attachs attachCurrent;

    @Wire
    Textbox txtDisplayNameComponentId;

    private String sType = "0";

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        this.self.setAttribute("controller", this, false);
        Execution execution = Executions.getCurrent();
        setRecordMode((String) execution.getArg().get("recordMode"));
        //setTemplateSelected((Template) execution.getArg().get("selectedRecord"));
        setParentWindow((Window) execution.getArg().get("parentWindow"));
        sType = (String) execution.getArg().get("type");
        String idOfDisplayNameComp = (String) Executions.getCurrent().getArg().get("idOfDisplayNameComp");
        txtDisplayNameComponentId.setValue(idOfDisplayNameComp);
        if ("1".equals(sType)) { //thanh toan chuyen khoan
            txtBillNo.setVisible(false);
            txtBillKey.setVisible(false);
            lbBillNo.setVisible(false);
            lbBillNoRequi.setVisible(false);
            lbBillKey.setVisible(false);
            lbBillKeyRequi.setVisible(false);

        } else if ("2".equals(sType)) { //thanh toan tien mat
            txtBillNo.setVisible(true);
            txtBillKey.setVisible(true);
            lbBillNo.setVisible(true);
            lbBillNoRequi.setVisible(true);
            lbBillKey.setVisible(true);
            lbBillKeyRequi.setVisible(true);
        }

    }

    public void onCreate$docCRUD(Event event) {
        this.binder = (AnnotateDataBinder) event.getTarget().getAttribute(
                "binder", true);
        this.binder.loadAll();
    }

    public void onClick$btnSave(Event event) {
        //  binder.saveAll();
        if (isValidate()) {
            String sTemp = "";
            if ("1".equals(sType)) {
                sTemp = "Hình thức thanh toán chuyển khoản";

            } else if ("2".equals(sType)) {
                sTemp = "Hình thức thanh toán hóa đơn tiền mặt";

            }

            String idOfDisplayName = txtDisplayNameComponentId.getValue();
            Textbox txtDeptName = (Textbox) Path.getComponent(idOfDisplayName);
            txtDeptName.setValue(sTemp);

            if (media != null) {

                Map<String, Object> args = new ConcurrentHashMap<>();
                args.put("mediaTypePayment", media);
                args.put("dateTypePayment", dbDayPayment.getValue());
                args.put("billNoTypePayment", txtBillNo.getText().trim());
                args.put("billKeyTypePayment", txtBillKey.getText().trim());
                Events.sendEvent("onAfterSelectMedia", parentWindow, args);

            }

            TypePaymentConfirm.onClose();
        } else {
            return;
        }

    }

    public void onClick$btnCancel(Event event) {
        TypePaymentConfirm.onClose();

    }

    public void onClick$btnShowDept() {

    }

    private boolean isValidate() {
        String message;
        if ("2".equals(sType)) {
            if ("".equals(txtBillNo.getText().trim())) {
                message = "Không được để trống mã hóa đơn";
                txtBillNo.focus();
                setWarningMessage(message);
                return false;
            }
            if ("".equals(txtBillKey.getText().trim())) {
                message = "Không được để trống ký hiệu hóa đơn";
                txtBillKey.focus();
                setWarningMessage(message);
                return false;
            }
        }
        if (dbDayPayment.getValue() == null) {
            message = "Không được để trống ngày thanh toán";
            setWarningMessage(message);
            return false;
        }
        if (media == null) {
            message = "Bạn vui lòng nhập file đính kèm hóa đơn";
            setWarningMessage(message);
            return false;
        }
        if ("".equals(dbDayPayment.getValue().toString())) {
            message = "Không được để trống ngày thanh toán";

            setWarningMessage(message);
            return false;
        }

        return true;

    }

    public void onUpload$btnUpload(UploadEvent evt) throws IOException {

        media = evt.getMedia();
        String extFile = media.getName().replace("\"", "");
        if (!FileUtil.validFileType(extFile)) {
            String sExt = ResourceBundleUtil.getString("extend_file", "config");
            showNotify("Định dạng file không được phép tải lên (" + sExt + ")");
            return;
        }

        if (attachCurrent != null) {
            Messagebox.show("Bạn có đồng ý thay thế tệp biểu mẫu cũ?", "Thông báo", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event evt) throws InterruptedException {
                    if (Messagebox.ON_OK.equals(evt.getName())) {
                        txtFileName.setValue(media.getName());
                        imgDelFile.setVisible(true);
                        String fullPath = attachCurrent.getAttachPath() + attachCurrent.getAttachName();
                        //xóa file cũ nếu có
                        File f = new File(fullPath);
                        if (f.exists()) {
                            ConvertFileUtils.deleteFile(fullPath);
                        }
                        AttachDAOHE attachDAOHE = new AttachDAOHE();
                        attachDAOHE.delete(attachCurrent);
                    }
                }
            });
        } else {
            txtFileName.setValue(media.getName());
            imgDelFile.setVisible(true);
        }

    }

    public void onClick$imgDelFile(Event ev) {
        media = null;
        txtFileName.setValue("");
        imgDelFile.setVisible(false);
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public BindingListModelList<Category> getCatList() {
        return catList;
    }

    public void setCatList(BindingListModelList<Category> catList) {
        this.catList = catList;
    }

    public void setAttachCurrent(Attachs attachCurrent) {
        this.attachCurrent = attachCurrent;
    }

    public Attachs getAttachCurrent() {
        return attachCurrent;
    }

    private Category getCatSelected(List items, Long catId) {
        Category result = new Category();
        if (items.isEmpty()) {
            return new Category();
        }
        if (items.size() == 1 || catId == null) {
            return (Category) items.get(0);
        }
        for (int i = 0; i < items.size(); i++) {
            Category item = (Category) items.get(i);
            if (item.getCategoryId().equals(catId)) {
                result = item;
                break;
            }
        }
        return result;
    }

    private void setWarningMessage(String message) {
        lbWarning.setValue(message);
    }
}
