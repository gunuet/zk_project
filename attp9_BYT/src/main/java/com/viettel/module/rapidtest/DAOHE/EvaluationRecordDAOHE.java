/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAOHE;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.rapidtest.BO.RtEvaluationRecord;
import com.viettel.utils.Constants;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class EvaluationRecordDAOHE extends
        GenericDAOHibernate<RtEvaluationRecord, Long> {

    public EvaluationRecordDAOHE() {
        super(RtEvaluationRecord.class);
    }

    @Override
    public void saveOrUpdate(RtEvaluationRecord obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }

        getSession().flush();
    }

    @Override
    public RtEvaluationRecord findById(Long id) {
        Query query = getSession().getNamedQuery(
                "RtEvaluationRecord.findByRequestCommentId");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (RtEvaluationRecord) result.get(0);
        }
    }

    @Override
    public void delete(RtEvaluationRecord obj) {
        obj.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(obj);
    }
    
    public RtEvaluationRecord getLastEvaluation(Long fileId){
         Query query = getSession().createQuery("select a from RtEvaluationRecord a where a.fileId = :fileId "
                 + "order by a.evaluationRecordId desc");
         query.setParameter("fileId", fileId);
         List<RtEvaluationRecord> lst = query.list();
         if(lst.size()>0){
             return lst.get(0);
         }else{
             return null;
         }
    }

}
