/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAOHE;

import com.viettel.utils.LogUtils;
import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.module.rapidtest.BO.ProcedureFeeSubprocedure;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Administrator
 */
public class ProcedureFeeSubDAOHE extends GenericDAOHibernate<ProcedureFeeSubprocedure, Long> {

    public ProcedureFeeSubDAOHE() {
        super(ProcedureFeeSubprocedure.class);
    }

//    public boolean hasDuplicate(ProcedureFeeSubprocedure rs) {
//        StringBuilder hql = new StringBuilder("select count(r) from ProcedureFeeSubprocedure r where r.isActive = 1 ");
//        List lstParams = new ArrayList();
//        if (rs.getProcedureId()!= null && rs.getProcedureId()> 0L) {
//            hql.append(" and r.id <> ?");
//            lstParams.add(rs.getId());
//        }
//
//        if (rs.getResourceName() != null && rs.getResourceCode().length() > 0) {
//            hql.append(" and (r.resourceName = ? or r.resourceCode = ?) ");
//            lstParams.add(rs.getResourceName().trim());
//            lstParams.add(rs.getResourceCode().trim());
//        }
//
//        Query query = session.createQuery(hql.toString());
//        for (int i = 0; i < lstParams.size(); i++) {
//            query.setParameter(i, lstParams.get(i));
//        }
//        
//        Long count = (Long) query.uniqueResult();
//        return count > 0l;
//
//    }

    public void delete(Long id) {
        ProcedureFeeSubprocedure obj = findById(id);
        obj.setIsActive(0l);
        update(obj);
    }

    public PagingListModel search(ProcedureFeeSubprocedure searchForm, int start, int take) {
        List listParam = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder("select r from ProcedureFeeSubprocedure r where r.isActive = 1 ");
            StringBuilder strCountBuf = new StringBuilder("select count(r) from ProcedureFeeSubprocedure r where r.isActive = 1 ");
            StringBuilder hql = new StringBuilder();
            if (searchForm != null) {
                if (searchForm.getProcedureId()!= null && searchForm.getProcedureId() > 0l) {
                    hql.append(" and r.procedureId = ? ");
                    listParam.add(searchForm.getProcedureId());
                }
                if (searchForm.getFeeId()!= null && searchForm.getFeeId() > 0l) {
                    hql.append(" and r.feeId = ? ");
                    listParam.add(searchForm.getFeeId());
                }
                if (searchForm.getSubProcedureId()!= null && searchForm.getSubProcedureId() > 0l) {
                    hql.append(" and r.subProcedureId = ? ");
                    listParam.add(searchForm.getSubProcedureId());
                }
//                if (searchForm.getResourceName() != null && !"".equals(searchForm.getResourceName())) {
//                    hql.append(" and lower(r.resourceName) like ? escape '/' ");
//                    listParam.add(StringUtils.toLikeString(searchForm.getResourceName()));
//                }



            }
            strBuf.append(hql);
            strCountBuf.append(hql);

            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(i, listParam.get(i));
                countQuery.setParameter(i, listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
}
