/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAOHE;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.rapidtest.BO.VFileRtAttach;
import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Document.Attachs;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class RapidTestAttachDAOHE extends
        GenericDAOHibernate<Attachs, Long> {

    public RapidTestAttachDAOHE() {
        super(Attachs.class);
    }

    @Override
    public void saveOrUpdate(Attachs rapidTestAttach) {
        if (rapidTestAttach != null) {
            super.saveOrUpdate(rapidTestAttach);
            //getSession().getTransaction().commit();
        }
    }

    @Override
    public Attachs findById(Long id) {
        Query query = getSession().getNamedQuery(
                "RapidTestAttach.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Attachs) result.get(0);
        }
    }

    @Override
    public void delete(Attachs rapidTestAttach) {
        rapidTestAttach.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(rapidTestAttach);
    }
    
    public void delete(Long id) {
        Query query = getSession().createQuery("update Attachs a set a.isActive = 0 where a.id = ?");
        query.setParameter(0, id);
        query.executeUpdate();
    }
    
    public List<VFileRtAttach> findRapidTestAttach(Long rapidTestId){
        Query query = getSession().createQuery("select a from VFileRtAttach a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.isActive = 1");
        
        query.setParameter(0, Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        query.setParameter(1, rapidTestId);
        List<VFileRtAttach> lst = query.list();
        return lst;
    }

    public List<VFileRtAttach> findRapidTestAttachByCode(Long rapidTestId, Long code){
        Query query = getSession().createQuery("select a from VFileRtAttach a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.attachType = ? "
                + "and a.isActive = 1");
        
        query.setParameter(0, Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        query.setParameter(1, rapidTestId);
        query.setParameter(2, code);
        List<VFileRtAttach> lst = query.list();
        return lst;
    }

    public List<VFileRtAttachAll> findRapidTestAllAttachByCode(Long rapidTestId, Long code){
        Query query = getSession().createQuery("select a from VFileRtAttachAll a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.attachType = ? "
                + "and a.isActive = 1");
        
        query.setParameter(0, Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        query.setParameter(1, rapidTestId);
        query.setParameter(2, code);
        List<VFileRtAttachAll> lst = query.list();
        return lst;
    }
    
    public List<VFileRtAttachAll> findRapidTestAllLastAttachByCode(Long rapidTestId, Long code){
        Query query = getSession().createQuery("select a from VFileRtAttachAll a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.attachType = ? "
                + "and a.isActive = 1 and a.attachId = (select max(b.attachId) from VFileRtAttachAll b where a.objectId = b.objectId and a.attachType = b.attachType and a.attachCat = b.attachCat and b.isActive = 1)");
        
        query.setParameter(0, Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        query.setParameter(1, rapidTestId);
        query.setParameter(2, code);
        List<VFileRtAttachAll> lst = query.list();
        return lst;
    }

    public List<VFileRtAttachAll> findRapidTestAllLastAttachByCodeList(Long rapidTestId, String codeList){
        Query query = getSession().createQuery("select a from VFileRtAttachAll a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.attachType in ("+ codeList +") "
                + "and a.isActive = 1 and a.attachId = (select max(b.attachId) from VFileRtAttachAll b where a.objectId = b.objectId and a.attachType = b.attachType and a.attachCat = b.attachCat and b.isActive = 1)");
        
        query.setParameter(0, Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        query.setParameter(1, rapidTestId);
        List<VFileRtAttachAll> lst = query.list();
        return lst;
    }

    public List<VFileRtAttachAll> findRapidTestAllAttachByCodeList(Long rapidTestId, String codeList){
        Query query = getSession().createQuery("select a from VFileRtAttachAll a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.attachType in ("+ codeList +") "
                + "and a.isActive = 1");
        
        query.setParameter(0, Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        query.setParameter(1, rapidTestId);
        List<VFileRtAttachAll> lst = query.list();
        return lst;
    }
    
    public int checkSignedFile(Long rapidTestId, String codeList){
        Query query = getSession().createQuery("select a from VFileRtAttachAll a "
                + "where a.attachCat = ? "
                + "and a.objectId = ? "
                + "and a.attachType in ("+ codeList +") "
                + "and a.isActive = 1");
        
        query.setParameter(0, Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
        query.setParameter(1, rapidTestId);
        List<VFileRtAttachAll> lst = query.list();
        if (lst == null || lst.isEmpty()) {
            return Constants.CHECK_VIEW.NOT_VIEW;
        } else {
            return Constants.CHECK_VIEW.VIEW;
        }
    }
}
