///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.viettel.module.rapidtest.DAOHE;
//
//import com.viettel.core.base.DAO.GenericDAOHibernate;
//import com.viettel.core.base.model.PagingListModel;
//import com.viettel.module.rapidtest.BO.RtFile;
//import com.viettel.module.rapidtest.BO.RtFileOld;
//import com.viettel.module.rapidtest.BO.VFileRtfile;
//import com.viettel.utils.Constants;
//import com.viettel.utils.DateTimeUtils;
//import com.viettel.utils.StringUtils;
//
//import com.viettel.voffice.model.SearchModel;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.hibernate.Query;
//
///**
// *
// * @author Linhdx
// */
//public class RapidTestDAOHEOld extends
//        GenericDAOHibernate<RtFileOld, Long> {
//
//    public RapidTestDAOHEOld() {
//        super(RtFileOld.class);
//    }
//
//    @Override
//    public void saveOrUpdate(RtFileOld rapidTest) {
//        if (rapidTest != null) {
//            super.saveOrUpdate(rapidTest);
//        }
//
//        getSession().flush();
//
//    }
//
//    @Override
//    public RtFileOld findById(Long id) {
//        Query query = getSession().getNamedQuery(
//                "RtFileOld.findById");
//        query.setParameter("id", id);
//        List result = query.list();
//        if (result.isEmpty()) {
//            return null;
//        } else {
//            return (RtFileOld) result.get(0);
//        }
//    }
//
//    public VFileRtfile findViewByFileId(Long fileId) {
//        Query query = getSession().createQuery("select a from VFileRtfile a where a.fileId = :fileId");
//        query.setParameter("fileId", fileId);
//        List result = query.list();
//        if (result.isEmpty()) {
//            return null;
//        } else {
//            return (VFileRtfile) result.get(0);
//        }
//    }
//
//    public RtFileOld findByFileId(Long id) {
//        Query query = getSession().getNamedQuery(
//                "RtFileOld.findByFileId");
//        query.setParameter("fileId", id);
//        List result = query.list();
//        if (result.isEmpty()) {
//            return null;
//        } else {
//            return (RtFileOld) result.get(0);
//        }
//    }
//
//    public void delete(RtFileOld vRtFile) {
////        Files findByFileId(vRtFile.getFileId());
////        rapidTest.setIsActive(Constants.Status.INACTIVE);
////        getSession().saveOrUpdate(rapidTest);
//    }
//
//    public List getList(Long deptId,
//            SearchModel model, int activePage, int pageSize,
//            boolean getSize) {
//        StringBuilder hql;
//
//        if (getSize) {
//            hql = new StringBuilder(
//                    "SELECT COUNT(d) FROM VFileRtfile d WHERE isActive = 1 ");
//        } else {
//            hql = new StringBuilder(
//                    "SELECT d FROM VFileRtfile d WHERE isActive = 1 ");
//        }
//
//        /*
//         * model nhất định phải != null
//         */
//        List docParams = new ArrayList();
//        if (model != null) {
//            //todo:fill query
//            if (model.getCreateDateFrom() != null) {
//                hql.append(" AND d.createDate >= ? ");
//                docParams.add(model.getCreateDateFrom());
//            }
//            if (model.getCreateDateTo() != null) {
//                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
//                hql.append(" AND d.createDate < ? ");
//                docParams.add(toDate);
//            }
//            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
//                hql.append(" AND lower(d.nswFileCode) like ? escape '/'");
//                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
//            }
//            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
//                hql.append(" AND lower(d.rapidTestNo) like ? escape '/'");
//                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
//            }
//            if (model.getStatus() != null) {
//                hql.append(" AND d.status = ?");
//                docParams.add(model.getStatus());
//            }
//            if (model.getDocumentTypeCode() != null) {
//                hql.append(" AND d.documentTypeCode = ?");
//                docParams.add(model.getDocumentTypeCode());
//            }
//        }
//
//        hql.append(" order by fileId desc");
//
//        Query fileQuery = session.createQuery(hql.toString());
//        List<RtFile> listObj;
//        for (int i = 0; i < docParams.size(); i++) {
//            fileQuery.setParameter(i, docParams.get(i));
//        }
//        if (getSize) {
//            return fileQuery.list();
//        } else {
//            if (activePage >= 0) {
//                fileQuery.setFirstResult(activePage * pageSize);
//            }
//            if (pageSize >= 0) {
//                fileQuery.setMaxResults(pageSize);
//            }
//            listObj = fileQuery.list();
//        }
//
//        return listObj;
//
//    }
//
////    public List<VFileRtfile> findFilesByReceiverAndDeptId(SearchModel model, Long receiverId, Long receiveDeptId, int start, int take) {
////        String hql = "SELECT distinct(n) FROM VFileRtfile n, Process p WHERE "
////                + " n.fileId = p.objectId AND "
////                + " n.fileType = p.objectType AND "
////                + " n.status = p.status AND "
////                + " (p.receiveUserId = :receiverId OR "
////                + " (p.receiveUserId is null AND "
////                + " p.receiveGroupId = :receiveDeptId)) "
////                + "order by n.fileId desc";
////        Query query = getSession().createQuery(hql);
////        query.setParameter("receiverId", receiverId);
////        query.setParameter("receiveDeptId", receiveDeptId);
////        List<VFileRtfile> listFiles = query.list();
////        return listFiles;
////    }
//    public PagingListModel findFilesByReceiverAndDeptId(SearchModel searchModel, 
//            Long receiverId, Long receiveDeptId, int start, int take) {
//        List listParam = new ArrayList();
//        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) ");
//        StringBuilder strCountBuf = new StringBuilder("select count(n) ");
//        StringBuilder hql = new StringBuilder();
//        if(null != searchModel.getMenuTypeStr())
//            switch (searchModel.getMenuTypeStr()) {
//            case Constants.MENU_TYPE.WAITPROCESS_STR:
//                hql.append(" FROM VFileRtfile n, Process p WHERE"
//                        + " n.fileId = p.objectId AND "
//                        + " n.fileType = p.objectType AND "
//                        + " n.status = p.status AND "
//                        + " p.receiveUserId = ? ");
//                listParam.add(receiverId);
//                break;
//            case Constants.MENU_TYPE.PROCESSING_STR:
//                hql.append(" FROM VFileRtfile n, Process p WHERE"
//                        + " n.fileId = p.objectId AND "
//                        + " n.fileType = p.objectType AND "
//                        + " n.status != p.status AND "
//                        + " p.receiveUserId = ? ");
//                listParam.add(receiverId);
//                break;
//            case Constants.MENU_TYPE.PROCESSED_STR:
//                hql.append(" FROM VFileRtfile n, Process p WHERE"
//                        + " n.fileId = p.objectId AND "
//                        + " n.fileType = p.objectType AND "
//                        + " n.status = p.status AND "
//                + " p.receiveUserId = ? ");
//                listParam.add(receiverId);
//                break;
//            case Constants.MENU_TYPE.DEPT_PROCESS_STR:
//                hql.append(" FROM VFileRtfile n, Process p WHERE"
//                        + " n.fileId = p.objectId AND "
//                        + " n.fileType = p.objectType AND "
//                        + " n.status = p.status AND "
//                        + " (p.receiveUserId is null AND "
//                + " p.receiveGroupId = ? ) ");
//                listParam.add(receiveDeptId);
//                break;
//            default:
//                hql.append(" FROM VFileRtfile n, Process p WHERE"
//                        + " n.fileId = p.objectId AND "
//                        + " n.fileType = p.objectType AND "
//                        + " n.status = p.status AND "
//                        + " p.receiveUserId = ? ");
//                listParam.add(receiverId);
//                break;
//        }
//        
//        //listParam.add(receiverId);
//        //listParam.add(receiveDeptId);
//        if (searchModel != null) {
//            //todo:fill query
//            if (searchModel.getCreateDateFrom() != null) {
//                hql.append(" AND n.createDate >= ? ");
//                listParam.add(searchModel.getCreateDateFrom());
//            }
//            if (searchModel.getCreateDateTo() != null) {
//                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
//                hql.append(" AND n.createDate < ? ");
//                listParam.add(toDate);
//            }
//            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
//                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
//                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
//            }
//            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
//                hql.append(" AND lower(n.rapidTestNo) like ? escape '/'");
//                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
//            }
//            if (searchModel.getStatus() != null) {
//                hql.append(" AND n.status = ?");
//                listParam.add(searchModel.getStatus());
//            }
//            if (searchModel.getDocumentTypeCode() != null) {
//                hql.append(" AND n.documentTypeCode = ?");
//                listParam.add(searchModel.getDocumentTypeCode());
//            }
//        }
//
//        hql.append(" order by n.fileId desc");
//        strBuf.append(hql);
//        strCountBuf.append(hql);
//
//        Query query = session.createQuery(strBuf.toString());
//        Query countQuery = session.createQuery(strCountBuf.toString());
//
//        for (int i = 0; i < listParam.size(); i++) {
//            query.setParameter(i, listParam.get(i));
//            countQuery.setParameter(i, listParam.get(i));
//        }
//
//        query.setFirstResult(start);
//        if (take < Integer.MAX_VALUE) {
//            query.setMaxResults(take);
//        }
//
//        List lst = query.list();
//        Long count = (Long) countQuery.uniqueResult();
//        PagingListModel model = new PagingListModel(lst, count);
//        return model;
//    }
//
//    public PagingListModel findFilesByCreatorId(SearchModel searchModel, Long creatorId, int start, int take) {
//        List listParam = new ArrayList();
//        StringBuilder strBuf = new StringBuilder("SELECT distinct(n) FROM VFileRtfile n WHERE n.isActive = 1  ");
//        StringBuilder strCountBuf = new StringBuilder("select count(n) FROM VFileRtfile n WHERE n.isActive = 1  ");
//        StringBuilder hql = new StringBuilder();
//        hql.append(" AND n.creatorId = ? ");
//        listParam.add(creatorId);
//
//        if (searchModel != null) {
//            //todo:fill query
//            if (searchModel.getCreateDateFrom() != null) {
//                hql.append(" AND n.createDate >= ? ");
//                listParam.add(searchModel.getCreateDateFrom());
//            }
//            if (searchModel.getCreateDateTo() != null) {
//                Date toDate = DateTimeUtils.addOneDay(searchModel.getCreateDateTo());
//                hql.append(" AND n.createDate < ? ");
//                listParam.add(toDate);
//            }
//            if (searchModel.getnSWFileCode() != null && searchModel.getnSWFileCode().trim().length() > 0) {
//                hql.append(" AND lower(n.nswFileCode) like ? escape '/'");
//                listParam.add(StringUtils.toLikeString(searchModel.getnSWFileCode()));
//            }
//            if (searchModel.getRapidTestNo() != null && searchModel.getRapidTestNo().trim().length() > 0) {
//                hql.append(" AND lower(n.rapidTestNo) like ? escape '/'");
//                listParam.add(StringUtils.toLikeString(searchModel.getRapidTestNo()));
//            }
//            if (searchModel.getStatus() != null) {
//                hql.append(" AND n.status = ?");
//                listParam.add(searchModel.getStatus());
//            }
//            if (searchModel.getDocumentTypeCode() != null) {
//                hql.append(" AND n.documentTypeCode = ?");
//                listParam.add(searchModel.getDocumentTypeCode());
//            }
//        }
//
//        hql.append(" order by n.fileId desc");
//
//        strBuf.append(hql);
//        strCountBuf.append(hql);
//
//        Query query = session.createQuery(strBuf.toString());
//        Query countQuery = session.createQuery(strCountBuf.toString());
//
//        for (int i = 0; i < listParam.size(); i++) {
//            query.setParameter(i, listParam.get(i));
//            countQuery.setParameter(i, listParam.get(i));
//        }
//
//        query.setFirstResult(start);
//        if (take < Integer.MAX_VALUE) {
//            query.setMaxResults(take);
//        }
//
//        List lst = query.list();
//        Long count = (Long) countQuery.uniqueResult();
//        PagingListModel model = new PagingListModel(lst, count);
//        return model;
//    }
//
//}
