/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAOHE;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.payment.BO.VRtPaymentInfo;

import com.viettel.utils.Constants;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.model.SearchModel;
import com.viettel.module.payment.search.SearchPaymentModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */

public class RapidTestPaymentInfoDAOHE extends GenericDAOHibernate<VRtPaymentInfo, Long> {

    public RapidTestPaymentInfoDAOHE() {
        super(VRtPaymentInfo.class);
    }

    public List getList(Long deptId,
            SearchPaymentModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VRtPaymentInfo d WHERE d.isActive = 1  ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VRtPaymentInfo d WHERE  d.isActive = 1  ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();

        docHQL.append(" AND d.status = ? ");
        docParams.add(Constants.RAPID_TEST.PAYMENT.PAY_ALREADY);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.nswFileCode) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatusPayment() != null) {
                docHQL.append(" AND d.statuspayment = ?");
                docParams.add(model.getStatusPayment());
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }
        }

        docHQL.append(" order by fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        List<VRtPaymentInfo> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    // payment cho doanh nghiep
    public List getListRequestPayment(Long deptId,
            SearchPaymentModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VRtPaymentInfo d WHERE d.isActive = 1 ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1  ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        docHQL.append(" AND d.createDeptId = ? ");
        docParams.add(deptId);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.nswFileCode) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatusPayment() != null) {
                docHQL.append(" AND d.statuspayment = ?");
                docParams.add(model.getStatusPayment());
            } else {
                docHQL.append(" AND d.status is not null");
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }
            //d.status is not null
            if (model.getTypeOrder() != null) {

                if (model.getTypeOrder() == 1L) {
                    docHQL.append(" order by d.rapidTestNo ");
                } else if (model.getTypeOrder() == 2L) {
                    docHQL.append(" order by d.documentTypeCode ");
                } else if (model.getTypeOrder() == 3L) {
                    docHQL.append(" order by d.rapidTestName ");
                } else {
                    docHQL.append(" order by d.fileId desc");
                }
                
            } else {
                docHQL.append(" order by d.fileId desc");
            }

        }
        Query fileQuery = session.createQuery(docHQL.toString());
        //LogUtils.addLog(docHQL.toString());
        List<VRtPaymentInfo> listObj;

        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
             //LogUtils.addLog("size" + fileQuery.list().size());
            
            return fileQuery.list();
            
        } else {
            
//            LogUtils.addLog("size result" + fileQuery.list().size());
//             for(int i=0;i<fileQuery.list().size();i++){
//            LogUtils.addLog("chi so tt"+((VRtPaymentInfo)(fileQuery.list().get(i))).getFeeName());
//        }
            
            if (activePage >= 0) {
                
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
            
        }
         
        return listObj;

    }

    
    public List getListRequestAcceptPayment(Long deptId,
            SearchPaymentModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VRtPaymentInfo d WHERE d.isActive = 1   ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1   ");
        }

        /*
         * model nhất định phải != null
         */

        List docParams = new ArrayList();

        //docHQL.append(" AND d.createDeptId = ? ");
        //docParams.add(deptId);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.nswFileCode) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatusPayment()!= null) {
                docHQL.append(" AND d.statuspayment = ?");
                docParams.add(model.getStatusPayment());
            } else {
                docHQL.append(" AND (d.status = 2 OR d.status = 3 OR d.status = 4)");
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }


            if (model.getTypeOrder() != null) {

                if (model.getTypeOrder() == 1L) {
                    docHQL.append(" order by d.rapidTestNo ");
                } else if (model.getTypeOrder() == 2L) {
                    docHQL.append(" order by d.documentTypeCode ");
                } else if (model.getTypeOrder() == 3L) {
                    docHQL.append(" order by d.rapidTestName ");
                } else {
                    docHQL.append(" order by d.fileId desc");
                }
            } else {
                docHQL.append(" order by d.fileId desc");
            }

        }



        Query fileQuery = session.createQuery(docHQL.toString());
        List<VRtPaymentInfo> listObj;

        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    // danh sach da thanh toan theo payment info id
    public List getListRequestPaymentInfoId(Long deptId,
            Long paymentInfoId) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1   ");

        docHQL.append(" AND d.paymentInfoId = ? ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, paymentInfoId);
        List<VRtPaymentInfo> listObj;

        listObj = fileQuery.list();

        return listObj;

    }

    // danh sach da thanh toan theo id
    public List getListRequestPaymentBillId(Long deptId,
            Long BillId) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1   ");

        docHQL.append(" AND d.status = 2 ");

        docHQL.append(" AND d.billId = ? ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());

        fileQuery.setParameter(0, BillId);
        List<VRtPaymentInfo> listObj;

        listObj = fileQuery.list();

        return listObj;

    }

    // Danh sach chua thanh toan
    public List getListRequestPaymentNotPayment(Long deptId) {
        StringBuilder docHQL;
        docHQL = new StringBuilder(
                "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1   ");

        docHQL.append(" AND d.status = 1 ");
        docHQL.append(" AND d.createDeptId = ? ");

        docHQL.append(" order by d.fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        fileQuery.setParameter(0, deptId);
        List<VRtPaymentInfo> listObj;
        listObj = fileQuery.list();

        return listObj;

    }

//
    public List getList(Long deptId,
            SearchModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VRtPaymentInfo d WHERE d.isActive = 1   ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1   ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        docHQL.append(" AND d.status = ? ");
        docParams.add(Constants.RAPID_TEST.PAYMENT.PAY_ALREADY);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.nswFileCode) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatus() != null) {
                docHQL.append(" AND d.status = ?");
                docParams.add(model.getStatus());
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }

        }

        docHQL.append(" order by fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        List<VRtPaymentInfo> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }

    public List getListRequestPayment(Long deptId,
            SearchModel model, int activePage, int pageSize,
            boolean getSize) {
        StringBuilder docHQL;

        if (getSize) {
            docHQL = new StringBuilder(
                    "SELECT COUNT(d) FROM VRtPaymentInfo d WHERE d.isActive = 1   ");
        } else {
            docHQL = new StringBuilder(
                    "SELECT d FROM VRtPaymentInfo d WHERE d.isActive = 1   ");
        }

        /*
         * model nhất định phải != null
         */
        List docParams = new ArrayList();
        docHQL.append(" AND d.status is not null ");
        //docParams.add(Constants.RAPID_TEST.PAYMENT.PAY_NEW);
        if (model != null) {
            //todo:fill query
            if (model.getCreateDateFrom() != null) {
                docHQL.append(" AND d.createDate >= ? ");
                docParams.add(model.getCreateDateFrom());
            }
            if (model.getCreateDateTo() != null) {
                Date toDate = DateTimeUtils.addOneDay(model.getCreateDateTo());
                docHQL.append(" AND d.createDate < ? ");
                docParams.add(toDate);
            }
            if (model.getnSWFileCode() != null && model.getnSWFileCode().trim().length() > 0) {
                docHQL.append(" AND lower(d.nswFileCode) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getnSWFileCode()));
            }
            if (model.getRapidTestNo() != null && model.getRapidTestNo().trim().length() > 0) {
                docHQL.append(" AND lower(d.rapidTestNo) like ? escape '/'");
                docParams.add(StringUtils.toLikeString(model.getRapidTestNo()));
            }
            if (model.getStatus() != null) {
                docHQL.append(" AND d.statusCode = ?");
                docParams.add(model.getStatus());
            }
            if (model.getDocumentTypeCode() != null) {
                docHQL.append(" AND d.documentTypeCode = ?");
                docParams.add(model.getDocumentTypeCode());
            }
        }

        docHQL.append(" order by fileId desc");

        Query fileQuery = session.createQuery(docHQL.toString());
        List<VRtPaymentInfo> listObj;
        for (int i = 0; i < docParams.size(); i++) {
            fileQuery.setParameter(i, docParams.get(i));
        }
        if (getSize) {
            return fileQuery.list();
        } else {
            if (activePage >= 0) {
                fileQuery.setFirstResult(activePage * pageSize);
            }
            if (pageSize >= 0) {
                fileQuery.setMaxResults(pageSize);
            }
            listObj = fileQuery.list();
        }

        return listObj;

    }
}
