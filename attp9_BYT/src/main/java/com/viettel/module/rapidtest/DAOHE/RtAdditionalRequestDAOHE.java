/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.DAOHE;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.rapidtest.BO.RtAdditionalRequest;
import com.viettel.utils.Constants;

import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class RtAdditionalRequestDAOHE extends
        GenericDAOHibernate<RtAdditionalRequest, Long> {

    public RtAdditionalRequestDAOHE() {
        super(RtAdditionalRequest.class);
    }

    @Override
    public void saveOrUpdate(RtAdditionalRequest obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }

        getSession().flush();
    }

    @Override
    public RtAdditionalRequest findById(Long id) {
        Query query = getSession().getNamedQuery(
                "RtAdditionalRequest.findByAdditionalRequestId");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (RtAdditionalRequest) result.get(0);
        }
    }

    @Override
    public void delete(RtAdditionalRequest obj) {
        obj.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(obj);
    }
    
    public List<RtAdditionalRequest> findAllActiveByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from RtAdditionalRequest a where a.fileId = :fileId "
                + "and a.isActive = :isActive");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        return result;
    }

}
