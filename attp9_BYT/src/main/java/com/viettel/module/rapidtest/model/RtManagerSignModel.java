/*
' * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.model;

import java.io.StringWriter;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.viettel.module.rapidtest.BO.RtFile;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.utils.LogUtils;

/**
 *
 * @author Vu Manh Ha
 */
public class RtManagerSignModel {

	private RtFile rtFile;
	private String createName;
	private Date signedDate;
	private String sendNo;
	private String businessName;
	private String businessAddress;
	private String pathTemplate;
	
	private Long rtPermitId;
	private Long rtSignType;
	private Long objectType;
	
	private String deptParent;
	private String receiptDeptName;
	private String contentDispatch;
	private String leaderSigned;
	

	public RtManagerSignModel(Long fileId, Long objectTypeIn, Long rtSignTypeIn) {
		
		RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
		rtFile = objDAOHE.findByFileId(fileId);
		rtSignType = rtSignTypeIn;
		objectType = objectTypeIn;

		RapidTestDAOHE vFileRtfileDAOHE = new RapidTestDAOHE();
		VFileRtfile file = vFileRtfileDAOHE.findViewByFileId(fileId);
		
		businessName = file.getBusinessName();
		businessAddress = file.getBusinessAddress();
		createName = file.getCreatorName();
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}


	public String getPathTemplate() {
		return pathTemplate;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public void setCosmeticPermitId(Long cosmeticPermitId) {
		this.rtPermitId = cosmeticPermitId;
	}

	public void setPathTemplate(String pathTemplate) {
		this.pathTemplate = pathTemplate;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public Date getSignedDate() {
		return signedDate;
	}

	public void setSignedDate(Date signedDate) {
		this.signedDate = signedDate;
	}

	public String getSendNo() {
		return sendNo;
	}

	public void setSendNo(String sendNo) {
		this.sendNo = sendNo;
	}

	public String getDeptParent() {
		return deptParent;
	}

	public void setDeptParent(String deptParent) {
		this.deptParent = deptParent;
	}

	public String getReceiptDeptName() {
		return receiptDeptName;
	}

	public void setReceiptDeptName(String receiptDeptName) {
		this.receiptDeptName = receiptDeptName;
	}

	public String getContentDispatch() {
		return contentDispatch;
	}

	public void setContentDispatch(String contentDispatch) {
		this.contentDispatch = contentDispatch;
	}

	public String getLeaderSigned() {
		return leaderSigned;
	}

	public void setLeaderSigned(String leaderSigned) {
		this.leaderSigned = leaderSigned;
	}

	public String toXML() throws JAXBException {
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(RtManagerSignModel.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			StringWriter builder = new StringWriter();
			jaxbMarshaller.marshal(this, builder);
			return builder.toString();
		} catch (Exception en) {
			LogUtils.addLogDB(en);
			return null;
		}

	}

	public RtFile getImportOrderFile() {
		return rtFile;
	}

	public void setImportOrderFile(RtFile importOrderFile) {
		this.rtFile = importOrderFile;
	}

	public Long getRtSignType() {
		return rtSignType;
	}

	public void setRtSignType(Long rtSignType) {
		this.rtSignType = rtSignType;
	}

	public Long getRtPermitId() {
		return rtPermitId;
	}

	public void setRtPermitId(Long rtPermitId) {
		this.rtPermitId = rtPermitId;
	}

	public Long getObjectType() {
		return objectType;
	}

	public void setObjectType(Long objectType) {
		this.objectType = objectType;
	}
}
