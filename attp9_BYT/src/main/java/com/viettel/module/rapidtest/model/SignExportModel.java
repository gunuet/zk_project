/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.module.rapidtest.model;

import java.util.Date;

import com.viettel.module.rapidtest.BO.VFileRtAttachAll;
import com.viettel.newsignature.plugin.SignPdfFile;

/**
 *
 * @author ChucHV
 */
public class SignExportModel {

private String filePath;
private String signFileFinalName;
private String certSerial;
private String base64Hash;
private VFileRtAttachAll file;
private SignPdfFile pdfSig;
private String outPutFileFinal;

public String getFilePath() {
	return filePath;
}
public void setFilePath(String filePath) {
	this.filePath = filePath;
}
public String getSignFileFinalName() {
	return signFileFinalName;
}
public void setSignFileFinalName(String signFileFinalName) {
	this.signFileFinalName = signFileFinalName;
}
public String getCertSerial() {
	return certSerial;
}
public void setCertSerial(String certSerial) {
	this.certSerial = certSerial;
}
public String getBase64Hash() {
	return base64Hash;
}
public void setBase64Hash(String base64Hash) {
	this.base64Hash = base64Hash;
}
public VFileRtAttachAll getFile() {
	return file;
}
public void setFile(VFileRtAttachAll file) {
	this.file = file;
}
public SignPdfFile getPdfSig() {
	return pdfSig;
}
public void setPdfSig(SignPdfFile pdfSig) {
	this.pdfSig = pdfSig;
}
public String getOutPutFileFinal() {
	return outPutFileFinal;
}
public void setOutPutFileFinal(String outPutFileFinal) {
	this.outPutFileFinal = outPutFileFinal;
}

}
