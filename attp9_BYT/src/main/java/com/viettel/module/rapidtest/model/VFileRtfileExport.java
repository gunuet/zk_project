package com.viettel.module.rapidtest.model;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.ListModelList;

import com.google.gson.Gson;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.importOrder.DAO.ImportOrderProductDAO;
import com.viettel.module.rapidtest.DAO.VFileRtflieDAO;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.utils.Constants;
import com.viettel.voffice.BO.Files;

public class VFileRtfileExport {

    public List<RtAssay> getList() {
        return list;
    }

    public void setList(List<RtAssay> list) {
        this.list = list;
    }
    private String pathTemplate;
    private VFileRtfile rtfile;
    private String leaderSigned;
    List<RtAssay> list;

    public String getPathTemplate() {
        return pathTemplate;
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

    public VFileRtfileExport(Long fileId) {
        VFileRtflieDAO rtflieDAO = new VFileRtflieDAO();
        rtfile = rtflieDAO.findById(fileId);
    }

    public VFileRtfile getRtfile() {
        return rtfile;
    }

    public void setRtfile(VFileRtfile rtfile) {
        this.rtfile = rtfile;
    }

    public String getLeaderSigned() {
        return leaderSigned;
    }

    public void setLeaderSigned(String leaderSigned) {
        this.leaderSigned = leaderSigned;
    }

}
