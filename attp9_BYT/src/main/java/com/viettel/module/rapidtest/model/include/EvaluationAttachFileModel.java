/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.rapidtest.model.include;

import org.zkoss.util.media.Media;
/**
 *
 * @author ThanhTM
 */
public class EvaluationAttachFileModel {
    private Long attachID;
    private String fileName;
    private String fileTypeName;
    private Media fileContent;
    private Long fileType;
    private Long objId;
    private Long objType;
    private String attachDes;
    private String filePath;
    
	public Long getAttachID() {
		return attachID;
	}
	public void setAttachID(Long attachID) {
		this.attachID = attachID;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileTypeName() {
		return fileTypeName;
	}
	public void setFileTypeName(String fileTypeName) {
		this.fileTypeName = fileTypeName;
	}
	public Media getFileContent() {
		return fileContent;
	}
	public void setFileContent(Media fileContent) {
		this.fileContent = fileContent;
	}
	public Long getFileType() {
		return fileType;
	}
	public void setFileType(Long fileType) {
		this.fileType = fileType;
	}
	public Long getObjId() {
		return objId;
	}
	public void setObjId(Long objId) {
		this.objId = objId;
	}
	public Long getObjType() {
		return objType;
	}
	public void setObjType(Long objType) {
		this.objType = objType;
	}
	public String getAttachDes() {
		return attachDes;
	}
	public void setAttachDes(String attachDes) {
		this.attachDes = attachDes;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
    
}
