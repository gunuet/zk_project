/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.module.rapidtest.model.include;

/**
 *
 * @author ThanhTM
 */
public class EvaluationModel {
    private String resonRequest;
    private String resultEvaluation;
    
    public String getResonRequest() {
        return resonRequest;
    }

    public void setResonRequest(String resonRequest) {
        this.resonRequest = resonRequest;
    }
    
     public String getResultEvaluation() {
        return resultEvaluation;
    }

    public void setResultEvaluation(String resultEvaluation) {
        this.resultEvaluation = resultEvaluation;
    }
    
}
