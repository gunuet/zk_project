package com.viettel.module.rest;

import java.io.File;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.springframework.stereotype.Component; 
import org.zkoss.zk.ui.util.Clients;
import com.viettel.utils.Constants;
import com.viettel.utils.RSAUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.DAOHE.AttachDAOHE;


@Component
@Path("/RestAPI")
public class RestAPI {

//    @GET
//    @Path("/downloadFiles")
//    @Produces(MediaType.TEXT_HTML)
//    public Response downloadFiles(@QueryParam("id") int id) throws Exception {
//        Response response = null;
//        String nswConfigFileUploadEncrypt = ResourceBundleUtil.getString("nsw_config_file_upload_encrypt", "config");
//        String b = enc;
//        b = "1";
//        if (id != 0 && !"true".equals(nswConfigFileUploadEncrypt)) {
//            Long attachId = Long.parseLong(String.valueOf(id));
//            AttachDAOHE attDAOHE = new AttachDAOHE();
//            Attachs att = attDAOHE.findById(attachId);
//
//            String path = att.getAttachPath() + "_" + att.getAttachName();
//            File f = new File(path);
//            if (f.exists()) {
//                ResponseBuilder builder = Response.ok(f);
//                builder.header("Content-Disposition", "attachment; filename=" + f.getName());
//                response = builder.build();
//            } else {
//                showNotification("File khong con ton tai", Constants.Notification.INFO);
//            }
//        }
//        else{
//            showNotification("Ban khong co quyen tai file", Constants.Notification.INFO);
//        }
//        return response;
//    }
    
    @GET
    @Path("/downloadFiles")
    @Produces(MediaType.TEXT_HTML)
    public Response downloadFiles(@QueryParam("enc") String enc) throws Exception {
//        String time;
        String idStr =null;
        RSAUtils util = new RSAUtils();
        String param = util.decrypt(enc);
        if (param != null && param.length() > 0) {
            String[] lst = param.split("&");
            for (String obj : lst) {
                String[] str = obj.split("=");
                if (str != null && str.length > 0) {
                    if ("id".equals(str[0])) {
                        idStr = str[1];
                    }
//                    if ("time".equals(str[0])) {
//                        time = str[1];
//                    }
                }
            }
        }
        Response response = null;
        if (idStr != null && !"0".equals(idStr)) {
            Long attachId = Long.parseLong(String.valueOf(idStr));
            AttachDAOHE attDAOHE = new AttachDAOHE();
            Attachs att = attDAOHE.findById(attachId);

            String path = att.getAttachPath() + "_" + att.getAttachName();
            File f = new File(path);
            if (f.exists()) {
                ResponseBuilder builder = Response.ok(f);
                builder.header("Content-Disposition", "attachment; filename=" + f.getName());
                response = builder.build();
            } else {
                showNotification("File khong con ton tai", Constants.Notification.ERROR);
            }
        } else{
                showNotification("Không đúng tham số download", Constants.Notification.ERROR);
        }
        return response;
    }
    
    public void showNotification(String message, String type) {
        int time = Constants.Notification.TIME_SHOW_SUCCSESS; 
        if (type.equals(Constants.Notification.ERROR)) {
            time = Constants.Notification.TIME_SHOW_ERROR; 
        }
        Clients.showNotification(message, type, null, "middle_center", time);
    }
}
