package com.viettel.sign;

import java.io.File;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.util.Date;
import java.util.List;
import org.apache.xml.security.Init;
import org.apache.xml.security.utils.Base64;

public class SignPdfFile
        extends com.viettel.signature.plugin.ViettelFileSigner {

    private String tmpFile;
    private Date signDate;
    private byte[] hash;
    private Certificate[] chain;
    private static String workingField;

    public static String getWorkingField() {
        return workingField;
    }

    public static void setWorkingField(String workingField) {
        SignPdfFile.workingField = workingField;
    }

    public String createHash(String filePath, Certificate[] chain)
            throws Exception {
        Init.init();
        PDFServerClientSignature pdfSig = new PDFServerClientSignature();
        File tempFile = File.createTempFile("temp", ".pdf");
        Date signDate = new Date();
        List<byte[]> lstHash = pdfSig.createHash(filePath, tempFile.getAbsolutePath(), chain, "Nnt Ky", "Viet Nam", signDate);
        this.tmpFile = tempFile.getAbsolutePath();
        this.signDate = signDate;
        this.hash = ((byte[]) lstHash.get(1));
        this.chain = chain;
        return Base64.encode(encodeData((byte[]) lstHash.get(0), "SHA1"));
    }

    @Override
    public void insertSignature(String extSig, String destFile)
            throws Exception {
        PDFServerClientSignature pdfSig = new PDFServerClientSignature();
        pdfSig.insertSignature(this.tmpFile, destFile, SignPdfFile.getWorkingField(), this.hash, Base64.decode(extSig), this.chain, this.signDate);
        new File(this.tmpFile).delete();
    }

    private byte[] encodeData(byte[] orginalData, String algorithm)
            throws Exception {
        return MessageDigest.getInstance(algorithm).digest(orginalData);
    }
}
