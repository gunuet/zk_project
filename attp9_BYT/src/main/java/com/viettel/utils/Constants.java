/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.utils;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ngoctm3@viettel.com.vn
 * @since since_text
 * @version 1.0
 */
public final class Constants {

    public static String NSW_FUNCTION(Long k) {
        if (k < 10L) {
            return "0" + k.toString();
        }
        return k.toString();
    }

    public static String NSW_TYPE(Long k) {
        if (k < 10L) {
            return "0" + k.toString();
        }
        return k.toString();
    }
    public static final String OBJECT_CONSTANT = "OBJECT";
    public static final Long OBJECT_DOC_OUT = 301L;
    public static final long COMBOBOX_HEADER_VALUE = -1l;
    public static final String COMBOBOX_HEADER_VALUE_STRING = "-1";
    public static final String COMBOBOX_HEADER_TEXT = "--Tất cả--";
    public static final String COMBOBOX_HEADER_TEXT_SELECT = "--Chọn--";
    public static final String COMBOBOX_HEADER_PROCESS_SELECT = "--Chọn thao tác xử lý--";
    public static final String LM_NAME_FIELD = "name";
    public static final long default_profile_id = 22L;
    public static final int tree_wrap_text_length = 20;
    public static final long max_semesester_number = 15L;
    public static final String VSA_USER_TOKEN = "vsaUserToken";
    public static final String prefix_outsite_office = "9999";
    public static final int PAGE_ZISE = 10;
    public static final Long CUC_ATTP_ID = 3402L;
    public static final Long CUC_QLD_ID = 3300L;
    public static final Long VU_TBYT_ID = 3409L;
    public static final Long CUC_ATTP_XNN_ID = 3402L;
    public static final Long BOYTE = 1101L;
    public static final Long IS_VALIDATE_OK = 1L;

    public static final Long DOC_TYPE_CODE_THEMMOI = 2050l;
    public static final Long DOC_TYPE_CODE_GIAHAN = 8801l;
    public static final Long DOC_TYPE_CODE_BOSUNG = 8800l;
    
    
    public static final Long FILE_TYPE_DYCNK = 1950L;
    public static final Long FILE_TYPE_TTB = 4950L;

    public static final String IS_VALIDATE_NOT_OK_SR = "Bạn vui lòng nhập đủ thông tin";
    private static final  char[] captchars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
        'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
        'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
        'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    
    public static char[] getCaptchars(){
        return captchars;
    }

    public interface NSW_SERVICE {

        public static final String DEPT_CODE = "ATTP";
        public static final String DEPT_NAME = "Cục An toàn thực phẩm";
        public static final String DEPT_CODE_TTB = "TTB";
        public static final String DEPT_NAME_TTB = "Vụ trang thiết bị";
        public static final String TYPE_GIAM = "1";
        public static final String TYPE_GIAM_HT = "GIAM";
        public static final String TYPE_THUONG = "2";
        public static final String TYPE_THUONG_HT = "THUONG";
        public static final String TYPE_CHAT = "3";
        public static final String TYPE_CHAT_HT = "CHAT";
        public static final Long TYPE_DN = 0L;
        public static final Long TYPE_CV = 1L;
        public static final String PLACE_NATIONAL = "nation";
        public static final String PLACE_PROVINCE = "province";
        public static final String HN_CODE = "HN";

        public static class FEE_STATUS {

            public static final Long MOI_TAO = 1L;
            public static final Long MOI_NOP = 2L;
            public static final Long XAC_NHAN = 3L;
            public static final Long TU_CHOI = 4L;
            public static final String FEE_NEW = "0";
            public static final String FEE_EDIT = "1";

        }

        public static class COMMENS {

            public static final String TIEP_NHAN_KT = "Cơ quan kiểm tra tiếp nhận kiểm tra các mặt hàng kiểm tra thường, chặt. Đề nghị Doanh nghiệp gửi lại thời gian, địa điểm tập kết hàng hóa";
            public static final String CHO_KQKT = "Hồ sơ cần kiểm nghiệm bổ sung, doanh nghiệp chờ kết quả kiểm tra";
            public static final String ERR_PERMIT_REDUCE = "Lỗi gửi kết quả xử lý mặt hàng kiểm tra giảm (ko có dữ liệu Permit)";
            public static final String ERR_PERMIT = "Lỗi gửi kết quả xử lý mặt hàng kiểm tra thường và chặt (ko có dữ liệu Permit)";
        }
    }

    public interface CALENDAR_STATUS {

        public static final Long NEW_CREATE = 0L; // Mới tao, chua nop
        public static final Long APPROVE_WAIT = 1L; // gui cho phe duyet
        public static final Long APPROVE_ACCEPT = 2L; // Phe duyet
        public static final Long APPROVE_REJECT = 3L; // Tu choi
        public static final Long APPROVE_CANCEL = 4L; // Da phe duyet, huy
        public static final Long USER_CANCEL = 5L; // Nguoi dung huy lich
    }

    public interface CALENDAR_VIEWTYPE {

        public static final Long VIEW_ONLY = 0L;
        public static final Long DEPT_APPROVE = 1L;
        public static final Long USER_APPROVE = 2L;
    }

    public interface CALENDAR_PARTICIPANT_TYPE {

        public static final Long PARTICIPANT = 0l;
        public static final Long PREPARE = 1l;
        public static final Long CHIEF = 2l;
        public static final Long APPROVE = 3l;
    }

    public interface FUNCTION_MESSAGE_IMPORT_ODDER {

        public String SENMS_04 = "senMS_04";// Hàm yêu cầu SDBS
        public String SENMS_38 = "senMS_38";// Hàm gửi đơn đăng ký đã được phê duyệt
        public String SENMS_11_12_13_14 = "senMS_11_12_13_14";// Hàm yêu cầu nộp phí, nộp lại phí
        public String SENMS_10 = "senMS_10";// Hàm gửi thông báo tiếp nhận kiểm tra (của CQKT)		
        public String SENMS_23 = "senMS_23";// Hàm gửi thông báo thời gian, kế hoạch kiểm tra
        public String SENMS_15 = "senMS_15";// Hàm gửi kết quả xử lý các mặt hàng kiểm tra giảm
        public String SENMS_16 = "senMS_16";// Hàm gửi kết quả xử lý các mặt hàng kiểm tra thường và chặt
        public String SENMS_24 = "senMS_24";// Hàm gửi thông báo chờ kết quả kiểm tra (trường hợp kiểm nghiệm bổ sung)
        
        public String SENMS_18 = "senMS_18";// Hàm gửi thông báo thời gian, kế hoạch kiểm tra
        public String SENMS_26 = "senMS_26";// Hàm gửi đồng ý cách xử lý mặt hàng không đạt
        public String SENMS_27 = "senMS_27";// Hàm gửi yêu cầu bổ sung xử lý mặt hàng ko đạt
        public String SENMS_29 = "senMS_29";// Hàm gửi đồng ý báo cáo xử lý xử lý mặt hàng không đạt
    }

    public interface FUNCTION_MESSAGE_RT {

        public String SendMs_03 = "sendMs_03";// Hàm gửi hẹn trả kết quả
        public String SendMs_04 = "sendMs_04";// Hàm yêu cầu SDBS
        public String SendMs_05 = "sendMs_05";// Hàm từ chối chứng nhận
        public String SendMs_06 = "sendMs_06";// Hàm cấp giấy chứng nhận		
        public String SendMs_07 = "sendMs_07";// Hàm cấp giấy chứng nhận gia hạn
        public String SendMs_30 = "sendMs_30";// Hàm trả thông tin cơ sở khảo nghiệm + yêu cầu nộp phí, mẫu
        public String SendMs_11 = "sendMs_11";// Hàm yêu cầu nộp lệ phí
        public String SendMs_31 = "sendMs_31";// Hàm thu hồi giấy chứng nhận
        public String SendMs_37 = "sendMs_37";// Hàm cho phép thay đổi nội dung lưu hành
    }

    public interface FUNCTION_MESSAGE_ID {

        public String SendMs_01 = "sendMs_01";
        public String SendMs_02 = "sendMs_02";
        public String SendMs_03 = "sendMs_03";
        public String SendMs_04 = "sendMs_04";
        public String SendMs_39 = "sendMs_39";
        public String SendMs_42 = "sendMS_42";
        public String SendMs_43 = "sendMS_43";
    }

    public interface ACTION {

        public interface TYPE {

            public static final Long INSERT = 0l;
            public static final Long UPDATE = 1l;
            public static final Long DELETE = 2l;
            public static final Long APPROVE = 3l;
            public static final Long REJECT = 4l;
            public static final Long VIEW = 5l;
            public static final Long TRANSFER = 6l;
            public static final Long LOCK = 7l;
            public static final Long UNLOCK = 8l;
            public static final Long LOGIN = 9l;
            public static final Long LOGOUT = 10l;
            public static final Long REGISTER = 11l;
            public static final Long LOOKUP = 12l;
        }

        public interface NAME {

            public static final String INSERT = "Thêm mới";
            public static final String UPDATE = "Cập nhật";
            public static final String DELETE = "Xóa";
            public static final String APPROVE = "Phê duyệt";
            public static final String REJECT = "Từ chối";
            public static final String VIEW = "Xem";
            public static final String TRANSFER = "Chuyển";
            public static final String LOCK = "Khóa";
            // hoangnv28
            public static final String DEFAULT = "Gửi xử lý";
            public static final String RETRIEVE = "Thu hồi";
            public static final String SUMPLEMENT = "Gửi bổ sung";
            public static final String PUT_IN_BOOK = "Vào sổ văn bản";
            public static final String GET_OPINION = "Xin ý kiến";
            public static final String GIVE_OPINION = "Cho ý kiến";
            public static final String CREATE_FILE = "Tạo hồ sơ";
            public static final String CREATE_DRAFT = "Tạo dự thảo";
            public static final String RETURN = "Trả lại";
            public static final String TRANSFORM_TO_PROCESS = "Chuyển về xử lý";
            public static final String TRANSFORM_TO_RECEIVE_TO_KNOW = "Nhận để biết";
            public static final String UNLOCK = "Mở khóa";
            public static final String LOGIN = "Đăng nhập";
            public static final String LOGOUT = "Đăng xuất";
            public static final String CAPTCHA_LOG = "Log Captcha";
        }
    }

    public interface NODE_TYPE {

        public static final Long NODE_TYPE_START = 2l;
        public static final Long NODE_TYPE_FINISH = 3l;
        public static final Long NODE_TYPE_WORK = 1l;
    }

    public interface NODE_ASSOCIATE_TYPE {

        public static final Long ALL = 0L;
        public static final Long NORMAL = 1l;
        // Auto-send to next user
        public static final Long TRANSPARENT = 2l;
        // Action to start procedure
        public static final Long START = -1l;
        // Return to previous user
        public static final Long RETURN_PREVIOUS = 3l;
        // Return to author of document (creator of document)
        public static final Long RETURN_AUTHOR = 4l;
        // Alway send to configured users
        public static final Long ALWAYS_SEND = 5l;
    }

    public interface NODE_ACTOR_RELATIVE {

        public static final Long ALL = -1l;
        public static final Long PARENT = -2l;
        public static final Long CURRENT = -3l;
        public static final Long CHILD = -4l;
        public static final Long SAME_PARENT = -6l;
        public static final Long SAME_PARENT_LEVEL = -7l;
    }

    // hoangnv28
    public interface Notification {

        public static final String EXIST_WARNING = "%s đã tồn tại";
        public static final String NOT_EXIST_WARNING = "%s không tồn tại";
        public static final String INT_WARNING = "Bạn phải nhập %s lớn hơn 0";
        public static final String INPUT_WARNING = "Bạn chưa nhập %s";
        public static final String SELECT_WARNING = "Bạn chưa chọn %s";
        public static final String SAVE_SUCCESS = "Lưu %s thành công";
        public static final String SAVE_DEVICE_SUCCESS = "Lưu %s thành công và chờ phê duyệt";
        public static final String SAVE_ERROR = "Lưu %s thất bại";
        public static final String UPDATE_SUCCESS = "Cập nhật %s thành công";
        public static final String UPDATE_ERROR = "Cập nhật $s thất bại";
        public static final String LOCK_CONFIRM = "Bạn có muốn khóa %s";
        public static final String LOCK_SUCCESS = "Khóa %s thành công";
        public static final String LOCK_ERROR = "Khóa %s thất bại";
        public static final String UNLOCK_COFIRM = "Bạn có muốn mở khóa %s";
        public static final String UNLOCK_SUCCESS = "Mở khóa %s thành công";
        public static final String UNLOCK_ERROR = "Mở khóa %s thất bại";
        public static final String DELETE_CONFIRM = "Bạn có chắc chắn muốn xóa %s không?";
        public static final String DELETE_CONFIRM_PRODUCTIMPORT = "Bạn có chắc chắn muốn xóa thiết bị y tế %s không?";
        public static final String DELETE_SUCCESS = "Xóa %s thành công";
        public static final String DELETE_ERROR = "Xóa %s thất bại";
        public static final String ERROR = "error";
        public static final String INFO = "info";
        public static final String WARNING = "warning";
        public static final String SAVE_CONFIRM = "Bạn có chắc muốn lưu %s lại không?";
        public static final String SAVE_COMPLETE_CONFIRM = "%s chưa được thêm vào danh sách, bạn có muốn thêm?";
        public static final String APROVE_CONFIRM = "Bạn có chắc muốn phê duyệt %s không?";
        public static final String APROVE_SUCCESS = "Phê duyệt %s thành công";
        public static final String APROVE_ERROR = "Phê duyệt %s thất bại";
        public static final String REJECT_SUCCESS = "Từ chối %s thành công";
        public static final String REJECT_ERROR = "Từ chối %s thất bại";
        public static final String BILL_CONFIRM = "Hóa đơn đã xác nhận";
        public static final String IMP_CONFIRM = "Bạn có chắc muốn nhập liệu %s không?";
        public static final String IMP_SUCCESS = "Nhập liệu %s thành công";
        public static final String IMP_ERROR = "Nhập liệu %s không thành công";
        public static final String SENDED_SUCCESS = "Đã gửi %s thành công";

        public static final String CONFIRM_GENERAL = "Bạn có chắc chắn muốn thao tác không?";

        //Time show alert
        public static final int TIME_SHOW_ERROR = 4000;
        public static final int TIME_SHOW_SUCCSESS = 2500;
    }

    /**
     * Status 0: delete;1: active
     */
    public interface Status {

        public static final Long ACTIVE = 1l;
        public static final Long INACTIVE = 0l;
        public static final Long DELETE = -1L;
        // hoangnv28
        public static final String ACTIVE_MSG = "Hoạt động";
        public static final String INACTIVE_MSG = "Bị khóa";
        public static final String DELETE_MSG = "Bị xóa";
        // nghiepnc
        public static final String PHEDUYET = "Đã phê duyệt";
        public static final String CHUAPHEDUYET = "Chưa phê duyệt";
        public static final String TUCHOI = "Đã từ chối";
    }

    /**
     * type of subject.major choosed in popup
     */
    public interface ChooserType {

        public static final String SUBJECT = "0";
        public static final String MAJOR = "1";
    }

    public interface Report {

        public static final String TOPIC_TEMPLATE_PATH = "/WEB-INF/template/report/knowledge/DSDeTai.xls";
        public static final String TOPIC_EXPORT_PATH = "/share/templateReport/knowledge/DSDeTai";
        public static final String BOOK_TEMPLATE_PATH = "/WEB-INF/template/report/knowledge/DSSach.xls";
        public static final String BOOK_EXPORT_PATH = "/share/templateReport/knowledge/DSSach";
        public static final String WORKSHOP_TEMPLATE_PATH = "/WEB-INF/template/report/knowledge/DSHoiThao.xls";
        public static final String WORKSHOP_EXPORT_PATH = "/share/templateReport/knowledge/DSHoiThao";
    }

    public interface Type {

        public static final long TOPIC = 0l;
        public static final long BOOK = 1l;
        public static final long WORKSHOP = 2l;
        public static final String LEAD = "Lãnh đạo";
        public static final String CLERICAL = "Văn thư";
        public static final String EXPERT = "Chuyên viên";
        public static final String OTHER = "Khác";
    }

    public interface Task {

        public static final long NEED_PROCESS = 0L;
        public static final long PROCESSING = 1L;
        public static final String PROCESSING_STR = "Đang thực hiện";
        public static final long FINISH = 2L;
        public static final String FINISH_STR = "Hoàn thành";
        public static final long MAX_PERCENT = 100L;
    }

    public interface CATEGORY_TYPE {

        public static final String WEIGHT = "WEIGHT";
        public static final String QUANTITY = "QUANTITY";
        public static final String METHOD_IMDOC = "METHOD_IMDOC";
        public static final Long IMPORT_ORDER_FILE_REGISTERED_PAPER = 125L;
        public static final Long IMPORT_ORDER_FILE_REDUCED_PAPER = 126L;//đồng ý giảm
        public static final Long IMPORT_ORDER_FILE_PAPER = 145L;
        public static final Long IMPORT_ORDER_AMENDMENT_PAPER = 153L;//yêu cầu bổ sung
        public static final Long IMPORT_ORDER_REPORT_PROCESS = 160L;
        public static final Long RAPID_TEST_ATT_TYPE = 165L;
        public static final String DEPARTMENT_TYPE = "DEPARTMENT_TYPE";
        public static final String RESOURCE_TYPE = "RESOURCE_TYPE"; // Do mat
        // cua van
        // ban
        public static final String VOFFICE_CAT_SECRET = "VOFFICE_CAT_SECRET"; // Do
        // mat
        // cua
        // van
        // ban
        public static final String DOCUMENT_TYPE = "VOFFICE_CAT_DOCTYPE"; // Loai
        // van
        // ban
        public static final String DOCUMENT_FIELD = "VOFFICE_CAT_DOCFIELD"; // Linh
        // vuc
        public static final String VOFFICE_CAT_URGENCY = "VOFFICE_CAT_URGENCY"; // Do
        // khan
        public static final String ACCESS_TYPE = "TC"; // Muc do truy cap
        public static final String IMPORTANT_TYPE = "IPT"; // Muc do quan trong
        public static final String SEND_TYPE = "VOPTN"; // Phuong thuc gui
        public static final String RECEIVE_TYPE = "VOFFICE_RECEIVE_TYPE";// Phuong
        // thuc
        // nhan
        public static final String FILE = "TTHC"; // Loai ho so
        public static final String[] toListCatType = {"VOFFICE_CAT_DOCTYPE",
            "VOFFICE_CAT_SECRET", "VOFFICE_CAT_URGENCY",
            "VOFFICE_CAT_DOCFIELD", "DEPARTMENT_TYPE", "RESOURCE_TYPE"};
        public static final String VOFFICE_CAT_BOOK_IN = "VOFFICE_CAT_BOOK_IN";
        public static final String VOFFICE_CAT_PRIORITY = "VOFFICE_CAT_PRIORITY";
        public static final String VOFFICE_CAT_NEXTSTAGE = "VOFFICE_CAT_NEXTSTAGE";
        public static final String VOFFICE_CAT_TYPE = "VOFFICE_CAT_TYPE";
        public static final Long PRIORITY_NORMAL = 791L;
        public static final Long DOCUMENT_RECEIVE = 600L;
        public static final String RAPID_TEST_FILE_TYPE = "RAPID_TEST_FILE_TYPE";
        public static final String COSMETIC_FILE_TYPE = "COSMETIC_FILE_TYPE";

        public static final String FILECAT_IMDOC = "FILECAT_IMDOC";// import
        public static final String FILECAT_IMDOC_KN_NOIBO = "FILECAT_IMDOC_KN_NOIBO";// Linhdx_ Cac file kiem nghiem noi bo khong gui DN
        public static final String FILECAT_IMDOC_KN_GUIDN = "FILECAT_IMDOC_KN_GUIDN";// Linhdx_ Cac file kiem nghiem co gui DN
        // ycnk
        // linhdx thanh toan
        public static final String PROCEDURE = "OBJECT"; // Danh muc thu tuc xet
        // nghiem nhanh
        public static final String SUB_PROCEDURE = "SUB_PROCEDURE";// Thuc tuc
        // con
        public static final String FEE = "FEE"; // Danh muc fee
        public static final String FEE_PAYMENT_TYPE = "FEE_PAYMENT_TYPE";// Hinh
        // thuc
        // thanh
        // toan
        public static final String RAPID_TEST_OBJECT = "HS_RAPIDTEST_KIT";
        public static final String COSMETIC_OBJECT = "HS_COSMETIC_KIT";
        public static final String IMPORT_ORDER_OBJECT = "HS_IMPORT_REQ";
        public static final String IMPORT_ORDER_OBJECT_NEW = "HS_IMPORT_REQ_NEW";
        public static final String IMPORT_DEVICE_OBJECT = "HS_IMPORT_DEVICE_KIT";
        public static final String IMPORT_DEVICE_DIEUCHINH_TEN = "HS_IMPORT_DEVICE_DIEUCHINH_TEN";

        public static final String PROCEDURE_TEMPLATE_TYPE = "PROCEDURE_TEMPLATE_TYPE";// Bieu
        // mau
        // trong
        // thu
        // tuc
        public static final String FILE_STATUS = "FILE_STATUS"; // Trang thai ho
        // so
        public static final String COS_INGREDIENT = "COS_INGREDIENT"; // Thanh
        // phan
        // trong
        // khai
        // baos
        public static final String PAYMENT_STATUS = "PAYMENT_STATUS"; // Trang
        // thai
        // ho so
        public static final String TESTTYPE_IMDOC = "TESTTYPE_IMDOC"; // Chỉ
        // tiêu
        // kiểm
        // tra
        public static final String FILETYPE_IMDOC = "FILETYPE_IMDOC"; // Phiếu
        // kiểm
        // nghiệm
        // sản
        // phẩm

        //Trang thiet bi: Danh sach hoi dong
        public static final String IMPORT_FILE_HOIDONG = "IMPORT_FILE_HOIDONG";

    }

    public interface IMPORT_TYPE {

        public static final String IMPORT_PRODUCT_CATEGORY = "ID_TTB_DMTTB"; // ma 
        public static final String ID_PRODUCT_PL = "ID_PRODUCT_PL";
        public static final String TTB_PRODUCT_ATT = "TTB_PRODUCT_ATT";
        // loai
        // danh
        // muc
        // thiet
        // bi
        // y
        // te
        public static final String IMPORT_FILE_TYPE = "IMPORT_FILE_TYPE";
    }

    public interface DOCUMENT_STATUS {

        public static final Long DRAFT = 0L; // du thao
        public static final Long PUBLISH = 1L; // da ban hanh
        public static final Long INSTRUCTION = 2L; // da xin y kien lanh dao
        public static final Long RETURN = 3L; // Lãnh đạo trả lại
        public static final Long APPROVAL = 4L; // "Đã phê duyệt va gui xu ly tiep";
        public static final Long SEND_COORDINATE = 5L; // "Đã gửi phối hợp";
        public static final Long RECEIVE_COORDINATE = 6L; // "Đã nhận ý kiến phối hợp";
        public static final Long ASSIGN_NEXT = 7L; // Da chuyen tiep
        public static final Long APPROVAL_FORM = 8L; // Da phe duyet ve the thuc
        public static final Long JUST_REFERENCE = 11L; // Cong bo van ban
        public static final Long ASSIGN_NUMBER = 12L;
        // van ban den
        public static final Long NEW = 0L;// moi nhan
        public static final Long PROCESSING = 1L; // dang xu li
        public static final Long PROCESSED = 2L;// da xu li
        public static final Long RETRIEVE_ALL = 3L;// thu hoi tat ca
        public static final Long RETURN_ALL = 4L;// tra lai tat ca
        // Trang thai dang String
        public static final String DRAFT_STR = "Dự thảo";
        public static final String PUBLISH_STR = "Đã ban hành";
        public static final String INSTRUCTION_STR = "Đã gửi lãnh đạo";
        public static final String RETURN_STR = "Lãnh đạo trả lại";
        public static final String APPROVAL_STR = "Đã phê duyệt";
        public static final String SEND_COORDINATE_STR = "Đã trình xử lý";
        public static final String RECEIVE_COORDINATE_STR = "Đã nhận ý kiến phối hợp";
    }

    // hoangnv28
    public interface DOCUMENT_MENU {

        public static final int ALL = 0;// menu tiep nhan van ban,du thao
        public static final int WAITING_PROCESS = 1;// menu cho xu li
        public static final int PROCESSED = 3; // menu da xu li
        public static final int PROCESSING = 2;// menu dang xu li
        public static final int RECEIVE_TO_KNOW = 4;// menu nhan de biet
        public static final int RETRIEVED = 5;// menu da thu hoi
        public static final int MENU_PUBLISHED = 6;// menu da ban hanh(doi voi
        // vb di)
        public static final int WAITING_GIVE_OPINION = 7;// menu cho y kien
        public static final int GAVE_OPINION = 8;// menu da cho y kien
        public static final int REPORT = 9;// Báo cáo in sổ
        public static final int VIEW = 10;// Báo cáo in sổ
    }

    public interface DOCUMENT_STORE {

        public static final Long STORE_NEXT = 1L; // Da chuyen luu tru
        public static final Long STORE_RECORD = 2L; // Da luu tru ho so
        public static final Long STORAGE = 3L; // Da luu tru
    }

    public interface ROLE {

        public static final Long EXPERT = 0L;
        public static final Long CLERICAL = 1L;
        public static final Long LEAD = 2L;
        public static final Long ALL = 3L;
        public static final String LEAD_POSITION = "LD";
        public static final Long VAN_THU = 502L;
        public static final Long VAN_THU_BO = 359L;
        public static final Long VAN_THU_CUC = 800L;
        public static final Long VAN_THU_DON_VI = 1600L;
        public static final Long VAN_THU_PHONG = 1650L;

    }

    public interface MONITOR {

        public static final String VBDi_TK = "VBDi_TK"; // MH TK vbdi
        public static final String VBDi_VS = "VBDi_VS"; // MH vao so VBDi
        public static final String VBDi_CBH = "VBDi_CBH"; // MH VB cho ban hanh
        public static final String VBDi_DVXL = "VBDi_DVXL"; // MH VB don vi cho
        // xy ly
        public static final String VBDi_VBNB = "VBDi_VBNB"; // MH VB den noi bo
        public static final String VBDi_DT = "VBDi_DT"; // MH DS VB du thao
        public static final String VBDi_DTA = "VBDi_DTA"; // MH DS VB du thao da
        // duoc Approve
        public static final String VBDi_TDHB = "VBDi_TDHB"; // MH DS VB theo doi
        // hoi bao
        public static final String VBDi_CXL = "VBDi_CXL"; // MH VB cho LD xu ly
        public static final String VBDi_DXL = "VBDi_DXL"; // MH VB LD da xu ly
        public static final String VBDi_DT_HSCV = "VBDi_DT_HSCV"; // MH DS VB du
        // thao cua
        // HSCV
        public static final String VBDi_BH_HSCV = "VBDi_BH_HSCV"; // MH DS VB da
        // ban hanh
        // cua HSCV
        public static final String VBDi_PD_BH_HSCV = "VBDi_PD_BH_HSCV"; // DS VB
        // chua
        // phe
        // duyet
        // hoac
        // chua
        // ban
        // hanh
    }

    public static class IDF_PROCESS_STATUS {

        public static final Long INITIAL = 0L; // moi khoi tao
        public static final Long SENT_DISPATCH = 11L; // Đã gửi CV SDBS
        public static final Long SENT_RETURN = 31000012L;// MinhNV - LDV yêu cầu SĐBS
        public static final Long CV_SENT_RETURN = 31000037L;//Linhdx- Chuyên viên gửi yêu cầu SDBS
        public static final Long VT_RETURN = 31000003L;
        public static final Long TKHD_RETURN = 31000014L;
        public static final Long DN_PAYMENT = 31000030L;
        public static final Long NEW = 0L; // moi den
        public static final Long BOOKED = 1L; // da luu so don vi
        public static final Long INSTRUCTION = 2L; // da chuyen xin y kien chi dao
        public static final Long ASSIGNED = 3L; // da phan cong cho chuyen vien
        public static final Long FINISH_1 = 4L; // hoan thanh (dong nghia voi ket thuc
        // VB)
        public static final Long FINISH_2 = 14L; // hoan thanh (khi chuyen VB cho don
        // vi)
        public static final Long RETURN = 5L; // tra lai
        public static final Long APPROVED = 6L; // da phe duyet
        public static final Long ASSIGN_NEXT = 7L; // da chuyen tiep
        public static final Long DOING = 8L; // dang xu ly
        public static final Long DID = 9L; // da xu ly
        public static final Long PUBLISHED = 10L; // da ban hanh
        public static final Long PROPOSED = 3L;// Đề xuất
        public static final Long READ = 12L;// Đã đọc
        public static final Long RETRIEVE = 13L;// Thu hoi
        // Array luu trang thai cua process tuong ung voi cac menu: "Cho xu li",
        // "Dang xu li", "Da xu li".
        private static final Long[] waitingProcessStatus = {
            Constants.IDF_PROCESS_STATUS.NEW,
            Constants.IDF_PROCESS_STATUS.BOOKED,
            Constants.IDF_PROCESS_STATUS.READ};
        public static Long[] getWaitingProcessStatus(){
            return waitingProcessStatus;
        }
        private static final Long[] processingStatus = {
            Constants.IDF_PROCESS_STATUS.INSTRUCTION,
            Constants.IDF_PROCESS_STATUS.ASSIGN_NEXT,
            Constants.IDF_PROCESS_STATUS.DID};
        public static Long[] getProcessingStatus(){
            return processingStatus;
        }
        private static final Long[] processedStatus = {
            Constants.IDF_PROCESS_STATUS.FINISH_2,
            Constants.IDF_PROCESS_STATUS.RETURN,
            Constants.IDF_PROCESS_STATUS.FINISH_1};
        public static Long[] getProcessedStatus(){
            return processedStatus;
        }
    };

    public static boolean isWaitingIdfProcess(Long processStatus) {
        return Arrays.asList(Constants.IDF_PROCESS_STATUS.waitingProcessStatus)
                .contains(processStatus);
    }

    public static boolean isIdfProcessing(Long processStatus) {
        return Arrays.asList(Constants.IDF_PROCESS_STATUS.processingStatus)
                .contains(processStatus);
    }

    public static class PROCESS_STATUS {

        public static final Long NO_NEED_PROCESS = -1L; // moi khoi tao
        public static final Long INITIAL = 0L; // moi khoi tao
        public static final Long SENT_DISPATCH = 11L; // Đã gửi CV SDBS
        public static final Long SENT_RETURN = 15L;
        public static final Long VT_RETURN = 23L;
        public static final Long NEW = 0L; // moi den
        public static final Long BOOKED = 1L; // da luu so don vi
        public static final Long INSTRUCTION = 2L; // da chuyen xin y kien chi dao
        public static final Long ASSIGNED = 3L; // da phan cong cho chuyen vien
        public static final Long FINISH_1 = 4L; // hoan thanh (dong nghia voi ket thuc
        // VB)
        public static final Long FINISH_2 = 14L; // hoan thanh (khi chuyen VB cho don
        // vi)
        public static final Long RETURN = 5L; // tra lai
        public static final Long APPROVED = 6L; // da phe duyet
        public static final Long ASSIGN_NEXT = 7L; // da chuyen tiep
        public static final Long DOING = 8L; // dang xu ly
        public static final Long DID = 9L; // da xu ly
        public static final Long PUBLISHED = 10L; // da ban hanh
        public static final Long PROPOSED = 3L;// Đề xuất
        public static final Long READ = 12L;// Đã đọc
        public static final Long RETRIEVE = 13L;// Thu hoi
        public static final Long SENT_SDBS1 = 21000012L;//truong phong ky  sdbx
        public static final Long SENT_SDBS2 = 21000057L;

        public static final Long SENT_RT_SDBS1 = 41000070L;
        
        public static final Long FINISH_RAPID = 41000055L; // Da cap phep
        // Array luu trang thai cua process tuong ung voi cac menu: "Cho xu li",
        // "Dang xu li", "Da xu li".
        private static final Long[] waitingProcessStatus = {
            Constants.PROCESS_STATUS.NEW, Constants.PROCESS_STATUS.BOOKED,
            Constants.PROCESS_STATUS.READ};
        public static Long[] getWaitingProcessStatus(){
            return waitingProcessStatus;
        }
        private static final Long[] processingStatus = {
            Constants.PROCESS_STATUS.INSTRUCTION,
            Constants.PROCESS_STATUS.ASSIGN_NEXT,
            Constants.PROCESS_STATUS.DID};
        public static Long[] getProcessingStatus(){
            return processingStatus;
        }
        
        
        private static final Long[] processedStatus = {
            Constants.PROCESS_STATUS.FINISH_2,
            Constants.PROCESS_STATUS.RETURN,
            Constants.PROCESS_STATUS.FINISH_1};
        public static Long[] getProcessedStatus(){
            return processedStatus;
        }
    };

    public static boolean isWaitingProcess(Long processStatus) {
        return Arrays.asList(Constants.PROCESS_STATUS.waitingProcessStatus)
                .contains(processStatus);
    }

    public static boolean isProcessing(Long processStatus) {
        return Arrays.asList(Constants.PROCESS_STATUS.processingStatus)
                .contains(processStatus);
    }

    public interface PROCESS_TYPE {

        public static Long COOPERATE = 0L; // phoi hop
        public static Long MAIN = 1L; // Xu ly chinh
        public static Long RECEIVE_TO_KNOW = 2L;// Nhan de biet
        public static Long COMMENT = 3L;// Cho y kien
        public static Long APPROVE = 4L;
        public static Long REFERENCE = 5L; // tham khao
    }

    public interface RECEIVE_USER_TYPE {

        public static Long OFFICE_LEADER = 1l; // lanh dao van phong
        public static Long LEADER = 2l; // lanh dao don vi
        public static Long OFFICE_PROCESS = 3l; // don vi xu ly
        public static Long MONITER = 4l; // Phong xu ly(Phong giam sat xu ly)
    }

    public interface OBJECT_TYPE {

        public static Long DOCUMENT_RECEIVE = 1L; // Van ban den
        public static Long DOCUMENT_PUBLISH = 2L; // Van ban di
        public static Long DOCUMENT_REF = 3L; // Van ban tham khao
        public static Long PROFILE_WORK = 4L; // ho so cong viec
        public static Long PROFILE_STORE = 5L; // Ho so luu tru
        public static Long FORM = 6L; // Phieu yeu cau
        public static Long FORM_ATTACH_FILE = 7L;// file bieu mau
        public static Long NOTIFY = 8L;
        public static Long CALENDAR = 9L; // Ho so
        //
        // phuc vu cho xem log
        //
        public static Long LOG_IN = 10L;
        public static Long LOG_OUT = 11L;
        public static Long USER = 12L;
        public static Long DEPT = 13L;
        public static Long CATEGORY = 14L;
        public static Long ROLE = 15L;
        public static Long FLOW = 16L;
        public static Long TASK = 17L;
        public static Long NOTIFY_OBJECT_TYPE = 18L;
        public static Long TECHNICAL_STANDARD_ATTACH = 19L;
        // Xet nghiem nhanh
        public static Long FILES_RAPIDTEST = 20L; // Ho so
        public static Long RAPID_TEST_FILE_TYPE = 21L;
        public static Long RAPID_TEST_HO_SO_GOC = 22L;
        public static Long RAPID_TEST_SDBS_DISPATCH = 23L;
        public static Long RAPID_TEST_PERMIT = 24L;// Giay phep
        public static Long YCNK_FILE = 25L;
        public static Long FILES = 26L; // Ho so
        public static Long TEMPLATE = 27L; // Bieu mau
        public static Long RAPID_TEST_PUBLIC_PROFILE = 35L;
        public static Long RAPID_TEST_PAYMENT_ALREADY = 36L;
        public static Long COSMETIC_FILE_TYPE = 37L;
        public static Long PAYMENT_EXPORT_PDF = 38L;

        public static Long COSMETIC_PERMIT = 39L;// Giay phep
        public static Long COSMETIC_REJECT_DISPATH = 40L;// Cong van tu choi
        public static Long COSMETIC_PUBLIC_PROFILE = 41L;
        public static Long COSMETIC_HO_SO_GOC = 42L;
        public static Long COSMETIC_CA_ATTACHMENT = 43L;
        public static Long CAPTCHA_LOG = 44L;
        public static Long REGISTER = 45L;
        public static Long LOOKUP = 46L;
        public static Long COSMETIC_SDBS_DISPATH = 44L;// Cong van sua doi bo
        // sung
        public static Long IMPORT_DEVICE_SDBS_DISPATH = 44L;
        // Vu: them dinh nghia chuc vu cho truong attach_type trong bảng attach
        // Su dung de hien ra danh sach cong van da ky cua doanh nghiep theo cong van cuoi cung
        public static Long IMPORT_FILE_TYPE = 47L;// xet nghiem nhanh  su dung

        public static Long IMPORT_CA_ATTACHMENT = 48L;
        public static Long IMPORT_ORDER_FILE = 50L;// yc nhap khau

        public static Long BOARD_SECRETARY_FILE_TYPE = 51L; // Bien ban hop cua
        // thu ky hoi dong
        public static Long BOARD_SECRETARY_CA_ATTACHMENT = 52L;
        // public static Long BOARD_SECRETARY_REJECT_FILE_TYPE = 53L; // Bien ban hop tu choi cap phep cua
        // thu ky hoi dong

        public static Long IMPORT_ORDER_FILE_RS = 61L;// yc nhap khau
        public static Long IMPORT_ORDER_FILE_CHECK = 62L;// yc nhap khau
        public static Long IMPORT_ORDER_FILE_TEST = 63L;// yc nhap khau
        public static Long IMPORT_ORDER_FILE_TEMP = 64L;// yc nhap khau
        public static Long IMPORT_ORDER_FILE_REPROCESS = 65L;// yc nhap khau
        public static Long IMPORT_ORDER_FILE_REPROCESS_RS = 66L;// yc nhap khau
        public static Long IMPORT_ORDER_FILE_REPORT = 67L;// yc nhap khau
        public static Long IMPORT_ORDER_FILE_KSKTKN = 68L;// Ky so kiem tra kiem
        // nghiem
        public static Long IMPORT_ORDER_FILE_KSPDHS = 69L;// Ky so phe duyet ho
        // so
        public static Long IMPORT_ORDER_FILE_KSKTG = 70L;// Ky so mat hang kiem
        public static Long IMPORT_FILE_DEVICE_TYPE = 70L;// thiet bi y te attach cat
        public static Long IMPORT_DEVICE_PRODUCT_MODEL = 71L;// thiet bi y te attach cat
        public static Long IMPORT_DEVICE_PRODUCT_ATT = 72L;
        public static Long ID_MEETING_ATT = 73L;
        // tra giam

        public static Long IMPORT_ORDER_FILE_NEW_GUIDN_GIAYTHONGBAOKETQUAKIEMTRA = 75L;// Giay thong bao ket qua kiem tra trong ddanh sach kiem tra kiem nghiem gui DN
        public static Long IMPORT_ORDER_FILE_NEW_GUIDN_KETQUAKIEMNGHIEM = 76L;// Ket qua kiem nghiem trong ddanh sach kiem tra kiem nghiem gui DN
        public static Long IMPORT_ORDER_FILE_NEW_GUIDN_TAILIEUKHAC = 77L;// Tai lieu khac trong ddanh sach kiem tra kiem nghiem gui DN

        //linhdx
        public static Long IMPORT_ORDER_FILE_NEW_NB_BIENBANKIEMTRA = 81L;// Bien ban kiem tra trong danh sach kiem tra kiem nghiem noi bo
        public static Long IMPORT_ORDER_FILE_NEW_NB_KETQUAKIEMTRA = 82L;// Ket qua kiem tra trong danh danh sach kiem tra kiem nghiem noi bo
        public static Long IMPORT_ORDER_FILE_NEW_NB_BIENBANLAYMAU = 83L;// Bien ban lay mau trong ddanh sach kiem tra kiem nghiem noi bo
        public static Long IMPORT_ORDER_FILE_NEW_NB_TAILIEUKHAC = 84L;// Bien ban lay mau trong ddanh sach kiem tra kiem nghiem noi bo

        public static Long IMPORT_DEVICE_FILE_ATTACH_TYPE_CTHD = 200L;// thiet bi y te attach type: chu tich hoi dong
        public static Long IMPORT_DEVICE_FILE_ATTACH_TYPE_LDV = 201L;// thiet bi y te attach type: lanh dao vu
        public static Long IMPORT_DEVICE_FILE_ATTACH_TYPE_LDB = 202L;// thiet bi y te attach type: lanh dao bo
        public static Long IMPORT_DEVICE_FILE_ATTACH_TYPE_VTVU = 203L;// thiet bi y te attach type: van thu vu
        public static Long IMPORT_DEVICE_FILE_ATTACH_TYPE_VTB = 204L;// thiet bi y te attach type: van thu bo
        public static Long IMPORT_DEVICE_FILE_ATTACH_TYPE_LDB_DK = 205L;// thiet bi y te attach type: lanh dao bo da ky
        public static Long IMPORT_DEVICE_FILE_ATTACH_TYPE_LDB_VTDK = 206L;// thiet bi y te attach type: van thu bo da ky

    }

    public interface OBJECT_TYPE_STR {

        public static String DOCUMENT_RECEIVE_STR = "Văn bản đến";
        public static String DOCUMENT_PUBLISH_STR = "Văn bản đi";
        public static String FILES_RAPIDTEST_STR = "Hồ sơ xét nghiệm nhanh"; // Ho
        // so
        public static String RAPID_TEST_FILE_TYPE_STR = "rapidtest";
        public static String RAPID_TEST_PUBLIC_PROFILE_STR = "Profile";
        public static String RAPID_TEST_DOCUMENT_STR = "Document";
        public static String COSMETIC_DOCUMENT_STR = "Document";
        public static String COSMETIC_DOCUMENT_TYPE_CA = "CA";
        public static String TEMPLATE_STR = "Template";
    }

    // ID của chức năng
    public interface OBJECT_ID {

        public static Long TIEP_NHAN_VAN_BAN = 11L;
    }

    public interface RECORDS_TYPE {

        public static Long REGISTER_RECORDS = 4l; // ho so dang ky
        public static Long RECORDS_ADVERTISE = 1l; // So hs qcao
        public static Long RECORDS_PUBLIC = 2l; // So hs cong bo
        public static Long RECORDS_DDK = 3l; // So hs DDK
    }

    public interface POSITION {
        // BTP

        public static String LEAD_CODE = "BTP_LD_P"; // Lanh dao
        public static String LEAD_OFFICE_CODE = "BTP_LD_VP"; // Lanh dao van
        // phong
        public static String LEADER_CODE = "BTP_LD";
        public static String STAFF_CODE = "POS8"; // Nhan vien
        // CUC ATTP
        // public static String LEAD_CODE = "VOFFICE_LD"; // Lanh dao
        // public static String LEAD_OFFICE_CODE = "VOFFICE_LD_VP"; // Lanh dao
        // van phong
        // public static String LEADER_CODE = "VOFFICE_LD";
        public static String LEAD_SMS_CODE = "^BTP_LD(.*)SMS$";
        public static String SMS_CODE = "_SMS";
        public static Long PHO_PHONG = 3L;
        public static String NGUOI_KY = "8, 9";
        public static Long CUC_TRUONG_ATTP = 7500L;
    }

    public interface ROLES {

        public static String LEAD_ROLE = "voffice_cvp"; // Chanh van phong
        public static String LEAD_OFFICE_ROLE = "voffice_ld"; // Lanh dao
        public static String STAFF_ROLE = "voffice_cv"; // Chuyen vien
        public static String CLERICAL_ROLE = "voffice_vt"; // Van thu
        public static String LEAD_MONITOR_ROLE = "voffice_ld_pc";
        public static String CBLT_CQ = "voffice_cblt_cq"; // Can bo luu tru co
        //linhdx role có thể hủy hồ sơ
        public static String ROLE_CAN_DELETE_FILE = "ROLE_CAN_DELETE_FILE"; // Can bo luu tru co

        // quan
        // ATTP HQMC
        public static String LEAD_UNIT = "voffice_lddv"; // lanh dao don vi

        // Vu them quyen thu ky hoi dong
        public static Long CHUCVU_THUKYHOIDDONG = 301L; // chuc vu thu ky hoi dong
        public static Long CHUCVU_THANHVIENHOIDDONG = 300L;
    }

    public interface DEPARTMENT {

        public static Long VIETTEL_ID = 801l;// id cua tap doan VIETTEL
        public static Long BTP_ID = 52l; // id cua bo tu phap
        public static Long DEPT_TYPE_PHONG = 8l; // Phong cua don vi
        public static String DEPT_TYPE_CODE_PHONG = "Văn phòng"; // ma cua phong
        public static Long DEPT_TYPE_CHECK = 951L;
        public static Long CUC_ATTP = 3402L;
        public static Long VAN_PHONG_BO = 3503L;
    }

    public interface CommentType {

        public static Long APPROVED = 1l; // Phe duyet
        public static Long RETURN = 2l; // tra lai
        public static Long COORDINATE = 3l; // phoi hop
        public static Long INSTRUCTION = 4l; // gui xin y kien lanh dao
    }

    public interface NOTIFY_TYPE {

        public static Long BY_RECEIVE_DOC = 1l; // Hồi báo bằng VB đến
        public static Long OTHER = 0l; // Hồi báo bằng dạng khác
        public static Long LEADER_OPINION = 2l;// Ý kiến lãnh đạo
        public static Long RETURN = 3l;// Trả lại
    }

    public interface NOTIFY_STATUS {

        public static Long ON_TIME = 1l; // Đã hồi báo (đúng hạn)
        public static Long OUT_TIME = 0l; // Đã hồi báo (quá thời hạn)
        public static String ON_TIME_STR = "Đã hồi báo (đúng hạn)";
        public static String OUT_TIME_STR = "Đã hồi báo (quá thời hạn)";
        public static Long ACTIVE = 1L; // Dang theo doi hoi bao
        public static Long INACTIVE = 2L; // Ket thuc theo doi hoi bao
    }

    public interface ROLE_STAFF {

        public static final String ROLE_PUBLISH = "ROLE_STAFF_PUBLISH";
        public static final String ROLE_ASSIGN = "ROLE_STAFF_ASSIGN";
    }

    public static class DEPT_TYPE {

        private static List<Long> PHONG_BAN = Arrays.asList(8L, 11L);
        public List<Long> GET_PHONG_BAN (){
            return PHONG_BAN;
        }
    }

    public interface FILE_STATUS {

        public static final Long PROCESSING = 1L;// Mới tao, chua nop
        public static final Long SENDING = 2l; // da hoan thanh
        public static final Long FINISHED = 3l; // da hoan thanh
        public static final Long REJECT = 4l; // da tra lai
        public static final Long APPROVE = 5l; // da phe duyet
    }

    public interface FILE_DESCRIPTION {

        public static final String ANNOUNCEMENT_FILE01 = "announcementFile01";//
        public static final String RE_ANNOUNCEMENT = "reAnnouncement";//
        public static final String RE_CONFIRM_FUNC_IMP = "reConfirmFuncImport";
        public static final String RE_CONFIRM_NORMAL_VN = "reConfirmNormalVN";
        public static final String RE_CONFIRM_FUNC_VN = "reConfirmFuncVN";
        public static final String REC_CONFIRM_NORMAL_IMP = "reConfirmNormalImp";
        public static final String CONFIRM_FUNC_IMP = "confirmFuncImport";
        public static final String CONFIRM_FUNC_VN = "confirmFuncVN";
        public static final String CONFIRM_NORMAL_IMP = "confirmNormalImport";
        public static final String ANNOUNCEMENT_FILE03 = "announcementFile03";
        public static final String ANNOUNCEMENT_4STAR = "announcement4star";
        public static final String CONFIRM_NORMAL_VN = "confirmNormalVN";
        public static final String CONFIRM_SATISFACTORY = "confirmSatisfactory";
    }

    public interface DIALOG_CONTROL {

        public static final String DEPARTMENT = "DEPARTMENT";
        public static final String USERS = "USERS";
        public static final String CATEGORY = "CATEGORY";
    }

    public interface UPLOAD {

        public static final String ATTACH_PATH = "/Share/upload/";
        public static final String AVATAR_PATH = "/Share/avatar/";
    }

    public interface RECORD_MODE {

        public static final String DELETE = "Delete";
        public static final String EDIT = "Edit";
        public static final String VIEW = "View";
        public static final String CREATE = "Create";
        public static final String PUBLISH = "Publish";
        public static final String ASSIGN_NUMBER = "AssignNumber";
        public static final String ASSIGN_AND_PUBLISH = "AssignAndPublish";
        public static final String CREATE_DOCOUT = "CreateDocOut";
        public static final String EDIT_DOCOUT = "EditDocOut";
    }

    public interface BUTTON_TOOLBAR_TYPE {

        public static final Long TRANSFER = 1L; // nut chuyen xu ly thong thuong
        public static final Long RETURN = 2L; // tra lai van ban
        public static final Long FINISH = 3L; // ket thuc van ban
        public static final Long RETRIEVE = 4L; // thu hoi van ban
    }

    public interface TREE_TYPE {

        public static int FULL = 1;
        public static int DEPT_ONLY = 2;
        public static int SPECIAL_DEPT = 3;
    }

    public interface TREE_MODE {

        public static Long SINGLE = 1L;
        public static Long MULTIPLE = 2L;
    }

    //
    // dung object_type trong bang attachs de phan biet: dinh kem cho vb den/vb
    // di/y kien
    // dung attach_type de phan biet loai file dinh kem cho vb di: phieu
    // trinh,du thao,lien quan
    public interface ATTACH_TYPE {

        public static Long ATT_REPORT = 1L; // file dinh kem la phieu trinh
        public static Long ATT_DRAFT = 2L; // file dinh kem la du thao
        public static Long ATT_DOC_RELATION = 3L; // file dinh kem la van ban
        // lien quan
    }

    public interface DOCUMENT_TYPE_NAME {

        public static final String DOCUMENT = "Văn bản";
        public static final String FILE = "Hồ sơ";
        public static final String ATTACH = "File đính kèm";
        public static final String PRODUCT = "Sản phẩm";
        public static final String IMPORT = "Thiết bị";
        public static final String ACCOUNT = "Tài khoản";
        public static final String BILL = "Biên lai";
        public static final String TMP_BILL = "Biên lai";
    }

    public interface TYPE_FILE {

        public static final String DELETE = "1";
        public static final String NOTDELETE = "0";
    }

    public interface USER_TYPE {

        public static final Long ADMIN = 1L;
        public static final Long NOT_ADMIN = 0L;
        public static final Long ENTERPRISE_USER = 2L;
    }

    public interface RAPID_TEST_STATUS {

        public static Long NEW = 0L; // moi den
        public static Long BOOKED = 1L; // da luu so don vi
        public static Long INSTRUCTION = 2L; // da chuyen xin y kien chi dao
        public static Long ASSIGNED = 3L; // da phan cong cho chuyen vien
        public static Long FINISH_1 = 4L; // hoan thanh (dong nghia voi ket thuc
    }

    public interface PROCEDURE_TEMPLATE_TYPE {

        public static Long ADDITIONAL_REQUEST = 0L;
        public static final String ADDITIONAL_REQUEST_STR = "Công văn Sửa đổi bổ sung";
        public static Long PERMIT = 1L;
        public static final String PERMIT_STR = "Giấy phép";
        public static Long PHILEPHI = 2L;
        public static final String PHILEPHI_STR = "Biên lai thu tiền phí, lệ phí";
    }

    public interface PERMIT_TYPE {

        //DYCNK
        public static Long REGISTERED_PAPER = 1L;
        public static Long REDUCED_PAPER = 2L;
        public static Long FILE_PAPER = 3L;
        public static Long FILE_SDBS = 4L;
        public static Long FILE_PDDK = 5L;
    }

    public interface PAYMENT {

        public int FEE_PAYMENT_TYPE_NHIEU_HO_SO = 2;

        public interface PHASE {

            public Long EVALUATION = 0L; // Giai doan tham dinh
            public Long PERMIT = 1L; // Giai doan cap so
        }
    }

    public interface RAPID_TEST {

        public String TYPE_CONFIG = "RAPID_TEST_CONFIG";
        public String CODE_CONFIG = "RAPID_TEST_CONFIG1";
        public Long DOCUMENT_TYPE_CODE_TAOMOI = 16L;
        public Long DOCUMENT_TYPE_CODE_BOSUNG = 17L;
        public Long DOCUMENT_TYPE_CODE_GIAHAN = 18L;
        public String DOCUMENT_TYPE_CODE_TAOMOI_STR = "Hồ sơ đề nghị đăng ký lưu hành bộ xét nghiệm nhanh thực phẩm";
        public String DOCUMENT_TYPE_CODE_BOSUNG_STR = "Hồ sơ đề nghị thay đổi nội dung đã đăng ký thông tin lưu hành bộ xét nghiệm nhanh thực phẩm";
        public String DOCUMENT_TYPE_CODE_GIAHAN_STR = "Hồ sơ đề nghị gia hạn đăng ký lưu hành bộ xét nghiệm nhanh thực phẩm";
        public Long DINH_TINH = 1L;
        public Long BAN_DINH_TINH = 2L;
        public Long DINH_LUONG = 3L;
        public String DINH_TINH_STR = "Định tính";
        public String BAN_DINH_TINH_STR = "Bán định lượng";
        public String DINH_LUONG_STR = "Định lượng";
        public String KY_DONG_DAU_LANH_DAO_CUC = "<SIB>";

        public interface EVAL_TYPE {

            public final Long CV_GUI_HEN_TRAKQ = 1L;
        }

        public interface NHOM_BIEU_MAU {

            public String HIEN_THI_CHO_DOANH_NGHIEP_VALUES = "'2521','2512','2516','2522','2523','165','2537','2539','2551','2553'";
            public String QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_KEY = "QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_KEY";
            public String QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_VALUES = "'2508','2517'";
            public String QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_KEY = "QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_KEY";
            public String QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_VALUES = "'2509','2518'";
            public String CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_KEY = "CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_KEY";
            public String CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_VALUES = "'2511','2519'";
            public String CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY = "CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY";
            public String CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES = "'2512','2520'";
            public String QUYET_DINH_THANH_LAP_HOI_DONG_KEY = "QUYET_DINH_THANH_LAP_HOI_DONG_KEY";
            public String QUYET_DINH_THANH_LAP_HOI_DONG_VALUES = "'2508'";
            public Long QUYET_DINH_THANH_LAP_HOI_DONG = 2508L;
            public String QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_KEY = "QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_KEY";
            public String QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_VALUES = "'2509'";
            public Long QUYET_DINH_THANH_LAP_HOI_DONG_DAKY = 2509L;
            public String QUYET_DINH_THANH_LAP_HOI_DONG_DAKYVADONGDAU_KEY = "QUYET_DINH_THANH_LAP_HOI_DONG_DAKYVADONGDAU_KEY";
            public String QUYET_DINH_THANH_LAP_HOI_DONG_DAKYVADONGDAU_VALUES = "'2510'";
            public Long QUYET_DINH_THANH_LAP_HOI_DONG_DAKYVADONGDAU = 2510L;
            public String CONG_VAN_KHONG_PHAI_XNN_KEY = "CONG_VAN_KHONG_PHAI_XNN_KEY";
            public String CONG_VAN_KHONG_PHAI_XNN_VALUES = "'2511'";
            public Long CONG_VAN_KHONG_PHAI_XNN = 2511L;
            public String CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY = "CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY";
            public String CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES = "'2512'";
            public Long CONG_VAN_KHONG_PHAI_XNN_DAKY = 2512L;
            public String GIAY_MOI_LAP_HOI_DONG_KEY = "GIAY_MOI_LAP_HOI_DONG_KEY";
            public String GIAY_MOI_LAP_HOI_DONG_VALUES = "'2517'";
            public Long GIAY_MOI_LAP_HOI_DONG = 2517L;
            public String GIAY_MOI_LAP_HOI_DONG_DAKY_KEY = "GIAY_MOI_LAP_HOI_DONG_DAKY_KEY";
            public String GIAY_MOI_LAP_HOI_DONG_DAKY_VALUES = "'2518'";
            public Long GIAY_MOI_LAP_HOI_DONG_DAKY = 2518L;
            public String GIAY_CHUNG_NHAN_LUU_HANH_KEY = "GIAY_CHUNG_NHAN_LUU_HANH_KEY";
            public String GIAY_CHUNG_NHAN_LUU_HANH_VALUES = "'2515'";
            public Long GIAY_CHUNG_NHAN_LUU_HANH = 2515L;
            public String GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY = "GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY";
            public String GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES = "'2516'";
            public Long GIAY_CHUNG_NHAN_LUU_HANH_DAKY = 2516L;
            public String BIEN_BAN_HOP_HOI_DONG_KEY = "BIEN_BAN_HOP_HOI_DONG_KEY";
            public String BIEN_BAN_HOP_HOI_DONG_VALUES = "'2519'";
            public Long BIEN_BAN_HOP_HOI_DONG = 2519L;
            public String BIEN_BAN_HOP_HOI_DONG_DAKY_KEY = "BIEN_BAN_HOP_HOI_DONG_DAKY_KEY";
            public String BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES = "'2520'";
            public Long BIEN_BAN_HOP_HOI_DONG_DAKY = 2520L;
            public Long GIAY_HEN_TRA_KQ = 164L;
            public Long CONG_VAN_SDBS = 165L;
            public Long CONG_VAN_CHOPHEP_THAYDOI = 166L;
            public Long GIAY_CN_GIAHAN = 167L;
            public Long CONG_VAN_THUHOI_CN = 168L;
            public Long CONG_VAN_YC_NOP_PHI_THAM_DINH = 2521L;
            public Long CONG_VAN_YC_NOP_PHI_THAM_DINH_MAU = 2522L;
            public Long CONG_VAN_YC_NOP_PHI_CAP_SO = 2523L;
            public Long FILE_KET_QUA_KHAO_NGHIEM = 5707L;

            public String THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_KEY = "THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_KEY";
            public String THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_VALUES = "'2530','2532'";
            public String THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_KEY = "THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_KEY";
            public String THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VA_GIAY_MOI_DAKY_VALUES = "'2531','2533'";
            public String THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_KEY = "THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_KEY";
            public String THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_VALUES = "'2534','2536'";
            public String THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY = "THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY";
            public String THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VA_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES = "'2535','2537'";
            public String THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_KEY = "THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_KEY";
            public String THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_VALUES = "'2530'";
            public Long THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG = 2530L;
            public String THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_KEY = "THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_KEY";
            public String THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY_VALUES = "'2531'";
            public Long THAYDOI_QUYET_DINH_THANH_LAP_HOI_DONG_DAKY = 2531L;
            public String THAYDOI_CONG_VAN_KHONG_PHAI_XNN_KEY = "THAYDOI_CONG_VAN_KHONG_PHAI_XNN_KEY";
            public String THAYDOI_CONG_VAN_KHONG_PHAI_XNN_VALUES = "'2536'";
            public Long THAYDOI_CONG_VAN_KHONG_PHAI_XNN = 2536L;
            public String THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY = "THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY";
            public String THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES = "'2537'";
            public Long THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY = 2537L;
            public String THAYDOI_GIAY_MOI_LAP_HOI_DONG_KEY = "THAYDOI_GIAY_MOI_LAP_HOI_DONG_KEY";
            public String THAYDOI_GIAY_MOI_LAP_HOI_DONG_VALUES = "'2532'";
            public Long THAYDOI_GIAY_MOI_LAP_HOI_DONG = 2532L;
            public String THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY_KEY = "THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY_KEY";
            public String THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY_VALUES = "'2533'";
            public Long THAYDOI_GIAY_MOI_LAP_HOI_DONG_DAKY = 2533L;
            public String THAYDOI_BIEN_BAN_HOP_HOI_DONG_KEY = "THAYDOI_BIEN_BAN_HOP_HOI_DONG_KEY";
            public String THAYDOI_BIEN_BAN_HOP_HOI_DONG_VALUES = "'2534'";
            public Long THAYDOI_BIEN_BAN_HOP_HOI_DONG = 2534L;
            public String THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY = "THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY_KEY";
            public String THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY_VALUES = "'2535'";
            public Long THAYDOI_BIEN_BAN_HOP_HOI_DONG_DAKY = 2535L;
            public String THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_KEY = "THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_KEY";
            public String THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_VALUES = "'2538'";
            public Long THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH = 2538L;
            public String THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY = "THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY";
            public String THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES = "'2539'";
            public Long THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY = 2539L;

            public String GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_KEY = "GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_KEY";
            public String GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_VALUES = "'2550'";
            public Long GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH = 2550L;
            public String GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY = "GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_KEY";
            public String GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY_VALUES = "'2551'";
            public Long GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY = 2551L;
            public String GIAHAN_CONG_VAN_KHONG_PHAI_XNN_KEY = "GIAHAN_CONG_VAN_KHONG_PHAI_XNN_KEY";
            public String GIAHAN_CONG_VAN_KHONG_PHAI_XNN_VALUES = "'2552'";
            public Long GIAHAN_CONG_VAN_KHONG_PHAI_XNN = 2552L;
            public String GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY = "GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY_KEY";
            public String GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY_VALUES = "'2553'";
            public Long GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY = 2553L;
        }

        public interface EVALUATION {

            public interface EVAL_TYPE {

                public Long BASIC_ASSAY = 1L; // Danh dau buoc chi dinh kiem nghiem
                public Long EVALUATION = 2L; // Danh dau buoc tham set
                public Long HENTRAKQ = 7L;
                public Long EVALUATION_VICEDEPT = 11L; // Danh dau buoc tham set
                public Long EVALUATION_COUNCIL = 15L; // Danh dau buoc thanh lap hoi dong
                public Long EVALUATION_COUNCIL_MEETING_DATE = 18L; // Danh dau ngay hop hoi dong
                public Long SDBS = 5L;
                public Long THAMDINH_DAT = 6L;
            }
        }

        public interface PAYMENT {

            public Long PAY_NEW = 1L; // Moi tao
            public Long PAY_ALREADY = 2L;// Da thanh toan
            public Long PAY_CONFIRMED = 3L;// Da xac nhan
            public Long PAY_REJECTED = 4L;// Da xac nhan tu choi
            public String PAY_NEW_STR = "Chưa thanh toán"; // Moi tao
            public String PAY_ALREADY_STR = "Đã thanh toán";// Da thanh toan
            public String PAY_CONFIRMED_STR = "Đã xác nhận";// Da xac nhan
            public String PAY_REJECTED_STR = "Đã từ chối";// Da xac nhan
            public Long BILL_NEW = 1L; // Moi tao
            public Long BILL_CONFIRMED = 2L;// Da xac nhan
            public Long BILL_REJECTED = 3L;// Da xac nhan tu choi
            public String BILL_NEW_STR = "Mới tạo"; // Moi tao
            public String BILL_CONFIRMED_STR = "Đã xác nhận";// Da xac nhan
            public String BILL_REJECTED_STR = "Từ chối";// Da xac nhan
            public Long FEE_PAYMENT_TYPE_CODE_KEYPAY = 1L;
            public Long FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN = 2L;
            public Long FEE_PAYMENT_TYPE_CODE_TIENMAT = 3L;
            public String FEE_PAYMENT_TYPE_CODE_KEYPAY_STR = "Keypay";
            public String FEE_PAYMENT_TYPE_CODE_CHUYENKHOAN_STR = "Chuyển khoản";
            public String FEE_PAYMENT_TYPE_CODE_TIENMAT_STR = "Tiền mặt";
            public int FEE_PAYMENT_TYPE_MOT_HO_SO = 1;
            public int FEE_PAYMENT_TYPE_NHIEU_HO_SO = 2;

            public interface PHASE {

                public Long EVALUATION = 0L; // Giai doan tham dinh
                public Long PERMIT = 1L; // Giai doan cap so
            }
        }

        public interface ADDITIONAL_REQUEST {

            public interface TYPE_EXPORT {

                public int EX_TEMP = 1; // Tao file chua ky
                public int EX_SIGN = 2;// Tao file da ky
            }
        }

        public static final Long CATEGORY_SYSTEM = 1l;
        public static final Long CATEGORY_NORMAL = 0l;
    }

    public interface PLACE {

        public static String NATION = "nation";
        public static String PROVINCE = "province";
        public static String DISTRICT = "district";
        public static String VILLAGE = "village";
        public static String VNCODE = "VN";
    }

    public interface MENU_TYPE {

        public static String CREATE_STR = "create";
        public static String WAITPROCESS_STR = "waitprocess";
        public static String PROCESSING_STR = "processing";
        public static String PROCESSED_STR = "processed";
        public static String DEPT_PROCESS_STR = "deptprocess";
        public static String DELETE_PERMIT = "deletepermit";
        public static String DEPT_PRODUCT_PROCESS_STR = "deptproductprocess";

    }

    public interface COSMETIC {

        public Long DOCUMENT_TYPE_CODE_TAOMOI = 16L;
        public Long DOCUMENT_TYPE_CODE_BOSUNG = 17L;
        public Long DOCUMENT_TYPE_CODE_GIAHAN = 18L;
        public String DOCUMENT_TYPE_CODE_TAOMOI_STR = "Hồ sơ đề nghị đăng ký lưu hành mỹ phẩm";
        public String DOCUMENT_TYPE_CODE_BOSUNG_STR = "Hồ sơ đề nghị gia hạn đăng ký lưu hành mỹ phẩm";
        public String DOCUMENT_TYPE_CODE_GIAHAN_STR = "Hồ sơ đề nghị gia hạn đăng ký lưu hành mỹ phẩm";
    }

    public interface IMPORT {

        public Long DOCUMENT_TYPE_CODE_TAOMOI = 19L;
        public Long TYPE_CREATE_MOI = 1L;
        public Long TYPE_CREATE_DIEUCHINH = 2L;
        public Long TYPE_CREATE_GIAHAN = 3L;
        
        public Long IS_CHANGE_TENTTB = 0L;
        public Long IS_CHANGE_HANGNUOC = 1L;
        
//        public Long DOCUMENT_TYPE_CODE_DIEUCHINH_TENTTB = 18L;
//        public Long DOCUMENT_TYPE_CODE_DIEUCHINH_HANGNUOC = 17L;
//        public Long DOCUMENT_TYPE_CODE_GIAHAN = 16L;
        public String DOCUMENT_TYPE_CODE_TAOMOI_STR = "Cấp mới";
        public String DOCUMENT_TYPE_CODE_DIEUCHINH_STR = "Điều chỉnh";
        public String DOCUMENT_TYPE_CODE_DIEUCHINH_HANGNUOC_STR = "Điều chỉnh hãng/nước sản xuất";
        public String DOCUMENT_TYPE_DIEUCHINH_TENTTB_STR = "Điều chỉnh tên trang thiết bị";
        public String DOCUMENT_TYPE_GIAHAN_STR = "Gia hạn";
    }

    public interface IMPORT_ORDER {

        public Long DOCUMENT_TYPE_ORDERCODE_TAOMOI = 30L;
        public Long DOCUMENT_TYPE_CODE_TAOMOI = 30L;

    }

    public interface IMPORT_ORDER_TYPE {

        public static final String ORDER_OBJECT = "HS_IMPORT_REQ";
    }

    // public Long DOCUMENT_TYPE_CODE_TAOMOI = 30L;
    // public Long DOCUMENT_TYPE_CODE_BOSUNG = 17L;
    // public Long DOCUMENT_TYPE_CODE_GIAHAN = 18L;
    // public String DOCUMENT_TYPE_CODE_TAOMOI_STR =
    // "Hồ sơ đề nghị đăng ký lưu hành mỹ phẩm";
    // public String DOCUMENT_TYPE_CODE_BOSUNG_STR =
    // "Hồ sơ đề nghị gia hạn đăng ký lưu hành mỹ phẩm";
    // public String DOCUMENT_TYPE_CODE_GIAHAN_STR =
    // "Hồ sơ đề nghị gia hạn đăng ký lưu hành mỹ phẩm";
    // }
    public interface PERMIT_STATUS {

        public Long SIGNED = 1L;// Da ky
        public Long PROVIDED_NUMBER = 2L;
    }

    public interface EVALUTION {

        public Long FILE_OK = 1L; // Dong y
        public Long FILE_NOK = 0L;
        public Long FILE_NEED_ADD = 2L;// yeu cau bo sung

        public interface LEGAL {

            public Long OK = 1L; // Dong y
            public Long NOK = 0L;
            public Long NEED_ADD = 2L;// yeu cau bo sung
        }

        public interface EFFECTIVE {

            public Long NO_LIMIT = 1L; // Khong gioo han
            public Long YEAR_3 = 2L;
            public Long YEAR_5 = 3L;// yeu cau bo sung
        }

        public interface USER_EVALUATION_TYPE {

            public Long STAFF = 1L; // Nhan vien
            public Long PP = 2L;// Phó phòng
            public Long TP = 3L;// Trưởng phòng
            public Long CP = 4L;
            public Long CT = 5L;// Cục trưởng
            public Long VT = 6L;// Van thu
        }

        public interface EVALUATION_LEADER_APPROVE {

            public Long APPROVE_FILE = 1L;
            public Long APPROVE_DISPATCH = 2L;
        }

        public interface DISPATCH_STATUS {

            public Long SIGNED = 1L;// Da ky
            public Long PROVIDED_NUMBER = 2L;
        }

        public interface BOOK_TYPE {

            public String BOOK_IN = "1";// Da ky
            public String BOOK_REJECT = "2";// Cong van tu choi
            public String BOOK_PERMIT = "3";// Dong y cap giay phep
            public String BOOK_ADD_REQUEST = "4";// Yeu cau bo sung
            public String BOOK_MEETINGS = "5";//MinhNV Sổ số biên bản họp TTBYT
            public String BOOK_REGISTER_XNK = "1";// Cong van tu choi
            public String BOOK_PERMIT_XNK = "3";// Dong y cap giay phep
            public String BOOK_ADD_REQUEST_XNK = "2";// Yeu cau bo sung

            public interface RAPID_TEST {

                public String GIAY_CHUNG_NHAN_XNN = "1";
                public String GIAY_MOI_HOP_HOI_DONG = "2";
                public String QUYET_DINH_LAP_HOI_DONG = "3";
                public String KHONG_PHAI_BO_XNN = "4";
                public String GIAY_YEU_CAU_SUA_DOI_BO_SUNG = "5";
                public String GIAY_YEU_CAU_THANH_TOAN = "6";
            }
        }
    }

    public interface CHECK_VIEW {

        public int VIEW = 1;
        public int NOT_VIEW = 0;
        public int NOT_ADMIN = -1;
    }

    public interface HOLIDAY {

        public Long ACTIVE = 1l;
        public Long INACTIVE = 0l;
        public String NOTI_NOT_DEL = "Ngày chọn xóa phải lớn hơn ngày hiện tại";
        public String NOTI_NOT_EDIT = "Ngày chọn sửa phải lớn hơn ngày hiện tại";
        public String COMBOBOX_HEADER_SELECT = "---Chọn---";
        public String NOTI_EMPTY_HOLIDAY_NAME = "Tên sự kiện không thể để trống";
        public String NOTI_EMPTY_HOLIDAY_DATE = "Ngày sự kiện không thể để trống";
        public String NOTI_HOLIDAY = "Ngày nghỉ không thể là Thứ bảy hoặc Chủ nhật";
        public String NOTI_WORK_HOLIDAY = "Ngày đi làm bù phải là Thứ bảy hoặc chủ nhật";
        public String NOTI_DATA_EXISTS = "Trùng dữ liệu đã tạo trên hệ thống";
        public String NOTI_SAVE_FINISH = "Lưu thành công";
    }

    public interface WARNING {

        public Long DEADLINE_ON = 1L;// Dung han
        public Long DEADLINE_MISS = 2L;// Qua han
        public Long DEADLINE_RECENT = 3L;// Gan den han

        public Long NUM_DAY_PROCESS = 3L;// 3 ngay xu ly

    }

    public static class IMPORT_EXCEL {

        public static final int PRODUCT_TYPE_ROW_INDEX = 22;
        public static final int PRODUCT_PRESENTATION_ROW_INDEX = 7;
        public static final int  PRODUCT_TYPE_COL_INDEX = 14;
        public static final int  PRODUCT_PRESENTATION_INDEX = 15;
        private static final String[] productPresenttationList = {";3", ";4", ";1", ";2", ";-1"};
        public static String[] getProductPresenttationList(){
            return productPresenttationList;
        }
        

        public static final int RAPID_TEST_TARGET_TESTING_ROW_INDEX = 22;
        public static final int RAPID_TEST_TARGET_TESTING_NAME_COL = 1;
        public static final int RAPID_TEST_TARGET_TESTING_RANGE_COL = 2;
        public static final int RAPID_TEST_TARGET_TESTING_LIMIT_COL = 3;
        public static final int RAPID_TEST_TARGET_TESTING_PRECISION_COL = 4;

    }

    public interface COUNT_HOME_TYPE {

        public static String TOTAL = "TOTAL";
        public static String WAIT_PROCESS = "WAIT_PROCESS";
        public static String PROCESS = "PROCESS";
        public static String FINISH = "FINISH";
    }

    public interface FILE_STATUS_CODE {

        // Mới tạo
        public static long STATUS_MOITAO = 0L;
        // Đã thẩm định
        public static long STATUS_CV_DATHAMDINH = 1L;
        // Đã xem xét
        public static long STATUS_TP_DAXEMXET = 2L;
        // Đã nộp
        public static long STATUS_DN_DANOP = 3L;
        // Đã trả kết quả
        public static long FINISH = 4L;
        // Đã tiếp nhận
        public static long STATUS_VT_DATIEPNHAN = 5L;
        // Đã phân công
        public static long STATUS_TP_DAPHANCONG = 6L;
        // Đã thanh toán
        public static long STATUS_DN_DATHANHTOAN = 7L;
        // Đã ký công văn SĐBS
        public static long STATUS_LD_DAKYCONGVANSDBS = 8L;
        // Đã thẩm xét
        public static long STATUS_TP_DATHAMXET = 9L;
        // Đã phê duyệt hồ sơ, yêu cầu nộp lệ phí
        public static long STATUS_LD_DAPHEDUYET = 10L;
        // Đã thông báo yêu cầu SĐBS
        public static long STATUS_DATHONGBAOSDBS = 11L;
        // Đã xác nhận thanh toán
        public static long STATUS_KT_DAXACNHANTHANHTOAN = 12L;
        // Đã sửa đổi bổ sung
        public static long STATUS_DN_DASDBS = 14L;
        // Trả yêu cầu kiểm tra lại
        public static long STATUS_TRAYEUCAUKIEMTRALAI = 15L;
        // Đã gửi phiếu báo thu
        public static long STATUS_VT_DAGUIPHIEUBAOTHU = 16L;
        // Chờ phân công
        public static long STATUS_CHOPHANCONG = 17L;
        // Chuyên viên thẩm định không đạt
        public static long STATUS_CV_THAMDINHKHONGDAT = 18L;
        // Chuyên viên thẩm định yêu cầu bổ sung
        public static long STATUS_CV_THAMDINHYCSDBS = 19L;
        // Từ chối phê duyệt
        public static long STATUS_LD_TUCHOIDUYET = 20L;
        // Đã từ chối xác nhận phí
        public static long STATUS_KT_TUCHOIXACNHANPHI = 21L;
        // Chuyên viên thẩm định đat
        public static long STATUS_CV_THAMDINHDAT = 22L;
        // Thành viên hội đồng duyệt hồ sơ
        public static long STATUS_TVHD_DUYET = 23L;
        // Thành viên hội đồng duyệt hồ sơ yêu cầu bổ sung
        public static long STATUS_TVHD_SDBS = 24L;
        // Thành viên hội đồng duyệt hồ sơ từ chối
        public static long STATUS_TVHD_TUCHOI = 25L;
        // Thư ký hội đồng duyệt hồ sơ yêu cầu bổ sung
        public static long STATUS_HHD = 26L;

        //Thực phẩm xuất nhập khẩu       
        // Đã nộp hồ sơ
        public static long STATUS_DN_DANOP_XNK = 21000001L;
        // IO_8_1_Văn thư đóng dấu số trả kết quả giảm
        public static long STATUS_VAN_THU_DONG_DAU_GIAM_XNK = 21000019L;
        // IO_23_1_Văn thư đóng dấu số trả kết quả chặt
        public static long STATUS_VAN_THU_DONG_DAU_CHAT_XNK = 21000047L;
        // IO_27_6_LDP gửi LDC phê duyệt kiến nghị lô hàng không đạt
        public static long STATUS_LDC_PHE_DUYET_XU_LY_LO_HANG_KHONG_DAT_XNK = 21000063L;

        //Trang thiết bị
        // Đã nộp hồ sơ
        public static long STATUS_DN_DANOP_TTB = 31000001L;
        // ID_19_1_Văn thư Bộ ký phát hành công văn
        public static long STATUS_VAN_THU_DONG_DAU_TTB = 31000033L;
        // ID_14_1_Văn thu vụ phê duyệt từ chối cấp phép
        public static long STATUS_VAN_THU_DONG_DAU_TU_CHOI_TTB = 31000027L;

        //Xét nghiệm nhanh
        // Đã nộp hồ sơ
        public static long STATUS_DN_DANOP_XNN = 41000001L;
        // RT_25_1_Văn thư đóng dấu giấy chứng nhận đăng ký lưu hành
        public static long STATUS_VAN_THU_DONG_DAU_XNN = 41000055L;
        // RT_25_2_Văn thư đống dấu công văn không phải bộ XNN
        public static long STATUS_VAN_THU_DONG_DAU_TU_CHOI_XNN = 41000067L;
    }

    // linhdx
    // tim vi tri de chen anh chu ky
    public interface REPLATE_CHARACTER_WHEN_FIND_LOCATION_TO_SIGN {

        // Doanh nghiep
        public static String LOCATION_BUSINESS = "<DN>";// Vi tri ky cua doanh
        // nghiep
        public static String LOCATION_LEADER = "<SI>";// Vi tri ky cua doanh
        // nghiep
    }

    // Vunt: xet gia tri cho evaluation type
    public interface TYPE_PROFESSIONAL {

        // Doanh nghiep
        public static Long TYPE_PROFESSIONAL_MAIN = 1L;// chuyen vien chinh
        public static Long TYPE_PROFESSIONAL_CROSS = 2L;// chuyen vien kiem tra
        // cheo
    }

    // Vunt: xet quyen cho evaluation type
    public interface EVAL_TYPE {

        // Doanh nghiep
        public static Long ROLE_CVC = 50L;// chuyen vien chinh
        public static Long ROLE_CVKTC = 51L;// chuyen vien kiem tra cheo
        public static Long ROLE_TVHD = 52L; // thanh vien hoi dong
        public static Long CHECK_DEPT = 10L; // ycnk co quan kiem tra
    }

    //linhdx 
    public interface DEFAULT_RECEIVE_USER_TYPE_ON_EVAL {

        // Nghiep vu xet nghiem nhanh
        public static String DEFAULT_RECEIVE_USER_TYPE_ON_EVAL_STR = "defaultReceiveUserTypeOnEval"; // pram
        public static Long IO_CQKT_DEFAULT_RECEIVE_USER_TYPE_ON_EVAL = 10L;// param value
        public static String IO_CQKT_DEFAULT_POSITION_RECEIVE_USER_CODE = "LD_CQKT";// Chuc vu mac dinh cua user trong co quan kiem tra de tiep nhan ho so
        public static Long RT_VICE_LEADER_DEFAULT_RECEIVE_USER_TYPE_ON_EVAL = 11L;// Pho phong
    }

    //hieptq 19022016
    public interface CQKTNC_CODE {

        // dat yeu cau nhap khau
        public static Long Vien_dinh_duong_viet_nam = 1l;
        public static Long Vien_kiem_nghiem_atvstp_quoc_gia = 2l;
        public static Long Trung_tam_ky_thuat_tieu_chuan_do_luong_chat_luong_khu_vuc_1 = 3l;
        public static Long Trung_tam_ky_thuat_tieu_chuan_do_luong_chat_luong_khu_vuc_2 = 7l;
        public static Long Trung_tam_ky_thuat_tieu_chuan_do_luong_chat_luong_khu_vuc_3 = 13l;
        public static Long Trung_tam_y_te_du_phong_quang_tri = 5l;
        public static Long Trung_tam_y_te_du_phong_hai_phong = 4l;
        public static Long Trung_tam_y_te_du_phong_da_nang = 6l;
        public static Long Vien_pasteur_nha_trang = 8l;
        public static Long Vien_ve_sinh_dich_te_tay_nguyen = 9l;
        public static Long Vien_ve_sinh_y_te_cong_cong_hcm = 10l;
        public static Long Chi_nhanh_cong_ty_co_phan_giam_dinh_vinacontrol = 11l;
        public static Long Trung_tam_y_te_du_phong_tay_ninh = 12l;
    }

    public interface CQKTNC_ID {

        // dat yeu cau nhap khau
        public static Long Vien_dinh_duong_viet_nam = 3600l;
        public static Long Vien_kiem_nghiem_atvstp_quoc_gia = 3601l;
        public static Long Trung_tam_ky_thuat_tieu_chuan_do_luong_chat_luong_khu_vuc_1 = 3558l;
        public static Long Trung_tam_ky_thuat_tieu_chuan_do_luong_chat_luong_khu_vuc_2 = 3552l;
        public static Long Trung_tam_ky_thuat_tieu_chuan_do_luong_chat_luong_khu_vuc_3 = 3553l;
        public static Long Trung_tam_y_te_du_phong_quang_tri = 3555l;
        public static Long Trung_tam_y_te_du_phong_hai_phong = 3554l;
        public static Long Trung_tam_y_te_du_phong_da_nang = 3556l;
        public static Long Vien_pasteur_nha_trang = 3551l;
        public static Long Vien_ve_sinh_dich_te_tay_nguyen = 3602l;
        public static Long Vien_ve_sinh_y_te_cong_cong_hcm = 3603l;
        public static Long Chi_nhanh_cong_ty_co_phan_giam_dinh_vinacontrol = 3550l;
        public static Long Trung_tam_y_te_du_phong_tay_ninh = 3557l;
    }

    public interface ExportData {

        // dat yeu cau nhap khau
        public static Long statusLD_CQKT_phancong = 51000004L;
        public static Long statusDN_GuiCQKT = 51000001L;
        
        
        public static Long status_Pheduyetdondk = 21000014L;
        public static Long status_Pheduyetdondk_moi = 21000093L;
        
        
        public static Long status_Banhanhhosogiam = 21000019L;
        public static Long status_Banhanhhosothuong = 21000047L;
        public static Long status_Banhanhhosogiam_moi = 21000088L;
        public static Long status_Banhanhhosothuong_moi = 21000081L;
        
        
        public static Long status_Xacnhanphigiam_moi = 21000087L;
        public static Long status_Xacnhanphithuong_moi = 51000028L;

    }

    public interface TtbStatus {

        //Trang thai TTB de thong ke
        public static Long statusAccept_Vanthubanhanh = 31000063L;
        public static Long statusReject_Vanthubanhanhtuchoi = 31000033L;
        public static Long statusReject_ThukyguiCVngoaidanhmuc = 31000047L;

    }
    
     public interface DYCNK_HS_KHONGDAT {
         public static String COMMENTS_TYPE_DN_DX = "1";// Doanh nghiệp đề xuất cách xử lý với mặt hàng không đạt
         public static String COMMENTS_TYPE_CV_DY = "2";// Chuyên viên đồng ý với đề xuất
         public static String COMMENTS_TYPE_CV_YCBS = "3";// Chuyên viên yêu cầu bổ sung
         public static String COMMENTS_TYPE_DN_BCKQ = "4";// Doanh nghiệp gửi báo cáo kết quả
         public static String COMMENTS_TYPE_CV_XNKQ = "5";// Chuyên viên xác nhận kết quả
         
     }

}
