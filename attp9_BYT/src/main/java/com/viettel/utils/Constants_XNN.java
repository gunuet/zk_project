/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.utils;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ngoctm3@viettel.com.vn
 * @since since_text
 * @version 1.0
 */
public final class Constants_XNN {
  
 public interface PLACE{
        public static String NATION = "nation"; 
        public static String PROVINCE = "province"; 
        public static String DISTRICT = "district"; 
        public static String VILLAGE = "village"; 
        public static String VNCODE = "VN"; 
    }
   public interface Status {
        public static final Long ACTIVE = 1l;
        public static final Long INACTIVE = 0l;
        public static final Long DELETE = -1L;
        //nghiepnc
        public static final String PHEDUYET = "Đã phê duyệt";
        public static final String CHUAPHEDUYET = "Chưa phê duyệt";
        public static final String TUCHOI = "Đã từ chối";
    }
    public interface Notification {

        public static final String REGISTER_SUCCESS = "Đăng ký thành công";
    }
    public interface CATEGORY_TYPE{
         public static final String BUSINESS_TYPE="BUSINESS_TYPE";
         public static final String USER_TYPE="USER_TYPE";
    }
    public interface USER_TYPE {
        public static final Long ALL = -1L;
        public static final Long ADMIN = 1L;
        public static final Long NOT_ADMIN = 0L;
        public static final Long ENTERPRISE_USER = 2L;
    }
}
