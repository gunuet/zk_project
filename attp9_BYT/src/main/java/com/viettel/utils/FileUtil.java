package com.viettel.utils;

import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import org.apache.commons.io.FilenameUtils;
import org.zkoss.util.media.Media;

public class FileUtil {

    public static void copyFile(File sourceFile, File destFile)
            throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;
        FileInputStream sourceStream;
        FileOutputStream destStream;
        try {
            sourceStream = new FileInputStream(sourceFile);
            destStream = new FileOutputStream(destFile);
            source = sourceStream.getChannel();
            destination = destStream.getChannel();
            destination.transferFrom(source, 0, source.size());
            sourceStream.close();
            destStream.close();
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
            
        }
    }

    public static boolean validFileType(String fileType) throws UnsupportedEncodingException {

        fileType = FilenameUtils.getExtension(fileType);
        boolean result = false;
        String sExt = ResourceBundleUtil.getString("extend_file", "config");
        String[] fileTypes = sExt.split(",");
        for (String s : fileTypes) {
            if (s.toLowerCase().equals(fileType.toLowerCase())) {
                result = true;
                break;
            }
        }
        return result;
        //return true;
    }

    public static boolean validFileTypeImage(String fileType) throws UnsupportedEncodingException {
        fileType = FilenameUtils.getExtension(fileType);
        boolean result = false;
        String sExt = ResourceBundleUtil.getString("extend_file_image", "config");
        String[] fileTypes = sExt.split(",");
        for (String s : fileTypes) {
            if (s.toLowerCase().equals(fileType.toLowerCase())) {
                result = true;
                break;
            }
        }
        return result;
        //return true;
    }

    public static boolean validFileTypeHq(String fileType) throws UnsupportedEncodingException {
        fileType = FilenameUtils.getExtension(fileType);
        boolean result = false;
        String sExt = ResourceBundleUtil.getString("extend_file_hq", "config");
        String[] fileTypes = sExt.split(",");
        for (String s : fileTypes) {
            if (s.toLowerCase().equals(fileType.toLowerCase())) {
                result = true;
                break;
            }
        }
        return result;
        //return true;
    }

    public static boolean validFileTypePdf(String fileType) throws UnsupportedEncodingException {
        fileType = FilenameUtils.getExtension(fileType);
        boolean result = false;
        String sExt = ResourceBundleUtil.getString("extend_file_pdf", "config");
        String[] fileTypes = sExt.split(",");
        for (String s : fileTypes) {
            if (s.toLowerCase().equals(fileType.toLowerCase())) {
                result = true;
                break;
            }
        }
        return result;
        //return true;
    }

    public static boolean saveFile(String path, Media media) throws Exception {
        boolean bReturn = true;
        if (media == null) {
            return bReturn;
        }
        File f = new File(path);

        if (f.exists()) {
        } else {
            f.createNewFile();
        }
        InputStream inputStream;
        OutputStream outputStream;
        try {
            //save to hard disk and database
            inputStream = media.getStreamData();
            outputStream = new FileOutputStream(f);

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
//            if (outputStream != null) {
                outputStream.close();
//            }
//            if (inputStream != null) {
                inputStream.close();
//            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            bReturn = false;
        } finally {

        }
        return bReturn;
    }

    public static boolean saveAttachFile(Long attachId, Media media) throws Exception {
        boolean bReturn = true;
        if (media == null) {
            return bReturn;
        }
        String path = ResourceBundleUtil.getString("dir_upload");
        File folder = new File(path);
        //
        // Kiem tra thu muc co ton tai hay khong
        //
        if (folder.exists()) {
        } else {
            folder.mkdirs();
        }
        //
        // Ghi ra file theo duong dan
        //
        path = path + File.separator + attachId;

        return saveFile(path, media);
    }

    public static File createTempFile(File source, String name) {
        String tempDir = System.getProperty("java.io.tmpdir");
        File tempFile = new File(tempDir, name);
        try {
            copyFile(source, tempFile);
        } catch (IOException e) {
            LogUtils.addLogDB(e);
        }
        return tempFile;
    }

    /**
     * Make folder path
     *
     * @param path
     * @return
     */
    public static boolean mkdirs(String path) {
        try {
            File f = new File(path);
            if (!f.exists()) {
                f.mkdirs();
            }
            return true;
        } catch (Exception e) {
            LogUtils.addLogDB(e);
        }
        return false;
    }

    public static String getSafeFileName(String input) {
        return getSafeFileNameAll(input);
    }

    public static String getSafeFileNameAll(String input) {
        if (input == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != '/' && c != ' ' && c != '#' && c != '%' && c != '@' && c != '(' && c != ')' && c != '^' && c != ';' && c != '\\' && c != ':' && c != '*' && c != '?' && c != '|' && c != '<' && c != '>' && c != 0) {
                sb.append(c);
            }
        }
        String text = (new BaseGenericForwardComposer()).removeVietnameseChar(sb.toString());

        return text;
    }

    /**
     * linhdx do bi loi file co dau cach nen 1 so file cu da co dau cach
     */
    public static String getSafeFileNameForDowload(String input) {
        if (input == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != '/' && c != '\\' && c != ':' && c != '*' && c != '?' && c != '|' && c != '<' && c != '>' && c != 0) {
                sb.append(c);
            }
        }
        String text = (new BaseGenericForwardComposer()).removeVietnameseChar(sb.toString());

        return text;
    }
}
