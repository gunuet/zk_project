/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viettel.core.base.DAO.BaseGenericForwardComposer;
import com.viettel.core.sys.BO.ActionLog;
import com.viettel.voffice.BO.Calendar.Calendar;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.BO.ErrorLog;
import com.viettel.core.user.BO.Department;
import com.viettel.voffice.BO.Document.DocumentPublish;
import com.viettel.voffice.BO.Document.DocumentReceive;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.user.BO.Roles;
import com.viettel.core.user.BO.Users;
import com.viettel.core.sys.DAO.ActionLogDAOHE;
import com.viettel.voffice.DAOHE.Calendar.CalendarDAOHE;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.sys.DAO.ErrorLogDAOHE;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.voffice.DAOHE.DocumentDAOHE;
import com.viettel.voffice.DAOHE.DocumentReceiveDAOHE;
import com.viettel.core.workflow.DAO.FlowDAOHE;
import com.viettel.core.user.DAO.RolesDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;

/**
 *
 * @author HaVM2
 */
public class LogUtils {

    private static final Logger logger = Logger.getLogger("");

    public static void addLog(String description) {
        LogStruct log = new LogStruct();
        log.className = Thread.currentThread().getStackTrace()[2].getClassName();
        log.lineOfCode = Thread.currentThread().getStackTrace()[2].getLineNumber();
        log.methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        log.description = description;
        logger.error("className:" + log.className + " lineOfCode:" + log.lineOfCode
                + " methodName: " + log.methodName + " description:" + log.description);
    }
    
    public static void addLog(Exception en) {
        LogStruct log = new LogStruct();
        log.className = Thread.currentThread().getStackTrace()[2].getClassName();
        log.lineOfCode = Thread.currentThread().getStackTrace()[2].getLineNumber();
        log.methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        //log.description = stackTrace(en);
        logger.error(log, en);
        logger.error("className:" + log.className + " lineOfCode:" + log.lineOfCode
                + " methodName: " + log.methodName + " description:" + log.description);
    }

    public static void addLogDB(String description) {
        LogStruct log = new LogStruct();
        log.className = Thread.currentThread().getStackTrace()[2].getClassName();
        log.lineOfCode = Thread.currentThread().getStackTrace()[2].getLineNumber();
        log.methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        log.description = description;
        logger.error("className:" + log.className + " lineOfCode:" + log.lineOfCode
                + " methodName: " + log.methodName + " description:" + log.description);
//        ErrorLog errorLog = new ErrorLog();
//        errorLog.setClassName(log.className);
//        errorLog.setLineOfCode(String.valueOf(log.lineOfCode));
//        errorLog.setMethodName(log.methodName);
//        errorLog.setDescription(log.description);
//        errorLog.setCreateDate(new Date());
//        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute("userToken");
//        if (tk != null) {
//            errorLog.setUserId(tk.getUserId());
//            errorLog.setUserName(tk.getUserName());
//            errorLog.setDeptId(tk.getDeptId());
//            errorLog.setDeptName(tk.getDeptName());
//        }
//
//        ErrorLogDAOHE adhe = new ErrorLogDAOHE();
//        adhe.saveOrUpdate(errorLog);
//        adhe.commit();
    }

    

    public static void addLogDB(Exception en) {
        LogStruct log = new LogStruct();
        log.className = Thread.currentThread().getStackTrace()[2].getClassName();
        log.lineOfCode = Thread.currentThread().getStackTrace()[2].getLineNumber();
        log.methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        log.description = stackTrace(en);
        logger.error("class:" + log.className + " line:" + log.lineOfCode
                + " method: " + log.methodName + " des:" + log.description);
//        ErrorLog errorLog = new ErrorLog();
//        errorLog.setClassName(log.className);
//        errorLog.setLineOfCode(String.valueOf(log.lineOfCode));
//        errorLog.setMethodName(log.methodName);
//        errorLog.setDescription(log.description);
//        errorLog.setCreateDate(new Date());
//        UserToken tk = null;
//        if(Sessions.getCurrent(true) !=null ){
//            tk = (UserToken) Sessions.getCurrent(true).getAttribute("userToken");
//        }
//        if (tk != null) {
//            errorLog.setUserId(tk.getUserId());
//            errorLog.setUserName(tk.getUserName());
//            errorLog.setDeptId(tk.getDeptId());
//            errorLog.setDeptName(tk.getDeptName());
//        }
//
//        ErrorLogDAOHE adhe = new ErrorLogDAOHE();
//        adhe.saveOrUpdate(errorLog);
//        adhe.commit();
    }
    
    public static void addLogNoImportant(Exception en) {
        logger.error("");
    }

    public static void addLog(Long deptId, Long userId, String userName, Long actionType, String actionName, Long modun, Long objectId, Long objectType, String objectTitle, String ip) {
        ActionLog log = new ActionLog();
        log.setUserId(userId);
        log.setUserName(userName);
        log.setActionType(actionType);
        log.setActionName(actionName);
        log.setModun(modun);
        log.setObjectId(objectId);
        log.setObjectType(objectType);
        log.setObjectTitle(objectTitle);
        log.setActionDate(new Date());
        log.setIp(ip);
        ActionLogDAOHE adhe = new ActionLogDAOHE();
        adhe.saveOrUpdate(log);
    }

    public static void addLog(Long deptId, Long userId, String userName, Long actionType, String actionName, Long modun, Long objectId, Long objectType, String objectTitle, String ip, Object obj) {
        ActionLog log = new ActionLog();
        log.setUserId(userId);
        log.setUserName(userName);
        log.setActionType(actionType);
        log.setActionName(actionName);
        log.setModun(modun);
        log.setObjectId(objectId);
        log.setObjectType(objectType);
        log.setObjectTitle(objectTitle);
        log.setActionDate(new Date());
        log.setIp(ip);
        if (obj != null) {
            GsonBuilder b = new GsonBuilder();
            b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
            Gson gson = b.create();
            //Gson gson = new Gson();
            String json = gson.toJson(obj);
            log.setOldData(json);
        }
        ActionLogDAOHE adhe = new ActionLogDAOHE();
        adhe.saveOrUpdate(log);
    }

    //
    // Khoi phuc lai log
    //
    public static void undoLog(Long logId) {
        ActionLogDAOHE ldhe = new ActionLogDAOHE();
        ActionLog log = ldhe.findById(logId);
        if (log == null) {
            return;
        }

        if (!log.getActionType().equals(Constants.ACTION.TYPE.UPDATE) && !log.getActionType().equals(Constants.ACTION.TYPE.DELETE)) {
            Clients.showNotification("co loi xay ra");
            return;
        }

        if (log.getOldData() == null) {
            Clients.showNotification("khong the undo");
            return;
        }

        try {
            Gson gson = new Gson();

            if (log.getObjectType().equals(Constants.OBJECT_TYPE.DOCUMENT_RECEIVE)) {
                DocumentReceive obj = gson.fromJson(log.getOldData(), DocumentReceive.class);
                DocumentReceiveDAOHE dhe = new DocumentReceiveDAOHE();
                dhe.update(obj);
            } else if (log.getObjectType().equals(Constants.OBJECT_TYPE.DOCUMENT_PUBLISH)) {
                DocumentPublish obj = gson.fromJson(log.getOldData(), DocumentPublish.class);
                DocumentDAOHE dhe = new DocumentDAOHE();
                dhe.update(obj);
            } else if (log.getObjectType().equals(Constants.OBJECT_TYPE.FILES)) {
//                Files obj = gson.fromJson(log.getOldData(), Files.class);
//                FilesDAOHE dhe = new FilesDAOHE();
//                dhe.update(obj);
            } else if (log.getObjectType().equals(Constants.OBJECT_TYPE.CALENDAR)) {
                Calendar obj = gson.fromJson(log.getOldData(), Calendar.class);
                CalendarDAOHE dhe = new CalendarDAOHE();
                dhe.update(obj);
            } else if (log.getObjectType().equals(Constants.OBJECT_TYPE.USER)) {
                Users obj = gson.fromJson(log.getOldData(), Users.class);
                UserDAOHE dhe = new UserDAOHE();
                dhe.update(obj);
            } else if (log.getObjectType().equals(Constants.OBJECT_TYPE.DEPT)) {
                Department obj = gson.fromJson(log.getOldData(), Department.class);
                DepartmentDAOHE dhe = new DepartmentDAOHE();
                dhe.update(obj);
            } else if (log.getObjectType().equals(Constants.OBJECT_TYPE.CATEGORY)) {
                Category obj = gson.fromJson(log.getOldData(), Category.class);
                CategoryDAOHE dhe = new CategoryDAOHE();
                dhe.update(obj);
            } else if (log.getObjectType().equals(Constants.OBJECT_TYPE.ROLE)) {
                Roles obj = gson.fromJson(log.getOldData(), Roles.class);
                RolesDAOHE dhe = new RolesDAOHE();
                dhe.update(obj);
            } else if (log.getObjectType().equals(Constants.OBJECT_TYPE.FLOW)) {
                Flow obj = gson.fromJson(log.getOldData(), Flow.class);
                FlowDAOHE dhe = new FlowDAOHE();
                dhe.update(obj);
            }
            //
            // undo xong thi phai xoa di
            //
            ldhe.delete(log);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            Clients.showNotification("Undo bi loi");
        }
    }
    private static String stackTrace(Exception cause) {
        if (cause == null)
            return "";
//        StringWriter sw = new StringWriter(1024);
//        final PrintWriter pw = new PrintWriter(sw);
//        cause.printStackTrace(pw);
//        pw.flush();
//        String rt = sw.toString();
//        if(rt.length()>1000){
//            rt = sw.toString().substring(0, 1000);
//        }
//        return rt;
        return cause.getMessage();
    }
}

class LogStruct {

    String className;
    int lineOfCode;
    String methodName;
    String description;
}
