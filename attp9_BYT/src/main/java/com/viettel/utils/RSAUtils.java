/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.utils;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.RSAPrivateKeySpec;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Linhdx
 */
public class RSAUtils {

    public RSAUtils() {
    }

    public String decrypt(String strInput) throws NoSuchAlgorithmException {
        String returnStr = "";
        String _module = "21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=";
//        String _exponent = "AQAB";
        String _d = "cgoRoAUpSVfHMdYXW9nA3dfX75dIamZnwPtFHq80ttagbIe4ToYYCcyUz5NElhiNQSESgS5uCgNWqWXt5PnPu4XmCXx6utco1UVH8HGLahzbAnSy6Cj3iUIQ7Gj+9gQ7PkC434HTtHazmxVgIR5l56ZjoQ8yGNCPZnsdYEmhJWk=";
        try {
            //byte[] expBytes = Base64.decodeBase64(_exponent);
            byte[] modBytes = Base64.decodeBase64(_module);
            byte[] dBytes = Base64.decodeBase64(_d);

            BigInteger modules = new BigInteger(1, modBytes);
            BigInteger d = new BigInteger(1, dBytes);

            KeyFactory factory = KeyFactory.getInstance("RSA");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            RSAPrivateKeySpec privSpec = new RSAPrivateKeySpec(modules, d);
            PrivateKey privKey = factory.generatePrivate(privSpec);
            cipher.init(Cipher.DECRYPT_MODE, privKey);

            byte[] encrypted = Base64.decodeBase64(strInput);
            byte[] decrypted = cipher.doFinal(encrypted);
            returnStr = new String(decrypted);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
        return returnStr;
    }
}
