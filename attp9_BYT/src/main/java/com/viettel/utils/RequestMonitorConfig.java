/*    */ package com.viettel.utils;
/*    */
/*    */ public class RequestMonitorConfig /*    */ {
    /*    */ private static Long logDuration;
    /*    */ private static String defAction;
    /*    */ private static String defMessage;
    /*    */
    /*    */ public static String getDefAction() /*    */ {
        /* 18 */ return defAction;
        /*    */    }
    /*    */
    /*    */ public static void setDefAction(String defAction) {
        /* 22 */ RequestMonitorConfig.defAction = defAction;
        /*    */    }
    /*    */
    /*    */ public static String getDefMessage() {
        /* 26 */ return defMessage;
        /*    */    }
    /*    */
    /*    */ public static void setDefMessage(String defMessage) {
        /* 30 */ RequestMonitorConfig.defMessage = defMessage;
        /*    */    }
    /*    */
    /*    */ public static Long getLogDuration() {
        /* 34 */ return logDuration;
        /*    */    }
    /*    */
    /*    */ public static void setLogDuration(Long logDuration) {
        /* 38 */ RequestMonitorConfig.logDuration = logDuration;
        /*    */    }
    /*    */ }

/* Location:           C:\Work\RDFW 315.jar
 * Qualified Name:     com.viettel.common.util.RequestMonitorConfig
 * JD-Core Version:    0.6.2
 */