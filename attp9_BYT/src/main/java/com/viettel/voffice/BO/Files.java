/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.BO;

import com.viettel.utils.StringUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MrBi
 */
@Entity
@Table(name = "FILES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Files.findAll", query = "SELECT f FROM Files f"),
    @NamedQuery(name = "Files.findByFileId", query = "SELECT f FROM Files f WHERE f.fileId = :fileId"),
    @NamedQuery(name = "Files.findByFileType", query = "SELECT f FROM Files f WHERE f.fileType = :fileType"),
    @NamedQuery(name = "Files.findByFileTypeName", query = "SELECT f FROM Files f WHERE f.fileTypeName = :fileTypeName"),
    @NamedQuery(name = "Files.findByFileCode", query = "SELECT f FROM Files f WHERE f.fileCode = :fileCode"),
    @NamedQuery(name = "Files.findByFileName", query = "SELECT f FROM Files f WHERE f.fileName = :fileName"),
    @NamedQuery(name = "Files.findByStatus", query = "SELECT f FROM Files f WHERE f.status = :status"),
    @NamedQuery(name = "Files.findByTaxCode", query = "SELECT f FROM Files f WHERE f.taxCode = :taxCode"),
    @NamedQuery(name = "Files.findByBusinessId", query = "SELECT f FROM Files f WHERE f.businessId = :businessId"),
    @NamedQuery(name = "Files.findByBusinessName", query = "SELECT f FROM Files f WHERE f.businessName = :businessName"),
    @NamedQuery(name = "Files.findByBusinessAddress", query = "SELECT f FROM Files f WHERE f.businessAddress = :businessAddress"),
    @NamedQuery(name = "Files.findByBusinessPhone", query = "SELECT f FROM Files f WHERE f.businessPhone = :businessPhone"),
    @NamedQuery(name = "Files.findByBusinessFax", query = "SELECT f FROM Files f WHERE f.businessFax = :businessFax"),
    @NamedQuery(name = "Files.findByCreateDate", query = "SELECT f FROM Files f WHERE f.createDate = :createDate"),
    @NamedQuery(name = "Files.findByModifyDate", query = "SELECT f FROM Files f WHERE f.modifyDate = :modifyDate"),
    @NamedQuery(name = "Files.findByCreatorId", query = "SELECT f FROM Files f WHERE f.creatorId = :creatorId"),
    @NamedQuery(name = "Files.findByCreatorName", query = "SELECT f FROM Files f WHERE f.creatorName = :creatorName"),
    @NamedQuery(name = "Files.findByCreateDeptId", query = "SELECT f FROM Files f WHERE f.createDeptId = :createDeptId"),
    @NamedQuery(name = "Files.findByCreateDeptName", query = "SELECT f FROM Files f WHERE f.createDeptName = :createDeptName"),
    @NamedQuery(name = "Files.findByIsActive", query = "SELECT f FROM Files f WHERE f.isActive = :isActive"),
    @NamedQuery(name = "Files.findByVersion", query = "SELECT f FROM Files f WHERE f.version = :version"),
    @NamedQuery(name = "Files.findByIsTemp", query = "SELECT f FROM Files f WHERE f.isTemp = :isTemp"),
    @NamedQuery(name = "Files.findByParentFileId", query = "SELECT f FROM Files f WHERE f.parentFileId = :parentFileId"),
    @NamedQuery(name = "Files.findByFlowId", query = "SELECT f FROM Files f WHERE f.flowId = :flowId"),
    @NamedQuery(name = "Files.findByStartDate", query = "SELECT f FROM Files f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "Files.findByNumDayProcess", query = "SELECT f FROM Files f WHERE f.numDayProcess = :numDayProcess"),
    @NamedQuery(name = "Files.findByDeadline", query = "SELECT f FROM Files f WHERE f.deadline = :deadline"),
    @NamedQuery(name = "Files.findByFlagView", query = "SELECT f FROM Files f WHERE f.flagView = :flagView"),
    @NamedQuery(name = "Files.findByBusinessEmail", query = "SELECT f FROM Files f WHERE f.businessEmail = :businessEmail"),
    @NamedQuery(name = "Files.findByNswFileCode", query = "SELECT f FROM Files f WHERE f.nswFileCode = :nswFileCode")})
public class Files implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "FILES_SEQ", sequenceName = "FILES_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILES_SEQ")
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "FILE_TYPE")
    private Long fileType;
    @Size(max = 510)
    @Column(name = "FILE_TYPE_NAME")
    private String fileTypeName;
    @Size(max = 62)
    @Column(name = "FILE_CODE")
    private String fileCode;
    @Size(max = 510)
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "STATUS")
    private Long status;
    @Size(max = 62)
    @Column(name = "TAX_CODE")
    private String taxCode;
    @Column(name = "BUSINESS_ID")
    private Long businessId;
    @Size(max = 510)
    @Column(name = "BUSINESS_NAME")
    private String businessName;
    @Size(max = 1000)
    @Column(name = "BUSINESS_ADDRESS")
    private String businessAddress;
    @Size(max = 62)
    @Column(name = "BUSINESS_PHONE")
    private String businessPhone;
    @Size(max = 62)
    @Column(name = "BUSINESS_FAX")
    private String businessFax;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "MODIFY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate;
    @Column(name = "CREATOR_ID")
    private Long creatorId;
    @Size(max = 510)
    @Column(name = "CREATOR_NAME")
    private String creatorName;
    @Column(name = "CREATE_DEPT_ID")
    private Long createDeptId;
    @Size(max = 510)
    @Column(name = "CREATE_DEPT_NAME")
    private String createDeptName;
    @Column(name = "IS_ACTIVE")
    private Long isActive;
    @Column(name = "VERSION")
    private Long version;
    @Column(name = "IS_TEMP")
    private Long isTemp;
    @Column(name = "PARENT_FILE_ID")
    private Long parentFileId;
    @Column(name = "FLOW_ID")
    private Long flowId;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "NUM_DAY_PROCESS")
    private Long numDayProcess;
    @Column(name = "DEADLINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;
    @Column(name = "FLAG_VIEW")
    private Long flagView;
    @Size(max = 500)
    @Column(name = "BUSINESS_EMAIL")
    private String businessEmail;
    @Size(max = 200)
    @Column(name = "NSW_FILE_CODE")
    private String nswFileCode;
    @Column(name = "NEXT_USER")
    private Long nextUser;
    @Column(name = "FINISH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;

    @Column(name = "FILE_TYPE_HS")
    private Long fileTypeHS;
    @Column(name = "IS_CHANGE")
    private Long isChange;
    @Column(name = "REASONS")
    private String reasons;
    @Column(name = "NOTES")
    private String notes;
    @Column(name = "QR_CODE")
    private byte[] qrCode;
    @Column(name = "HAVE_QR_CODE")
    private Long haveQrCode;

    public Files() {
    }

    public Files(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileType() {
        return fileType;
    }

    public void setFileType(Long fileType) {
        this.fileType = fileType;
    }

    public String getFileTypeName() {
        return fileTypeName;
    }

    public void setFileTypeName(String fileTypeName) {
        this.fileTypeName = fileTypeName;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessFax() {
        return businessFax;
    }

    public void setBusinessFax(String businessFax) {
        this.businessFax = businessFax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Long getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(Long createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public Long getIsActive() {
        return isActive;
    }

    public void setIsActive(Long isActive) {
        this.isActive = isActive;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(Long isTemp) {
        this.isTemp = isTemp;
    }

    public Long getParentFileId() {
        return parentFileId;
    }

    public void setParentFileId(Long parentFileId) {
        this.parentFileId = parentFileId;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Long getNumDayProcess() {
        return numDayProcess;
    }

    public void setNumDayProcess(Long numDayProcess) {
        this.numDayProcess = numDayProcess;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Long getFlagView() {
        return flagView;
    }

    public void setFlagView(Long flagView) {
        this.flagView = flagView;
    }

    public String getBusinessEmail() {
        return replaceCharacter(businessEmail);
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = replaceCharacter(businessEmail);
    }

    public String getNswFileCode() {
        return nswFileCode;
    }

    public void setNswFileCode(String nswFileCode) {
        this.nswFileCode = nswFileCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileId != null ? fileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Files)) {
            return false;
        }
        Files other = (Files) object;
        if ((this.fileId == null && other.fileId != null) || (this.fileId != null && !this.fileId.equals(other.fileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.voffice.BO.Files[ fileId=" + fileId + " ]";
    }

    /**
     * @return the nextUser
     */
    public Long getNextUser() {
        return nextUser;
    }

    /**
     * @param nextUser the nextUser to set
     */
    public void setNextUser(Long nextUser) {
        this.nextUser = nextUser;
    }

    /**
     * @return the finishDate
     */
    public Date getFinishDate() {
        return finishDate;
    }

    /**
     * @param finishDate the finishDate to set
     */
    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Long getFileTypeHS() {
        return fileTypeHS;
    }

    public void setFileTypeHS(Long fileTypeHS) {
        this.fileTypeHS = fileTypeHS;
    }

    public Long getIsChange() {
        return isChange;
    }

    public void setIsChange(Long isChange) {
        this.isChange = isChange;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public String replaceCharacter(String sourceStr) {
        return StringUtils.replaceCharacter(sourceStr);
    }

    public byte[] getQrCode() {
        return qrCode;
    }

    public void setQrCode(byte[] qrCode) {
        this.qrCode = qrCode;
    }

    public Long getHaveQrCode() {
        return haveQrCode;
    }

    public void setHaveQrCode(Long haveQrCode) {
        this.haveQrCode = haveQrCode;
    }

}
