/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.LogUtils;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Files;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

/**
 *
 * @author Linhdx
 */
public class FilesDAOHE extends GenericDAOHibernate<Files, Long> {

    public FilesDAOHE() {
        super(Files.class);
    }

    @Override
    public void saveOrUpdate(Files files) {
        if (files != null) {
            //getSession().merge(files);
            super.saveOrUpdate(files);
        }

        getSession().flush();

    }

    public void SaveAndCommit(Files files) {
        if (files != null) {
            super.saveOrUpdate(files);
        }
        getSession().flush();
        //getSession().getTransaction().commit();

    }

    public void commit() {
        getSession().getTransaction().commit();

    }

    public void rollBack() {
        getSession().getTransaction().rollback();
    }

    @Override
    public Files findById(Long id) {
        Query query = getSession().getNamedQuery("Files.findByFileId");
        query.setParameter("fileId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Files) result.get(0);
        }
    }

    public Files findByNswCode(String code, Long type, Long temp, Long active) {
        Query query;
        if (temp == 1L) {
            query = getSession()
                    .createQuery(
                            "Select f from Files f where f.nswFileCode = :code "
                            + " and f.fileType=:type "
                            + " and  f.isActive = :active and  f.isTemp = :temp  order by f.startDate desc");
            query.setParameter("code", code);
            query.setParameter("type", type);
            query.setParameter("temp", temp);
            query.setParameter("active", active);

        } else {
            query = getSession()
                    .createQuery(
                            "Select f from Files f where f.nswFileCode = :code "
                            + " and f.fileType=:type "
                            + " and  f.isActive = :active  order by f.startDate desc");
            query.setParameter("code", code);
            query.setParameter("type", type);
            query.setParameter("active", active);

        }
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Files) result.get(0);
        }
    }

    public PagingListModel searchFilesPermit(Files searchForm, int start, int take) {
        List listParam = new ArrayList();
        StringBuilder strBuf = new StringBuilder("SELECT n from Files n where n.isActive = 1 and n.finishDate is not null ");
        StringBuilder strCountBuf = new StringBuilder(
                "select count(n) from Files n where n.isActive = 1 and n.finishDate is not null ");
        StringBuilder hql = new StringBuilder();
        if (StringUtils.validString(searchForm.getTaxCode())) {
            hql.append(" AND lower(n.taxCode) like ? escape '/'");
            listParam.add(StringUtils.toLikeString(searchForm
                    .getTaxCode()));
        }
        if (StringUtils.validString(searchForm.getFileCode())) {
            hql.append(" AND lower(n.fileCode) like ? escape '/'");
            listParam.add(StringUtils.toLikeString(searchForm
                    .getFileCode()));
        }
        if (StringUtils.validString(searchForm.getBusinessName())) {
            hql.append(" AND lower(n.businessName) like ? escape '/'");
            listParam.add(StringUtils.toLikeString(searchForm
                    .getBusinessName()));
        }
        if (searchForm.getFileType() != null && searchForm.getFileType() > 1L) {
            hql.append(" AND n.fileType = ? ");
            listParam.add(searchForm.getFileType());
        }
        if (searchForm.getStatus() != null && searchForm.getStatus() > 1L) {
            hql.append(" AND n.status = ? ");
            listParam.add(searchForm.getStatus());
        }
        strBuf.append(hql);
        strCountBuf.append(hql);
        strBuf.append(" order by n.modifyDate");
        Query query = session.createQuery(strBuf.toString());
        Query countQuery = session.createQuery(strCountBuf.toString());

        for (int i = 0; i < listParam.size(); i++) {
            query.setParameter(i, listParam.get(i));
            countQuery.setParameter(i, listParam.get(i));
        }

        query.setFirstResult(start);
        if (take < Integer.MAX_VALUE) {
            query.setMaxResults(take);
        }

        List lst = query.list();
        Long count = (Long) countQuery.uniqueResult();
        PagingListModel model = new PagingListModel(lst, count);
        return model;
    }

//    public Files findByNswCode(String code) {
//        Query query = getSession()
//                .createQuery(
//                        "Select f from Files f where nswFileCode = :code and isActive = 1 and ( isTemp = 0 or isTemp is null )");
//        query.setParameter("code", code);
//        List result = query.list();
//        if (result.isEmpty()) {
//            return null;
//        } else {
//            return (Files) result.get(0);
//        }
//    }
    public Files findByNswCodeAndDoctype(String code, Long type) {
        Query query = getSession()
                .createQuery(
                        "Select f from Files f where f.nswFileCode = :code "
                        + " and f.fileType=:type"
                        + " and f.isActive = 1 and ( f.isTemp = 0 or f.isTemp is null )");
        query.setParameter("code", code);
        query.setParameter("type", type);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Files) result.get(0);
        }
    }

    @Override
    public void delete(Files files) {
        files.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(files);
    }

    public void delete(Long fileId) {
        Files files = findById(fileId);
        files.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(files);
        getSession().flush();
    }

    public Long countHomePage(String countType) {
        if (countType.isEmpty()) {
            return 0L;
        }
        List listParam = new ArrayList();
        StringBuilder strCountBuf = new StringBuilder("select count(n) ");
        StringBuilder hql = new StringBuilder();
        hql.append(" FROM Files n " + "WHERE n.isActive = 1 "
                + "AND (n.isTemp is null OR n.isTemp = 0) ");
        Long count = 0L;
        try {
            switch (countType) {
                case Constants.COUNT_HOME_TYPE.TOTAL:
                    break;
                case Constants.COUNT_HOME_TYPE.WAIT_PROCESS:
                    hql.append("AND ((n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) " + ") ");
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP_XNK);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP_TTB);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP_XNN);
                    break;
                case Constants.COUNT_HOME_TYPE.PROCESS:
                    hql.append("AND n.status not in (?,?,?,?,?,?,?,?,?,?,?,?) ");
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP_XNK);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP_TTB);
                    listParam.add(Constants.FILE_STATUS_CODE.STATUS_DN_DANOP_XNN);

                    listParam.add(Constants.FILE_STATUS_CODE.FINISH);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_GIAM_XNK);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_CHAT_XNK);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_LDC_PHE_DUYET_XU_LY_LO_HANG_KHONG_DAT_XNK);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_TTB);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_TU_CHOI_TTB);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_XNN);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_TU_CHOI_XNN);
                    break;
                case Constants.COUNT_HOME_TYPE.FINISH:
                    hql.append("AND ((n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) "
                            + "OR (n.status = ?) " + "OR (n.status = ?) " + ") ");
                    listParam.add(Constants.FILE_STATUS_CODE.FINISH);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_GIAM_XNK);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_CHAT_XNK);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_LDC_PHE_DUYET_XU_LY_LO_HANG_KHONG_DAT_XNK);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_TTB);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_TU_CHOI_TTB);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_XNN);
                    listParam
                            .add(Constants.FILE_STATUS_CODE.STATUS_VAN_THU_DONG_DAU_TU_CHOI_XNN);
                    break;
                default:
                    break;
            }

            strCountBuf.append(hql);
            Query countQuery = session.createQuery(strCountBuf.toString());

            for (int i = 0; i < listParam.size(); i++) {
                countQuery.setParameter(i, listParam.get(i));
            }

            count = (Long) countQuery.uniqueResult();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            // Add log to here
        }
        return count;
    }

    public int checkHaveQrCode(Long fileId) {
        String HQL = " SELECT count(r) FROM Files r WHERE r.isActive=:isActive AND r.fileId=:fileId AND r.haveQrCode = :haveQrCode";
        Query query = session.createQuery(HQL.toString());
        query.setParameter("isActive", Constants_Cos.Status.ACTIVE);
        query.setParameter("fileId", fileId);
        query.setParameter("haveQrCode", Constants_Cos.Status.ACTIVE);
        Long count = (Long) query.uniqueResult();
        if (count > 0) {
            return Constants.CHECK_VIEW.VIEW;
        }
        return Constants.CHECK_VIEW.NOT_VIEW;
    }
}
