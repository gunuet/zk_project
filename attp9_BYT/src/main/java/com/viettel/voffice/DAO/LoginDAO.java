/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.utils.ResourceBundleUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;
import org.zkforge.bwcaptcha.Captcha;

/**
 *
 * @author HaVM2
 */
public class LoginDAO extends BaseComposer {

    @Wire
    Textbox userName;
    @Wire
    Textbox password, txtCaptchaId;
    @Wire
    Captcha capcha;
    @Wire
    Label userNameError;
    @Wire
    Label passwordError;
    @Wire
    Label loginError;
    @Wire
    Vbox vboxCaptchaId;

    @Override
    public ComponentInfo doBeforeCompose(Page page,
            Component parent,
            ComponentInfo compInfo) {
        Session ss = Sessions.getCurrent();
        if (ss.getAttribute("countCaptcha") == null) {
            ss.setAttribute("countCaptcha", 0);
        }
        if (ss.getAttribute("userToken") != null) {
//            Execution exec = Executions.getCurrent();
//            HttpServletRequest req = (HttpServletRequest) exec.getNativeRequest();
            Executions.sendRedirect("/index.zul");
        }
        return compInfo;
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        this.getPage().setTitle(ResourceBundleUtil.getString("title"));
        super.doAfterCompose(comp);
        capcha.setBgColor(16777215);
        capcha.setCaptchars(Constants.getCaptchars());
        if (Integer.valueOf(Sessions.getCurrent().getAttribute("countCaptcha").toString()) >= 2) {
            vboxCaptchaId.setVisible(true);
            Sessions.getCurrent().setAttribute("isVisibleCaptcha", true);
        }
    }

    @Listen("onOK=#loginForm")
    public void onEnter() throws Exception {
        onLogin();
    }

    @Listen("onClick=#btnLogin")
    public void onLogin() throws Exception {
        if(userName.getValue().equals("the.lv")){
            loginError.setValue("Bạn chưa làm gì đã lỗi rồi nhé");
            return;
        }
//        LogUtils.addLog("Xin chào vừa vào login nè");
//        if(userName.getValue().trim().length()==3)
//        {
//            Helper hp = new Helper();
//            hp.receiveMs("<?xml version=\"1.0\" encoding=\"utf-8\"?><Envelope><Header><Reference><version>1.0</version></Reference><From><name>0304184415</name></From><To><name>BYT</name><identity>0100100108912</identity></To><Subject><type>21</type><function>01</function><reference>122015000034</reference><preReference>122015000034</preReference><documentYear>2015</documentYear><sendDate>2015-06-12 17:45:02</sendDate></Subject></Header><Body><Content><ImportDocument><AttachmentList><Attachment><AttachmentId>424</AttachmentId><AttachmentCode>120</AttachmentCode><AttachmentName>1._bill_-_SWB_XC151807.pdf</AttachmentName><AttachTypeCode>3</AttachTypeCode></Attachment><Attachment><AttachmentId>425</AttachmentId><AttachmentCode>121</AttachmentCode><AttachmentName>1.pdf</AttachmentName><AttachTypeCode>4</AttachTypeCode></Attachment><Attachment><AttachmentId>427</AttachmentId><AttachmentCode>123</AttachmentCode><AttachmentName>pack.pdf</AttachmentName><AttachTypeCode>6</AttachTypeCode></Attachment><Attachment><AttachmentId>428</AttachmentId><AttachmentCode>124</AttachmentCode><AttachmentName>1._Contract-HaiPhong.pdf</AttachmentName><AttachTypeCode>143</AttachTypeCode></Attachment><Attachment><AttachmentId>429</AttachmentId><AttachmentCode>125</AttachmentCode><AttachmentName>174-COCA_FCB_PART_3,.pdf</AttachmentName><AttachTypeCode>22</AttachTypeCode></Attachment></AttachmentList><ProductList><Product><Pass d4p1:nil=\"true\" xmlns:d4p1=\"http://www.w3.org/2001/XMLSchema-instance\" /><NetweightUnitName>Kilogam (gồm bao bì)</NetweightUnitName><TotalUnitName>Túi</TotalUnitName><ManufactureAddress>172 Bocheng Wu Road, Boxing County, Shandong Province, China</ManufactureAddress><NationalName>China</NationalName><Manufacture>SHANDONG XIANGCHI JIANYUAN BIO - TECH CO., LTD</Manufacture><ProductDescriptions>Đóng 22 tấn/bao</ProductDescriptions><ProductName>Xiro Fructose ( High Fructose Corn Syrup 55)</ProductName><ProductCode>122015000034000001</ProductCode><NationalCode>CN</NationalCode><Total>5</Total><TotalUnitCode>BAG</TotalUnitCode><Netweight>110500</Netweight><NetweightUnitCode>KII</NetweightUnitCode><ConfirmAnnounceNo>174/ATTP-SP</ConfirmAnnounceNo><ConfirmAnnounceDate /><CheckTime /><BaseUnit>1056088000</BaseUnit><CreatedDate>2015-06-12 17:36:31</CreatedDate><ExpiredDate /></Product></ProductList><DeptCode>4</DeptCode><ExporterName>SHANDONG XIANGCHI JIANYUAN BIO- TECH CO., LTD</ExporterName><ExporterAddress>172 Bocheng Wu Road, Boxing County, Shandong Province, China</ExporterAddress><ExporterPhone>0086 543 2617618</ExporterPhone><ExporterFax>0086 543 2617618</ExporterFax><ExporterEmail>sandy@hfcs.net.cn</ExporterEmail><ExporterGateName>QINGDAO PORT, CHINA</ExporterGateName><ImporterGateCode>VNNHC</ImporterGateCode><ComingDate>2015-06-12 00:00:00</ComingDate><BillNo>XC 151807</BillNo><CheckTime>2015-06-15 00:00:00</CheckTime><CheckPlace>Cảng Nam Hải</CheckPlace><CreatedBy>0304184415</CreatedBy><CreatedDate>2015-06-12 17:31:13</CreatedDate><CreaterName>Nguyễn Thị Hải Yến</CreaterName><CustomCode>03PA  </CustomCode><CustomsName>CC HQ QL hàng ĐT-GC Hải Phòng                                                                    </CustomsName><ContractNo>RMSWT-0315-1891</ContractNo><DeptName>Trung tâm Y tế dự phòng Hải Phòng</DeptName><GoodsOwnerAddress>Km 17, Quốc lộ 1A, xã Duyên Thái, huyện Thường Tín, Thành phố Hà Nội</GoodsOwnerAddress><GoodsOwnerEmail>yennt.nltrans@gmail.com</GoodsOwnerEmail><GoodsOwnerFax>84 313569286</GoodsOwnerFax><GoodsOwnerName>Chi nhánh công ty TNHH nước giải khát Coca Cola Việt Nam tại Hà Nội</GoodsOwnerName><GoodsOwnerPhone>84 902005158</GoodsOwnerPhone><ModifiedDate /><NswFileCode>122015000034</NswFileCode><ResponsiblePersonName>Chi nhánh công ty TNHH nước giải khát Coca Cola Việt Nam tại Hà Nội</ResponsiblePersonName><ResponsiblePersonAddress>Km 17, Quốc lộ 1A, xã Duyên Thái, huyện Thường Tín, Thành phố Hà Nội</ResponsiblePersonAddress><ResponsiblePersonPhone>84 902005158</ResponsiblePersonPhone><ResponsiblePersonFax>84 313569286</ResponsiblePersonFax><ResponsiblePersonEmail>yennt.nltrans@gmail.com</ResponsiblePersonEmail><TransNo>AQDVHCD 5020661</TransNo></ImportDocument></Content></Body></Envelope>");
//        }
        Session ss = Sessions.getCurrent();
        if (Long.valueOf(ss.getAttribute("countCaptcha").toString()) == 0) {
            ss.setAttribute("timeStart", new Date());
            ss.setAttribute("timeEnd", new Date());
            ss.setAttribute("isVisibleCaptcha", false);
        }
        if (Long.valueOf(ss.getAttribute("countCaptcha").toString()) == 5) {
            ss.setAttribute("timeEnd", new Date());
        }
        Long timeStart = ((Date) ss.getAttribute("timeStart")).getTime();
        Long timeEnd = ((Date) ss.getAttribute("timeEnd")).getTime();
        Long time = timeEnd - timeStart;
        if (Integer.valueOf(ss.getAttribute("countCaptcha").toString()) > 0
                && capcha != null
                && capcha.getValue() != null
                && !capcha.getValue().equals(txtCaptchaId.getValue())
                && (Boolean) ss.getAttribute("isVisibleCaptcha")) {
            loginError.setVisible(true);
            loginError.setValue("Nhập lại mã Captcha");

            String name = userName.getText();
            UserDAOHE udhe = new UserDAOHE();
            Users user = udhe.getUserByName(name);
//            if(user != null && !"admin".equals(user.getUserName())){
//                user.setStatus(0L);
//                udhe.saveOrUpdate(user);
//                loginError.setValue("Tài khoản đã bị khóa");
//                return;
//            }
            Long userId = -1L;
            if (user != null) {
                userId = user.getUserId();
            }
            LogUtils.addLog(null, userId, null, Constants.ACTION.TYPE.LOGIN, Constants.ACTION.NAME.CAPTCHA_LOG,
                    Constants.OBJECT_TYPE.LOG_IN, null, Constants.OBJECT_TYPE.LOG_IN, null, ss.getRemoteAddr());
            txtCaptchaId.setValue(null);
            capcha.randomValue();
            return;
        }

//        capcha.setWidth("1000");
//        capcha.randomValue();
//        capcha.setBgColor(2);
        String name = userName.getText();
        String pass = password.getText();
        UserDAOHE udhe = new UserDAOHE();
        String strLogin = udhe.checkLogin(name, pass);
        if ("".equals(strLogin)) {
            UserToken token = new UserToken();
            Users user = udhe.getUserByName(name);
            if (user != null) {
                token.setUserId(user.getUserId());
                token.setUserName(user.getUserName());
                token.setUserFullName(user.getFullName());
                token.setDeptId(user.getDeptId());
                token.setDeptName(user.getDeptName());
                token.setPosId(user.getPosId());
                token.setPosName(user.getPosName());
                token.setAvatarPath(user.getAvartarPath());
            }
            List lstDepartments = null;
            if (user != null) {
                lstDepartments = udhe.getWorkingDepartmentOfUser(user.getUserId());
            }

            //
            // Fix session fixation
            //
            if (lstDepartments != null && lstDepartments.size() > 1) {
                Map args = new ConcurrentHashMap();
                args.put("token", token);
                Window wnd = (Window) Executions.createComponents("/Pages/selectWorkingDept.zul", null, args);
                wnd.doModal();
            } else {
                if (lstDepartments != null && lstDepartments.size() == 1) {
                    Department d = (Department) lstDepartments.get(0);
                    token.setDeptId(d.getDeptId());
                    token.setDeptName(d.getDeptName());
                }
                if (user != null) {
                    token.setLstMenu(udhe.getMenuOfUsers(user.getUserId()));
                }

                if (token.getLstMenu() == null || token.getLstMenu().size() == 0) {
//                if(!token.getUserName().contains("admin")){
                    loginError.setValue("Bạn chưa được phân quyền vào hệ thống, hãy hỏi quản trị để biết chi tiết");
                } else {
                    HttpServletRequest req = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
                    HttpSession httpSession = req.getSession(true);
                    //httpSession.invalidate();
                    httpSession = req.getSession(true);
                    httpSession.setAttribute("userToken", token);
//                    String hopdong = ResourceBundleUtil.getString("hopdong", "config");
//                    if ("true".equals(hopdong)) {
//                        if (user.getBirthday() != null) {//Da dang nhap
//                            Date date = user.getBirthday();
//                            Date now = new Date();
//                            if (now.getTime() - date.getTime() < 60000) {
//                                loginError.setValue("Bạn đã đăng nhập ở 1 máy khác");
//                                return;
//                            }
//                            
//                        } else {
//                            user.setBirthday(new Date());
//                            udhe.saveOrUpdate(user);
//                        }
//                    }
//                    udhe.saveOrUpdate(user);
                    Executions.sendRedirect("/index.zul");
//                    Executions.sendRedirect("http://dantri.com.vn/", true);
//                    Execution exec = Executions.getCurrent();
//                    HttpServletResponse response = (HttpServletResponse) exec.getNativeResponse();
//                    try {
//                        response.sendRedirect(response.encodeRedirectURL("http://dantri.com.vn/")); //assume there is /login
//                    } catch (IOException ex) {
//                        Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                    exec.setVoided(true); //no need to create UI since redirect will take place

                    // ghi log dang nhap
                    String ip = req.getHeader("X-FORWARDED-FOR");
                    if (ip == null) {
                        ip = req.getRemoteAddr();
                    }
                    ss.removeAttribute("timeStart");
                    ss.removeAttribute("timeEnd");
                    ss.removeAttribute("isVisibleCaptcha");
                    ss.removeAttribute("countCaptcha");
                    LogUtils.addLog(token.getDeptId(), token.getUserId(), token.getUserName(), Constants.ACTION.TYPE.LOGIN, Constants.ACTION.NAME.LOGIN,
                            Constants.OBJECT_TYPE.LOG_IN, token.getUserId(), Constants.OBJECT_TYPE.LOG_IN, token.getUserName(), ip);

                }
            }
        } else {
            txtCaptchaId.setValue(null);
            if (capcha != null) {
                capcha.randomValue();
            }

            if (time > 5 * 1000 && Long.valueOf(ss.getAttribute("countCaptcha").toString()) <= 2) {
                ss.setAttribute("countCaptcha", -1);
                ss.setAttribute("isVisibleCaptcha", false);
            }
            ss.setAttribute("countCaptcha", Long.valueOf(ss.getAttribute("countCaptcha").toString()) + 1);
            if (Long.valueOf(ss.getAttribute("countCaptcha").toString()) > 2
                    && time <= 5 * 1000) {
                vboxCaptchaId.setVisible(true);
                ss.setAttribute("isVisibleCaptcha", true);
            }
            loginError.setValue(strLogin);
        }

    }

    @Listen("onClick=#btnGetPass")
    public void onGetPass() {
        Window userChangePass = (Window) Executions.createComponents("/Pages/admin/user/userGetPass.zul", null, null);
        userChangePass.doModal();
    }
}
