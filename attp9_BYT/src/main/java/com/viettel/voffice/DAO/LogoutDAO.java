/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO;

import static com.mchange.v2.c3p0.impl.C3P0Defaults.user;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author HaVM2
 */
public class LogoutDAO extends BaseComposer {

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        Session session = Sessions.getCurrent();
        UserToken token = (UserToken)session.getAttribute("userToken");
        UserDAOHE udhe = new UserDAOHE();
        Users user = udhe.findById(token.getUserId());
        user.setBirthday(null);
        udhe.saveOrUpdate(user);
        session.invalidate();

        
        Executions.sendRedirect("/Pages/login.zul");
    }

}
