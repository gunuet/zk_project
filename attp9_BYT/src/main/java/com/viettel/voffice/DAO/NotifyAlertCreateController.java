/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.voffice.DAO;

import com.viettel.utils.Constants;
import com.viettel.utils.ConvertFileUtils;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.BO.Users;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.voffice.BO.Home.Notify;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.voffice.DAOHE.NotifyDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.utils.LogUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkforge.ckez.CKeditor;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author huantn1
 */
public class NotifyAlertCreateController extends NotifyListController{
    
    @Wire
    private Textbox txtNotifyId,txtTitle;
    @Wire
    private Listbox lboxStatus;
    @Wire
    private Datebox dbEndTime;
    @Wire
    private CKeditor ckContent;
    @Wire
    Window createNotifyDlg;
    @Wire("#notifyWindowSelect #notifyWindowUserDept #lstNotify")
    private Listbox lstNotify;
    @Wire
    private  DepartmentDAOHE departmentDAOHE =new DepartmentDAOHE();
    private  UserDAOHE userDAOHE = new UserDAOHE();
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        loadItemForEdit();
    }
    private void loadItemForEdit() {
        Notify u = (Notify) Executions.getCurrent().getArg().get("notify");
        if (u == null) {
            return;
        }
        txtNotifyId.setValue(u.getNotifyId().toString());
        txtTitle.setValue(u.getTitle().toString());
        ckContent.setValue(u.getContent()== null ? "" : u.getContent());
        dbEndTime.setValue(u.getEndTime());
        List<NodeDeptUser> tempList = new ArrayList<NodeDeptUser>();
        if(u.getMultiDept() != null || u.getMultiUser() != null){
            String[] itemsMutilDept = u.getMultiDept().split(";");
            String[] itemsMutilUser = u.getMultiUser().split(";");
            for(int i = 0 ; i< itemsMutilDept.length; i++){
                NodeDeptUser nodeDeptUser = new NodeDeptUser();
                Department dept = departmentDAOHE.findById(Long.parseLong(itemsMutilDept[i]));
                nodeDeptUser.setDeptName(dept.getDeptName());
                nodeDeptUser.setDeptId(Long.parseLong(itemsMutilDept[i]));
                if( !"null".equals(itemsMutilUser[i])){
                    Users user = userDAOHE.findById(Long.parseLong(itemsMutilUser[i]));
                    nodeDeptUser.setUserName(user.getFullName());
                    nodeDeptUser.setUserId(Long.parseLong(itemsMutilUser[i]));
                }
                tempList.add(nodeDeptUser);
            }
        }
        lstNotify.setModel(new ListModelList(tempList));
    }
    
    @Listen("onClick=#btnChoose")
    public void openTree() throws Exception {
        Map<String, Object> arguments = new ConcurrentHashMap<String, Object>();
        arguments.put("lstNotify", lstNotify);
        createWindow("wdTreeNotify",
                    "/Pages/notify/treeObjectsNotify.zul",
                    arguments, Window.MODAL);
    }
    @Listen("onClick=#btnCreate")
    public void onCreate() throws Exception {
        try {
            if (validate()) {
                onSaveUser();
            }
        } catch (Exception en) {
            LogUtils.addLogDB(en);
            showNotification(en.getMessage(), Constants.Notification.ERROR);
        }
    }
    @Listen("onClick=#btnClose")
    public void onClose() {
        createNotifyDlg.detach();
    }
    private boolean validate() throws Exception {
        if (txtTitle.getValue().isEmpty()) {
            txtTitle.focus();
            throw new Exception("Tên chức vụ không được để trống");
        }
        if (txtTitle.getValue().length() > 250) {
            txtTitle.focus();
            throw new Exception("Tên chức vụ nhỏ hơn 250 ký tự");
        }
        if (ckContent.getValue().length() > 500 ) {
            txtTitle.focus();
            throw new Exception("Nội dung thông báo quá lớn ( nhỏ hơn 500 ký tự)");
        }
        return true;
    }
    @Listen("onClick=#btnShowSearchDept")
    public void onOpenDeptSelect() {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("idOfDisplayNameComp", "/createNotifyDlg/txtSearchDept");
        args.put("idOfDisplayIdComp", "/createNotifyDlg/txtSearchDeptId");
        Window showDeptDlg = (Window) Executions.createComponents("/Pages/admin/user/userDept.zul", null, args);
        showDeptDlg.doModal();
    }
    private void onSaveUser() {
        boolean error = false;
        try {
            NotifyDAOHE notifyDAOHE = new NotifyDAOHE();
            Notify notify = new Notify();
            if ( !txtNotifyId.getValue().isEmpty()) {
                notify = notifyDAOHE.findById(Long.parseLong(txtNotifyId.getValue()));
            } 
            notify.setUserId(getUserId());
            notify.setTitle(txtTitle.getValue().trim());
            notify.setContent(ckContent.getValue().trim());
            notify.setSendTime(new Date());
            notify.setEndTime(dbEndTime.getValue());
            String status = lboxStatus.getSelectedItem().getValue();
            notify.setStatus(Long.parseLong(status));
            notify.setObjectType(Constants.OBJECT_TYPE.NOTIFY_OBJECT_TYPE);
            String listMutilUserId = "";
            String listMutilDeptId = "";
            NodeDeptUser ndu;
            for (Listitem item : lstNotify.getItems()) {
                ndu = (NodeDeptUser) item.getValue();
                listMutilUserId = listMutilUserId + ndu.getUserId() + ";";
                listMutilDeptId = listMutilDeptId + ndu.getDeptId() + ";";
            }
            notify.setMultiUser(listMutilUserId);
            notify.setMultiDept(listMutilDeptId);
            notifyDAOHE.createOrUpdate(notify);
            notifyDAOHE.flush();

        } catch (Exception en) {
            LogUtils.addLogDB(en);
            showNotification(en.getMessage(), Constants.Notification.ERROR);
            error = true;
        }
        if (!error) {
            showNotification("Cập nhật thành công", Constants.Notification.INFO);
            Window parentWnd = (Window) Path.getComponent("/notifyWindow");
            Events.sendEvent(new Event("onReload", parentWnd, null));
            if (txtTitle.getValue() != null && txtTitle.getValue().length() > 0) {
                //
                // cap nhat thi dong lai
                //
                createNotifyDlg.detach();
            } else {
                //
                // them moi thi reload lai form
                //
                txtTitle.setValue("");
                ckContent.setValue("");
            }
        }
    }
//    @Listen("onClick=#btnUpload")
//    public void onUpload(UploadEvent evt) throws IOException {
//        media = evt.getMedia();
//String extFile = media.getName().replace("\"", "");
//            if (!FileUtil.validFileType(extFile)) {
//                showNotification("Ð?nh d?ng file không du?c phép t?i lên",
//                        Constants.Notification.WARNING);
//                return;
//            }
//        if (attachCurrent != null) {
//            Messagebox.show("Bạn có đồng ý thay thế tệp biểu mẫu cũ?", "Thông báo", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
//                public void onEvent(Event evt) throws InterruptedException {
//                    if (Messagebox.ON_OK.equals(evt.getName())) {
//                        txtFileName.setValue(media.getName());
//                        imgDelFile.setVisible(true);
//                        String fullPath = attachCurrent.getAttachPath() + attachCurrent.getAttachName();
//                        //xóa file cũ nếu có
//                        File f = new File(fullPath);
//                        if (f.exists()) {
//                            ConvertFileUtils.deleteFile(fullPath);
//                        }
//                        AttachDAOHE attachDAOHE = new AttachDAOHE();
//                        attachDAOHE.delete(attachCurrent);
//                    }
//                }
//            });
//        } else {
//            txtFileName.setValue(media.getName());
//            imgDelFile.setVisible(true);
//        }
//
//    }
    
}
