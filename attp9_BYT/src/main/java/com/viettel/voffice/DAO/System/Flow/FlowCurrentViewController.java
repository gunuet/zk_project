/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Flow;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;
import com.viettel.core.workflow.BO.Process;
import java.util.Date;

/**
 *
 * @author HaVM2
 */
public class FlowCurrentViewController extends BaseComposer {

    private static final long serialVersionUID = 1L;

    @Wire
    Window flowConfigDlg;
    @Wire
    Listbox lbProcess;

    Long objectId;
    Long objectType;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        objectId = (Long) Executions.getCurrent().getArg().get("objectId");
        objectType = (Long) Executions.getCurrent().getArg().get("objectType");
        displayFlow(objectId, objectType);
    }

    public void displayFlow(Long objectId, Long objectType) {
        ProcessDAOHE pdhe = new ProcessDAOHE();
        List lstProcess = pdhe.getProcessProcess(objectId, objectType);
        if (lstProcess == null || lstProcess.isEmpty()) {
            return;
        }
        //
        // Hien thi tren danh sach
        //
        ListModelList lstModel = new ListModelList(lstProcess);
        lbProcess.setModel(lstModel);
        //
        // hien thi do hoa
        //
        //Gson gson = new Gson();
        //String json = gson.toJson(lstProcess);
        // viethd3 19/03/2015
        // li do: phan ve do thi luong xu ly dang bi loi nen khong hien thi
        //Clients.evalJavaScript("page.process = " + json + ";");
        //Clients.evalJavaScript("loadAndDrawFlowContent();");
    }

    @Listen("onBack=#flowConfigDlg")
    public void onBack() {
        flowConfigDlg.detach();
    }

    public String getStatus(Long status) {
        return WorkflowAPI.getStatusName(status);
    }

    public boolean isAbleToModifyReset(Process p) {
        boolean result = false;
        //String userId = getUserName();
        if ("admin".equals(getUserName()) && p.getFinishDate() != null) {
            result = true;
        }
        return result;
    }

    public boolean isAbleToModifySet(Process p) {
        boolean result = false;
        //String userId = getUserName();
        if ("admin".equals(getUserName()) && p.getFinishDate() == null) {
            result = true;
        }
        return result;
    }
    public boolean isAbleToViewStatus(Process p) {
        boolean result = false;
        //String userId = getUserName();
        if ("admin".equals(getUserName())) {
            result = true;
        }
        return result;
    }

    @Listen("onUpdate=#lbProcess")
    public void onUpdate() {

    }

    @Listen("onResetFinishDate=#lbProcess")
    public void onResetFinishDate(Event ev) {
        Process p = (Process) ev.getData();
        if (p.getFinishDate() != null) {
            p.setFinishDate(null);
        }
        ProcessDAOHE pHe = new ProcessDAOHE();
        pHe.saveAndCommit(p);

        displayFlow(objectId, objectType);
    }

    @Listen("onSetFinishDate=#lbProcess")
    public void onSetFinishDate(Event ev) {
        Process p = (Process) ev.getData();
        if (p.getFinishDate() == null) {
            p.setFinishDate(new Date());
        }
        ProcessDAOHE pHe = new ProcessDAOHE();
        pHe.saveAndCommit(p);
        displayFlow(objectId, objectType);
    }

}
