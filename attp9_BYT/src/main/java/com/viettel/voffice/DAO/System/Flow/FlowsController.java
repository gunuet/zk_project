/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Flow;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.user.BO.VUsers;
import com.viettel.core.workflow.DAO.FlowDAOHE;
import com.viettel.core.workflow.model.FlowModel;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author HaVM2
 */
public class FlowsController extends BaseComposer {

    @Wire
    Textbox txtSearchFlowName, txtSearchFlowCode, txtSearchDeptName, txtSearchDeptId;
    @Wire
    Listbox cbSearchObjects;
    @Wire
    Listbox lstFlows;
    @Wire
    Window createDlg, flowWindow, configWindow;

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);
            CategoryDAOHE cdhe = new CategoryDAOHE();
            List lstObjects = cdhe.getSelectCategoryByType(Constants.OBJECT_CONSTANT);
            ListModelArray lstModel = new ListModelArray(lstObjects);
            cbSearchObjects.setModel(lstModel);
            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    @Listen("onClick=#btnShowSearchDept")
    public void onOpenDeptSelect() {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("idOfDisplayNameComp", "/flowWindow/txtSearchDeptName");
        args.put("idOfDisplayIdComp", "/flowWindow/txtSearchDeptId");
        Window showDeptDlg = (Window) Executions.createComponents("/Pages/admin/user/userDept.zul", null, args);
        showDeptDlg.doModal();

    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        Clients.showBusy("");
        try {
            FlowModel fm = new FlowModel();
            fm.setFlowName(txtSearchFlowName.getValue());
            fm.setFlowCode(txtSearchFlowCode.getValue());
            if (cbSearchObjects.getSelectedItem() != null) {
                fm.setObjectId((Long) cbSearchObjects.getSelectedItem().getValue());
            }
            if (!txtSearchDeptId.getValue().isEmpty()) {
                fm.setDeptId(Long.parseLong(txtSearchDeptId.getValue()));
            } else {
                if (!"admin".equals(getUserName())) {
                    fm.setDeptId(getDeptId());
                }
            }

            FlowDAOHE fdhe = new FlowDAOHE();
            List lst = fdhe.search(fm);
            ListModelArray lma = new ListModelArray(lst);
            lstFlows.setModel(lma);
        } catch (WrongValueException | NumberFormatException ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    @Listen("onReload=#flowWindow")
    public void onReloadGrid() {
        onSearch();
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() {
        createDlg = (Window) Executions.createComponents("/Pages/admin/flow/flowCreate.zul", null, null);
        createDlg.doModal();
    }

    @Listen("onEdit=#lstFlows")
    public void onEdit() {
        Flow item = lstFlows.getSelectedItem().getValue();
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("item", item);
        createDlg = (Window) Executions.createComponents("/Pages/admin/flow/flowCreate.zul", null, args);
        createDlg.doModal();
    }

    @Listen("onDelete=#lstFlows")
    public void onDelete() {
        Messagebox.show("Xác nhận xóa người dùng?", "Thông báo", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event e) throws IOException {
                        if (null != e.getName()) {
                            switch (e.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    Flow item = lstFlows.getSelectedItem().getValue();
                                    deleteFlow(item);
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                        lstFlows.clearSelection();
                    }
                });
    }

    public void deleteFlow(Flow item) {
        try {            
            FlowDAOHE fdhe = new FlowDAOHE();
            item = fdhe.findById(item.getFlowId());
            addLog(Constants.ACTION.TYPE.DELETE, Constants.ACTION.NAME.DELETE,
                    item.getFlowId(), Constants.OBJECT_TYPE.FLOW, item.getFlowName(), item);
            item.setIsActive(-1l);
            fdhe.update(item);
            showSuccessNotification("Xóa thành công");
            onSearch();
        } catch (Exception en) {
            LogUtils.addLogDB(en);
            showNotification(en.getMessage());
        }
    }

    @Listen("onConfig=#lstFlows")
    public void onConfig() {
        Flow item = lstFlows.getSelectedItem().getValue();
        FlowDAOHE fdhe = new FlowDAOHE();
        FlowModel fm = fdhe.getFlowModel(item.getFlowId());
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("parentWindow", flowWindow);
        args.put("flowModel", fm);
        //flowWindow.setHeight("0px");
        //flowWindow.setVisible(false);
        if (configWindow != null) {
            configWindow.detach();
        }
        configWindow = (Window) Executions.createComponents("/Pages/admin/flow/flowConfig.zul", flowWindow.getParent(), args);
        configWindow.doModal();
    }
}
