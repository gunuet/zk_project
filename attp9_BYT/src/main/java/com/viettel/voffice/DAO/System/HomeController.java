/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.base.model.ToolbarModel;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.BO.VDepartment;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.module.importDevice.DAO.IdMeetingDAO;
import com.viettel.module.importDevice.DAO.ListImportDeviceDAO;
import com.viettel.module.importDevice.Model.SearchDeviceModel;
import com.viettel.module.importOrder.DAO.ImportOrderFileDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.DateTimeUtils;
import com.viettel.utils.LogUtils;
import com.viettel.voffice.BO.Calendar.Calendar;
import com.viettel.voffice.BO.Document.DocumentPublish;
import com.viettel.voffice.BO.Home.Notify;
import com.viettel.voffice.DAOHE.DocumentDAOHE;
import com.viettel.voffice.DAOHE.DocumentReceiveDAOHE;
import com.viettel.voffice.DAOHE.NotifyDAOHE;
import com.viettel.voffice.DAOHE.Calendar.CalendarDAOHE;
import com.viettel.voffice.model.DocumentReceiveSearchModel;
import com.viettel.voffice.model.DocumentSearchModel;
import com.viettel.voffice.model.SearchModel;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Vlayout;

/**
 *
 * @author havm2
 */
@SuppressWarnings("serial")
public class HomeController extends BaseComposer {

    @Wire
    Textbox txtStart, txtEnd;
    @Wire
    Vlayout divATTP, divTTB, divXNN;
    @Wire
    Window homeWnd, windowNotifyDetailView;
    @Wire
    Label lbDocOutNotProcess, lbDocOutPublish, lbDocOutInWeek, lbCalInDay,
            lbCalInTomorow, lbCalInWeek, lbFileCoop, lbFileMain, lbFileAll,
            lbFileCoopTbi, lbFileMainTbi, lbFileAllTbi, lbFileCoFopXnn, lbFileCoopXnn,
            lbFileMainXnn, lbFileAllXnn, lbFileXnk, lbFileXnn, lbFileTbi,
            lbGroupTtbAll, lbGroupTtbAccept, lbGroupTtbReject, lbGroupTtbMeetingReport;
    @Wire
    private Label lbDocInWaitingProcess, lbDocInExpired, lbDocInProcessing;
    @Wire
    private Groupbox gbDoanhNghiep, gbChuyenVien;
    UserDAOHE userDAOHE = new UserDAOHE();
    private Users user;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); // To change body of generated methods,
        // choose Tools | Templates.
//        onLoadNotify(null);
//        onLoadCountDocOut();
//        onLoadCountDocIn();
//        onLoadCountCalendar();
//        onCountFiles();

        Long userId = getUserId();
        UserDAOHE userDAO = new UserDAOHE();
        user = userDAO.findById(userId);

        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            gbDoanhNghiep.setVisible(true);
            gbChuyenVien.setVisible(false);
        } else {
            gbChuyenVien.setVisible(true);
            gbDoanhNghiep.setVisible(false);
        }
        onCountFiles();
    }

    @Listen("onLoadNext=#homeWnd")
    public void onNextNotify(Event event) {
        Long start = 0l;
        Long end = 0l;

        if (txtStart != null) {
            if (!txtStart.getValue().isEmpty()) {
                start = Long.parseLong(txtStart.getValue());
            }
        }

        if (txtEnd != null) {
            if (!txtEnd.getValue().isEmpty()) {
                end = Long.parseLong(txtEnd.getValue());
            }
        }

        start += end;
        if (txtStart != null) {
            txtStart.setValue(start.toString());
        }

        onLoadNotify(event);
    }

    @Listen("onReLoadNotify=#homeWnd")
    public void onReLoadNotify(Event event) {
        txtStart.setValue("0");
        onLoadNotify(event);
    }

    @Listen("onSearchFullText=#homeWnd")
    public void onSearch(Event event) {
        txtStart.setValue("0");
        String data = event.getData().toString();
        Gson gson = new Gson();
        ToolbarModel model = gson.fromJson(data, ToolbarModel.class);

        NotifyDAOHE ndhe = new NotifyDAOHE();
        Notify searchNotify = null;
        if (model != null) {
            searchNotify = new Notify();
            searchNotify.setContent(model.getSearchText());
        }
        // List<Notify> lstNotify = ndhe.getNotifyOfUser(getUserId(),
        // searchNotify, 0l, 20l);
        List<Notify> lstNotify = ndhe.getNotifyOfUser(getUserId(), getDeptId(),
                searchNotify, 0l, 20l);
        String objects = gson.toJson(lstNotify);
        Clients.evalJavaScript("clearNotify();");
        Clients.evalJavaScript("page.lstNotify=" + objects + ";");
        Clients.evalJavaScript("createNotifys()");
    }

    @Listen("onLoadNotify=#homeWnd")
    public void onLoadNotify(Event event) {
        Long start = 0l;
        Long end = 0l;

        if (txtStart != null) {
            if (!txtStart.getValue().isEmpty()) {
                start = Long.parseLong(txtStart.getValue());
            }
        }

        if (txtEnd != null) {
            if (!txtEnd.getValue().isEmpty()) {
                end = Long.parseLong(txtEnd.getValue());
            }
        }

        if (end == 0l) {
            end = 20l;
        }

        Gson gson = new Gson();
        Notify searchNotify = null;
        if (event != null) {
            searchNotify = gson.fromJson(event.getData().toString(),
                    Notify.class);
        }
        NotifyDAOHE ndhe = new NotifyDAOHE();
        // List<Notify> lstNotify = ndhe.getNotifyOfUser(getUserId(),
        // searchNotify, start, end);
        List<Notify> lstNotify = ndhe.getNotifyOfUser(getUserId(), getDeptId(),
                searchNotify, start, end);
        String objects = gson.toJson(lstNotify);
        // Clients.evalJavaScript("clearNotify();");
        Clients.evalJavaScript("page.lstNotify=" + objects + ";");
        Clients.evalJavaScript("createNotifys()");
        // Clients.evalJavaScript("createNotifys('" + objects + "')");
        // Thông báo nội bô
//        onSearch();
        List<Notify> lstNotifyAlert = ndhe.getNotifyAlertOfUser(getUserId(), getListVDepartment(), null, start, end);
        String objectsNotifyAlert = gson.toJson(lstNotifyAlert);
        // Clients.evalJavaScript("clearNotify();");
        Clients.evalJavaScript("page.lstNotifyAlert=" + objectsNotifyAlert + ";");
        Clients.evalJavaScript("createNotifysAlert()");
    }

    public List<String> getListVDepartment() {
        Long deptId = getDeptId();
        Users user = userDAOHE.findById(getUserId());
        List<String> deptLst = new ArrayList<String>();
        try {

            deptLst.add(user.getDeptId().toString());
            VDepartment vDepartment = (new DepartmentDAOHE()).getVDepartmentDeptId(deptId);
            if (vDepartment != null && vDepartment.getDeptPath() != null) {
                String[] pathStr = vDepartment.getDeptPath().split("/");

                if (pathStr.length != 0) {
                    int deptIndex = 0;
                    int len = pathStr.length;
                    for (int i = 0; i < len; i++) {
                        if (deptId.toString().equals(pathStr[i])) {
                            deptIndex = i;
                            break;
                        }
                    }
                    for (int i = 0; i <= deptIndex; i++) {
                        String a = pathStr[i];
                        if (a != null && !"".equals(a)) {
                            deptLst.add(a);
                        }
                    }
                }
            }
        } catch (NumberFormatException ex) {
            LogUtils.addLogDB(ex);
        }
        return deptLst;
    }

    @Listen("onOpenView = #homeWnd")
    public void onOpenView(Event event) {
        if (event != null) {
            Gson gson = new Gson();
            String data = event.getData().toString();
            Notify notify = gson.fromJson(data, Notify.class);
            Map<String, Object> arguments = new ConcurrentHashMap<>();
            // Van ban den
            if (Constants.OBJECT_TYPE.DOCUMENT_RECEIVE.equals(notify
                    .getObjectType())) {
                arguments.put("documentReceiveId", notify.getObjectId());
                arguments.put("menuType", Constants.DOCUMENT_MENU.VIEW);
                arguments.put("parentWindow", homeWnd);
                createWindow("windowDocInView",
                        "/Pages/document/docIn/viewDocIn.zul", arguments,
                        Window.EMBEDDED);
//                DocumentReceiveDAOHE documentReceiveDAOHE = new DocumentReceiveDAOHE();
//                List listProcess = documentReceiveDAOHE.getProcessFromDocument(notify.getObjectId(), getUserId(), getDeptId(), isFileClerk());

                homeWnd.setVisible(false);
            } else if (Constants.OBJECT_TYPE.DOCUMENT_PUBLISH.equals(notify
                    .getObjectType())) {
                // Van ban di
                DocumentDAOHE docPublishDaoHe = new DocumentDAOHE();
                DocumentPublish docPublish = docPublishDaoHe.getById(
                        "documentPublishId", notify.getObjectId());
                arguments.put("selectedRecord", docPublish);
                arguments.put("recordMode", "Edit");
                arguments.put("parentWindow", homeWnd);
                arguments.put("urlType", Constants.DOCUMENT_MENU.ALL);
                createWindow("docView", "/Pages/document/docOut/view.zul",
                        arguments, Window.MODAL);
            } else if (Constants.OBJECT_TYPE.CALENDAR.equals(notify
                    .getObjectType())) {
                CalendarDAOHE cdhe = new CalendarDAOHE();
                Calendar cal = cdhe.findById(notify.getObjectId());
                arguments.put("calendar", cal);
                arguments.put("viewType",
                        Constants.CALENDAR_VIEWTYPE.USER_APPROVE);
                arguments.put("fromHome", true);
                if (cal.getCreateUserId().equals(getUserId())) {
                    Window createWnd = (Window) Executions.createComponents(
                            "/Pages/calendar/calendarCreateWnd.zul", null,
                            arguments);
                    createWnd.doModal();
                } else {
                    Window viewWnd = (Window) Executions.createComponents(
                            "/Pages/calendar/calendarViewWnd.zul", null,
                            arguments);
                    viewWnd.doModal();
                }
            } else if (Constants.OBJECT_TYPE.FILES.equals(notify
                    .getObjectType())) {
                arguments.put("fileId", notify.getObjectId());
                Window viewWnd = (Window) Executions.createComponents(
                        "/Pages/file/fileViewWnd.zul", null, arguments);
                viewWnd.setWidth("1200px");
                viewWnd.doModal();
            }
        }
    }

    /**
     * ham nay load thong bao noi bo
     *
     * @param event
     */
    @Listen("onOpenViewAlert = #homeWnd")
    public void onOpenViewAlert(Event event) {
//        String str = "";
        if (event != null) {
            Gson gson = new Gson();
            String data = event.getData().toString();
            Notify notify = gson.fromJson(data, Notify.class);
//            Map<String, Object> arguments = new ConcurrentHashMap<>();
            Map arguments = new ConcurrentHashMap();
            arguments.put("notify", notify);
//            arguments.put("parentWindow", homeWnd);
            windowNotifyDetailView = (Window) Executions.createComponents("/Pages/notify/notifyDetailView.zul", null, arguments);
            windowNotifyDetailView.doModal();
//            createWindow("windowNotifyDetailView",
//                    "/Pages/notify/notifyDetailView.zul", arguments,
//                    Window.EMBEDDED);

//            homeWnd.setVisible(false);
        }
    }

    public Long countDocOutHome(int criteria) {
        DocumentDAOHE docDaoHe = new DocumentDAOHE();
        DocumentSearchModel searchForm = new DocumentSearchModel();
        if (criteria == 1) {
            // count so van ban di cho xu ly
            searchForm.setUrlType(Constants.DOCUMENT_MENU.WAITING_PROCESS);
        } else if (criteria == 2) {
            // count so van ban qua han
            searchForm.setUrlType(Constants.DOCUMENT_MENU.MENU_PUBLISHED);
            searchForm.setStatus(Constants.DOCUMENT_STATUS.PUBLISH);
        } else {
            // count so van ban trong tuan
            searchForm.setUrlType(Constants.DOCUMENT_MENU.WAITING_PROCESS);
            searchForm.setDateCreateFrom(DateTimeUtils.getWeekStart(null));
            searchForm.setDateCreateTo(DateTimeUtils.getWeekEnd(null));
        }

        searchForm.setCreatorId(getUserId());
        Long count = docDaoHe.countDocument(searchForm, 0, Integer.MAX_VALUE);
        if (count == null) {
            return 0L;
        }
        return count;
    }

    private void onLoadCountDocOut() {
        lbDocOutNotProcess.setValue(countDocOutHome(1).toString());
        lbDocOutPublish.setValue(countDocOutHome(2).toString());
        lbDocOutInWeek.setValue(countDocOutHome(3).toString());
    }

    private void onLoadCountDocIn() {
        DocumentReceiveDAOHE documentReceiveDAOHE = new DocumentReceiveDAOHE();
        DocumentReceiveSearchModel model = new DocumentReceiveSearchModel();
        model.setMenuType(Constants.DOCUMENT_MENU.WAITING_PROCESS);
        Long size = (Long) documentReceiveDAOHE.getDocumentReceiveOfUser(
                getUserId(), getDeptId(), model, -1, -1, true, isFileClerk())
                .get(0);
        lbDocInWaitingProcess.setValue(size.toString());

        lbDocInExpired.setValue(String.valueOf(documentReceiveDAOHE
                .numberOfExpiredDoc(getDeptId(), getUserId(), isFileClerk())));

        model.setMenuType(Constants.DOCUMENT_MENU.PROCESSING);
        size = (Long) documentReceiveDAOHE.getDocumentReceiveOfUser(
                getUserId(), getDeptId(), model, -1, -1, true, isFileClerk())
                .get(0);
        lbDocInProcessing.setValue(size.toString());
    }

    private void onLoadCountCalendar() {
        CalendarDAOHE cdhe = new CalendarDAOHE();
        Long userId = getUserId();
        int cToday = cdhe.countCalendarInDay(userId);
        int cTomorow = cdhe.countCalendarInTomorow(userId);
        int cInWeek = cdhe.countCalendarInWeek(userId);
        lbCalInDay.setValue(String.valueOf(cToday));
        lbCalInTomorow.setValue(String.valueOf(cTomorow));
        lbCalInWeek.setValue(String.valueOf(cInWeek));
    }

    private void onCountFiles() {
        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            lbFileXnk.setValue(String
                    .valueOf(getCountXNK(Constants.MENU_TYPE.WAITPROCESS_STR)));
            lbFileTbi.setValue(String
                    .valueOf(getCountTbi(Constants.MENU_TYPE.WAITPROCESS_STR)));
            lbFileXnn.setValue(String
                    .valueOf(getCountXnn(Constants.MENU_TYPE.WAITPROCESS_STR)));
        } else {
            DepartmentDAOHE depDAO = new DepartmentDAOHE();
            Department deptCQKT = depDAO.getByCode("CCQKT");
            Department dept = depDAO.findById(user.getDeptId());
            Department deptTTB = depDAO.findById(Constants.VU_TBYT_ID);
            Department deptp = depDAO.findById(dept.getParentId());
            Long rs = 3L;
            if (Constants.VU_TBYT_ID.equals(dept.getDeptId()) || Constants.VU_TBYT_ID.equals(dept.getParentId()) || deptTTB.getParentId().equals(user.getDeptId())) {
                rs = 1L;
                divATTP.setVisible(false);
                divXNN.setVisible(false);
            }
            if (Constants.CUC_ATTP_XNN_ID.equals(dept.getDeptId()) || Constants.CUC_ATTP_XNN_ID.equals(dept.getParentId()) || (deptp != null && Constants.CUC_ATTP_XNN_ID.equals(deptp.getParentId()))) {
                rs = 2L;
                divTTB.setVisible(false);
            }
            if (deptCQKT != null && (deptCQKT.getDeptId().equals(dept.getDeptId()) || deptCQKT.getDeptId().equals(dept.getParentId()) || (deptp != null && deptCQKT.getDeptId().equals(deptp.getParentId())))) {
                rs = 4L;
                divTTB.setVisible(false);
                divXNN.setVisible(false);
            }
            if (rs != 1L) {
                lbFileAll.setValue(String
                        .valueOf(getCountXNK(Constants.MENU_TYPE.WAITPROCESS_STR)));
                lbFileMain.setValue(String
                        .valueOf(getCountXNK(Constants.MENU_TYPE.PROCESSING_STR)));
                lbFileCoop.setValue(String
                        .valueOf(getCountXNK(Constants.MENU_TYPE.PROCESSED_STR)));
                if (rs != 4L) {
                    lbFileAllXnn.setValue(String
                            .valueOf(getCountXnn(Constants.MENU_TYPE.WAITPROCESS_STR)));
                    lbFileMainXnn.setValue(String
                            .valueOf(getCountXnn(Constants.MENU_TYPE.PROCESSING_STR)));
                    lbFileCoopXnn.setValue(String
                            .valueOf(getCountXnn(Constants.MENU_TYPE.PROCESSED_STR)));
                }
            }
            if (rs != 2L) {
                lbFileAllTbi.setValue(String
                        .valueOf(getCountTbi(Constants.MENU_TYPE.WAITPROCESS_STR)));
                lbFileMainTbi.setValue(String
                        .valueOf(getCountTbi(Constants.MENU_TYPE.PROCESSING_STR)));
                lbFileCoopTbi.setValue(String
                        .valueOf(getCountTbi(Constants.MENU_TYPE.PROCESSED_STR)));
                //linhdx 20160413 Yeu cau moi
                lbGroupTtbAll.setValue(String.valueOf(getCountGroupTtb(null)));
                List<Long> lstStatusAccept = new ArrayList();
                lstStatusAccept.add(Constants.TtbStatus.statusAccept_Vanthubanhanh);
                lbGroupTtbAccept.setValue(String.valueOf(getCountGroupTtb(lstStatusAccept)));

                List<Long> lstStatusReject = new ArrayList();
                lstStatusReject.add(Constants.TtbStatus.statusReject_ThukyguiCVngoaidanhmuc);
                lstStatusReject.add(Constants.TtbStatus.statusReject_Vanthubanhanhtuchoi);
                lbGroupTtbReject.setValue(String.valueOf(getCountGroupTtb(lstStatusReject)));
                lbGroupTtbMeetingReport.setValue(String.valueOf(getCountMeetingTtb()));

            }
        }
    }

    @Listen("onVisible = #homeWnd")
    public void onVisible() {
        homeWnd.setVisible(true);
    }

    private int getCountTbi(String menuType) {

        SearchDeviceModel searchModel = new SearchDeviceModel();

        int take = 0;
        int start = 10;
        searchModel.setMenuTypeStr(menuType);

        ListImportDeviceDAO objDAOHE = new ListImportDeviceDAO();
        PagingListModel plm;

        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(),
                    start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel,
                    getUserId(), getDeptId(), start, take);
        }

        return plm.getCount();
    }

    private int getCountXnn(String menuType) {

        SearchModel searchModel = new SearchModel();

        int take = 0;
        int start = 10;
        searchModel.setMenuTypeStr(menuType);

        RapidTestDAOHE objDAOHE = new RapidTestDAOHE();
        PagingListModel plm;

        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(),
                    start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel,
                    getUserId(), getDeptId(), start, take);
        }

        return plm.getCount();
    }

    private int getCountXNK(String menuType) {

        SearchModel searchModel = new SearchModel();

        int take = 0;
        int start = 10;
        searchModel.setMenuTypeStr(menuType);

        ImportOrderFileDAO objDAOHE = new ImportOrderFileDAO();
        PagingListModel plm;

        if (Constants.USER_TYPE.ENTERPRISE_USER.equals(user.getUserType())) {
            plm = objDAOHE.findFilesByCreatorId(searchModel, getUserId(),
                    start, take);
        } else {
            plm = objDAOHE.findFilesByReceiverAndDeptId(searchModel,
                    getUserId(), getDeptId(), start, take);
        }

        return plm.getCount();
    }

    private int getCountGroupTtb(List<Long> lstStatus) {
        ListImportDeviceDAO objDAOHE = new ListImportDeviceDAO();
        int count = objDAOHE.getCountGroupTtb(lstStatus);
        return count;
    }

    private Long getCountMeetingTtb() {
        IdMeetingDAO objDAOHE = new IdMeetingDAO();
        Long count = objDAOHE.getCountMeetingTtb();
        return count;
    }

}
