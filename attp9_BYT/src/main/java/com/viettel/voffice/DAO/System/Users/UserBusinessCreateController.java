/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Users;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.core.user.BO.Users;
import com.viettel.utils.EncryptDecryptUtils;
import com.viettel.voffice.BO.Business;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.sys.DAO.PlaceDAOHE;
import com.viettel.core.sys.DAO.RegisterDAOHE;
import com.viettel.core.user.BO.RoleUserDept;
import com.viettel.core.user.DAO.RoleUserDeptDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.utils.Constants_Cos;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Register;
import com.viettel.voffice.DAOHE.BusinessDAOHE;
import com.viettel.ws.SendEmailSms;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author HaVM2
 */
public class UserBusinessCreateController extends BaseComposer {

    @Wire
    Window createUserBusiness;
    Window parentWindow;
    @Wire
    private Image avatar;
    private Media media;
    private List listRegister;
    private Long id;
    private Register register;
    private String sTinh, sHuyen, sXa, sLoaiHinh, sChucVu;
    private String password;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map<String, Object>) Executions
                .getCurrent().getArg();
        id = (Long) arguments.get("id");
        RegisterDAOHE obj = new RegisterDAOHE();
        register = obj.findViewByFileId(id);
        parentWindow = (Window) arguments.get("parentWindow");
        CategoryDAOHE cdhe = new CategoryDAOHE();
//        PlaceDAOHE objhe = new PlaceDAOHE();
        sTinh = cdhe.LayTruongPlace_Name(Long.parseLong(register.getBusinessProvince()));
        sHuyen = cdhe.LayTruongPlace_Name(Long.parseLong(register.getBusinessDistrict()));
        sXa = cdhe.LayTruongPlace_Name(Long.parseLong(register.getBusinessTown()));
        sLoaiHinh = register.getBusinessTypeName();
        sChucVu = register.getPosName();
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    @Listen("onClick = #btnPheDuyet")
    public void onClickPheDuyet() {
        String message = String.format(Constants.Notification.APROVE_CONFIRM,
                Constants.DOCUMENT_TYPE_NAME.ACCOUNT);

        Messagebox.show(message, "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event event) {
                        if (null != event.getName()) {
                            switch (event.getName()) {
                                case Messagebox.ON_OK:
                                    createAcount();
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                    }
                });

    }

    private void createAcount() {
        try {
            //Lưu tai khoan vao bang user
            //update trang thai bang Register                                                     

            RegisterDAOHE registerDAOHE = new RegisterDAOHE();
            Register reg = registerDAOHE.findViewByFileId(id);
            reg.setStatus(Constants.Status.ACTIVE);
            registerDAOHE.saveOrUpdate(reg);

            // tao bang bussiness
            BusinessDAOHE busDAOHE = new BusinessDAOHE();
            Business bus = new Business();
            bus.setBusinessName(reg.getBusinessNameVi());
            bus.setBusinessNameEng(reg.getBusinessNameEng());
            bus.setBusinessNameAlias(reg.getBusinessNameAlias());
            bus.setBusinessTaxCode(reg.getBusinessTaxCode());
            bus.setBusinessTypeName(reg.getBusinessTypeName());
            bus.setBusinessTypeId(reg.getBusinessTypeId());
            bus.setBusinessLicense(reg.getBusinessLicense());
            bus.setBusinessProvince(reg.getBusinessProvince());
            bus.setBusinessAddress(reg.getBusinessAdd());
            bus.setBusinessTelephone(reg.getBusinessTelephone());
            bus.setBusinessFax(reg.getBusinessFax());
            bus.setBusinessWebsite(reg.getBusinessWebsite());
            bus.setGoverningBody(reg.getGoverningBody());
            bus.setManageEmail(reg.getManageEmail());
            bus.setBusinessTown(reg.getBusinessTown());
            bus.setBusinessDistrict(reg.getBusinessDistrict());
            bus.setBusinessEstablishYear(reg.getBusinessEstablishYear());
            bus.setIsActive(Constants.Status.ACTIVE);
            busDAOHE.saveOrUpdate(bus);
            //Tao bang user
            UserDAOHE userDAOHE = new UserDAOHE();
            Users user = new Users();
            user.setUserType(Constants.USER_TYPE.ENTERPRISE_USER);
            user.setUserName(reg.getBusinessTaxCode());
            //chucvu
            user.setPosId(reg.getPosId());
            user.setPosName(reg.getPosName());
//            //set password mac dinh
            String userName = reg.getBusinessTaxCode().toLowerCase().trim();
            //String password = 
            user.setPassword(EncryptDecryptUtils.encrypt(userName + password));
            //user.setPassword(EncryptDecryptUtils.encrypt(password.toString()));
            user.setStatus(1L);
            user.setFullName(reg.getUserFullName());
            user.setEmail(reg.getUserEmail());
            user.setTelephone(reg.getUserMobile());
            user.setBusinessId(bus.getBusinessId());
            user.setBusinessName(bus.getBusinessName());
            userDAOHE.saveOrUpdate(user);
//            //Tao bang role_user_dept
            RoleUserDeptDAOHE roleUserDeptDAOHE = new RoleUserDeptDAOHE();
            RoleUserDept roleUserDept = new RoleUserDept();
            roleUserDept.setIsActive(1L);
            roleUserDept.setUserId(user.getUserId());
            roleUserDept.setRoleId(Constants_Cos.ROLE_ID.QLD_DN);
            roleUserDeptDAOHE.saveOrUpdate(roleUserDept);
            createUserBusiness.onClose();
            Events.sendEvent("onVisible", parentWindow, null);
            showNotification("Phê duyệt thành công", Constants.Notification.INFO);

            SendEmailSms ses = new SendEmailSms();
            String msge = "Kính gửi: Doanh nghiệp " + reg.getBusinessNameVi()
                    + " - Có mã số thuế: " + reg.getBusinessTaxCode()
                    + "<br//>Tài khoản của quý khách đã được kích hoạt. "
                    + "<br//>Tài khoản đăng nhập: " + reg.getBusinessTaxCode() + " - Mật khẩu mặc định: " + password.toString()
                    //+ "<br//>Quý khách có thể truy cập vào hệ thống theo đường dẫn: <a href=//\"http://www.congbomypham.cqldvn.gov.vn:8080//cosmetic/\">congbomypham.cqldvn.gov.vn<//a> và xem tài liệu hướng dẫn sử dụng phần mềm."
                    + "<br//"
                    + "<br//>Trân trọng,"
                    + "<br//>Cục QLD";
            ses.sendEmailManual("Thong bao trang thai phe duyet tai khoan", user.getEmail(), msge);
        } catch (Exception e) {
            LogUtils.addLogDB(e);
            showNotification("Phê duyệt không thành công", Constants.Notification.ERROR);
        } finally {
        }
    }

    @Listen("onClick = #btnTuChoi")
    public void onClickTuChoi() {
        Map<String, Object> arguments = new ConcurrentHashMap<>();
        arguments.put("parentWindow", createUserBusiness);
        arguments.put("crudMode", "CREATE");
        arguments.put("id", id);
        createWindow("windowcreateUserBusiness", "/Pages/admin/user/userdialogTuChoi.zul",
                arguments, Window.MODAL);
    }

    @Listen("onClose = #createUserBusiness")
    public void onClose() {
        createUserBusiness.onClose();
        Events.sendEvent("onVisible", parentWindow, null);
    }

    @Listen("onVisible = #createUserBusiness")
    public void onVisible() {
        Events.sendEvent("onVisible", parentWindow, null);
    }

    public boolean CheckVisibleButton() {
        if (register.getStatus() == Constants.Status.ACTIVE || register.getStatus() == Constants.Status.DELETE) {
            return false;
        }
        return true;
    }

    public Register getRegister() {
        return register;
    }

    public String getsTinh() {
        return sTinh;
    }

    public String getsHuyen() {
        return sHuyen;
    }

    public String getsXa() {
        return sXa;
    }

    public String getsLoaiHinh() {
        return sLoaiHinh;
    }

    public String getsChucVu() {
        return sChucVu;
    }

    public String getPassword() throws UnsupportedEncodingException {
//         password = PublicFunctionModel.RandomNumber();
        if (!register.getStatus().equals(Constants.Status.ACTIVE)) {
//            password = ResourceBundleUtil.getString("passworDefault", "config");
            password = StringUtils.randomstring();
        }
        return password;
    }
}
