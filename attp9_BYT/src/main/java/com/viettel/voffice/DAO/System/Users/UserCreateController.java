/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Users;

import com.mchange.v2.c3p0.impl.C3P0Defaults;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.BO.Position;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.core.user.DAO.PositionDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.utils.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author HaVM2
 */
public class UserCreateController extends BaseComposer {

    @Wire
    Textbox txtUserId, txtFullName, txtUserName, txtPassword, txtReTypePassword, txtEmail,
            txtTelephone, txtDeptName, txtDeptId, txtStaffCode, txtIDNumber;
    @Wire
    Datebox txtBirthday;
    @Wire
    Listbox cbPosition, cbUserType;
    @Wire
    Window showDeptDlg, createDlg;
    @Wire
    Radio radioMan,radioWoman;
    @Wire
    private Image avatar;
    private Media media;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        PositionDAOHE pdhe = new PositionDAOHE();
        List lstPosData;
        if ("admin".equals(getUserName())) {
            lstPosData = pdhe.getSelectPosition(null);
        } else {
            lstPosData = pdhe.getSelectPosition(getDeptId());
        }
        ListModelArray lstPosition = new ListModelArray(lstPosData);
        cbPosition.setModel(lstPosition);
        cbPosition.renderAll();
        cbPosition.setSelectedIndex(0);

        loadItemForEdit();
    }

    private void loadItemForEdit() {
        Users u = (Users) Executions.getCurrent().getArg().get("user");
        if (u == null) {
            return;
        }
        avatar.setSrc(ResourceBundleUtil.getString("dir_avartar") + u.getUserId() + "?time=" + u.getRandom());
        txtUserId.setValue(u.getUserId().toString());
        txtFullName.setValue(u.getFullName() == null ? "" : u.getFullName().trim());
        txtUserName.setValue(u.getUserName() == null ? "" : u.getUserName().trim());
        txtPassword.setVisible(false);
        txtReTypePassword.setVisible(false);
        if (u.getEmail() == null) {
            txtEmail.setValue("");
        } else {
            txtEmail.setValue(u.getEmail().trim());
        }
        txtTelephone.setValue(u.getTelephone() == null ? "" : u.getTelephone().trim());
        txtDeptName.setValue(u.getDeptName() == null ? "" : u.getDeptName());
        txtDeptId.setValue(u.getDeptId() == null ? "" : u.getDeptId().toString());
        txtStaffCode.setValue(u.getStaffCode() == null ? "" : u.getStaffCode().trim());
        if (u.getIdNumber() != null) {
            txtIDNumber.setValue(u.getIdNumber().trim());
        }
        txtBirthday.setValue(u.getBirthday());

        for (int i = 0; i < cbPosition.getListModel().getSize(); i++) {
            Position pos = (Position) cbPosition.getListModel().getElementAt(i);
            if (pos.getPosId().equals(u.getPosId())) {
                cbPosition.setSelectedIndex(i);
                break;
            }
        }
        if(u.getGender() == 1L){
            radioMan.setChecked(true);
            radioWoman.setChecked(false);
        }else{
            radioMan.setChecked(false);
            radioWoman.setChecked(true);
        }

        for (int i = 0; i < cbUserType.getItems().size(); i++) {
            Long userType = Long.parseLong((String) cbUserType.getItems().get(i).getValue());
            if (userType.equals(u.getUserType())) {
                cbUserType.setSelectedIndex(i);
                break;
            }
        }

        Clients.evalJavaScript("hidePasswordRows();");

    }

    @Listen("onClick=#btnShowDept")
    public void onOpenDeptSelect() {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("idOfDisplayNameComp", "/createDlg/txtDeptName");
        args.put("idOfDisplayIdComp", "/createDlg/txtDeptId");

        showDeptDlg = (Window) Executions.createComponents("/Pages/admin/user/userDept.zul", null, args);
        showDeptDlg.doModal();
    }

    private boolean validate() throws Exception {
        if (txtFullName.getValue().isEmpty()) {
            txtFullName.focus();
            throw new Exception("Họ và tên không được để trống");
        }
        if (txtUserName.getValue().isEmpty()) {
            txtUserName.focus();
            throw new Exception("Tên đăng nhập không được để trống");
        }
        if (txtEmail.getValue() != null && txtEmail.getValue().trim().length() > 0) {
            boolean bValidateEmail = StringUtils.checkEmail(txtEmail.getValue().trim());
            if (!bValidateEmail) {
                txtEmail.focus();
                throw new Exception("Email không đúng định dạng");
            }
        }

        if (txtBirthday.getValue() != null) {
            Date birthday = txtBirthday.getValue();
            if (birthday.after(new Date())) {
                txtBirthday.focus();
                throw new Exception("Ngày sinh phải diễn ra trước thời điểm hiện tại");
            }
        }

        if (txtUserId.getValue() == null || txtUserId.getValue().length() == 0) {
            if (txtPassword.getValue() == null || txtPassword.getValue().trim().length() == 0) {
                txtPassword.focus();
                throw new Exception("Mật khẩu không được để trống");
            } else if (txtReTypePassword.getValue() == null) {
                txtReTypePassword.focus();
                throw new Exception("Mật khẩu gõ lại để trống");
            } else if (!txtPassword.getValue().equals(txtReTypePassword.getValue())) {
                txtReTypePassword.focus();
                throw new Exception("Mật khẩu gõ lại không trùng");
            }

            String password = txtPassword.getValue().trim();
            if (!password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
                Messagebox.show("Mật khẩu nhập không phải là mật khẩu mạnh, bạn có muốn tiếp tục",
                        "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
                        Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {

                            @Override
                            public void onEvent(Event e) {
                                if (null != e.getName()) {
                                    switch (e.getName()) {
                                        case Messagebox.ON_OK:
                                            onSaveUser();
                                            break;
                                        case Messagebox.ON_NO:
                                            break;
                                    }
                                }
                            }
                        });
                return false;

            }
        }

        return true;
    }

    private void onSaveUser() {
        boolean error = false;
        try {


            UserDAOHE udhe = new UserDAOHE();
            Users user;
            if (txtUserId.getValue() != null && !txtUserId.getValue().isEmpty()) {
                user = udhe.findById(Long.parseLong(txtUserId.getValue()));
                //
                // Cap nhat log truoc khi them moi
                //
                addLog(Constants.ACTION.TYPE.UPDATE, Constants.ACTION.NAME.UPDATE,
                        user.getUserId(), Constants.OBJECT_TYPE.USER, user.getUserName(), user);

            } else {
                String userName = txtUserName.getValue();
                String password = txtPassword.getValue();
                String saltPassword = EncryptDecryptUtils.encrypt(userName
                        + password);
                user = new Users();
                user.setPassword(saltPassword);
            }

            Long deptIdChild = null;
            if (txtDeptId.getValue() != null && txtDeptId.getValue().length() > 0) {
                deptIdChild = (Long.parseLong(txtDeptId.getValue()));
            } 
            Long deptIdParent = getDeptId();
            Long userIdParent = getUserId();
            Long userIdChild = null;
            if(txtUserId.getValue() != null && !txtUserId.getValue().isEmpty()){
                userIdChild = Long.parseLong(txtUserId.getValue());
            }
            boolean checkRole = udhe.checkRole(userIdParent, deptIdParent, userIdChild, deptIdChild);
            if (!checkRole) {
                showNotification("Bạn không có quyền thao tác");
                return;
            }
            user.setFullName(txtFullName.getValue().trim());
            user.setUserName(txtUserName.getValue().trim());
            user.setEmail(txtEmail.getValue().trim());
            user.setTelephone(txtTelephone.getValue().trim());
            if (txtDeptId.getValue() != null && txtDeptId.getValue().length() > 0) {
                user.setDeptId(Long.parseLong(txtDeptId.getValue()));
            } else {
                user.setDeptId(null);
            }
            user.setDeptName(txtDeptName.getValue());
            user.setPosId((Long) cbPosition.getSelectedItem().getValue());
            if (user.getPosId() != null && user.getPosId() > 0l) {
                user.setPosName(cbPosition.getSelectedItem().getLabel());
            }
            user.setStaffCode(txtStaffCode.getValue().trim());
            Long sex = 1L;
            if(radioWoman.isChecked()){
                sex = 0L;
            }
            user.setGender(sex);
            //linhdx: add them loai nguoi dung: Quan tri hay thuong
            user.setUserType(Long.parseLong((String) cbUserType.getSelectedItem().getValue()));
            user.setIdNumber(txtIDNumber.getValue().trim());
            user.setBirthday(txtBirthday.getValue());
            user.setAvartarPath(ResourceBundleUtil.getString("dir_avartar"));
            user.setStatus(1l);

            //linhdx
            //Kiem tra co phai user quan tri:  
            //Neu la user quan tri moi cho phep tao user quan tri tiep theo
            //Neu la admin thi them binh thuong
            //Hoac neu la user thuong thi chi duoc them nguoi dung moi la user thuong
            Boolean isAdmin = checkIsAdmin(getUserId());
            if (isAdmin || (!isAdmin && !Objects.equals(user.getUserType(), Constants.USER_TYPE.ADMIN))) {

                udhe.createOrUpdate(user);
                udhe.flush();

                if (avatar != null && avatar.getContent() != null) {
                    InputStream fis = avatar.getContent().getStreamData();
                    AttachDAOHE adhe = new AttachDAOHE();
                    adhe.saveAvatar(fis, user.getUserId().toString());
                }
                if (txtUserId.getValue() == null || txtUserId.getValue().length() == 0) {
                    addLog(Constants.ACTION.TYPE.INSERT, Constants.ACTION.NAME.INSERT,
                            user.getUserId(), Constants.OBJECT_TYPE.USER, user.getUserName());
                }
            } else {
                //linhdx neu khong phai admin thi chi them duoc user thuong
                showNotification("Bạn không phải là quản trị nên không được tạo user quản trị.", Constants.Notification.ERROR);
                error = true;
            }

        } catch (Exception en) {

            showNotification(en.getMessage(), Constants.Notification.ERROR);
            LogUtils.addLogDB(en);
            error = true;
        }
        if (!error) {
            showNotification("Cập nhật thành công", Constants.Notification.INFO);
            Window parentWnd = (Window) Path.getComponent("/userManWindow");
            Events.sendEvent(new Event("onReload", parentWnd, null));
            if (txtUserId.getValue() != null && txtUserId.getValue().length() > 0) {
                //
                // cap nhat thi dong lai
                //
                createDlg.detach();
            } else {
                //
                // them moi thi reload lai form
                //
                txtFullName.setValue("");
                txtUserName.setValue("");
                txtPassword.setValue("");
                txtReTypePassword.setValue("");
                txtTelephone.setValue("");
                txtStaffCode.setValue("");
                txtIDNumber.setValue("");
                txtEmail.setValue("");
            }
        }
    }

    private Boolean checkIsAdmin(Long userId) {
        UserDAOHE userDAOHE = new UserDAOHE();
        return userDAOHE.checkIsAdmin(userId);
    }

    @Listen("onClick=#btnCreate")
    public void onCreate() throws Exception {
        try {
            if (validate()) {
                onSaveUser();
            }
        } catch (Exception en) {
            LogUtils.addLogDB(en);
            showNotification(en.getMessage(), Constants.Notification.ERROR);
        }
    }

    @Listen("onClick=#btnClose")
    public void onClose() {
        createDlg.detach();
    }

    @Listen("onUpload = #btnUpload")
    public void handle(UploadEvent evt) throws IOException {

        media = evt.getMedia();
        if (media instanceof org.zkoss.image.Image) {
            avatar.setContent((org.zkoss.image.Image) media);
        } else {
            Messagebox.show("Không phải ảnh: " + media, "Lỗi", Messagebox.OK, Messagebox.ERROR);
            return;
        }

        //org.zkoss.image.Image test = (org.zkoss.image.Image) media;
    }
}
