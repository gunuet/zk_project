/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Users;

import java.util.Date;
import java.util.Random;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.EncryptDecryptUtils;
import com.viettel.utils.LogUtils;
import com.viettel.ws.SendEmailSms;

/**
 * 
 * @author HaVM2
 */
public class UserGetPassController extends BaseComposer {

	@Wire
	Textbox txtUserName, txtEmail;
	@Wire
	Window resetPassDlg;
	Users u;
	UserDAOHE udhe;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}

	private void updatePassword() {
		try {

			Random rnd = new java.security.SecureRandom();
			int randomInt = rnd.nextInt(1000000);
			String pass = EncryptDecryptUtils.encrypt(String.valueOf(randomInt)).substring(0,6).toLowerCase();
			
			udhe = new UserDAOHE();
			
			u = udhe.getUserByName(txtUserName.getValue());

			u.setForgotPassword(EncryptDecryptUtils.encrypt(u.getUserName()
					.toLowerCase() + pass));

			Date d = new Date();
			u.setForgotPasswordExpire(d);

			udhe.update(u);
			
			SendEmailSms ses = new SendEmailSms();
			String msge = "Kính gửi: "
					+ u.getUserName()
					+ "<br//> Mật khẩu của bạn đã được reset là " + pass
					+ "<br//> Sau khi đăng nhập thành công đề nghị bạn đổi mật khẩu mới.";
			ses.sendEmailManual(
					"Mat khau cua user " + u.getUserName(), u.getEmail(),
					msge);

			showSuccessNotification("Cập nhật thành công");
			
		/*	addLog(Constants.ACTION.TYPE.UPDATE, Constants.ACTION.NAME.UPDATE,
					u.getUserId(), Constants.OBJECT_TYPE.USER,
					"Lấy mật khẩu account " + u.getUserName());
*/			
			resetPassDlg.detach();

		} catch (Exception en) {
			LogUtils.addLogDB(en);
			showNotification("Cập nhật không thành công");
		}
	}

	@Listen("onClick=#btnSavePassword")
	public void savePassword() {
		try {

			if (txtUserName.getValue().trim().length() == 0) {
				showNotification("Chưa nhập tên đăng nhập");
				txtUserName.focus();
				return;
			}

			if (txtEmail.getValue().trim().length() == 0) {
				showNotification("Chưa nhập email");
				txtEmail.focus();
				return;
			}
			
			udhe = new UserDAOHE();

			u = udhe.getUserByName(txtUserName.getValue());

			if (u != null) {
				if (!txtEmail.getValue().trim().equals(u.getEmail())) {
					showNotification("Email không chính xác");
					txtEmail.focus();
					return;
				}
			} else {
				showNotification("Tên đăng nhập không chính xác");
				txtUserName.focus();
				return;
			}

			Messagebox.show("Bạn có muốn lấy lại mật khẩu không", "Xác nhận",
					Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
					new org.zkoss.zk.ui.event.EventListener() {

						@Override
						public void onEvent(Event e) {
							if (null != e.getName()) {
								switch (e.getName()) {
								case Messagebox.ON_OK:
									updatePassword();
									break;
								case Messagebox.ON_NO:
									break;
								}
							}
						}
					});

		} catch (Exception ex) {
			LogUtils.addLogDB(ex);
			showNotification("Cập nhật không thành công");
		}
	}
}
