/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Users;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.core.user.BO.Users;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.user.DAO.UserDAOHE;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author HaVM2
 */
public class UserResetPassController extends BaseComposer {

    @Wire
    Textbox txtCurrentPassword, txtPassword, txtReTypePassword;
    @Wire
    Window resetPassDlg;
    Long userId;
    Long type;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        userId = (Long) Executions.getCurrent().getArg().get("userId");
        type = (Long) Executions.getCurrent().getArg().get("type");
    }

    private void updatePassword() {
        try {

            UserDAOHE udhe = new UserDAOHE();
            Users u = udhe.findById(userId);
            String password = txtPassword.getValue().trim();
            if (!password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
                showNotification("Mật khẩu nhập không phải là mật khẩu mạnh", Constants.Notification.ERROR);
                txtPassword.focus();
            } else {
                udhe.updatePassword(userId, txtPassword.getValue());
                showSuccessNotification("Cập nhật thành công");
                resetPassDlg.detach();
                addLog(Constants.ACTION.TYPE.UPDATE, Constants.ACTION.NAME.UPDATE,
                        userId, Constants.OBJECT_TYPE.USER, "Đổi mật khẩu acci" + u.getUserName());
            }

        } catch (Exception en) {
            LogUtils.addLogDB(en);
            showNotification("Cập nhật không thành công");
        }
    }

    @Listen("onClick=#btnSavePassword")
    public void savePassword() {
        try {

            if (type != null) {
                if (txtCurrentPassword.getValue().trim().length() == 0) {
                    showNotification("Chưa nhập mật khẩu cũ");
                    txtCurrentPassword.focus();
                    return;
                }

                UserDAOHE udhe = new UserDAOHE();
                Users u = udhe.findById(userId);

                String strLogin = udhe.checkLogin(u.getUserName(), txtCurrentPassword.getValue());
                if (!"".equals(strLogin)) {
                    showNotification("Mật khẩu cũ không chính xác");
                    txtCurrentPassword.focus();
                    return;
                }
                if (txtCurrentPassword.getValue().equals(txtPassword.getValue())) {
                    showNotification("Mật khẩu mới phải khác mật khẩu cũ", Constants.Notification.ERROR);
                    txtPassword.focus();
                    return;
                }
            }

            if (txtPassword.getValue().trim().length() == 0) {
                showNotification("Chưa nhập mật khẩu mới", Constants.Notification.ERROR);
                txtPassword.focus();
                return;
            }
            if (txtReTypePassword.getValue().trim().length() == 0) {
                showNotification("Chưa xác nhận mật khẩu mới", Constants.Notification.ERROR);
                txtReTypePassword.focus();
                return;
            }

            if (!txtPassword.getValue().equals(txtReTypePassword.getValue())) {
                showNotification("Mật khẩu gõ lại không trùng với mật khẩu mới", Constants.Notification.ERROR);
                txtPassword.focus();
                return;
            } else {
                updatePassword();
            }
//                String password = txtPassword.getValue().trim();
//                if (!password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
//                    Messagebox.show("Mật khẩu nhập không phải là mật khẩu mạnh, bạn có muốn tiếp tục",
//                            "Xác nhận", Messagebox.OK | Messagebox.CANCEL,
//                            Messagebox.QUESTION,
//                            new org.zkoss.zk.ui.event.EventListener() {
//
//                                @Override
//                                public void onEvent(Event e) {
//                                    if (null != e.getName()) {
//                                        switch (e.getName()) {
//                                            case Messagebox.ON_OK:
//                                                updatePassword();
//                                                break;
//                                            case Messagebox.ON_NO:
//                                                break;
//                                        }
//                                    }
//                                }
//                            });
//
//                } else {
//                    updatePassword();
//                }
//            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Cập nhật không thành công");
        }
    }

    @Listen("onClick=#btnRestorePassword")
    public void restorePassword() throws Exception {
        UserDAOHE udhe = new UserDAOHE();
        boolean isOk = udhe.restorePass(userId);
        if (isOk) {
            showSuccessNotification("Cập nhật thành công");
        } else {
            showSuccessNotification("Lỗi cập nhật");
        }
    }

}
