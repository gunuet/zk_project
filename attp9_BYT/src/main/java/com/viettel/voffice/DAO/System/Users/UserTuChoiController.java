/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Users;

import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.BO.Position;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.core.user.DAO.PositionDAOHE;
import com.viettel.core.sys.DAO.RegisterDAOHE;
import com.viettel.voffice.BO.Register;
import com.viettel.module.rapidtest.BO.VFileRtfile;
import com.viettel.voffice.model.SearchModel;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.databind.BindingListModelList;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author HaVM2
 */
public class UserTuChoiController extends BaseComposer {

    @Wire
    Window userdialogTuChoi;
    Window parentWindow;
    @Wire
    private Textbox tbDescription;
    private Long id;
    
    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        Map<String, Object> arguments = (Map<String, Object>) Executions
                .getCurrent().getArg();
        id = (Long) arguments.get("id");
        parentWindow = (Window) arguments.get("parentWindow");
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    @Listen("onClick =#userdialogTuChoi #btnTuChoiOK")
    public void onClickXacNhanTuChoi() {
        //update truong ly do
        RegisterDAOHE registerDAOHE = new RegisterDAOHE();
        Register reg = registerDAOHE.findViewByFileId(id);
        reg.setDescription(tbDescription.getValue());
        reg.setStatus(Constants.Status.DELETE);
        registerDAOHE.saveOrUpdate(reg);
        
        userdialogTuChoi.onClose();
      
        Events.sendEvent("onVisible", parentWindow, null);
          parentWindow.onClose();
        showNotification("Từ chối thành công", Constants.Notification.INFO);
        
        

    }

}
