/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Users;

import com.viettel.utils.DateTimeUtils;
import com.viettel.core.user.BO.Users;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.sys.BO.Place;
import com.viettel.core.sys.DAO.CategoryDAOHE;
import com.viettel.core.sys.DAO.PlaceDAOHE;
import com.viettel.core.sys.DAO.RegisterDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.module.rapidtest.DAO.RegisterController;
import com.viettel.utils.Constants;
import com.viettel.utils.Constants_XNN;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.ValidatorUtil;
import com.viettel.voffice.BO.Business;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BusinessDAOHE;
import com.viettel.voffice.model.PublicFunctionModel;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;

/**
 *
 * @author HaVM2
 */
public class UserViewController extends BaseComposer {

    @Wire
    Label lbFullName, lbUserName, lbEmail,
            lbTelephone, lbDeptName, lbStaffCode, lbIDNumber, lbBirthday, lbPosition, lbGender;
//    @Wire
//    Image avatar;
    @Wire
    Listbox lstTinh, lstHuyen, lstXa, lstLoaiHinhDN, lstChucVu;
    @Wire
    Textbox txtPassword, txtPasswordNew, txtPasswordConfirm, txtTenDayDu, txtTenDangNhap, txtSoDiDong, txtUserEmail,
            txtTenTiengViet, txtTenTiengAnh, txtTenVietTat, txtMaSoThue, txtDKKD, txtDiaChi, txtDienThoai, txtEmailDoanhNghiep,
            txtFax, txtWebsite, txtNamThanhLap, txtCoQuanChuQuan;

    @Wire
    Window userViewDlg;
    @Wire
    Div businessType, divAvatar, SystemType, divmain;
    @Wire
    Image avatar;
    private Media media;
    Long userId;
    Long businessId;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        loadItemForEdit();
    }

    private void loadItemForEdit() {
        Users u = (Users) Executions.getCurrent().getArg().get("user");
        if (u == null) {
            return;
        }
        //  avatar.setSrc(ResourceBundleUtil.getString("dir_avartar") + u.getUserId() + "?time=" + u.getRandom());
        userId = u.getUserId();
        lbFullName.setValue(u.getFullName() == null ? "" : u.getFullName().trim());
        lbUserName.setValue(u.getUserName() == null ? "" : u.getUserName().trim());
        if (u.getEmail() == null) {
            lbEmail.setValue("");
        } else {
            lbEmail.setValue(u.getEmail().trim());
        }
        lbTelephone.setValue(u.getTelephone() == null ? "" : u.getTelephone().trim());
        lbDeptName.setValue(u.getDeptName() == null ? "" : u.getDeptName());
        lbStaffCode.setValue(u.getStaffCode() == null ? "" : u.getStaffCode().trim());
        if (u.getIdNumber() != null) {
            lbIDNumber.setValue(u.getIdNumber().trim());
        }
        if (u.getBirthday() != null) {
            lbBirthday.setValue(DateTimeUtils.convertDateToString(u.getBirthday()));
        }
        lbPosition.setValue(u.getPosName());
        if (u.getGender() == null) {
            //
            // Chua chon gioi tinh
            //
            lbGender.setValue("");
        } else if (u.getGender() == 1l) {
            lbGender.setValue("Nam");
        } else {
            lbGender.setValue("Nữ");
        }

        CategoryDAOHE cdhe = new CategoryDAOHE();
        PlaceDAOHE phe = new PlaceDAOHE();
        List lstProvince = phe.findPlaceSearchBycode(Constants_XNN.PLACE.PROVINCE, Constants.PLACE.VNCODE);
        ListModelArray lstModelProvince = new ListModelArray(lstProvince);
        lstTinh.setModel(lstModelProvince);
        lstTinh.renderAll();
        lstTinh.setSelectedIndex(0);

        List lstObjects = cdhe.getSelectCategoryByType(Constants_XNN.CATEGORY_TYPE.BUSINESS_TYPE, "value");
        ListModelArray lstPosition = new ListModelArray(lstObjects);
        lstLoaiHinhDN.setModel(lstPosition);
        lstLoaiHinhDN.renderAll();
        lstLoaiHinhDN.setSelectedIndex(0);

        lstObjects = cdhe.getSelectCategoryByType(Constants_XNN.CATEGORY_TYPE.USER_TYPE, "value");
        lstPosition = new ListModelArray(lstObjects);
        lstChucVu.setModel(lstPosition);
        lstChucVu.renderAll();
        lstChucVu.setSelectedIndex(0);

        if (userId != null) {
            txtTenDayDu.setValue(u.getFullName() == null ? "" : u.getFullName().trim());
            txtTenDangNhap.setValue(u.getUserName() == null ? "" : u.getUserName().trim());
            txtSoDiDong.setValue(u.getTelephone() == null ? "" : u.getTelephone().trim());
            txtUserEmail.setValue(u.getEmail() == null ? "" : u.getEmail().trim());
            if (u.getPosId() != null) {
                for (int i = 0; i < lstChucVu.getListModel().getSize(); i++) {
                    Category ct = (Category) lstChucVu.getListModel().getElementAt(i);
                    if (u.getPosId().equals(ct.getCategoryId())) {
                        lstChucVu.setSelectedIndex(i);
                        break;
                    }
                }
            }
            businessId = u.getBusinessId();
            BusinessDAOHE busdao = new BusinessDAOHE();
            Business bus = busdao.findById(businessId);

            if (businessId != null) {
                if (bus.getBusinessTypeId() != null) {
                    for (int i = 0; i < lstLoaiHinhDN.getListModel().getSize(); i++) {
                        Category ct = (Category) lstLoaiHinhDN.getListModel().getElementAt(i);
                        if (bus.getBusinessTypeId().equals(ct.getCategoryId())) {
                            lstLoaiHinhDN.setSelectedIndex(i);
                            break;
                        }
                    }
                }

                if (bus.getBusinessProvince() != null) {

                    for (int i = 0; i < lstTinh.getListModel().getSize(); i++) {
                        Place ct = (Place) lstTinh.getListModel().getElementAt(i);
//                        String s = ct.getPlaceId().toString();
                        if (bus.getBusinessProvince().equals(ct.getPlaceId().toString())) {
                            lstTinh.setSelectedIndex(i);
                            break;
                        }
                    }
                }
                //list huyen
                List lstdistrict = phe.findAllPlaceSearch(Constants_XNN.PLACE.DISTRICT, Long.parseLong(bus.getBusinessProvince()));
                ListModelArray lstModelDistrict = new ListModelArray(lstdistrict);
                lstHuyen.setModel(lstModelDistrict);
                lstHuyen.renderAll();
                if (bus.getBusinessDistrict() != null) {
                    for (int i = 0; i < lstHuyen.getListModel().getSize(); i++) {
                        Place ct = (Place) lstHuyen.getListModel().getElementAt(i);
                        if (bus.getBusinessDistrict().equals(ct.getPlaceId().toString())) {
                            lstHuyen.setSelectedIndex(i);
                            break;
                        }
                    }
                }

                //list xa
                List lsttown = phe.findAllPlaceSearch(Constants_XNN.PLACE.VILLAGE, Long.parseLong(bus.getBusinessDistrict()));
                ListModelArray lstModelTown = new ListModelArray(lsttown);
                lstXa.setModel(lstModelTown);
                lstXa.renderAll();

                if (bus.getBusinessTown() != null) {
                    for (int i = 0; i < lstXa.getListModel().getSize(); i++) {
                        Place ct = (Place) lstXa.getListModel().getElementAt(i);
                        if (bus.getBusinessTown().equals(ct.getPlaceId().toString())) {
                            lstXa.setSelectedIndex(i);
                            break;
                        }
                    }
                }

                txtTenTiengViet.setValue(bus.getBusinessName() == null ? "" : bus.getBusinessName().trim());
                txtTenTiengAnh.setValue(bus.getBusinessNameEng() == null ? "" : bus.getBusinessNameEng().trim());
                txtTenVietTat.setValue(bus.getBusinessNameAlias() == null ? "" : bus.getBusinessNameAlias().trim());
                txtDKKD.setValue(bus.getBusinessLicense() == null ? "" : bus.getBusinessLicense().trim());
                txtDiaChi.setValue(bus.getBusinessAddress() == null ? "" : bus.getBusinessAddress().trim());
                txtDienThoai.setValue(bus.getBusinessTelephone() == null ? "" : bus.getBusinessTelephone().trim());
                txtFax.setValue(bus.getBusinessFax() == null ? "" : bus.getBusinessFax().trim());
                txtEmailDoanhNghiep.setValue(bus.getManageEmail() == null ? "" : bus.getManageEmail().trim());
                txtMaSoThue.setValue(bus.getBusinessTaxCode() == null ? "" : bus.getBusinessTaxCode().trim());
                txtWebsite.setValue(bus.getBusinessWebsite() == null ? "" : bus.getBusinessWebsite().trim());
                txtNamThanhLap.setValue(bus.getBusinessEstablishYear() == null ? "" : bus.getBusinessEstablishYear().trim());
                txtCoQuanChuQuan.setValue(bus.getGoverningBody() == null ? "" : bus.getGoverningBody().trim());
            }
            if (u.getUserType() == Constants_XNN.USER_TYPE.ENTERPRISE_USER) {
                divAvatar.setVisible(false);
                SystemType.setVisible(false);
            } else {
                businessType.setVisible(false);
                divmain.setWidth("450px");
                userViewDlg.setWidth("600px");
            }
        }
    }

    public void onSelectProvince() {
        if (lstTinh.getSelectedItem() != null) {
            int idx = lstTinh.getSelectedItem().getIndex();
            PlaceDAOHE phe = new PlaceDAOHE();
            if (idx > 0l) {
                Long id = (Long) lstTinh.getSelectedItem().getValue();
                List lstProvince = phe.findAllPlaceSearch(Constants_XNN.PLACE.DISTRICT, id);
                ListModelArray lstModelProvince = new ListModelArray(lstProvince);
                lstHuyen.setModel(lstModelProvince);
                lstHuyen.renderAll();
                lstXa.renderAll();
                lstXa.setSelectedIndex(0);
                lstHuyen.setSelectedIndex(0);
            } //an nut chon
            else {
                List lstProvince = phe.findAllPlaceSearch(Constants_XNN.PLACE.DISTRICT, -1L);
                ListModelArray lstModelProvince = new ListModelArray(lstProvince);
                lstHuyen.setModel(lstModelProvince);
                lstHuyen.renderAll();

                List lstTown = phe.findAllPlaceSearch(Constants_XNN.PLACE.VILLAGE, -1L);
                ListModelArray lstModelXa = new ListModelArray(lstTown);
                lstXa.setModel(lstModelXa);
                lstXa.renderAll();
                lstXa.setSelectedIndex(0);
                lstHuyen.setSelectedIndex(0);
            }
        }
    }

    public void onSelectDistrict() {
        if (lstHuyen.getSelectedItem() != null) {
            int idx = lstHuyen.getSelectedItem().getIndex();
            PlaceDAOHE phe = new PlaceDAOHE();
            if (idx > 0l) {
                Long id = (Long) lstHuyen.getSelectedItem().getValue();
                List lstProvince = phe.findAllPlaceSearch(Constants_XNN.PLACE.VILLAGE, id);
                ListModelArray lstModelProvince = new ListModelArray(lstProvince);
                lstXa.setModel(lstModelProvince);
                lstXa.renderAll();
                lstXa.setSelectedIndex(0);
            } else {
                List lstProvince = phe.findAllPlaceSearch(Constants_XNN.PLACE.VILLAGE, -1L);
                ListModelArray lstModelProvince = new ListModelArray(lstProvince);
                lstXa.setModel(lstModelProvince);
                lstXa.renderAll();
                lstXa.setSelectedIndex(0);
            }
        }
    }

    @Listen("onClick=#btnSaveInfo")
    public void Save() {
        //Cap nhat bang business
        try {
            if (isValidatedData()) {
                BusinessDAOHE busdao = new BusinessDAOHE();
                Business bus = busdao.findById(businessId);
                bus.setBusinessName(txtTenTiengViet.getValue());
                bus.setBusinessNameEng(txtTenTiengAnh.getValue());
                bus.setBusinessNameAlias(txtTenVietTat.getValue());
                bus.setBusinessLicense(txtDKKD.getValue());
                bus.setBusinessAddress(txtDiaChi.getValue());
                bus.setBusinessTelephone(txtDienThoai.getValue());
                bus.setBusinessFax(txtFax.getValue());
                bus.setManageEmail(txtEmailDoanhNghiep.getValue());
                bus.setBusinessWebsite(txtWebsite.getValue());
                bus.setBusinessEstablishYear(txtNamThanhLap.getValue());
                bus.setGoverningBody(txtCoQuanChuQuan.getValue());
                bus.setBusinessProvince(lstTinh.getSelectedItem().getValue().toString());
                bus.setBusinessDistrict(lstHuyen.getSelectedItem().getValue().toString());
                bus.setBusinessTown(lstXa.getSelectedItem().getValue().toString());
                bus.setBusinessTypeId(Long.parseLong(lstLoaiHinhDN.getSelectedItem().getValue().toString()));
                bus.setBusinessTypeName(lstLoaiHinhDN.getSelectedItem().getValue().toString());
                busdao.saveOrUpdate(bus);

                //cap nhat user
                UserDAOHE userdao = new UserDAOHE();
                Users users = userdao.findById(userId);
                users.setFullName(txtTenDayDu.getValue());
                users.setTelephone(txtSoDiDong.getValue());
                users.setEmail(txtUserEmail.getValue());
                users.setPosId(Long.parseLong(lstChucVu.getSelectedItem().getValue().toString()));
                users.setPosName(lstChucVu.getSelectedItem().getValue().toString());
                userdao.saveOrUpdate(users);
                showSuccessNotification("Cập nhật thành công");
                userViewDlg.detach();
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Cập nhật không thành công");
        }
    }

    private boolean isValidatedData() {
        String errorMsg;
        if ("-1".equals(lstLoaiHinhDN.getSelectedItem().getValue().toString())) {
            showNotification("Bạn phải chọn loại hình!");
            lstLoaiHinhDN.focus();
            return false;
        }
        if (PublicFunctionModel.validateBlank(txtTenTiengViet.getText())) {
            showNotification("Tên tiếng việt không thể để trống!");
            txtTenTiengViet.focus();
            return false;
        }

        if (txtDKKD.getText().matches("\\s*")) {
            showNotification("Số đăng ký KD không thể để trống!");
            txtDKKD.focus();
            return false;
        }
        if (PublicFunctionModel.validateNumber(txtDKKD.getText())) {
            showNotification("Số đăng ký kinh doanh phải là số!");
            txtDKKD.focus();
            return false;
        }
        if ("-1".equals(lstTinh.getSelectedItem().getValue().toString())) {
            showNotification("Bạn phải chọn Tỉnh/Thành phố!");
            lstTinh.focus();
            return false;
        }
        if (lstHuyen.getSelectedItem() != null) {
            int idx = lstHuyen.getSelectedItem().getIndex();
            if (idx <= 0) {
                showNotification("Bạn phải chọn Quận/Huyện!");
                lstHuyen.focus();
                return false;
            }
        } else {
            showNotification("Bạn phải chọn Quận/Huyện!");
            lstHuyen.focus();
            return false;
        }

        if ("-1".equals(lstXa.getSelectedItem().getValue().toString())) {
            showNotification("Bạn phải chọn Xã/Phường/Thị trấn!");
            lstXa.focus();
            return false;
        }
        if (txtDiaChi.getText().matches("\\s*")) {
            showNotification("Địa chỉ chi tiết không thể để trống!");
            txtDiaChi.focus();
            return false;
        }
        if (txtNamThanhLap.getText() != null) {
            if (PublicFunctionModel.validateNumber(txtNamThanhLap.getText())) {
                showNotification("Năm thành lập phải là số!");
                txtNamThanhLap.focus();
                return false;
            }
        }
        if (txtNamThanhLap.getText() != null) {
            Integer year = Calendar.getInstance().get(Calendar.YEAR);
            try {
                if (Integer.parseInt(txtNamThanhLap.getText()) > year) {
                    showNotification("Năm thành lập không thể lớn năm hiện tại");
                    txtNamThanhLap.focus();
                    return false;
                }
            } catch (WrongValueException e) {
                LogUtils.addLogDB(e);
                showNotification("Năm thành lập phải là kiểu số không lớn hơn năm hiện tại");
                txtNamThanhLap.focus();
                return false;
            }
        }

        if ((errorMsg = ValidatorUtil.validateTextbox(txtDienThoai, true, 31, ValidatorUtil.PATTERN_CHECK_PHONENUMBER, ValidatorUtil.PHONENUMBER_MIN_LENGTH)) != null) {
            showNotification(String.format(errorMsg, "Điện thoại doanh nghiệp"));
            txtDienThoai.focus();
            return false;
        }

        if ((errorMsg = ValidatorUtil.validateTextbox(txtFax, false, 31, ValidatorUtil.PATTERN_CHECK_PHONENUMBER, ValidatorUtil.PHONENUMBER_MIN_LENGTH)) != null) {
            showNotification(String.format(errorMsg, "Số Fax doanh nghiệp"));
            txtFax.focus();
            return false;
        }

        if ((errorMsg = ValidatorUtil.validateTextbox(txtEmailDoanhNghiep, true, 31, ValidatorUtil.PATTERN_CHECK_EMAIL)) != null) {
            showNotification(String.format(errorMsg, "Email doanh nghiệp"));
            txtEmailDoanhNghiep.focus();
            return false;
        }

        if (txtTenDayDu.getText()
                .matches("\\s*")) {
            showNotification("Họ và tên người đại diện không thể để trống!");
            txtTenDayDu.focus();
            return false;
        }

        if ((errorMsg = ValidatorUtil.validateTextbox(txtUserEmail, true, 31, ValidatorUtil.PATTERN_CHECK_EMAIL)) != null) {
            showNotification(String.format(errorMsg, "Email"));
            txtUserEmail.focus();
            return false;
        }

        if ("-1".equals(lstChucVu.getSelectedItem()
                .getValue().toString())) {
            showNotification("Bạn phải chọn chức vụ người đại diện!");
            lstChucVu.focus();
            return false;
        }

        if ((errorMsg = ValidatorUtil.validateTextbox(txtSoDiDong, true, 31, ValidatorUtil.PATTERN_CHECK_PHONENUMBER, ValidatorUtil.PHONENUMBER_MIN_LENGTH)) != null) {
            showNotification(String.format(errorMsg, "Số di động người đại diện"));
            txtSoDiDong.focus();
            return false;
        }

        return true;
    }

    private void updatePassword() {
        try {
            UserDAOHE udhe = new UserDAOHE();
            Users u = udhe.findById(userId);
            String password = txtPasswordNew.getValue().trim();
            if (!password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
                showNotification("Mật khẩu nhập không phải là mật khẩu mạnh", Constants.Notification.ERROR);
                txtPasswordNew.focus();
            } else {
            	udhe.updatePassword(userId, txtPasswordNew.getValue());
                showSuccessNotification("Cập nhật thành công");
                userViewDlg.detach();
                addLog(Constants.ACTION.TYPE.UPDATE, Constants.ACTION.NAME.UPDATE,
                        userId, Constants.OBJECT_TYPE.USER, "Đổi mật khẩu acci" + u.getUserName());
            }
        } catch (Exception en) {
            LogUtils.addLogDB(en);
            showNotification("Cập nhật không thành công");
        }
    }

    @Listen("onClick=#btnSavePassword")
    public void savePassword() {
        try {
            if (txtPassword.getValue().trim().length() == 0) {
                showNotification("Chưa nhập mật khẩu cũ");
                txtPassword.focus();
                return;
            }

            UserDAOHE udhe = new UserDAOHE();
            Users u = udhe.findById(userId);
            String strLogin = udhe.checkLogin(u.getUserName(), txtPassword.getValue());
            if (!"".equals(strLogin)) {
                showNotification("Mật khẩu cũ không chính xác");
                txtPassword.focus();
                return;
            }
            if (txtPasswordNew.getValue().trim().length() == 0) {
                showNotification("Chưa nhập mật khẩu mới", Constants.Notification.ERROR);
                txtPasswordNew.focus();
                return;
            }
            

            if (txtPasswordConfirm.getValue().trim().length() == 0) {
                showNotification("Chưa xác nhận mật khẩu mới", Constants.Notification.ERROR);
                txtPasswordConfirm.focus();
                return;
            }
            if (!txtPasswordNew.getValue().equals(txtPasswordConfirm.getValue())) {
                showNotification("Mật khẩu gõ lại không trùng với mật khẩu mới", Constants.Notification.ERROR);
                txtPassword.focus();
            } else {
            	updatePassword();
               
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            showNotification("Cập nhật không thành công");
        }
    }

    @Listen("onUpload = #btnUpload")
    public void handle(UploadEvent evt) throws IOException {
        media = evt.getMedia();
        if (media instanceof org.zkoss.image.Image) {
            avatar.setContent((org.zkoss.image.Image) media);
            InputStream fis = avatar.getContent().getStreamData();
            AttachDAOHE adhe = new AttachDAOHE();
            adhe.saveAvatar(fis, userId.toString());
        } else {
            Messagebox.show("Không phải ảnh: " + media, "Lỗi", Messagebox.OK, Messagebox.ERROR);
        }
    }

    public String getLabel(String key) {
        try {
            return ResourceBundleUtil.getString(key, "language_XNN_vi");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
}
