/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAO.System.Users;

import com.viettel.utils.Constants;
import com.viettel.core.user.model.UserBean;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.BO.VUsers;
import com.viettel.core.base.DAO.BaseComposer;
import com.viettel.core.user.DAO.PositionDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.utils.Constants_XNN;
import com.viettel.utils.LogUtils;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author HaVM2
 */
public class UsersController extends BaseComposer {

    @Wire
    Textbox txtSearchUserName, txtSearchFullName, txtSearchStaffCode, txtSearchDept, txtSearchDeptId, txtSearchTelephone;
    ListModelArray lstUser;
    @Wire
    Listbox lstItems;
    @Wire
    Listbox cbSearchPosition, cbUserType;
    ListModelArray lstPosition;
    @Wire
    Popup popup;
    @Wire
    Window createDlg, roleDlg, resetPassDlg, userManWindow;
    @Wire
    Paging userPagingBottom;
    UserBean searchForm;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        return super.doBeforeCompose(page, parent, compInfo);
    }

    @Override
    public void doAfterCompose(Component comp) {
        try {
            super.doAfterCompose(comp);

            PositionDAOHE pdhe = new PositionDAOHE();
            List lstPosData = pdhe.getSelectPosition(null);
            lstPosition = new ListModelArray(lstPosData);
            cbSearchPosition.setModel(lstPosition);

            ListModel infos = new ListModelArray(
                    new String[][]{
                        {Constants_XNN.USER_TYPE.ALL.toString(), "----Chọn----"},
                        {Constants_XNN.USER_TYPE.ADMIN.toString(), "Quản trị hệ thống"},
                        {Constants_XNN.USER_TYPE.NOT_ADMIN.toString(), "Cán bộ"},
                        {Constants_XNN.USER_TYPE.ENTERPRISE_USER.toString(), "Doanh nghiệp"}});
            cbUserType.setModel(infos);
            cbUserType.renderAll();
            cbUserType.setSelectedIndex(0);
            userPagingBottom.setActivePage(0);
            onSearch();
        } catch (Exception ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
            Clients.clearBusy();
        }
    }

    private void fillDataToList() {
        UserDAOHE udhe = new UserDAOHE();
        int take = userPagingBottom.getPageSize();
        int start = userPagingBottom.getActivePage() * userPagingBottom.getPageSize();
        PagingListModel plm = udhe.searchVUser(searchForm, start, take);
        userPagingBottom.setTotalSize(plm.getCount());
        if (plm.getCount() == 0) {
            userPagingBottom.setVisible(false);
        } else {
            userPagingBottom.setVisible(true);
        }
        ListModelArray lstModel = new ListModelArray(plm.getLstReturn());
        lstItems.setModel(lstModel);

    }

    @Listen("onPaging = #userPagingBottom")
    public void onPaging(Event event) {
        fillDataToList();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() throws IOException {

        Clients.showBusy("");
        try {
            userPagingBottom.setActivePage(0);
            userPagingBottom.setActivePage(0);
            searchForm = new UserBean();
            searchForm.setUserName(txtSearchUserName.getValue());
            searchForm.setFullName(txtSearchFullName.getValue());
            searchForm.setTelephone(txtSearchTelephone.getValue());
            searchForm.setStaffCode(txtSearchStaffCode.getValue());
            searchForm.setDeptName(txtSearchDept.getValue());
            if (txtSearchDeptId.getValue() != null && txtSearchDeptId.getValue().length() > 0) {
                searchForm.setDeptId(Long.parseLong(txtSearchDeptId.getValue()));
            } else {
                if ("admin".equals(getUserName())) {
                } else {
                    searchForm.setDeptId(getDeptId());
                }
            }

            if (cbSearchPosition.getSelectedItem() != null) {
                Long posId = cbSearchPosition.getSelectedItem().getValue();
                searchForm.setPosId(posId);
            }
            if (cbUserType.getSelectedItem() != null) {
                Long userType = Long.parseLong(cbUserType.getSelectedItem().getValue().toString());
                searchForm.setUserType(userType);
            }
            fillDataToList();
        } catch (WrongValueException | NumberFormatException ex) {
            LogUtils.addLogNoImportant(ex);
            showNotification("Có lỗi xảy ra! Xin vui lòng thử lại!", Constants.Notification.ERROR);
        } finally {
            Clients.clearBusy();
        }
    }

    @Listen("onReload=#userManWindow")
    public void onReload() throws IOException {
        onSearch();
    }

    @Listen("onClick=#btnOpenCreate")
    public void onOpenCreate() {
        createDlg = (Window) Executions.createComponents("/Pages/admin/user/userCreate.zul", null, null);
        createDlg.doModal();
    }

    @Listen("onClick=#btnClose")
    public void onClose() {
        createDlg.detach();
    }

    @Listen("onEdit=#lstItems")
    public void onEdit() throws IOException {
        Listitem item = lstItems.getSelectedItem();
        VUsers vu = item.getValue();
        UserDAOHE udhe = new UserDAOHE();
        Long deptIdChild = vu.getDeptId();
        Long deptIdParent = getDeptId();
        Long userIdParent = getUserId();
        Long userIdChild = vu.getUserId();
        boolean checkRole = udhe.checkRole(userIdParent, deptIdParent, userIdChild, deptIdChild);
        if (!checkRole) {
            showNotification("Bạn không có quyền thao tác");
            return;
        }

        Users u = udhe.findById(vu.getUserId());
        if (u != null) {
            Map args = new ConcurrentHashMap();
            args.put("user", u);
            createDlg = (Window) Executions.createComponents("/Pages/admin/user/userCreate.zul", null, args);
            createDlg.setTitle("Chỉnh sửa thông tin người dùng");
            createDlg.doModal();
        }
    }

    @Listen("onRole=#lstItems")
    public void onUserRole() throws IOException {
        Listitem item = lstItems.getSelectedItem();
        VUsers vu = item.getValue();
        UserDAOHE udhe = new UserDAOHE();
        Long deptIdChild = vu.getDeptId();
        Long deptIdParent = getDeptId();
        Long userIdParent = getUserId();
        Long userIdChild = vu.getUserId();
        boolean checkRole = udhe.checkRole(userIdParent, deptIdParent, userIdChild, deptIdChild);
        if (!checkRole) {
            showNotification("Bạn không có quyền thao tác");
            return;
        }
        Users u = udhe.findById(vu.getUserId());
        if (u != null) {
            Map args = new ConcurrentHashMap();
            args.put("user", u);
            args.put("parentWindow", userManWindow);
            roleDlg = (Window) Executions.createComponents("/Pages/admin/user/userRole.zul", null, args);
            roleDlg.doModal();
        }
    }

    @Listen("onDelete=#lstItems")
    public void onDelete() throws IOException {
        Messagebox.show("Xác nhận xóa người dùng?", "Thông báo", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event e) throws IOException {
                        if (null != e.getName()) {
                            switch (e.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    Listitem item = lstItems.getSelectedItem();
                                    VUsers vu = item.getValue();
                                    deleteUser(vu);
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                        lstItems.clearSelection();
                    }
                });        
    }
    
    public void deleteUser(VUsers vu) throws IOException{
        UserDAOHE udhe = new UserDAOHE();
        Long deptIdChild = vu.getDeptId();
        Long deptIdParent = getDeptId();
        Long userIdParent = getUserId();
        Long userIdChild = vu.getUserId();
        boolean checkRole = udhe.checkRole(userIdParent, deptIdParent, userIdChild, deptIdChild);
        if (!checkRole) {
            showNotification("Bạn không có quyền thao tác");
            return;
        }
        udhe.checkRole(Long.MIN_VALUE, Long.MIN_VALUE, Long.MIN_VALUE, Long.MIN_VALUE);

        if (vu.getUserId().equals(getUserId())) {
            showNotification("Không được phép xóa chính account của mình");
            return;
        }
        if (udhe.checkNodeDeptUserId(vu.getUserId())) {
            showNotification("Tài khoản này nằm trong Node");
            return;
        }

        Users u = udhe.findById(vu.getUserId());
        if (u != null) {
            addLog(Constants.ACTION.TYPE.DELETE, Constants.ACTION.NAME.DELETE,
                    u.getUserId(), Constants.OBJECT_TYPE.USER, u.getUserName(), u);
            u.setStatus(-1l);
            udhe.update(u);
            udhe.getSession().getTransaction().commit();
            onSearch();
            showSuccessNotification("Xóa thành công");
        }
    }

    @Listen("onLock=#lstItems")
    public void onLock() throws IOException {
        Messagebox.show("Xác nhận Khóa người dùng?", "Thông báo", Messagebox.OK | Messagebox.CANCEL,
                Messagebox.QUESTION, Messagebox.CANCEL, new org.zkoss.zk.ui.event.EventListener() {
                    @Override
                    public void onEvent(Event e) throws IOException {
                        if (null != e.getName()) {
                            switch (e.getName()) {
                                case Messagebox.ON_OK:
                                    // OK is clicked
                                    Listitem item = lstItems.getSelectedItem();
                                    VUsers vu = item.getValue();
                                    onLockUser(vu);
                                    onSearch();
                                    break;
                                case Messagebox.ON_NO:
                                    break;
                            }
                        }
                        lstItems.clearSelection();
                    }
                });         
        
    }
    
    public void onLockUser(VUsers vu){
        UserDAOHE udhe = new UserDAOHE();
        Long deptIdChild = vu.getDeptId();
        Long deptIdParent = getDeptId();
        Long userIdParent = getUserId();
        Long userIdChild = vu.getUserId();
        boolean checkRole = udhe.checkRole(userIdParent, deptIdParent, userIdChild, deptIdChild);
        if (!checkRole) {
            showNotification("Bạn không có quyền thao tác");
            return;
        }
        Users u = udhe.findById(vu.getUserId());
        if (u != null) {
            u.setStatus(0l);
            udhe.update(u);  
            udhe.getSession().getTransaction().commit();
            showSuccessNotification("Khóa thành công");
            addLog(Constants.ACTION.TYPE.LOCK, Constants.ACTION.NAME.LOCK,
                    u.getUserId(), Constants.OBJECT_TYPE.USER, u.getUserName());
        }        
    }

    @Listen("onUnlock=#lstItems")
    public void onUnLock() throws IOException {
        Listitem item = lstItems.getSelectedItem();
        VUsers vu = item.getValue();
        UserDAOHE udhe = new UserDAOHE();
        Long deptIdChild = vu.getDeptId();
        Long deptIdParent = getDeptId();
        Long userIdParent = getUserId();
        Long userIdChild = vu.getUserId();
        boolean checkRole = udhe.checkRole(userIdParent, deptIdParent, userIdChild, deptIdChild);
        if (!checkRole) {
            showNotification("Bạn không có quyền thao tác");
            return;
        }

        Users u = udhe.findById(vu.getUserId());
        if (u != null) {
            u.setStatus(1l);
            udhe.update(u);
            udhe.getSession().getTransaction().commit();
            showSuccessNotification("Mở khóa thành công");
            addLog(Constants.ACTION.TYPE.UNLOCK, Constants.ACTION.NAME.UNLOCK,
                    u.getUserId(), Constants.OBJECT_TYPE.USER, u.getUserName());
        }
        onSearch();
    }

    @Listen("onResetPass=#lstItems")
    public void onResetPass() throws IOException {
        Listitem item = lstItems.getSelectedItem();
        VUsers vu = item.getValue();
        UserDAOHE udhe = new UserDAOHE();
        Long deptIdChild = vu.getDeptId();
        Long deptIdParent = getDeptId();
        Long userIdParent = getUserId();
        Long userIdChild = vu.getUserId();
        boolean checkRole = udhe.checkRole(userIdParent, deptIdParent, userIdChild, deptIdChild);
        if (!checkRole) {
            showNotification("Bạn không có quyền thao tác");
            return;
        }
        Map args = new ConcurrentHashMap();
        args.put("userId", vu.getUserId());
        if (vu.getUserId() == getUserId()) {
            args.put("type", 1l);
            resetPassDlg = (Window) Executions.createComponents("/Pages/admin/user/userChangePass.zul", null, args);
            resetPassDlg.doModal();
        } else {
            resetPassDlg = (Window) Executions.createComponents("/Pages/admin/user/userResetPass.zul", null, args);
            resetPassDlg.doModal();
        }
    }

    @Listen("onClick=#btnShowSearchDept")
    public void onOpenDeptSelect() {
        Map<String, Object> args = new ConcurrentHashMap();
        args.put("idOfDisplayNameComp", "/userManWindow/txtSearchDept");
        args.put("idOfDisplayIdComp", "/userManWindow/txtSearchDeptId");
        Window showDeptDlg = (Window) Executions.createComponents("/Pages/admin/user/userDept.zul", null, args);
        showDeptDlg.doModal();
    }

    public ListModelArray getLstUser() {
        return lstUser;
    }

    public void setLstUser(ListModelArray lstUser) {
        this.lstUser = lstUser;
    }
}
