/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAOHE;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.core.base.model.PagingListModel;
import com.viettel.core.sys.BO.Objects;
import com.viettel.core.user.model.UserToken;
import com.viettel.utils.Constants;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Document.Attachs;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author ChucHV
 */
public class AttachDAOHE extends GenericDAOHibernate<Attachs, Long> {

    public AttachDAOHE() {
        super(Attachs.class);
    }

    @Override
    public Attachs findById(Long id) {
        Query query = getSession().getNamedQuery(
                "Attachs.findByAttachId");
        query.setParameter("attachId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Attachs) result.get(0);
        }
    }

    public boolean saveFile(InputStream in, String filePath, String fileName) {
        boolean bReturn = true;
        try {
            File folderExisting = new File(filePath);
            if (!folderExisting.isDirectory()) {
                folderExisting.mkdirs();
            }

            OutputStream out = new FileOutputStream(filePath + File.separator + fileName);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();
        } catch (IOException e) {
            LogUtils.addLogDB(e);
            bReturn = false;
        }
        return bReturn;
    }

    public void deleteByFileId(Long fileId, Long attCat) {
        Query query = getSession().createQuery("UPDATE  Attachs set isActive = 0 WHERE objectId = :fileId AND attachCat = :cat");
        query.setParameter("fileId", fileId);
        query.setParameter("cat", attCat);
        query.executeUpdate();
    }

    public boolean saveAvatar(InputStream is, String fileName) {
        String uploadPath = ResourceBundleUtil.getString("dir_avartar");
        String folderPath = Executions.getCurrent().getDesktop().getWebApp().getRealPath(uploadPath);
        return saveFile(is, folderPath, fileName);
    }

    public Long saveNewAttach(InputStream is, String rootPath) {
        return -1L;
    }
    //Su dung cho TABLE OBJECTS

    public void setIsTempAttach(Long newfileId, Long oldfileId) {
        Query query = getSession().createQuery("UPDATE Attachs SET objectId = :newfileId WHERE objectId = :oldfileId AND attachCat = :cat ");
        query.setParameter("newfileId", newfileId);
        query.setParameter("oldfileId", oldfileId);
        query.setParameter("cat", Constants.OBJECT_TYPE.IMPORT_ORDER_FILE);
        query.executeUpdate();
    }

    public void setIsTempAttach(Long newfileId, Long oldfileId, Long attcat) {
        Query query = getSession().createQuery("UPDATE Attachs SET objectId = :newfileId WHERE objectId = :oldfileId AND attachCat = :cat ");
        query.setParameter("newfileId", newfileId);
        query.setParameter("oldfileId", oldfileId);
        query.setParameter("cat", attcat);
        query.executeUpdate();
    }

    public Attachs getByObjectIdAndCode(String code, Long objectId) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId AND attachCode = :attachCode AND a.isActive = 1";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", objectId);
        query.setParameter("attachCode", code);
        List result = query.list();
        if (result.size() > 0) {
            return (Attachs) result.get(0);
        } else {
            return null;
        }
    }

    public Attachs getLastByObjectIdAndType(Long objectId, Long attachType) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId AND attachType = :attachType AND a.isActive = 1 order by dateCreate desc";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", objectId);
        query.setParameter("attachType", attachType);
        List result = query.list();
        if (result.size() > 0) {
            return (Attachs) result.get(0);
        } else {
            return null;
        }
    }

    public Attachs getByObjectId(Objects object) {
        return this.getByObjectId(object.getObjectId());
    }

    public Attachs getByObjectId(Long objectId) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId AND a.isActive = 1";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", objectId);
        List result = query.list();
        if (result.size() > 0) {
            return (Attachs) result.get(0);
        } else {
            return null;
        }
    }

    public List<Attachs> getByObjectIdAndType(Long id, Long attachCat) {
        try {
            String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId AND a.attachCat =:attachCat AND a.isActive = 1 ORDER by attachId desc";
            Query query = getSession().createQuery(hql);
            query.setParameter("objectId", id);
            query.setParameter("attachCat", attachCat);
            return query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
    public List<Attachs> getByObjectIdAndAttachCatAndAttachType(Long id, Long attachCat, Long attachType) {
        String hql = "SELECT a FROM Attachs a WHERE "
                + "a.objectId = :objectId "
                + "AND a.attachCat =:attachCat "
                + "AND a.attachType =:attachType "
                + "AND a.isActive = 1 "
                + "ORDER by dateCreate desc";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", id);
        query.setParameter("attachCat", attachCat);
        query.setParameter("attachType", attachType);
        return query.list();
    }

    public List<Attachs> getByObjectIdAndAttachCatAndAttachTypeIsSent(Long id, Long attachCat, Long attachType, Long isSent) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId AND a.attachCat =:attachCat AND a.attachType =:attachType AND a.isSent = :isSent AND a.isActive = 1 ORDER by dateCreate desc";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", id);
        query.setParameter("attachCat", attachCat);
        query.setParameter("attachType", attachType);
        query.setParameter("isSent", isSent);
        return query.list();
    }

    //thanhdv:9/5
    public List<Attachs> getByObjectIdAndAttachCatAndAttachTypeByIdMAX(Long id, Long attachCat, Long attachType) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId AND a.attachCat =:attachCat AND a.attachType =:attachType "
                + " AND a.attachId=(SELECT MAX(a2.attachId) FROM Attachs a2 where a2.attachCat =:attachCat AND a2.attachType =:attachType and a2.objectId=:objectId )"
                + " AND a.isActive = 1 ORDER by attachId desc";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", id);
        query.setParameter("attachCat", attachCat);
        query.setParameter("attachType", attachType);
        return query.list();
    }

    public List<Attachs> getByAllFileAttachSDBS(Long id, Long attachCat, Long attachType) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId AND a.attachCat =:attachCat AND a.attachType =:attachType "
                + " AND a.isActive = 1 ORDER by attachId desc";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", id);
        query.setParameter("attachCat", attachCat);
        query.setParameter("attachType", attachType);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    public List<Attachs> getAllByObjectIdAndType(Long id, Long attachCat) {
        try {
            String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId AND a.attachCat =:attachCat ";
            Query query = getSession().createQuery(hql);
            query.setParameter("objectId", id);
            query.setParameter("attachCat", attachCat);
            return query.list();
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<Attachs> getByObjectId(Long objectId, Long attachCat) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId "
                + " AND a.attachCat = :attachCat AND a.isActive = 1";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", objectId);
        query.setParameter("attachCat", attachCat);
        List result = query.list();
        return result;
    }

    //@SuppressWarnings({"/*rawtypes*/", "unchecked"})
    public Attachs getByObjectIdAndAttachCat(Long objectId, Long attachCat) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId "
                + " AND a.attachCat = :attachCat AND a.isActive = 1 order by dateCreate desc";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", objectId);
        query.setParameter("attachCat", attachCat);
        List result = query.list();
        if (result.size() > 0) {
            return (Attachs) result.get(0);
        } else {
            return null;
        }
    }

    public Attachs getByObjectIdAndAttachType(Long objectId, Long attachType, Boolean currUs) {
        String[] attachDes = new String[]{};
        return getByObjectIdAndAttachType(objectId, attachType, currUs, attachDes);
    }

    public Attachs getByObjectIdAndAttachType(Long objectId, Long attachType, Boolean currUs, String... attachDes) {
        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId "
                + " AND a.attachType = :attachType AND a.isActive = 1 ";
        if (currUs) {
            hql += "AND a.creatorId = :userId ";
        } else {
            hql += "AND a.creatorId != :userId ";
        }
        if (attachDes != null && attachDes.length > 0) {
            hql += "AND a.attachDes = :attachDes ";
        }
        hql += " order by dateCreate desc";
        UserToken us = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", objectId);
        query.setParameter("attachType", attachType);
        query.setParameter("userId", us.getUserId());
        if (attachDes != null && attachDes.length > 0) {
            query.setParameter("attachDes", attachDes[0]);
        }
        List result = query.list();
        if (result.size() > 0) {
            return (Attachs) result.get(0);
        } else {
            return null;
        }
    }

    @SuppressWarnings("rawtypes")
    public void deleteAttach(Long objectId, Long attachCat, List lstNotDelete) {
        String hql;// = "update Attachs a set a.isActive = -1 where a.objectId = :objectId and a.attachCat=:attachCat and a.attachId not in (:lstNotDelete)";
        Query query;
        if (lstNotDelete != null && lstNotDelete.size() > 0) {
            hql = "update Attachs a set a.isActive = -1 where a.objectId = :objectId and a.attachCat=:attachCat and a.attachId not in (:lstNotDelete)";
            query = session.createQuery(hql);
            query.setParameter("objectId", objectId);
            query.setParameter("attachCat", attachCat);
            query.setParameterList("lstNotDelete", lstNotDelete);
        } else {
            hql = "update Attachs a set a.isActive = -1 where a.objectId = :objectId and a.attachCat=:attachCat";
            query = session.createQuery(hql);
            query.setParameter("objectId", objectId);
            query.setParameter("attachCat", attachCat);
        }
        query.executeUpdate();
    }

    @Override
    public void saveOrUpdate(Attachs attachs) {
        if (attachs != null) {
            super.saveOrUpdate(attachs);
        }
        getSession().flush();
    }

    public void saveAttach(List<Attachs> list) {
        for (Attachs attachs : list) {
            getSession().saveOrUpdate(attachs);
        }
    }

    public void deleteAttach(Attachs attach) {
        attach.setIsActive(Constants.Status.INACTIVE);
        getSession().saveOrUpdate(attach);
    }

    public PagingListModel searchPublicFile(Attachs searchForm, int start, int take, Long userId) {
        List listParam = new ArrayList();
        List listParamName = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder("select new com.viettel.voffice.model.AttachCategoryModel(a,pp)  from Attachs a,"
                    + "Category pp where a.isActive = 1   and a.attachType = pp.categoryId AND pp.categoryTypeCode='COSMETIC_FILE_TYPE' "
                    + " AND a.attachCat=:attachCat AND a.creatorId=:creatorId  ");
            StringBuilder strCountBuf = new StringBuilder("select count(a) from Attachs a where a.isActive = 1"
                    + " AND a.attachCat=:attachCat  AND a.creatorId=:creatorId ");
            StringBuilder hql = new StringBuilder();
            if (searchForm != null) {
                if (searchForm.getAttachType() != null) {
                    hql.append(" and a.attachType=:attachType ");
                    listParam.add(searchForm.getAttachType());
                    listParamName.add("attachType");
                }
                if (searchForm.getAttachName() != null) {
                    hql.append(" and lower(a.attachName) LIKE :attachName escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getAttachName()));
                    listParamName.add("attachName");
                }
            }
            strBuf.append(hql);
            strCountBuf.append(hql);

            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());

            query.setParameter("attachCat", Constants.OBJECT_TYPE.COSMETIC_PUBLIC_PROFILE);
            query.setParameter("creatorId", userId);
//            query.setParameter("attachType", 3L);
            countQuery.setParameter("attachCat", Constants.OBJECT_TYPE.COSMETIC_PUBLIC_PROFILE);
            countQuery.setParameter("creatorId", userId);

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(listParamName.get(i).toString(), listParam.get(i));
                countQuery.setParameter(listParamName.get(i).toString(), listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    // Vunt: tạo hàm list ra danh sách hồ sơ chung
    public PagingListModel searchPublicFileImport(Attachs searchForm, int start, int take, Long userId) {
        List listParam = new ArrayList();
        List listParamName = new ArrayList();
        try {
            StringBuilder strBuf = new StringBuilder("select new com.viettel.voffice.model.AttachCategoryModel(a,pp)  from Attachs a,"
                    + "Category pp where a.isActive = 1   and a.attachType = pp.categoryId AND pp.categoryTypeCode='IMPORT_FILE_TYPE' "
                    + " AND a.attachCat=:attachCat AND a.creatorId=:creatorId  ");
            StringBuilder strCountBuf = new StringBuilder("select count(a) from Attachs a where a.isActive = 1"
                    + " AND a.attachCat=:attachCat  AND a.creatorId=:creatorId ");
            StringBuilder hql = new StringBuilder();
            if (searchForm != null) {
                if (searchForm.getAttachType() != null) {
                    hql.append(" and a.attachType=:attachType ");
                    listParam.add(searchForm.getAttachType());
                    listParamName.add("attachType");
                }
                if (searchForm.getAttachName() != null) {
                    hql.append(" and lower(a.attachName) LIKE :attachName escape '/' ");
                    listParam.add(StringUtils.toLikeString(searchForm.getAttachName()));
                    listParamName.add("attachName");
                }
            }
            strBuf.append(hql);
            strCountBuf.append(hql);

            Query query = session.createQuery(strBuf.toString());
            Query countQuery = session.createQuery(strCountBuf.toString());

            query.setParameter("attachCat", Constants.OBJECT_TYPE.IMPORT_FILE_TYPE);
            query.setParameter("creatorId", userId);
//            query.setParameter("attachType", 3L);
            countQuery.setParameter("attachCat", Constants.OBJECT_TYPE.IMPORT_FILE_TYPE);
            countQuery.setParameter("creatorId", userId);

            for (int i = 0; i < listParam.size(); i++) {
                query.setParameter(listParamName.get(i).toString(), listParam.get(i));
                countQuery.setParameter(listParamName.get(i).toString(), listParam.get(i));
            }

            query.setFirstResult(start);
            if (take < Integer.MAX_VALUE) {
                query.setMaxResults(take);
            }

            List lst = query.list();
            Long count = (Long) countQuery.uniqueResult();
            PagingListModel model = new PagingListModel(lst, count);
            return model;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public boolean CheckTypePublicFile(Long attachType, Long creatorId, Long attachCat, Long attachId) {

        String HQL = "select count(r) from Attachs r where r.isActive = 1 "
                + " AND r.attachCat=:attachCat AND r.attachType=:attachType AND r.creatorId=:creatorId";
        if (attachId != null) {
            HQL += " AND r.attachId!=:attachId";
        }
        Query query = session.createQuery(HQL);
        query.setParameter("attachType", attachType);
        query.setParameter("creatorId", creatorId);
        query.setParameter("attachCat", attachCat);
        if (attachId != null) {
            query.setParameter("attachId", attachId);
        }
        Long count = (Long) query.uniqueResult();
        //nếu đã có trong CSDL sẽ trả về true
        if (count > 0) {
            return true;
        }
        return false;
    }

    public List getListProFile(Long creatorId) {
        String HQL = "SELECT  new com.viettel.voffice.model.AttachCategoryModel(a,c) from Attachs a,Category c WHERE  a.isActive=1 AND a.attachType = c.categoryId AND a.attachCat=:attachCat AND a.creatorId=:creatorId";
        Query query = session.createQuery(HQL);
        query.setParameter("attachCat", Constants.OBJECT_TYPE.COSMETIC_PUBLIC_PROFILE);
        query.setParameter("creatorId", creatorId);
        List lst = query.list();
        return lst;
    }

    public List getListProFileImport(Long creatorId) {
        String HQL = "SELECT  new com.viettel.voffice.model.AttachCategoryModel(a,c) from Attachs a,Category c WHERE  a.isActive=1 AND a.attachType = c.categoryId AND a.attachCat=:attachCat AND a.creatorId=:creatorId ORDER by a.attachId desc";
        Query query = session.createQuery(HQL);
        query.setParameter("attachCat", Constants.OBJECT_TYPE.IMPORT_FILE_TYPE);
        query.setParameter("creatorId", creatorId);
        List lst = query.list();
        return lst;
    }

    public List getListProFileDeviceImport(Long creatorId) {
        String HQL = "SELECT  new com.viettel.voffice.model.AttachCategoryModel(a,c) from Attachs a,Category c WHERE  a.isActive=1 AND a.attachType = c.categoryId AND a.attachCat=:attachCat AND a.creatorId=:creatorId ORDER by a.attachId desc";
        Query query = session.createQuery(HQL);
        query.setParameter("attachCat", Constants.OBJECT_TYPE.COSMETIC_PUBLIC_PROFILE);
        query.setParameter("creatorId", creatorId);
        List lst = query.list();
        return lst;
    }

    /**
     * Tim attachs theo ObjectId co isActive = 1
     *
     * @author giangnh20
     * @param objectId
     * @return
     */
    public List<Attachs> findByObjectId(Long objectId) {
        List<Attachs> result = new ArrayList<Attachs>();
        if (objectId != null) {
            Query query = getSession().getNamedQuery("Attachs.findByObjectIdAndActive");
            query.setParameter("objectId", objectId);
            result = query.list();
        }
        return result;
    }

    public List<Attachs> findByObjectId(Long objectId, Long attachCat) {

        String hql = "SELECT a FROM Attachs a WHERE a.objectId = :objectId "
                + " AND a.attachCat = :attachCat AND a.isActive = 1";
        Query query = getSession().createQuery(hql);
        query.setParameter("objectId", objectId);
        query.setParameter("attachCat", attachCat);
        List result = query.list();
        return result;
    }

    /**
     * Clone attachs when copy files to another
     *
     * @author giangnh20
     * @param sourceFileId
     * @param targetFileId
     */
    public void copyAttachs(Long sourceFileId, Long targetFileId) {
        List<Attachs> attachList = findByObjectId(sourceFileId);
        for (Attachs attachs : attachList) {
            Attachs other = attachs.copyAttachsWithNoId();
            other.setObjectId(targetFileId);
            super.saveOrUpdate(other);
        }
        getSession().flush();
    }

    public void copyAttachs(Long sourceFileId, Long targetFileId, Long attachCat) {
        List<Attachs> attachList = findByObjectId(sourceFileId, attachCat);
        for (Attachs attachs : attachList) {
            Attachs other = attachs.copyAttachsWithNoId();
            other.setObjectId(targetFileId);
            super.saveOrUpdate(other);
        }
        getSession().flush();
    }

    /**
     * Tim attachs theo attachId
     *
     * @author thanhtm
     * @param attachId
     * @return
     */
    public Attachs findByAttachId(Long attachId) {
        if (attachId != null) {
            Query query = getSession().getNamedQuery("Attachs.findByAttachId");
            query.setParameter("attachId", attachId);
            List result = query.list();
            if (result.isEmpty()) {
                return null;
            } else {
                return (Attachs) result.get(0);
            }
        } else {
            return null;
        }
    }

    @Override
    public Session getSession() {
        return super.getSession(); //To change body of generated methods, choose Tools | Templates.
    }
}
