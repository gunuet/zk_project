/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAOHE;

import java.util.List;

import org.hibernate.Query;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.rapidtest.BO.RtPermit;
import com.viettel.utils.Constants;

/**
 *
 * @author Linhdx
 */
public class PermitDAO extends
        GenericDAOHibernate<Permit, Long> {

    public PermitDAO() {
        super(Permit.class);
    }

    @Override
    public void saveOrUpdate(Permit obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }
    }

    @Override
    public Permit findById(Long id) {
        Query query = getSession().getNamedQuery(
                "RtPermit.findByPermitId");
        query.setParameter("permitId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Permit) result.get(0);
        }
    }

    public List<Permit> findAllPermitActiveByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from Permit a where a.fileId = :fileId "
                + "and a.isActive = :isActive");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        return result;
    }

    public List<Permit> findAllPermitActiveByFileIdAndType(Long fileId, Long type) {
        Query query = getSession().createQuery("select a from Permit a where a.fileId = :fileId "
                + " and a.type = :type "
                + "and a.isActive = :isActive");
        query.setParameter("fileId", fileId);
        query.setParameter("type", type);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        return result;
    }

    public Permit findTtbCreateNewByReveiveNo(String receiveNo) {
        Query query = getSession().createQuery("select a from Permit a, VFileImportDevice b where a.fileId = b.fileId and a.receiveNo = :receiveNo "
                + " and b.fileType = :fileType "
                + " and (b.typeCreate = 1 or b.typeCreate = 2 or b.typeCreate is null ) "
                + " and a.isActive = :isActive"
                + " order by a.signDate desc");
        query.setParameter("receiveNo", receiveNo);
        query.setParameter("fileType", Constants.FILE_TYPE_TTB);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List<Permit> lst = query.list();
        if (lst.size() > 0) {
            return lst.get(0);
        }
        return null;
    }

    public Permit findLastByFileIdAndType(Long fileId, Long type) {
        if (type != null) {
            Query query = getSession().createQuery("select a from Permit a where a.fileId = :fileId and a.isActive = 1 "
                    + "and a.type = :type order by signDate desc");
            query.setParameter("fileId", fileId);
            query.setParameter("type", type);
            List result = query.list();
            if (result.isEmpty()) {
                return null;
            } else {
                return (Permit) result.get(0);
            }
        } else {
            Query query = getSession().createQuery("select a from Permit a where a.fileId = :fileId and a.isActive = 1 "
                    + " order by signDate desc");
            query.setParameter("fileId", fileId);
            List result = query.list();
            if (result.isEmpty()) {
                return null;
            } else {
                return (Permit) result.get(0);
            }
        }
    }

    public List<RtPermit> findAllActiveByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from RtPermit a where a.fileId = :fileId "
                + "and a.isActive = :isActive");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        return result;
    }

    public Permit findPermitByFileId(Long fileId) {
        Permit obj = new Permit();
        Query query = getSession().createQuery("select a from Permit a where a.fileId = :fileId "
                + "and a.isActive = :isActive ORDER BY a.permitId desc");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List result = query.list();
        if (result != null && !result.isEmpty()) {
            obj = (Permit) result.get(0);
        }
        return obj;
    }

    public boolean updateIsQrCode(Long fileId) {
        boolean result = false;
        Permit cospermitbo = findPermitByFileId(fileId);
        if (cospermitbo != null) {
            cospermitbo.setIsQrCode(1L);
            this.saveOrUpdate(cospermitbo);
            result = true;
        }
        return result;
    }
}
