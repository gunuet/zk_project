/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.DAOHE;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.module.rapidtest.BO.RtPermit;
import com.viettel.utils.Constants;

import java.util.List;

import org.hibernate.Query; 

/**
 *
 * @author Linhdx
 */
public class PermitDAOHE extends
        GenericDAOHibernate<RtPermit, Long> {

    public PermitDAOHE() {
        super(RtPermit.class);
    }

    @Override
    public void saveOrUpdate(RtPermit obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }
    }

    @Override
    public RtPermit findById(Long id) {
        Query query = getSession().getNamedQuery(
                "RtPermit.findByPermitId");
        query.setParameter("permitId", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (RtPermit) result.get(0);
        }
    }
    
        public List<RtPermit> findAllActiveByFileId(Long fileId) {
        Query query = getSession().createQuery("select a from Permit a where a.fileId = :fileId "
                + "and a.isActive = :isActive");
        query.setParameter("fileId", fileId);
        query.setParameter("isActive", Constants.Status.ACTIVE);
        List<RtPermit> result = query.list();
        return result;
    }

    

}
