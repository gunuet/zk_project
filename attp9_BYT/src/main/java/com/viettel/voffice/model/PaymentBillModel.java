/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.model;


import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import java.io.Serializable;
import com.viettel.core.workflow.BO.Process;
/**
 *
 * @author giangpn
 */

public class PaymentBillModel implements Serializable {

    private PaymentInfo paymentInfo;

    private Bill bill;

    
    public PaymentBillModel() {
    }

    public PaymentBillModel(PaymentInfo paymentInfo,Bill bill) {
        this.paymentInfo = paymentInfo;
        this.bill = bill;
    }
    
    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }
}
