/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.voffice.model.exporter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.zkoss.zk.ui.Component;
import com.viettel.utils.LogUtils;

/**
 *
 * @author ChucHV
 */
public class Utils {

    public static String getAlign(Component cmp) {
        return (String) invokeComponentGetter(cmp, "getAlign");
    }

    public static Object invokeComponentGetter(Component target, String... methods) {
        Class<? extends Component> cls = target.getClass();
        for (String methodName : methods) {
            try {
                Method method = cls.getMethod(methodName, (Class<?>[]) null);
                Object ret = method.invoke(target, (Object[]) null);
                if (ret != null) {
                    return ret;
                }
            } catch (SecurityException | NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
                LogUtils.addLog(e);
            }
        }
        return null;
    }   
}
