/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Amendment")
public class Amendment {

    public Amendment() {

    }

    @XmlElement(name = "Organization", required = true)
    private String Organization;
    @XmlElement(name = "Division", required = true)
    private String Division;
    @XmlElement(name = "Name", required = true)
    private String Name;
    @XmlElement(name = "AmendmentContent", required = true)
    private String AmendmentContent;
    @XmlElement(name = "ExpirationDate", required = true)
    private String ExpirationDate;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;

    public Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public String getOrganization() {
        return Organization;
    }

    public String getDivision() {
        return Division;
    }

    public String getName() {
        return Name;
    }

    public String getAmendmentContent() {
        return AmendmentContent;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setOrganization(String Organization) {
        this.Organization = Organization;
    }

    public void setDivision(String Division) {
        this.Division = Division;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setAmendmentContent(String AmendmentContent) {
        this.AmendmentContent = AmendmentContent;
    }

    public void setExpirationDate(String ExpirationDate) {
        this.ExpirationDate = ExpirationDate;
    }

}
