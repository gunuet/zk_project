/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "Assay")
@XmlAccessorType(XmlAccessType.FIELD)
public class Assay {

    public Assay() {

    }
    @XmlElement(name = "AssayCode", required = true)
    private String AssayCode;
    @XmlElement(name = "AssayName", required = true)
    private String AssayName;
    @XmlElement(name = "AssayAddress", required = true)
    private String AssayAddress;
    @XmlElement(name = "AssayPhone", required = true)
    private String AssayPhone;
    @XmlElement(name = "AssayFax", required = true)
    private String AssayFax;

    public String getAssayCode() {
        return AssayCode;
    }

    public String getAssayName() {
        return AssayName;
    }

    public String getAssayAddress() {
        return AssayAddress;
    }

    public String getAssayPhone() {
        return AssayPhone;
    }

    public String getAssayFax() {
        return AssayFax;
    }

    public void setAssayCode(String AssayCode) {
        this.AssayCode = AssayCode;
    }

    public void setAssayName(String AssayName) {
        this.AssayName = AssayName;
    }

    public void setAssayAddress(String AssayAddress) {
        this.AssayAddress = AssayAddress;
    }

    public void setAssayPhone(String AssayPhone) {
        this.AssayPhone = AssayPhone;
    }

    public void setAssayFax(String AssayFax) {
        this.AssayFax = AssayFax;
    }

}
