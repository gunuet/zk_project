/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Attachment")
public class Attachment {

    @XmlElement(name = "AttachmentId")
    private String AttachmentId;
    @XmlElement(name = "AttachmentName")
    private String AttachmentName;
    @XmlElement(name = "AttachTypeCode")
    private String AttachTypeCode;
    @XmlElement(name = "AttachTypeName")
    private String AttachTypeName;
    @XmlElement(name = "AttachmentTypeCode")
    private String AttachmentTypeCode;
    @XmlElement(name = "AttachmentTypeName")
    private String AttachmentTypeName;
    @XmlElement(name = "FileByte")
    private String FileByte;
//    @XmlElement(name = "Filebyte")
//    private String Filebyte;
    @XmlElement(name = "AttachPath")
    private String AttachPath;
    @XmlElement(name = "NswFileCode")
    private String NswFileCode;
    @XmlElement(name = "AttachmentCode")
    private String AttachmentCode;

    @XmlElement(name = "FileName")
    private String FileName;

    //linhdx 20160519 them khi gui file lon
    @XmlElement(name = "NswProductFileCode")
    private String NswProductFileCode;

    @XmlElement(name = "NswFileType")
    private String NswFileType;

    @XmlElement(name = "AttachMohId")
    private String AttachMohId;
    
    @XmlElement(name = "Step")
    private String Step;

    public Attachment() {

    }

    public String getAttachmentCode() {
        return AttachmentCode;
    }

    public void setAttachmentCode(String AttachmentCode) {
        this.AttachmentCode = AttachmentCode;
    }

    public String getNswFileCode() {
        return NswFileCode;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public String getAttachmentId() {
        return AttachmentId;
    }

    public String getAttachmentName() {
        return AttachmentName;
    }

    public String getAttachTypeCode() {
        return AttachTypeCode;
    }

    public String getAttachmentTypeCode() {
        return AttachmentTypeCode;
    }

    public String getAttachmentTypeName() {
        return AttachmentTypeName;
    }

    public String getAttachTypeName() {
        return AttachTypeName;
    }

    public String getAttachPath() {
        return AttachPath;
    }

    public void setAttachmentId(String AttachmentId) {
        this.AttachmentId = AttachmentId;
    }

    public void setAttachmentName(String AttachmentName) {
        this.AttachmentName = AttachmentName;
    }

    public void setAttachTypeCode(String AttachTypeCode) {
        this.AttachTypeCode = AttachTypeCode;
    }

    public void setAttachTypeName(String AttachTypeName) {
        this.AttachTypeName = AttachTypeName;
    }

    public void setAttachmentTypeCode(String AttachmentTypeCode) {
        this.AttachmentTypeCode = AttachmentTypeCode;
    }

    public void setAttachmentTypeName(String AttachmentTypeName) {
        this.AttachmentTypeName = AttachmentTypeName;
    }

    public String getFileByte() {
//        if(Filebyte != null){
//            FileByte = Filebyte;
//        }
        return FileByte;
    }

    public void setFileByte(String FileByte) {
        this.FileByte = FileByte;
        //this.Filebyte = FileByte;
    }

    public void setAttachPath(String AttachPath) {
        this.AttachPath = AttachPath;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String FileName) {
        this.FileName = FileName;
    }

    public String getNswProductFileCode() {
        return NswProductFileCode;
    }

    public void setNswProductFileCode(String NswProductFileCode) {
        this.NswProductFileCode = NswProductFileCode;
    }

    public String getNswFileType() {
        return NswFileType;
    }

    public void setNswFileType(String NswFileType) {
        this.NswFileType = NswFileType;
    }

    public String getAttachMohId() {
        return AttachMohId;
    }

    public void setAttachMohId(String AttachMohId) {
        this.AttachMohId = AttachMohId;
    }

    public String getStep() {
        return Step;
    }

    public void setStep(String Step) {
        this.Step = Step;
    }
    
    

}
