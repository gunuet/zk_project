/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "CertifiedCirculation")
@XmlAccessorType(XmlAccessType.FIELD)
public class CertifiedCirculation {

    public CertifiedCirculation() {

    }
    @XmlElement(name = "CirculatingNo")
    private String CirculatingNo;
    @XmlElement(name = "RapidTestName")
    private String RapidTestName;
    @XmlElement(name = "SignCirculatingDate")
    private String SignCirculatingDate;
    @XmlElement(name = "Manufacture")
    private String Manufacture;
    @XmlElement(name = "ManufactureAddress")
    private String ManufactureAddress;
    @XmlElement(name = "ManufactureState")
    private String ManufactureState;
    @XmlElement(name = "BusinessName")
    private String BusinessName;
    @XmlElement(name = "BusinessAddress")
    private String BusinessAddress;
    @XmlElement(name = "SignRank")
    private String SignRank;
    @XmlElement(name = "SignName")
    private String SignName;
    @XmlElement(name = "SignDate")
    private String SignDate;
    @XmlElement(name = "SignPlace")
    private String SignPlace;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;

    public Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public String getCirculatingNo() {
        return CirculatingNo;
    }

    public String getRapidTestName() {
        return RapidTestName;
    }

    public String getSignCirculatingDate() {
        return SignCirculatingDate;
    }

    public String getManufacture() {
        return Manufacture;
    }

    public String getManufactureAddress() {
        return ManufactureAddress;
    }

    public String getManufactureState() {
        return ManufactureState;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public String getBusinessAddress() {
        return BusinessAddress;
    }

    public String getSignRank() {
        return SignRank;
    }

    public String getSignName() {
        return SignName;
    }

    public String getSignDate() {
        return SignDate;
    }

    public String getSignPlace() {
        return SignPlace;
    }

    public void setCirculatingNo(String CirculatingNo) {
        this.CirculatingNo = CirculatingNo;
    }

    public void setRapidTestName(String RapidTestName) {
        this.RapidTestName = RapidTestName;
    }

    public void setSignCirculatingDate(String SignCirculatingDate) {
        this.SignCirculatingDate = SignCirculatingDate;
    }

    public void setManufacture(String Manufacture) {
        this.Manufacture = Manufacture;
    }

    public void setManufactureAddress(String ManufactureAddress) {
        this.ManufactureAddress = ManufactureAddress;
    }

    public void setManufactureState(String ManufactureState) {
        this.ManufactureState = ManufactureState;
    }

    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    public void setBusinessAddress(String BusinessAddress) {
        this.BusinessAddress = BusinessAddress;
    }

    public void setSignRank(String SignRank) {
        this.SignRank = SignRank;
    }

    public void setSignName(String SignName) {
        this.SignName = SignName;
    }

    public void setSignDate(String SignDate) {
        this.SignDate = SignDate;
    }

    public void setSignPlace(String SignPlace) {
        this.SignPlace = SignPlace;
    }

}
