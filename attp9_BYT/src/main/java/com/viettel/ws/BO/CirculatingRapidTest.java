/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "CirculatingRapidTest")
@XmlAccessorType(XmlAccessType.FIELD)
public class CirculatingRapidTest {

    public CirculatingRapidTest() {

    }
    @XmlElement(name = "NSWFileCode")
    private String NSWFileCode;
    @XmlElement(name = "RapidTestNo")
    private String RapidTestNo;
    @XmlElement(name = "TaxCode")
    private String TaxCode;
    @XmlElement(name = "BusinessName")
    private String BusinessName;
    @XmlElement(name = "BusinessAddress")
    private String BusinessAddress;
    @XmlElement(name = "BusinessPhone")
    private String BusinessPhone;
    @XmlElement(name = "BusinessFax")
    private String BusinessFax;
    @XmlElement(name = "BusinessEmail")
    private String BusinessEmail;
    @XmlElement(name = "RapidTestName")
    private String RapidTestName;
    @XmlElement(name = "RapidTestCode")
    private String RapidTestCode;
    @XmlElement(name = "PlaceOfManufacture")
    private String PlaceOfManufacture;
    @XmlElement(name = "Manufacture")
    private String Manufacture;
    @XmlElement(name = "AddressOfManufacture")
    private String AddressOfManufacture;
    @XmlElement(name = "StateOfManufacture")
    private String StateOfManufacture;
    @XmlElement(name = "NameOfState")
    private String NameOfState;
    @XmlElement(name = "PropertiesTests")
    private String PropertiesTests;
    @XmlElement(name = "OperatingPrinciples")
    private String OperatingPrinciples;
    @XmlElement(name = "Description")
    private String Description;
    @XmlElement(name = "Packaging")
    private String Packaging;
    @XmlElement(name = "ShelfLife")
    private String ShelfLife;
    @XmlElement(name = "StorageConditions")
    private String StorageConditions;
    @XmlElement(name = "OtherInfos")
    private String OtherInfos;
    @XmlElement(name = "SignPlace")
    private String SignPlace;
    @XmlElement(name = "SignDate")
    private String SignDate;
    @XmlElement(name = "SignName")
    private String SignName;
    @XmlElement(name = "SignPlaceName")
    private String SignPlaceName;
    @XmlElementWrapper(name = "Attachments")
    @XmlElement(name = "Attachment")
    private List<Attachment> Attachments;
    @XmlElementWrapper(name = "ListOfTargetTesting")
    @XmlElement(name = "TargetTesting")
    private List<TargetTesting> ListOfTargetTesting;

    public List<TargetTesting> getListOfTargetTesting() {
        return ListOfTargetTesting;
    }

    public void setListOfTargetTesting(List<TargetTesting> ListOfTargetTesting) {
        this.ListOfTargetTesting = ListOfTargetTesting;
    }

    public List<Attachment> getAttachments() {
        return Attachments;
    }

    public void setAttachments(List<Attachment> Attachments) {
        this.Attachments = Attachments;
    }

    public String getSignPlaceName() {
        return SignPlaceName;
    }

    public void setSignPlaceName(String SignPlaceName) {
        this.SignPlaceName = SignPlaceName;
    }

    public String getNameOfState() {
        return NameOfState;
    }

    public void setNameOfState(String NameOfState) {
        this.NameOfState = NameOfState;
    }

    public String getNSWFileCode() {
        return NSWFileCode;
    }

    public String getRapidTestNo() {
        return RapidTestNo;
    }

    public String getTaxCode() {
        return TaxCode;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public String getBusinessAddress() {
        return BusinessAddress;
    }

    public String getBusinessPhone() {
        return BusinessPhone;
    }

    public String getBusinessFax() {
        return BusinessFax;
    }

    public String getBusinessEmail() {
        return BusinessEmail;
    }

    public String getRapidTestName() {
        return RapidTestName;
    }

    public String getRapidTestCode() {
        return RapidTestCode;
    }

    public String getPlaceOfManufacture() {
        return PlaceOfManufacture;
    }

    public String getManufacture() {
        return Manufacture;
    }

    public String getAddressOfManufacture() {
        return AddressOfManufacture;
    }

    public String getStateOfManufacture() {
        return StateOfManufacture;
    }

    public String getPropertiesTests() {
        return PropertiesTests;
    }

    public String getOperatingPrinciples() {
        return OperatingPrinciples;
    }

    public String getDescription() {
        return Description;
    }

    public String getPackaging() {
        return Packaging;
    }

    public String getShelfLife() {
        return ShelfLife;
    }

    public String getStorageConditions() {
        return StorageConditions;
    }

    public String getOtherInfos() {
        return OtherInfos;
    }

    public String getSignPlace() {
        return SignPlace;
    }

    public String getSignDate() {
        return SignDate;
    }

    public String getSignName() {
        return SignName;
    }

    public void setNSWFileCode(String NSWFileCode) {
        this.NSWFileCode = NSWFileCode;
    }

    public void setRapidTestNo(String RapidTestNo) {
        this.RapidTestNo = RapidTestNo;
    }

    public void setTaxCode(String TaxCode) {
        this.TaxCode = TaxCode;
    }

    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    public void setBusinessAddress(String BusinessAddress) {
        this.BusinessAddress = BusinessAddress;
    }

    public void setBusinessPhone(String BusinessPhone) {
        this.BusinessPhone = BusinessPhone;
    }

    public void setBusinessFax(String BusinessFax) {
        this.BusinessFax = BusinessFax;
    }

    public void setBusinessEmail(String BusinessEmail) {
        this.BusinessEmail = BusinessEmail;
    }

    public void setRapidTestName(String RapidTestName) {
        this.RapidTestName = RapidTestName;
    }

    public void setRapidTestCode(String RapidTestCode) {
        this.RapidTestCode = RapidTestCode;
    }

    public void setPlaceOfManufacture(String PlaceOfManufacture) {
        this.PlaceOfManufacture = PlaceOfManufacture;
    }

    public void setManufacture(String Manufacture) {
        this.Manufacture = Manufacture;
    }

    public void setAddressOfManufacture(String AddressOfManufacture) {
        this.AddressOfManufacture = AddressOfManufacture;
    }

    public void setStateOfManufacture(String StateOfManufacture) {
        this.StateOfManufacture = StateOfManufacture;
    }

    public void setPropertiesTests(String PropertiesTests) {
        this.PropertiesTests = PropertiesTests;
    }

    public void setOperatingPrinciples(String OperatingPrinciples) {
        this.OperatingPrinciples = OperatingPrinciples;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setPackaging(String Packaging) {
        this.Packaging = Packaging;
    }

    public void setShelfLife(String ShelfLife) {
        this.ShelfLife = ShelfLife;
    }

    public void setStorageConditions(String StorageConditions) {
        this.StorageConditions = StorageConditions;
    }

    public void setOtherInfos(String OtherInfos) {
        this.OtherInfos = OtherInfos;
    }

    public void setSignPlace(String SignPlace) {
        this.SignPlace = SignPlace;
    }

    public void setSignDate(String SignDate) {
        this.SignDate = SignDate;
    }

    public void setSignName(String SignName) {
        this.SignName = SignName;
    }

}
