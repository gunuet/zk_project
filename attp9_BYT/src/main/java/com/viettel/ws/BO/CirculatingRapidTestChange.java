/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "CirculatingRapidTestChange")
@XmlAccessorType(XmlAccessType.FIELD)
public class CirculatingRapidTestChange {

    public CirculatingRapidTestChange() {

    }
    @XmlElement(name = "NSWFileCode")
    private String NSWFileCode;
    @XmlElement(name = "RapidTestChangeNo")
    private String RapidTestChangeNo;
    @XmlElement(name = "TaxCode")
    private String TaxCode;
    @XmlElement(name = "BusinessName")
    private String BusinessName;
    @XmlElement(name = "BusinessAddress")
    private String BusinessAddress;
    @XmlElement(name = "BusinessPhone")
    private String BusinessPhone;
    @XmlElement(name = "BusinessFax")
    private String BusinessFax;
    @XmlElement(name = "BusinessEmail")
    private String BusinessEmail;
    @XmlElement(name = "RapidTestName")
    private String RapidTestName;
    @XmlElement(name = "RapidTestCode")
    private String RapidTestCode;
    @XmlElement(name = "CirculatingRapidTestNo")
    private String CirculatingRapidTestNo;
    @XmlElement(name = "DateIssue")
    private String DateIssue;
    @XmlElement(name = "Contents")
    private String Contents;
    @XmlElement(name = "SignPlace")
    private String SignPlace;
    @XmlElement(name = "SignPlaceName")
    private String SignPlaceName;
    @XmlElement(name = "SignDate")
    private String SignDate;
    @XmlElement(name = "SignName")
    private String SignName;
    @XmlElementWrapper(name = "Attachments")
    @XmlElement(name = "Attachment")
    private List<Attachment> Attachments;

    public List<Attachment> getAttachments() {
        return Attachments;
    }

    public void setAttachments(List<Attachment> Attachments) {
        this.Attachments = Attachments;
    }

    public String getSignPlaceName() {
        return SignPlaceName;
    }

    public void setSignPlaceName(String SignPlaceName) {
        this.SignPlaceName = SignPlaceName;
    }

    public String getNSWFileCode() {
        return NSWFileCode;
    }

    public String getRapidTestChangeNo() {
        return RapidTestChangeNo;
    }

    public String getTaxCode() {
        return TaxCode;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public String getBusinessAddress() {
        return BusinessAddress;
    }

    public String getBusinessPhone() {
        return BusinessPhone;
    }

    public String getBusinessFax() {
        return BusinessFax;
    }

    public String getBusinessEmail() {
        return BusinessEmail;
    }

    public String getRapidTestName() {
        return RapidTestName;
    }

    public String getRapidTestCode() {
        return RapidTestCode;
    }

    public String getCirculatingRapidTestNo() {
        return CirculatingRapidTestNo;
    }

    public String getDateIssue() {
        return DateIssue;
    }

    public String getContents() {
        return Contents;
    }

    public String getSignPlace() {
        return SignPlace;
    }

    public String getSignDate() {
        return SignDate;
    }

    public String getSignName() {
        return SignName;
    }

    public void setNSWFileCode(String NSWFileCode) {
        this.NSWFileCode = NSWFileCode;
    }

    public void setRapidTestChangeNo(String RapidTestChangeNo) {
        this.RapidTestChangeNo = RapidTestChangeNo;
    }

    public void setTaxCode(String TaxCode) {
        this.TaxCode = TaxCode;
    }

    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    public void setBusinessAddress(String BusinessAddress) {
        this.BusinessAddress = BusinessAddress;
    }

    public void setBusinessPhone(String BusinessPhone) {
        this.BusinessPhone = BusinessPhone;
    }

    public void setBusinessFax(String BusinessFax) {
        this.BusinessFax = BusinessFax;
    }

    public void setBusinessEmail(String BusinessEmail) {
        this.BusinessEmail = BusinessEmail;
    }

    public void setRapidTestName(String RapidTestName) {
        this.RapidTestName = RapidTestName;
    }

    public void setRapidTestCode(String RapidTestCode) {
        this.RapidTestCode = RapidTestCode;
    }

    public void setCirculatingRapidTestNo(String CirculatingRapidTestNo) {
        this.CirculatingRapidTestNo = CirculatingRapidTestNo;
    }

    public void setDateIssue(String DateIssue) {
        this.DateIssue = DateIssue;
    }

    public void setContents(String Contents) {
        this.Contents = Contents;
    }

    public void setSignPlace(String SignPlace) {
        this.SignPlace = SignPlace;
    }

    public void setSignDate(String SignDate) {
        this.SignDate = SignDate;
    }

    public void setSignName(String SignName) {
        this.SignName = SignName;
    }

}
