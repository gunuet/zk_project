/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CirculatingRapidTestExtension")
public class CirculatingRapidTestExtension {

    public CirculatingRapidTestExtension() {

    }
    @XmlElement(name = "NSWFileCode")
    private String NSWFileCode;
    @XmlElement(name = "RapidTestExtensionNo")
    private String RapidTestExtensionNo;
    @XmlElement(name = "TaxCode")
    private String TaxCode;
    @XmlElement(name = "BusinessName")
    private String BusinessName;
    @XmlElement(name = "BusinessAddress")
    private String BusinessAddress;
    @XmlElement(name = "BusinessPhone")
    private String BusinessPhone;
    @XmlElement(name = "BusinessFax")
    private String BusinessFax;
    @XmlElement(name = "BusinessEmail")
    private String BusinessEmail;
    @XmlElement(name = "RapidTestName")
    private String RapidTestName;
    @XmlElement(name = "CirculatingRapidTestNo")
    private String CirculatingRapidTestNo;
    @XmlElement(name = "DateIssue")
    private String DateIssue;
    @XmlElement(name = "DateEffect")
    private String DateEffect;
    @XmlElement(name = "ExtensionNo")
    private String ExtensionNo;
    @XmlElement(name = "AttachmentsInfo")
    private String AttachmentsInfo;
    @XmlElement(name = "SignPlace")
    private String SignPlace;
    @XmlElement(name = "SignPlaceName")
    private String SignPlaceName;
    @XmlElement(name = "SignDate")
    private String SignDate;
    @XmlElement(name = "SignName")
    private String SignName;
    @XmlElementWrapper(name = "Attachments")
    @XmlElement(name = "Attachment")
    private List<Attachment> Attachments;

    public String getNSWFileCode() {
        return NSWFileCode;
    }

    public String getRapidTestExtensionNo() {
        return RapidTestExtensionNo;
    }

    public String getTaxCode() {
        return TaxCode;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public String getBusinessAddress() {
        return BusinessAddress;
    }

    public String getBusinessPhone() {
        return BusinessPhone;
    }

    public String getBusinessFax() {
        return BusinessFax;
    }

    public String getBusinessEmail() {
        return BusinessEmail;
    }

    public String getRapidTestName() {
        return RapidTestName;
    }

    public String getCirculatingRapidTestNo() {
        return CirculatingRapidTestNo;
    }

    public String getDateIssue() {
        return DateIssue;
    }

    public String getDateEffect() {
        return DateEffect;
    }

    public String getExtensionNo() {
        return ExtensionNo;
    }

    public String getAttachmentsInfo() {
        return AttachmentsInfo;
    }

    public String getSignPlace() {
        return SignPlace;
    }

    public String getSignPlaceName() {
        return SignPlaceName;
    }

    public void setSignPlaceName(String SignPlaceName) {
        this.SignPlaceName = SignPlaceName;
    }

    public String getSignDate() {
        return SignDate;
    }

    public String getSignName() {
        return SignName;
    }

    public List<Attachment> getAttachments() {
        return Attachments;
    }

    public void setNSWFileCode(String NSWFileCode) {
        this.NSWFileCode = NSWFileCode;
    }

    public void setRapidTestExtensionNo(String RapidTestExtensionNo) {
        this.RapidTestExtensionNo = RapidTestExtensionNo;
    }

    public void setTaxCode(String TaxCode) {
        this.TaxCode = TaxCode;
    }

    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    public void setBusinessAddress(String BusinessAddress) {
        this.BusinessAddress = BusinessAddress;
    }

    public void setBusinessPhone(String BusinessPhone) {
        this.BusinessPhone = BusinessPhone;
    }

    public void setBusinessFax(String BusinessFax) {
        this.BusinessFax = BusinessFax;
    }

    public void setBusinessEmail(String BusinessEmail) {
        this.BusinessEmail = BusinessEmail;
    }

    public void setRapidTestName(String RapidTestName) {
        this.RapidTestName = RapidTestName;
    }

    public void setCirculatingRapidTestNo(String CirculatingRapidTestNo) {
        this.CirculatingRapidTestNo = CirculatingRapidTestNo;
    }

    public void setDateIssue(String DateIssue) {
        this.DateIssue = DateIssue;
    }

    public void setDateEffect(String DateEffect) {
        this.DateEffect = DateEffect;
    }

    public void setExtensionNo(String ExtensionNo) {
        this.ExtensionNo = ExtensionNo;
    }

    public void setAttachmentsInfo(String AttachmentsInfo) {
        this.AttachmentsInfo = AttachmentsInfo;
    }

    public void setSignPlace(String SignPlace) {
        this.SignPlace = SignPlace;
    }

    public void setSignDate(String SignDate) {
        this.SignDate = SignDate;
    }

    public void setSignName(String SignName) {
        this.SignName = SignName;
    }

    public void setAttachments(List<Attachment> Attachments) {
        this.Attachments = Attachments;
    }

}
