/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Content")
public class Content {

    public Content() {
    }
    @XmlElement(name = "ImportDocument")
    private ImportDocument ImportDocument;
    @XmlElement(name = "Document")
    private Document Document;
    @XmlElement(name = "Response")
    private Response Response;
    @XmlElement(name = "FileResults")
    private FileResults FileResults;
    @XmlElement(name = "FilesCost")
    private FilesCost FilesCost;
    @XmlElement(name = "FilesEdit")
    private FilesEdit FilesEdit;
    @XmlElementWrapper(name = "ErrorList")
    @XmlElement(name = "Error")
    private List<Error> ErrorList;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;
    @XmlElement(name = "ReceiveDate")
    private String ReceiveDate;
//Xét nghiệm nhanh bs
    @XmlElement(name = "CirculatingRapidTest")
    private CirculatingRapidTest CirculatingRapidTest;
    @XmlElement(name = "CirculatingRapidTestChange")
    private CirculatingRapidTestChange CirculatingRapidTestChange;
    @XmlElement(name = "CirculatingRapidTestExtension")
    private CirculatingRapidTestExtension CirculatingRapidTestExtension;
    @XmlElement(name = "PromissoryNote")
    private PromissoryNote PromissoryNote;
    @XmlElement(name = "Amendment")
    private Amendment Amendment;
    @XmlElement(name = "RejectCertificate")
    private RejectCertificate RejectCertificate;
    @XmlElement(name = "CertifiedCirculation")
    private CertifiedCirculation CertifiedCirculation;
    @XmlElement(name = "ExtensionCirculation")
    private ExtensionCirculation ExtensionCirculation;
    @XmlElement(name = "RequireFees")
    private RequireFees RequireFees;
    @XmlElement(name = "InfoFees")
    private InfoFees InfoFees;
    @XmlElement(name = "Recovery")
    private Recovery Recovery;
    @XmlElement(name = "RequestAttachment")
    private RequestAttachment RequestAttachment;
    @XmlElement(name = "ResponseAttachment")
    private ResponseAttachment ResponseAttachment;
    @XmlElement(name = "FileAttach")
    private FileAttach FileAttach;
    @XmlElement(name = "NoticeResult")
    private NoticeResult NoticeResult;
    @XmlElement(name = "NoticeCancle")
    private NoticeCancle NoticeCancle;

    @XmlElement(name = "Sync")
    private Sync Sync;

    @XmlElement(name = "GetStatus")
    protected GetStatus getStatus;
    @XmlElement(name = "ReturnStatus")
    protected ReturnStatus returnStatus;

    @XmlElement(name = "SendVoffice")
    protected SendVoffice sendVoffice;

    public void setResponseAttachment(ResponseAttachment ResponseAttachment) {
        this.ResponseAttachment = ResponseAttachment;
    }

    public ResponseAttachment getResponseAttachment() {
        return ResponseAttachment;
    }

    public void setRequestAttachment(RequestAttachment RequestAttachment) {
        this.RequestAttachment = RequestAttachment;
    }

    public RequestAttachment getRequestAttachment() {
        return RequestAttachment;
    }

    public void setRecovery(Recovery Recovery) {
        this.Recovery = Recovery;
    }

    public Recovery getRecovery() {
        return Recovery;
    }

    public void setInfoFees(InfoFees InfoFees) {
        this.InfoFees = InfoFees;
    }

    public InfoFees getInfoFees() {
        return InfoFees;
    }

    public void setExtensionCirculation(ExtensionCirculation ExtensionCirculation) {
        this.ExtensionCirculation = ExtensionCirculation;
    }

    public ExtensionCirculation getExtensionCirculation() {
        return ExtensionCirculation;
    }

    public void setRequireFees(RequireFees RequireFees) {
        this.RequireFees = RequireFees;
    }

    public RequireFees getRequireFees() {
        return RequireFees;
    }

    public void setCertifiedCirculation(CertifiedCirculation CertifiedCirculation) {
        this.CertifiedCirculation = CertifiedCirculation;
    }

    public CertifiedCirculation getCertifiedCirculation() {
        return CertifiedCirculation;
    }

    public void setRejectCertificate(RejectCertificate RejectCertificate) {
        this.RejectCertificate = RejectCertificate;
    }

    public RejectCertificate getRejectCertificate() {
        return RejectCertificate;
    }

    public void setAmendment(Amendment Amendment) {
        this.Amendment = Amendment;
    }

    public Amendment getAmendment() {
        return Amendment;
    }

    public void setPromissoryNote(PromissoryNote PromissoryNote) {
        this.PromissoryNote = PromissoryNote;
    }

    public PromissoryNote getPromissoryNote() {
        return PromissoryNote;
    }

    public void setCirculatingRapidTestExtension(CirculatingRapidTestExtension CirculatingRapidTestExtension) {
        this.CirculatingRapidTestExtension = CirculatingRapidTestExtension;
    }

    public CirculatingRapidTestExtension getCirculatingRapidTestExtension() {
        return CirculatingRapidTestExtension;
    }

    public void setCirculatingRapidTestChange(CirculatingRapidTestChange CirculatingRapidTestChange) {
        this.CirculatingRapidTestChange = CirculatingRapidTestChange;
    }

    public CirculatingRapidTestChange getCirculatingRapidTestChange() {
        return CirculatingRapidTestChange;
    }

    public void setCirculatingRapidTest(CirculatingRapidTest CirculatingRapidTest) {
        this.CirculatingRapidTest = CirculatingRapidTest;
    }

    public CirculatingRapidTest getCirculatingRapidTest() {
        return CirculatingRapidTest;
    }

    public void setReceiveDate(String ReceiveDate) {
        this.ReceiveDate = ReceiveDate;
    }

    public String getReceiveDate() {
        return ReceiveDate;
    }

    public void setImportDocument(ImportDocument ImportDocument) {
        this.ImportDocument = ImportDocument;
    }

    public ImportDocument getImportDocument() {
        return ImportDocument;
    }

    public void setFileResults(FileResults FileResults) {
        this.FileResults = FileResults;
    }

    public FileResults getFileResults() {
        return FileResults;
    }

    public FilesCost getFilesCost() {
        return FilesCost;
    }

    public void setFilesCost(FilesCost FilesCost) {
        this.FilesCost = FilesCost;
    }

    public FilesEdit getFilesEdit() {
        return FilesEdit;
    }

    public void setFilesEdit(FilesEdit FilesEdit) {
        this.FilesEdit = FilesEdit;
    }

    public void setErrorList(List<Error> ErrorList) {
        this.ErrorList = ErrorList;
    }

    public List<Error> getErrorList() {
        return ErrorList;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public Attachment getAttachment() {
        return Attachment;
    }

    /**
     * @return the Document
     */
    public Document getDocument() {
        return Document;
    }

    /**
     * @param Document the Document to set
     */
    public void setDocument(Document Document) {
        this.Document = Document;
    }

    /**
     * @return the Response
     */
    public Response getResponse() {
        return Response;
    }

    /**
     * @param Response the Response to set
     */
    public void setResponse(Response Response) {
        this.Response = Response;
    }

    /**
     * @return the FileAttach
     */
    public FileAttach getFileAttach() {
        return FileAttach;
    }

    /**
     * @param FileAttach the FileAttach to set
     */
    public void setFileAttach(FileAttach FileAttach) {
        this.FileAttach = FileAttach;
    }

    public com.viettel.ws.BO.NoticeResult getNoticeResult() {
        return NoticeResult;
    }

    public void setNoticeResult(com.viettel.ws.BO.NoticeResult NoticeResult) {
        this.NoticeResult = NoticeResult;
    }

    public com.viettel.ws.BO.NoticeCancle getNoticeCancle() {
        return NoticeCancle;
    }

    public void setNoticeCancle(com.viettel.ws.BO.NoticeCancle NoticeCancle) {
        this.NoticeCancle = NoticeCancle;
    }

    public Sync getSync() {
        return Sync;
    }

    public void setSync(Sync Sync) {
        this.Sync = Sync;
    }

    public GetStatus getGetStatus() {
        return getStatus;
    }

    public void setGetStatus(GetStatus getStatus) {
        this.getStatus = getStatus;
    }

    public ReturnStatus getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(ReturnStatus returnStatus) {
        this.returnStatus = returnStatus;
    }

    public SendVoffice getSendVoffice() {
        return sendVoffice;
    }

    public void setSendVoffice(SendVoffice sendVoffice) {
        this.sendVoffice = sendVoffice;
    }

}
