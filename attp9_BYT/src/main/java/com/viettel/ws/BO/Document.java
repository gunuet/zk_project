/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document")
public class Document {

    @XmlElement(name = "From")
    private String From;
    @XmlElement(name = "Content")
    private String Content;
    @XmlElement(name = "SignName")
    private String SignName;
    @XmlElement(name = "UserSign")
    private String UserSign;
    @XmlElement(name = "Editor")
    private String Editor;
    @XmlElement(name = "EditorName")
    private String EditorName;
    @XmlElement(name = "SignDate")
    private String SignDate;
    @XmlElement(name = "GroupProductName")
    private String GroupProductName;
    @XmlElement(name = "GroupProductCode")
    private String GroupProductCode;
    @XmlElement(name = "DirectorName")
    private String DirectorName;
    @XmlElement(name = "DirectorPhone")
    private String DirectorPhone;
    @XmlElement(name = "DirectorMobiPhone")
    private String DirectorMobiPhone;
    @XmlElement(name = "SignPlace")
    private String SignPlace;
    @XmlElement(name = "ReceiveId")
    private String ReceiveId;
    @XmlElement(name = "SendId")
    private String SendId;
    @XmlElement(name = "FileAttach")
    private FileAttach FileAttach;
    @XmlElement(name = "FileCode")
    private String FileCode;
    @XmlElement(name = "TaxCode")
    private String TaxCode;
    @XmlElement(name = "BusinessName")
    private String BusinessName;
    @XmlElement(name = "BusinessAddress")
    private String BusinessAddress;
    @XmlElement(name = "BusinessPhone")
    private String BusinessPhone;
    @XmlElement(name = "BusinessFax")
    private String BusinessFax;
    @XmlElement(name = "OfficersName")
    private String OfficersName;
    @XmlElement(name = "OfficersPhone")
    private String OfficersPhone;
    @XmlElement(name = "OfficersMobile")
    private String OfficersMobile;
    @XmlElement(name = "ReceiveBookNumber")
    private String ReceiveBookNumber;
    @XmlElement(name = "IntendedUse")
    private String IntendedUse;
    @XmlElement(name = "Price")
    private String Price;
    @XmlElement(name = "PriceCode")
    private String PriceCode;
    @XmlElement(name = "SendBookNumber")
    private String SendBookNumber;
    @XmlElement(name = "Type")
    private String Type;
    @XmlElement(name = "ProductName")
    private String ProductName;
    @XmlElementWrapper(name = "Products")
    @XmlElement(name = "Product")
    private List<Product> Products;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;
    @XmlElementWrapper(name = "Attachments")
    @XmlElement(name = "Attachment")
    private List<Attachment> Attachments;
    @XmlElement(name = "PermitSignDate")
    private String PermitSignDate;
    @XmlElement(name = "PermitSignName")
    private String PermitSignName;
    @XmlElement(name = "PermitPosName")
    private String PermitPosName;
    @XmlElement(name = "EquipmentNo")
    private String EquipmentNo;
//Huy ho so TTB
    @XmlElement(name = "Reasons")
    private String Reasons;
    @XmlElement(name = "ProcessName")
    private String ProcessName;
    @XmlElement(name = "Position")
    private String Position;
    @XmlElement(name = "NswFileCode")
    private String NswFileCode;
    @XmlElement(name = "ExpAuthoriSation")
    private String ExpAuthoriSation;
    @XmlElement(name = "FileType")
    private String FileType;
    @XmlElement(name = "IsChange")
    private String IsChange;
    @XmlElement(name = "Notes")
    private String Notes;

    public Document() {
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String From) {
        this.From = From;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public String getSignName() {
        return SignName;
    }

    public void setSignName(String SignName) {
        this.SignName = SignName;
    }

    public String getUserSign() {
        return UserSign;
    }

    public void setUserSign(String UserSign) {
        this.UserSign = UserSign;
    }

    public String getSignDate() {
        return SignDate;
    }

    public void setSignDate(String SignDate) {
        this.SignDate = SignDate;
    }

    public String getReceiveId() {
        return ReceiveId;
    }

    public void setReceiveId(String ReceiveId) {
        this.ReceiveId = ReceiveId;
    }

    public String getSendId() {
        return SendId;
    }

    public void setSendId(String SendId) {
        this.SendId = SendId;
    }

    public FileAttach getFileAttach() {
        return FileAttach;
    }

    public void setFileAttach(FileAttach FileAttach) {
        this.FileAttach = FileAttach;
    }

    public String getFileCode() {
        return FileCode;
    }

    public void setFileCode(String FileCode) {
        this.FileCode = FileCode;
    }

    public String getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(String TaxCode) {
        this.TaxCode = TaxCode;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    public String getBusinessAddress() {
        return BusinessAddress;
    }

    public void setBusinessAddress(String BusinessAddress) {
        this.BusinessAddress = BusinessAddress;
    }

    public String getBusinessPhone() {
        return BusinessPhone;
    }

    public void setBusinessPhone(String BusinessPhone) {
        this.BusinessPhone = BusinessPhone;
    }

    public String getBusinessFax() {
        return BusinessFax;
    }

    public void setBusinessFax(String BusinessFax) {
        this.BusinessFax = BusinessFax;
    }

    public String getOfficersName() {
        return OfficersName;
    }

    public void setOfficersName(String OfficersName) {
        this.OfficersName = OfficersName;
    }

    public String getOfficersPhone() {
        return OfficersPhone;
    }

    public void setOfficersPhone(String OfficersPhone) {
        this.OfficersPhone = OfficersPhone;
    }

    public String getOfficersMobile() {
        return OfficersMobile;
    }

    public void setOfficersMobile(String OfficersMobile) {
        this.OfficersMobile = OfficersMobile;
    }

    public String getReceiveBookNumber() {
        return ReceiveBookNumber;
    }

    public void setReceiveBookNumber(String ReceiveBookNumber) {
        this.ReceiveBookNumber = ReceiveBookNumber;
    }

    public String getIntendedUse() {
        return IntendedUse;
    }

    public void setIntendedUse(String IntendedUse) {
        this.IntendedUse = IntendedUse;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getPriceCode() {
        return PriceCode;
    }

    public void setPriceCode(String PriceCode) {
        this.PriceCode = PriceCode;
    }

    public String getSendBookNumber() {
        return SendBookNumber;
    }

    public void setSendBookNumber(String SendBookNumber) {
        this.SendBookNumber = SendBookNumber;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public List<Product> getProducts() {
        return Products;
    }

    public void setProducts(List<Product> Products) {
        this.Products = Products;
    }

    public com.viettel.ws.BO.Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(com.viettel.ws.BO.Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public List<com.viettel.ws.BO.Attachment> getAttachments() {
        return Attachments;
    }

    public void setAttachments(List<com.viettel.ws.BO.Attachment> Attachments) {
        this.Attachments = Attachments;
    }

    /**
     * @return the ProductName
     */
    public String getProductName() {
        return ProductName;
    }

    /**
     * @param ProductName the ProductName to set
     */
    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    /**
     * @return the Editor
     */
    public String getEditor() {
        return Editor;
    }

    /**
     * @param Editor the Editor to set
     */
    public void setEditor(String Editor) {
        this.Editor = Editor;
    }

    /**
     * @return the EditorName
     */
    public String getEditorName() {
        return EditorName;
    }

    /**
     * @param EditorName the EditorName to set
     */
    public void setEditorName(String EditorName) {
        this.EditorName = EditorName;
    }

    /**
     * @return the PemitSignDate
     */
    public String getPermitSignDate() {
        return PermitSignDate;
    }

    /**
     * @param PemitSignDate the PemitSignDate to set
     */
    public void setPermitSignDate(String PermitSignDate) {
        this.PermitSignDate = PermitSignDate;
    }

    /**
     * @return the PermitSignName
     */
    public String getPermitSignName() {
        return PermitSignName;
    }

    /**
     * @param PermitSignName the PermitSignName to set
     */
    public void setPermitSignName(String PermitSignName) {
        this.PermitSignName = PermitSignName;
    }

    /**
     * @return the PermitPosName
     */
    public String getPermitPosName() {
        return PermitPosName;
    }

    /**
     * @param PermitPosName the PermitPosName to set
     */
    public void setPermitPosName(String PermitPosName) {
        this.PermitPosName = PermitPosName;
    }

    /**
     * @return the GroupProductName
     */
    public String getGroupProductName() {
        return GroupProductName;
    }

    /**
     * @param GroupProductName the GroupProductName to set
     */
    public void setGroupProductName(String GroupProductName) {
        this.GroupProductName = GroupProductName;
    }

    /**
     * @return the GroupProductCode
     */
    public String getGroupProductCode() {
        return GroupProductCode;
    }

    /**
     * @param GroupProductCode the GroupProductCode to set
     */
    public void setGroupProductCode(String GroupProductCode) {
        this.GroupProductCode = GroupProductCode;
    }

    /**
     * @return the DirectorName
     */
    public String getDirectorName() {
        return DirectorName;
    }

    /**
     * @param DirectorName the DirectorName to set
     */
    public void setDirectorName(String DirectorName) {
        this.DirectorName = DirectorName;
    }

    /**
     * @return the DirectorPhone
     */
    public String getDirectorPhone() {
        return DirectorPhone;
    }

    /**
     * @param DirectorPhone the DirectorPhone to set
     */
    public void setDirectorPhone(String DirectorPhone) {
        this.DirectorPhone = DirectorPhone;
    }

    /**
     * @return the DirectorMobiPhone
     */
    public String getDirectorMobiPhone() {
        return DirectorMobiPhone;
    }

    /**
     * @param DirectorMobiPhone the DirectorMobiPhone to set
     */
    public void setDirectorMobiPhone(String DirectorMobiPhone) {
        this.DirectorMobiPhone = DirectorMobiPhone;
    }

    /**
     * @return the SignPlace
     */
    public String getSignPlace() {
        return SignPlace;
    }

    /**
     * @param SignPlace the SignPlace to set
     */
    public void setSignPlace(String SignPlace) {
        this.SignPlace = SignPlace;
    }

    /**
     * @return the EquipmentNo
     */
    public String getEquipmentNo() {
        return EquipmentNo;
    }

    /**
     * @param EquipmentNo the EquipmentNo to set
     */
    public void setEquipmentNo(String EquipmentNo) {
        this.EquipmentNo = EquipmentNo;
    }

    public String getReasons() {
        return Reasons;
    }

    public void setReasons(String Reasons) {
        this.Reasons = Reasons;
    }

    /**
     * @return the Reason
     */
    /**
     * @return the ProcessName
     */
    public String getProcessName() {
        return ProcessName;
    }

    /**
     * @param ProcessName the ProcessName to set
     */
    public void setProcessName(String ProcessName) {
        this.ProcessName = ProcessName;
    }

    /**
     * @return the Position
     */
    public String getPosition() {
        return Position;
    }

    /**
     * @param Position the Position to set
     */
    public void setPosition(String Position) {
        this.Position = Position;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public String getNswFileCode() {
        return NswFileCode;
    }

    public String getExpAuthoriSation() {
        return ExpAuthoriSation;
    }

    public void setExpAuthoriSation(String ExpAuthoriSation) {
        this.ExpAuthoriSation = ExpAuthoriSation;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String FileType) {
        this.FileType = FileType;
    }

    public String getIsChange() {
        return IsChange;
    }

    public void setIsChange(String IsChange) {
        this.IsChange = IsChange;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String Notes) {
        this.Notes = Notes;
    }
}
