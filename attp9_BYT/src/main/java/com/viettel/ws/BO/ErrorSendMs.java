/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@Entity
@Table(name = "ERROR_SEND_MS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ErrorSendMs.findAll", query = "SELECT e FROM ErrorSendMs e"),
    @NamedQuery(name = "ErrorSendMs.findById", query = "SELECT e FROM ErrorSendMs e WHERE e.id = :id"),
    @NamedQuery(name = "ErrorSendMs.findByMsId", query = "SELECT e FROM ErrorSendMs e WHERE e.msId = :msId"),
    @NamedQuery(name = "ErrorSendMs.findByMsCode", query = "SELECT e FROM ErrorSendMs e WHERE e.msCode = :msCode"),
    @NamedQuery(name = "ErrorSendMs.findByMsFunction", query = "SELECT e FROM ErrorSendMs e WHERE e.msFunction = :msFunction"),
    @NamedQuery(name = "ErrorSendMs.findByFileId", query = "SELECT e FROM ErrorSendMs e WHERE e.fileId = :fileId"),
    @NamedQuery(name = "ErrorSendMs.findByCreatedDate", query = "SELECT e FROM ErrorSendMs e WHERE e.createdDate = :createdDate"),
    @NamedQuery(name = "ErrorSendMs.findByMsPhase", query = "SELECT e FROM ErrorSendMs e WHERE e.msPhase = :msPhase"),
    @NamedQuery(name = "ErrorSendMs.findByMsUpdate", query = "SELECT e FROM ErrorSendMs e WHERE e.msUpdate = :msUpdate"),
    @NamedQuery(name = "ErrorSendMs.findByActions", query = "SELECT e FROM ErrorSendMs e WHERE e.actions = :actions")})
public class ErrorSendMs implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @SequenceGenerator(name = "ERROR_SEND_MS_SEQ", sequenceName = "ERROR_SEND_MS_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ERROR_SEND_MS_SEQ")

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 400)
    @Column(name = "MS_ID")
    private String msId;
    @Size(max = 400)
    @Column(name = "MS_CODE")
    private String msCode;
    @Size(max = 400)
    @Column(name = "MS_FUNCTION")
    private String msFunction;
    @Column(name = "FILE_ID")
    private Long fileId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    @Size(max = 20)
    @Column(name = "MS_PHASE")
    private String msPhase;
    @Column(name = "MS_UPDATE")
    private Long msUpdate;
    @Size(max = 500)
    @Column(name = "ACTIONS")
    private String actions;

    public ErrorSendMs() {
    }

    public ErrorSendMs(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsId() {
        return msId;
    }

    public void setMsId(String msId) {
        this.msId = msId;
    }

    public String getMsCode() {
        return msCode;
    }

    public void setMsCode(String msCode) {
        this.msCode = msCode;
    }

    public String getMsFunction() {
        return msFunction;
    }

    public void setMsFunction(String msFunction) {
        this.msFunction = msFunction;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getMsPhase() {
        return msPhase;
    }

    public void setMsPhase(String msPhase) {
        this.msPhase = msPhase;
    }

    public Long getMsUpdate() {
        return msUpdate;
    }

    public void setMsUpdate(Long msUpdate) {
        this.msUpdate = msUpdate;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ErrorSendMs)) {
            return false;
        }
        ErrorSendMs other = (ErrorSendMs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.ws.BO.ErrorSendMs[ id=" + id + " ]";
    }

}
