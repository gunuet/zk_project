/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "ExtensionCirculation")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtensionCirculation {

    public ExtensionCirculation() {

    }
    @XmlElement(name = "CirculatingNo")
    private String CirculatingNo;
    @XmlElement(name = "ExtensionNo")
    private String ExtensionNo;
    @XmlElement(name = "CirculatingRapidTestNo")
    private String CirculatingRapidTestNo;
    @XmlElement(name = "CirculatingRapidTestDate")
    private String CirculatingRapidTestDate;
    @XmlElement(name = "RapidTestName")
    private String RapidTestName;
    @XmlElement(name = "BusinessName")
    private String BusinessName;
    @XmlElement(name = "BusinessAddress")
    private String BusinessAddress;
    @XmlElement(name = "BusinessPhone")
    private String BusinessPhone;
    @XmlElement(name = "BusinessFax")
    private String BusinessFax;
    @XmlElement(name = "BusinessEmail")
    private String BusinessEmail;
    @XmlElement(name = "SignRank")
    private String SignRank;
    @XmlElement(name = "SignPlace")
    private String SignPlace;
    @XmlElement(name = "SignDate")
    private String SignDate;
    @XmlElement(name = "SignName")
    private String SignName;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;

    public Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public String getCirculatingNo() {
        return CirculatingNo;
    }

    public String getExtensionNo() {
        return ExtensionNo;
    }

    public String getCirculatingRapidTestNo() {
        return CirculatingRapidTestNo;
    }

    public String getCirculatingRapidTestDate() {
        return CirculatingRapidTestDate;
    }

    public String getRapidTestName() {
        return RapidTestName;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public String getBusinessAddress() {
        return BusinessAddress;
    }

    public String getBusinessPhone() {
        return BusinessPhone;
    }

    public String getBusinessFax() {
        return BusinessFax;
    }

    public String getBusinessEmail() {
        return BusinessEmail;
    }

    public String getSignRank() {
        return SignRank;
    }

    public String getSignPlace() {
        return SignPlace;
    }

    public String getSignDate() {
        return SignDate;
    }

    public String getSignName() {
        return SignName;
    }

    public void setCirculatingNo(String CirculatingNo) {
        this.CirculatingNo = CirculatingNo;
    }

    public void setExtensionNo(String ExtensionNo) {
        this.ExtensionNo = ExtensionNo;
    }

    public void setCirculatingRapidTestNo(String CirculatingRapidTestNo) {
        this.CirculatingRapidTestNo = CirculatingRapidTestNo;
    }

    public void setCirculatingRapidTestDate(String CirculatingRapidTestDate) {
        this.CirculatingRapidTestDate = CirculatingRapidTestDate;
    }

    public void setRapidTestName(String RapidTestName) {
        this.RapidTestName = RapidTestName;
    }

    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    public void setBusinessAddress(String BusinessAddress) {
        this.BusinessAddress = BusinessAddress;
    }

    public void setBusinessPhone(String BusinessPhone) {
        this.BusinessPhone = BusinessPhone;
    }

    public void setBusinessFax(String BusinessFax) {
        this.BusinessFax = BusinessFax;
    }

    public void setBusinessEmail(String BusinessEmail) {
        this.BusinessEmail = BusinessEmail;
    }

    public void setSignRank(String SignRank) {
        this.SignRank = SignRank;
    }

    public void setSignPlace(String SignPlace) {
        this.SignPlace = SignPlace;
    }

    public void setSignDate(String SignDate) {
        this.SignDate = SignDate;
    }

    public void setSignName(String SignName) {
        this.SignName = SignName;
    }

}
