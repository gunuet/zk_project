/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "Fee")
@XmlAccessorType(XmlAccessType.FIELD)
    public class Fee {

        @XmlElement(name = "FeeId")
        private String FeeId;
        @XmlElement(name = "Cost")
        private String Cost;
        @XmlElement(name = "BankNo")
        private String BankNo;
        @XmlElement(name = "DeptCode")
        private String DeptCode;
        @XmlElement(name = "DeptName")
        private String DeptName;
        @XmlElement(name = "Comments")
        private String Comments;
        //Bổ sung giấy phép
//        @XmlElement(name = "Pass")
//        private String Pass;
//        @XmlElement(name = "Reason")
//        private String Reason;

        public Fee() {

        }

//        public String getPass() {
//            return Pass;
//        }
//
//        public void setPass(String Pass) {
//            this.Pass = Pass;
//        }
//
//        public void setReason(String Reason) {
//            this.Reason = Reason;
//        }
//
//        public String getReason() {
//            return Reason;
//        }

        public String getFeeId() {
            return FeeId;
        }

        public String getCost() {
            return Cost;
        }

        public String getBankNo() {
            return BankNo;
        }

        public String getDeptCode() {
            return DeptCode;
        }

        public String getDeptName() {
            return DeptName;
        }

        public String getComments() {
            return Comments;
        }

        public void setFeeId(String FeeId) {
            this.FeeId = FeeId;
        }

        public void setCost(String Cost) {
            this.Cost = Cost;
        }

        public void setBankNo(String BankNo) {
            this.BankNo = BankNo;
        }

        public void setDeptCode(String DeptCode) {
            this.DeptCode = DeptCode;
        }

        public void setDeptName(String DeptName) {
            this.DeptName = DeptName;
        }

        public void setComments(String Comments) {
            this.Comments = Comments;
        }

    }
