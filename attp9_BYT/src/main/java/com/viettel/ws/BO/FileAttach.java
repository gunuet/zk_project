/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileAttach")
public class FileAttach {

    @XmlElement(name = "FileName")
    private String FileName;
    @XmlElement(name = "FileByte")
    private String FileByte;
    @XmlElement(name = "FileCode")
    private String FileCode;
    public String getFileName() {
        return FileName;
    }

    public void setFileName(String FileName) {
        this.FileName = FileName;
    }

    public String getFileByte() {
        return FileByte;
    }

    public void setFileByte(String FileByte) {
        this.FileByte = FileByte;
    }

    /**
     * @return the FileCode
     */
    public String getFileCode() {
        return FileCode;
    }

    /**
     * @param FileCode the FileCode to set
     */
    public void setFileCode(String FileCode) {
        this.FileCode = FileCode;
    }

}
