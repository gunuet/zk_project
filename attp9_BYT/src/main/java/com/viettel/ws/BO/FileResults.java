/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="FileResults")
public class FileResults {

    public FileResults() {

    }
    @XmlElement(name = "NswFileCode")
    private String NswFileCode;
    @XmlElement(name = "DeptCode")
    private String DeptCode;
    @XmlElement(name = "DeptName")
    private String DeptName;
    @XmlElement(name = "Comments")
    private String Comments;
    @XmlElementWrapper(name = "ProductList")
    @XmlElement(name = "Product")
    private List<Product> ProductList;
//Bổ sung thông báo thời gian, kế hoạch kiểm tra
    @XmlElement(name = "CheckDate")
    private String CheckDate;
    @XmlElement(name = "CheckPlace")
    private String CheckPlace;
    @XmlElement(name = "ProcessName")
    private String ProcessName;
    @XmlElement(name = "ProcessPhone")
    private String ProcessPhone;
//Bổ sung thông tin thông báo thời gian, địa điểm tập kết hàng hóa
    @XmlElement(name = "ContactPhone")
    private String ContactPhone;

    public String getContactPhone() {
        return ContactPhone;
    }

    public void setContactPhone(String ContactPhone) {
        this.ContactPhone = ContactPhone;
    }

    public String getCheckDate() {
        return CheckDate;
    }

    public String getCheckPlace() {
        return CheckPlace;
    }

    public String getProcessName() {
        return ProcessName;
    }

    public String getProcessPhone() {
        return ProcessPhone;
    }

    public void setCheckDate(String CheckDate) {
        this.CheckDate = CheckDate;
    }

    public void setCheckPlace(String CheckPlace) {
        this.CheckPlace = CheckPlace;
    }

    public void setProcessName(String ProcessName) {
        this.ProcessName = ProcessName;
    }

    public void setProcessPhone(String ProcessPhone) {
        this.ProcessPhone = ProcessPhone;
    }

    public String getNswFileCode() {
        return NswFileCode;
    }

    public String getDeptCode() {
        return DeptCode;
    }

    public String getDeptName() {
        return DeptName;
    }

    public String getComments() {
        return Comments;
    }

    public List<Product> getProductList() {
        return ProductList;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public void setDeptCode(String DeptCode) {
        this.DeptCode = DeptCode;
    }

    public void setDeptName(String DeptName) {
        this.DeptName = DeptName;
    }

    public void setComments(String Comments) {
        this.Comments = Comments;
    }

    public void setProductList(List<Product> ProductList) {
        this.ProductList = ProductList;
    }

}
