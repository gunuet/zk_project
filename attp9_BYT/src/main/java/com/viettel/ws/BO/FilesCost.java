/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FilesCost")
public class FilesCost {

    @XmlElement(name = "NswFileCode")
    private String NswFileCode;
    @XmlElement(name = "ProcessDate")
    private String ProcessDate;
    @XmlElement(name = "ProcessName")
    private String ProcessName;
    @XmlElement(name = "Comments")
    private String Comments;
    @XmlElement(name = "Type")
    private String Type;
    @XmlElementWrapper(name = "FeeList")
    @XmlElement(name = "Fee")
    private List<Fee> FeeList;
    @XmlElement(name = "Fee")
    private Fee Fee;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;
    //linhdx file tra ket qua tu NSW
    @XmlElement(name = "PaymentDate")
    private String PaymentDate;
    @XmlElement(name = "PaymentName")
    private String PaymentName;
    @XmlElement(name = "InvoiceNo")
    private String InvoiceNo;
    @XmlElement(name = "TotalFee")
    private String TotalFee;
    @XmlElement(name = "Note")
    private String Note;

    public FilesCost() {
    }

    public Fee getFee() {
        return Fee;
    }

    public void setFee(Fee Fee) {
        this.Fee = Fee;
    }

    public Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public String getNswFileCode() {
        return NswFileCode;
    }

    public String getProcessDate() {
        return ProcessDate;
    }

    public String getProcessName() {
        return ProcessName;
    }

    public String getComments() {
        return Comments;
    }

    public String getType() {
        return Type;
    }

    public List<Fee> getFeeList() {
        return FeeList;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public void setProcessDate(String ProcessDate) {
        this.ProcessDate = ProcessDate;
    }

    public void setProcessName(String ProcessName) {
        this.ProcessName = ProcessName;
    }

    public void setComments(String Comments) {
        this.Comments = Comments;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public void setFeeList(List<Fee> FeeList) {
        this.FeeList = FeeList;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String InvoiceNo) {
        this.InvoiceNo = InvoiceNo;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String Note) {
        this.Note = Note;
    }

    public String getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(String PaymentDate) {
        this.PaymentDate = PaymentDate;
    }

    public String getPaymentName() {
        return PaymentName;
    }

    public void setPaymentName(String PaymentName) {
        this.PaymentName = PaymentName;
    }

    public String getTotalFee() {
        return TotalFee;
    }

    public void setTotalFee(String TotalFee) {
        this.TotalFee = TotalFee;
    }
    
    
}
