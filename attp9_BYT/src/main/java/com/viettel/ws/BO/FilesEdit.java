/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FilesEdit")
public class FilesEdit {

    public FilesEdit() {
    }
    @XmlElement(name = "NswFileCode")
    private String NswFileCode;
    @XmlElement(name = "DeptCode")
    private String DeptCode;
    @XmlElement(name = "DeptName")
    private String DeptName;
    @XmlElement(name = "ProcessDate")
    private String ProcessDate;
    @XmlElement(name = "ProcessName")
    private String ProcessName;
    @XmlElement(name = "Comments")
    private String Comments;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;
    @XmlElement(name = "ExpDate")
    private String ExpDate;

    public Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public String getNswFileCode() {
        return NswFileCode;
    }

    public String getDeptCode() {
        return DeptCode;
    }

    public String getDeptName() {
        return DeptName;
    }

    public String getProcessDate() {
        return ProcessDate;
    }

    public String getProcessName() {
        return ProcessName;
    }

    public String getComments() {
        return Comments;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public void setDeptCode(String DeptCode) {
        this.DeptCode = DeptCode;
    }

    public void setDeptName(String DeptName) {
        this.DeptName = DeptName;
    }

    public void setProcessDate(String ProcessDate) {
        this.ProcessDate = ProcessDate;
    }

    public void setProcessName(String ProcessName) {
        this.ProcessName = ProcessName;
    }

    public void setComments(String Comments) {
        this.Comments = Comments;
    }

    public String getExpDate() {
        return ExpDate;
    }

    public void setExpDate(String ExpDate) {
        this.ExpDate = ExpDate;
    }
}
