package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * NhoDVT
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStatus")
public class GetStatus {

    public GetStatus() {
    }
    @XmlElement(name = "NswFileCode")
    protected String NswFileCode;

    public String getNswFileCode() {
        return NswFileCode;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    
}
