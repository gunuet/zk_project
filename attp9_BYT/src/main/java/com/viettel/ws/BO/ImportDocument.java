/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImportDocument")
public class ImportDocument {

    @XmlElement(name = "NswFileCode")
    private String NswFileCode;
    @XmlElement(name = "CreatedDate")
    private String CreatedDate;
    @XmlElement(name = "CreatedBy")
    private String CreatedBy;
    @XmlElement(name = "ModifiedDate")
    private String ModifiedDate;
    @XmlElement(name = "TransNo")
    private String TransNo;
    @XmlElement(name = "ContractNo")
    private String ContractNo;
    @XmlElement(name = "BillNo")
    private String BillNo;
    @XmlElement(name = "GoodsOwnerName")
    private String GoodsOwnerName;
    @XmlElement(name = "GoodsOwnerAddress")
    private String GoodsOwnerAddress;
    @XmlElement(name = "GoodsOwnerPhone")
    private String GoodsOwnerPhone;
    @XmlElement(name = "GoodsOwnerFax")
    private String GoodsOwnerFax;
    @XmlElement(name = "GoodsOwnerEmail")
    private String GoodsOwnerEmail;
    @XmlElement(name = "ResponsiblePersonName")
    private String ResponsiblePersonName;
    @XmlElement(name = "ResponsiblePersonAddress")
    private String ResponsiblePersonAddress;
    @XmlElement(name = "ResponsiblePersonPhone")
    private String ResponsiblePersonPhone;
    @XmlElement(name = "ResponsiblePersonFax")
    private String ResponsiblePersonFax;
    @XmlElement(name = "ResponsiblePersonEmail")
    private String ResponsiblePersonEmail;
    @XmlElement(name = "ExporterName")
    private String ExporterName;
    @XmlElement(name = "ExporterAddress")
    private String ExporterAddress;
    @XmlElement(name = "ExporterPhone")
    private String ExporterPhone;
    @XmlElement(name = "ExporterFax")
    private String ExporterFax;
    @XmlElement(name = "ExporterEmail")
    private String ExporterEmail;
    @XmlElement(name = "ComingDate")
    private String ComingDate;
    @XmlElement(name = "ExporterGateCode")
    private String ExporterGateCode;
    @XmlElement(name = "ExporterGateName")
    private String ExporterGateName;
    @XmlElement(name = "ImporterGateCode")
    private String ImporterGateCode;
    @XmlElement(name = "ImporterGateName")
    private String ImporterGateName;
    @XmlElement(name = "CustomsCode")
    private String CustomsCode;
    @XmlElement(name = "CustomsName")
    private String CustomsName;
    @XmlElement(name = "CheckTime")
    private String CheckTime;
    @XmlElement(name = "CheckPlace")
    private String CheckPlace;
    @XmlElement(name = "DeptCode")
    private String DeptCode;
    @XmlElement(name = "DeptName")
    private String DeptName;
    @XmlElement(name = "CreaterName")
    private String CreaterName;
    @XmlElementWrapper(name = "ProductList")
    @XmlElement(name = "Product")
    private List<Product> ProductList;
    @XmlElementWrapper(name = "AttachmentList")
    @XmlElement(name = "Attachment")
    private List<Attachment> AttachmentList;
//Giay phep bo sung
    @XmlElement(name = "FilesNo")
    private String FilesNo;
    @XmlElement(name = "Type")
    private String Type;
//Don dang ky bo sung
    private String ProcessDate;
    @XmlElement(name = "ProcessName")
    private String ProcessName;
    @XmlElement(name = "Comments")
    private String Comments;
    @XmlElement(name = "CommentsType")
    private String CommentsType;

    public ImportDocument() {

    }

    public String getProcessDate() {
        return ProcessDate;
    }

    public void setProcessDate(String ProcessDate) {
        this.ProcessDate = ProcessDate;
    }

    public String getProcessName() {
        return ProcessName;
    }

    public void setProcessName(String ProcessName) {
        this.ProcessName = ProcessName;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getFilesNo() {
        return FilesNo;
    }

    public void setFilesNo(String FilesNo) {
        this.FilesNo = FilesNo;
    }

    public String getNswFileCode() {
        return NswFileCode;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public String getTransNo() {
        return TransNo;
    }

    public String getContractNo() {
        return ContractNo;
    }

    public String getBillNo() {
        return BillNo;
    }

    public String getGoodsOwnerName() {
        return GoodsOwnerName;
    }

    public String getGoodsOwnerAddress() {
        return GoodsOwnerAddress;
    }

    public String getGoodsOwnerPhone() {
        return GoodsOwnerPhone;
    }

    public String getGoodsOwnerFax() {
        return GoodsOwnerFax;
    }

    public String getGoodsOwnerEmail() {
        return GoodsOwnerEmail;
    }

    public String getResponsiblePersonName() {
        return ResponsiblePersonName;
    }

    public String getResponsiblePersonAddress() {
        return ResponsiblePersonAddress;
    }

    public String getResponsiblePersonPhone() {
        return ResponsiblePersonPhone;
    }

    public String getResponsiblePersonFax() {
        return ResponsiblePersonFax;
    }

    public String getResponsiblePersonEmail() {
        return ResponsiblePersonEmail;
    }

    public String getExporterName() {
        return ExporterName;
    }

    public String getExporterAddress() {
        return ExporterAddress;
    }

    public String getExporterPhone() {
        return ExporterPhone;
    }

    public String getExporterFax() {
        return ExporterFax;
    }

    public String getExporterEmail() {
        return ExporterEmail;
    }

    public String getComingDate() {
        return ComingDate;
    }

    public String getExporterGateCode() {
        return ExporterGateCode;
    }

    public String getExporterGateName() {
        return ExporterGateName;
    }

    public String getImporterGateCode() {
        return ImporterGateCode;
    }

    public String getImporterGateName() {
        return ImporterGateName;
    }

    public String getCustomsCode() {
        return CustomsCode;
    }

    public String getCustomsName() {
        return CustomsName;
    }

    public String getCheckTime() {
        return CheckTime;
    }

    public String getCheckPlace() {
        return CheckPlace;
    }

    public String getDeptCode() {
        return DeptCode;
    }

    public String getDeptName() {
        return DeptName;
    }

    public String getCreaterName() {
        return CreaterName;
    }

    public List<Product> getProductList() {
        return ProductList;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public void setCreatedBy(String CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public void setModifiedDate(String ModifiedDate) {
        this.ModifiedDate = ModifiedDate;
    }

    public void setTransNo(String TransNo) {
        this.TransNo = TransNo;
    }

    public void setContractNo(String ContractNo) {
        this.ContractNo = ContractNo;
    }

    public void setBillNo(String BillNo) {
        this.BillNo = BillNo;
    }

    public void setGoodsOwnerName(String GoodsOwnerName) {
        this.GoodsOwnerName = GoodsOwnerName;
    }

    public void setGoodsOwnerAddress(String GoodsOwnerAddress) {
        this.GoodsOwnerAddress = GoodsOwnerAddress;
    }

    public void setGoodsOwnerPhone(String GoodsOwnerPhone) {
        this.GoodsOwnerPhone = GoodsOwnerPhone;
    }

    public void setGoodsOwnerFax(String GoodsOwnerFax) {
        this.GoodsOwnerFax = GoodsOwnerFax;
    }

    public void setGoodsOwnerEmail(String GoodsOwnerEmail) {
        this.GoodsOwnerEmail = GoodsOwnerEmail;
    }

    public void setResponsiblePersonName(String ResponsiblePersonName) {
        this.ResponsiblePersonName = ResponsiblePersonName;
    }

    public void setResponsiblePersonAddress(String ResponsiblePersonAddress) {
        this.ResponsiblePersonAddress = ResponsiblePersonAddress;
    }

    public void setResponsiblePersonPhone(String ResponsiblePersonPhone) {
        this.ResponsiblePersonPhone = ResponsiblePersonPhone;
    }

    public void setResponsiblePersonFax(String ResponsiblePersonFax) {
        this.ResponsiblePersonFax = ResponsiblePersonFax;
    }

    public void setResponsiblePersonEmail(String ResponsiblePersonEmail) {
        this.ResponsiblePersonEmail = ResponsiblePersonEmail;
    }

    public void setExporterName(String ExporterName) {
        this.ExporterName = ExporterName;
    }

    public void setExporterAddress(String ExporterAddress) {
        this.ExporterAddress = ExporterAddress;
    }

    public void setExporterPhone(String ExporterPhone) {
        this.ExporterPhone = ExporterPhone;
    }

    public void setExporterFax(String ExporterFax) {
        this.ExporterFax = ExporterFax;
    }

    public void setExporterEmail(String ExporterEmail) {
        this.ExporterEmail = ExporterEmail;
    }

    public void setComingDate(String ComingDate) {
        this.ComingDate = ComingDate;
    }

    public void setExporterGateCode(String ExporterGateCode) {
        this.ExporterGateCode = ExporterGateCode;
    }

    public void setExporterGateName(String ExporterGateName) {
        this.ExporterGateName = ExporterGateName;
    }

    public void setImporterGateCode(String ImporterGateCode) {
        this.ImporterGateCode = ImporterGateCode;
    }

    public void setImporterGateName(String ImporterGateName) {
        this.ImporterGateName = ImporterGateName;
    }

    public void setCustomsCode(String CustomsCode) {
        this.CustomsCode = CustomsCode;
    }

    public void setCustomsName(String CustomsName) {
        this.CustomsName = CustomsName;
    }

    public void setCheckTime(String CheckTime) {
        this.CheckTime = CheckTime;
    }

    public void setCheckPlace(String CheckPlace) {
        this.CheckPlace = CheckPlace;
    }

    public void setDeptCode(String DeptCode) {
        this.DeptCode = DeptCode;
    }

    public void setDeptName(String DeptName) {
        this.DeptName = DeptName;
    }

    public void setCreaterName(String CreaterName) {
        this.CreaterName = CreaterName;
    }

    public void setProductList(List<Product> ProductList) {
        this.ProductList = ProductList;
    }

    public void setAttachmentList(List<Attachment> AttachmentList) {
        this.AttachmentList = AttachmentList;
    }

    public List<Attachment> getAttachmentList() {
        return AttachmentList;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String Comments) {
        this.Comments = Comments;
    }

    public String getCommentsType() {
        return CommentsType;
    }

    public void setCommentsType(String CommentsType) {
        this.CommentsType = CommentsType;
    }

}
