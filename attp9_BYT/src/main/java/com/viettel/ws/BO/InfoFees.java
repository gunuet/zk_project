/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoFees")
public class InfoFees {

    public InfoFees() {

    }
    @XmlElement(name = "FeedsValue")
    private String FeedsValue;
    @XmlElementWrapper(name = "Attachments")
    @XmlElement(name = "Attachment")
    private List<Attachment> Attachments;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;

    public Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public List<Attachment> getAttachments() {
        return Attachments;
    }

    public void setAttachments(List<Attachment> Attachments) {
        this.Attachments = Attachments;
    }

    public String getFeedsValue() {
        return FeedsValue;
    }

    public void setFeedsValue(String FeedsValue) {
        this.FeedsValue = FeedsValue;
    }

}
