/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NoticeCancle")
public class NoticeCancle {


    @XmlElement(name = "Reasons")
    private String Reasons;

    @XmlElement(name = "NswFileCode")
    private String NswFileCode;


    
    
    

    public NoticeCancle() {
    }

    public String getNswFileCode() {
        return NswFileCode;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public String getReasons() {
        return Reasons;
    }

    public void setReasons(String Reasons) {
        this.Reasons = Reasons;
    }

   
}
