/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NoticeResult")
public class NoticeResult {

    @XmlElement(name = "NswFileCode")
    private String NswFileCode;
    @XmlElement(name = "TaxCode")
    private String TaxCode;
    @XmlElement(name = "SendBookNumber")
    private String SendBookNumber;
    @XmlElement(name = "BusinessName")
    private String BusinessName;
    @XmlElement(name = "Report")
    private String Report;
    @XmlElement(name = "ReportDate")
    private String ReportDate;
    @XmlElement(name = "ReceiveBookNumber")
    private String ReceiveBookNumber;
    @XmlElement(name = "SignDate")
    private String SignDate;
    @XmlElement(name = "Effect")
    private String Effect;
    @XmlElement(name = "ProductName")
    private String ProductName;
    @XmlElement(name = "Type")
    private String Type;
    @XmlElement(name = "Reason")
    private String Reason;
    @XmlElement(name = "SignPlace")
    private String SignPlace;
    @XmlElement(name = "PermitSignDate")
    private String PermitSignDate;
    @XmlElement(name = "PermitSignName")
    private String PermitSignName;
    @XmlElement(name = "PermitPosName")
    private String PermitPosName;
    @XmlElementWrapper(name = "Products")
    @XmlElement(name = "Product")
    private List<Product> Products;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;

    public NoticeResult() {
    }

    public com.viettel.ws.BO.Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(com.viettel.ws.BO.Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String BusinessName) {
        this.BusinessName = BusinessName;
    }

    public String getEffect() {
        return Effect;
    }

    public void setEffect(String Effect) {
        this.Effect = Effect;
    }

    public String getNswFileCode() {
        return NswFileCode;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public String getPermitPosName() {
        return PermitPosName;
    }

    public void setPermitPosName(String PermitPosName) {
        this.PermitPosName = PermitPosName;
    }

    public String getPermitSignDate() {
        return PermitSignDate;
    }

    public void setPermitSignDate(String PermitSignDate) {
        this.PermitSignDate = PermitSignDate;
    }

    public String getPermitSignName() {
        return PermitSignName;
    }

    public void setPermitSignName(String PermitSignName) {
        this.PermitSignName = PermitSignName;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public List<Product> getProducts() {
        return Products;
    }

    public void setProducts(List<Product> Products) {
        this.Products = Products;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String Reason) {
        this.Reason = Reason;
    }

    public String getReceiveBookNumber() {
        return ReceiveBookNumber;
    }

    public void setReceiveBookNumber(String ReceiveBookNumber) {
        this.ReceiveBookNumber = ReceiveBookNumber;
    }

    public String getReport() {
        return Report;
    }

    public void setReport(String Report) {
        this.Report = Report;
    }

    public String getReportDate() {
        return ReportDate;
    }

    public void setReportDate(String ReportDate) {
        this.ReportDate = ReportDate;
    }

    public String getSendBookNumber() {
        return SendBookNumber;
    }

    public void setSendBookNumber(String SendBookNumber) {
        this.SendBookNumber = SendBookNumber;
    }

    public String getSignDate() {
        return SignDate;
    }

    public void setSignDate(String SignDate) {
        this.SignDate = SignDate;
    }

    public String getSignPlace() {
        return SignPlace;
    }

    public void setSignPlace(String SignPlace) {
        this.SignPlace = SignPlace;
    }

    public String getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(String TaxCode) {
        this.TaxCode = TaxCode;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }
}
