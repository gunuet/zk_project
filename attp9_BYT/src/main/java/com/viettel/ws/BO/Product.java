/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Product")
public class Product {

    @XmlElement(name = "ProductId")
    private String ProductId;
    @XmlElement(name = "ProductName")
    private String ProductName;
    @XmlElement(name = "ProductDescriptions")
    private String ProductDescriptions;
    @XmlElement(name = "HSCode")
    private String HSCode;
    @XmlElement(name = "ProductCode")
    private String ProductCode;
    @XmlElement(name = "NationalCode")
    private String NationalCode;
    @XmlElement(name = "NationalName")
    private String NationalName;
    @XmlElement(name = "ConfirmAnnounceNo")
    private String ConfirmAnnounceNo;
    @XmlElement(name = "Total")
    private String Total;
    @XmlElement(name = "TotalUnitCode")
    private String TotalUnitCode;
    @XmlElement(name = "TotalUnitName")
    private String TotalUnitName;
    @XmlElement(name = "Netweight")
    private String Netweight;
    @XmlElement(name = "NetweightUnitCode")
    private String NetweightUnitCode;
    @XmlElement(name = "NetweightUnitName")
    private String NetweightUnitName;
    @XmlElement(name = "BaseUnit")
    private String BaseUnit;
//Bổ sung kết quả xử lý
    @XmlElementWrapper(name = "AttachmentList")
    @XmlElement(name = "Attachment")
    private List<Attachment> AttachmentList;
//Bổ sung hồ sơ kiểm tra
    @XmlElement(name = "ProcessType")
    private String ProcessType;
    @XmlElement(name = "ProcessTypeName")
    private String ProcessTypeName;
    @XmlElement(name = "ReProcess")
    private String ReProcess;
    @XmlElement(name = "Comment")
    private String Comment;
    @XmlElement(name = "Manufacturer")
    private String Manufacturer;
    @XmlElement(name = "ManufacturerAddress")
    private String ManufacturerAddress;
    //Thông báo kiểm tra
    @XmlElement(name = "CheckMethodCode")
    private String CheckMethodCode;
    @XmlElement(name = "CheckMethodName")
    private String CheckMethodName;
    @XmlElement(name = "Pass")
    private String Pass;
    @XmlElement(name = "Result")
    private String Result;
    @XmlElement(name = "Reason")
    private String Reason;
    @XmlElement(name = "ProductManufacture")
    private String ProductManufacture;
    @XmlElement(name = "Quantity")
    private String Quantity;
    @XmlElement(name = "IsCategory")
    private String IsCategory;

    //TTB
    @XmlElement(name = "GroupProductName")
    private String GroupProductName;
    @XmlElement(name = "GroupProductCode")
    private String GroupProductCode;
    @XmlElement(name = "Name")
    private String Name;
    @XmlElement(name = "Mode")
    private String Mode;
    @XmlElement(name = "ManufacturerNational")
    private String ManufacturerNational;
    @XmlElement(name = "ManufacturerNationalCode")
    private String ManufacturerNationalCode;
    @XmlElement(name = "ProductYear")
    private String ProductYear;
    @XmlElement(name = "Distributor")
    private String Distributor;
    @XmlElement(name = "DistributorNational")
    private String DistributorNational;
    @XmlElement(name = "DistributorNationalCode")
    private String DistributorNationalCode;
    @XmlElement(name = "NationalProduct")
    private String NationalProduct;
    @XmlElement(name = "NationalProductCode")
    private String NationalProductCode;
    @XmlElementWrapper(name = "Attachments")
    @XmlElement(name = "Attachment")
    private List<Attachment> Attachments;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;
    public Product() {

    }

    public void setReason(String Reason) {
        this.Reason = Reason;
    }

    public String getReason() {
        return Reason;
    }

    public void setResult(String Result) {
        this.Result = Result;
    }

    public String getResult() {
        return Result;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public String getPass() {
        return Pass;
    }

    public void setCheckMethodCode(String CheckMethodCode) {
        this.CheckMethodCode = CheckMethodCode;
    }

    public void setCheckMethodName(String CheckMethodName) {
        this.CheckMethodName = CheckMethodName;
    }

    public String getCheckMethodName() {
        return CheckMethodName;
    }

    public String getCheckMethodCode() {
        return CheckMethodCode;
    }

    public List<Attachment> getAttachmentList() {
        return AttachmentList;
    }

    public void setAttachmentList(List<Attachment> AttachmentList) {
        this.AttachmentList = AttachmentList;
    }

    public void setManufacturerAddress(String ManufacturerAddress) {
        this.ManufacturerAddress = ManufacturerAddress;
    }

    public void setManufacturer(String Manufacturer) {
        this.Manufacturer = Manufacturer;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public String getManufacturerAddress() {
        return ManufacturerAddress;
    }

    public String getProcessType() {
        return ProcessType;
    }

    public String getProcessTypeName() {
        return ProcessTypeName;
    }

    public String getReProcess() {
        return ReProcess;
    }

    public String getComment() {
        return Comment;
    }

    public void setProcessType(String ProcessType) {
        this.ProcessType = ProcessType;
    }

    public void setProcessTypeName(String ProcessTypeName) {
        this.ProcessTypeName = ProcessTypeName;
    }

    public void setReProcess(String ReProcess) {
        this.ReProcess = ReProcess;
    }

    public void setComment(String Comment) {
        this.Comment = Comment;
    }

    public String getProductId() {
        return ProductId;
    }

    public String getProductName() {
        return ProductName;
    }

    public String getProductDescriptions() {
        return ProductDescriptions;
    }

    public String getHSCode() {
        return HSCode;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public String getNationalCode() {
        return NationalCode;
    }

    public String getNationalName() {
        return NationalName;
    }

    public String getConfirmAnnounceNo() {
        return ConfirmAnnounceNo;
    }

    public String getTotal() {
        return Total;
    }

    public String getTotalUnitCode() {
        return TotalUnitCode;
    }

    public String getTotalUnitName() {
        return TotalUnitName;
    }

    public String getNetweight() {
        return Netweight;
    }

    public String getNetweightUnitCode() {
        return NetweightUnitCode;
    }

    public String getNetweightUnitName() {
        return NetweightUnitName;
    }

    public String getBaseUnit() {
        return BaseUnit;
    }

    public void setProductId(String ProductId) {
        this.ProductId = ProductId;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public void setProductDescriptions(String ProductDescriptions) {
        this.ProductDescriptions = ProductDescriptions;
    }

    public void setHSCode(String HSCode) {
        this.HSCode = HSCode;
    }

    public void setProductCode(String ProductCode) {
        this.ProductCode = ProductCode;
    }

    public void setNationalCode(String NationalCode) {
        this.NationalCode = NationalCode;
    }

    public void setNationalName(String NationalName) {
        this.NationalName = NationalName;
    }

    public void setConfirmAnnounceNo(String ConfirmAnnounceNo) {
        this.ConfirmAnnounceNo = ConfirmAnnounceNo;
    }

    public void setTotal(String Total) {
        this.Total = Total;
    }

    public void setTotalUnitCode(String TotalUnitCode) {
        this.TotalUnitCode = TotalUnitCode;
    }

    public void setTotalUnitName(String TotalUnitName) {
        this.TotalUnitName = TotalUnitName;
    }

    public void setNetweight(String Netweight) {
        this.Netweight = Netweight;
    }

    public void setNetweightUnitCode(String NetweightUnitCode) {
        this.NetweightUnitCode = NetweightUnitCode;
    }

    public void setNetweightUnitName(String NetweightUnitName) {
        this.NetweightUnitName = NetweightUnitName;
    }

    public void setBaseUnit(String BaseUnit) {
        this.BaseUnit = BaseUnit;
    }

    public String getGroupProductName() {
        return GroupProductName;
    }

    public void setGroupProductName(String GroupProductName) {
        this.GroupProductName = GroupProductName;
    }

    public String getGroupProductCode() {
        return GroupProductCode;
    }

    public void setGroupProductCode(String GroupProductCode) {
        this.GroupProductCode = GroupProductCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getMode() {
        return Mode;
    }

    public void setMode(String Mode) {
        this.Mode = Mode;
    }

    public String getManufacturerNational() {
        return ManufacturerNational;
    }

    public void setManufacturerNational(String ManufacturerNational) {
        this.ManufacturerNational = ManufacturerNational;
    }

    public String getManufacturerNationalCode() {
        return ManufacturerNationalCode;
    }

    public void setManufacturerNationalCode(String ManufacturerNationalCode) {
        this.ManufacturerNationalCode = ManufacturerNationalCode;
    }

    public String getProductYear() {
        return ProductYear;
    }

    public void setProductYear(String ProductYear) {
        this.ProductYear = ProductYear;
    }

    public String getDistributor() {
        return Distributor;
    }

    public void setDistributor(String Distributor) {
        this.Distributor = Distributor;
    }

    public String getDistributorNational() {
        return DistributorNational;
    }

    public void setDistributorNational(String DistributorNational) {
        this.DistributorNational = DistributorNational;
    }

    public String getDistributorNationalCode() {
        return DistributorNationalCode;
    }

    public void setDistributorNationalCode(String DistributorNationalCode) {
        this.DistributorNationalCode = DistributorNationalCode;
    }

    /**
     * @return the NationalProduct
     */
    public String getNationalProduct() {
        return NationalProduct;
    }

    /**
     * @param NationalProduct the NationalProduct to set
     */
    public void setNationalProduct(String NationalProduct) {
        this.NationalProduct = NationalProduct;
    }

    /**
     * @return the NationalProductCode
     */
    public String getNationalProductCode() {
        return NationalProductCode;
    }

    /**
     * @param NationalProductCode the NationalProductCode to set
     */
    public void setNationalProductCode(String NationalProductCode) {
        this.NationalProductCode = NationalProductCode;
    }

    /**
     * @return the ProductManufacture
     */
    public String getProductManufacture() {
        return ProductManufacture;
    }

    /**
     * @param ProductManufacture the ProductManufacture to set
     */
    public void setProductManufacture(String ProductManufacture) {
        this.ProductManufacture = ProductManufacture;
    }

    /**
     * @return the Quantity
     */
    public String getQuantity() {
        return Quantity;
    }

    /**
     * @param Quantity the Quantity to set
     */
    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    /**
     * @return the IsCategory
     */
    public String getIsCategory() {
        return IsCategory;
    }

    /**
     * @param IsCategory the IsCategory to set
     */
    public void setIsCategory(String IsCategory) {
        this.IsCategory = IsCategory;
    }

    /**
     * @return the Attachments
     */
    public List<Attachment> getAttachments() {
        return Attachments;
    }

    /**
     * @param Attachments the Attachments to set
     */
    public void setAttachments(List<Attachment> Attachments) {
        this.Attachments = Attachments;
    }

    /**
     * @return the Attachment
     */
    public Attachment getAttachment() {
        return Attachment;
    }

    /**
     * @param Attachment the Attachment to set
     */
    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }
}
