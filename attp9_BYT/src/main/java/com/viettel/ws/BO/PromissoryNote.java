/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "PromissoryNote")
@XmlAccessorType(XmlAccessType.FIELD)
public class PromissoryNote {

    public PromissoryNote() {

    }
    @XmlElement(name = "Organization")
    private String Organization;
    @XmlElement(name = "Division")
    private String Division;
    @XmlElement(name = "Name")
    private String Name;
    @XmlElement(name = "PromissoryContent")
    private String PromissoryContent;
    @XmlElement(name = "PromissoryDate")
    private String PromissoryDate;

    @XmlElement(name = "Attachment")
    private Attachment Attachment;

    public Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public String getOrganization() {
        return Organization;
    }

    public String getDivision() {
        return Division;
    }

    public String getName() {
        return Name;
    }

    public String getPromissoryContent() {
        return PromissoryContent;
    }

    public String getPromissoryDate() {
        return PromissoryDate;
    }

    public void setOrganization(String Organization) {
        this.Organization = Organization;
    }

    public void setDivision(String Division) {
        this.Division = Division;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setPromissoryContent(String PromissoryContent) {
        this.PromissoryContent = PromissoryContent;
    }

    public void setPromissoryDate(String PromissoryDate) {
        this.PromissoryDate = PromissoryDate;
    }

}
