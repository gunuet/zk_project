/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "RequestAttachment")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestAttachment {

    public RequestAttachment() {

    }
    @XmlElement(name = "AttachmentCode")
    private String AttachmentCode;

    public String getAttachmentCode() {
        return AttachmentCode;
    }

    public void setAttachmentCode(String AttachmentCode) {
        this.AttachmentCode = AttachmentCode;
    }

}
