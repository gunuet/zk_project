/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "RequireFees")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequireFees {

    public RequireFees() {
    }
    @XmlElement(name = "Organization")
    private String Organization;
    @XmlElement(name = "Division")
    private String Division;
    @XmlElement(name = "Name")
    private String Name;
    @XmlElement(name = "AmendmentContent")
    private String AmendmentContent;
    @XmlElement(name = "FeesValue")
    private String FeesValue;
    @XmlElement(name = "ExpirationDate")
    private String ExpirationDate;
    @XmlElementWrapper(name = "ListAssay")
    @XmlElement(name = "Assay")
    private List<Assay> ListAssay;
    @XmlElement(name = "Content")
    private String Content;
    @XmlElement(name = "Attachment")
    private Attachment Attachment;

    public Attachment getAttachment() {
        return Attachment;
    }

    public void setAttachment(Attachment Attachment) {
        this.Attachment = Attachment;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public String getOrganization() {
        return Organization;
    }

    public String getDivision() {
        return Division;
    }

    public String getName() {
        return Name;
    }

    public String getAmendmentContent() {
        return AmendmentContent;
    }

    public String getFeesValue() {
        return FeesValue;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public List<Assay> getListAssay() {
        return ListAssay;
    }

    public void setOrganization(String Organization) {
        this.Organization = Organization;
    }

    public void setDivision(String Division) {
        this.Division = Division;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setAmendmentContent(String AmendmentContent) {
        this.AmendmentContent = AmendmentContent;
    }

    public void setFeesValue(String FeesValue) {
        this.FeesValue = FeesValue;
    }

    public void setExpirationDate(String ExpirationDate) {
        this.ExpirationDate = ExpirationDate;
    }

    public void setListAssay(List<Assay> ListAssay) {
        this.ListAssay = ListAssay;
    }

}
