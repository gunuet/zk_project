/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Response")
public class Response {

    @XmlElement(name = "ResponseNo")
    private String ResponseNo;
    @XmlElement(name = "ResponseId")
    private String ResponseId;
    
    public Response() {

    }

    public Response(String no, String id) {
        this.ResponseNo = no;
        this.ResponseId = id;
    }

    public String getResponseNo() {
        return ResponseNo;
    }

    public void setResponseNo(String ResponseNo) {
        this.ResponseNo = ResponseNo;
    }

    public String getResponseId() {
        return ResponseId;
    }

    public void setResponseId(String ResponseId) {
        this.ResponseId = ResponseId;
    }

}
