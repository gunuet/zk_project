/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "ResponseAttachment")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseAttachment {

    public ResponseAttachment() {

    }
    @XmlElement(name = "FileByte")
    private String FileByte;

    public String getFileByte() {
        return FileByte;
    }

    public void setFileByte(String FileByte) {
        this.FileByte = FileByte;
    }

}
