package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * NhoDVT
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReturnStatus")
public class ReturnStatus {

    public ReturnStatus() {
    }
    @XmlElement(name = "NswFileCode")
    protected String NswFileCode;
    @XmlElement(name = "StatusCode")
    protected String StatusCode;
    @XmlElement(name = "StatusName")
    protected String StatusName;

    public String getNswFileCode() {
        return NswFileCode;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusName() {
        return StatusName;
    }

    public void setStatusName(String StatusName) {
        this.StatusName = StatusName;
    }
}
