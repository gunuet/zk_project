package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * NhoDVT
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendVoffice")
public class SendVoffice {

    public SendVoffice() {
    }
    @XmlElement(name = "NswFileCode")
    protected String NswFileCode;
    
    @XmlElement(name = "BookReceive")
    protected String BookReceive;
    
    @XmlElement(name = "BookSend")
    protected String BookSend;

    public String getNswFileCode() {
        return NswFileCode;
    }

    public void setNswFileCode(String NswFileCode) {
        this.NswFileCode = NswFileCode;
    }

    public String getBookReceive() {
        return BookReceive;
    }

    public void setBookReceive(String BookReceive) {
        this.BookReceive = BookReceive;
    }

    public String getBookSend() {
        return BookSend;
    }

    public void setBookSend(String BookSend) {
        this.BookSend = BookSend;
    }
    
    

    
}
