/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlType(name = "TargetTesting")
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetTesting {

    public TargetTesting() {

    }
    @XmlElement(name = "TargetTestName")
    private String TargetTestName;
    @XmlElement(name = "RangeOfApplication")
    private String RangeOfApplication;
    @XmlElement(name = "LimitDevelopment")
    private String LimitDevelopment;
    @XmlElement(name = "Precision")
    private String Precision;

    public String getLimitDevelopment() {
        return LimitDevelopment;
    }

    public String getPrecision() {
        return Precision;
    }

    public String getTargetTestName() {
        return TargetTestName;
    }

    public void setTargetTestName(String TargetTestName) {
        this.TargetTestName = TargetTestName;
    }

    public String getRangeOfApplication() {
        return RangeOfApplication;
    }

    public void setRangeOfApplication(String RangeOfApplication) {
        this.RangeOfApplication = RangeOfApplication;
    }

    public void setLimitDevelopment(String LimitDevelopment) {
        this.LimitDevelopment = LimitDevelopment;
    }

    public void setPrecision(String Precision) {
        this.Precision = Precision;
    }

}
