/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws.BO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author E5420
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceivePHHQ")
public class receivePHHQ {

    public receivePHHQ() {
    }
    @XmlElement(name = "SoCongBo")
    private String SoCongBo;
    @XmlElement(name = "NgayCongBo")
    private String NgayCongBo;

    public String getSoCongBo() {
        return SoCongBo;
    }

    public void setSoCongBo(String SoCongBo) {
        this.SoCongBo = SoCongBo;
    }

    public String getNgayCongBo() {
        return NgayCongBo;
    }

    public void setNgayCongBo(String NgayCongBo) {
        this.NgayCongBo = NgayCongBo;
    }

}
