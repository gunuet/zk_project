//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.11.11 at 11:26:55 AM ICT 
//


package com.viettel.ws.BO_IO;

import java.lang.Long;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Attachments" type="{}Attachments"/>
 *         &lt;element name="Products" type="{}ProductsType"/>
 *         &lt;element name="NswFileCode" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="BusinessName" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="BusinessAddress" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="BusinessPhone" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BusinessFax" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="DirectorName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DirectorPhone" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="DirectorMobiPhone" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="OfficersName" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="OfficersPhone" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="OfficersMobile" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="IntendedUse" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="SignDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ReceiveBookNumber" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="PriceCode" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="FileType" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="IsChange" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="PermitSignDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ExpAuthoriSation" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ProductName" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="GroupProductCode" type="{http://www.w3.org/2001/XMLSchema}Long"/>
 *         &lt;element name="GroupProductName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", propOrder = {
    "attachments",
    "products",
    "nswFileCode",
    "taxCode",
    "businessName",
    "businessAddress",
    "businessPhone",
    "businessFax",
    "directorName",
    "directorPhone",
    "directorMobiPhone",
    "officersName",
    "officersPhone",
    "officersMobile",
    "intendedUse",
    "signDate",
    "receiveBookNumber",
    "priceCode",
    "fileType",
    "isChange",
    "permitSignDate",
    "expAuthoriSation",
    "productName",
    "groupProductCode",
    "groupProductName"
})
public class Document {

    @XmlElement(name = "Attachments", required = true)
    protected Attachments attachments;
    @XmlElement(name = "Products", required = true)
    protected Products products;
    @XmlElement(name = "NswFileCode", required = true)
    protected String nswFileCode;
    @XmlElement(name = "TaxCode")
    protected Long taxCode;
    @XmlElement(name = "BusinessName")
    protected String businessName;
    @XmlElement(name = "BusinessAddress")
    protected String businessAddress;
    @XmlElement(name = "BusinessPhone", required = true)
    protected String businessPhone;
    @XmlElement(name = "BusinessFax")
    protected String businessFax;
    @XmlElement(name = "DirectorName", required = true)
    protected String directorName;
    @XmlElement(name = "DirectorPhone")
    protected String directorPhone;
    @XmlElement(name = "DirectorMobiPhone")
    protected String directorMobiPhone;
    @XmlElement(name = "OfficersName")
    protected String officersName;
    @XmlElement(name = "OfficersPhone")
    protected String officersPhone;
    @XmlElement(name = "OfficersMobile")
    protected String officersMobile;
    @XmlElement(name = "IntendedUse")
    protected String intendedUse;
    @XmlElement(name = "SignDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected String signDate;
    @XmlElement(name = "ReceiveBookNumber")
    protected String receiveBookNumber;
    @XmlElement(name = "PriceCode")
    protected Long priceCode;
    @XmlElement(name = "FileType")
    protected Long fileType;
    @XmlElement(name = "IsChange")
    protected Long isChange;
    @XmlElement(name = "PermitSignDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected String permitSignDate;
    @XmlElement(name = "ExpAuthoriSation", required = true)
    @XmlSchemaType(name = "dateTime")
    protected String expAuthoriSation;
    @XmlElement(name = "ProductName")
    protected String productName;
    @XmlElement(name = "GroupProductCode")
    protected String groupProductCode;
    @XmlElement(name = "GroupProductName", required = true)
    protected String groupProductName;

    /**
     * Gets the value of the attachments property.
     * 
     * @return
     *     possible object is
     *     {@link Attachments }
     *     
     */
    public Attachments getAttachments() {
        return attachments;
    }

    /**
     * Sets the value of the attachments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Attachments }
     *     
     */
    public void setAttachments(Attachments value) {
        this.attachments = value;
    }

    /**
     * Gets the value of the products property.
     * 
     * @return
     *     possible object is
     *     {@link ProductsType }
     *     
     */
    public Products getProducts() {
        return products;
    }

    /**
     * Sets the value of the products property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductsType }
     *     
     */
    public void setProducts(Products value) {
        this.products = value;
    }

    /**
     * Gets the value of the nswFileCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public String getNswFileCode() {
        return nswFileCode;
    }

    /**
     * Sets the value of the nswFileCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNswFileCode(String value) {
        this.nswFileCode = value;
    }

    /**
     * Gets the value of the taxCode property.
     * 
     */
    public Long getTaxCode() {
        return taxCode;
    }

    /**
     * Sets the value of the taxCode property.
     * 
     */
    public void setTaxCode(Long value) {
        this.taxCode = value;
    }

    /**
     * Gets the value of the businessName property.
     * 
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * Sets the value of the businessName property.
     * 
     */
    public void setBusinessName(String value) {
        this.businessName = value;
    }

    /**
     * Gets the value of the businessAddress property.
     * 
     */
    public String getBusinessAddress() {
        return businessAddress;
    }

    /**
     * Sets the value of the businessAddress property.
     * 
     */
    public void setBusinessAddress(String value) {
        this.businessAddress = value;
    }

    /**
     * Gets the value of the businessPhone property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public String getBusinessPhone() {
        return businessPhone;
    }

    /**
     * Sets the value of the businessPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBusinessPhone(String value) {
        this.businessPhone = value;
    }

    /**
     * Gets the value of the businessFax property.
     * 
     */
    public String getBusinessFax() {
        return businessFax;
    }

    /**
     * Sets the value of the businessFax property.
     * 
     */
    public void setBusinessFax(String value) {
        this.businessFax = value;
    }

    /**
     * Gets the value of the directorName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectorName() {
        return directorName;
    }

    /**
     * Sets the value of the directorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectorName(String value) {
        this.directorName = value;
    }

    /**
     * Gets the value of the directorPhone property.
     * 
     */
    public String getDirectorPhone() {
        return directorPhone;
    }

    /**
     * Sets the value of the directorPhone property.
     * 
     */
    public void setDirectorPhone(String value) {
        this.directorPhone = value;
    }

    /**
     * Gets the value of the directorMobiPhone property.
     * 
     */
    public String getDirectorMobiPhone() {
        return directorMobiPhone;
    }

    /**
     * Sets the value of the directorMobiPhone property.
     * 
     */
    public void setDirectorMobiPhone(String value) {
        this.directorMobiPhone = value;
    }

    /**
     * Gets the value of the officersName property.
     * 
     */
    public String getOfficersName() {
        return officersName;
    }

    /**
     * Sets the value of the officersName property.
     * 
     */
    public void setOfficersName(String value) {
        this.officersName = value;
    }

    /**
     * Gets the value of the officersPhone property.
     * 
     */
    public String getOfficersPhone() {
        return officersPhone;
    }

    /**
     * Sets the value of the officersPhone property.
     * 
     */
    public void setOfficersPhone(String value) {
        this.officersPhone = value;
    }

    /**
     * Gets the value of the officersMobile property.
     * 
     */
    public String getOfficersMobile() {
        return officersMobile;
    }

    /**
     * Sets the value of the officersMobile property.
     * 
     */
    public void setOfficersMobile(String value) {
        this.officersMobile = value;
    }

    /**
     * Gets the value of the intendedUse property.
     * 
     */
    public String getIntendedUse() {
        return intendedUse;
    }

    /**
     * Sets the value of the intendedUse property.
     * 
     */
    public void setIntendedUse(String value) {
        this.intendedUse = value;
    }

    /**
     * Gets the value of the signDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignDate() {
        return signDate;
    }

    /**
     * Sets the value of the signDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignDate(String value) {
        this.signDate = value; 
    }

    /**
     * Gets the value of the receiveBookNumber property.
     * 
     */
    public String getReceiveBookNumber() {
        return receiveBookNumber;
    }

    /**
     * Sets the value of the receiveBookNumber property.
     * 
     */
    public void setReceiveBookNumber(String value) {
        this.receiveBookNumber = value;
    }

    /**
     * Gets the value of the priceCode property.
     * 
     */
    public Long getPriceCode() {
        return priceCode;
    }

    /**
     * Sets the value of the priceCode property.
     * 
     */
    public void setPriceCode(Long value) {
        this.priceCode = value;
    }

    /**
     * Gets the value of the fileType property.
     * 
     */
    public Long getFileType() {
        return fileType;
    }

    /**
     * Sets the value of the fileType property.
     * 
     */
    public void setFileType(Long value) {
        this.fileType = value;
    }

    /**
     * Gets the value of the isChange property.
     * 
     */
    public Long getIsChange() {
        return isChange;
    }

    /**
     * Sets the value of the isChange property.
     * 
     */
    public void setIsChange(Long value) {
        this.isChange = value;
    }

    /**
     * Gets the value of the permitSignDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPermitSignDate() {
        return permitSignDate;
    }

    /**
     * Sets the value of the permitSignDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPermitSignDate(String value) {
        this.permitSignDate = value;
    }

    /**
     * Gets the value of the expAuthoriSation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpAuthoriSation() {
        return expAuthoriSation;
    }

    /**
     * Sets the value of the expAuthoriSation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpAuthoriSation(String value) {
        this.expAuthoriSation = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     */
    public void setProductName(String value) {
        this.productName = value;
    }

    /**
     * Gets the value of the groupProductCode property.
     * 
     */
    public String getGroupProductCode() {
        return groupProductCode;
    }

    /**
     * Sets the value of the groupProductCode property.
     * 
     */
    public void setGroupProductCode(String value) {
        this.groupProductCode = value;
    }

    /**
     * Gets the value of the groupProductName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupProductName() {
        return groupProductName;
    }

    /**
     * Sets the value of the groupProductName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupProductName(String value) {
        this.groupProductName = value;
    }

}
