/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws;

import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.utils.LogUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;

/**
 *
 * @author HaVM2
 */
public class BaseWS {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BaseWS.class);

    public boolean checkUser(String userName, String password) {
        UserDAOHE udhe = new UserDAOHE();
        String bReturn = udhe.checkLogin(userName, password);
        return bReturn.isEmpty();
    }
    public Boolean deleteFileSign(String userId, String fileId, String signType, Integer indexFile) {
        Boolean noError = true;
        ResourceBundle rb1 = ResourceBundle.getBundle("config");
        String PATH1 = rb1.getString("sign_download");
        File folder = new File(PATH1);
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                try {
                    // Check file name
                    String[] splitName = file.getName().split("_");
                    if (splitName.length != 4 && splitName.length != 5) {
                        continue;
                    }

                    // Compare User Id
                    if (!splitName[1].equals(userId)) {
                        continue;
                    }
                    // Compare User Id
                    if (!splitName[2].equals(fileId)) {
                        continue;
                    }
                    // Check sign Type
                    String signTypeSplit = splitName[3];
                    if (splitName.length == 4) {
                        String splitTypeStr = signTypeSplit.substring(0, signTypeSplit.indexOf(".pdf"));
                        if (!splitTypeStr.startsWith(signType)) {
                            continue;
                        }
                    } else if (splitName.length == 5) {
                        if (!signTypeSplit.startsWith(signType)) {
                            continue;
                        }
                    }

                    // Check file number for sign
                    if (splitName.length == 5) {
                        Integer indexFileSplit = Integer.parseInt(splitName[4].substring(0, splitName[4].indexOf(".pdf")));
                        if (!indexFileSplit.equals(indexFile)) {
                            continue;
                        }
                    }
                    file.delete();
                    //break;
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    noError = false;
                    continue;
                }
            }
        }
        return noError;
    }

//    // doc file ra mang byte
//    public byte[] read(File file) throws IOException {
//        ByteArrayOutputStream ous = null;
//        InputStream ios = null;
//        try {
//            byte[] buffer = new byte[4096];
//            ous = new ByteArrayOutputStream();
//            ios = new FileInputStream(file);
//            int read = 0;
//            while ((read = ios.read(buffer)) != -1) {
//                ous.write(buffer, 0, read);
//            }
//            ous.close();
//            ios.close();
//                    
//        } finally {
//            try {
//                if (ous != null) {
//                    ous.close();
//                }
//            } catch (IOException e) {
//                LogUtils.addLogDB(e);
//            }
//
//        }
//        return ous.toByteArray();
//    }
}
