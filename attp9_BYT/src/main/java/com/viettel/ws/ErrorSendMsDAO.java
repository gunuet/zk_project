/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.ws;

import com.viettel.core.base.DAO.GenericDAOHibernate;
import com.viettel.utils.Constants;
import com.viettel.ws.BO.ErrorSendMs;
import java.util.List;

import org.hibernate.Query;
/**
 *
 * @author E5420
 */
 
public class ErrorSendMsDAO extends
        GenericDAOHibernate<ErrorSendMs, Long> {

    public ErrorSendMsDAO() {
        super(ErrorSendMs.class);
    }

    @Override
    public void saveOrUpdate(ErrorSendMs obj) {
        if (obj != null) {
            super.saveOrUpdate(obj);
        }
        getSession().flush();
    }

    @Override
    public ErrorSendMs findById(Long id) {
        Query query = getSession().getNamedQuery(
                "ErrorSendMs.findById");
        query.setParameter("id", id);
        List result = query.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (ErrorSendMs) result.get(0);
        }
    }
}
