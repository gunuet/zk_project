///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.viettel.ws;
//
//import com.viettel.core.user.DAO.UserDAOHE;
//import com.viettel.ws.BO_IO.*;
////import com.viettel.ws.BO.*;
//import com.viettel.core.sys.BO.*;
//import com.viettel.core.sys.DAO.*;
//import com.viettel.core.user.BO.Department;
//import com.viettel.core.user.BO.Users;
//import com.viettel.core.user.model.UserToken;
//import com.viettel.core.workflow.BO.Flow;
//import com.viettel.core.workflow.BO.Node;
//import com.viettel.core.workflow.BO.NodeDeptUser;
//import com.viettel.core.workflow.BO.NodeToNode;
//import com.viettel.core.workflow.DAO.ProcessDAOHE;
//import com.viettel.core.workflow.WorkflowAPI;
//import com.viettel.module.evaluation.BO.AdditionalRequest;
//import com.viettel.module.evaluation.BO.Permit;
//import com.viettel.module.importOrder.BO.*;
//import com.viettel.module.importOrder.DAO.*;
//import com.viettel.utils.Constants;
//import com.viettel.utils.FileUtil;
//import com.viettel.utils.ResourceBundleUtil;
//import com.viettel.voffice.BO.Document.Attachs;
//import com.viettel.voffice.BO.Files;
//import com.viettel.voffice.DAO.FilesDAOHE;
//import com.viettel.voffice.DAOHE.AttachDAOHE;
//import fr.opensagres.xdocreport.core.io.IOUtils;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.io.StringReader;
//import java.io.StringWriter;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;
//import javax.xml.bind.Unmarshaller;
//import java.util.Date;
//import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
//import com.viettel.module.importDevice.BO.ImportDeviceFile;
//import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
//import com.viettel.module.payment.BO.Bill;
//import com.viettel.module.payment.BO.PaymentInfo;
//import com.viettel.module.payment.DAO.BillDAO;
//import com.viettel.module.payment.DAO.PaymentInfoDAO;
//import com.viettel.utils.EncryptDecryptUtils;
//import com.viettel.utils.LogUtils;
//import com.viettel.utils.StringUtils;
//import com.viettel.voffice.BO.Business;
//import com.viettel.voffice.DAOHE.BusinessDAOHE;
//import com.viettel.voffice.DAOHE.PermitDAO;
//import com.viettel.ws.client.IMOHService;
//import com.viettel.ws.client.MOHService;
//import java.util.ArrayList;
//import java.util.ResourceBundle;
//import org.apache.xml.security.exceptions.Base64DecodingException;
//import org.apache.commons.codec.binary.Base64;
//import org.zkoss.zk.ui.Sessions;
//
///**
// *
// * @author hieptq
// */
//public class HelperID {
//
//    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(HelperID.class);
//    private final SimpleDateFormat formatterYear = new SimpleDateFormat("yyyy");
//    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//    ImportedFoodService imService;
//
//    //Insert or update danh mục từ NSW
//    private void saveOrUpdatePort(String Code, String Name, Boolean isVN) {
//        try {
//            if (StringUtils.validString(Code) && StringUtils.validString(Name)) {
//                PortDAOHE pDH = new PortDAOHE();
//                Port p = pDH.findByCode(Code);
//                if (p != null && p.getId() != null) {
//                    p.setName(Name);
//                    pDH.saveOrUpdate(p);
//                } else {
//                    p = new Port();
//                    p.setCode(Code);
//                    p.setName(Name);
//                    if (isVN) {
//                        p.setNationalCode("VN");
//                    }
//                    p.setIsActive(1L);
//                    pDH.saveOrUpdate(p);
//                }
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public void saveOrUpdatePlace(String Code, String Name, String type) {
//        try {
//            if (StringUtils.validString(Code) && StringUtils.validString(Name)) {
//                PlaceDAOHE pDH = new PlaceDAOHE();
//                Place p = pDH.findByCode(Code, type);
//                if (p != null && p.getPlaceId() != null) {
//                    p.setName(Name);
//                    pDH.saveOrUpdate(p);
//                } else {
//                    p = new Place();
//                    p.setCode(Code);
//                    p.setName(Name);
//                    p.setPlaceTypeCode(type);
//                    p.setIsActive(1L);
//                    pDH.saveOrUpdate(p);
//                }
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    private void saveOrUpdateCustomerBranch(String Code, String Name) {
//        try {
//            if (StringUtils.validString(Code) && StringUtils.validString(Name)) {
//                CustomBranchDAOHE csbDH = new CustomBranchDAOHE();
//                CustomBranch csb = csbDH.findByCode(Code);
//                if (csb != null && csb.getId() != null) {
//                    csb.setName(Name);
//                    csbDH.saveOrUpdate(csb);
//                } else {
//                    csb = new CustomBranch();
//                    csb.setCode(Code);
//                    csb.setName(Name);
//                    csb.setIsActive(1L);
//                    csbDH.saveOrUpdate(csb);
//                }
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    private void saveOrUpdateCategory(String Code, String Name, String Type) {
//        try {
//            if (StringUtils.validString(Code) && StringUtils.validString(Name)) {
//                CategoryDAOHE cDH = new CategoryDAOHE();
//                Category c = cDH.findCategory(Code, Type);
//                if (c != null && c.getCategoryId() != null) {
//                    c.setName(Name);
//                    cDH.saveOrUpdate(c);
//                } else {
//                    c = new Category();
//                    c.setCode(Code);
//                    c.setName(Name);
//                    c.setCategoryTypeCode(Type);
//                    c.setIsActive(1L);
//                    cDH.saveOrUpdate(c);
//                }
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    //Convert Object <---> XML 
//    public String ObjectToXml(Object obj) {
//
//        String result = "";
//        java.io.StringWriter sw = new StringWriter();
//        try {
//            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
//            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//            // output pretty printed
//            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//            jaxbMarshaller.marshal(obj, sw);
//            result = sw.toString();
//        } catch (JAXBException ex) {
//            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return result.trim();
//    }
//
//    public Envelope xmlToEnvelope(String xml) {
//        try {
//            JAXBContext jc = JAXBContext.newInstance(Envelope.class);
//            Unmarshaller unmarshaller = jc.createUnmarshaller();
//            StringReader reader = new StringReader(xml);
//            return (Envelope) unmarshaller.unmarshal(reader);
//        } catch (JAXBException e) {
//            log.error(e);
//            return null;
//        }
//    }
//    //Save ImportOrderFile from NSW
//
//    public Files copyFilesToFiles(Files files, Files filesnew) {
//        try {
//            if (filesnew == null) {
//                filesnew = new Files();
//            }
//            filesnew.setIsTemp(1L);
//            filesnew.setBusinessAddress(files.getBusinessAddress());
//            filesnew.setBusinessEmail(files.getBusinessEmail());
//            filesnew.setBusinessFax(files.getBusinessFax());
//            filesnew.setBusinessId(files.getBusinessId());
//            filesnew.setBusinessName(files.getBusinessName());
//            filesnew.setBusinessPhone(files.getBusinessPhone());
//            filesnew.setCreateDate(files.getCreateDate());
//            filesnew.setStartDate(files.getStartDate());
//            filesnew.setCreateDeptId(files.getCreateDeptId());
//            filesnew.setCreateDeptName(files.getCreateDeptName());
//            filesnew.setCreatorId(files.getCreatorId());
//            filesnew.setCreatorName(files.getCreatorName());
//            filesnew.setFileCode(files.getFileCode());
//            filesnew.setFileName(files.getFileName());
//            filesnew.setFileType(files.getFileType());
//            filesnew.setFileTypeName(files.getFileTypeName());
//            filesnew.setFlowId(files.getFlowId());
//            filesnew.setIsActive(1L);
//            filesnew.setModifyDate(files.getModifyDate());
//            filesnew.setNswFileCode(files.getNswFileCode());
//            filesnew.setStatus(files.getStatus());
//            filesnew.setTaxCode(files.getTaxCode());
//            return filesnew;
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class
//                    .getName()).log(Level.SEVERE, null, ex);
//            return null;
//        }
//    }
//
//    private Files saveFileFromImDoc(Document imDoc, Files files) throws ParseException, Exception {
//        try {
//            if (files == null || files.getFileId() == null) {
//                files = new Files();
//            } else {
//                FilesDAOHE fdh = new FilesDAOHE();
//                ImportDeviceFiletDAO imFileDAO = new ImportDeviceFiletDAO();
//                Files fileTemp = copyFilesToFiles(files, null);
//                fdh.saveOrUpdate(fileTemp);
////                ImportDeviceFile imFile = imFileDAO.findById(files.getFileId());
////                imFile.setFileId(fileTemp.getFileId());
////                imFileDAO.saveOrUpdate(imFile);
////                ImportOrderProductDAO impDao = new ImportOrderProductDAO();
////                impDao.setIsTempProduct(fileTemp.getFileId(), files.getFileId());
////                AttachDAOHE attDH = new AttachDAOHE();
////                attDH.setIsTempAttach(fileTemp.getFileId(), files.getFileId());
//            }
//
//            files.setBusinessName(imDoc.getBusinessName());
//            files.setBusinessAddress(imDoc.getBusinessAddress());
//            files.setBusinessPhone(imDoc.getBusinessPhone());
//            files.setBusinessFax(imDoc.getBusinessFax());
//            files.setBusinessEmail("business@moh.gov.vn");
//            files.setStartDate(new Date());
//            
//            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
//            files.setIsActive(0L);
//            files.setIsTemp(0L);
//            WorkflowAPI w = new WorkflowAPI();
//            Category ftype = w.getProcedureTypeByCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
//            if (ftype != null) {
//                files.setFileType(ftype.getCategoryId());
//                files.setFileTypeName(ftype.getName());
//            }
//            
//            //files.setFileCode(imDoc.getNswFileCode());
//            files.setNswFileCode(imDoc.getNswFileCode());
//            
//            UserDAOHE usDAOHE = new UserDAOHE();
//            Users u = usDAOHE.getUserByName(String.valueOf(imDoc.getTaxCode()));
//            if (u == null) {
//                u = new Users();
//                Business b = new Business();
//                BusinessDAOHE bDAOHE = new BusinessDAOHE();
//                b.setBusinessAddress(files.getBusinessAddress());
//                b.setBusinessFax(files.getBusinessFax());
//                b.setBusinessName(files.getBusinessName());
//                b.setBusinessTaxCode(files.getTaxCode());
//                b.setBusinessTelephone(files.getBusinessPhone());
//                b.setIsActive(1L);
//                b.setUserName(files.getTaxCode());
//                bDAOHE.saveOrUpdate(b);
//                u.setBusinessName(imDoc.getBusinessName());
//                u.setUserName(String.valueOf(imDoc.getTaxCode()));
//                u.setUserType(Constants.USER_TYPE.ENTERPRISE_USER);
//                u.setBusinessId(b.getBusinessId());
//                u.setPassword(EncryptDecryptUtils.encrypt(u.getUserName() + "!@@"));
//                u.setStatus(1L);
//                usDAOHE.saveOrUpdate(u);
//            }
//            files.setCreatorId(u.getUserId());
//            files.setBusinessId(u.getBusinessId());
//            FilesDAOHE fDAOHE = new FilesDAOHE();
//            fDAOHE.saveOrUpdate(files);
//            return files;
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
//            return null;
//        }
//    }
//
//
//    private ImportDeviceFile getImFileFromImDoc(Document imDoc, Long fileId) throws ParseException {
//        try {
//            ImportDeviceFile importDeviceFile = new ImportDeviceFile();
//            importDeviceFile.setNswFileCode(imDoc.getNswFileCode());
//            importDeviceFile.setImporterName(imDoc.getBusinessName());
//            importDeviceFile.setImporterAddress(imDoc.getBusinessAddress());
//            importDeviceFile.setImporterPhone(imDoc.getBusinessPhone());
//            importDeviceFile.setImporterFax(imDoc.getBusinessFax());
//            
//            
//            
//            importDeviceFile.setDirector(imDoc.getDirectorName());
//            importDeviceFile.setDirectorPhone(imDoc.getDirectorPhone());
//            importDeviceFile.setDirectorMobile(imDoc.getDirectorMobiPhone());
//            
//                importDeviceFile.setResponsiblePersonName(imDoc.getOfficersName());
//            importDeviceFile.setResponsiblePersonPhone(imDoc.getOfficersPhone());
//            importDeviceFile.setResponsiblePersonMobile(imDoc.getOfficersMobile()); 
//            
//            
//            importDeviceFile.setEquipmentNo(imDoc.getReceiveBookNumber());
//            importDeviceFile.setImportPurpose(imDoc.getIntendedUse());
//            //importDeviceFile.setUqDate(StringUtils.validString(imDoc.getExpAuthoriSation()) ? formatterDateTime.parse(imDoc.getExpAuthoriSation()) : null);
//            long value = Long.valueOf(imDoc.getPriceCode());
//            if (value == 1) {
//                importDeviceFile.setPrice(500000L);
//            } else if (value == 2) {
//                importDeviceFile.setPrice(1000000L);
//            } else if (value == 3) {
//                importDeviceFile.setPrice(3000000L);
//            } else {
//                importDeviceFile.setPrice(200000L);
//            }
//
//            
//            importDeviceFile.setProductName(imDoc.getProductName()); 
//            String groupProductCode= imDoc.getGroupProductCode(); 
//            CategoryDAOHE catDAOHE = new CategoryDAOHE();
//            ResourceBundle rb = ResourceBundle.getBundle("config");
//            String catType = rb.getString("TTB_GROUP");
//            List<Category> lst = catDAOHE.findAllCategoryByCode(catType, groupProductCode);
//            if(lst !=null && lst.size() >0){
//                Category a = lst.get(0);
//                importDeviceFile.setGroupProductId(a.getCategoryId().toString());
//                importDeviceFile.setGroupProductName(a.getName());
//            }
//           
//            
//
//            
//
//            
//           // importDeviceFile.setSignPlace(imDoc.getSign);
//             // importDeviceFile.setSignDate(StringUtils.validString(imDoc.getSignDate()) ? formatterDateTime.parse(imDoc.getSignDate()) : null);
////            importDeviceFile.setIsoDate(dbIsoDate.getValue());
////            importDeviceFile.setCfsDate(dbCfsDate.getValue());
//            
//            
//            
//            importDeviceFile.setDocumentTypeCode(Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI);
//            importDeviceFile.setFileId(fileId);
//            return importDeviceFile;
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
//            return null;
//        }
//    }
//
//    private com.viettel.ws.BO_IO.Error saveImFileFromImDoc(Document imDoc, Long fileId) {
//        com.viettel.ws.BO_IO.Error err = new com.viettel.ws.BO_IO.Error();
//        ImportDeviceFiletDAO ifDAO = new ImportDeviceFiletDAO();
//        try {
//            ImportDeviceFile imFile = getImFileFromImDoc(imDoc, fileId);
//            ifDAO.saveOrUpdate(imFile);
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
//            err.setErrorCode("L000-0000-0000");
//            err.setErrorName("Lỗi thêm mới hồ sơ nhập khẩu trang thiết bị (ImportDeviceFile): " + imDoc.getNswFileCode());
//            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
//            return err;
//        }
//        return null;
//    }
//
////    private List<com.viettel.ws.BO.Error> saveListImProductFromImDoc(ImportDocument imDoc, Long fileId, Boolean isUpdate) throws ParseException {
////
////        List<Product> listProduct = imDoc.getProductList();
////        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
////        if (listProduct == null || listProduct.size() == 0) {
////
////            com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////            err.setErrorCode("A000-0000-0000");
////            err.setErrorName("Không có dữ liệu sản phẩm");
////            err.setSolution("Kiểm tra lại ms gửi dữ liệu");
////            errList.add(err);
////            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, err);
////            return errList;
////        }
////        ImportOrderProductDAO ipDAO = new ImportOrderProductDAO();
////        if (listProduct != null && listProduct.size() > 0) {
////            for (Product product : listProduct) {
////                try {
////                    ImportOrderProduct imProduct = new ImportOrderProduct();
////                    imProduct.setBaseUnit(!StringUtils.validString(product.getBaseUnit()) ? null : new Double(product.getBaseUnit()).longValue());
////                    imProduct.setConfirmAnnounceNo(product.getConfirmAnnounceNo());
////                    imProduct.setFileId(fileId);
////                    imProduct.setManufacTurer(product.getManufacturer());
////                    imProduct.setManufacturerAddress(product.getManufacturerAddress());
////                    saveOrUpdatePlace(product.getNationalCode(), product.getNationalName(), "nation");
////                    imProduct.setNationalCode(product.getNationalCode());
////                    imProduct.setNationalName(product.getNationalName());
////                    saveOrUpdateCategory(product.getNetweightUnitCode(), product.getNetweightUnitName(), Constants.CATEGORY_TYPE.WEIGHT);
////                    imProduct.setNetweight(!StringUtils.validString(product.getNetweight()) ? null : Double.parseDouble(product.getNetweight())
////                    );
////                    imProduct.setNetweightUnitCode(product.getNetweightUnitCode());
////                    imProduct.setNetweightUnitName(product.getNetweightUnitName());
////                    imProduct.setProductCode(product.getProductCode());
////                    imProduct.setProductDescription(product.getProductDescriptions());
////                    imProduct.setProductName(product.getProductName());
////                    if (isUpdate) {
////                        imProduct.setCheckMethodCode(Constants.NSW_SERVICE.TYPE_GIAM_HT.equals(product.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_GIAM : (Constants.NSW_SERVICE.TYPE_THUONG_HT.equals(product.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_THUONG : Constants.NSW_SERVICE.TYPE_CHAT));
////                        imProduct.setCheckMethodName(product.getCheckMethodName());
////                        imProduct.setPass(!StringUtils.validString(product.getPass()) ? null : Long.parseLong(product.getPass()));
////                    }
////                    imProduct.setTotal(!StringUtils.validString(product.getTotal()) ? null : Double.parseDouble(product.getTotal()));
////                    saveOrUpdateCategory(product.getTotalUnitCode(), product.getTotalUnitName(), Constants.CATEGORY_TYPE.QUANTITY);
////                    imProduct.setTotalUnitCode(product.getTotalUnitCode());
////                    imProduct.setTotalUnitName(product.getTotalUnitName());
////                    //
////                    ipDAO.saveOrUpdate(imProduct);
////                } catch (Exception ex) {
////                    Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
////                    com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////                    err.setErrorCode("L000-0000-0000");
////                    err.setErrorName("Lỗi thêm mới sản phẩm: " + product.getProductCode());
////                    err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
////                    errList.add(err);
////                }
////            }
////        }
////        return errList;
////    }
////
////    private List<com.viettel.ws.BO.Error> saveFileAttFromNSW(List<Attachment> listatt, Long fileId, String nswFileCode) throws FileNotFoundException, IOException, Base64DecodingException {
////        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
////        if (listatt != null && listatt.size() > 0) {
////            for (Attachment att : listatt) {
////                //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)
////                try {
////                    Boolean k = saveAttachmentFromNSW(att, fileId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE, nswFileCode);
////                    if (!k) {
////                        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////                        err.setErrorCode("L000-0000-0000");
////                        err.setErrorName("Lỗi thêm mới file đính kèm: " + att.getAttachPath());
////                        err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
////                        errList.add(err);
////                    }
////                } catch (Exception ex) {
////                    Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
////                    com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////                    err.setErrorCode("L000-0000-0000");
////                    err.setErrorName("Lỗi thêm mới file đính kèm: " + att.getAttachPath());
////                    err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
////                    errList.add(err);
////                }
////            }
////        }
////        return errList;
////    }
////
////    public Boolean saveAttachmentFromNSW(Attachment att, Long objectId, Long attCat, String nswFileCode) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            AttachDAOHE attachDAOHE = new AttachDAOHE();
////            Attachs attach = new Attachs();
////            String folderPath = ResourceBundleUtil.getString("dir_upload");
////            FileUtil.mkdirs(folderPath);
////            String separator = ResourceBundleUtil.getString("separator");
////            if (!"/".equals(separator) && !"\\".equals(separator)) {
////                separator = "/";
////            }
////            String fileName = att.getAttachmentName();
////
////            attach.setAttachName(fileName);
////            attach.setAttachCode(att.getAttachmentId());
////            attach.setAttachTypeName(att.getAttachTypeName());
////            attach.setIsActive(Constants.Status.ACTIVE);
////            attach.setIsSent(1L);
////            attach.setObjectId(objectId);
////            attach.setAttachCat(attCat);
////            attach.setAttachType(!StringUtils.validString(att.getAttachTypeCode()) ? null : Long.parseLong(att.getAttachTypeCode()));
////            attachDAOHE.saveOrUpdate(attach);
////            folderPath += separator + attCat + separator + attach.getAttachId();
////            attach.setAttachPath(folderPath);
////            attachDAOHE.saveOrUpdate(attach);
////            File f = new File(attach.getFullPathFile());
////            if (!f.exists()) {
////                // tao folder
////                if (!f.getParentFile().exists()) {
////                    f.getParentFile().mkdirs();
////                }
////                f.createNewFile();
////            }
////            //save to hard disk and database
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(36L), Constants.NSW_FUNCTION(35L), nswFileCode, nswFileCode, attach.getAttachId().toString());
////            Body bd = new Body();
////            Content ct = new Content();
////            Attachment attSend = new Attachment();
////            attSend.setNswFileCode(nswFileCode);
////            attSend.setAttachmentId(att.getAttachmentId());
////            ct.setAttachment(attSend);
////            bd.setContent(ct);
////            evl.setBody(bd);
////
////            return sendMs(evl);
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
////            return false;
////        }
////    }
////
//    private void saveOrUpdateFiles(String nsWCode, Long docType, Boolean pass) {
//        try {
//            FilesDAOHE fdh = new FilesDAOHE();
//            Files files = fdh.findByNswCode(nsWCode,docType, 0L, 0L);
//            if (files != null) {
//                Long fileId = files.getFileId();
//                if (pass) {
//                    files.setIsActive(1L);
//                    fdh.SaveAndCommit(files);
//                } else {
//                    ImportOrderFileDAO imFileDAO = new ImportOrderFileDAO();
//                    ImportOrderProductDAO impDao = new ImportOrderProductDAO();
//                    AttachDAOHE attDH = new AttachDAOHE();
//                    Files fileTemp = fdh.findByNswCode(nsWCode,docType, 1L, 1L);
//
//                    //rollback files cũ
//                    files = copyFilesToFiles(fileTemp, files);
//                    files.setIsTemp(0L);
//                    files.setIsActive(1L);
//                    fileTemp.setIsActive(0L);
//                    //   fdh.saveOrUpdate(files);
//                    //   fdh.SaveAndCommit(fileTemp);
//                    //dell file mới
//                    ImportOrderFile imFiledel = imFileDAO.findById(fileId);
//                    if (imFiledel != null) {
//                        imFileDAO.delete(imFiledel);
//                    }
//                    //rollback files cũ
//                    ImportOrderFile imFile = imFileDAO.findById(fileTemp.getFileId());
//                    imFile.setFileId(fileId);
//                    imFileDAO.saveOrUpdate(imFile);
//                    //dell sp mới
//                    impDao.deleteByFileId(fileId);
//                    //rollback sp cũ
//                    impDao.setIsTempProduct(files.getFileId(), fileTemp.getFileId());
//                    //dell att mới
//                    attDH.deleteByFileId(fileId, Constants.OBJECT_TYPE.IMPORT_ORDER_FILE);
//                    //rollback att cũ
//                    attDH.setIsTempAttach(files.getFileId(), fileTemp.getFileId());
//
//                    fdh.saveOrUpdate(files);
//                    fdh.SaveAndCommit(fileTemp);
//                }
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
////
//    private void deleteAtt(Long objectId, Long attCat) {
//        try {
//            AttachDAOHE attDH = new AttachDAOHE();
//            ImportOrderProductDAO impDAO = new ImportOrderProductDAO();
//            List<ImportOrderProduct> lsP = impDAO.findAllIdByFileIdAndPass(objectId, 0L);
//            if (lsP != null && lsP.size() > 0) {
//                for (ImportOrderProduct p : lsP) {
//                    attDH.deleteByFileId(p.getProductId(), attCat);
//                }
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
////    // Tạo Envelope Sent
////
////    public Envelope makeEnvelopeSend(String type, String function, String ref, String pref, String msId) {
////        Envelope ev = new Envelope();
////        Header hd = new Header();
////
////        hd.setReference(new Reference("1.0", msId));
////        hd.setFrom(new From("Cục An toàn thực phẩm - Bộ Y tế", "ATTP"));
////        hd.setTo(new From("Hệ thống NSW", "NSW"));
////        Date d = new Date();
////        hd.setSubject(new Subject(type, function, ref, pref, formatterYear.format(d), formatterDateTime.format(d)));
////        ev.setHeader(hd);
////        return ev;
////    }
////
////    //Các hàm nhận kết quả, nhận yêu cầu
////    //Nhận hồ sơ + hồ sơ SDBS
////    public List<com.viettel.ws.BO.Error> receiveMs01_02(Envelope envelop, Files files, Boolean isUpdate) throws ParseException, IOException, Exception {
////        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
////        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////        ImportDocument imDoc = new ImportDocument();
////        try {
////            imDoc = envelop.getBody().getContent().getImportDocument();
////            files = saveFileFromImDoc(imDoc, files);
////            if (files.getFileId() >= 0) {
////                com.viettel.ws.BO.Error errImDoc = saveImFileFromImDoc(imDoc, files.getFileId());
////                if (errImDoc != null) {
////                    errList.add(errImDoc);
////                }
////                errList.addAll(saveListImProductFromImDoc(imDoc, files.getFileId(), isUpdate));
////                List<Attachment> listatt = imDoc.getAttachmentList();
////                if (listatt == null || listatt.isEmpty()) {
////                    err.setErrorCode("A000-0000-0000");
////                    err.setErrorName("Không có dữ liệu file đính kèm");
////                    err.setSolution("Kiểm tra lại ms gửi dữ liệu");
////                    errList.add(err);
////                    Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, err);
////                    return errList;
////                }
////                errList.addAll(saveFileAttFromNSW(listatt, files.getFileId(), imDoc.getNswFileCode()));
////            }
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
////            err.setErrorCode("L000-0000-0000");
////            err.setErrorName("Lỗi thêm mới hồ sơ đạt yêu cầu nhập khẩu(Files): " + imDoc.getNswFileCode());
////            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
////            errList.add(err);
////        }
////
////        return errList;
////    }
////
//    //40-66
//    public List<com.viettel.ws.BO_IO.Error> receiveMs40_66(Envelope envelop, Files files, Boolean isUpdate) throws ParseException, IOException, Exception {
//        List<com.viettel.ws.BO_IO.Error> errList = new ArrayList<>();
//        com.viettel.ws.BO_IO.Error err = new com.viettel.ws.BO_IO.Error();
//        Document imDoc = new Document();
//        try {
//            imDoc = envelop.getBody().getContent().getDocument();
//            files = saveFileFromImDoc(imDoc, files);
//            if (files.getFileId() >= 0) {
//                com.viettel.ws.BO_IO.Error errImDoc = saveImFileFromImDoc(imDoc, files.getFileId());
//                if (errImDoc != null) {
//                    errList.add(errImDoc);
//                }
////                errList.addAll(saveListImProductFromImDoc(imDoc, files.getFileId(), isUpdate));
////                List<Attachment> listatt = (List<Attachment>) imDoc.getAttachment();
////                if (listatt == null || listatt.isEmpty()) {
////                    err.setErrorCode("A000-0000-0000");
////                    err.setErrorName("Không có dữ liệu file đính kèm");
////                    err.setSolution("Kiểm tra lại ms gửi dữ liệu");
////                    errList.add(err);
////                    Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, err);
////                    return errList;
////                }
////                errList.addAll(saveFileAttFromNSW(listatt, files.getFileId(), imDoc.getNswFileCode()));
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
//            err.setErrorCode("L000-0000-0000");
//            err.setErrorName("Lỗi thêm mới hồ sơ (Files): " + imDoc.getNswFileCode());
//            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
//            errList.add(err);
//        }
//
//        return errList;
//    }
////
//////Nhận yêu cầu lấy File đính kèm
////    public Content receiveMs_35(Envelope envelop, Files files) throws ParseException, IOException {
////        Content ct = new Content();
////        try {
////            Attachment att = envelop.getBody().getContent().getAttachment();
////            AttachDAOHE aDH = new AttachDAOHE();
////            // Attachs atts = aDH.getByObjectIdAndCode(att.getAttachTypeCode(), files.getFileId());
////            Attachs atts = aDH.findById(!StringUtils.validString(att.getAttachmentId()) ? null : Long.parseLong(att.getAttachmentId()));
////            att.setAttachTypeCode(atts.getAttachType().toString());
////            att.setAttachTypeName(atts.getAttachTypeName());
////            att.setAttachmentId(atts.getAttachId().toString());
////            Path path = Paths.get(atts.getFullPathFile());
////            byte[] data = java.nio.file.Files.readAllBytes(path);
////            att.setFileByte(Base64.encodeBase64String(data));
////            ct.setAttachment(att);
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////            List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
////            com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////            err.setErrorCode("U000-0000-0000");
////            err.setErrorName("Lỗi tải file đính kèm");
////            err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
////            errList.add(err);
////            ct.setErrorList(errList);
////        }
////        return ct;
////    }
////
////    //Nhận kết quả nộp phí
////    public List<com.viettel.ws.BO.Error> reiceiveMs_19_20(Envelope envelope, Files files) throws FileNotFoundException, Base64DecodingException {
////        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
////        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////        try {
////            FilesCost fc = envelope.getBody().getContent().getFilesCost();
////            Boolean k;
////            Attachment att = fc.getAttachment();
////            List<Fee> lsFee = fc.getFeeList();
////            Long costs = 0L;
////            PaymentInfoDAO pDAO = new PaymentInfoDAO();
////            for (Fee f : lsFee) {
////                costs += f.getCost() == null ? 0 : new Double(f.getCost()).longValue();
////            }
////            Bill b = new Bill();
////            BillDAO bDAO = new BillDAO();
////            b.setCreatorId(files.getCreatorId());
////            b.setTotal(costs);
////            b.setCreatetorName(files.getCreatorName());
////            b.setCreateDate(new java.util.Date());
////            b.setPaymentDate(new Date());
////            b.setStatus(Constants.NSW_SERVICE.FEE_STATUS.MOI_NOP);
////            bDAO.saveOrUpdate(b);
////            if (att != null && StringUtils.validString(att.getAttachmentId())) {
////                k = saveAttachmentFromNSW(att, b.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY, fc.getNswFileCode());
////                if (!k) {
////                    err.setErrorCode("U000-0000-0000");
////                    err.setErrorName("Lỗi tải file đính kèm phí");
////                    err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
////                    errList.add(err);
////                    return errList;
////                }
////            }
////            for (Fee f : lsFee) {
////                PaymentInfo p = pDAO.findById(f.getFeeId() == null ? null : Long.parseLong(f.getFeeId()));
////                if (p != null) {
////                    p.setStatus(Constants.NSW_SERVICE.FEE_STATUS.MOI_NOP);
////                    p.setBillId(b.getBillId());
////                    pDAO.saveOrUpdate(p);
////                }
////            }
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
////            err.setErrorCode(
////                    "U000-0000-0000");
////            err.setErrorName(
////                    "Lỗi nhận kết quả nộp phí");
////            err.setSolution(
////                    "Kiểm tra chỉ tiêu thông tin và hàm validate");
////            errList.add(err);
////        }
////        return errList;
////    }
////
////    //Nhận thông báo thời gian, địa điêm tập kết hàng hóa
////    public List<com.viettel.ws.BO.Error> reiceiveMs_21(Envelope envelope, Files files) {
////        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
////        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////        try {
////            FileResults fc = envelope.getBody().getContent().getFileResults();
////            ProcessDocumentDAO pdDAO = new ProcessDocumentDAO();
////            ProcessDocument pd = pdDAO.getProcessDocumentByType(Constants.NSW_SERVICE.TYPE_DN, files.getFileId());
////            if (pd == null) {
////                pd = new ProcessDocument();
////            }
////            pd.setCheckDate(!StringUtils.validString(fc.getCheckDate()) ? null : formatterDateTime.parse(fc.getCheckDate()));
////            pd.setCheckPlace(fc.getCheckPlace());
////            pd.setComents(fc.getComments());
////            pd.setProcessPhone(fc.getContactPhone());
////            pd.setType(Constants.NSW_SERVICE.TYPE_DN);
////            pd.setIsActive(1L);
////            pd.setFileId(files.getFileId());
////            pd.setCreatedDate(new Date());
////            pdDAO.saveOrUpdate(pd);
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
////            err.setErrorCode("U000-0000-0000");
////            err.setErrorName("Lỗi nhận thông báo thời gian, địa điểm tập kết hàng hóa");
////            err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
////            errList.add(err);
////        }
////        return errList;
////    }
////
////    //Nhận hồ sơ xử lý mặt hàng không đạt từ NSW
////    public List<com.viettel.ws.BO.Error> reiceiveMs_25_34(Envelope envelope, Files files, Boolean report) throws FileNotFoundException, Base64DecodingException {
////        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
////        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
////        try {
////            ImportDocument imDoc = envelope.getBody().getContent().getImportDocument();
////            Long attCat = !report ? Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS : Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPORT;
////            AttachDAOHE attDAO = new AttachDAOHE();
////            List<Product> lsProduct = imDoc.getProductList();
////            ImportOrderProductDAO imPDAO = new ImportOrderProductDAO();
////            if (lsProduct != null && lsProduct.size() > 0) {
////                for (Product p : lsProduct) {
////                    ImportOrderProduct imP = imPDAO.findByProductCode(files.getFileId(), p.getProductCode());
////                    if (imP == null) {
////                        err.setErrorCode("P000-0000-0000");
////                        err.setErrorName("Lỗi tìm kiếm sản phẩm theo productCode");
////                        err.setSolution("Kiểm tra chỉ tiêu thông tin productCode đã nhập");
////                        errList.add(err);
////                        return errList;
////                    }
////                    imP.setProcessType(!StringUtils.validString(p.getProcessType()) ? null : Long.parseLong(p.getProcessType()));
////                    imP.setProcessTypeName(p.getProcessTypeName());
////                    imP.setReProcess(!StringUtils.validString(p.getReProcess()) ? null : Long.parseLong(p.getReProcess()));
////                    imP.setComments(p.getComment());
////                    attDAO.deleteAttach(imP.getProductId(), attCat, null);
////                    List<Attachment> lsAttm = p.getAttachmentList();
////                    if (lsAttm != null && lsAttm.size() > 0) {
////                        for (Attachment att : lsAttm) {
////                            Boolean k = saveAttachmentFromNSW(att, imP.getProductId(), attCat, imDoc.getNswFileCode());
////                            if (!k) {
////                                err.setErrorCode("U000-0000-0000");
////                                err.setErrorName("Lỗi tải file đính kèm phí");
////                                err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
////                                errList.add(err);
////                                return errList;
////
////                            }
////                        }
////                    }
////                }
////            }
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////            err.setErrorCode(
////                    "U000-0000-0000");
////            err.setErrorName(
////                    "Lỗi nhận hồ sơ/kết quả xử lý mặt hàng không đạt");
////            err.setSolution(
////                    "Kiểm tra chỉ tiêu thông tin và hàm validate");
////            errList.add(err);
////        }
////        return errList;
////    }
////
////    public com.viettel.ws.BO.Error CheckFiles(String processId, Files f, String function) {
////        com.viettel.ws.BO.Error rs = null;
////        if ("01".equals(function) || "40".equals(function)) {
////            if (StringUtils.validString(processId)) {
////                rs = new com.viettel.ws.BO.Error();
////                rs.setErrorCode("MsID-0000-0000");
////                rs.setErrorName("messeageId phải để trống trong trường hợp hồ sơ mới");
////                rs.setSolution("Bỏ trường messeageId trong header");
////            }
////            if (f != null) {
////                rs = new com.viettel.ws.BO.Error();
////                rs.setErrorCode("NswFilecode-0000-0000");
////                rs.setErrorName("NswFilecode đã có trong hệ thống");
////                rs.setSolution("NswFilecode nhập không chính xác");
////            }
////        } else if (!StringUtils.validString(processId)) {
////            rs = new com.viettel.ws.BO.Error();
////            rs.setErrorCode("MsID-0000-0000");
////            rs.setErrorName("messeageId không được đê trống");
////            rs.setSolution("Thêm trường messeageId ứng với processId (hệ thống BYT) tương ứng trong header");
////        } else if (f == null) {
////            rs = new com.viettel.ws.BO.Error();
////            rs.setErrorCode("MsID-0000-0000");
////            rs.setErrorName("nswFileCode không chuẩn xác");
////            rs.setSolution("Thêm trường nswFileCode chuẩn xác");
////
////        } else {
////            try {
////                if (!"35".equals(function) && !"32".equals(function)) {
////                    ProcessDAOHE pDAOHE = new ProcessDAOHE();
////                    com.viettel.core.workflow.BO.Process currentP = pDAOHE.findById(!StringUtils.validString(processId) ? null : Long.parseLong(processId));
////                    if (currentP == null || currentP.getFinishDate() != null || !currentP.getReceiveUser().equals(f.getTaxCode())) {
////                        rs = new com.viettel.ws.BO.Error();
////                        rs.setErrorCode("MsID-0000-0000");
////                        rs.setErrorName("messeageId nhập sai dữ liệu");
////                        rs.setSolution("Thêm trường messeageId ứng với processId (hệ thống BYT) tương ứng trong header");
////                    }
////                }
////            } catch (Exception ex) {
////
////                Logger.getLogger(HelperID.class
////                        .getName()).log(Level.SEVERE, null, ex);
////                rs = new com.viettel.ws.BO.Error();
////
////                rs.setErrorCode(
////                        "MsID-0000-0000");
////                rs.setErrorName(
////                        "messeageId nhập sai dữ liệu");
////                rs.setSolution(
////                        "Thêm trường messeageId ứng với processId (hệ thống BYT) tương ứng trong header");
////
////            }
////        }
////        return rs;
////    }
////
////    //Nhận ms từ NSW
//    public String receiveMs(String evl) throws ParseException, IOException, Exception {
//        Body bd = new Body();
//        Content ct = new Content();
//        List<com.viettel.ws.BO_IO.Error> errList = new ArrayList<>();
//        com.viettel.ws.BO_IO.Error err = null;
//        String type = null;
//        String function = null;
//        String ref = null;
//        String pref = null;
//        String msId = null;
//        FilesDAOHE fDAOHE = new FilesDAOHE();
//        try {
//            Envelope envelop = xmlToEnvelope(evl);
//            // String docType = Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT;
//            if (envelop != null) {
//                type = envelop.getHeader().getSubject().getType();
//                
//                //linhdx khong hieu ref và pref de lam gi
////                ref = envelop.getHeader().getSubject().getReference();
////                pref = envelop.getHeader().getSubject().getPreReference();
//                
//                pref = envelop.getHeader().getSubject().getPreReference();
//                ref = pref;
//                
//                msId = envelop.getHeader().getReference().getMessageId();
//                function = envelop.getHeader().getSubject().getFunction();
//                Helper_QTest hpQ = new Helper_QTest();
//                Helper_VofficeMoh hpTTB = new Helper_VofficeMoh();
//                String docType = Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT;
//                WorkflowAPI w = new WorkflowAPI();
//                Long docTypeId = w.getProcedureTypeIdByCode(docType);
//                Files f = fDAOHE.findByNswCodeAndDoctype(ref,docTypeId);
//                if (!"35".equals(function) && !"32".equals(function)) {
////                    err = CheckFiles(msId, f, function);
//                }
//                if (err != null) {
////                    errList.add(err);
////                    ct.setErrorList(errList);
////                    function = Constants.NSW_FUNCTION(22L);
//                } else {
//                    err = new com.viettel.ws.BO_IO.Error();
//                    switch (function) {
////                        case "35":
////                            ct = receiveMs_35(envelop, f);
////                            if (ct.getErrorList() != null) {
////                                function = Constants.NSW_FUNCTION(22L);
////                            } else {
////                                function = Constants.NSW_FUNCTION(37L);
////                                type = Constants.NSW_TYPE(37L);
////                            }
////                            break;
////                        case "32":
////                            docType = Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT;
////                            ct = hpQ.receiveMs_32(envelop, f);
////                            if (ct.getErrorList() != null) {
////                                function = Constants.NSW_FUNCTION(22L);
////                            } else {
////                                function = Constants.NSW_FUNCTION(33L);
////                                type = Constants.NSW_TYPE(63L);
////                            }
////                            break;
//                        default:
//                            switch (function) {
//                                case "40":
//                                    if (Constants.NSW_TYPE(66L).equals(type)) {
//                                        errList = receiveMs40_66(envelop, f, false);
//                                    } else //                                    {
//                                    //                                        docType = Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT;
//                                    //                                        errList = hpQ.receiveMs01_02(envelop, f, type, false);
//                                    //                                    }
//                                    {
//                                        break;
//                                    }
////                                case "02":
////                                    if (Constants.NSW_TYPE(21L).equals(type)) {
////                                        errList = receiveMs01_02(envelop, f, true);
////                                    } else {
////                                        docType = Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT;
////                                        errList = hpQ.receiveMs01_02(envelop, f, type, true);
////                                    }
////                                    break;
////                                case "21":
////                                    errList = reiceiveMs_21(envelop, f);
////                                    break;
////                                case "19":
////                                    if (Constants.NSW_TYPE(24L).equals(type)) {
////                                        errList = reiceiveMs_19_20(envelop, f);
////                                    } else {
////                                        docType = Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT;
////                                        errList = hpQ.receiveMs_19(envelop, f);
////                                    }
////                                    break;
////                                case "20":
////                                    errList = reiceiveMs_19_20(envelop, f);
////                                    break;
////                                case "44":
////                                    errList = hpTTB.reiceiveMs_44(envelop, f);
////                                    docType = Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT;
////                                    break;
////                                case "40":
////                                    errList = hpTTB.reiceiveMs_40_41(envelop, f);
////                                    docType = Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT;
////                                    break;
////                                case "41":
////                                    errList = hpTTB.reiceiveMs_40_41(envelop, f);
////                                    docType = Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT;
////                                    break;
////                                case "25":
////                                    if (Constants.NSW_TYPE(34L).equals(type)) {
////
////                                        errList = reiceiveMs_25_34(envelop, f, true);
////                                    } else {
////                                        errList = reiceiveMs_25_34(envelop, f, false);
////                                    }
////                                    break;
//                            }
//                            if (errList.size() > 0) {
//                               // ct.setErrorList(errList);
//                                switch (function) {
//                                    case "02":
//                                        if (Constants.NSW_TYPE(21L).equals(type)) {
//                                            saveOrUpdateFiles(ref, false);
//                                        } else {
//                                            hpQ.saveOrUpdateFiles(ref, false);
//                                        }
//                                        break;
//                                    case "41":
//                                        hpTTB.saveOrUpdateFiles(ref, false);
//                                        break;
//                                    case "25":
//                                        deleteAtt(f.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS);
//                                        break;
//                                    case "34":
//                                        deleteAtt(f.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPORT);
//                                        break;
//                                }
//                                function = Constants.NSW_FUNCTION(22L);
//                            } else {
//                                WorkflowAPI w = new WorkflowAPI();
//                                Category ftype = w.getProcedureTypeByCode(docType);
//                                Long isactive = 1L;
//                                if ("01".equals(function) || "02".equals(function) || "41".equals(function) || "40".equals(function)) {
//                                    isactive = 0L;
//                                    f = fDAOHE.findByNswCode(ref);
//                                }
//
//                                Boolean k = saveOrUpdateProcess(msId, ref, ftype.getCategoryId(), isactive);
//                                if (k == true) {
//                                   // ct.setReceiveDate(formatterDateTime.format(new Date()));
//                                    if (Constants.NSW_FUNCTION(2L).equals(function) || Constants.NSW_FUNCTION(1L).equals(function)) {
//                                        saveOrUpdateFiles(ref, true);
//                                    } else if (Constants.NSW_FUNCTION(40L).equals(function) || Constants.NSW_FUNCTION(41L).equals(function)) {
//                                        hpTTB.saveOrUpdateFiles(ref, true);
//                                    }
//                                    function = Constants.NSW_FUNCTION(99L);
//                                } else {
//                                    err.setErrorCode("L000-0000-0000");
//                                    err.setErrorName("Lỗi cập nhật process sau khi nhận ms thành công");
//                                    err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin");
//                                    errList.add(err);
//                                    Logger
//                                            .getLogger(HelperID.class
//                                                    .getName()).log(Level.SEVERE, null, err);
//                                   // ct.setErrorList(errList);
//                                    switch (function) {
//                                        case "02":
//                                            if (Constants.NSW_TYPE(21L).equals(type)) {
//                                                saveOrUpdateFiles(ref, false);
//                                            } else {
//                                                hpQ.saveOrUpdateFiles(ref, false);
//                                            }
//                                            break;
//                                        case "41":
//                                            hpTTB.saveOrUpdateFiles(ref, false);
//                                            break;
//                                        case "25":
//                                            deleteAtt(f.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS);
//                                            break;
//                                        case "34":
//                                            deleteAtt(f.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPORT);
//                                            break;
//                                    }
//                                    function = Constants.NSW_FUNCTION(22L);
//                                }
//                            }
//                            break;
//                    }
//                }
//            } else {
//                err = new com.viettel.ws.BO_IO.Error();
//                err.setErrorCode("L000-0000-0000");
//                err.setErrorName("Lỗi convert thông điệp ra object");
//                err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin");
//                errList.add(err);
//                Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, err);
//               // ct.setErrorList(errList);
//                function = Constants.NSW_FUNCTION(22L);
//            }
//        } catch (Exception ex) {
//            err = new com.viettel.ws.BO_IO.Error();
//            err.setErrorCode("L000-0000-0000");
//            err.setErrorName("Lỗi nhận dữ liệu");
//            err.setSolution("Kiểm tra lại kết nối giữa 2 hệ thống");
//            errList.add(err);
//            Logger.getLogger(HelperID.class.getName()).log(Level.SEVERE, null, ex);
//          //  ct.setErrorList(errList);
//            function = Constants.NSW_FUNCTION(22L);
//        }
//        Envelope ev = makeEnvelopeSend(type, function, ref, pref, msId);
//        bd.setContent(ct);
//        ev.setBody(bd);
//        if (Constants.NSW_TYPE(22L).equals(type)) {
//            fDAOHE.rollBack();
//        }
//        return ObjectToXml(ev);
//    }
////
////    //Hàm cập nhật luồng sau khi nhận ms thành công từ NSW
//    public Boolean saveOrUpdateProcess(String processId, String nswCode, Long docType, Long isactive) {
//        try {
//
//            FilesDAOHE fDAOHE = new FilesDAOHE();
//            Files files = fDAOHE.findByNswCode(nswCode, 0L, isactive);
//            WorkflowAPI workflowInstance = WorkflowAPI.getInstance();
//            List<NodeToNode> actions = new ArrayList<>();
//            List<Flow> lstFlows = workflowInstance.getFlowByDeptNObject(Constants.VU_TBYT_ID, docType);
//            Flow f = new Flow();
//            if (lstFlows != null) {
//                f = lstFlows.get(0);
//            }
//            ProcessDAOHE pDAOHE = new ProcessDAOHE();
//            if (!StringUtils.validString(processId)) {
//                Node nextNode = WorkflowAPI.getInstance()
//                        .getFirstNodeOfFlow(f);
//                actions = workflowInstance.getNextActionOfNodeId(nextNode.getNodeId());
//                NodeToNode nTn = actions.get(0);
//                List<NodeDeptUser> lsnDu = WorkflowAPI.getInstance().findNDUsByNodeId(nTn.getNextId(), false);
//                for (NodeDeptUser nDu : lsnDu) {
//                    com.viettel.core.workflow.BO.Process p = new com.viettel.core.workflow.BO.Process();
//                    p.setActionName(nTn.getAction());
//                    p.setActionType(nTn.getType());
//                    p.setIsActive(1L);
//                    p.setNodeId(nTn.getNextId());
//                    p.setSendUser(files.getTaxCode());
//                    p.setSendUserId(files.getCreatorId());
//                    p.setStatus(nTn.getStatus());
//                    p.setReceiveUserId(nDu.getUserId());
//                    p.setReceiveUser(nDu.getUserName());
//                    p.setNote(nDu.getNodeName());
//                    p.setProcessType(nDu.getProcessType());
//                    p.setReceiveGroupId(nDu.getDeptId());
//                    p.setReceiveGroup(nDu.getDeptName());
//                    p.setObjectId(files.getFileId());
//                    p.setSendDate(new Date());
//                    p.setObjectType(files.getFileType());
//                    p.setOrderProcess(0L);
//                    p.setPreviousNodeId(nTn.getPreviousId());
//                    pDAOHE.saveOrUpdate(p);
//                    files.setStatus(nTn.getStatus());
//                }
//                fDAOHE.saveOrUpdate(files);
//
//            } else {
//                com.viettel.core.workflow.BO.Process currentP = pDAOHE.findById(!StringUtils.validString(processId) ? null : Long.parseLong(processId));
//                actions = workflowInstance.getNextActionOfNodeId(currentP.getNodeId());
//
//                NodeToNode nTn = actions.get(0);
//                if (actions.size() > 0 && actions.get(0).getFormId() == null) {
//                    nTn = actions.get(1);
//                }
//                List<NodeDeptUser> lsnDu = WorkflowAPI.getInstance().findNDUsByNodeId(nTn.getNextId(), false);
//                List<com.viettel.core.workflow.BO.Process> lsPRcU = pDAOHE.getDescProcess(files.getFileId(), docType);
//                for (int i = 0; i < lsnDu.size(); i++) {
//                    NodeDeptUser nduReceive = lsnDu.get(i);
//                    com.viettel.core.workflow.BO.Process p = new com.viettel.core.workflow.BO.Process();
//                    if (nduReceive.getUserId() != null) {
//                        Boolean ctn = true;
//                        for (com.viettel.core.workflow.BO.Process ps : lsPRcU) {
//                            if (nduReceive.getUserId().equals(ps.getReceiveUserId())) {
//                                ctn = false;
//                                break;
//                            }
//                        }
//                        if (ctn) {
//                            continue;
//                        }
//                        p.setReceiveUserId(nduReceive.getUserId());
//                        p.setReceiveUser(nduReceive.getUserName());
//                    } else {
//                        UserDAOHE usDH = new UserDAOHE();
//                        List<Users> lsu = usDH.getUserByDeptPosID(nduReceive.getDeptId(), nduReceive.getPosId());
//                        Users user = null;
//                        if (lsPRcU != null && lsPRcU.size() > 0 && lsu != null && lsu.size() > 0) {
//                            for (Users u : lsu) {
//                                if (user == null && lsPRcU.contains(u.getUserId())) {
//                                    user = u;
//                                    break;
//                                }
//                            }
//                        }
//                        if (user == null) {
//                            continue;
//                        };
//                        p.setReceiveUserId(user.getUserId());
//                        p.setReceiveUser(user.getUserName());
//                    }
//                    p.setActionName(nTn.getAction());
//                    p.setActionType(nTn.getType());
//                    p.setIsActive(1L);
//                    p.setNodeId(nTn.getNextId());
//                    p.setSendUser(files.getTaxCode());
//                    p.setSendUserId(files.getCreatorId());
//                    p.setStatus(nTn.getStatus());
//                    p.setReceiveGroupId(nduReceive.getDeptId());
//                    p.setReceiveGroup(nduReceive.getDeptName());
//                    p.setNote(nduReceive.getNodeName());
//                    p.setObjectId(files.getFileId());
//                    p.setObjectType(files.getFileType());
//                    p.setSendDate(new Date());
//                    p.setPreviousNodeId(nTn.getPreviousId());
//                    p.setProcessType(nduReceive.getProcessType());
//                    p.setOrderProcess(currentP.getOrderProcess() + 1L);
//                    p.setParentId(currentP.getProcessId());
//                    files.setStatus(nTn.getStatus());
//                    pDAOHE.saveOrUpdate(p);
//                    currentP.setFinishDate(new java.util.Date());
//                    currentP.setIsActive(1L);
//                    pDAOHE.saveOrUpdate(currentP);
//                }
//                if (isactive == 0L) {
//                    fDAOHE.saveOrUpdate(files);
//                } else {
//                    fDAOHE.SaveAndCommit(files);
//                }
//            }
//            return true;
//
//        } catch (Exception ex) {
//            Logger.getLogger(HelperID.class
//                    .getName()).log(Level.SEVERE, null, ex);
//            return false;
//        }
//    }
////    //Các hàm gửi kết quả, gửi yêu cầu
////    //Gửi ms to NSW
////
////    private Long getUserId() {
////        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
////                "userToken");
////        return tk.getUserId();
////    }
////
////    public Boolean sendMs(Envelope envelop) throws FileNotFoundException, IOException, Base64DecodingException {
////        Boolean rs = true;
////        if ("false".equals(ResourceBundleUtil.getString("send_service", "config"))) {
////            return true;
////        }
////        try {
////            MOHService moh = new MOHService();
////            IMOHService client = moh.getBasicHttpBindingIMOHService();
////            String evlrs = ObjectToXml(envelop);
////            // hàm nhận kết quả
////            String response = client.receive(evlrs);
////            Envelope enveloprs = xmlToEnvelope(response);
////            String function = enveloprs.getHeader().getSubject().getFunction();
////            switch (function) {
////                //36
////                case "37":
////                    Attachment att = enveloprs.getBody().getContent().getAttachment();
////                    Long attsId = !StringUtils.validString(enveloprs.getHeader().getReference().getMessageId()) ? null : Long.parseLong(enveloprs.getHeader().getReference().getMessageId());
////                    AttachDAOHE attDH = new AttachDAOHE();
////                    Attachs attach = attDH.findById(attsId);
////                    File f = new File(attach.getFullPathFile());
////                    if (att != null && StringUtils.validString(att.getFileByte())) {
////                        byte[] data = Base64.decodeBase64(att.getFileByte());
////                        OutputStream outputStream = new FileOutputStream(f);
////                        IOUtils.write(data, outputStream);
////                        outputStream.close();
////                    } else {
////                        rs = false;
////                    }
////                    break;
////                //33
////                case "33":
////                    ResponseAttachment rsatt = enveloprs.getBody().getContent().getResponseAttachment();
////                    Long attsId32 = !StringUtils.validString(enveloprs.getHeader().getReference().getMessageId()) ? null : Long.parseLong(enveloprs.getHeader().getReference().getMessageId());
////                    AttachDAOHE attDH32 = new AttachDAOHE();
////                    Attachs attach32 = attDH32.findById(attsId32);
////                    File f32 = new File(attach32.getFullPathFile());
////                    if (rsatt != null && rsatt.getFileByte() != null) {
////                        byte[] data32 = Base64.decodeBase64(rsatt.getFileByte());
////                        OutputStream outputStream32 = new FileOutputStream(f32);
////                        IOUtils.write(data32, outputStream32);
////                        outputStream32.close();
////                    } else {
////                        //rs =false, test để true
////                        rs = true;
////                    }
////                    break;
////
////                case "22":
////                    Logger.getLogger(HelperID.class
////                            .getName()).log(Level.SEVERE, null, ObjectToXml(enveloprs));
////                    rs = false;
////                    break;
////
////                case "99":
////                    break;
////                default:
////                    rs = false;
////
////                    break;
////            }
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////            rs = false;
////        }
////        return rs;
////    }
////
////    //Yêu cầu SDBS
////    public Boolean senMS_04(Long fileId, Long phase, Long processId, Boolean isUpdate) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////            AttachDAOHE atDAOHE = new AttachDAOHE();
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(phase == 1L ? 33L : 34L), Constants.NSW_FUNCTION(4L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////            AdditionalRequestDAO arqDAO = new AdditionalRequestDAO();
////            Body bd = new Body();
////            Content ct = new Content();
////            FilesEdit fedit = new FilesEdit();
////            Attachs a = null;
////            AdditionalRequest addrq = arqDAO.findLastActiveByFileId(fileId, getUserId());
////
////            if (addrq != null) {
////                a = atDAOHE.getLastByObjectIdAndType(addrq.getAdditionalRequestId(), Constants.CATEGORY_TYPE.IMPORT_ORDER_AMENDMENT_PAPER);
////            }
////            Attachment atm = new Attachment();
////            if (a != null) {
////                atm.setAttachmentId(a.getAttachId().toString());
////                atm.setAttachmentName(a.getAttachName());
////                atm.setAttachTypeCode(a.getAttachType() == null ? "" : a.getAttachType().toString());
////                atm.setAttachTypeName(a.getAttachTypeName());
////            }
////            if (addrq != null) {
////                fedit.setComments(addrq.getContent());
////                fedit.setProcessDate(addrq.getCreateDate() == null ? "" : formatterDateTime.format(addrq.getCreateDate()));
////                fedit.setProcessName(addrq.getCreatorName());
////            }
////            if (isUpdate) {
////                ProcessDAOHE psDH = new ProcessDAOHE();
////                com.viettel.core.workflow.BO.Process ps = psDH.findById(processId);
////                fedit.setComments(ps.getNote());
////            }
////            fedit.setDeptCode(Constants.NSW_SERVICE.DEPT_CODE);
////            fedit.setDeptName(Constants.NSW_SERVICE.DEPT_NAME);
////            fedit.setAttachment(atm);
////            ct.setFilesEdit(fedit);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
////    //Yêu cầu SDBS
////    public Boolean senMS_18(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(33L), Constants.NSW_FUNCTION(18L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////
////            ProcessDAOHE psDAO = new ProcessDAOHE();
////            com.viettel.core.workflow.BO.Process ps = psDAO.findById(processId);
////
////            Body bd = new Body();
////            Content ct = new Content();
////            FilesEdit fedit = new FilesEdit();
////            fedit.setComments(ps.getNote());
////            fedit.setProcessDate(ps.getSendDate() == null ? "" : formatterDateTime.format(ps.getSendDate()));
////            fedit.setProcessName(ps.getSendUser());
////            fedit.setDeptCode(Constants.NSW_SERVICE.DEPT_CODE);
////            fedit.setDeptName(Constants.NSW_SERVICE.DEPT_NAME);
////            ct.setFilesEdit(fedit);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
////    //Gửi đơn đăng ký đã được duyệt
////    public Boolean senMS_38(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////
////            AttachDAOHE attDH = new AttachDAOHE();
////            UserDAOHE us = new UserDAOHE();
//////            PermitDAO peDAO = new PermitDAO();
//////            Permit pe = peDAO.findLastByFileIdAndType(imFile.getFileId(), Constants.PERMIT_TYPE.REGISTERED_PAPER);
//////            if (pe == null) {
//////                return false;
//////            }
////            Attachs atts = attDH.getLastByObjectIdAndType(fileId, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REGISTERED_PAPER);
////            if (atts == null) {
////                return false;
////            }
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(35L), Constants.NSW_FUNCTION(38L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////            ImportDocument id = new ImportDocument();
////            Attachment attach = new Attachment();
////            attach.setAttachmentId(atts.getAttachId().toString());
////            attach.setAttachmentName(atts.getAttachName());
////            attach.setAttachTypeCode(atts.getAttachType().toString());
////            attach.setAttachTypeName(atts.getAttachTypeName());
////            attach.setAttachmentCode(atts.getAttachId().toString());
////            List<Attachment> lsA = new ArrayList<>();
////            lsA.add(attach);
////            id.setNswFileCode(imFile.getNswFileCode());
////            id.setDeptCode(Constants.NSW_SERVICE.DEPT_CODE);
////            id.setDeptName(Constants.NSW_SERVICE.DEPT_NAME);
////            id.setProcessDate(atts.getDateCreate() == null ? "" : formatterDateTime.format(atts.getDateCreate()));
////            id.setProcessName(us.findById(atts.getCreatorId()).getUserName());
////            id.setAttachmentList(lsA);
////            Body bd = new Body();
////            Content ct = new Content();
////            ct.setImportDocument(id);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
////    //Gửi yêu cầu nộp phí + phí bổ sung (phase = 0  gửi lên cục, phase = 1 gửi phí tới CQKT)
////    public Boolean senMS_11_12_13_14(Long fileId, Boolean update, Long phase, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////
////            String function = update ? Constants.NSW_FUNCTION(12L) : Constants.NSW_FUNCTION(11L);
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(23L), function, imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////            FilesCost fc = new FilesCost();
////            //com.viettel.module.evaluation.DAO.EvaluationRecordDAO ercDAO = new com.viettel.module.evaluation.DAO.EvaluationRecordDAO();
////            //EvaluationRecord erc = ercDAO.findLastActiveByFileId(fileId);
////            //Gson gson = new Gson();
////            //EvaluationModel model = gson.fromJson(erc.getMainContent(), EvaluationModel.class);
////            ProcessDAOHE psDAOHE = new ProcessDAOHE();
////            com.viettel.core.workflow.BO.Process curenP = psDAOHE.findById(processId);
////
////            fc.setComments(curenP.getNote());
////            fc.setNswFileCode(imFile.getNswFileCode());
////            fc.setProcessDate(curenP.getSendDate() == null ? "" : formatterDateTime.format(curenP.getSendDate()));
////            fc.setProcessName(curenP.getSendUser());
////            fc.setType(update ? Constants.NSW_SERVICE.FEE_STATUS.FEE_EDIT : Constants.NSW_SERVICE.FEE_STATUS.FEE_NEW);
////            List<Fee> lsFee = new ArrayList<>();
////            PaymentInfoDAO pinfoDAO = new PaymentInfoDAO();
////            Long status = update ? Constants.NSW_SERVICE.FEE_STATUS.TU_CHOI : Constants.NSW_SERVICE.FEE_STATUS.MOI_TAO;
////            List<PaymentInfo> lsPinfo = pinfoDAO.getListPaymentByStatus(imFile.getFileId(), status, phase);
////            Long cost = 0L;
////            if (lsPinfo != null && lsPinfo.size() > 0) {
////                for (PaymentInfo p : lsPinfo) {
////                    Fee f = new Fee();
////                    f.setBankNo(p.getBankNo());
////                    f.setComments(p.getFeeName());
////                    cost += p.getCost();
////                    f.setCost(p.getCost().toString());
////                    f.setDeptCode(p.getDeptCode());
////                    f.setDeptName(p.getDeptName());
////                    f.setFeeId(p.getPaymentInfoId().toString());
////                    lsFee.add(f);
////                }
////            }
////            if (cost == 0L) {
////                return true;
////            }
////            fc.setFeeList(lsFee);
////            Content ct = new Content();
////            Body bd = new Body();
////            ct.setFilesCost(fc);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
////    //Gửi thông báo tiếp nhận kiểm tra
////    public Boolean senMS_10(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(26L), Constants.NSW_FUNCTION(10L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////            Body bd = new Body();
////            Content ct = new Content();
////            FileResults frs = new FileResults();
////            ProcessDAOHE psDAOHE = new ProcessDAOHE();
////            com.viettel.core.workflow.BO.Process curenP = psDAOHE.findById(processId);
////            WorkflowAPI w = new WorkflowAPI();
////            Department dept = w.getParentDeptByUsId(curenP.getSendUserId());
////            frs.setNswFileCode(imFile.getNswFileCode());
////            if (dept != null) {
////                frs.setDeptCode(dept.getDeptCode());
////                frs.setDeptName(dept.getDeptName());
////                imFile.setCheckDeptId(dept.getDeptId());
////                imFile.setCheckDeptCode(dept.getDeptCode());
////                imFile.setCheckDeptName(dept.getDeptName());
////                imDAO.saveOrUpdate(imFile);
////            }
////            frs.setComments(Constants.NSW_SERVICE.COMMENS.TIEP_NHAN_KT);
////            List<Product> lsP = new ArrayList<>();
////            ImportOrderProductDAO imPDAO = new ImportOrderProductDAO();
////            List<ImportOrderProduct> lsImP = imPDAO.getImportOrderProductList(imFile.getFileId());
////            if (lsImP != null && lsImP.size() > 0) {
////                for (ImportOrderProduct imP : lsImP) {
////                    Product p = new Product();
////                    p.setProductId(imP.getProductId().toString());
////                    p.setProductName(imP.getProductName());
////                    p.setProductCode(imP.getProductCode());
////                    p.setConfirmAnnounceNo(imP.getConfirmAnnounceNo());
////                    p.setCheckMethodCode(Constants.NSW_SERVICE.TYPE_GIAM_HT.equals(imP.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_GIAM : (Constants.NSW_SERVICE.TYPE_THUONG_HT.equals(imP.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_THUONG : Constants.NSW_SERVICE.TYPE_CHAT));
////                    p.setCheckMethodName(imP.getCheckMethodName());
////                    lsP.add(p);
////                }
////            }
////            frs.setProductList(lsP);
////            ct.setFileResults(frs);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
////    //Gửi thông báo thời gian, kế hoạch kiểm tra
////    public Boolean senMS_23(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(27L), Constants.NSW_FUNCTION(23L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////            Body bd = new Body();
////            Content ct = new Content();
////            FileResults frs = new FileResults();
////            ProcessDocumentDAO pdDAO = new ProcessDocumentDAO();
////            ProcessDocument pd = pdDAO.getProcessDocumentByType(Constants.NSW_SERVICE.TYPE_CV, imFile.getFileId());
////            frs.setNswFileCode(imFile.getNswFileCode());
////            frs.setDeptCode(imFile.getCheckDeptCode());
////            frs.setDeptName(imFile.getCheckDeptName());
////            frs.setComments(pd.getComents());
////            frs.setContactPhone(pd.getProcessPhone());
////            frs.setProcessPhone(pd.getProcessPhone());
////            frs.setProcessName(pd.getProcessName());
////            frs.setCheckDate(pd.getCheckDate() == null ? "" : formatterDateTime.format(pd.getCheckDate()));
////            frs.setCheckPlace(pd.getCheckPlace());
////            List<Product> lsP = new ArrayList<>();
////            ImportOrderProductDAO imPDAO = new ImportOrderProductDAO();
////            List<ImportOrderProduct> lsImP = imPDAO.getImportOrderProductList(imFile.getFileId());
////            if (lsImP != null && lsImP.size() > 0) {
////                for (ImportOrderProduct imP : lsImP) {
////                    Product p = new Product();
////                    p.setProductId(imP.getProductId().toString());
////                    p.setProductName(imP.getProductName());
////                    p.setProductCode(imP.getProductCode());
////                    p.setConfirmAnnounceNo(imP.getConfirmAnnounceNo());
////                    p.setCheckMethodCode(Constants.NSW_SERVICE.TYPE_GIAM_HT.equals(imP.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_GIAM : (Constants.NSW_SERVICE.TYPE_THUONG_HT.equals(imP.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_THUONG : Constants.NSW_SERVICE.TYPE_CHAT));
////                    p.setCheckMethodName(imP.getCheckMethodName());
////                    lsP.add(p);
////                }
////            }
////            frs.setProductList(lsP);
////            ct.setFileResults(frs);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
////    //Gửi kết quả xử lý các mặt hàng ktra giảm
////    public Boolean senMS_15(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            FilesDAOHE fDAOHE = new FilesDAOHE();
////            Files files = fDAOHE.findById(fileId);
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////            PermitDAO pmDAO = new PermitDAO();
////            Permit pm = pmDAO.findLastByFileIdAndType(fileId, Constants.PERMIT_TYPE.REDUCED_PAPER);
////
////            if (pm == null) {
////                Logger.getLogger(HelperID.class
////                        .getName()).log(Level.SEVERE, null, Constants.NSW_SERVICE.COMMENS.ERR_PERMIT_REDUCE);
////
////                return false;
////            }
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(25L), Constants.NSW_FUNCTION(15L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////            Body bd = new Body();
////            Content ct = new Content();
////            ImportDocument imDoc = new ImportDocument();
////            imDoc.setBillNo(imFile.getBillNo());
////            imDoc.setCheckPlace(imFile.getCheckPlace());
////            imDoc.setCheckTime(imFile.getCheckTime() == null ? "" : formatterDateTime.format(imFile.getCheckTime()));
////            imDoc.setComingDate(imFile.getComingDate() == null ? "" : formatterDateTime.format(imFile.getComingDate()));
////            imDoc.setContractNo(imFile.getContractNo());
////            imDoc.setCreatedBy(files.getTaxCode());
////            imDoc.setCreatedDate(files.getCreateDate() == null ? "" : formatterDateTime.format(files.getCreateDate()));
////            imDoc.setCreaterName(files.getCreatorName());
////            imDoc.setCustomsCode(imFile.getCustomsBranchCode());
////            imDoc.setCustomsName(imFile.getCustomsBranchName());
////            imDoc.setDeptCode(Constants.NSW_SERVICE.DEPT_CODE);
////            imDoc.setDeptName(Constants.NSW_SERVICE.DEPT_NAME);
////            imDoc.setExporterAddress(imFile.getExporterAddress());
////            imDoc.setExporterEmail(imFile.getExporterEmail());
////            imDoc.setExporterFax(imFile.getExporterFax());
////            imDoc.setExporterGateCode(imFile.getExporterGateCode());
////            imDoc.setExporterGateName(imFile.getExporterGateName());
////            imDoc.setExporterName(imFile.getExporterGateName());
////            imDoc.setExporterPhone(imFile.getExporterPhone());
////            imDoc.setFilesNo(pm.getReceiveNo());
////            imDoc.setGoodsOwnerAddress(files.getBusinessAddress());
////            imDoc.setGoodsOwnerEmail(files.getBusinessEmail());
////            imDoc.setGoodsOwnerFax(files.getBusinessFax());
////            imDoc.setGoodsOwnerName(files.getBusinessName());
////            imDoc.setGoodsOwnerPhone(files.getBusinessPhone());
////            imDoc.setImporterGateCode(imFile.getImporterGateCode());
////            imDoc.setImporterGateName(imFile.getImporterGateName());
////            imDoc.setModifiedDate(files.getModifyDate() == null ? "" : formatterDateTime.format(files.getModifyDate()));
////            imDoc.setNswFileCode(files.getNswFileCode());
////            imDoc.setResponsiblePersonAddress(imFile.getResponsiblePersonAddress());
////            imDoc.setResponsiblePersonEmail(imFile.getResponsiblePersonEmail());
////            imDoc.setResponsiblePersonFax(imFile.getResponsiblePersonFax());
////            imDoc.setResponsiblePersonName(imFile.getResponsiblePersonName());
////            imDoc.setResponsiblePersonPhone(imFile.getResponsiblePersonPhone());
////            imDoc.setTransNo(imFile.getTransNo());
////            //1 Kiểm tra giảm
////            imDoc.setType(Constants.NSW_SERVICE.TYPE_GIAM);
////            List<Product> lsP = new ArrayList<>();
////            ImportOrderProductDAO imPDAO = new ImportOrderProductDAO();
////            List<ImportOrderProduct> lsImP = imPDAO.getImportOrderProductList(imFile.getFileId(), true);
////            if (lsImP != null && lsImP.size() > 0) {
////                for (ImportOrderProduct imP : lsImP) {
////                    Product p = new Product();
////                    p.setProductId(imP.getProductId().toString());
////                    p.setProductName(imP.getProductName());
////                    p.setProductCode(imP.getProductCode());
////                    p.setConfirmAnnounceNo(imP.getConfirmAnnounceNo());
////                    p.setBaseUnit(imP.getBaseUnit() == null ? "" : imP.getBaseUnit().toString());
////                    p.setCheckMethodCode(Constants.NSW_SERVICE.TYPE_GIAM_HT.equals(imP.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_GIAM : (Constants.NSW_SERVICE.TYPE_THUONG_HT.equals(imP.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_THUONG : Constants.NSW_SERVICE.TYPE_CHAT));
////                    p.setCheckMethodName(imP.getCheckMethodName());
////                    p.setReason(imP.getReason());
////                    p.setConfirmAnnounceNo(imP.getConfirmAnnounceNo());
////                    p.setManufacturer(imP.getManufacTurer());
////                    p.setManufacturerAddress(imP.getManufacturerAddress());
////                    p.setNationalCode(imP.getNationalCode());
////                    p.setNationalName(imP.getNationalName());
////                    p.setNetweight(imP.getNetweight() == null ? "" : imP.getNetweight().toString());
////                    p.setNetweightUnitCode(imP.getNetweightUnitCode());
////                    p.setNetweightUnitName(imP.getNetweightUnitName());
////                    p.setTotal(imP.getTotal() == null ? "" : imP.getTotal().toString());
////                    p.setTotalUnitCode(imP.getTotalUnitCode());
////                    p.setTotalUnitName(imP.getTotalUnitName());
////                    p.setPass(imP.getPass() == null ? "" : imP.getPass().toString());
////                    lsP.add(p);
////                }
////            }
////            AttachDAOHE attDAOHE = new AttachDAOHE();
////            List<Attachment> lsAttachment = new ArrayList<>();
////            //Lấy giấy thông báo kt giảm
//////            PermitDAO peDAO = new PermitDAO();
//////            Permit pe = peDAO.findLastByFileIdAndType(imFile.getFileId(), Constants.PERMIT_TYPE.REDUCED_PAPER);
//////            if (pe == null) {
//////                return false;
//////            }
////            Attachs att = attDAOHE.getLastByObjectIdAndType(fileId, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_REDUCED_PAPER);
////            if (att == null) {
////                return false;
////            }
////            imDoc.setProcessDate(att.getDateCreate() == null ? "" : formatterDateTime.format(att.getDateCreate()));
////            imDoc.setProcessName(att.getCreatorName());
////            Attachment a = new Attachment();
////            //  a.setAttachPath(att.getAttachPath());
////            a.setAttachTypeCode(att.getAttachType() == null ? "" : att.getAttachType().toString());
////            a.setAttachTypeName(att.getAttachTypeName());
////            a.setAttachmentId(att.getAttachId().toString());
////            a.setAttachmentName(att.getAttachName());
////            a.setNswFileCode(files.getNswFileCode());
////            a.setAttachmentCode(att.getAttachId().toString());
////            lsAttachment.add(a);
////            //Lấy file DN tải
////
//////        List<Attachs> lsATTDN = attDAOHE.getByObjectId(files.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE);
//////        if (lsATTDN != null && lsATTDN.size() > 0) {
//////            for (Attachs at : lsATTDN) {
//////                Attachment a = new Attachment();
//////                a.setAttachPath(at.getAttachCode().toString());
//////                a.setAttachTypeCode(at.getAttachType()==null?"":at.getAttachType().toString());
//////                a.setAttachTypeName(at.getAttachTypeName().toString());
//////                a.setAttachmentId(at.getAttachId().toString());
//////                a.setAttachmentName(at.getAttachName());
//////                a.setNswFileCode(files.getNswFileCode());
//////                lsAttachment.add(a);
//////            }
//////        }
////            imDoc.setProductList(lsP);
////            imDoc.setAttachmentList(lsAttachment);
////            ct.setImportDocument(imDoc);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
////    //Gửi kết quả xử lý các mặt hàng kiểm tra thường và chặt
////    public Boolean senMS_16(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            FilesDAOHE fDAOHE = new FilesDAOHE();
////            Files files = fDAOHE.findById(fileId);
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////            PermitDAO pmDAO = new PermitDAO();
////            Permit pm = pmDAO.findLastByFileIdAndType(fileId, Constants.PERMIT_TYPE.FILE_PAPER);
////
////            if (pm == null) {
////                Logger.getLogger(HelperID.class
////                        .getName()).log(Level.SEVERE, null, Constants.NSW_SERVICE.COMMENS.ERR_PERMIT);
////
////                return false;
////            }
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(25L), Constants.NSW_FUNCTION(16L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////            Body bd = new Body();
////            Content ct = new Content();
////            ImportDocument imDoc = new ImportDocument();
////            ProcessDocumentDAO pdDAO = new ProcessDocumentDAO();
////            ProcessDocument pd = pdDAO.getProcessDocumentByType(Constants.NSW_SERVICE.TYPE_CV, imFile.getFileId());
////            imDoc.setBillNo(imFile.getBillNo());
////            imDoc.setCheckPlace(pd.getCheckPlace());
////            imDoc.setCheckTime(pd.getCheckDate() == null ? "" : formatterDateTime.format(pd.getCheckDate()));
////            imDoc.setComingDate(imFile.getComingDate() == null ? "" : formatterDateTime.format(imFile.getComingDate()));
////            imDoc.setContractNo(imFile.getContractNo());
////            imDoc.setCreatedBy(files.getTaxCode());
////            imDoc.setCreatedDate(files.getCreateDate() == null ? "" : formatterDateTime.format(files.getCreateDate()));
////            imDoc.setCreaterName(files.getCreatorName());
////            imDoc.setCustomsCode(imFile.getCustomsBranchCode());
////            imDoc.setCustomsName(imFile.getCustomsBranchName());
////            imDoc.setDeptCode(imFile.getCheckDeptCode());
////            imDoc.setDeptName(imFile.getCheckDeptName());
////            imDoc.setExporterAddress(imFile.getExporterAddress());
////            imDoc.setExporterEmail(imFile.getExporterEmail());
////            imDoc.setExporterFax(imFile.getExporterFax());
////            imDoc.setExporterGateCode(imFile.getExporterGateCode());
////            imDoc.setExporterGateName(imFile.getExporterGateName());
////            imDoc.setExporterName(imFile.getExporterGateName());
////            imDoc.setExporterPhone(imFile.getExporterPhone());
////            imDoc.setFilesNo(pm.getReceiveNo());
////            imDoc.setGoodsOwnerAddress(files.getBusinessAddress());
////            imDoc.setGoodsOwnerEmail(files.getBusinessEmail());
////            imDoc.setGoodsOwnerFax(files.getBusinessFax());
////            imDoc.setGoodsOwnerName(files.getBusinessName());
////            imDoc.setGoodsOwnerPhone(files.getBusinessPhone());
////            imDoc.setImporterGateCode(imFile.getImporterGateCode());
////            imDoc.setImporterGateName(imFile.getImporterGateName());
////            imDoc.setModifiedDate(files.getModifyDate() == null ? "" : formatterDateTime.format(files.getModifyDate()));
////            imDoc.setNswFileCode(files.getNswFileCode());
////            imDoc.setResponsiblePersonAddress(imFile.getResponsiblePersonAddress());
////            imDoc.setResponsiblePersonEmail(imFile.getResponsiblePersonEmail());
////            imDoc.setResponsiblePersonFax(imFile.getResponsiblePersonFax());
////            imDoc.setResponsiblePersonName(imFile.getResponsiblePersonName());
////            imDoc.setResponsiblePersonPhone(imFile.getResponsiblePersonPhone());
////            imDoc.setTransNo(imFile.getTransNo());
////            //2 type kiểm tra thường và chặt
////            imDoc.setType(Constants.NSW_SERVICE.TYPE_THUONG);
////            List<Product> lsP = new ArrayList<>();
////            ImportOrderProductDAO imPDAO = new ImportOrderProductDAO();
////            List<ImportOrderProduct> lsImP = imPDAO.getImportOrderProductList(imFile.getFileId(), false);
////            AttachDAOHE attDAOHE = new AttachDAOHE();
////            if (lsImP != null && lsImP.size() > 0) {
////                for (ImportOrderProduct imP : lsImP) {
////                    Product p = new Product();
////                    List<Attachment> lsAttm = new ArrayList<>();
////                    p.setProductId(imP.getProductId().toString());
////                    p.setProductName(imP.getProductName());
////                    p.setProductCode(imP.getProductCode());
////                    p.setConfirmAnnounceNo(imP.getConfirmAnnounceNo());
////                    p.setCheckMethodCode(Constants.NSW_SERVICE.TYPE_GIAM_HT.equals(imP.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_GIAM : (Constants.NSW_SERVICE.TYPE_THUONG_HT.equals(imP.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_THUONG : Constants.NSW_SERVICE.TYPE_CHAT));
////                    p.setCheckMethodName(imP.getCheckMethodName());
////                    p.setBaseUnit(imP.getBaseUnit() == null ? "" : imP.getBaseUnit().toString());
////                    p.setReason(imP.getReason());
////                    p.setConfirmAnnounceNo(imP.getConfirmAnnounceNo());
////                    p.setManufacturer(imP.getManufacTurer());
////                    p.setManufacturerAddress(imP.getManufacturerAddress());
////                    p.setNationalCode(imP.getNationalCode());
////                    p.setNationalName(imP.getNationalName());
////                    p.setNetweight(imP.getNetweight() == null ? "" : imP.getNetweight().toString());
////                    p.setNetweightUnitCode(imP.getNetweightUnitCode());
////                    p.setNetweightUnitName(imP.getNetweightUnitName());
////                    p.setTotal(imP.getTotal() == null ? "" : imP.getTotal().toString());
////                    p.setTotalUnitCode(imP.getTotalUnitCode());
////                    p.setTotalUnitName(imP.getTotalUnitName());
////                    p.setPass(imP.getPass() == null ? "" : imP.getPass().toString());
////                    p.setReProcess(imP.getReason());
////                    // 3 File kiểm nghiệm
////                    List<Attachs> lsATTSP = attDAOHE.getByObjectId(imP.getProductId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_TEST);
////                    if (lsATTSP != null && lsATTSP.size() > 0) {
////                        for (Attachs at : lsATTSP) {
////                            Attachment a = new Attachment();
////                            // a.setAttachPath(at.getAttachPath());
////                            a.setAttachTypeCode(at.getAttachType() == null ? "" : at.getAttachType().toString());
////                            a.setAttachTypeName(at.getAttachTypeName());
////                            a.setAttachmentId(at.getAttachId().toString());
////                            a.setAttachmentCode(at.getAttachId().toString());
////                            a.setAttachmentName(at.getAttachName());
////                            a.setNswFileCode(files.getNswFileCode());
////                            lsAttm.add(a);
////                        }
////                        p.setAttachmentList(lsAttm);
////                        lsP.add(p);
////                    }
////                }
////            }
////            //File lanhx đạo ký
////            List<Attachment> lsAttachment = new ArrayList<>();
//////            PermitDAO peDAO = new PermitDAO();
//////            Permit pe = peDAO.findLastByFileIdAndType(imFile.getFileId(), Constants.PERMIT_TYPE.REDUCED_PAPER);
//////            if (pe == null) {
//////                return false;
//////            }
////            Attachs att = attDAOHE.getLastByObjectIdAndType(fileId, Constants.CATEGORY_TYPE.IMPORT_ORDER_FILE_PAPER);
////            if (att == null) {
////                return false;
////            }
////            imDoc.setProcessDate(att.getDateCreate() == null ? "" : formatterDateTime.format(att.getDateCreate()));
////            imDoc.setProcessName(att.getCreatorName());
////            Attachment a = new Attachment();
////            //  a.setAttachPath(att.getAttachPath());
////            a.setAttachTypeCode(att.getAttachType() == null ? "" : att.getAttachType().toString());
////            a.setAttachTypeName(att.getAttachTypeName());
////            a.setAttachmentId(att.getAttachId().toString());
////            a.setAttachmentCode(att.getAttachId().toString());
////            a.setAttachmentName(att.getAttachName());
////            a.setNswFileCode(files.getNswFileCode());
////            lsAttachment.add(a);
////            // File Hồ sơ kiểm tra
////            List<Attachs> lsATTKT = attDAOHE.getByObjectId(files.getFileId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_CHECK);
////            if (lsATTKT != null && lsATTKT.size() > 0) {
////                for (Attachs at : lsATTKT) {
////                    Attachment akt = new Attachment();
////                    //  akt.setAttachPath(at.getAttachPath());
////                    akt.setAttachTypeCode(at.getAttachType() == null ? "" : at.getAttachType().toString());
////                    akt.setAttachTypeName(at.getAttachTypeName());
////                    akt.setAttachmentId(at.getAttachId().toString());
////                    akt.setAttachmentCode(at.getAttachId().toString());
////                    akt.setAttachmentName(at.getAttachName());
////                    akt.setNswFileCode(files.getNswFileCode());
////                    lsAttachment.add(akt);
////                }
////            }
////            imDoc.setProductList(lsP);
////            imDoc.setAttachmentList(lsAttachment);
////            ct.setImportDocument(imDoc);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
//////Gửi thông báo chờ kết quả kiểm tra
////    public Boolean senMS_24(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
////        try {
////            ImportOrderFileDAO imDAO = new ImportOrderFileDAO();
////            ImportOrderFile imFile = imDAO.findByFileId(fileId);
////
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(29L), Constants.NSW_FUNCTION(24L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
////            Body bd = new Body();
////            Content ct = new Content();
////            ProcessDocumentDAO pdDAO = new ProcessDocumentDAO();
////            ProcessDocument pd = pdDAO.getProcessDocumentByType(Constants.NSW_SERVICE.TYPE_CV, imFile.getFileId());
////            FileResults frs = new FileResults();
////            frs.setNswFileCode(imFile.getNswFileCode());
////            frs.setDeptCode(imFile.getCheckDeptCode());
////            frs.setDeptName(imFile.getCheckDeptName());
////            frs.setComments(Constants.NSW_SERVICE.COMMENS.CHO_KQKT);
////            frs.setCheckDate(pd.getCheckDate() == null ? "" : formatterDateTime.format(pd.getCheckDate()));
////            frs.setCheckPlace(pd.getCheckPlace());
////            ct.setFileResults(frs);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
////
////    //Gửi kết quả kiểm tra các mặt hàng không đạt
////    public Boolean senMS_26(Long fileId, Long processId) throws IOException, FileNotFoundException, Base64DecodingException {
////        try {
////            FilesDAOHE fDAOHE = new FilesDAOHE();
////            Files files = fDAOHE.findById(fileId);
////
////            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(32L), Constants.NSW_FUNCTION(26L), files.getNswFileCode(), files.getNswFileCode(), processId.toString());
////            Body bd = new Body();
////            Content ct = new Content();
////            ImportOrderProductDAO imPDAO = new ImportOrderProductDAO();
////            AttachDAOHE attDAOHE = new AttachDAOHE();
////            ImportDocument imDoc = new ImportDocument();
////            imDoc.setNswFileCode(files.getNswFileCode());
////            imDoc.setCreatedDate(files.getCreateDate() == null ? "" : formatterDateTime.format(files.getCreateDate()));
////            imDoc.setModifiedDate(files.getModifyDate() == null ? "" : formatterDateTime.format(files.getModifyDate()));
////            imDoc.setCreaterName(files.getCreatorName());
////
////            List<Product> lsP = new ArrayList<>();
////            List<ImportOrderProduct> lsImProduct = imPDAO.findAllIdByFileIdAndPass(files.getFileId(), 0L);
////            if (lsImProduct != null && lsImProduct.size() > 0) {
////                for (ImportOrderProduct imp : lsImProduct) {
////                    Product p = new Product();
////                    p.setProductId(imp.getProductId().toString());
////                    p.setProductCode(imp.getProductCode());
////                    p.setProductName(imp.getProductName());
////                    p.setCheckMethodCode(Constants.NSW_SERVICE.TYPE_GIAM_HT.equals(imp.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_GIAM : (Constants.NSW_SERVICE.TYPE_THUONG_HT.equals(imp.getCheckMethodCode()) ? Constants.NSW_SERVICE.TYPE_THUONG : Constants.NSW_SERVICE.TYPE_CHAT));
////                    p.setCheckMethodName(imp.getCheckMethodName());
////                    p.setProcessType(imp.getProcessType() == null ? "" : imp.getProcessType().toString());
////                    p.setProcessTypeName(imp.getProcessTypeName());
////                    p.setReProcess(imp.getReProcess() == null ? "" : imp.getReProcess().toString());
////                    p.setComment(imp.getComments());
////                    p.setResult(imp.getResult());
////                    List<Attachs> lsAtts = attDAOHE.getAllByObjectIdAndType(imp.getProductId(), Constants.OBJECT_TYPE.IMPORT_ORDER_FILE_REPROCESS_RS);
////                    List<Attachment> lsAttachment = new ArrayList<>();
////                    if (lsAtts != null && lsAtts.size() > 0) {
////                        for (Attachs a : lsAtts) {
////                            Attachment att = new Attachment();
////                            att.setAttachmentId(a.getAttachId().toString());
////                            att.setAttachTypeCode(a.getAttachType() == null ? "" : a.getAttachType().toString());
////                            att.setAttachTypeName(a.getAttachTypeName());
////                            att.setAttachmentName(a.getAttachName());
////                            lsAttachment.add(att);
////                        }
////                        p.setAttachmentList(lsAttachment);
////                    }
////                    lsP.add(p);
////                }
////                imDoc.setProductList(lsP);
////            }
////            ct.setImportDocument(imDoc);
////            bd.setContent(ct);
////            evl.setBody(bd);
////            return sendMs(evl);
////
////        } catch (Exception ex) {
////            Logger.getLogger(HelperID.class
////                    .getName()).log(Level.SEVERE, null, ex);
////
////            return false;
////        }
////    }
//
//    // hieptq update 111115
//    public void onSave(int typeSave) throws Exception {
//        try {
//            createObjectCreatNew();
//        } catch (Exception e) {
//            LogUtils.addLogDB(e);
//        }
//    }
//
//    private ImportOrderFile importOrderfiles;
//
//    private static Files files;
//
//    private void createObjectCreatNew() throws Exception {
//        FilesDAOHE filesDAOHE = new FilesDAOHE();
//        files = createFile();
//        filesDAOHE.saveOrUpdate(files);
////        ImportOrderFileDAO importfileDAO = new ImportOrderFileDAO();
////        importOrderfiles = importOrderFileNew();
////        importfileDAO.saveOrUpdate(importOrderfiles);
//        // saveProduct(importOrderfiles.getFileId());
//    }
//
////      private void saveProduct(Long importFileId) {
////        ListModel<ImportOrderProduct> list = lbOrderProduct.getModel();
////        int size = list.getSize();
////        for (int i = 0; i < size; i++) {
////            ImportOrderProduct obj = list.getElementAt(i);
////            obj.setFileId(importFileId);
////            new ImportOrderProductDAO().saveOrUpdate(obj);
////        }
////    }
//    private Files createFile() throws Exception {
////        Date dateNow = new Date();
////        Files files = new Files();
////        files.setCreateDate(dateNow);
////        files.setModifyDate(dateNow);
////        files.setCreatorId(getUserId());
////        files.setCreatorName(getUserFullName());
////        files.setCreateDeptId(getDeptId());
////        files.setCreateDeptName(getDeptName());
////        files.setIsActive(Constants.Status.ACTIVE);
////        files.setBusinessName(lbOwnerName.getValue());
////        files.setBusinessAddress(lbOwnerAddress.getValue());
////        files.setBusinessEmail(tbOwnerEmail.getValue());
////        files.setBusinessPhone(lbOwnerPhone.getValue());
////        files.setBusinessFax(lbOwnerFax.getValue());
////        if (files.getStatus() == null) {
////            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
////        }
////
////        files.setFileType(FILE_TYPE);
//        return files;
//    }
//
////    private ImportOrderFile importOrderFileNew() {
////        importOrderfiles = new ImportOrderFile();
//////        if ("COPY".equals(crudMode)) {
//////            importOrderfiles.setFileId(null);
//////            files.setFileCode(null);
//////            importOrderfiles.setImportOrderFileId(null);
//////            if (files != null && originalFilesId != null) {
//////                AttachDAOHE daoHE = new AttachDAOHE();
//////                daoHE.copyAttachs(originalFilesId, files.getFileId(),Constants.OBJECT_TYPE.IMPORT_ORDER_FILE);
//////            }
//////        }
////        importOrderfiles.setFileId(files.getFileId());
////        if (documentTypeCode != null) {
////            importOrderfiles.setDocumentTypeCode(documentTypeCode);
////            nswFileCode = getAutoNswFileCode(documentTypeCode);
////            if (files.getFileCode() == null) {
////                files.setFileCode(nswFileCode);
////            }
////
////            importOrderfiles.setResponsiblePersonName(tbPersonName.getText());
////            importOrderfiles.setResponsiblePersonAddress(tbPersonAddress.getText());
////            importOrderfiles.setResponsiblePersonPhone(tbPersonPhone.getText());
////            importOrderfiles.setResponsiblePersonFax(tbPersonFax.getText());
////            importOrderfiles.setResponsiblePersonEmail(tbPersonEmail.getText());
////            importOrderfiles.setExporterName(tbExporterName.getText());
////            importOrderfiles.setExporterAddress(tbExporterAddress.getText());
////            importOrderfiles.setExporterFax(tbExporterFax.getText());
////            importOrderfiles.setExporterPhone(tbExportPhone.getText());
////            importOrderfiles.setExporterEmail(tbExporterEmail.getText());
////            importOrderfiles.setComingDate(dbComingDate.getValue());
////            importOrderfiles.setTransNo(tbTransNo.getText());
////            importOrderfiles.setContractNo(tbContractNo.getText());
////            importOrderfiles.setBillNo(tbBillNo.getText());
////            importOrderfiles.setCheckPlace(tbCheckPlace.getText());
////            importOrderfiles.setCheckTime(dbCheckDate.getValue());
////            importOrderfiles.setCustomsBranchName(tbBranchName.getText());
////            importOrderfiles.setImporterGateName(tbImportGate.getText());
////            importOrderfiles.setExporterGateName(tbExportGate.getText());
////            importOrderfiles.setDeptName(tbDepartment.getText());
////        }
////        return importOrderfiles;
////
////    }
//    
//     public com.viettel.ws.BO_IO.Envelope makeEnvelopeSend(String type, String function, String ref, String pref, String msId) {
//        com.viettel.ws.BO_IO.Envelope ev = new com.viettel.ws.BO_IO.Envelope();
//        com.viettel.ws.BO_IO.Header hd = new com.viettel.ws.BO_IO.Header();
////        hd.setReference(new com.viettel.ws.BO_IO.Reference("1.0", msId));
////        hd.setFrom(new com.viettel.ws.BO_IO.From("Cục An toàn thực phẩm - Bộ Y tế", "ATTP"));
////        hd.setTo(new com.viettel.ws.BO_IO.From("Hệ thống NSW", "NSW"));
////        Date d = new Date();
////        hd.setSubject(new com.viettel.ws.BO_IO.Subject(type, function, ref, pref, formatterYear.format(d), formatterDateTime.format(d)));
//        ev.setHeader(hd);
//        return ev;
//    }
//}
