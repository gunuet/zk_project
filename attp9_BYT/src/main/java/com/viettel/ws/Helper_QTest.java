/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws;

import com.google.gson.Gson;
import com.viettel.core.sys.BO.Category;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.DepartmentDAOHE;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.EvaluationRecord;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.evaluation.DAO.EvaluationRecordDAO;
import com.viettel.module.evaluation.Model.EvaluationModel;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.module.rapidtest.BO.RtAssay;
import com.viettel.module.rapidtest.BO.RtFile;
import com.viettel.module.rapidtest.BO.RtTargetTesting;
import com.viettel.module.rapidtest.DAO.RtTargetTestingDAO;
import com.viettel.module.rapidtest.DAOHE.RapidTestDAOHE;
import com.viettel.utils.Constants;
import com.viettel.utils.EncryptDecryptUtils;
import com.viettel.utils.FileUtil;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.voffice.BO.Business;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BusinessDAOHE;
import com.viettel.ws.BO.*;
import java.io.File;
import java.io.FileNotFoundException;
import com.viettel.core.user.BO.Department;
import com.viettel.core.user.model.UserToken;
import com.viettel.utils.LogUtils;
import java.io.IOException;
import com.viettel.voffice.BO.VFeeProcedure;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.commons.codec.binary.Base64;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.voffice.DAOHE.VFeeProcedureDAOHE;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author E5420
 */
public class Helper_QTest {

    private final Helper helper = new Helper();
    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    ImportedFoodService imService;
    Helper hp = new Helper();

    //Nhận hs, hsSDBS từ NSW
    private Files saveFileFromRtFile(CirculatingRapidTest rtFile, Files files) throws ParseException {
        if (files == null || files.getFileId() == null) {
            files = new Files();
        }
        try {
            files.setBusinessName(rtFile.getBusinessName());
            files.setBusinessAddress(rtFile.getBusinessAddress());
            files.setBusinessPhone(rtFile.getBusinessPhone());
            files.setBusinessFax(rtFile.getBusinessFax());
            files.setBusinessEmail(rtFile.getBusinessEmail());
            files.setCreateDate(new java.util.Date());
            files.setModifyDate(new java.util.Date());
            files.setCreatorName(rtFile.getSignName());
            files.setFileCode(rtFile.getNSWFileCode());
            files.setNswFileCode(rtFile.getNSWFileCode());
            files.setTaxCode(rtFile.getTaxCode());
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
            files.setIsActive(0L);
            files.setIsTemp(0L);
            WorkflowAPI w = new WorkflowAPI();
            Category ftype = w.getProcedureTypeByCode(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
            if (ftype != null) {
                files.setFileType(ftype.getCategoryId());
                files.setFileTypeName(ftype.getName());
            }
            // Save user from NSW
            UserDAOHE usDAOHE = new UserDAOHE();
            Users u = usDAOHE.getUserByName(files.getTaxCode());
            if (u == null) {
                Business b = new Business();
                u = new Users();
                BusinessDAOHE bDAOHE = new BusinessDAOHE();
                b.setBusinessAddress(files.getBusinessAddress());
                b.setBusinessFax(files.getBusinessFax());
                b.setBusinessName(files.getBusinessName());
                b.setBusinessTaxCode(files.getTaxCode());
                b.setBusinessTelephone(files.getBusinessPhone());
                b.setIsActive(1L);
                b.setUserName(files.getBusinessFax());
                bDAOHE.saveOrUpdate(b);
                u.setBusinessName(files.getBusinessName());
                u.setUserName(files.getTaxCode());
                u.setUserType(Constants.USER_TYPE.ENTERPRISE_USER);
                u.setPassword(EncryptDecryptUtils.encrypt(u.getUserName() + "!@@"));
                u.setStatus(1L);
                u.setBusinessId(b.getBusinessId());
                usDAOHE.saveOrUpdate(u);
            }
            files.setCreatorId(u.getUserId());
            files.setBusinessId(u.getBusinessId());
            FilesDAOHE fDAOHE = new FilesDAOHE();
            fDAOHE.saveOrUpdate(files);
            return files;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    private Files saveFileFromRtFile(CirculatingRapidTestChange rtFile, Files files) throws ParseException {
        if (files == null || files.getFileId() == null) {
            files = new Files();
        }
        try {
            files.setBusinessName(rtFile.getBusinessName());
            files.setBusinessAddress(rtFile.getBusinessAddress());
            files.setBusinessPhone(rtFile.getBusinessPhone());
            files.setBusinessFax(rtFile.getBusinessFax());
            files.setBusinessEmail(rtFile.getBusinessEmail());
            files.setCreateDate(new java.util.Date());
            files.setModifyDate(new java.util.Date());
            files.setCreatorName(rtFile.getSignName());
            files.setFileCode(rtFile.getNSWFileCode());
            files.setNswFileCode(rtFile.getNSWFileCode());
            files.setTaxCode(rtFile.getTaxCode());
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
            files.setIsActive(0L);
            files.setIsTemp(0L);
            WorkflowAPI w = new WorkflowAPI();
            Category ftype = w.getProcedureTypeByCode(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
            if (ftype != null) {
                files.setFileType(ftype.getCategoryId());
                files.setFileTypeName(ftype.getName());
            }
            // Save user from NSW
            UserDAOHE usDAOHE = new UserDAOHE();
            Users u = usDAOHE.getUserByName(files.getTaxCode());
            if (u == null) {
                u = new Users();
                Business b = new Business();
                BusinessDAOHE bDAOHE = new BusinessDAOHE();
                b.setBusinessAddress(files.getBusinessAddress());
                b.setBusinessFax(files.getBusinessFax());
                b.setBusinessName(files.getBusinessName());
                b.setBusinessTaxCode(files.getTaxCode());
                b.setBusinessTelephone(files.getBusinessPhone());
                b.setIsActive(1L);
                b.setUserName(files.getBusinessFax());
                bDAOHE.saveOrUpdate(b);
                u.setBusinessName(files.getBusinessName());
                u.setUserName(files.getTaxCode());
                u.setUserType(Constants.USER_TYPE.ENTERPRISE_USER);
                u.setBusinessId(b.getBusinessId());
                u.setPassword(EncryptDecryptUtils.encrypt(u.getUserName() + "!@@"));
                u.setStatus(1L);
                usDAOHE.saveOrUpdate(u);
            }
            files.setCreatorId(u.getUserId());
            files.setBusinessId(u.getBusinessId());
            FilesDAOHE fDAOHE = new FilesDAOHE();
            fDAOHE.saveOrUpdate(files);
            return files;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    private Files saveFileFromRtFile(CirculatingRapidTestExtension rtFile, Files files) throws ParseException {
        if (files == null || files.getFileId() == null) {
            files = new Files();
        }
        try {
            files.setBusinessName(rtFile.getBusinessName());
            files.setBusinessAddress(rtFile.getBusinessAddress());
            files.setBusinessPhone(rtFile.getBusinessPhone());
            files.setBusinessFax(rtFile.getBusinessFax());
            files.setBusinessEmail(rtFile.getBusinessEmail());
            files.setCreateDate(new java.util.Date());
            files.setModifyDate(new java.util.Date());
            files.setCreatorName(rtFile.getSignName());
            files.setFileCode(rtFile.getNSWFileCode());
            files.setNswFileCode(rtFile.getNSWFileCode());
            files.setTaxCode(rtFile.getTaxCode());
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
            files.setIsActive(0L);
            files.setIsTemp(0L);
            WorkflowAPI w = new WorkflowAPI();
            Category ftype = w.getProcedureTypeByCode(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
            if (ftype != null) {
                files.setFileType(ftype.getCategoryId());
                files.setFileTypeName(ftype.getName());
            }
            // Save user from NSW
            UserDAOHE usDAOHE = new UserDAOHE();
            Users u = usDAOHE.getUserByName(files.getTaxCode());
            if (u == null) {
                u = new Users();
                Business b = new Business();
                BusinessDAOHE bDAOHE = new BusinessDAOHE();
                b.setBusinessAddress(files.getBusinessAddress());
                b.setBusinessFax(files.getBusinessFax());
                b.setBusinessName(files.getBusinessName());
                b.setBusinessTaxCode(files.getTaxCode());
                b.setBusinessTelephone(files.getBusinessPhone());
                b.setIsActive(1L);
                b.setUserName(files.getBusinessFax());
                bDAOHE.saveOrUpdate(b);
                u.setBusinessName(files.getBusinessName());
                u.setUserName(files.getTaxCode());
                u.setUserType(Constants.USER_TYPE.ENTERPRISE_USER);
                u.setBusinessId(b.getBusinessId());
                u.setPassword(EncryptDecryptUtils.encrypt(u.getUserName() + "!@@"));
                u.setStatus(1L);
                usDAOHE.saveOrUpdate(u);
            }
            files.setCreatorId(u.getUserId());
            files.setBusinessId(u.getBusinessId());
            FilesDAOHE fDAOHE = new FilesDAOHE();
            fDAOHE.saveOrUpdate(files);
            return files;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    private List<com.viettel.ws.BO.Error> saveRtFileFromRtFile(CirculatingRapidTest rtFile, Long filesId) throws ParseException {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
        RtFile rt = new RtFile();
        try {
            rt.setNswFileCode(rtFile.getNSWFileCode());
            rt.setRapidTestName(rtFile.getRapidTestName());
            rt.setRapidTestNo(rtFile.getRapidTestNo());
            rt.setPlaceOfManufacture(rtFile.getPlaceOfManufacture());
            rt.setManufacture(rtFile.getManufacture());
            rt.setAddressOfManufacture(rtFile.getAddressOfManufacture());
            helper.saveOrUpdatePlace(rtFile.getStateOfManufacture(), rtFile.getNameOfState(), Constants.NSW_SERVICE.PLACE_NATIONAL);
            rt.setStateOfManufacture(rtFile.getStateOfManufacture());
            rt.setNameOfState(rtFile.getNameOfState());
            rt.setPropertiesTests(rtFile.getPropertiesTests() == null ? null : Long.parseLong(rtFile.getPropertiesTests()));
            rt.setOperatingPrinciples(rtFile.getOperatingPrinciples());
            rt.setDescription(rtFile.getDescription());
            rt.setPakaging(rtFile.getPackaging());
            String value = rtFile.getShelfLife();
            Long vulueL = 0L;
            try {
                vulueL = Long.valueOf(value);
            } catch (Exception ex) {
                LogUtils.addLog(ex);
            }

            rt.setShelfLifeMonth(vulueL);
            rt.setShelfLife(rtFile.getShelfLife());
            rt.setStorageConditions(rtFile.getStorageConditions());
            rt.setOtherInfos(rtFile.getOtherInfos());
            rt.setSignDate(!StringUtils.validString(rtFile.getSignDate()) ? null : formatterDateTime.parse(rtFile.getSignDate()));
            rt.setSignName(rtFile.getSignName());
            helper.saveOrUpdatePlace(rtFile.getSignPlace(), rtFile.getSignPlaceName(), Constants.NSW_SERVICE.PLACE_PROVINCE);
            rt.setSignPlace(rtFile.getSignPlace());
            rt.setSignPlaceName(rtFile.getSignPlaceName());
            rt.setDocumentTypeCode(Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_TAOMOI);
            rt.setRapidTestCode(rtFile.getRapidTestCode());
            rt.setFileId(filesId);
            rtDAOHE.saveOrUpdate(rt);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode("L000-0000-0000");
            err.setErrorName("Lỗi thêm mới/cập nhật hồ sơ xét nghiệm nhanh(rtFile): " + rtFile.getNSWFileCode());
            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    private List<com.viettel.ws.BO.Error> saveRtFileFromRtFile(CirculatingRapidTestChange rtFile, Long filesId) {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
        RtFile rt = new RtFile();
        try {
            rt.setNswFileCode(rtFile.getNSWFileCode());
            rt.setRapidTestName(rtFile.getRapidTestName());
            rt.setRapidTestChangeNo(rtFile.getRapidTestChangeNo());
            rt.setCirculatingRapidTestNo(rtFile.getCirculatingRapidTestNo());
            rt.setDateIssue(!StringUtils.validString(rtFile.getDateIssue()) ? null : formatterDateTime.parse(rtFile.getDateIssue()));

            rt.setContents(rtFile.getContents());
            rt.setSignDate(!StringUtils.validString(rtFile.getSignDate()) ? null : formatterDateTime.parse(rtFile.getSignDate()));
            rt.setSignName(rtFile.getSignName());
            helper.saveOrUpdatePlace(rtFile.getSignPlace(), rtFile.getSignPlaceName(), Constants.NSW_SERVICE.PLACE_PROVINCE);
            rt.setSignPlace(rtFile.getSignPlace());
            rt.setSignPlaceName(rtFile.getSignPlaceName());
            rt.setDocumentTypeCode(Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_BOSUNG);
            rt.setRapidTestCode(rtFile.getRapidTestCode());
            rt.setFileId(filesId);
            rtDAOHE.saveOrUpdate(rt);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode("L000-0000-0000");
            err.setErrorName("Lỗi thêm mới/cập nhật hồ sơ xét nghiệm nhanh(rtFile): " + rtFile.getNSWFileCode());
            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    private List<com.viettel.ws.BO.Error> saveRtFileFromRtFile(CirculatingRapidTestExtension rtFile, Long filesId) {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
        RtFile rt = new RtFile();
        try {
            rt.setNswFileCode(rtFile.getNSWFileCode());
            rt.setRapidTestName(rtFile.getRapidTestName());
            rt.setRapidTestNo(rtFile.getRapidTestExtensionNo());
            rt.setCirculatingRapidTestNo(rtFile.getCirculatingRapidTestNo());
            rt.setDateIssue(!StringUtils.validString(rtFile.getDateIssue()) ? null : formatterDateTime.parse(rtFile.getDateIssue()));
            rt.setDateEffect(!StringUtils.validString(rtFile.getDateEffect()) ? null : formatterDateTime.parse(rtFile.getDateEffect()));
            rt.setExtensionNo(rtFile.getExtensionNo() == null ? null : Long.parseLong(rtFile.getExtensionNo()));
            rt.setAttachmentsInfo(rtFile.getAttachmentsInfo());
            rt.setSignDate(!StringUtils.validString(rtFile.getSignDate()) ? null : formatterDateTime.parse(rtFile.getSignDate()));
            rt.setSignName(rtFile.getSignName());
            helper.saveOrUpdatePlace(rtFile.getSignPlace(), rtFile.getSignPlaceName(), Constants.NSW_SERVICE.PLACE_PROVINCE);
            rt.setSignPlace(rtFile.getSignPlace());
            rt.setSignPlaceName(rtFile.getSignPlaceName());
            rt.setDocumentTypeCode(Constants.RAPID_TEST.DOCUMENT_TYPE_CODE_GIAHAN);
            rt.setFileId(filesId);
            rtDAOHE.saveOrUpdate(rt);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode("L000-0000-0000");
            err.setErrorName("Lỗi thêm mới/cập nhật hồ sơ xét nghiệm nhanh(rtFile): " + rtFile.getNSWFileCode());
            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    private List<com.viettel.ws.BO.Error> saveRtTargetTesTing(List<TargetTesting> lsTarget, Long fileId) {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        RtTargetTestingDAO rtttDAO = new RtTargetTestingDAO();
        try {
            if (lsTarget != null && lsTarget.size() > 0) {
                for (TargetTesting target : lsTarget) {
                    RtTargetTesting rttTarget = new RtTargetTesting();
                    rttTarget.setFileId(fileId);
                    rttTarget.setIsActive(1L);
                    rttTarget.setLimitDevelopment(target.getLimitDevelopment());
                    rttTarget.setName(target.getTargetTestName());
                    rttTarget.setPrecision(target.getPrecision());
                    rttTarget.setRangeOfApplications(target.getRangeOfApplication());
                    rtttDAO.saveOrUpdateNotCommit(rttTarget);
                }
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode("L000-0000-0000");
            err.setErrorName("Lỗi thêm mới/cập nhật chỉ tiêu xét nghiệm(RtTargetTesting): ");
            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    public List<com.viettel.ws.BO.Error> receiveMs01_02(Envelope ev, Files f, String type, Boolean isUpdate) throws FileNotFoundException, Base64DecodingException {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        RtFile rtf = new RtFile();
        FilesDAOHE fDAOHE = new FilesDAOHE();
        String nswFileCode = "";
        RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
        AttachDAOHE attDH = new AttachDAOHE();
        RtTargetTestingDAO rtTargetDAO = new RtTargetTestingDAO();
        try {
            if (Constants.NSW_TYPE(50L).equals(type)) {
                CirculatingRapidTest rtFile = ev.getBody().getContent().getCirculatingRapidTest();
                nswFileCode = rtFile.getNSWFileCode();
                if (isUpdate) {
                    Files f2 = helper.copyFilesToFiles(f, null);
                    fDAOHE.saveOrUpdate(f2);
                    rtf = rtDAOHE.findByFileId(f.getFileId());
                    attDH.setIsTempAttach(f2.getFileId(), f.getFileId());
                    rtTargetDAO.setIsTempTargetTesting(f2.getFileId(), f.getFileId());
                    rtf.setFileId(f2.getFileId());
                    rtDAOHE.saveOrUpdate(rtf);
                }
                f = saveFileFromRtFile(rtFile, f);
                if (f == null) {
                    err.setErrorCode("L000-0000-0000");
                    err.setErrorName("Lỗi thêm mới/cập nhật hồ sơ xét nghiệm nhanh(Files): " + nswFileCode);
                    err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
                    errList.add(err);
                    return errList;
                }
                errList.addAll(saveRtFileFromRtFile(rtFile, f.getFileId()));
                errList.addAll(saveRtTargetTesTing(rtFile.getListOfTargetTesting(), f.getFileId()));
                errList.addAll(saveFileAttFromNSW(rtFile.getAttachments(), f.getFileId(), f.getNswFileCode()));
            } else if (Constants.NSW_TYPE(52L).equals(type)) {
                CirculatingRapidTestExtension rtFile = ev.getBody().getContent().getCirculatingRapidTestExtension();
                nswFileCode = rtFile.getNSWFileCode();
                if (isUpdate) {
                    Files f2 = helper.copyFilesToFiles(f, null);
                    fDAOHE.saveOrUpdate(f2);
                    rtf = rtDAOHE.findByFileId(f.getFileId());
                    attDH.setIsTempAttach(f2.getFileId(), f.getFileId());
                    rtTargetDAO.setIsTempTargetTesting(f2.getFileId(), f.getFileId());
                    rtf.setFileId(f2.getFileId());
                    rtDAOHE.saveOrUpdate(rtf);
                }
                f = saveFileFromRtFile(rtFile, f);
                if (f == null) {
                    err.setErrorCode("L000-0000-0000");
                    err.setErrorName("Lỗi thêm mới/cập nhật hồ sơ xét nghiệm nhanh(Files): " + nswFileCode);
                    err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
                    errList.add(err);
                    return errList;
                }
                errList.addAll(saveRtFileFromRtFile(rtFile, f.getFileId()));
                errList.addAll(saveFileAttFromNSW(rtFile.getAttachments(), f.getFileId(), f.getNswFileCode()));
                if (!isUpdate && errList.size() <= 0) {

                }
            } else if (Constants.NSW_TYPE(51L).equals(type)) {
                CirculatingRapidTestChange rtFile = ev.getBody().getContent().getCirculatingRapidTestChange();
                nswFileCode = rtFile.getNSWFileCode();
                if (isUpdate) {
                    Files f2 = helper.copyFilesToFiles(f, null);
                    fDAOHE.saveOrUpdate(f2);
                    rtf = rtDAOHE.findByFileId(f.getFileId());
                    attDH.setIsTempAttach(f2.getFileId(), f.getFileId());
                    rtTargetDAO.setIsTempTargetTesting(f2.getFileId(), f.getFileId());
                    rtf.setFileId(f2.getFileId());
                    rtDAOHE.saveOrUpdate(rtf);
                }
                f = saveFileFromRtFile(rtFile, f);
                if (f == null) {
                    err.setErrorCode("L000-0000-0000");
                    err.setErrorName("Lỗi thêm mới/cập nhật hồ sơ xét nghiệm nhanh(Files): " + nswFileCode);
                    err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
                    errList.add(err);
                    return errList;
                }
                errList.addAll(saveRtFileFromRtFile(rtFile, f.getFileId()));
                errList.addAll(saveFileAttFromNSW(rtFile.getAttachments(), f.getFileId(), f.getNswFileCode()));
            }
            if (!isUpdate && addFee(f.getFileId()) == false) {
                err.setErrorCode("L000-0000-0000");
                err.setErrorName("Lỗi thêm mới bản ghi phí cho hồ sơ: " + nswFileCode);
                err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
                errList.add(err);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode("L000-0000-0000");
            err.setErrorName("Lỗi thêm mới/cập nhật hồ sơ xét nghiệm nhanh(rtFile): " + nswFileCode);
            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    private Boolean addFee(Long fileId) {
        try {
            VFeeProcedureDAOHE vFeeFileDAOHE = new VFeeProcedureDAOHE();
            DepartmentDAOHE dpDAOHE = new DepartmentDAOHE();
            Department dpm = dpDAOHE.findById(Constants.CUC_ATTP_XNN_ID);
            List<VFeeProcedure> lstVFeeProcedure = vFeeFileDAOHE
                    .getListFee(Constants.CATEGORY_TYPE.RAPID_TEST_OBJECT);
            PaymentInfoDAO feePaymentInfoDAOHE = new PaymentInfoDAO();
            List lst = feePaymentInfoDAOHE.getListPayment(fileId);
            if (lst.isEmpty()) {
                // Neu chua co ban ghi thanh toan thi luu lai
                for (VFeeProcedure obj : lstVFeeProcedure) {

                    PaymentInfo feePaymentInfo = new PaymentInfo();
                    feePaymentInfo.setFileId(fileId);
                    feePaymentInfo.setFeeId(obj.getFeeId());
                    feePaymentInfo.setFeeName(obj.getName());
                    feePaymentInfo
                            .setIsActive(Constants.Status.ACTIVE);
                    feePaymentInfo
                            .setStatus(Constants.RAPID_TEST.PAYMENT.PAY_NEW);
                    feePaymentInfo.setPhase(obj.getPhase());
                    feePaymentInfo.setBankNo(dpm.getBankNo());
                    String value = obj.getCost();
                    Long valueL = 0L;
                    try {
                        valueL = Long.valueOf(value);
                    } catch (Exception e) {
                        LogUtils.addLog(e);
                    }
                    feePaymentInfo.setCost(valueL);
                    feePaymentInfoDAOHE
                            .saveOrUpdate(feePaymentInfo);
                }
            }
            return true;
        } catch (Exception ex) {
            Logger.getLogger(Helper_QTest.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private List<com.viettel.ws.BO.Error> saveFileAttFromNSW(List<Attachment> listatt, Long fileId, String nswFileCode) throws FileNotFoundException, IOException, Base64DecodingException {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        if (listatt != null && listatt.size() > 0) {
            for (Attachment att : listatt) {
                //Lay duong dan tuyet doi cua thu muc /Share/img (nam trong folder target)
                try {
                    Boolean k = saveAttachmentFromNSW(att, fileId, Constants.OBJECT_TYPE.RAPID_TEST_HO_SO_GOC, nswFileCode);
                    if (!k) {
                        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
                        err.setErrorCode("L000-0000-0000");
                        err.setErrorName("Lỗi thêm mới file đính kèm: " + att.getAttachPath());
                        err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
                        errList.add(err);
                    }
                } catch (Exception ex) {
                    LogUtils.addLogDB(ex);
                    com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
                    err.setErrorCode("L000-0000-0000");
                    err.setErrorName("Lỗi thêm mới file đính kèm: " + att.getAttachPath());
                    err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
                    errList.add(err);
                }
            }
        }
        return errList;
    }

    public Content receiveMs_32(Envelope envelop, Files files) throws ParseException, IOException {
        Content ct = new Content();
        try {
            RequestAttachment att = envelop.getBody().getContent().getRequestAttachment();
            AttachDAOHE aDH = new AttachDAOHE();
            // Attachs atts = aDH.getByObjectIdAndCode(att.getAttachTypeCode(), files.getFileId());
            Attachs atts = aDH.findById(att.getAttachmentCode() == null ? null : Long.parseLong(att.getAttachmentCode()));
            ResponseAttachment rsatt = new ResponseAttachment();
            Path path = Paths.get(atts.getFullPathFile());
            byte[] data = java.nio.file.Files.readAllBytes(path);
            rsatt.setFileByte(Base64.encodeBase64String(data));
            ct.setResponseAttachment(rsatt);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
            com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
            err.setErrorCode("U000-0000-0000");
            err.setErrorName("Lỗi tải file đính kèm");
            err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
            errList.add(err);
            ct.setErrorList(errList);
        }
        return ct;
    }

    private Boolean saveAttachmentFromNSW(Attachment att, Long objectId, Long attCat, String nswFileCode) throws FileNotFoundException, IOException, Base64DecodingException {
        try {
            if (att.getAttachMohId() != null && !"0".equals(att.getAttachMohId())) {
                Long attachId = Long.valueOf(att.getAttachMohId());
                AttachDAOHE attachDAOHE = new AttachDAOHE();
                Attachs attach = attachDAOHE.findByAttachId(attachId);
                attach.setObjectId(objectId);
                attach.setAttachCat(attCat);
                attach.setAttachType(!StringUtils.validString(att.getAttachmentTypeCode()) ? null : Long.parseLong(att.getAttachmentTypeCode()));
                attach.setAttachTypeName(att.getAttachmentTypeName());
                attachDAOHE.saveOrUpdate(attach);
                return true;
            } else {
                AttachDAOHE attachDAOHE = new AttachDAOHE();
                Attachs attach = new Attachs();
                String folderPath = ResourceBundleUtil.getString("dir_upload");
                FileUtil.mkdirs(folderPath);
                String separator = ResourceBundleUtil.getString("separator");
                if (!"/".equals(separator) && !"\\".equals(separator)) {
                    separator = "/";
                }
                String fileName = att.getAttachmentName();
                fileName = FileUtil.getSafeFileNameAll(fileName);

                attach.setAttachName(fileName);
                attach.setAttachCode(att.getAttachmentId());
                attach.setAttachTypeName(att.getAttachmentTypeName());
                attach.setIsActive(Constants.Status.ACTIVE);
                attach.setObjectId(objectId);
                attach.setAttachCat(attCat);
                attach.setAttachType(att.getAttachmentTypeCode() == null ? null : Long.parseLong(att.getAttachmentTypeCode()));
                attachDAOHE.saveOrUpdate(attach);
                folderPath += separator + attCat + separator + attach.getAttachId();
                attach.setAttachPath(folderPath);
                attachDAOHE.saveOrUpdate(attach);
                File f = new File(attach.getFullPathFile());
                if (!f.exists()) {
                    // tao folder
                    if (!f.getParentFile().exists()) {
                        f.getParentFile().mkdirs();
                    }
                    f.createNewFile();
                }
                //save to hard disk and database
                Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(62L), Constants.NSW_FUNCTION(32L), nswFileCode, nswFileCode, attach.getAttachId().toString());
                Body bd = new Body();
                Content ct = new Content();
                RequestAttachment attSend = new RequestAttachment();
                attSend.setAttachmentCode(att.getAttachmentCode());
                ct.setRequestAttachment(attSend);
                bd.setContent(ct);
                evl.setBody(bd);
                return helper.sendMs(evl);
            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    // Nhận kết quả nộp phí
    public List<com.viettel.ws.BO.Error> receiveMs_19(Envelope ev, Files files) {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        try {
            InfoFees iF = ev.getBody().getContent().getInfoFees();
            PaymentInfoDAO pDAO = new PaymentInfoDAO();
            Bill b = new Bill();
            BillDAO bDAO = new BillDAO();
            b.setCreatorId(files.getCreatorId());
            b.setTotal(iF.getFeedsValue() == null ? null : new Double(iF.getFeedsValue()).longValue());
            b.setCreatetorName(files.getCreatorName());
            b.setCreateDate(new java.util.Date());
            b.setPaymentDate(new java.util.Date());
            b.setStatus(Constants.NSW_SERVICE.FEE_STATUS.MOI_NOP);
            bDAO.saveOrUpdate(b);
            Attachment atm = iF.getAttachment();
            if (atm != null) {
                Boolean k1 = saveAttachmentFromNSW(atm, b.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY, files.getNswFileCode());
                if (!k1) {
                    com.viettel.ws.BO.Error err1 = new com.viettel.ws.BO.Error();
                    err1.setErrorCode("L000-0000-0000");
                    err1.setErrorName("Lỗi thêm mới file đính kèm: " + atm.getAttachmentCode());
                    err1.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
                    errList.add(err1);
                }
            }
            List<Attachment> lsa = iF.getAttachments();
            if (lsa != null && lsa.size() > 0) {
                for (Attachment att : lsa) {
                    Boolean k = saveAttachmentFromNSW(att, files.getFileId(), Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE, files.getNswFileCode());
                    if (!k) {
                        com.viettel.ws.BO.Error err1 = new com.viettel.ws.BO.Error();
                        err1.setErrorCode("L000-0000-0000");
                        err1.setErrorName("Lỗi thêm mới file đính kèm: " + att.getAttachmentCode());
                        err1.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
                        errList.add(err1);
                    }
                }

            }
            List<PaymentInfo> lsp = pDAO.getListPaymentByStatus(files.getFileId(), Constants.NSW_SERVICE.FEE_STATUS.MOI_TAO);
            if (lsp != null && lsp.size() > 0) {
                for (PaymentInfo pi : lsp) {
                    if (iF.getFeedsValue().equals(pi.getCost().toString())) {
                        pi.setStatus(Constants.NSW_SERVICE.FEE_STATUS.MOI_NOP);
                        pi.setBillId(b.getBillId());
                        pDAO.saveOrUpdate(pi);
                    }
                }
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode("L000-0000-0000");
            err.setErrorName("Lỗi Tiếp nhận thông tin nộp phí");
            err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    public void saveOrUpdateFiles(String nsWCode, Long docType, Boolean pass) {
        try {
            FilesDAOHE fdh = new FilesDAOHE();
            Files files = fdh.findByNswCode(nsWCode, docType, 0L, 0L);
            if (files != null) {
                Long fileId = files.getFileId();
                if (pass) {
                    files.setIsActive(1L);
                    fdh.SaveAndCommit(files);
                } else {
                    RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
                    RtTargetTestingDAO rtrrDAO = new RtTargetTestingDAO();
                    AttachDAOHE attDH = new AttachDAOHE();
                    Files fileTemp = fdh.findByNswCode(nsWCode, docType, 1L, 1L);

                    //rollback files cũ
                    files = helper.copyFilesToFiles(fileTemp, files);
                    files.setIsTemp(0L);
                    fileTemp.setIsActive(0L);
                    //dell file mới
                    RtFile rtfile = rtDAOHE.findByFileId(fileId);
                    if (rtfile != null) {
                        rtDAOHE.delete(rtfile);
                    }
                    //rollback files cũ
                    RtFile rtfileold = rtDAOHE.findByFileId(fileTemp.getFileId());
                    rtfileold.setFileId(fileId);
                    rtDAOHE.saveOrUpdate(rtfileold);
                    //dell tgt mới
                    rtrrDAO.deleteByFileId(fileId);
                    //rollback tgt cũ
                    rtrrDAO.setIsTempTargetTesting(files.getFileId(), fileTemp.getFileId());
                    //dell att mới
                    attDH.deleteByFileId(fileId, Constants.OBJECT_TYPE.RAPID_TEST_FILE_TYPE);
                    //rollback att cũ
                    attDH.setIsTempAttach(files.getFileId(), fileTemp.getFileId());
                    fdh.saveOrUpdate(files);
                    fdh.SaveAndCommit(fileTemp);
                }
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    //Gửi ms to NSW
    public Boolean sendMs(Envelope envelop) throws FileNotFoundException, IOException, Base64DecodingException {
        return helper.sendMs(envelop);
    }

    private Long getUserId() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getUserId();
    }

    //Gửi hẹn trả kết quả
    public Boolean sendMs_03(Long fileId, Long processId) throws FileNotFoundException, Base64DecodingException, IOException {
        try {
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);

            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(53L), Constants.NSW_FUNCTION(3L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), processId.toString());
            EvaluationRecordDAO ercDAO = new EvaluationRecordDAO();
            EvaluationRecord erc = ercDAO.findLastActiveByFileId(rtfile.getFileId(), getUserId(), Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.HENTRAKQ);
            Body bd = new Body();
            Content ct = new Content();
            Gson gson = new Gson();
            EvaluationModel model = gson.fromJson(erc.getFormContent(), EvaluationModel.class);
            PromissoryNote prnote = new PromissoryNote();
//            if (erc != null) {
            prnote.setDivision(model.getDeptName());
            prnote.setName(model.getUserName());
            prnote.setOrganization(Constants.NSW_SERVICE.DEPT_NAME);
            prnote.setPromissoryContent(model.getContent() == null ? " " : model.getContent());
            prnote.setPromissoryDate(model.getReturnDate() == null ? "" : formatterDateTime.format(model.getReturnDate()));

//            } else {
//                ProcessDAOHE psDAOHE = new ProcessDAOHE();
//                com.viettel.core.workflow.BO.Process ps = psDAOHE.findById(processId);
//                prnote.setDivision(ps.getSendGroup());
//                prnote.setName(ps.getSendUser());
//                prnote.setOrganization(Constants.NSW_SERVICE.DEPT_NAME);
//                prnote.setPromissoryContent(ps.getNote() == null ? " " : ps.getNote());
//                prnote.setPromissoryDate(null);
//            }
            ct.setPromissoryNote(prnote);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    //Yêu cầu SDBS
    public Boolean sendMs_04(Long fileId, Long processId) throws FileNotFoundException, Base64DecodingException, IOException {
        try {
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);

            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(54L), Constants.NSW_FUNCTION(4L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), processId.toString());
            AdditionalRequestDAO arqDAO = new AdditionalRequestDAO();
            AdditionalRequest addrq = arqDAO.findLastActiveByFileId(rtfile.getFileId(), getUserId());
            AttachDAOHE atDAOHE = new AttachDAOHE();
            //bo sung atttype
            Attachs a = atDAOHE.getLastByObjectIdAndType(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_SDBS);
            Attachment atm = new Attachment();
            if (a != null) {
                atm.setAttachmentId(a.getAttachId().toString());
                atm.setAttachmentName(a.getAttachName());
                atm.setAttachmentTypeCode(a.getAttachType().toString());
                atm.setAttachmentTypeName(a.getAttachTypeName());
            }
            Body bd = new Body();
            Content ct = new Content();
            Amendment adnt = new Amendment();
            adnt.setDivision(addrq.getCreateDeptName());
            adnt.setName(addrq.getCreatorName());
            adnt.setOrganization(Constants.NSW_SERVICE.DEPT_NAME);
            adnt.setAmendmentContent(addrq.getContent());
            adnt.setExpirationDate(null);
            adnt.setAttachment(atm);
            ct.setAmendment(adnt);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    //Từ chối chứng nhận
    public Boolean sendMs_05(Long fileId, Long processId, Long phase) throws FileNotFoundException, Base64DecodingException, IOException {
        try {
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);

            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(55L), Constants.NSW_FUNCTION(5L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), processId.toString());
            //  EvaluationRecordDAO ercDAO = new EvaluationRecordDAO();
            //EvaluationRecord erc = ercDAO.findLastActiveByFileId(rtfile.getFileId());
            //bo sung atttype
            AttachDAOHE atDAOHE = new AttachDAOHE();
            Attachs a = null;
            if (phase == 1L) {
                a = atDAOHE.getLastByObjectIdAndType(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_KHONG_PHAI_XNN_DAKY);
            } else if (phase == 2L) {
                a = atDAOHE.getLastByObjectIdAndType(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_CONG_VAN_KHONG_PHAI_XNN_DAKY);
            } else if (phase == 3L) {
                a = atDAOHE.getLastByObjectIdAndType(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_CONG_VAN_KHONG_PHAI_XNN_DAKY);
            }
            RejectCertificate rjC = new RejectCertificate();
            if (a != null) {
                Attachment atm = new Attachment();
                atm.setAttachmentId(a.getAttachId().toString());
                atm.setAttachmentName(a.getAttachName());
                atm.setAttachmentTypeCode(a.getAttachType().toString());
                atm.setAttachmentTypeName(a.getAttachTypeName());
                rjC.setAttachment(atm);
            }
            Body bd = new Body();
            Content ct = new Content();
            //  Gson gson = new Gson();
            ProcessDAOHE psDH = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process ps = psDH.findById(processId);
            //  EvaluationModel model = gson.fromJson(erc.getMainContent(), EvaluationModel.class);
            rjC.setDivision(ps.getSendGroup());
            rjC.setName(ps.getSendUser());
            rjC.setOrganization(Constants.NSW_SERVICE.DEPT_NAME);
            rjC.setRejectCertificateContent(ps.getNote() != null ? ps.getNote() : "Nội dung xem file đính kèm");
            ct.setRejectCertificate(rjC);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    //Cấp giấy chứng nhận
    public Boolean sendMs_06(Long fileId, Long processId) throws FileNotFoundException, Base64DecodingException, IOException {
        try {
            FilesDAOHE fDAOHE = new FilesDAOHE();
            Files files = fDAOHE.findById(fileId);
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);
            PermitDAO pmDAO = new PermitDAO();
            Permit pm = pmDAO.findLastByFileIdAndType(fileId, null);
            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(56L), Constants.NSW_FUNCTION(6L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), processId.toString());
            // EvaluationRecordDAO ercDAO = new EvaluationRecordDAO();
            // EvaluationRecord erc = ercDAO.findLastActiveByFileId(rtfile.getFileId());
            //bo sung atttype
            AttachDAOHE atDAOHE = new AttachDAOHE();
            Attachs a = atDAOHE.getLastByObjectIdAndType(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAY_CHUNG_NHAN_LUU_HANH_DAKY);
            Attachment atm = new Attachment();
            if (a != null) {
                atm.setAttachmentId(a.getAttachId().toString());
                atm.setAttachmentCode(a.getAttachId().toString());
                atm.setAttachmentName(a.getAttachName());
                atm.setAttachmentTypeCode(a.getAttachType().toString());
                atm.setAttachmentTypeName(a.getAttachTypeName());
            }
            Body bd = new Body();
            Content ct = new Content();
            // Gson gson = new Gson();
            //  EvaluationModel model = gson.fromJson(erc.getMainContent(), EvaluationModel.class);
            ProcessDAOHE psDH = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process ps = psDH.findById(processId);
            CertifiedCirculation cc = new CertifiedCirculation();
            cc.setCirculatingNo(pm.getReceiveNo());
            cc.setRapidTestName(rtfile.getRapidTestName());
            cc.setSignCirculatingDate(formatterDateTime.format(pm.getSignDate()));
            cc.setManufacture(rtfile.getManufacture());
            cc.setManufactureAddress(rtfile.getAddressOfManufacture());
            cc.setManufactureState(rtfile.getStateOfManufacture());
            cc.setBusinessAddress(files.getBusinessAddress());
            cc.setBusinessName(files.getBusinessName());
            cc.setSignDate(formatterDateTime.format(ps.getSendDate()));
            cc.setSignName(ps.getSendUser());
            cc.setSignPlace("HN");
            cc.setSignRank(ps.getSendGroup());
            cc.setAttachment(atm);
            ct.setCertifiedCirculation(cc);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    //Cấp giấy chứng nhận gia hạn
    public Boolean sendMs_07(Long fileId, Long processId) throws FileNotFoundException, Base64DecodingException, IOException {
        try {
            FilesDAOHE fDAOHE = new FilesDAOHE();
            Files files = fDAOHE.findById(fileId);
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);

            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(57L), Constants.NSW_FUNCTION(7L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), processId.toString());
            // EvaluationRecordDAO ercDAO = new EvaluationRecordDAO();
            // EvaluationRecord erc = ercDAO.findLastActiveByFileId(rtfile.getFileId());
            //bo sung atttype
            AttachDAOHE atDAOHE = new AttachDAOHE();
            Attachs a = atDAOHE.getLastByObjectIdAndType(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.GIAHAN_GIAY_CHUNG_NHAN_LUU_HANH_DAKY);
            Attachment atm = new Attachment();
            if (a != null) {
                atm.setAttachmentId(a.getAttachId().toString());
                atm.setAttachmentCode(a.getAttachId().toString());
                atm.setAttachmentName(a.getAttachName());
                atm.setAttachmentTypeCode(a.getAttachType().toString());
                atm.setAttachmentTypeName(a.getAttachTypeName());
            }
            Body bd = new Body();
            Content ct = new Content();
            // Gson gson = new Gson();
            // EvaluationModel model = gson.fromJson(erc.getMainContent(), EvaluationModel.class);
            PermitDAO pmDAO = new PermitDAO();
            Permit pm = pmDAO.findLastByFileIdAndType(fileId, null);
            ProcessDAOHE psDH = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process ps = psDH.findById(processId);
            ExtensionCirculation ec = new ExtensionCirculation();
            ec.setCirculatingNo(rtfile.getCirculatingNo());
            ec.setExtensionNo(rtfile.getExtensionNo().toString());
            ec.setCirculatingRapidTestNo(rtfile.getCirculatingRapidTestNo());
            ec.setCirculatingRapidTestDate(rtfile.getSignCirculatingDate() == null ? "" : formatterDateTime.format(rtfile.getSignCirculatingDate()));
            ec.setRapidTestName(rtfile.getRapidTestName());
            ec.setBusinessAddress(files.getBusinessAddress());
            ec.setBusinessName(files.getBusinessName());
            ec.setBusinessEmail(files.getBusinessEmail());
            ec.setBusinessFax(files.getBusinessFax());
            ec.setBusinessPhone(files.getBusinessPhone());
            ec.setSignDate(formatterDateTime.format(pm.getSignDate()));
            ec.setSignName(ps.getSendUser());
            ec.setSignPlace(Constants.NSW_SERVICE.HN_CODE);
            ec.setSignRank(ps.getSendGroup());
            ec.setAttachment(atm);
            ct.setExtensionCirculation(ec);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    //Trả thông tin cơ sở khảo nghiêm + phí + mẫu
    public Boolean sendMs_30(Long fileId, Long processId, Boolean sentkn) throws FileNotFoundException, IOException, Base64DecodingException {
        try {
            //   FilesDAOHE fDAOHE = new FilesDAOHE();
            //   Files files = fDAOHE.findById(fileId);
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);

            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(58L), Constants.NSW_FUNCTION(30L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), processId.toString());
            Body bd = new Body();
            Content ct = new Content();
            RequireFees rf = new RequireFees();
            String content;
            EvaluationRecord erc = null;
            Gson gson = new Gson();
            EvaluationModel model = null;
            if (sentkn) {
                EvaluationRecordDAO ercDAO = new EvaluationRecordDAO();
                erc = ercDAO.findLastActiveByFileId(rtfile.getFileId(), getUserId(), Constants.RAPID_TEST.EVALUATION.EVAL_TYPE.EVALUATION);
                model = gson.fromJson(erc.getFormContent(), EvaluationModel.class);
            }
            if (erc != null && model != null) {
                content = model.getContent();
                rf.setDivision(model.getDeptName());
                rf.setExpirationDate(model.getExpirationDate() == null ? "" : formatterDateTime.format(model.getExpirationDate()));
                rf.setName(model.getUserName());

                List<Assay> lsAss = new ArrayList<>();
                if (model.getEvaluationBasicAssay() != null && model.getEvaluationBasicAssay().size() > 0) {
                    for (RtAssay c : model.getEvaluationBasicAssay()) {
                        Assay a = new Assay();
                        a.setAssayAddress(c.getAddress());
                        a.setAssayCode(c.getCode());
                        a.setAssayName(c.getName());
                        a.setAssayPhone(c.getPhone());
                        lsAss.add(a);
                    }
                }
                rf.setListAssay(lsAss);

            } else {
                ProcessDAOHE psDAOHE = new ProcessDAOHE();
                com.viettel.core.workflow.BO.Process ps = psDAOHE.findById(processId);
                content = ps.getNote() != null ? ps.getNote() : "Nội dung xem file đính kèm";
                rf.setOrganization(Constants.NSW_SERVICE.DEPT_NAME);
                rf.setDivision(ps.getSendGroup());
                rf.setName(ps.getSendUser());
                rf.setExpirationDate(null);
            }
            Long cost = 0L;
            PaymentInfoDAO pyDAO = new PaymentInfoDAO();
            List<PaymentInfo> lsPay = pyDAO.getListPaymentByStatus(fileId, Constants.NSW_SERVICE.FEE_STATUS.MOI_TAO, 1L);
            if (lsPay != null && lsPay.size() > 0) {
                content += "\nThông tin tài khoản ngân hàng: "
                        + lsPay.get(0).getBankNo();
                for (PaymentInfo p : lsPay) {
                    cost += p.getCost();

                }
            }
            rf.setAmendmentContent(content);
            rf.setFeesValue(cost.toString());
            ct.setRequireFees(rf);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    //Thông tin yêu cầu nộp lệ phí
    public Boolean sendMs_11(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
        try {
            //   FilesDAOHE fDAOHE = new FilesDAOHE();
            // Files files = fDAOHE.findById(fileId);
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);

            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(60L), Constants.NSW_FUNCTION(11L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), processId.toString());
            ProcessDAOHE psDAOHE = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process ps = psDAOHE.findById(processId);
            Body bd = new Body();
            Content ct = new Content();
            RequireFees rF = new RequireFees();
            String content = "";
            // EvaluationRecordDAO ercDAO = new EvaluationRecordDAO();
            // EvaluationRecord erc = ercDAO.findLastActiveByFileId(rtfile.getFileId());
            // Gson gson = new Gson();
            //EvaluationModel model = gson.fromJson(erc.getMainContent(), EvaluationModel.class);
            PaymentInfoDAO pyDAO = new PaymentInfoDAO();
            Long cost = 0L;
            List<PaymentInfo> lsPay = pyDAO.getListPaymentByStatus(fileId, Constants.NSW_SERVICE.FEE_STATUS.MOI_TAO, 2L);
            List<PaymentInfo> lsPay1 = pyDAO.getListPaymentByStatus(fileId, Constants.NSW_SERVICE.FEE_STATUS.TU_CHOI, 2L);
            if (lsPay1 != null) {
                lsPay.addAll(lsPay1);
            }
            if (lsPay != null && lsPay.size() > 0) {
                content += "\nThông tin tài khoản ngân hàng: "
                        + lsPay.get(0).getBankNo();
                for (PaymentInfo p : lsPay) {
                    cost += p.getCost();
                }
            }
            rF.setAmendmentContent(ps.getNote() != null ? ps.getNote() : "Nội dung xem file đính kèm");
            rF.setOrganization(Constants.NSW_SERVICE.DEPT_NAME);
            rF.setDivision(ps.getSendGroup());
            rF.setName(ps.getSendUser());
            rF.setFeesValue(cost.toString());
            rF.setExpirationDate(null);
            ct.setRequireFees(rF);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    //Thông tin thu hồi chứng nhận
    public Boolean sendMs_31(Long fileId) throws FileNotFoundException, IOException, Base64DecodingException {
        try {
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);

            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(61L), Constants.NSW_FUNCTION(31L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), fileId.toString());
            //   EvaluationRecordDAO ercDAO = new EvaluationRecordDAO();
            //  EvaluationRecord erc = ercDAO.findLastActiveByFileId(rtfile.getFileId());
            //bo sung atttype
//            AttachDAOHE atDAOHE = new AttachDAOHE();
//            Attachs at = atDAOHE.getLastByObjectIdAndType(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.CONG_VAN_THUHOI_CN);
//            Attachment atm = new Attachment();
//            if (at != null) {
//                atm.setAttachmentId(at.getAttachId().toString());
//                atm.setAttachmentCode(at.getAttachId().toString());
//                atm.setAttachmentName(at.getAttachName());
//                atm.setAttachmentTypeCode(at.getAttachType().toString());
//                atm.setAttachmentTypeName(at.getAttachTypeName());
//            }
            //  Gson gson = new Gson();
            //  EvaluationModel model = gson.fromJson(erc.getMainContent(), EvaluationModel.class);
            //    PermitDAO pmDAO = new PermitDAO();
            //  Permit pm = pmDAO.findLastByFileIdAndType(fileId, null);
//            ProcessDAOHE psDH = new ProcessDAOHE();
//            com.viettel.core.workflow.BO.Process ps = psDH.findById(processId);
            Body bd = new Body();
            Content ct = new Content();
            Recovery re = new Recovery();
            re.setRecoveryContent(rtfile.getWithdraw());
            re.setOrganization(Constants.NSW_SERVICE.DEPT_NAME);
            re.setDivision("Phòng Nghiệp vụ");
            re.setName("Lãnh đạo");
            //re.setAttachment(atm);
            ct.setRecovery(re);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }

    //Thông tin cho phép thay đổi nội dung lưu hành
    public Boolean sendMs_37(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
        try {
            RapidTestDAOHE rtDAOHE = new RapidTestDAOHE();
            RtFile rtfile = rtDAOHE.findByFileId(fileId);

            Envelope evl = helper.makeEnvelopeSend(Constants.NSW_TYPE(64L), Constants.NSW_FUNCTION(37L), rtfile.getNswFileCode(), rtfile.getNswFileCode(), processId.toString());
            // EvaluationRecordDAO ercDAO = new EvaluationRecordDAO();
            // EvaluationRecord erc = ercDAO.findLastActiveByFileId(rtfile.getFileId());
            //bo sung atttype
            AttachDAOHE atDAOHE = new AttachDAOHE();
            Attachs at = atDAOHE.getLastByObjectIdAndType(fileId, Constants.RAPID_TEST.NHOM_BIEU_MAU.THAYDOI_GIAY_CHUNG_NHAN_LUU_HANH_DAKY);
            Attachment atm = new Attachment();
            if (at != null) {
                atm.setAttachmentId(at.getAttachId().toString());
                atm.setAttachmentCode(at.getAttachId().toString());
                atm.setAttachmentName(at.getAttachName());
                atm.setAttachmentTypeCode(at.getAttachType().toString());
                atm.setAttachmentTypeName(at.getAttachTypeName());
            }
            Body bd = new Body();
            Content ct = new Content();
            RequireFees rF = new RequireFees();
            //PermitDAO pmDAO = new PermitDAO();
            //Permit pm = pmDAO.findLastByFileIdAndType(fileId, null);
            ProcessDAOHE psDH = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process ps = psDH.findById(processId);
            //  Gson gson = new Gson();
            //  EvaluationModel model = gson.fromJson(erc.getMainContent(), EvaluationModel.class);
            rF.setContent(ps.getNote() != null ? ps.getNote() : "Nội dung xem file đính kèm");
            rF.setOrganization(Constants.NSW_SERVICE.DEPT_NAME);
            rF.setDivision(ps.getSendGroup());
            rF.setName(ps.getSendUser());
            rF.setAttachment(atm);
            ct.setRequireFees(rF);
            bd.setContent(ct);
            evl.setBody(bd);
            return helper.sendMs(evl);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
    }
}
