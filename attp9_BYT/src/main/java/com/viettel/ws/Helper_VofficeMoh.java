/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws;

import com.viettel.core.sys.BO.Category;
import com.viettel.core.user.BO.Users;
import com.viettel.core.user.DAO.UserDAOHE;
import com.viettel.core.user.model.UserToken;
import com.viettel.core.workflow.BO.Flow;
import com.viettel.core.workflow.BO.NodeDeptUser;
import com.viettel.core.workflow.BO.NodeToNode;
import com.viettel.core.workflow.DAO.ProcessDAOHE;
import com.viettel.core.workflow.WorkflowAPI;
import com.viettel.module.evaluation.BO.AdditionalRequest;
import com.viettel.module.evaluation.BO.Permit;
import com.viettel.module.evaluation.DAO.AdditionalRequestDAO;
import com.viettel.module.importDevice.BO.ImportDeviceFile;
import com.viettel.module.importDevice.BO.ImportDeviceProduct;
import com.viettel.module.importDevice.BO.VFileImportDevice;
import com.viettel.module.importDevice.DAO.ImportDeviceFiletDAO;
import com.viettel.module.importDevice.DAO.ImportDeviceProductDAO;
import com.viettel.module.payment.BO.Bill;
import com.viettel.module.payment.BO.PaymentInfo;
import com.viettel.module.payment.DAO.BillDAO;
import com.viettel.module.payment.DAO.PaymentInfoDAO;
import com.viettel.utils.Constants;
import com.viettel.utils.EncryptDecryptUtils;
import com.viettel.utils.FileUtil;
import com.viettel.utils.LogUtils;
import com.viettel.utils.ResourceBundleUtil;
import com.viettel.utils.StringUtils;
import com.viettel.voffice.BO.Business;
import com.viettel.voffice.BO.Document.Attachs;
import com.viettel.voffice.BO.Files;
import com.viettel.voffice.DAO.FilesDAOHE;
import com.viettel.voffice.DAOHE.AttachDAOHE;
import com.viettel.voffice.DAOHE.BookDocumentDAOHE;
import com.viettel.voffice.DAOHE.BusinessDAOHE;
import com.viettel.voffice.DAOHE.PermitDAO;
import com.viettel.ws.BO.*;
import com.viettel.ws.client.Voffice.*;
import com.viettel.wsgateway.Gateway;
import com.viettel.wsgateway.Gateway_Service;

import fr.opensagres.xdocreport.core.io.IOUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.zkoss.zk.ui.Sessions;

/**
 *
 * @author E5420
 */
public class Helper_VofficeMoh {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Helper_VofficeMoh.class);
    private final SimpleDateFormat formatterDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private final SimpleDateFormat formatterYear = new SimpleDateFormat("yyyy");
    VofficeMohService vofficeService;
    Helper hp = new Helper();

    public Envelope makeEnvelopeSend(String type, String function, String ref, String pref, String msId) {
        Envelope ev = new Envelope();
        Header hd = new Header();

        hd.setReference(new Reference("1.0", msId));
        hd.setFrom(new From("Vụ Trang thiết bị - Bộ Y tế", "TTB"));
        hd.setTo(new From("Hệ thống Voffice", "BYT"));
        Date d = new Date();
        hd.setSubject(new Subject(type, function, ref, pref, formatterYear.format(d), formatterDateTime.format(d)));
        ev.setHeader(hd);
        return ev;
    }

    public Boolean saveOrUpdateProcess(com.viettel.core.workflow.BO.Process currentP, ImportDeviceFile file, Long docType) {
        try {

            FilesDAOHE fDAOHE = new FilesDAOHE();
            Files files = fDAOHE.findById(file.getFileId());
            WorkflowAPI workflowInstance = WorkflowAPI.getInstance();
            List<NodeToNode> actions;// = new ArrayList<>();
//            List<Flow> lstFlows = workflowInstance.getFlowByDeptNObject(Constants.VU_TBYT_ID, docType);
//            Flow f;// = new Flow();
//            if (lstFlows != null) {
//                f = lstFlows.get(0);
//            }
            ProcessDAOHE pDAOHE = new ProcessDAOHE();
            actions = workflowInstance.getNextActionOfNodeId(currentP.getNodeId());

            NodeToNode nTn = actions.get(0);
            if (actions.size() > 0 && actions.get(0).getFormId() == null) {
                nTn = actions.get(1);
            }
            List<NodeDeptUser> lsnDu = WorkflowAPI.getInstance().findNDUsByNodeId(nTn.getNextId(), false);
            List<com.viettel.core.workflow.BO.Process> lsPRcU = pDAOHE.getDescProcess(files.getFileId(), docType);
            for (int i = 0; i < lsnDu.size(); i++) {
                NodeDeptUser nduReceive = lsnDu.get(i);
                com.viettel.core.workflow.BO.Process p = new com.viettel.core.workflow.BO.Process();
                if (nduReceive.getUserId() != null) {
                    Boolean ctn = true;
                    for (com.viettel.core.workflow.BO.Process ps : lsPRcU) {
                        if (nduReceive.getUserId().equals(ps.getReceiveUserId())) {
                            ctn = false;
                            break;
                        }
                    }
                    if (ctn) {
                        continue;
                    }
                    p.setReceiveUserId(nduReceive.getUserId());
                    p.setReceiveUser(nduReceive.getUserName());
                } else {
                    UserDAOHE usDH = new UserDAOHE();
                    List<Users> lsu = usDH.getUserByDeptPosID(nduReceive.getDeptId(), nduReceive.getPosId());
                    Users user = null;
                    if (lsPRcU != null && lsPRcU.size() > 0 && lsu != null && lsu.size() > 0) {
                        for (Users u : lsu) {
                            if (user == null && lsPRcU.contains(u.getUserId())) {
                                user = u;
                                break;
                            }
                        }
                    }
                    if (user == null) {
                        continue;
                    };
                    p.setReceiveUserId(user.getUserId());
                    p.setReceiveUser(user.getUserName());
                }
                p.setActionName(nTn.getAction());
                p.setActionType(nTn.getType());
                p.setIsActive(1L);
                p.setNodeId(nTn.getNextId());
                p.setSendUser(currentP.getReceiveUser());
                p.setSendUserId(currentP.getReceiveUserId());
                p.setStatus(nTn.getStatus());
                p.setReceiveGroupId(nduReceive.getDeptId());
                p.setReceiveGroup(nduReceive.getDeptName());
                p.setNote(nduReceive.getNodeName());
                p.setObjectId(files.getFileId());
                p.setObjectType(files.getFileType());
                p.setSendDate(new Date());
                p.setPreviousNodeId(nTn.getPreviousId());
                p.setProcessType(nduReceive.getProcessType());
                p.setOrderProcess(currentP.getOrderProcess() + 1L);
                p.setParentId(currentP.getProcessId());
                files.setStatus(nTn.getStatus());
                fDAOHE.saveOrUpdate(files);
                pDAOHE.saveOrUpdate(p);
                currentP.setFinishDate(new java.util.Date());
                currentP.setIsActive(1L);
                if (i < lsnDu.size() - 1) {
                    pDAOHE.saveOrUpdate(currentP);
                } else {
                    pDAOHE.saveAndCommit(currentP);
                }
            }

            return true;

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

            return false;
        }
    }

    public Boolean sendMs(Envelope envelop) throws FileNotFoundException, IOException {
        Boolean rs = true;
        if ("false".equals(ResourceBundleUtil.getString("send_service", "config"))) {
            return rs;
        }
        try {
            Gateway_Service moh = new Gateway_Service();
            Gateway client = moh.getGatewayPort();
            String evlrs = hp.ObjectToXml(envelop);
            // hàm nhận kết quả
            String response = client.receive(evlrs);
            Envelope enveloprs = hp.xmlToEnvelope(response);
            String function = enveloprs.getHeader().getSubject().getFunction();

            switch (function) {
                case "22":
                    Logger.getLogger(Helper_VofficeMoh.class.getName()).log(Level.SEVERE, null, hp.ObjectToXml(enveloprs));
                    rs = false;
                    break;

                case "99":
                    break;
                default:
                    rs = false;

                    break;
            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            rs = false;
        }
        return rs;
    }

    public String receiveMs(String evl) throws ParseException, IOException, Exception {
        Body bd = new Body();
        Content ct = new Content();
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = null;
        String type = null;
        String function = null;
        String ref = null;
        String pref = null;
        String msId = null;
        FilesDAOHE fDAOHE = new FilesDAOHE();
        try {
            Envelope envelop = hp.xmlToEnvelope(evl);

            // String docType = Constants.CATEGORY_TYPE.IMPORT_ORDER_OBJECT;
            if (envelop != null) {
                type = envelop.getHeader().getSubject().getType();
                ref = envelop.getHeader().getSubject().getReference();
                pref = envelop.getHeader().getSubject().getPreReference();
                msId = envelop.getHeader().getReference().getMessageId();
                function = envelop.getHeader().getSubject().getFunction();
                Document doc = envelop.getBody().getContent().getDocument();
                ImportDeviceFiletDAO imdDAO = new ImportDeviceFiletDAO();
                ImportDeviceFile file = null;
                Files f = null;
                com.viettel.core.workflow.BO.Process last = null;
                if (!"05".equals(function)) {
                    file = imdDAO.getFileBySendId(Long.parseLong(doc.getReceiveId()));
                    ProcessDAOHE psDAO = new ProcessDAOHE();
                    if (file == null) {
                        err = new com.viettel.ws.BO.Error();
                        err.setErrorCode("L000-0000-0000");
                        err.setErrorName("Id Công văn số đến nhập sai dữ liệu");
                        err.setSolution("Nhập chuẩn xác trường SendId");
                    } else {
                        FilesDAOHE FDAO = new FilesDAOHE();
                        f = FDAO.findById(file.getFileId());
                        last = (com.viettel.core.workflow.BO.Process) psDAO.getDescProcess(file.getFileId(), f.getFileType()).get(0);
                        if (!last.getReceiveUser().equals(ResourceBundleUtil.getString("user_voffice"))) {
                            err = new com.viettel.ws.BO.Error();
                            err.setErrorCode("U000-0000-0000");
                            err.setErrorName("Lỗi kết nối service");
                            err.setSolution("Kiểm tra người dùng tương tác với Voffice");
                        }
                    }
                }
                if (err != null) {
                    errList.add(err);
                    ct.setErrorList(errList);
                    function = Constants.NSW_FUNCTION(22L);
                } else {
                    err = new com.viettel.ws.BO.Error();
                    switch (function) {

                        case "04":
                            errList = ReceiveMs_04(doc, file);
                            break;
                        case "05":
                            return ReceiveMs_05(envelop);
                    }
                    if (errList.size() > 0) {
                        ct.setErrorList(errList);
                        function = Constants.NSW_FUNCTION(22L);
                    } else {

                        Boolean k = false;
                        if(f != null){
                            k = saveOrUpdateProcess(last, file, f.getFileType());
                        }
   
                        if (k == true) {
                            ct.setReceiveDate(formatterDateTime.format(new Date()));
                            function = Constants.NSW_FUNCTION(99L);
                        } else {
                            err.setErrorCode("L000-0000-0000");
                            err.setErrorName("Lỗi cập nhật process sau khi nhận ms thành công");
                            err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin");
                            errList.add(err);
                            Logger.getLogger(Helper_VofficeMoh.class.getName()).log(Level.SEVERE, null, err);
                            ct.setErrorList(errList);
                            function = Constants.NSW_FUNCTION(22L);
                        }
                    }
                }
            } else {
                err = new com.viettel.ws.BO.Error();
                err.setErrorCode("L000-0000-0000");
                err.setErrorName("Lỗi convert thông điệp ra object");
                err.setSolution("Kiểm tra lại trường chỉ tiêu thông tin");
                errList.add(err);
                Logger.getLogger(Helper_VofficeMoh.class.getName()).log(Level.SEVERE, null, err);
                ct.setErrorList(errList);
                function = Constants.NSW_FUNCTION(22L);
            }
        } catch (Exception ex) {
            err = new com.viettel.ws.BO.Error();
            err.setErrorCode("L000-0000-0000");
            err.setErrorName("Lỗi nhận dữ liệu");
            err.setSolution("Kiểm tra lại kết nối giữa 2 hệ thống");
            errList.add(err);
            LogUtils.addLogDB(ex);
            ct.setErrorList(errList);
            function = Constants.NSW_FUNCTION(22L);
        }
        Envelope ev = makeEnvelopeSend(type, function, ref, pref, msId);
        bd.setContent(ct);
        ev.setBody(bd);
        if (Constants.NSW_TYPE(22L).equals(type)) {
            fDAOHE.rollBack();
        }
        return hp.ObjectToXml(ev);
    }

    public String getMaxBookNumber(Long bookId) {
        BookDocumentDAOHE bookDocumentDAOHE = new BookDocumentDAOHE();
        return bookDocumentDAOHE.getMaxBookNumber(bookId).toString();
    }

    public Boolean SendMs_03(Long fileId, Long processId, Long phase) throws Exception {
        String date_ud = ResourceBundleUtil.getString("TTB_nsw_time", "config");
        FilesDAOHE fDAOHE = new FilesDAOHE();
        Files files = fDAOHE.findById(fileId);
        Date d_ud = formatterDateTime.parse(date_ud);
        if (d_ud.after(files.getCreateDate())) {
            return true;
        }
        ImportDeviceFiletDAO imDAO = new ImportDeviceFiletDAO();
        AttachDAOHE atDAO = new AttachDAOHE();
        ImportDeviceFile file = imDAO.findByFileId(fileId);
        Envelope send = makeEnvelopeSend("05", "03", file.getNswFileCode(), file.getNswFileCode(), processId.toString());
        try {

            Body bd = new Body();
            FileAttach fia = new FileAttach();
            Attachs att = atDAO.getByObjectIdAndAttachCat(fileId, phase == 1L ? Constants.OBJECT_TYPE.COSMETIC_PERMIT : Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH);

            Content ct = new Content();
            Document d = new Document();
//            UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
//                    "userToken");
            fia.setFileName(att.getAttachName());
            fia.setFileCode(att.getAttachId().toString());
            //Path path = Paths.get(att.getFullPathFile());
            // byte[] data = java.nio.file.Files.readAllBytes(path);
            // fia.setFileByte(Base64.encodeBase64String(data));
            if (file.getSendId() != null) {
                d.setSendId(file.getSendId().toString());
            } else {
                d.setSendId("0");
            }

            d.setType(phase.toString());
            d.setFileAttach(fia);
            ct.setDocument(d);
            bd.setContent(ct);
            send.setBody(bd);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

            return false;
        }
        return sendMs(send);
    }

    public List<com.viettel.ws.BO.Error> ReceiveMs_04(Document doc, ImportDeviceFile file) throws FileNotFoundException, IOException {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
//        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
//        FileAttach att = doc.getFileAttach();
//        Long attCat = 0L;
//        Long attType = Constants.OBJECT_TYPE.IMPORT_DEVICE_FILE_ATTACH_TYPE_VTB;
//        String attTypeName = "";
//        Long type = Long.parseLong(doc.getType());
//        try {
//
//            if (type == 1L) {
//                attCat = Constants.OBJECT_TYPE.COSMETIC_PERMIT;
//                attTypeName = "Công văn đồng ý cấp phép";
//            } else {
//                attCat = Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH;
//                attTypeName = "Công văn từ chối cấp phép";
//            }
//            AttachDAOHE attachDAOHE = new AttachDAOHE();
//            Attachs attach = new Attachs();
//            String folderPath = ResourceBundleUtil.getString("dir_upload");
//            FileUtil.mkdirs(folderPath);
//            String separator = ResourceBundleUtil.getString("separator");
//            if (!"/".equals(separator) && !"\\".equals(separator)) {
//                separator = "/";
//            }
//            String fileName = att.getFileName();
//
//            attach.setAttachName(fileName);
//            attach.setAttachTypeName(attTypeName);
//            attach.setIsActive(Constants.Status.ACTIVE);
//            attach.setObjectId(file.getFileId());
//            attach.setIsSent(1L);
//            attach.setAttachCat(attCat);
//            attach.setAttachType(attType);
//            attachDAOHE.saveOrUpdate(attach);
//            folderPath += separator + attCat + separator + attach.getAttachId();
//            attach.setAttachPath(folderPath);
//            attach.setAttachCode(attach.getAttachId().toString());
//            attachDAOHE.saveOrUpdate(attach);
//            FileUtil.mkdirs(folderPath);
//            File f = new File(attach.getFullPathFile());
//            String filebyte = sendMS05(att.getFileCode());
//            if (StringUtils.validString(filebyte)) {
//                byte[] data = Base64.decodeBase64(att.getFileByte());
//                OutputStream outputStream = new FileOutputStream(f);
//                IOUtils.write(data, outputStream);
//                outputStream.close();
//            } else {
//                err.setErrorCode("L000-0000-0000");
//                err.setErrorName("Không có dữ liệu file đính kèm");
//                err.setSolution("Truyền dữ liệu file đính kèm vào");
//                errList.add(err);
//
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(Helper_VofficeMoh.class
//                    .getName()).log(Level.SEVERE, null, ex);
//            err.setErrorCode(
//                    "L000-0000-0000");
//            err.setErrorName(
//                    "Lỗi nhận messeage");
//            err.setSolution(
//                    "Kiểm tra lại trường dữ liệu nhập");
//            errList.add(err);
//        }
        return errList;
    }

    public String ReceiveMs_05(Envelope ev) throws Exception {
        try {
            FileAttach fa = ev.getBody().getContent().getFileAttach();
            AttachDAOHE atDAO = new AttachDAOHE();
            Attachs att = atDAO.findByAttachId(Long.parseLong(fa.getFileCode()));

            Path path = Paths.get(att.getFullPathFile());
            byte[] data = java.nio.file.Files.readAllBytes(path);
            fa.setFileByte(Base64.encodeBase64String(data));
            Envelope elv = makeEnvelopeSend("07", "06", fa.getFileCode(), fa.getFileCode(), fa.getFileCode());
            Body bd = new Body();
            Content ct = new Content();
            ct.setFileAttach(fa);
            bd.setContent(ct);
            elv.setBody(bd);
            return hp.ObjectToXml(elv);
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            Envelope elv = makeEnvelopeSend("06", "22", "22", "22", "22");
            Body bd = new Body();
            Content ct = new Content();
            List<com.viettel.ws.BO.Error> lsE = new ArrayList<>();
            com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
            err.setErrorCode("L000-0000-0000");
            err.setErrorName("Lỗi nhận yêu cầu lấy file đín kèm");
            err.setSolution(ex.toString());
            lsE.add(err);
            ct.setErrorList(lsE);
            bd.setContent(ct);
            elv.setBody(bd);
            return hp.ObjectToXml(elv);
        }
    }

    public Response getReceiveVal(Document d, ImportDeviceFile file, Long bookId) throws Exception {
        String date_ud = ResourceBundleUtil.getString("TTB_nsw_time", "config");
        FilesDAOHE fDAOHE = new FilesDAOHE();
        Files files = fDAOHE.findById(file.getFileId());
        Date d_ud = formatterDateTime.parse(date_ud);
        if ("false".equals(ResourceBundleUtil.getString("send_service_voffie", "config")) || d_ud.after(files.getCreateDate())) {
            return new Response(getMaxBookNumber(bookId), "0");
        }
        try {
            Envelope send = makeEnvelopeSend("01", "01", file.getNswFileCode(), file.getNswFileCode(), file.getFileId().toString());
            Body bd = new Body();
            Content ct = new Content();
            ct.setDocument(d);
            bd.setContent(ct);
            send.setBody(bd);
            VuTTBService moh = new VuTTBService();
            IVuTTBService client = moh.getBasicHttpBindingIVuTTBService();
            String evlrs = hp.ObjectToXml(send);
            // hàm nhận kết quả
            String response = client.recevie(evlrs);
            Envelope enveloprs = hp.xmlToEnvelope(response);
            String function = enveloprs.getHeader().getSubject().getFunction();
            switch (function) {
                case "22":
                    Logger.getLogger(Helper_VofficeMoh.class.getName()).log(Level.SEVERE, null, enveloprs.getBody().getContent().getErrorList());
                    return null;

                case "01":
                    return enveloprs.getBody().getContent().getResponse();
                default:
                    return null;

            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

            return null;
        }
    }

    public Response getSendVal(Document d, ImportDeviceFile file, Long bookId) throws Exception {
        String date_ud = ResourceBundleUtil.getString("TTB_nsw_time", "config");
        FilesDAOHE fDAOHE = new FilesDAOHE();
        Files files = fDAOHE.findById(file.getFileId());
        Date d_ud = formatterDateTime.parse(date_ud);
        if ("false".equals(ResourceBundleUtil.getString("send_service_voffie", "config")) || d_ud.after(files.getCreateDate())) {
            return new Response(getMaxBookNumber(bookId), "0");
        }
        try {
            Envelope send = makeEnvelopeSend("03", "02", file.getNswFileCode(), file.getNswFileCode(), file.getFileId().toString());
            Body bd = new Body();
            Content ct = new Content();
            ct.setDocument(d);
            bd.setContent(ct);
            send.setBody(bd);
            VuTTBService moh = new VuTTBService();
            IVuTTBService client = moh.getBasicHttpBindingIVuTTBService();
            String evlrs = hp.ObjectToXml(send);
            // hàm nhận kết quả
            String response = client.recevie(evlrs);
            Envelope enveloprs = hp.xmlToEnvelope(response);
            String function = enveloprs.getHeader().getSubject().getFunction();
            switch (function) {
                case "22":
                    Logger.getLogger(Helper_VofficeMoh.class.getName()).log(Level.SEVERE, null, enveloprs.getBody().getContent().getErrorList());
                    return null;

                case "02":
                    return enveloprs.getBody().getContent().getResponse();
                default:
                    return null;

            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

            return null;
        }
    }

    public Boolean SendMs_39(Long fileId, Long processId, Long phase) throws Exception {
        String date_ud = ResourceBundleUtil.getString("TTB_nsw_time", "config");
        FilesDAOHE fDAOHE = new FilesDAOHE();
        Files files = fDAOHE.findById(fileId);
        Date d_ud = formatterDateTime.parse(date_ud);
        if (d_ud.after(files.getCreateDate())) {
            return true;
        }
        ImportDeviceFiletDAO imDAO = new ImportDeviceFiletDAO();
        AttachDAOHE atDAO = new AttachDAOHE();
        ImportDeviceFile file = imDAO.findByFileId(fileId);
        FilesDAOHE fDH = new FilesDAOHE();
        Files f = fDH.findById(fileId);
        Content ct = new Content();
        NoticeResult d = new NoticeResult();
        Body bd = new Body();
        PermitDAO pmDAO = new PermitDAO();
        Permit pm = pmDAO.findLastByFileIdAndType(fileId, null);
        Envelope send = hp.makeEnvelopeSend("65", "39", file.getNswFileCode(), file.getNswFileCode(), processId.toString());
        try {
            d.setNswFileCode(file.getNswFileCode());
            d.setTaxCode(f.getTaxCode());
            d.setSignDate(formatterDateTime.format(file.getSignDate()));// Ngay ky cua doanh nghiep
            Date signDateL = new Date();;
            if (pm != null) {
                d.setSendBookNumber(pm.getReceiveNo());
                signDateL = pm.getSignDate();
            }

            d.setBusinessName(file.getImporterName());
            d.setReport("30/2015/TT-BYT");
            d.setReportDate("2015-10-12 10:00:00");
            d.setProductName(file.getProductName());
            d.setReceiveBookNumber(file.getEquipmentNo());

			//            Calendar calEndDate = Calendar.getInstance();;
//            calEndDate.setTime(signDateL);
//            calEndDate.add(Calendar.YEAR, 1);
//            Date endDate = calEndDate.getTime();
            //endDate.setYear(endDate.getYear() + 1);
            String endDateConfig = ResourceBundleUtil.getString("ttb_endDate");
            Date endDate = formatterDateTime.parse(endDateConfig);
            Date uQDate = file.getUqDate();
            if (uQDate != null) {
                if (!endDate.before(uQDate)) {
                    endDate = uQDate;
                }
            }
            d.setEffect(formatterDateTime.format(endDate));
            d.setProductName(file.getProductName());
            d.setType(phase.toString());
            String equipmentNo = file.getEquipmentNo();
            Date createDate = f.getCreateDate();
            String cDate = "";
            if (createDate != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(createDate);
                int days = cal.get(Calendar.DAY_OF_MONTH);
                int months = cal.get(Calendar.MONTH) + 1;
                int years = cal.get(Calendar.YEAR);
                cDate = days + "/" + months + "/" + years;
            }
            String businessName = f.getBusinessName();
            String reason = "Bộ Y tế (Vụ Trang thiết bị và Công trình y tế) nhận được Công văn"
                    + " số " + equipmentNo
                    + " ngày " + cDate
                    + " của " + businessName
                    + " về việc nhập khẩu trang thiết bị y tế."
                    + " Sau khi xem xét tài liệu gửi kèm, mặt hàng Công ty nhập khẩu không thuộc danh mục"
                    + " Phụ lục số I của Thông tư 30/2015/TT-BYT, công ty không phải xin giấy phép nhập khẩu của Bộ Y tế";
            d.setReason(reason);
            d.setSignPlace("Hà Nội");
            Date signDate = new Date();
            if (pm != null && pm.getSignDate() != null) {
                signDate = pm.getSignDate();
            }
            d.setPermitSignDate(formatterDateTime.format(signDate));
            if ("2".equals(d.getType())) {//Neu la cong van tu choi thi theo pm
                String permitSignerName = ResourceBundleUtil.getString("ttb_signer");
                String permitPosName = ResourceBundleUtil.getString("ttb_posName");
                if (pm != null) {
                    if (pm.getSignerName() != null) {
                        permitSignerName = pm.getSignerName();
                    }

                    UserDAOHE udhe = new UserDAOHE();
                    Users u = udhe.findById(pm.getBusinessId());
                    if (u != null) {
                        permitPosName = u.getPosName();

                    }
                }
                d.setPermitSignName(permitSignerName);
                d.setPermitPosName(permitPosName);

            } else {
                //Neu la cong van cap phep thi la nguyen viet tien
                d.setPermitSignName(ResourceBundleUtil.getString("ttb_signer"));
                d.setPermitPosName(ResourceBundleUtil.getString("ttb_posName"));
                
            }

//            if (file.getPrice() == 500000L) {
//                d.setPrice("Thiết bị y tế nhập khẩu trị giá dưới 1 tỷ đồng");
//            } else if (file.getPrice() == 1000000L) {
//                d.setPrice("Thiết bị y tế nhập khẩu trị giá từ 1 tỷ đến 3 tỷ đồng");
//            } else if (file.getPrice() == 3000000L) {
//                d.setPrice("Thiết bị y tế nhập khẩu trị giá trên 3 tỷ đồng");
//            } else {
//                d.setPrice("Dụng cụ y tế, vật tư cấy ghép nhập khẩu");
//            }
            ImportDeviceProductDAO imdpDAO = new ImportDeviceProductDAO();
            Long fileIdNeedToFindProduct = fileId;
            if (Constants.IMPORT.TYPE_CREATE_GIAHAN.equals(file.getTypeCreate())) {
            	String sendBookNumber = file.getSendBookNumber();
                PermitDAO pDAO = new PermitDAO();
                Permit permitOld = pDAO.findTtbCreateNewByReveiveNo(sendBookNumber);
                if(permitOld != null && permitOld.getFileId() !=null){
                	fileIdNeedToFindProduct = permitOld.getFileId();
                }
            } 
            @SuppressWarnings("unchecked")
			List<ImportDeviceProduct> lsP = imdpDAO.findByImportFileId(fileIdNeedToFindProduct);
            if (lsP != null && lsP.size() > 0) {
                List<Product> products = new ArrayList<>();
                for (ImportDeviceProduct idp : lsP) {
                    Product p = new Product();
                    p.setGroupProductCode(file.getGroupProductId());
                    p.setGroupProductName(file.getGroupProductName());
                    p.setName(file.getProductName());
                    p.setMode(idp.getModel());
                    p.setManufacturer(idp.getManufacturer());
                    p.setManufacturerNational(idp.getNationalName());
                    p.setManufacturerNationalCode(idp.getNationalCode());
                    p.setProductYear(idp.getProduceYear());
                    p.setDistributor(idp.getDistributor());
                    p.setDistributorNational(idp.getNationalDistributorCode());
                    p.setDistributorNationalCode(idp.getNationalDistributorCode());
                    p.setNationalProduct(idp.getNationalProduct());
                    p.setNationalProductCode(idp.getNationalProductCode());
                    p.setIsCategory(idp.getIsCategory().toString());
                    p.setQuantity(idp.getQuantity());
                    p.setProductManufacture(idp.getProductManufacture());
                    products.add(p);
                }
                d.setProducts(products);
            }
            Attachment attm = new Attachment();
            Attachs att = atDAO.getByObjectIdAndAttachCat(fileId, phase == 1L ? Constants.OBJECT_TYPE.COSMETIC_PERMIT : Constants.OBJECT_TYPE.COSMETIC_REJECT_DISPATH);
            attm.setAttachmentId(att.getAttachId().toString());
            attm.setAttachmentCode(att.getAttachId().toString());
            attm.setAttachmentTypeCode(att.getAttachType().toString());
            String attTypeName = att.getAttachTypeName();
            if (attTypeName == null) {
                attTypeName = "Giấy phép";
            }
            attm.setAttachmentTypeName(attTypeName);
            attm.setAttachmentName(att.getAttachName());
            //  Path path = Paths.get(att.getFullPathFile());
            //   byte[] data = java.nio.file.Files.readAllBytes(path);
            //   attm.setFileByte(Base64.encodeBase64String(data));
//            UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
//                    "userToken");
//            d.setSendId(file.getSendId().toString());
            d.setType(phase.toString());
            d.setAttachment(attm);
            ct.setNoticeResult(d);
            bd.setContent(ct);
            send.setBody(bd);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

            return false;
        }
        return hp.sendMs(send);
    }

    public Boolean SendMs_45(VFileImportDevice vfile, String reason) throws Exception {
        String date_ud = ResourceBundleUtil.getString("TTB_nsw_time", "config");
        Date d_ud = formatterDateTime.parse(date_ud);
        if (d_ud.after(vfile.getCreateDate())) {
            return true;
        }
//        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
//                "userToken");
        Content ct = new Content();
        NoticeCancle d = new NoticeCancle();
        Body bd = new Body();
        Envelope send = hp.makeEnvelopeSend("67", "45", vfile.getNswFileCode(), vfile.getNswFileCode(), vfile.getNswFileCode());
        try {
            //d.setFileCode(vfile.getNswFileCode());
            d.setNswFileCode(vfile.getNswFileCode());
            d.setReasons(reason);
//            d.setProcessName(tk.getUserFullName());
//            d.setPosition(tk.getPosName());
            ct.setNoticeCancle(d);
            bd.setContent(ct);
            send.setBody(bd);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

            return false;
        }
        return hp.sendMs(send);
    }

    private Long getUserId() {
        UserToken tk = (UserToken) Sessions.getCurrent(true).getAttribute(
                "userToken");
        return tk.getUserId();
    }

    public Boolean sendMS_42(Long fileId, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
        try {
            ImportDeviceFiletDAO imDAO = new ImportDeviceFiletDAO();
            ImportDeviceFile imFile = imDAO.findByFileId(fileId);
            AttachDAOHE atDAOHE = new AttachDAOHE();
            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(33L), Constants.NSW_FUNCTION(42L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
            AdditionalRequestDAO arqDAO = new AdditionalRequestDAO();
            Body bd = new Body();
            Content ct = new Content();
            FilesEdit fedit = new FilesEdit();
            Attachs a = null;
            AdditionalRequest addrq = arqDAO.findLastActiveByFileId(fileId, getUserId());
            if (addrq != null) {
                a = atDAOHE.getByObjectIdAndAttachCat(fileId, Constants.OBJECT_TYPE.IMPORT_DEVICE_SDBS_DISPATH);
            }
            Attachment atm = new Attachment();
            if (a != null) {
                atm.setAttachmentId(a.getAttachId().toString());
                atm.setAttachmentName(a.getAttachName());
                atm.setAttachmentTypeCode(a.getAttachType() == null ? "1111" : a.getAttachType().toString());
                atm.setAttachmentTypeName(a.getAttachTypeName() == null ? "Công văn yêu cầu SDBS" : a.getAttachTypeName().toString());
                fedit.setAttachment(atm);
            }
            if (addrq != null) {
                fedit.setComments(addrq.getContent());
                fedit.setProcessDate(addrq.getCreateDate() == null ? "" : formatterDateTime.format(addrq.getCreateDate()));
                fedit.setProcessName(addrq.getCreatorName());
            }
            fedit.setDeptCode(Constants.NSW_SERVICE.DEPT_CODE_TTB);
            fedit.setDeptName(Constants.NSW_SERVICE.DEPT_NAME_TTB);
            fedit.setNswFileCode(imFile.getNswFileCode());
            Date timeExpire = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(timeExpire);
            c.add(Calendar.DAY_OF_MONTH, 60);
            timeExpire = c.getTime();
            fedit.setExpDate(formatterDateTime.format(timeExpire));
            ct.setFilesEdit(fedit);
            bd.setContent(ct);
            evl.setBody(bd);
            //return true;
            return hp.sendMs(evl);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

            return false;
        }
    }

    public Boolean sendMS_43(Long fileId, Long phase, Long processId) throws FileNotFoundException, IOException, Base64DecodingException {
        try {
            ImportDeviceFiletDAO imDAO = new ImportDeviceFiletDAO();
            ImportDeviceFile imFile = imDAO.findByFileId(fileId);
            Envelope evl = makeEnvelopeSend(Constants.NSW_TYPE(23L), Constants.NSW_FUNCTION(43L), imFile.getNswFileCode(), imFile.getNswFileCode(), processId.toString());
            FilesCost fc = new FilesCost();
            //com.viettel.module.evaluation.DAO.EvaluationRecordDAO ercDAO = new com.viettel.module.evaluation.DAO.EvaluationRecordDAO();
            //EvaluationRecord erc = ercDAO.findLastActiveByFileId(fileId);
            //Gson gson = new Gson();
            //EvaluationModel model = gson.fromJson(erc.getMainContent(), EvaluationModel.class);
            ProcessDAOHE psDAOHE = new ProcessDAOHE();
            com.viettel.core.workflow.BO.Process curenP = psDAOHE.findById(processId);

            fc.setComments(curenP.getNote());
            if ("".equals(curenP.getNote())) {
                fc.setComments("Yêu cầu nộp Phí");
            }
            fc.setNswFileCode(imFile.getNswFileCode());
            fc.setProcessDate(curenP.getSendDate() == null ? "" : formatterDateTime.format(curenP.getSendDate()));
            fc.setProcessName(curenP.getSendUser());
            fc.setType(Constants.NSW_SERVICE.FEE_STATUS.FEE_NEW);
            List<Fee> lsFee = new ArrayList<>();
            PaymentInfoDAO pinfoDAO = new PaymentInfoDAO();
            Long status = Constants.NSW_SERVICE.FEE_STATUS.MOI_TAO;
            List<PaymentInfo> lsPinfo = pinfoDAO.getListPaymentByStatus(imFile.getFileId(), status, phase);
            Long cost = 0L;
            if (lsPinfo != null && lsPinfo.size() > 0) {
                for (PaymentInfo p : lsPinfo) {
                    Fee f = new Fee();
                    f.setBankNo(p.getBankNo());
                    f.setComments(p.getFeeName());
                    cost += p.getCost();
                    f.setCost(p.getCost().toString());
                    f.setDeptCode(p.getDeptCode());
                    f.setDeptName(p.getDeptName());
                    f.setFeeId(p.getPaymentInfoId().toString());
                    lsFee.add(f);
                }
            }
            if (cost == 0L) {
                return true;
            }
            fc.setFeeList(lsFee);
            Content ct = new Content();
            Body bd = new Body();
            ct.setFilesCost(fc);
            bd.setContent(ct);
            evl.setBody(bd);
            //return true;
            return hp.sendMs(evl);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);

            return false;
        }
    }

    public List<com.viettel.ws.BO.Error> reiceiveMs_44(Envelope envelope, Files files) throws FileNotFoundException, Base64DecodingException {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        try {
            FilesCost fc = envelope.getBody().getContent().getFilesCost();

            Bill b = new Bill();
            BillDAO bDAO = new BillDAO();
            b.setCreatorId(files.getCreatorId());
            b.setTotal(Long.valueOf(fc.getTotalFee()));
            b.setCreatetorName(files.getCreatorName());
            b.setCreateDate(new java.util.Date());
            b.setPaymentDate(new Date());
            b.setStatus(Constants.NSW_SERVICE.FEE_STATUS.MOI_NOP);
            b.setCode(fc.getInvoiceNo());
            bDAO.saveOrUpdate(b);

            PaymentInfoDAO pDAO = new PaymentInfoDAO();
            String nswFile = fc.getNswFileCode();
            List<PaymentInfo> lstPaymentInfo = pDAO.getListPaymentbynswFileCode(nswFile);
            for (PaymentInfo p : lstPaymentInfo) {
                p.setStatus(Constants.NSW_SERVICE.FEE_STATUS.MOI_NOP);
                p.setBillId(b.getBillId());
                pDAO.saveOrUpdate(p);
            }
            Attachment att = fc.getAttachment();
            saveAttachmentFee(att, b.getBillId(), Constants.OBJECT_TYPE.RAPID_TEST_PAYMENT_ALREADY);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode(
                    "U000-0000-0000");
            err.setErrorName(
                    "Lỗi nhận kết quả nộp phí");
            err.setSolution(
                    "Kiểm tra chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    public Boolean saveAttachmentFee(Attachment att, Long objectId, Long attCat) throws FileNotFoundException, IOException, Base64DecodingException {
        try {
            if (att == null) {
                return false;
            }
            if (att.getAttachMohId() != null && !"0".equals(att.getAttachMohId())) {
                Long attachId = Long.valueOf(att.getAttachMohId());
                AttachDAOHE attachDAOHE = new AttachDAOHE();
                Attachs attach = attachDAOHE.findByAttachId(attachId);
                attach.setObjectId(objectId);
                attach.setAttachCat(attCat);
                attach.setAttachCode("PNT");
                attach.setAttachTypeName("Phiếu nộp tiền");
                attach.setIsActive(Constants.Status.ACTIVE);
                attach.setIsSent(1L);
                attach.setAttachType(null);
                attachDAOHE.saveOrUpdate(attach);
                return true;
            } else {
                AttachDAOHE attachDAOHE = new AttachDAOHE();
                Attachs attach = new Attachs();
                String folderPath = ResourceBundleUtil.getString("dir_upload");
                FileUtil.mkdirs(folderPath);
                String separator = ResourceBundleUtil.getString("separator");
                if (!"/".equals(separator) && !"\\".equals(separator)) {
                    separator = "/";
                }
                String fileName = att.getFileName();
                fileName = FileUtil.getSafeFileNameAll(fileName);

                attach.setAttachName(fileName);
                attach.setAttachCode("PNT");
                attach.setAttachTypeName("Phiếu nộp tiền");
                attach.setIsActive(Constants.Status.ACTIVE);
                attach.setIsSent(1L);
                attach.setObjectId(objectId);
                attach.setAttachCat(attCat);
                attach.setAttachType(null);

                attachDAOHE.saveOrUpdate(attach);
                folderPath += separator + attCat + separator + attach.getAttachId();
                attach.setAttachPath(folderPath);

                attachDAOHE.saveOrUpdate(attach);
                File f = new File(attach.getFullPathFile());
                if (!f.exists()) {
                    // tao folder
                    if (!f.getParentFile().exists()) {
                        f.getParentFile().mkdirs();
                    }
                    f.createNewFile();
                }

                if (StringUtils.validString(att.getFileByte())) {
                    byte[] data = Base64.decodeBase64(att.getFileByte());
                    OutputStream outputStream = new FileOutputStream(f);
                    IOUtils.write(data, outputStream);
                    outputStream.close();
                }
            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
        return true;
    }

//    private List<com.viettel.ws.BO.Error> checkDoc(Document dc) {
//        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
//        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
//        if (dc == null) {
//            err.setErrorCode("A000-0000-0000");
//            err.setErrorName("Lỗi cấu trúc thông điệp");
//            err.setSolution("Gửi cấu trúc thông điệp theo đúng định dạng");
//            errList.add(err);
//        }
//        Attachment att = dc.getAttachment();
//        if (att == null) {
//            err.setErrorCode("A000-0000-0000");
//            err.setErrorName("Thiếu đăng ký kinh doanh");
//            err.setSolution("Bổ sung đăng ký kinh doanh");
//            errList.add(err);
//            return errList;
//        }
//        List<Product> lsp = dc.getProducts();
//        if (lsp == null || lsp.isEmpty()) {
//            err.setErrorCode("A000-0000-0000");
//            err.setErrorName("Không có thông tin sản phẩm");
//            err.setSolution("Bổ sung thông tin sản phẩm");
//            errList.add(err);
//        } else {
//            for (Product p : lsp) {
//                if ("1".equals(p.getIsCategory())) {
//                    if (p.getAttachment() == null || p.getAttachment().getAttachmentCode() == null) {
//                        err.setErrorCode("A000-0000-0000");
//                        err.setErrorName("Sản phẩm model " + p.getMode() + " không có phụ lục đính kèm như đã chọn");
//                        err.setSolution("Bổ sung phụ lục đính kèm");
//                        errList.add(err);
//                        return errList;
//                    } else if (p.getAttachments() == null || p.getAttachmentList().isEmpty()) {
//                        err.setErrorCode("A000-0000-0000");
//                        err.setErrorName("Sản phẩm model " + p.getMode() + " không có tài liệu đính kèm");
//                        err.setSolution("Bổ sung tài liệu đính kèm");
//                        errList.add(err);
//                    }
//                }
//            }
//        }
//        return errList;
//    }

    private Files saveFileFromImDoc(Document dc, Files files) {
        try {
            FilesDAOHE fdh = new FilesDAOHE();
            if (files == null || files.getFileId() == null) {
                files = new Files();
            } else {
                ImportDeviceFiletDAO imfileDao = new ImportDeviceFiletDAO();
                Files fileTemp = hp.copyFilesToFiles(files, null);
                fdh.saveOrUpdate(fileTemp);
                ImportDeviceFile imFile = imfileDao.findByFileId(files.getFileId());
                if (imFile != null) {
                    imFile.setFileId(fileTemp.getFileId());
                    imfileDao.saveOrUpdate(imFile);
                    ImportDeviceProductDAO impDao = new ImportDeviceProductDAO();
                    impDao.setIsTempProduct(fileTemp.getFileId(), files.getFileId());
                    AttachDAOHE attDH = new AttachDAOHE();
                    attDH.setIsTempAttach(fileTemp.getFileId(), files.getFileId(), Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE);
                }

            }
            UserDAOHE usDAOHE = new UserDAOHE();
            Users u = usDAOHE.getUserByName(dc.getTaxCode());
            if (u == null) {
                u = new Users();
                Business b = new Business();
                BusinessDAOHE bDAOHE = new BusinessDAOHE();
                b.setBusinessAddress(dc.getBusinessAddress());
                b.setBusinessFax(dc.getBusinessFax());
                b.setBusinessName(dc.getBusinessName());
                b.setBusinessTaxCode(dc.getTaxCode());
                b.setBusinessTelephone(dc.getBusinessPhone());
                b.setIsActive(1L);
                b.setUserName(dc.getTaxCode());
                bDAOHE.saveOrUpdate(b);
                u.setBusinessName(dc.getBusinessName());
                u.setUserName(dc.getTaxCode());
                u.setUserType(Constants.USER_TYPE.ENTERPRISE_USER);
                u.setBusinessId(b.getBusinessId());
                u.setPassword(EncryptDecryptUtils.encrypt(u.getUserName() + "!@@"));
                u.setStatus(1L);
                usDAOHE.saveOrUpdate(u);
            }
            WorkflowAPI w = new WorkflowAPI();
            Category ftype = w.getProcedureTypeByCode(Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT);
            if (ftype != null) {
                files.setFileType(ftype.getCategoryId());
                files.setFileTypeName(ftype.getName());
            } else {
                files.setFileType(Long.MIN_VALUE);
                files.setFileTypeName(null);
            }
            files.setBusinessAddress(dc.getBusinessAddress());
            files.setBusinessFax(dc.getBusinessFax());
            files.setBusinessName(dc.getBusinessName());
            files.setBusinessPhone(dc.getBusinessPhone());
            files.setCreateDate(new Date());
            files.setCreatorName(dc.getBusinessName());
            files.setCreatorId(u.getUserId());
            files.setBusinessId(u.getBusinessId());
            files.setFileCode(dc.getFileCode());
            files.setNswFileCode(dc.getNswFileCode());
            files.setFlowId(Long.MIN_VALUE);
            files.setIsActive(0L);
            files.setIsTemp(0L);
            files.setModifyDate(new Date());
            files.setStartDate(new Date());
            files.setStatus(Constants.PROCESS_STATUS.INITIAL);
            files.setTaxCode(dc.getTaxCode());

            try {
                String fileType = dc.getFileType();//linhdx - Loai ho so: 1: Them moi: 2 : Thay doi: 3 : gia han
                Long fileTypeL = Long.valueOf(fileType);
                files.setFileTypeHS(fileTypeL);
            } catch (Exception ex) {
                LogUtils.addLog(ex);
            }

            try {
                String isChange = dc.getIsChange();//linhdx - Loai ho so: 1: Them moi: 2 : Thay doi: 3 : gia han
                Long isChangeL = Long.valueOf(isChange);
                files.setIsChange(isChangeL);
            } catch (Exception ex) {
                LogUtils.addLog(ex);
            }
            files.setReasons(dc.getReasons());
            files.setNotes(dc.getNotes());

            fdh.saveOrUpdate(files);

            return files;
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    public void saveOrUpdateFiles(String nsWCode, Long docType, Boolean pass) {
        try {
            FilesDAOHE fdh = new FilesDAOHE();
            Files files = fdh.findByNswCode(nsWCode, docType, 0L, 0L);
            if (files != null) {
                Long fileId = files.getFileId();
                if (pass) {
                    files.setIsActive(1L);
                    fdh.SaveAndCommit(files);
                } else {
                    ImportDeviceFiletDAO imfileDao = new ImportDeviceFiletDAO();
                    ImportDeviceProductDAO impDao = new ImportDeviceProductDAO();
                    AttachDAOHE attDH = new AttachDAOHE();
                    Files fileTemp = fdh.findByNswCode(nsWCode, docType, 1L, 1L);

                    //rollback files cũ
                    files = hp.copyFilesToFiles(fileTemp, files);
                    files.setIsTemp(0L);
                    files.setIsActive(1L);
                    fileTemp.setIsActive(0L);
                    //dell id file cu
                    ImportDeviceFile imFiledel = imfileDao.findById(fileId);
                    imFiledel.setFileId(fileTemp.getFileId());
                    imfileDao.saveOrUpdate(imFiledel);
                    //rollback file moi
                    ImportDeviceFile imFile = imfileDao.findById(fileTemp.getFileId());
                    imFile.setFileId(fileId);
                    imfileDao.saveOrUpdate(imFile);
                    //delete product cu
                    List<ImportDeviceProduct> lsp = impDao.findAllIdByFileId(fileId);
                    for (ImportDeviceProduct p : lsp) {
                        attDH.deleteByFileId(fileId, Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL);
                        attDH.deleteByFileId(fileId, Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_ATT);
                        p.setFileId(fileTemp.getFileId());
                        impDao.delete(p);
                    }
                    //rollback product cu
                    impDao.setIsTempProduct(fileId, fileTemp.getFileId());

                    //dell att cu
                    attDH.deleteByFileId(fileId, Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE);
                    //rollback att moi
                    attDH.setIsTempAttach(files.getFileId(), fileTemp.getFileId(), Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE);

                    fdh.saveOrUpdate(files);
                    fdh.SaveAndCommit(fileTemp);
                }
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
        }
    }

    private ImportDeviceFile saveImpDeviceFile(Document dc, Files files) throws Exception {
        try {
            ImportDeviceFiletDAO idfDAO = new ImportDeviceFiletDAO();
            ImportDeviceFile idf = new ImportDeviceFile();
            idf.setDirector(dc.getDirectorName());
            idf.setDirectorMobile(dc.getDirectorMobiPhone());
            idf.setDirectorPhone(dc.getDirectorPhone());
            idf.setDocumentTypeCode(Constants.IMPORT.DOCUMENT_TYPE_CODE_TAOMOI);
            idf.setEquipmentNo(dc.getReceiveBookNumber());
            idf.setFileId(files.getFileId());
            idf.setGroupProductId(dc.getGroupProductCode());
            idf.setGroupProductName(dc.getGroupProductName());
            idf.setImportPurpose(dc.getIntendedUse());
            idf.setImporterAddress(dc.getBusinessAddress());
            idf.setImporterFax(dc.getBusinessFax());
            idf.setImporterName(dc.getBusinessName());
            idf.setImporterPhone(dc.getBusinessPhone());
            idf.setNswFileCode(dc.getNswFileCode());
            String typeCreate = dc.getFileType();
            if (typeCreate != null && !"".equals(typeCreate)) {
                Long typeCreateL = Long.valueOf(typeCreate);
                idf.setTypeCreate(typeCreateL);
            }
            String isChange = dc.getIsChange();
            if (isChange != null && !"".equals(isChange)) {
                Long isChangeL = Long.valueOf(isChange);
                idf.setIsChange(isChangeL);
            }

            String sendBookNumber = dc.getSendBookNumber();
            idf.setSendBookNumber(sendBookNumber);
            PermitDAO pDAO = new PermitDAO();
            Permit permitOld = pDAO.findTtbCreateNewByReveiveNo(sendBookNumber);
            if (permitOld != null) {
                ImportDeviceFiletDAO dao = new ImportDeviceFiletDAO();
                ImportDeviceFile idOld = dao.findByFileId(permitOld.getFileId());
                idf.setOldNswFileCode(idOld.getNswFileCode());
            }

            switch (dc.getPriceCode()) {
                case "1":
                    idf.setPrice(500000L);
                    break;
                case "2":
                    idf.setPrice(1000000L);
                    break;
                case "3":
                    idf.setPrice(3000000L);
                    break;
                default:
                    idf.setPrice(0L);
                    break;
            }
            idf.setProductName(dc.getProductName());
            idf.setResponsiblePersonMobile(dc.getOfficersMobile());
            idf.setResponsiblePersonName(dc.getOfficersName());
            idf.setResponsiblePersonPhone(dc.getOfficersPhone());
            idf.setSignDate(formatterDateTime.parse(dc.getSignDate()));
            idf.setSignName(dc.getSignName());
            idf.setSignPlace(dc.getSignPlace());

            idf.setUqDate(formatterDateTime.parse(dc.getExpAuthoriSation()));

            idfDAO.saveOrUpdate(idf);
            return idf;
        } catch (Exception ex) {
            //System.out.println(ex);
            LogUtils.addLogDB(ex);
            return null;
        }
    }

    private List<com.viettel.ws.BO.Error> saveListProducts(List<Product> lsp, Files files) {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        try {
            for (Product p : lsp) {
                ImportDeviceProductDAO idpDAO = new ImportDeviceProductDAO();
                ImportDeviceProduct idP = new ImportDeviceProduct();
                idP.setFileId(files.getFileId());

                idP.setIsCategory("1".equals(p.getIsCategory()) ? 1L : 0L);
                idP.setManufacturer(p.getManufacturer());
                idP.setModel(p.getMode());
                idP.setSendProductCode(0L);
                idP.setNationalCode(p.getManufacturerNationalCode());
                idP.setNationalName(p.getManufacturerNational());

                idP.setDistributor(p.getDistributor());
                idP.setNationalDistributorCode(p.getDistributorNationalCode());
                idP.setNationalDistributorName(p.getDistributorNational());

                idP.setProductManufacture(p.getProductManufacture());
                idP.setNationalProductCode(p.getNationalProductCode());
                idP.setNationalProduct(p.getNationalProduct());

                idP.setProduceYear(p.getProductYear());

                idP.setQuantity(p.getQuantity());
                idpDAO.saveOrUpdate(idP);
                if ("1".equals(p.getIsCategory())) {
                    //linhdx 20151123
                    Attachment atch = p.getAttachment();
                    //Attachment attPL = p.getAttachment();
                    Boolean kPL;
                    kPL = hp.saveAttachmentFromNSW(atch, idP.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_MODEL, files.getNswFileCode(), "TTB");
                    if (!kPL) {
                        err.setErrorCode("U000-0000-0000");
                        err.setErrorName(
                                "Lỗi lưu file phụ lục sản phẩm model " + atch.getAttachmentCode());
                        err.setSolution(
                                "Kiểm tra chỉ tiêu thông tin và hàm validate");
                        errList.add(err);
                        break;
                    }
                }
                for (Attachment att : p.getAttachments()) {
                    Boolean k = hp.saveAttachmentFromNSW(att, idP.getProductId(), Constants.OBJECT_TYPE.IMPORT_DEVICE_PRODUCT_ATT, files.getNswFileCode(), "TTB");
                    if (!k) {
                        err.setErrorCode("U000-0000-0000");
                        err.setErrorName(
                                "Lỗi lưu file đính kèm " + att.getAttachmentCode());
                        err.setSolution(
                                "Kiểm tra chỉ tiêu thông tin và hàm validate");
                        errList.add(err);
                    }
                }
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode("U000-0000-0000");
            err.setErrorName(
                    "Lỗi nhận kết quả nộp phí");
            err.setSolution(
                    "Kiểm tra chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    public List<com.viettel.ws.BO.Error> reiceiveMs_40_41(Envelope envelope, Files files) throws Exception {
        List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
        com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
        try {
            Document dc = envelope.getBody().getContent().getDocument();

            //errList = checkDoc(dc);
            if (!errList.isEmpty()) {
                return errList;
            }
            Files f = saveFileFromImDoc(dc, files);
            if (f == null) {
                err.setErrorCode("U000-0000-0000");
                err.setErrorName("Lỗi lưu Files");
                err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
                errList.add(err);
                return errList;
            }
            ImportDeviceFile idf = saveImpDeviceFile(dc, f);
            if (idf == null) {
                err.setErrorCode("U000-0000-0000");
                err.setErrorName("Lỗi lưu ImportDeviceFile");
                err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
                errList.add(err);
                return errList;
            }
            errList = saveListProducts(dc.getProducts(), f);
            if (!errList.isEmpty()) {
                return errList;
            }
            Boolean k;

            List<Attachment> lstAttach = dc.getAttachments();
            for (Attachment att : lstAttach) {
                k = hp.saveAttachmentFromNSW(att, f.getFileId(), Constants.OBJECT_TYPE.IMPORT_FILE_DEVICE_TYPE, dc.getNswFileCode(), "TTB");
                if (!k) {
                    err.setErrorCode("U000-0000-0000");
                    err.setErrorName("Lỗi tải file đính kèm phí");
                    err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
                    errList.add(err);
                    return errList;
                }
            }

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            err.setErrorCode(
                    "U000-0000-0000");
            err.setErrorName(
                    "Lỗi nhận hồ sơ");
            err.setSolution(
                    "Kiểm tra chỉ tiêu thông tin và hàm validate");
            errList.add(err);
        }
        return errList;
    }

    public String sendMS05(String code) throws Exception {
        try {
            Envelope send = makeEnvelopeSend("06", "05", code, code, code);
            Body bd = new Body();
            Content ct = new Content();
            FileAttach fa = new FileAttach();
            fa.setFileCode(code);
            ct.setFileAttach(fa);
            bd.setContent(ct);
            send.setBody(bd);
            VuTTBService moh = new VuTTBService();
            IVuTTBService client = moh.getBasicHttpBindingIVuTTBService();
            String evlrs = hp.ObjectToXml(send);
            String response = client.recevie(evlrs);
            Envelope enveloprs = hp.xmlToEnvelope(response);
            String function = enveloprs.getHeader().getSubject().getFunction();
            if ("06".equals(function)) {
                return enveloprs.getBody().getContent().getFileAttach().getFileByte();
            } else {
                return null;
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return null;
        }
    }
    
    public Content receiveMs_71(Envelope envelop, Files files) throws ParseException, IOException {
        Content ct = new Content();
        try {
           GetStatus getStatus = envelop.getBody().getContent().getGetStatus();
            String nswFileCode = getStatus.getNswFileCode();
            FilesDAOHE dao = new FilesDAOHE();
            String docType = Constants.CATEGORY_TYPE.IMPORT_DEVICE_OBJECT;
            
            WorkflowAPI w = new WorkflowAPI();
            Long docTypeId = w.getProcedureTypeIdByCode(docType);
            
            Files f = dao.findByNswCodeAndDoctype(nswFileCode, docTypeId); 
            if (f != null) {
                ReturnStatus status = new ReturnStatus();
                status.setNswFileCode(nswFileCode);
                status.setStatusCode(String.valueOf(f.getStatus()));
                String statusName = WorkflowAPI.getStatusName(f.getStatus());
                status.setStatusName(statusName);
                ct.setReturnStatus(status);
            }
        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            List<com.viettel.ws.BO.Error> errList = new ArrayList<>();
            com.viettel.ws.BO.Error err = new com.viettel.ws.BO.Error();
            err.setErrorCode("U000-0000-0000");
            err.setErrorName("Lỗi khi lấy trạng thái");
            err.setSolution("Kiểm tra chỉ tiêu thông tin và hàm validate");
            errList.add(err);
            ct.setErrorList(errList);
        }
        return ct;
    }
    
    

    
    public boolean SendMs_73(ImportDeviceFile f, String bookReceive) throws Exception { 
        Content ct = new Content();
        SendVoffice d = new SendVoffice();
        Body bd = new Body();
        Envelope send = hp.makeEnvelopeSend("73", "73", f.getNswFileCode(), f.getNswFileCode(), f.getNswFileCode());
        try {
            d.setNswFileCode(f.getNswFileCode());
            d.setBookReceive(bookReceive);
            ct.setSendVoffice(d);
            bd.setContent(ct);
            send.setBody(bd);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
        return hp.sendMs(send);
    }
    
    public boolean SendMs_74(ImportDeviceFile f, String bookSend) throws Exception { 
        Content ct = new Content();
        SendVoffice d = new SendVoffice();
        Body bd = new Body();
        Envelope send = hp.makeEnvelopeSend("74", "74", f.getNswFileCode(), f.getNswFileCode(), f.getNswFileCode());
        try {
            d.setNswFileCode(f.getNswFileCode());
            d.setBookSend(bookSend);
            ct.setSendVoffice(d);
            bd.setContent(ct);
            send.setBody(bd);

        } catch (Exception ex) {
            LogUtils.addLogDB(ex);
            return false;
        }
        return hp.sendMs(send);
    }
}
