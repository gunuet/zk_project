/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws;

import com.viettel.utils.ResourceBundleUtil;
import static com.viettel.voffice.framework.BaseHibernateDAOMDB.getSession;
import com.viettel.ws.BO.Envelope;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.hibernate.Session;

/**
 *
 * @author E5420
 */
@WebService(serviceName = "ImportedFoodService")
public class ImportedFoodService extends BaseWS {

    @Resource
    WebServiceContext wsContext;

    @WebMethod(operationName = "getMessageFromNSW")
    public String getMessageFromNSW(@WebParam(name = "name") String env) throws ParseException, IOException, Exception {
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        String Ip = req.getRemoteAddr();
//        String dm = req.getServerName();
//        String url = ResourceBundleUtil.getString("nsw_wsdl");
//        if (url.indexOf(Ip) < 0 || url.indexOf(dm) < 0) {
//            return "Người dùng không có quyền truy cập ";
//        }
        Helper hp = new Helper();
        return hp.receiveMs(env);
    }
}
