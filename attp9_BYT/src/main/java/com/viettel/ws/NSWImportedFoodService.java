/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws;

import com.viettel.module.importfood.uitls.IfdConstants;
import com.viettel.module.importfood.ws.*;
import com.viettel.module.importfood.ws.envelope.Envelope;
import com.viettel.module.importfood.ws.envelope.Error;
import com.viettel.module.importfood.ws.services.EnvelopeHelper;
import java.io.IOException;
import java.text.ParseException;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.ws.WebServiceContext;

/**
 *
 * @author E5420
 */
@WebService(serviceName = "NSWImportedFoodService")
public class NSWImportedFoodService extends BaseWS {

    @Resource
    WebServiceContext wsContext;
    
    private EnvelopeHelper envelopeService;  
    
    @WebMethod(operationName = "getMessageFromNSW")
    public String getMessageFromNSW(@WebParam(name = "name") String env) throws ParseException, IOException, Exception {
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        String Ip = req.getRemoteAddr();
//        String dm = req.getServerName();
//        String url = ResourceBundleUtil.getString("nsw_wsdl");
//        if (url.indexOf(Ip) < 0 || url.indexOf(dm) < 0) {
//            return "Người dùng không có quyền truy cập ";
//        }

        NSWReceiveHelper hp = new NSWReceiveHelper();
        envelopeService = new EnvelopeHelper();

        Error error;
        String xml = env;
        String xmlReturn = "";
        try {
            String documentType = envelopeService.getDocumentType(xml);
            String msgType = envelopeService.getValueFromXml(xml, "/Envelope/Header/Subject/type");
            String reference = envelopeService.getValueFromXml(xml, "/Envelope/Header/Subject/reference");
            if (documentType != null) {
                switch (documentType) {
                    
                    case IfdConstants.MohProceduce.THONG_THUONG: // Thu tuc kiem tra thong thuong
                    case IfdConstants.MohProceduce.CHAT: // Thu tuc chat
                        xmlReturn = hp.receiveMs(xml);
                        break;
                    default:
                        error = envelopeService.createError(IfdConstants.Error.ERR01_CODE, IfdConstants.Error.ERR01);
                        Envelope envEr = envelopeService.createEnvelopeError(reference, IfdConstants.MohProceduce.ERROR, msgType, error);
                        xmlReturn = hp.ObjectToXml(envEr);
                        break;
                }
            } else {
                error = envelopeService.createError(IfdConstants.Error.ERR02_CODE, IfdConstants.Error.ERR02);
                Envelope envl = envelopeService.createEnvelopeError(reference, IfdConstants.MohProceduce.ERROR, msgType, error);
                xmlReturn = hp.ObjectToXml(envl);
            }
        } catch (Exception ex) {
            error = envelopeService.createError(IfdConstants.Error.ERR03_CODE, IfdConstants.Error.ERR03, ex);
            Envelope envl = envelopeService.createEnvelopeError("00", IfdConstants.MohProceduce.ERROR, "00", error);
            xmlReturn = hp.ObjectToXml(envl);
        }

        
        return xmlReturn;
    }
}
