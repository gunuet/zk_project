/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.ws;

import java.io.IOException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author E5420
 */
@WebService(serviceName = "VofficeMohService")
public class VofficeMohService extends BaseWS  {

    @WebMethod(operationName = "receiveFromVofficeMoh")
    public String receiveFromVofficeMoh(@WebParam(name = "name") String elv) throws IOException, Exception {
        Helper_VofficeMoh hp= new Helper_VofficeMoh();       
        return hp.receiveMs(elv);
    }
}
