
package com.viettel.ws.client;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IMOHService", targetNamespace = "http://tempuri.org/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface IMOHService {


    /**
     * 
     */
    @WebMethod(operationName = "DoWork", action = "http://tempuri.org/IMOHService/DoWork")
    @RequestWrapper(localName = "DoWork", targetNamespace = "http://tempuri.org/", className = "com.viettel.ws.client.DoWork")
    @ResponseWrapper(localName = "DoWorkResponse", targetNamespace = "http://tempuri.org/", className = "com.viettel.ws.client.DoWorkResponse")
    public void doWork();

    /**
     * 
     * @param xmlData
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "Receive", action = "http://tempuri.org/IMOHService/Receive")
    @WebResult(name = "ReceiveResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "Receive", targetNamespace = "http://tempuri.org/", className = "com.viettel.ws.client.Receive")
    @ResponseWrapper(localName = "ReceiveResponse", targetNamespace = "http://tempuri.org/", className = "com.viettel.ws.client.ReceiveResponse")
    public String receive(
        @WebParam(name = "XmlData", targetNamespace = "http://tempuri.org/")
        String xmlData);

}
