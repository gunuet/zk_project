/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ws;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.util.PDFStreamEngine;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.TextPosition;

/**
 *
 * @author E5420
 */
public class findTexPos extends PDFTextStripper {

    private static int pageNo;
    static List<TextPosition> rs;
    static Boolean rt;
    private static char[] cfind;
    private static int[] st;
    int l = 0;

    public findTexPos()
            throws IOException {
        super.setSortByPosition(true);
    }

    public synchronized static float[] getpos(String sfind, String path) throws Exception {
        float height = 0;
        if (sfind == null || sfind.isEmpty()) {
            sfind = "<SI>";
        }
        cfind = sfind.toCharArray();
        st = new int[cfind.length + 1];
        rt = false;
        pageNo = 1;
        rs = new ArrayList<>();
        try {
            File input = new File(path);
            PDDocument document = PDDocument.load(input);
            findTexPos printer = new findTexPos();
            List allPages = document.getDocumentCatalog().getAllPages();
            for (int i = 0; i <= allPages.size() - 1; i++) {
                PDPage page = (PDPage) allPages.get(i);
                height = page.getMediaBox().getHeight();
                PDStream contents = page.getContents();
                if (contents != null) {
                    printer.processStream(page, page.findResources(), page.getContents().getStream());

                }

                if (rt) {
                    break;
                }
                pageNo++;
            }
            if (rs.size() > 0) {
                float y = rs.get(0).getY() > height - 100 ? height - 100 : rs.get(0).getY();
                y = rs.get(0).getY() < 100 ? 100 : y;
                float[] k = {rs.get(0).getX(), height - y, pageNo};
                return k;
            } else {
                return null;
            }
        } finally {
        }
    }

    private synchronized void resetSt(int toval) {
        for (int i = 0; i <= toval; i++) {
            if (i > 0) {
                st[i] = 0;
            }
        }
    }

    @Override
    protected synchronized void processTextPosition(TextPosition text) {
        if (rt) {
            return;
        }
        st[0]++;
        String tChar = text.getCharacter();
        if (cfind[0] == tChar.toCharArray()[0] && rs.isEmpty()) {
            resetSt(1);
            st[1] = st[0];
            l = 1;
            rs.add(text);
            if (cfind.length == 1) {
                rt = true;
            }
        } else {
            if (l < cfind.length) {
                if (cfind[l] == tChar.toCharArray()[0] && rs.size() == l) {
                    st[l + 1] = st[0];
                    rs.add(text);
                    l++;
                } else {
                    if (l > 1) {
                        resetSt(l);
                        rs = new ArrayList();
                        //rs.removeAll(rs);
                    }
                    l = 0;
                }
            } else {
                rt = true;
            }
        }
    }
}
