package com.viettel.core.sys.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Applications.class)
public abstract class Applications_ {

	public static volatile SingularAttribute<Applications, String> appName;
	public static volatile SingularAttribute<Applications, Long> appId;
	public static volatile SingularAttribute<Applications, String> lockDescription;
	public static volatile SingularAttribute<Applications, String> description;
	public static volatile SingularAttribute<Applications, String> appCode;
	public static volatile SingularAttribute<Applications, Short> status;

}

