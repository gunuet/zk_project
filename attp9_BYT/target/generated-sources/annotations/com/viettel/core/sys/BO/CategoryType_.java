package com.viettel.core.sys.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CategoryType.class)
public abstract class CategoryType_ {

	public static volatile SingularAttribute<CategoryType, Long> isSystem;
	public static volatile SingularAttribute<CategoryType, String> code;
	public static volatile SingularAttribute<CategoryType, Date> createdDate;
	public static volatile SingularAttribute<CategoryType, Long> updatedBy;
	public static volatile SingularAttribute<CategoryType, Long> createdBy;
	public static volatile SingularAttribute<CategoryType, Long> isDeptDepend;
	public static volatile SingularAttribute<CategoryType, String> name;
	public static volatile SingularAttribute<CategoryType, Long> categoryTypeId;
	public static volatile SingularAttribute<CategoryType, Date> updatedDate;
	public static volatile SingularAttribute<CategoryType, Long> isActive;

}

