package com.viettel.core.sys.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Category.class)
public abstract class Category_ {

	public static volatile SingularAttribute<Category, String> deptName;
	public static volatile SingularAttribute<Category, String> code;
	public static volatile SingularAttribute<Category, Long> updatedBy;
	public static volatile SingularAttribute<Category, Long> deptId;
	public static volatile SingularAttribute<Category, String> description;
	public static volatile SingularAttribute<Category, String> categoryTypeCode;
	public static volatile SingularAttribute<Category, Date> updatedDate;
	public static volatile SingularAttribute<Category, Long> isActive;
	public static volatile SingularAttribute<Category, Date> expiryDate;
	public static volatile SingularAttribute<Category, Long> isDefault;
	public static volatile SingularAttribute<Category, Date> createdDate;
	public static volatile SingularAttribute<Category, Long> createdBy;
	public static volatile SingularAttribute<Category, String> parentCode;
	public static volatile SingularAttribute<Category, Long> sortOrder;
	public static volatile SingularAttribute<Category, String> name;
	public static volatile SingularAttribute<Category, String> value;
	public static volatile SingularAttribute<Category, Long> categoryId;

}

