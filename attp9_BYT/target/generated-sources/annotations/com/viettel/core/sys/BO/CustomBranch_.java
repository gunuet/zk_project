package com.viettel.core.sys.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CustomBranch.class)
public abstract class CustomBranch_ {

	public static volatile SingularAttribute<CustomBranch, String> code;
	public static volatile SingularAttribute<CustomBranch, Date> createdDate;
	public static volatile SingularAttribute<CustomBranch, String> createdBy;
	public static volatile SingularAttribute<CustomBranch, String> provinceCode;
	public static volatile SingularAttribute<CustomBranch, String> name;
	public static volatile SingularAttribute<CustomBranch, Long> id;
	public static volatile SingularAttribute<CustomBranch, Long> isActive;

}

