package com.viettel.core.sys.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ErrorLog.class)
public abstract class ErrorLog_ {

	public static volatile SingularAttribute<ErrorLog, String> deptName;
	public static volatile SingularAttribute<ErrorLog, String> lineOfCode;
	public static volatile SingularAttribute<ErrorLog, Long> deptId;
	public static volatile SingularAttribute<ErrorLog, String> methodName;
	public static volatile SingularAttribute<ErrorLog, String> description;
	public static volatile SingularAttribute<ErrorLog, String> className;
	public static volatile SingularAttribute<ErrorLog, String> userName;
	public static volatile SingularAttribute<ErrorLog, Long> userId;
	public static volatile SingularAttribute<ErrorLog, Long> errorLogId;
	public static volatile SingularAttribute<ErrorLog, Date> createDate;

}

