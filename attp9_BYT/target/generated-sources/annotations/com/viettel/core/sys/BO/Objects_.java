package com.viettel.core.sys.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Objects.class)
public abstract class Objects_ {

	public static volatile SingularAttribute<Objects, Integer> ord;
	public static volatile SingularAttribute<Objects, String> objectCode;
	public static volatile SingularAttribute<Objects, Long> objectTypeId;
	public static volatile SingularAttribute<Objects, String> objectUrl;
	public static volatile SingularAttribute<Objects, Long> appId;
	public static volatile SingularAttribute<Objects, String> objectLevel;
	public static volatile SingularAttribute<Objects, String> objectName;
	public static volatile SingularAttribute<Objects, String> description;
	public static volatile SingularAttribute<Objects, Long> objectId;
	public static volatile SingularAttribute<Objects, Long> parentId;
	public static volatile SingularAttribute<Objects, Long> status;

}

