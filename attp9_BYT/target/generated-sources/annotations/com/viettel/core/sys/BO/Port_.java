package com.viettel.core.sys.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Port.class)
public abstract class Port_ {

	public static volatile SingularAttribute<Port, String> nationalCode;
	public static volatile SingularAttribute<Port, String> code;
	public static volatile SingularAttribute<Port, Date> createdDate;
	public static volatile SingularAttribute<Port, Long> createdBy;
	public static volatile SingularAttribute<Port, String> name;
	public static volatile SingularAttribute<Port, Long> id;
	public static volatile SingularAttribute<Port, Long> isActive;

}

