package com.viettel.core.sys.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductTarget.class)
public abstract class ProductTarget_ {

	public static volatile SingularAttribute<ProductTarget, String> code;
	public static volatile SingularAttribute<ProductTarget, Long> cost;
	public static volatile SingularAttribute<ProductTarget, Date> createdDate;
	public static volatile SingularAttribute<ProductTarget, Long> createdBy;
	public static volatile SingularAttribute<ProductTarget, String> name;
	public static volatile SingularAttribute<ProductTarget, String> testMethod;
	public static volatile SingularAttribute<ProductTarget, String> typeName;
	public static volatile SingularAttribute<ProductTarget, Long> id;
	public static volatile SingularAttribute<ProductTarget, Long> isActive;
	public static volatile SingularAttribute<ProductTarget, String> type;

}

