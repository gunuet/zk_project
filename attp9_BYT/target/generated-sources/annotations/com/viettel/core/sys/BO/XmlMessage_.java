package com.viettel.core.sys.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(XmlMessage.class)
public abstract class XmlMessage_ {

	public static volatile SingularAttribute<XmlMessage, String> msid;
	public static volatile SingularAttribute<XmlMessage, String> func;
	public static volatile SingularAttribute<XmlMessage, String> nswFileCode;
	public static volatile SingularAttribute<XmlMessage, Long> id;
	public static volatile SingularAttribute<XmlMessage, String> message;
	public static volatile SingularAttribute<XmlMessage, String> type;
	public static volatile SingularAttribute<XmlMessage, Date> createDate;

}

