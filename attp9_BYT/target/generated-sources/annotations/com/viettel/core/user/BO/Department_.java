package com.viettel.core.user.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Department.class)
public abstract class Department_ {

	public static volatile SingularAttribute<Department, String> deptName;
	public static volatile SingularAttribute<Department, String> address;
	public static volatile SingularAttribute<Department, Long> deptId;
	public static volatile SingularAttribute<Department, String> bankNo;
	public static volatile SingularAttribute<Department, String> telephone;
	public static volatile SingularAttribute<Department, Long> deptType;
	public static volatile SingularAttribute<Department, String> fax;
	public static volatile SingularAttribute<Department, String> deptCode;
	public static volatile SingularAttribute<Department, String> email;
	public static volatile SingularAttribute<Department, Long> parentId;
	public static volatile SingularAttribute<Department, Long> status;

}

