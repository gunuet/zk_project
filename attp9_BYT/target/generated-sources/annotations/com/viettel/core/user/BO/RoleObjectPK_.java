package com.viettel.core.user.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RoleObjectPK.class)
public abstract class RoleObjectPK_ {

	public static volatile SingularAttribute<RoleObjectPK, Long> roleId;
	public static volatile SingularAttribute<RoleObjectPK, Long> objectId;

}

