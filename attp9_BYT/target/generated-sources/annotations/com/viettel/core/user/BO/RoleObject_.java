package com.viettel.core.user.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RoleObject.class)
public abstract class RoleObject_ {

	public static volatile SingularAttribute<RoleObject, Long> roleId;
	public static volatile SingularAttribute<RoleObject, String> description;
	public static volatile SingularAttribute<RoleObject, Long> isActive;
	public static volatile SingularAttribute<RoleObject, Long> objectId;
	public static volatile SingularAttribute<RoleObject, RoleObjectPK> roleObjectPK;

}

