package com.viettel.core.user.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Users.class)
public abstract class Users_ {

	public static volatile SingularAttribute<Users, Date> birthday;
	public static volatile SingularAttribute<Users, String> deptName;
	public static volatile SingularAttribute<Users, String> posName;
	public static volatile SingularAttribute<Users, String> staffCode;
	public static volatile SingularAttribute<Users, Long> gender;
	public static volatile SingularAttribute<Users, String> ip;
	public static volatile SingularAttribute<Users, Long> deptId;
	public static volatile SingularAttribute<Users, Long> businessId;
	public static volatile SingularAttribute<Users, String> businessName;
	public static volatile SingularAttribute<Users, String> fullName;
	public static volatile SingularAttribute<Users, String> telephone;
	public static volatile SingularAttribute<Users, String> userName;
	public static volatile SingularAttribute<Users, String> idNumber;
	public static volatile SingularAttribute<Users, Long> userId;
	public static volatile SingularAttribute<Users, Long> posId;
	public static volatile SingularAttribute<Users, String> password;
	public static volatile SingularAttribute<Users, Short> passwordChanged;
	public static volatile SingularAttribute<Users, String> forgotPassword;
	public static volatile SingularAttribute<Users, Long> userType;
	public static volatile SingularAttribute<Users, Date> forgotPasswordExpire;
	public static volatile SingularAttribute<Users, Date> lastResetPassword;
	public static volatile SingularAttribute<Users, String> email;
	public static volatile SingularAttribute<Users, String> avartarPath;
	public static volatile SingularAttribute<Users, Long> status;

}

