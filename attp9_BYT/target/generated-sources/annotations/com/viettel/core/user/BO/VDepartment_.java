package com.viettel.core.user.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VDepartment.class)
public abstract class VDepartment_ {

	public static volatile SingularAttribute<VDepartment, String> deptName;
	public static volatile SingularAttribute<VDepartment, Long> deptLevel;
	public static volatile SingularAttribute<VDepartment, Long> deptId;
	public static volatile SingularAttribute<VDepartment, String> deptPath;

}

