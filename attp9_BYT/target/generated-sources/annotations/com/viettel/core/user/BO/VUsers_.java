package com.viettel.core.user.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VUsers.class)
public abstract class VUsers_ {

	public static volatile SingularAttribute<VUsers, Date> birthday;
	public static volatile SingularAttribute<VUsers, String> deptName;
	public static volatile SingularAttribute<VUsers, String> posName;
	public static volatile SingularAttribute<VUsers, String> staffCode;
	public static volatile SingularAttribute<VUsers, String> role;
	public static volatile SingularAttribute<VUsers, Long> gender;
	public static volatile SingularAttribute<VUsers, String> ip;
	public static volatile SingularAttribute<VUsers, Long> deptId;
	public static volatile SingularAttribute<VUsers, Long> businessId;
	public static volatile SingularAttribute<VUsers, String> businessName;
	public static volatile SingularAttribute<VUsers, String> fullName;
	public static volatile SingularAttribute<VUsers, String> telephone;
	public static volatile SingularAttribute<VUsers, String> userName;
	public static volatile SingularAttribute<VUsers, String> idNumber;
	public static volatile SingularAttribute<VUsers, Long> userId;
	public static volatile SingularAttribute<VUsers, Long> posId;
	public static volatile SingularAttribute<VUsers, String> password;
	public static volatile SingularAttribute<VUsers, Short> passwordChanged;
	public static volatile SingularAttribute<VUsers, Long> userType;
	public static volatile SingularAttribute<VUsers, Date> lastResetPassword;
	public static volatile SingularAttribute<VUsers, String> email;
	public static volatile SingularAttribute<VUsers, String> avartarPath;
	public static volatile SingularAttribute<VUsers, Long> status;

}

