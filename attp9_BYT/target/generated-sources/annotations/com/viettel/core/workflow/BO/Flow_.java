package com.viettel.core.workflow.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Flow.class)
public abstract class Flow_ {

	public static volatile SingularAttribute<Flow, String> deptName;
	public static volatile SingularAttribute<Flow, String> flowCode;
	public static volatile SingularAttribute<Flow, Long> isUsing;
	public static volatile SingularAttribute<Flow, Long> deptId;
	public static volatile SingularAttribute<Flow, String> description;
	public static volatile SingularAttribute<Flow, String> objectName;
	public static volatile SingularAttribute<Flow, Long> isActive;
	public static volatile SingularAttribute<Flow, Long> flowId;
	public static volatile SingularAttribute<Flow, String> flowName;
	public static volatile SingularAttribute<Flow, String> categoryName;
	public static volatile SingularAttribute<Flow, Long> objectId;
	public static volatile SingularAttribute<Flow, Long> categoryId;

}

