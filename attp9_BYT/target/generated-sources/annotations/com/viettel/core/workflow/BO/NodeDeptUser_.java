package com.viettel.core.workflow.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NodeDeptUser.class)
public abstract class NodeDeptUser_ {

	public static volatile SingularAttribute<NodeDeptUser, String> nodeName;
	public static volatile SingularAttribute<NodeDeptUser, String> deptName;
	public static volatile SingularAttribute<NodeDeptUser, String> posName;
	public static volatile SingularAttribute<NodeDeptUser, Long> deptLevel;
	public static volatile SingularAttribute<NodeDeptUser, Long> deptId;
	public static volatile SingularAttribute<NodeDeptUser, Long> nodeDeptUserId;
	public static volatile SingularAttribute<NodeDeptUser, String> userName;
	public static volatile SingularAttribute<NodeDeptUser, Long> userId;
	public static volatile SingularAttribute<NodeDeptUser, String> allias;
	public static volatile SingularAttribute<NodeDeptUser, Long> posId;
	public static volatile SingularAttribute<NodeDeptUser, Long> useAllias;
	public static volatile SingularAttribute<NodeDeptUser, Long> optionSelected;
	public static volatile SingularAttribute<NodeDeptUser, Long> processType;
	public static volatile SingularAttribute<NodeDeptUser, Long> nodeId;

}

