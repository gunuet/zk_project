package com.viettel.core.workflow.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NodeToNodePK.class)
public abstract class NodeToNodePK_ {

	public static volatile SingularAttribute<NodeToNodePK, Long> previousId;
	public static volatile SingularAttribute<NodeToNodePK, Long> nextId;

}

