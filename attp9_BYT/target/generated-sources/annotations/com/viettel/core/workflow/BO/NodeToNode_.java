package com.viettel.core.workflow.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NodeToNode.class)
public abstract class NodeToNode_ {

	public static volatile SingularAttribute<NodeToNode, Long> formId;
	public static volatile SingularAttribute<NodeToNode, Long> previousId;
	public static volatile SingularAttribute<NodeToNode, Long> nextId;
	public static volatile SingularAttribute<NodeToNode, String> action;
	public static volatile SingularAttribute<NodeToNode, Long> id;
	public static volatile SingularAttribute<NodeToNode, Long> isActive;
	public static volatile SingularAttribute<NodeToNode, Long> type;
	public static volatile SingularAttribute<NodeToNode, Long> status;

}

