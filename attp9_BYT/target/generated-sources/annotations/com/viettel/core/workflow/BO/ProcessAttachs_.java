package com.viettel.core.workflow.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProcessAttachs.class)
public abstract class ProcessAttachs_ {

	public static volatile SingularAttribute<ProcessAttachs, Long> processId;
	public static volatile SingularAttribute<ProcessAttachs, Long> attachId;
	public static volatile SingularAttribute<ProcessAttachs, Long> processAttachId;
	public static volatile SingularAttribute<ProcessAttachs, Long> status;

}

