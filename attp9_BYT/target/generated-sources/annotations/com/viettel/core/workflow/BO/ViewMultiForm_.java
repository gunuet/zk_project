package com.viettel.core.workflow.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ViewMultiForm.class)
public abstract class ViewMultiForm_ {

	public static volatile SingularAttribute<ViewMultiForm, String> formName;
	public static volatile SingularAttribute<ViewMultiForm, Long> id;
	public static volatile SingularAttribute<ViewMultiForm, Long> isActive;
	public static volatile SingularAttribute<ViewMultiForm, Long> fileType;
	public static volatile SingularAttribute<ViewMultiForm, Long> status;
	public static volatile SingularAttribute<ViewMultiForm, Date> createDate;

}

