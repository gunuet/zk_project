package com.viettel.module.BO.test;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TestBill.class)
public abstract class TestBill_ {

	public static volatile SingularAttribute<TestBill, String> gateIn;
	public static volatile SingularAttribute<TestBill, Date> dateCheck;
	public static volatile SingularAttribute<TestBill, String> billNumberMove;
	public static volatile SingularAttribute<TestBill, String> tesBillName;
	public static volatile SingularAttribute<TestBill, String> billNumberContract;
	public static volatile SingularAttribute<TestBill, String> organirationCheck;
	public static volatile SingularAttribute<TestBill, String> timeImport;
	public static volatile SingularAttribute<TestBill, String> testBillcode;
	public static volatile SingularAttribute<TestBill, String> customsBranch;
	public static volatile SingularAttribute<TestBill, String> billNumberBill;
	public static volatile SingularAttribute<TestBill, Long> testBillId;
	public static volatile SingularAttribute<TestBill, String> placeCheck;
	public static volatile SingularAttribute<TestBill, String> gateOut;

}

