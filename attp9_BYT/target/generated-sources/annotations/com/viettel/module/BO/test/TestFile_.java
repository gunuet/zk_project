package com.viettel.module.BO.test;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TestFile.class)
public abstract class TestFile_ {

	public static volatile SingularAttribute<TestFile, Date> dateIn;
	public static volatile SingularAttribute<TestFile, String> ownerCode;
	public static volatile SingularAttribute<TestFile, Long> testFileId;
	public static volatile SingularAttribute<TestFile, String> responseBussinessAddress;
	public static volatile SingularAttribute<TestFile, String> responseBussinessEmail;
	public static volatile SingularAttribute<TestFile, String> exportBussinessCode;
	public static volatile SingularAttribute<TestFile, String> ownerFax;
	public static volatile SingularAttribute<TestFile, String> testFileStatus;
	public static volatile SingularAttribute<TestFile, Long> ownerId;
	public static volatile SingularAttribute<TestFile, String> testFileCode;
	public static volatile SingularAttribute<TestFile, String> responseBussinessPhone;
	public static volatile SingularAttribute<TestFile, String> ownerName;
	public static volatile SingularAttribute<TestFile, String> exportBussinessAddress;
	public static volatile SingularAttribute<TestFile, String> exportBussinessPhone;
	public static volatile SingularAttribute<TestFile, String> responseBussinessName;
	public static volatile SingularAttribute<TestFile, String> testFileName;
	public static volatile SingularAttribute<TestFile, String> ownerPhone;
	public static volatile SingularAttribute<TestFile, Long> responseBussinessId;
	public static volatile SingularAttribute<TestFile, String> responseBussinessFax;
	public static volatile SingularAttribute<TestFile, BigInteger> exportBussinessId;
	public static volatile SingularAttribute<TestFile, String> ownerEmail;
	public static volatile SingularAttribute<TestFile, String> exportBussinessEmail;
	public static volatile SingularAttribute<TestFile, String> exportBussinessName;
	public static volatile SingularAttribute<TestFile, String> ownerAddress;
	public static volatile SingularAttribute<TestFile, String> exportBussinessFax;
	public static volatile SingularAttribute<TestFile, String> responseBussinessCode;

}

