package com.viettel.module.BO.test;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TestProduct.class)
public abstract class TestProduct_ {

	public static volatile SingularAttribute<TestProduct, Long> testProductId;
	public static volatile SingularAttribute<TestProduct, BigInteger> cost;
	public static volatile SingularAttribute<TestProduct, String> numberPublish;
	public static volatile SingularAttribute<TestProduct, BigInteger> numbers;
	public static volatile SingularAttribute<TestProduct, String> testProductCode;
	public static volatile SingularAttribute<TestProduct, String> madeIn;
	public static volatile SingularAttribute<TestProduct, String> testProductName;
	public static volatile SingularAttribute<TestProduct, String> productDepcription;

}

