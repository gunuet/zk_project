package com.viettel.module.BO.test;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TestingImportedFood.class)
public abstract class TestingImportedFood_ {

	public static volatile SingularAttribute<TestingImportedFood, String> deptName;
	public static volatile SingularAttribute<TestingImportedFood, String> exporterAddress;
	public static volatile SingularAttribute<TestingImportedFood, String> exporterEmail;
	public static volatile SingularAttribute<TestingImportedFood, Short> checkMethod;
	public static volatile SingularAttribute<TestingImportedFood, String> goodsOwnerAddress;
	public static volatile SingularAttribute<TestingImportedFood, String> goodsOwnerName;
	public static volatile SingularAttribute<TestingImportedFood, String> responsiblePersonAddress;
	public static volatile SingularAttribute<TestingImportedFood, String> importerGateCode;
	public static volatile SingularAttribute<TestingImportedFood, Short> isDeleted;
	public static volatile SingularAttribute<TestingImportedFood, String> exporterPhone;
	public static volatile SingularAttribute<TestingImportedFood, String> responsiblePersonPhone;
	public static volatile SingularAttribute<TestingImportedFood, String> checkPlace;
	public static volatile SingularAttribute<TestingImportedFood, String> responsiblePersonName;
	public static volatile SingularAttribute<TestingImportedFood, String> goodsOwnerPhone;
	public static volatile SingularAttribute<TestingImportedFood, String> goodsOwnerEmail;
	public static volatile SingularAttribute<TestingImportedFood, String> exporterGateCode;
	public static volatile SingularAttribute<TestingImportedFood, Date> comingDate;
	public static volatile SingularAttribute<TestingImportedFood, String> goodsOwnerFax;
	public static volatile SingularAttribute<TestingImportedFood, Date> createdDate;
	public static volatile SingularAttribute<TestingImportedFood, String> exporterGateName;
	public static volatile SingularAttribute<TestingImportedFood, Date> checkTime;
	public static volatile SingularAttribute<TestingImportedFood, String> createdBy;
	public static volatile SingularAttribute<TestingImportedFood, Long> nswFileCode;
	public static volatile SingularAttribute<TestingImportedFood, String> importerGateName;
	public static volatile SingularAttribute<TestingImportedFood, String> responsiblePersonEmail;
	public static volatile SingularAttribute<TestingImportedFood, Date> modifiedDate;
	public static volatile SingularAttribute<TestingImportedFood, String> exporterName;
	public static volatile SingularAttribute<TestingImportedFood, String> responsiblePersonFax;
	public static volatile SingularAttribute<TestingImportedFood, String> deptCode;
	public static volatile SingularAttribute<TestingImportedFood, Long> fileId;

}

