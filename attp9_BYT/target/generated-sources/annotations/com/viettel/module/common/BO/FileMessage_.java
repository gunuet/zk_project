package com.viettel.module.common.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FileMessage.class)
public abstract class FileMessage_ {

	public static volatile SingularAttribute<FileMessage, Long> createDeptId;
	public static volatile SingularAttribute<FileMessage, String> createDeptName;
	public static volatile SingularAttribute<FileMessage, Date> modifyDate;
	public static volatile SingularAttribute<FileMessage, Long> fileMessageId;
	public static volatile SingularAttribute<FileMessage, Long> creatorId;
	public static volatile SingularAttribute<FileMessage, String> creatorName;
	public static volatile SingularAttribute<FileMessage, String> message;
	public static volatile SingularAttribute<FileMessage, Long> isActive;
	public static volatile SingularAttribute<FileMessage, Long> fileId;
	public static volatile SingularAttribute<FileMessage, Date> createDate;

}

