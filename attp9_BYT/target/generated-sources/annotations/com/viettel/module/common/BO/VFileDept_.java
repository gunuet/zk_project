package com.viettel.module.common.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFileDept.class)
public abstract class VFileDept_ {

	public static volatile SingularAttribute<VFileDept, Long> createDeptId;
	public static volatile SingularAttribute<VFileDept, String> fileName;
	public static volatile SingularAttribute<VFileDept, Long> flagView;
	public static volatile SingularAttribute<VFileDept, Long> businessId;
	public static volatile SingularAttribute<VFileDept, String> businessName;
	public static volatile SingularAttribute<VFileDept, Long> creatorId;
	public static volatile SingularAttribute<VFileDept, String> creatorName;
	public static volatile SingularAttribute<VFileDept, Long> isActive;
	public static volatile SingularAttribute<VFileDept, Long> parentFileId;
	public static volatile SingularAttribute<VFileDept, Long> numDayProcess;
	public static volatile SingularAttribute<VFileDept, String> statusName;
	public static volatile SingularAttribute<VFileDept, String> fileTypeName;
	public static volatile SingularAttribute<VFileDept, String> businessAddress;
	public static volatile SingularAttribute<VFileDept, Date> deadline;
	public static volatile SingularAttribute<VFileDept, Long> flowId;
	public static volatile SingularAttribute<VFileDept, Date> createDate;
	public static volatile SingularAttribute<VFileDept, String> businessEmail;
	public static volatile SingularAttribute<VFileDept, String> createDeptName;
	public static volatile SingularAttribute<VFileDept, Date> modifyDate;
	public static volatile SingularAttribute<VFileDept, String> fileCode;
	public static volatile SingularAttribute<VFileDept, String> fileDate;
	public static volatile SingularAttribute<VFileDept, String> taxCode;
	public static volatile SingularAttribute<VFileDept, Long> documentTypeCode;
	public static volatile SingularAttribute<VFileDept, String> nswFileCode;
	public static volatile SingularAttribute<VFileDept, Short> isTemp;
	public static volatile SingularAttribute<VFileDept, Long> deptTestingId;
	public static volatile SingularAttribute<VFileDept, String> businessFax;
	public static volatile SingularAttribute<VFileDept, String> businessPhone;
	public static volatile SingularAttribute<VFileDept, Long> fileType;
	public static volatile SingularAttribute<VFileDept, Date> startDate;
	public static volatile SingularAttribute<VFileDept, Long> fileId;
	public static volatile SingularAttribute<VFileDept, Long> status;

}

