package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Annexe.class)
public abstract class Annexe_ {

	public static volatile SingularAttribute<Annexe, String> refNo;
	public static volatile SingularAttribute<Annexe, String> note;
	public static volatile SingularAttribute<Annexe, Long> annexeType;
	public static volatile SingularAttribute<Annexe, String> substance;
	public static volatile SingularAttribute<Annexe, String> conditionsOfUse;
	public static volatile SingularAttribute<Annexe, Date> allowedUntil;
	public static volatile SingularAttribute<Annexe, String> annexeTypeName;
	public static volatile SingularAttribute<Annexe, Long> isActive;
	public static volatile SingularAttribute<Annexe, String> casNumber;
	public static volatile SingularAttribute<Annexe, String> fieldUse;
	public static volatile SingularAttribute<Annexe, Long> annexeId;
	public static volatile SingularAttribute<Annexe, String> maximumAuthorized;
	public static volatile SingularAttribute<Annexe, String> limitationRequirements;

}

