package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CaUser.class)
public abstract class CaUser_ {

	public static volatile SingularAttribute<CaUser, Date> createdAt;
	public static volatile SingularAttribute<CaUser, String> caSerial;
	public static volatile SingularAttribute<CaUser, String> signature;
	public static volatile SingularAttribute<CaUser, Long> caId;
	public static volatile SingularAttribute<CaUser, String> stamper;
	public static volatile SingularAttribute<CaUser, Long> isActive;
	public static volatile SingularAttribute<CaUser, Long> userId;
	public static volatile SingularAttribute<CaUser, Date> updatedAt;

}

