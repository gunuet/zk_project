package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosAdditionalRequest.class)
public abstract class CosAdditionalRequest_ {

	public static volatile SingularAttribute<CosAdditionalRequest, Long> createDeptId;
	public static volatile SingularAttribute<CosAdditionalRequest, String> createDeptName;
	public static volatile SingularAttribute<CosAdditionalRequest, Long> creatorId;
	public static volatile SingularAttribute<CosAdditionalRequest, String> creatorName;
	public static volatile SingularAttribute<CosAdditionalRequest, Date> receiveDate;
	public static volatile SingularAttribute<CosAdditionalRequest, Long> isActive;
	public static volatile SingularAttribute<CosAdditionalRequest, Long> version;
	public static volatile SingularAttribute<CosAdditionalRequest, String> content;
	public static volatile SingularAttribute<CosAdditionalRequest, String> receiveNo;
	public static volatile SingularAttribute<CosAdditionalRequest, Long> additionalRequestId;
	public static volatile SingularAttribute<CosAdditionalRequest, Long> commentType;
	public static volatile SingularAttribute<CosAdditionalRequest, Long> attachId;
	public static volatile SingularAttribute<CosAdditionalRequest, Date> createDate;
	public static volatile SingularAttribute<CosAdditionalRequest, Long> fileId;
	public static volatile SingularAttribute<CosAdditionalRequest, Long> status;

}

