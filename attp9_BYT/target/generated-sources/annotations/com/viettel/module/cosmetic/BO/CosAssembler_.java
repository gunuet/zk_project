package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosAssembler.class)
public abstract class CosAssembler_ {

	public static volatile SingularAttribute<CosAssembler, String> address;
	public static volatile SingularAttribute<CosAssembler, String> phone;
	public static volatile SingularAttribute<CosAssembler, Long> assemblerId;
	public static volatile SingularAttribute<CosAssembler, String> name;
	public static volatile SingularAttribute<CosAssembler, Long> assemblerSub;
	public static volatile SingularAttribute<CosAssembler, String> fax;
	public static volatile SingularAttribute<CosAssembler, Long> isActive;
	public static volatile SingularAttribute<CosAssembler, Long> assemblerMain;
	public static volatile SingularAttribute<CosAssembler, Long> cosFileId;

}

