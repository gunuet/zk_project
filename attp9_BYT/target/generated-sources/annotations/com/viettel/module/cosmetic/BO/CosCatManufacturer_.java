package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosCatManufacturer.class)
public abstract class CosCatManufacturer_ {

	public static volatile SingularAttribute<CosCatManufacturer, String> address;
	public static volatile SingularAttribute<CosCatManufacturer, String> nameValid;
	public static volatile SingularAttribute<CosCatManufacturer, String> phone;
	public static volatile SingularAttribute<CosCatManufacturer, String> name;
	public static volatile SingularAttribute<CosCatManufacturer, String> fax;
	public static volatile SingularAttribute<CosCatManufacturer, Long> isActive;
	public static volatile SingularAttribute<CosCatManufacturer, Long> userId;
	public static volatile SingularAttribute<CosCatManufacturer, Long> catManufacturerId;

}

