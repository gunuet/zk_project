package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosCosfileIngre.class)
public abstract class CosCosfileIngre_ {

	public static volatile SingularAttribute<CosCosfileIngre, Long> ingredientId;
	public static volatile SingularAttribute<CosCosfileIngre, String> variantOrShade;
	public static volatile SingularAttribute<CosCosfileIngre, String> ingredientName;
	public static volatile SingularAttribute<CosCosfileIngre, String> description;
	public static volatile SingularAttribute<CosCosfileIngre, Long> isActive;
	public static volatile SingularAttribute<CosCosfileIngre, Long> cosFileId;
	public static volatile SingularAttribute<CosCosfileIngre, Double> percent;
	public static volatile SingularAttribute<CosCosfileIngre, Long> cosfileIngreId;

}

