package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosEvaluationRecord.class)
public abstract class CosEvaluationRecord_ {

	public static volatile SingularAttribute<CosEvaluationRecord, String> nameproductContent;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> nameproduct;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> criteria;
	public static volatile SingularAttribute<CosEvaluationRecord, String> criteriaContent;
	public static volatile SingularAttribute<CosEvaluationRecord, String> businessName;
	public static volatile SingularAttribute<CosEvaluationRecord, String> userName;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> isActive;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> userId;
	public static volatile SingularAttribute<CosEvaluationRecord, String> mechanismContent;
	public static volatile SingularAttribute<CosEvaluationRecord, String> productName;
	public static volatile SingularAttribute<CosEvaluationRecord, String> legalContent;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> processId;
	public static volatile SingularAttribute<CosEvaluationRecord, String> mainContent;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> legal;
	public static volatile SingularAttribute<CosEvaluationRecord, String> businessAddress;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> attachId;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> mechanism;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> evaluationRecordId;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> fileType;
	public static volatile SingularAttribute<CosEvaluationRecord, Date> createDate;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> fileId;
	public static volatile SingularAttribute<CosEvaluationRecord, Long> status;

}

