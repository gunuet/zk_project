package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosFile.class)
public abstract class CosFile_ {

	public static volatile SingularAttribute<CosFile, String> signPlace;
	public static volatile SingularAttribute<CosFile, String> importerPhone;
	public static volatile SingularAttribute<CosFile, String> otherProductPresentation;
	public static volatile SingularAttribute<CosFile, String> distributorPhone;
	public static volatile SingularAttribute<CosFile, Long> tested;
	public static volatile SingularAttribute<CosFile, Long> extensionNo;
	public static volatile SingularAttribute<CosFile, String> attachmentInfos;
	public static volatile SingularAttribute<CosFile, String> importerFax;
	public static volatile SingularAttribute<CosFile, String> otherProductType;
	public static volatile SingularAttribute<CosFile, String> productPresentation;
	public static volatile SingularAttribute<CosFile, Date> signDate;
	public static volatile SingularAttribute<CosFile, String> productName;
	public static volatile SingularAttribute<CosFile, String> distributePersonEmail;
	public static volatile SingularAttribute<CosFile, String> distributorName;
	public static volatile SingularAttribute<CosFile, String> distributePersonName;
	public static volatile SingularAttribute<CosFile, Long> roled;
	public static volatile SingularAttribute<CosFile, String> importerName;
	public static volatile SingularAttribute<CosFile, String> listVariantOrShade;
	public static volatile SingularAttribute<CosFile, String> distributorRegisNumber;
	public static volatile SingularAttribute<CosFile, String> dateEffect;
	public static volatile SingularAttribute<CosFile, String> intendedUse;
	public static volatile SingularAttribute<CosFile, String> cosmeticNo;
	public static volatile SingularAttribute<CosFile, String> productType;
	public static volatile SingularAttribute<CosFile, String> distributorAddress;
	public static volatile SingularAttribute<CosFile, String> brandName;
	public static volatile SingularAttribute<CosFile, String> signName;
	public static volatile SingularAttribute<CosFile, String> distributePersonPosition;
	public static volatile SingularAttribute<CosFile, Long> cosFileId;
	public static volatile SingularAttribute<CosFile, String> distributorFax;
	public static volatile SingularAttribute<CosFile, Long> documentTypeCode;
	public static volatile SingularAttribute<CosFile, String> circulatingNo;
	public static volatile SingularAttribute<CosFile, String> contents;
	public static volatile SingularAttribute<CosFile, String> nswFileCode;
	public static volatile SingularAttribute<CosFile, Long> ingreConfirm2;
	public static volatile SingularAttribute<CosFile, Long> ingreConfirm1;
	public static volatile SingularAttribute<CosFile, String> importerAddress;
	public static volatile SingularAttribute<CosFile, String> distributePersonPhone;
	public static volatile SingularAttribute<CosFile, Long> fileId;
	public static volatile SingularAttribute<CosFile, Date> dateIssue;

}

