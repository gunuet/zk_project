package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosIngredient.class)
public abstract class CosIngredient_ {

	public static volatile SingularAttribute<CosIngredient, Long> ingredientId;
	public static volatile SingularAttribute<CosIngredient, String> code;
	public static volatile SingularAttribute<CosIngredient, String> name;
	public static volatile SingularAttribute<CosIngredient, Long> isActive;

}

