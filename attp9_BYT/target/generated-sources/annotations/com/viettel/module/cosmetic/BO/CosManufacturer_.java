package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosManufacturer.class)
public abstract class CosManufacturer_ {

	public static volatile SingularAttribute<CosManufacturer, String> address;
	public static volatile SingularAttribute<CosManufacturer, String> phone;
	public static volatile SingularAttribute<CosManufacturer, Long> manufacturerId;
	public static volatile SingularAttribute<CosManufacturer, String> name;
	public static volatile SingularAttribute<CosManufacturer, String> fax;
	public static volatile SingularAttribute<CosManufacturer, Long> isActive;
	public static volatile SingularAttribute<CosManufacturer, Long> cosFileId;

}

