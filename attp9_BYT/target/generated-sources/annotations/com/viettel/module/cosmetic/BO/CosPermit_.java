package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosPermit.class)
public abstract class CosPermit_ {

	public static volatile SingularAttribute<CosPermit, Long> permitId;
	public static volatile SingularAttribute<CosPermit, String> businessName;
	public static volatile SingularAttribute<CosPermit, Long> businessId;
	public static volatile SingularAttribute<CosPermit, Date> receiveDate;
	public static volatile SingularAttribute<CosPermit, String> telephone;
	public static volatile SingularAttribute<CosPermit, Date> signDate;
	public static volatile SingularAttribute<CosPermit, Long> isActive;
	public static volatile SingularAttribute<CosPermit, String> productName;
	public static volatile SingularAttribute<CosPermit, String> signerName;
	public static volatile SingularAttribute<CosPermit, String> receiveNo;
	public static volatile SingularAttribute<CosPermit, String> receiveDeptName;
	public static volatile SingularAttribute<CosPermit, String> fax;
	public static volatile SingularAttribute<CosPermit, Long> attachId;
	public static volatile SingularAttribute<CosPermit, String> email;
	public static volatile SingularAttribute<CosPermit, Long> fileId;
	public static volatile SingularAttribute<CosPermit, Long> status;

}

