package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosProductPresentaton.class)
public abstract class CosProductPresentaton_ {

	public static volatile SingularAttribute<CosProductPresentaton, String> nameVi;
	public static volatile SingularAttribute<CosProductPresentaton, Long> productPresentationId;
	public static volatile SingularAttribute<CosProductPresentaton, String> nameEn;
	public static volatile SingularAttribute<CosProductPresentaton, Long> isActive;
	public static volatile SingularAttribute<CosProductPresentaton, Long> isDifferent;

}

