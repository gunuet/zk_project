package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosProductTypeSub.class)
public abstract class CosProductTypeSub_ {

	public static volatile SingularAttribute<CosProductTypeSub, Long> ord;
	public static volatile SingularAttribute<CosProductTypeSub, String> nameVi;
	public static volatile SingularAttribute<CosProductTypeSub, Long> productTypeSubId;
	public static volatile SingularAttribute<CosProductTypeSub, String> nameEn;
	public static volatile SingularAttribute<CosProductTypeSub, Long> isActive;
	public static volatile SingularAttribute<CosProductTypeSub, Long> productTypeId;

}

