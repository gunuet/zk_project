package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosProductType.class)
public abstract class CosProductType_ {

	public static volatile SingularAttribute<CosProductType, Long> ord;
	public static volatile SingularAttribute<CosProductType, String> name;
	public static volatile SingularAttribute<CosProductType, Long> isActive;
	public static volatile SingularAttribute<CosProductType, Long> productTypeId;

}

