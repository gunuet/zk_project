package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CosReject.class)
public abstract class CosReject_ {

	public static volatile SingularAttribute<CosReject, Long> rejectId;
	public static volatile SingularAttribute<CosReject, String> businessName;
	public static volatile SingularAttribute<CosReject, Long> businessId;
	public static volatile SingularAttribute<CosReject, Date> receiveDate;
	public static volatile SingularAttribute<CosReject, String> telephone;
	public static volatile SingularAttribute<CosReject, Date> signDate;
	public static volatile SingularAttribute<CosReject, Long> isActive;
	public static volatile SingularAttribute<CosReject, String> productName;
	public static volatile SingularAttribute<CosReject, String> signerName;
	public static volatile SingularAttribute<CosReject, String> receiveNo;
	public static volatile SingularAttribute<CosReject, String> receiveDeptName;
	public static volatile SingularAttribute<CosReject, String> fax;
	public static volatile SingularAttribute<CosReject, Long> attachId;
	public static volatile SingularAttribute<CosReject, String> email;
	public static volatile SingularAttribute<CosReject, Long> fileId;
	public static volatile SingularAttribute<CosReject, Long> status;

}

