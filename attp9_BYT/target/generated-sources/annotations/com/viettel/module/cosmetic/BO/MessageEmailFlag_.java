package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MessageEmailFlag.class)
public abstract class MessageEmailFlag_ {

	public static volatile SingularAttribute<MessageEmailFlag, Short> flag;

}

