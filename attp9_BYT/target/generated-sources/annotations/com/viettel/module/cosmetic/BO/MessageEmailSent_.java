package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MessageEmailSent.class)
public abstract class MessageEmailSent_ {

	public static volatile SingularAttribute<MessageEmailSent, Long> senderId;
	public static volatile SingularAttribute<MessageEmailSent, Short> msgType;
	public static volatile SingularAttribute<MessageEmailSent, Long> messageId;
	public static volatile SingularAttribute<MessageEmailSent, String> receiveEmail;
	public static volatile SingularAttribute<MessageEmailSent, String> content;
	public static volatile SingularAttribute<MessageEmailSent, Date> sendTime;

}

