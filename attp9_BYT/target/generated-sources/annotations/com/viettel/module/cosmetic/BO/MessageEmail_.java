package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MessageEmail.class)
public abstract class MessageEmail_ {

	public static volatile SingularAttribute<MessageEmail, Long> senderId;
	public static volatile SingularAttribute<MessageEmail, Long> msgType;
	public static volatile SingularAttribute<MessageEmail, Date> sentTimeReq;
	public static volatile SingularAttribute<MessageEmail, Long> messageId;
	public static volatile SingularAttribute<MessageEmail, String> receiveEmail;
	public static volatile SingularAttribute<MessageEmail, Long> isSent;
	public static volatile SingularAttribute<MessageEmail, Long> sendCount;
	public static volatile SingularAttribute<MessageEmail, String> content;
	public static volatile SingularAttribute<MessageEmail, Date> sendTime;
	public static volatile SingularAttribute<MessageEmail, String> errorMsg;

}

