package com.viettel.module.cosmetic.BO;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MessageSmsSent.class)
public abstract class MessageSmsSent_ {

	public static volatile SingularAttribute<MessageSmsSent, String> deptName;
	public static volatile SingularAttribute<MessageSmsSent, BigInteger> senderId;
	public static volatile SingularAttribute<MessageSmsSent, BigInteger> msgType;
	public static volatile SingularAttribute<MessageSmsSent, String> phoneNumber;
	public static volatile SingularAttribute<MessageSmsSent, BigDecimal> messageId;
	public static volatile SingularAttribute<MessageSmsSent, Date> sentTime;
	public static volatile SingularAttribute<MessageSmsSent, String> userName;
	public static volatile SingularAttribute<MessageSmsSent, BigInteger> userId;
	public static volatile SingularAttribute<MessageSmsSent, String> content;
	public static volatile SingularAttribute<MessageSmsSent, BigInteger> parentId;

}

