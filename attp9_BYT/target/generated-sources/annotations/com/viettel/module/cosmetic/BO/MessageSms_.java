package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MessageSms.class)
public abstract class MessageSms_ {

	public static volatile SingularAttribute<MessageSms, String> deptName;
	public static volatile SingularAttribute<MessageSms, Long> msgType;
	public static volatile SingularAttribute<MessageSms, Long> messageId;
	public static volatile SingularAttribute<MessageSms, Long> isSent;
	public static volatile SingularAttribute<MessageSms, String> userName;
	public static volatile SingularAttribute<MessageSms, Long> sendCount;
	public static volatile SingularAttribute<MessageSms, Long> userId;
	public static volatile SingularAttribute<MessageSms, String> content;
	public static volatile SingularAttribute<MessageSms, Long> parentId;
	public static volatile SingularAttribute<MessageSms, String> errorMsg;
	public static volatile SingularAttribute<MessageSms, Long> senderId;
	public static volatile SingularAttribute<MessageSms, String> phoneNumber;
	public static volatile SingularAttribute<MessageSms, Date> sentTimeReq;
	public static volatile SingularAttribute<MessageSms, Long> broadcastId;
	public static volatile SingularAttribute<MessageSms, Date> sentTime;

}

