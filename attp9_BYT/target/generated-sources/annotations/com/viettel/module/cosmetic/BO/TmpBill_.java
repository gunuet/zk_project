package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TmpBill.class)
public abstract class TmpBill_ {

	public static volatile SingularAttribute<TmpBill, String> code;
	public static volatile SingularAttribute<TmpBill, String> productCode;
	public static volatile SingularAttribute<TmpBill, String> price;
	public static volatile SingularAttribute<TmpBill, String> businessName;
	public static volatile SingularAttribute<TmpBill, Date> billDate;
	public static volatile SingularAttribute<TmpBill, String> type;
	public static volatile SingularAttribute<TmpBill, String> billNo;
	public static volatile SingularAttribute<TmpBill, Long> isLastImp;
	public static volatile SingularAttribute<TmpBill, Long> tmpBillId;
	public static volatile SingularAttribute<TmpBill, Long> status;

}

