package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFileCosfile.class)
public abstract class VFileCosfile_ {

	public static volatile SingularAttribute<VFileCosfile, String> fileName;
	public static volatile SingularAttribute<VFileCosfile, String> otherProductPresentation;
	public static volatile SingularAttribute<VFileCosfile, String> distributorPhone;
	public static volatile SingularAttribute<VFileCosfile, Long> tested;
	public static volatile SingularAttribute<VFileCosfile, String> attachmentInfos;
	public static volatile SingularAttribute<VFileCosfile, Long> businessId;
	public static volatile SingularAttribute<VFileCosfile, String> businessName;
	public static volatile SingularAttribute<VFileCosfile, String> otherProductType;
	public static volatile SingularAttribute<VFileCosfile, Date> signDate;
	public static volatile SingularAttribute<VFileCosfile, Long> isActive;
	public static volatile SingularAttribute<VFileCosfile, String> productName;
	public static volatile SingularAttribute<VFileCosfile, String> distributePersonEmail;
	public static volatile SingularAttribute<VFileCosfile, String> distributePersonName;
	public static volatile SingularAttribute<VFileCosfile, Long> parentFileId;
	public static volatile SingularAttribute<VFileCosfile, String> distributorRegisNumber;
	public static volatile SingularAttribute<VFileCosfile, String> fileTypeName;
	public static volatile SingularAttribute<VFileCosfile, String> businessAddress;
	public static volatile SingularAttribute<VFileCosfile, String> distributorAddress;
	public static volatile SingularAttribute<VFileCosfile, String> brandName;
	public static volatile SingularAttribute<VFileCosfile, Date> modifyDate;
	public static volatile SingularAttribute<VFileCosfile, String> fileCode;
	public static volatile SingularAttribute<VFileCosfile, String> distributePersonPosition;
	public static volatile SingularAttribute<VFileCosfile, String> taxCode;
	public static volatile SingularAttribute<VFileCosfile, Long> version;
	public static volatile SingularAttribute<VFileCosfile, Long> documentTypeCode;
	public static volatile SingularAttribute<VFileCosfile, String> contents;
	public static volatile SingularAttribute<VFileCosfile, Long> ingreConfirm2;
	public static volatile SingularAttribute<VFileCosfile, Long> isTemp;
	public static volatile SingularAttribute<VFileCosfile, Long> ingreConfirm1;
	public static volatile SingularAttribute<VFileCosfile, String> distributePersonPhone;
	public static volatile SingularAttribute<VFileCosfile, String> businessFax;
	public static volatile SingularAttribute<VFileCosfile, String> businessPhone;
	public static volatile SingularAttribute<VFileCosfile, Date> startDate;
	public static volatile SingularAttribute<VFileCosfile, Long> fileId;
	public static volatile SingularAttribute<VFileCosfile, Date> dateIssue;
	public static volatile SingularAttribute<VFileCosfile, Long> status;
	public static volatile SingularAttribute<VFileCosfile, String> signPlace;
	public static volatile SingularAttribute<VFileCosfile, Long> createDeptId;
	public static volatile SingularAttribute<VFileCosfile, String> importerPhone;
	public static volatile SingularAttribute<VFileCosfile, Long> extensionNo;
	public static volatile SingularAttribute<VFileCosfile, Long> creatorId;
	public static volatile SingularAttribute<VFileCosfile, String> creatorName;
	public static volatile SingularAttribute<VFileCosfile, String> importerFax;
	public static volatile SingularAttribute<VFileCosfile, String> productPresentation;
	public static volatile SingularAttribute<VFileCosfile, String> distributorName;
	public static volatile SingularAttribute<VFileCosfile, Long> roled;
	public static volatile SingularAttribute<VFileCosfile, String> importerName;
	public static volatile SingularAttribute<VFileCosfile, String> dateEffect;
	public static volatile SingularAttribute<VFileCosfile, Long> numDayProcess;
	public static volatile SingularAttribute<VFileCosfile, String> intendedUse;
	public static volatile SingularAttribute<VFileCosfile, Date> deadline;
	public static volatile SingularAttribute<VFileCosfile, String> cosmeticNo;
	public static volatile SingularAttribute<VFileCosfile, String> productType;
	public static volatile SingularAttribute<VFileCosfile, Date> createDate;
	public static volatile SingularAttribute<VFileCosfile, String> createDeptName;
	public static volatile SingularAttribute<VFileCosfile, String> signName;
	public static volatile SingularAttribute<VFileCosfile, Long> cosFileId;
	public static volatile SingularAttribute<VFileCosfile, String> distributorFax;
	public static volatile SingularAttribute<VFileCosfile, String> circulatingNo;
	public static volatile SingularAttribute<VFileCosfile, String> nswFileCode;
	public static volatile SingularAttribute<VFileCosfile, String> importerAddress;
	public static volatile SingularAttribute<VFileCosfile, Long> fileType;

}

