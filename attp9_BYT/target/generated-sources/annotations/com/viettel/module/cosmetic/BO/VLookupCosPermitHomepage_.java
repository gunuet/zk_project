package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VLookupCosPermitHomepage.class)
public abstract class VLookupCosPermitHomepage_ {

	public static volatile SingularAttribute<VLookupCosPermitHomepage, String> receiveNo;
	public static volatile SingularAttribute<VLookupCosPermitHomepage, String> businessName;
	public static volatile SingularAttribute<VLookupCosPermitHomepage, Date> receiveDate;
	public static volatile SingularAttribute<VLookupCosPermitHomepage, String> businessAddress;
	public static volatile SingularAttribute<VLookupCosPermitHomepage, Long> cosFileId;
	public static volatile SingularAttribute<VLookupCosPermitHomepage, String> productName;
	public static volatile SingularAttribute<VLookupCosPermitHomepage, Long> status;
	public static volatile SingularAttribute<VLookupCosPermitHomepage, Long> fileId;

}

