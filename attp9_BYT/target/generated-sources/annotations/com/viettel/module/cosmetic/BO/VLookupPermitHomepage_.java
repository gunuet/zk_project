package com.viettel.module.cosmetic.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VLookupPermitHomepage.class)
public abstract class VLookupPermitHomepage_ {

	public static volatile SingularAttribute<VLookupPermitHomepage, String> receiveNo;
	public static volatile SingularAttribute<VLookupPermitHomepage, String> businessName;
	public static volatile SingularAttribute<VLookupPermitHomepage, String> businessAddress;
	public static volatile SingularAttribute<VLookupPermitHomepage, String> productName;
	public static volatile SingularAttribute<VLookupPermitHomepage, Date> effectiveDate;

}

