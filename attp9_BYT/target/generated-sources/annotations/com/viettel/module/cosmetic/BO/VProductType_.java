package com.viettel.module.cosmetic.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VProductType.class)
public abstract class VProductType_ {

	public static volatile SingularAttribute<VProductType, Long> ord;
	public static volatile SingularAttribute<VProductType, String> nameVi;
	public static volatile SingularAttribute<VProductType, String> name;
	public static volatile SingularAttribute<VProductType, String> nameEn;
	public static volatile SingularAttribute<VProductType, Long> isActive;
	public static volatile SingularAttribute<VProductType, Long> productTypeId;

}

