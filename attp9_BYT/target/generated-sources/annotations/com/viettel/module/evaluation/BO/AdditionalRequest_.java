package com.viettel.module.evaluation.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdditionalRequest.class)
public abstract class AdditionalRequest_ {

	public static volatile SingularAttribute<AdditionalRequest, Long> createDeptId;
	public static volatile SingularAttribute<AdditionalRequest, String> createDeptName;
	public static volatile SingularAttribute<AdditionalRequest, Long> requestType;
	public static volatile SingularAttribute<AdditionalRequest, Long> creatorId;
	public static volatile SingularAttribute<AdditionalRequest, String> creatorName;
	public static volatile SingularAttribute<AdditionalRequest, Long> isActive;
	public static volatile SingularAttribute<AdditionalRequest, Long> version;
	public static volatile SingularAttribute<AdditionalRequest, String> content;
	public static volatile SingularAttribute<AdditionalRequest, Long> additionalRequestId;
	public static volatile SingularAttribute<AdditionalRequest, Long> attachId;
	public static volatile SingularAttribute<AdditionalRequest, Long> contentType;
	public static volatile SingularAttribute<AdditionalRequest, Long> fileId;
	public static volatile SingularAttribute<AdditionalRequest, Date> createDate;
	public static volatile SingularAttribute<AdditionalRequest, Long> status;

}

