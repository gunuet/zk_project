package com.viettel.module.evaluation.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EvaluationRecord.class)
public abstract class EvaluationRecord_ {

	public static volatile SingularAttribute<EvaluationRecord, Long> evalType;
	public static volatile SingularAttribute<EvaluationRecord, Long> creatorId;
	public static volatile SingularAttribute<EvaluationRecord, String> creatorName;
	public static volatile SingularAttribute<EvaluationRecord, Long> isActive;
	public static volatile SingularAttribute<EvaluationRecord, Long> processId;
	public static volatile SingularAttribute<EvaluationRecord, String> formContent;
	public static volatile SingularAttribute<EvaluationRecord, String> mainContent;
	public static volatile SingularAttribute<EvaluationRecord, Long> attachId;
	public static volatile SingularAttribute<EvaluationRecord, Long> evaluationRecordId;
	public static volatile SingularAttribute<EvaluationRecord, Long> fileType;
	public static volatile SingularAttribute<EvaluationRecord, Long> fileId;
	public static volatile SingularAttribute<EvaluationRecord, Date> createDate;
	public static volatile SingularAttribute<EvaluationRecord, Long> status;

}

