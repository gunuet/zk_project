package com.viettel.module.evaluation.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Permit.class)
public abstract class Permit_ {

	public static volatile SingularAttribute<Permit, Long> permitId;
	public static volatile SingularAttribute<Permit, String> businessName;
	public static volatile SingularAttribute<Permit, Long> businessId;
	public static volatile SingularAttribute<Permit, Date> receiveDate;
	public static volatile SingularAttribute<Permit, String> telephone;
	public static volatile SingularAttribute<Permit, Date> signDate;
	public static volatile SingularAttribute<Permit, Long> type;
	public static volatile SingularAttribute<Permit, Long> isActive;
	public static volatile SingularAttribute<Permit, String> signerName;
	public static volatile SingularAttribute<Permit, String> receiveNo;
	public static volatile SingularAttribute<Permit, String> receiveDeptName;
	public static volatile SingularAttribute<Permit, String> fax;
	public static volatile SingularAttribute<Permit, Long> attachId;
	public static volatile SingularAttribute<Permit, String> email;
	public static volatile SingularAttribute<Permit, Date> effectiveDate;
	public static volatile SingularAttribute<Permit, Long> fileId;
	public static volatile SingularAttribute<Permit, Long> status;
	public static volatile SingularAttribute<Permit, Long> isQrCode;

}

