package com.viettel.module.importDevice.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AppliesFee.class)
public abstract class AppliesFee_ {

	public static volatile SingularAttribute<AppliesFee, String> note;
	public static volatile SingularAttribute<AppliesFee, String> amountFee;
	public static volatile SingularAttribute<AppliesFee, Long> isDeleted;
	public static volatile SingularAttribute<AppliesFee, Long> is_Active;
	public static volatile SingularAttribute<AppliesFee, Long> id;
	public static volatile SingularAttribute<AppliesFee, String> feeType;
	public static volatile SingularAttribute<AppliesFee, Long> importDeviceFileId;

}

