package com.viettel.module.importDevice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HealEquipmentCerfiticate.class)
public abstract class HealEquipmentCerfiticate_ {

	public static volatile SingularAttribute<HealEquipmentCerfiticate, String> note;
	public static volatile SingularAttribute<HealEquipmentCerfiticate, String> path;
	public static volatile SingularAttribute<HealEquipmentCerfiticate, String> attachName;
	public static volatile SingularAttribute<HealEquipmentCerfiticate, Long> id;
	public static volatile SingularAttribute<HealEquipmentCerfiticate, String> cerfiticateNumber;
	public static volatile SingularAttribute<HealEquipmentCerfiticate, Long> isActive;
	public static volatile SingularAttribute<HealEquipmentCerfiticate, String> healEquimentName;
	public static volatile SingularAttribute<HealEquipmentCerfiticate, Date> createDate;

}

