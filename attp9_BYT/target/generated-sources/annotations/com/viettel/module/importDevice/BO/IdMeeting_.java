package com.viettel.module.importDevice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IdMeeting.class)
public abstract class IdMeeting_ {

	public static volatile SingularAttribute<IdMeeting, String> result;
	public static volatile SingularAttribute<IdMeeting, String> statusDocName;
	public static volatile SingularAttribute<IdMeeting, Long> books;
	public static volatile SingularAttribute<IdMeeting, Long> sortOrder;
	public static volatile SingularAttribute<IdMeeting, Long> id;
	public static volatile SingularAttribute<IdMeeting, String> position;
	public static volatile SingularAttribute<IdMeeting, String> place;
	public static volatile SingularAttribute<IdMeeting, String> people;
	public static volatile SingularAttribute<IdMeeting, Date> day;
	public static volatile SingularAttribute<IdMeeting, Long> status;
	public static volatile SingularAttribute<IdMeeting, String> fileId;

}

