package com.viettel.module.importDevice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImportDeviceFile.class)
public abstract class ImportDeviceFile_ {

	public static volatile SingularAttribute<ImportDeviceFile, String> signPlace;
	public static volatile SingularAttribute<ImportDeviceFile, String> importerPhone;
	public static volatile SingularAttribute<ImportDeviceFile, Long> sendId;
	public static volatile SingularAttribute<ImportDeviceFile, String> importerFax;
	public static volatile SingularAttribute<ImportDeviceFile, String> importerEmail;
	public static volatile SingularAttribute<ImportDeviceFile, Date> signDate;
	public static volatile SingularAttribute<ImportDeviceFile, String> sendBookNumber;
	public static volatile SingularAttribute<ImportDeviceFile, String> productName;
	public static volatile SingularAttribute<ImportDeviceFile, String> directorPhone;
	public static volatile SingularAttribute<ImportDeviceFile, Long> importType;
	public static volatile SingularAttribute<ImportDeviceFile, Long> typeCreate;
	public static volatile SingularAttribute<ImportDeviceFile, String> importerName;
	public static volatile SingularAttribute<ImportDeviceFile, Date> uqDate;
	public static volatile SingularAttribute<ImportDeviceFile, Long> price;
	public static volatile SingularAttribute<ImportDeviceFile, String> oldNswFileCode;
	public static volatile SingularAttribute<ImportDeviceFile, String> responsiblePersonPhone;
	public static volatile SingularAttribute<ImportDeviceFile, String> responsiblePersonName;
	public static volatile SingularAttribute<ImportDeviceFile, Long> importDeviceFileId;
	public static volatile SingularAttribute<ImportDeviceFile, Long> bookMeeting;
	public static volatile SingularAttribute<ImportDeviceFile, String> signName;
	public static volatile SingularAttribute<ImportDeviceFile, String> importPurpose;
	public static volatile SingularAttribute<ImportDeviceFile, String> director;
	public static volatile SingularAttribute<ImportDeviceFile, String> groupProductId;
	public static volatile SingularAttribute<ImportDeviceFile, Long> bookSend;
	public static volatile SingularAttribute<ImportDeviceFile, String> equipmentNo;
	public static volatile SingularAttribute<ImportDeviceFile, String> responsiblePersonMobile;
	public static volatile SingularAttribute<ImportDeviceFile, Long> receiveId;
	public static volatile SingularAttribute<ImportDeviceFile, String> commentCV;
	public static volatile SingularAttribute<ImportDeviceFile, Date> cfsDate;
	public static volatile SingularAttribute<ImportDeviceFile, String> permitNumber;
	public static volatile SingularAttribute<ImportDeviceFile, String> groupProductName;
	public static volatile SingularAttribute<ImportDeviceFile, Long> documentTypeCode;
	public static volatile SingularAttribute<ImportDeviceFile, String> nswFileCode;
	public static volatile SingularAttribute<ImportDeviceFile, String> signedData;
	public static volatile SingularAttribute<ImportDeviceFile, Long> bookNumber;
	public static volatile SingularAttribute<ImportDeviceFile, Date> permitDate;
	public static volatile SingularAttribute<ImportDeviceFile, String> importerAddress;
	public static volatile SingularAttribute<ImportDeviceFile, String> directorMobile;
	public static volatile SingularAttribute<ImportDeviceFile, Date> isoDate;
	public static volatile SingularAttribute<ImportDeviceFile, Long> isChange;
	public static volatile SingularAttribute<ImportDeviceFile, Long> fileId;

}

