package com.viettel.module.importDevice.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImportDeviceProduct.class)
public abstract class ImportDeviceProduct_ {

	public static volatile SingularAttribute<ImportDeviceProduct, String> nationalCode;
	public static volatile SingularAttribute<ImportDeviceProduct, String> nationalProductCode;
	public static volatile SingularAttribute<ImportDeviceProduct, String> quantity;
	public static volatile SingularAttribute<ImportDeviceProduct, Long> productId;
	public static volatile SingularAttribute<ImportDeviceProduct, String> nationalProduct;
	public static volatile SingularAttribute<ImportDeviceProduct, Long> sendProductCode;
	public static volatile SingularAttribute<ImportDeviceProduct, String> groupProductId;
	public static volatile SingularAttribute<ImportDeviceProduct, String> distributor;
	public static volatile SingularAttribute<ImportDeviceProduct, String> productName;
	public static volatile SingularAttribute<ImportDeviceProduct, String> manufacturer;
	public static volatile SingularAttribute<ImportDeviceProduct, String> nationalDistributorCode;
	public static volatile SingularAttribute<ImportDeviceProduct, Long> isCategory;
	public static volatile SingularAttribute<ImportDeviceProduct, String> groupProductName;
	public static volatile SingularAttribute<ImportDeviceProduct, String> nationalDistributorName;
	public static volatile SingularAttribute<ImportDeviceProduct, String> model;
	public static volatile SingularAttribute<ImportDeviceProduct, String> produceYear;
	public static volatile SingularAttribute<ImportDeviceProduct, String> productManufacture;
	public static volatile SingularAttribute<ImportDeviceProduct, Long> fileId;
	public static volatile SingularAttribute<ImportDeviceProduct, String> nationalName;
	public static volatile SingularAttribute<ImportDeviceProduct, String> sendProductPlace;

}

