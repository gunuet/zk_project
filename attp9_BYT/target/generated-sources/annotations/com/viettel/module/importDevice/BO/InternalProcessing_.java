package com.viettel.module.importDevice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InternalProcessing.class)
public abstract class InternalProcessing_ {

	public static volatile SingularAttribute<InternalProcessing, Long> is_Active;
	public static volatile SingularAttribute<InternalProcessing, Long> meetingId;
	public static volatile SingularAttribute<InternalProcessing, Long> id;
	public static volatile SingularAttribute<InternalProcessing, String> content;
	public static volatile SingularAttribute<InternalProcessing, Date> createDate;

}

