package com.viettel.module.importDevice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ReceiptPayment.class)
public abstract class ReceiptPayment_ {

	public static volatile SingularAttribute<ReceiptPayment, String> createBy;
	public static volatile SingularAttribute<ReceiptPayment, String> companyName;
	public static volatile SingularAttribute<ReceiptPayment, String> companyAddress;
	public static volatile SingularAttribute<ReceiptPayment, Long> billId;
	public static volatile SingularAttribute<ReceiptPayment, Long> id;
	public static volatile SingularAttribute<ReceiptPayment, String> taxCode;
	public static volatile SingularAttribute<ReceiptPayment, Long> isActive;
	public static volatile SingularAttribute<ReceiptPayment, String> receiptPaymentNumber;
	public static volatile SingularAttribute<ReceiptPayment, Date> createDate;

}

