package com.viettel.module.importDevice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFileImportDevice.class)
public abstract class VFileImportDevice_ {

	public static volatile SingularAttribute<VFileImportDevice, String> fileName;
	public static volatile SingularAttribute<VFileImportDevice, Long> businessId;
	public static volatile SingularAttribute<VFileImportDevice, String> businessName;
	public static volatile SingularAttribute<VFileImportDevice, String> importerEmail;
	public static volatile SingularAttribute<VFileImportDevice, Date> signDate;
	public static volatile SingularAttribute<VFileImportDevice, Long> isActive;
	public static volatile SingularAttribute<VFileImportDevice, String> productName;
	public static volatile SingularAttribute<VFileImportDevice, String> directorPhone;
	public static volatile SingularAttribute<VFileImportDevice, Long> parentFileId;
	public static volatile SingularAttribute<VFileImportDevice, Long> typeCreate;
	public static volatile SingularAttribute<VFileImportDevice, Long> price;
	public static volatile SingularAttribute<VFileImportDevice, String> fileTypeName;
	public static volatile SingularAttribute<VFileImportDevice, String> responsiblePersonPhone;
	public static volatile SingularAttribute<VFileImportDevice, String> businessAddress;
	public static volatile SingularAttribute<VFileImportDevice, Long> importDeviceFileId;
	public static volatile SingularAttribute<VFileImportDevice, Long> bookMeeting;
	public static volatile SingularAttribute<VFileImportDevice, Date> modifyDate;
	public static volatile SingularAttribute<VFileImportDevice, String> director;
	public static volatile SingularAttribute<VFileImportDevice, String> groupProductId;
	public static volatile SingularAttribute<VFileImportDevice, String> fileCode;
	public static volatile SingularAttribute<VFileImportDevice, String> equipmentNo;
	public static volatile SingularAttribute<VFileImportDevice, String> taxCode;
	public static volatile SingularAttribute<VFileImportDevice, String> responsiblePersonMobile;
	public static volatile SingularAttribute<VFileImportDevice, Long> version;
	public static volatile SingularAttribute<VFileImportDevice, String> groupProductName;
	public static volatile SingularAttribute<VFileImportDevice, Long> documentTypeCode;
	public static volatile SingularAttribute<VFileImportDevice, Long> isTemp;
	public static volatile SingularAttribute<VFileImportDevice, String> directorMobile;
	public static volatile SingularAttribute<VFileImportDevice, String> businessFax;
	public static volatile SingularAttribute<VFileImportDevice, String> businessPhone;
	public static volatile SingularAttribute<VFileImportDevice, Date> startDate;
	public static volatile SingularAttribute<VFileImportDevice, Long> fileId;
	public static volatile SingularAttribute<VFileImportDevice, Long> status;
	public static volatile SingularAttribute<VFileImportDevice, String> signPlace;
	public static volatile SingularAttribute<VFileImportDevice, Long> createDeptId;
	public static volatile SingularAttribute<VFileImportDevice, String> importerPhone;
	public static volatile SingularAttribute<VFileImportDevice, Long> creatorId;
	public static volatile SingularAttribute<VFileImportDevice, String> creatorName;
	public static volatile SingularAttribute<VFileImportDevice, String> importerFax;
	public static volatile SingularAttribute<VFileImportDevice, Long> importType;
	public static volatile SingularAttribute<VFileImportDevice, String> importerName;
	public static volatile SingularAttribute<VFileImportDevice, Long> numDayProcess;
	public static volatile SingularAttribute<VFileImportDevice, Date> deadline;
	public static volatile SingularAttribute<VFileImportDevice, String> responsiblePersonName;
	public static volatile SingularAttribute<VFileImportDevice, Date> createDate;
	public static volatile SingularAttribute<VFileImportDevice, String> createDeptName;
	public static volatile SingularAttribute<VFileImportDevice, String> signName;
	public static volatile SingularAttribute<VFileImportDevice, String> importPurpose;
	public static volatile SingularAttribute<VFileImportDevice, String> permitNumber;
	public static volatile SingularAttribute<VFileImportDevice, String> nswFileCode;
	public static volatile SingularAttribute<VFileImportDevice, String> signedData;
	public static volatile SingularAttribute<VFileImportDevice, Long> bookNumber;
	public static volatile SingularAttribute<VFileImportDevice, Date> permitDate;
	public static volatile SingularAttribute<VFileImportDevice, String> importerAddress;
	public static volatile SingularAttribute<VFileImportDevice, Date> finishDate;
	public static volatile SingularAttribute<VFileImportDevice, Long> fileType;

}

