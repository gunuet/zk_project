package com.viettel.module.importDevice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VIdfPaymentinfo.class)
public abstract class VIdfPaymentinfo_ {

	public static volatile SingularAttribute<VIdfPaymentinfo, Long> createDeptId;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> note;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> paymentConfirm;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> statuspayment;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> businessName;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> creatorId;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> creatorName;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> isActive;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> feeId;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> paymentActionUser;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> paymentTypeId;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> importerName;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> businessAddress;
	public static volatile SingularAttribute<VIdfPaymentinfo, Date> dateConfirm;
	public static volatile SingularAttribute<VIdfPaymentinfo, Date> createDate;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> importDeviceFileId;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> phase;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> createDeptName;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> cost;
	public static volatile SingularAttribute<VIdfPaymentinfo, Date> modifyDate;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> paymentPerson;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> billCode;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> taxCode;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> paymentInfoId;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> documentTypeCode;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> paymentCode;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> nswFileCode;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> billId;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> feeName;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> businessFax;
	public static volatile SingularAttribute<VIdfPaymentinfo, String> businessPhone;
	public static volatile SingularAttribute<VIdfPaymentinfo, Date> paymentDate;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> fileType;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> statusfile;
	public static volatile SingularAttribute<VIdfPaymentinfo, Long> fileId;

}

