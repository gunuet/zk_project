package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImportOrderFile.class)
public abstract class ImportOrderFile_ {

	public static volatile SingularAttribute<ImportOrderFile, String> signPlace;
	public static volatile SingularAttribute<ImportOrderFile, String> deptName;
	public static volatile SingularAttribute<ImportOrderFile, String> contractNo;
	public static volatile SingularAttribute<ImportOrderFile, String> exporterAddress;
	public static volatile SingularAttribute<ImportOrderFile, String> checkDeptName;
	public static volatile SingularAttribute<ImportOrderFile, String> exporterEmail;
	public static volatile SingularAttribute<ImportOrderFile, String> goodsOwnerAddress;
	public static volatile SingularAttribute<ImportOrderFile, String> goodsOwnerName;
	public static volatile SingularAttribute<ImportOrderFile, String> exporterFax;
	public static volatile SingularAttribute<ImportOrderFile, Long> checkDeptId;
	public static volatile SingularAttribute<ImportOrderFile, Date> signDate;
	public static volatile SingularAttribute<ImportOrderFile, Long> registerNo;
	public static volatile SingularAttribute<ImportOrderFile, String> responsiblePersonAddress;
	public static volatile SingularAttribute<ImportOrderFile, String> filesNo;
	public static volatile SingularAttribute<ImportOrderFile, String> importerGateCode;
	public static volatile SingularAttribute<ImportOrderFile, String> filesNoReduced;
	public static volatile SingularAttribute<ImportOrderFile, Long> importOrderFileId;
	public static volatile SingularAttribute<ImportOrderFile, String> exporterPhone;
	public static volatile SingularAttribute<ImportOrderFile, String> responsiblePersonPhone;
	public static volatile SingularAttribute<ImportOrderFile, String> checkPlace;
	public static volatile SingularAttribute<ImportOrderFile, String> billNo;
	public static volatile SingularAttribute<ImportOrderFile, String> responsiblePersonName;
	public static volatile SingularAttribute<ImportOrderFile, String> customsBranchName;
	public static volatile SingularAttribute<ImportOrderFile, String> goodsOwnerPhone;
	public static volatile SingularAttribute<ImportOrderFile, String> signName;
	public static volatile SingularAttribute<ImportOrderFile, String> checkDeptCode;
	public static volatile SingularAttribute<ImportOrderFile, String> comments;
	public static volatile SingularAttribute<ImportOrderFile, String> goodsOwnerEmail;
	public static volatile SingularAttribute<ImportOrderFile, String> exporterGateCode;
	public static volatile SingularAttribute<ImportOrderFile, String> transNo;
	public static volatile SingularAttribute<ImportOrderFile, Date> comingDate;
	public static volatile SingularAttribute<ImportOrderFile, Long> permitNo;
	public static volatile SingularAttribute<ImportOrderFile, String> goodsOwnerFax;
	public static volatile SingularAttribute<ImportOrderFile, String> exporterGateName;
	public static volatile SingularAttribute<ImportOrderFile, Long> documentTypeCode;
	public static volatile SingularAttribute<ImportOrderFile, Date> checkTime;
	public static volatile SingularAttribute<ImportOrderFile, String> nswFileCode;
	public static volatile SingularAttribute<ImportOrderFile, String> importerGateName;
	public static volatile SingularAttribute<ImportOrderFile, String> signedData;
	public static volatile SingularAttribute<ImportOrderFile, String> responsiblePersonEmail;
	public static volatile SingularAttribute<ImportOrderFile, String> exporterName;
	public static volatile SingularAttribute<ImportOrderFile, String> commentsType;
	public static volatile SingularAttribute<ImportOrderFile, String> customsBranchCode;
	public static volatile SingularAttribute<ImportOrderFile, String> responsiblePersonFax;
	public static volatile SingularAttribute<ImportOrderFile, String> deptCode;
	public static volatile SingularAttribute<ImportOrderFile, Long> fileId;

}

