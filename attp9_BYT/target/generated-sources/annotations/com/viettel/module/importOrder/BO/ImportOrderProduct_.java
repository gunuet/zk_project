package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImportOrderProduct.class)
public abstract class ImportOrderProduct_ {

	public static volatile SingularAttribute<ImportOrderProduct, String> reason;
	public static volatile SingularAttribute<ImportOrderProduct, String> nationalCode;
	public static volatile SingularAttribute<ImportOrderProduct, Date> dateOfManufacturer;
	public static volatile SingularAttribute<ImportOrderProduct, Date> expiredDate;
	public static volatile SingularAttribute<ImportOrderProduct, String> manufacturerAddress;
	public static volatile SingularAttribute<ImportOrderProduct, String> totalUnitName;
	public static volatile SingularAttribute<ImportOrderProduct, String> productName;
	public static volatile SingularAttribute<ImportOrderProduct, Long> baseUnit;
	public static volatile SingularAttribute<ImportOrderProduct, Double> total;
	public static volatile SingularAttribute<ImportOrderProduct, String> checkMethodName;
	public static volatile SingularAttribute<ImportOrderProduct, byte[]> qrCode;
	public static volatile SingularAttribute<ImportOrderProduct, Long> reTest;
	public static volatile SingularAttribute<ImportOrderProduct, Long> processType;
	public static volatile SingularAttribute<ImportOrderProduct, String> netweightUnitCode;
	public static volatile SingularAttribute<ImportOrderProduct, String> results;
	public static volatile SingularAttribute<ImportOrderProduct, String> productDescription;
	public static volatile SingularAttribute<ImportOrderProduct, String> nationalName;
	public static volatile SingularAttribute<ImportOrderProduct, String> comments;
	public static volatile SingularAttribute<ImportOrderProduct, Long> productId;
	public static volatile SingularAttribute<ImportOrderProduct, Long> pass;
	public static volatile SingularAttribute<ImportOrderProduct, Long> reProcess;
	public static volatile SingularAttribute<ImportOrderProduct, String> manufacTurer;
	public static volatile SingularAttribute<ImportOrderProduct, String> productNumber;
	public static volatile SingularAttribute<ImportOrderProduct, String> totalUnitCode;
	public static volatile SingularAttribute<ImportOrderProduct, String> productCode;
	public static volatile SingularAttribute<ImportOrderProduct, String> hsCode;
	public static volatile SingularAttribute<ImportOrderProduct, Long> haveQrCode;
	public static volatile SingularAttribute<ImportOrderProduct, String> processTypeName;
	public static volatile SingularAttribute<ImportOrderProduct, Double> netweight;
	public static volatile SingularAttribute<ImportOrderProduct, String> checkMethodCode;
	public static volatile SingularAttribute<ImportOrderProduct, String> netweightUnitName;
	public static volatile SingularAttribute<ImportOrderProduct, String> confirmAnnounceNo;
	public static volatile SingularAttribute<ImportOrderProduct, Long> fileId;

}

