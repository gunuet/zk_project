package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProcessDocument.class)
public abstract class ProcessDocument_ {

	public static volatile SingularAttribute<ProcessDocument, Date> createdDate;
	public static volatile SingularAttribute<ProcessDocument, String> processName;
	public static volatile SingularAttribute<ProcessDocument, String> processPhone;
	public static volatile SingularAttribute<ProcessDocument, Long> id;
	public static volatile SingularAttribute<ProcessDocument, String> coments;
	public static volatile SingularAttribute<ProcessDocument, Date> checkDate;
	public static volatile SingularAttribute<ProcessDocument, String> checkPlace;
	public static volatile SingularAttribute<ProcessDocument, Long> isActive;
	public static volatile SingularAttribute<ProcessDocument, Long> type;
	public static volatile SingularAttribute<ProcessDocument, Long> fileId;

}

