package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductProductTarget.class)
public abstract class ProductProductTarget_ {

	public static volatile SingularAttribute<ProductProductTarget, String> comments;
	public static volatile SingularAttribute<ProductProductTarget, Long> cost;
	public static volatile SingularAttribute<ProductProductTarget, Long> productId;
	public static volatile SingularAttribute<ProductProductTarget, Long> pass;
	public static volatile SingularAttribute<ProductProductTarget, String> maxValue;
	public static volatile SingularAttribute<ProductProductTarget, String> typeName;
	public static volatile SingularAttribute<ProductProductTarget, Long> isActive;
	public static volatile SingularAttribute<ProductProductTarget, String> type;
	public static volatile SingularAttribute<ProductProductTarget, Long> productTargetId;
	public static volatile SingularAttribute<ProductProductTarget, Date> createdDate;
	public static volatile SingularAttribute<ProductProductTarget, Long> createdBy;
	public static volatile SingularAttribute<ProductProductTarget, String> name;
	public static volatile SingularAttribute<ProductProductTarget, String> testMethod;
	public static volatile SingularAttribute<ProductProductTarget, Long> id;
	public static volatile SingularAttribute<ProductProductTarget, String> testValue;
	public static volatile SingularAttribute<ProductProductTarget, Long> status;

}

