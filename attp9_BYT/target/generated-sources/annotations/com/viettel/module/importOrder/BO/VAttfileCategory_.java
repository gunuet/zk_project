package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VAttfileCategory.class)
public abstract class VAttfileCategory_ {

	public static volatile SingularAttribute<VAttfileCategory, String> attachPath;
	public static volatile SingularAttribute<VAttfileCategory, Long> attachCat;
	public static volatile SingularAttribute<VAttfileCategory, Date> dateModify;
	public static volatile SingularAttribute<VAttfileCategory, Long> creatorId;
	public static volatile SingularAttribute<VAttfileCategory, Long> modifierId;
	public static volatile SingularAttribute<VAttfileCategory, Long> isSent;
	public static volatile SingularAttribute<VAttfileCategory, Long> isActive;
	public static volatile SingularAttribute<VAttfileCategory, Date> dateCreate;
	public static volatile SingularAttribute<VAttfileCategory, Long> version;
	public static volatile SingularAttribute<VAttfileCategory, Long> attachType;
	public static volatile SingularAttribute<VAttfileCategory, String> attachCode;
	public static volatile SingularAttribute<VAttfileCategory, String> attachUrl;
	public static volatile SingularAttribute<VAttfileCategory, String> attachName;
	public static volatile SingularAttribute<VAttfileCategory, String> typeFileName;
	public static volatile SingularAttribute<VAttfileCategory, Long> attachId;
	public static volatile SingularAttribute<VAttfileCategory, Long> objectId;

}

