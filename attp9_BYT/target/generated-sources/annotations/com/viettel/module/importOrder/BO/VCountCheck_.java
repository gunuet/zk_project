package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VCountCheck.class)
public abstract class VCountCheck_ {

	public static volatile SingularAttribute<VCountCheck, String> reason;
	public static volatile SingularAttribute<VCountCheck, String> nationalCode;
	public static volatile SingularAttribute<VCountCheck, Date> dateOfManufacturer;
	public static volatile SingularAttribute<VCountCheck, Date> expiredDate;
	public static volatile SingularAttribute<VCountCheck, Long> creatorId;
	public static volatile SingularAttribute<VCountCheck, String> manufacturerAddress;
	public static volatile SingularAttribute<VCountCheck, String> totalUnitName;
	public static volatile SingularAttribute<VCountCheck, String> productName;
	public static volatile SingularAttribute<VCountCheck, String> manufacturer;
	public static volatile SingularAttribute<VCountCheck, Long> baseUnit;
	public static volatile SingularAttribute<VCountCheck, Long> total;
	public static volatile SingularAttribute<VCountCheck, String> checkMethodName;
	public static volatile SingularAttribute<VCountCheck, Short> reTest;
	public static volatile SingularAttribute<VCountCheck, Short> processType;
	public static volatile SingularAttribute<VCountCheck, String> netweightUnitCode;
	public static volatile SingularAttribute<VCountCheck, String> results;
	public static volatile SingularAttribute<VCountCheck, String> productDescription;
	public static volatile SingularAttribute<VCountCheck, String> nationalName;
	public static volatile SingularAttribute<VCountCheck, Date> createDate;
	public static volatile SingularAttribute<VCountCheck, String> comments;
	public static volatile SingularAttribute<VCountCheck, Long> productId;
	public static volatile SingularAttribute<VCountCheck, Short> pass;
	public static volatile SingularAttribute<VCountCheck, Short> reProcess;
	public static volatile SingularAttribute<VCountCheck, String> productNumber;
	public static volatile SingularAttribute<VCountCheck, String> totalUnitCode;
	public static volatile SingularAttribute<VCountCheck, String> productCode;
	public static volatile SingularAttribute<VCountCheck, String> hsCode;
	public static volatile SingularAttribute<VCountCheck, String> processTypeName;
	public static volatile SingularAttribute<VCountCheck, String> nswFileCode;
	public static volatile SingularAttribute<VCountCheck, Long> netweight;
	public static volatile SingularAttribute<VCountCheck, String> checkMethodCode;
	public static volatile SingularAttribute<VCountCheck, String> netweightUnitName;
	public static volatile SingularAttribute<VCountCheck, String> confirmAnnounceNo;
	public static volatile SingularAttribute<VCountCheck, Long> fileId;

}

