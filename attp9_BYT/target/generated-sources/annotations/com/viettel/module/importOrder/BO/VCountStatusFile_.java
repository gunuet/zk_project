package com.viettel.module.importOrder.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VCountStatusFile.class)
public abstract class VCountStatusFile_ {

	public static volatile SingularAttribute<VCountStatusFile, Long> deptId;
	public static volatile SingularAttribute<VCountStatusFile, String> deptPath;
	public static volatile SingularAttribute<VCountStatusFile, Long> receiveGroupId;
	public static volatile SingularAttribute<VCountStatusFile, Long> status;

}

