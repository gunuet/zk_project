package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFIofIopP.class)
public abstract class VFIofIopP_ {

	public static volatile SingularAttribute<VFIofIopP, String> deptName;
	public static volatile SingularAttribute<VFIofIopP, String> fileName;
	public static volatile SingularAttribute<VFIofIopP, String> exporterEmail;
	public static volatile SingularAttribute<VFIofIopP, Long> businessId;
	public static volatile SingularAttribute<VFIofIopP, String> businessName;
	public static volatile SingularAttribute<VFIofIopP, String> exporterFax;
	public static volatile SingularAttribute<VFIofIopP, Date> signDate;
	public static volatile SingularAttribute<VFIofIopP, Long> isActive;
	public static volatile SingularAttribute<VFIofIopP, String> productName;
	public static volatile SingularAttribute<VFIofIopP, String> signerName;
	public static volatile SingularAttribute<VFIofIopP, String> importerGateCode;
	public static volatile SingularAttribute<VFIofIopP, Long> parentFileId;
	public static volatile SingularAttribute<VFIofIopP, String> fileTypeName;
	public static volatile SingularAttribute<VFIofIopP, String> receiveDeptName;
	public static volatile SingularAttribute<VFIofIopP, String> responsiblePersonPhone;
	public static volatile SingularAttribute<VFIofIopP, String> checkPlace;
	public static volatile SingularAttribute<VFIofIopP, String> businessAddress;
	public static volatile SingularAttribute<VFIofIopP, String> billNo;
	public static volatile SingularAttribute<VFIofIopP, String> customsBranchName;
	public static volatile SingularAttribute<VFIofIopP, String> productDescription;
	public static volatile SingularAttribute<VFIofIopP, Date> modifyDate;
	public static volatile SingularAttribute<VFIofIopP, Long> productId;
	public static volatile SingularAttribute<VFIofIopP, Long> pass;
	public static volatile SingularAttribute<VFIofIopP, String> fileCode;
	public static volatile SingularAttribute<VFIofIopP, Date> receiveDate;
	public static volatile SingularAttribute<VFIofIopP, String> taxCode;
	public static volatile SingularAttribute<VFIofIopP, Long> version;
	public static volatile SingularAttribute<VFIofIopP, String> goodsOwnerFax;
	public static volatile SingularAttribute<VFIofIopP, String> exporterGateName;
	public static volatile SingularAttribute<VFIofIopP, Long> documentTypeCode;
	public static volatile SingularAttribute<VFIofIopP, Date> checkTime;
	public static volatile SingularAttribute<VFIofIopP, Long> haveQrCode;
	public static volatile SingularAttribute<VFIofIopP, String> receiveNo;
	public static volatile SingularAttribute<VFIofIopP, String> importerGateName;
	public static volatile SingularAttribute<VFIofIopP, String> responsiblePersonEmail;
	public static volatile SingularAttribute<VFIofIopP, Short> isTemp;
	public static volatile SingularAttribute<VFIofIopP, String> businessFax;
	public static volatile SingularAttribute<VFIofIopP, String> businessPhone;
	public static volatile SingularAttribute<VFIofIopP, String> deptCode;
	public static volatile SingularAttribute<VFIofIopP, Date> startDate;
	public static volatile SingularAttribute<VFIofIopP, Long> fileId;
	public static volatile SingularAttribute<VFIofIopP, Long> status;
	public static volatile SingularAttribute<VFIofIopP, String> confirmAnnounceNo;
	public static volatile SingularAttribute<VFIofIopP, String> signPlace;
	public static volatile SingularAttribute<VFIofIopP, Long> createDeptId;
	public static volatile SingularAttribute<VFIofIopP, String> nationalCode;
	public static volatile SingularAttribute<VFIofIopP, String> contractNo;
	public static volatile SingularAttribute<VFIofIopP, String> exporterAddress;
	public static volatile SingularAttribute<VFIofIopP, Long> creatorId;
	public static volatile SingularAttribute<VFIofIopP, String> creatorName;
	public static volatile SingularAttribute<VFIofIopP, String> goodsOwnerAddress;
	public static volatile SingularAttribute<VFIofIopP, String> goodsOwnerName;
	public static volatile SingularAttribute<VFIofIopP, String> responsiblePersonAddress;
	public static volatile SingularAttribute<VFIofIopP, Long> importOrderFileId;
	public static volatile SingularAttribute<VFIofIopP, String> exporterPhone;
	public static volatile SingularAttribute<VFIofIopP, Long> numDayProcess;
	public static volatile SingularAttribute<VFIofIopP, Date> deadline;
	public static volatile SingularAttribute<VFIofIopP, String> responsiblePersonName;
	public static volatile SingularAttribute<VFIofIopP, Date> createDate;
	public static volatile SingularAttribute<VFIofIopP, String> nationalName;
	public static volatile SingularAttribute<VFIofIopP, String> goodsOwnerPhone;
	public static volatile SingularAttribute<VFIofIopP, String> createDeptName;
	public static volatile SingularAttribute<VFIofIopP, String> signName;
	public static volatile SingularAttribute<VFIofIopP, String> goodsOwnerEmail;
	public static volatile SingularAttribute<VFIofIopP, String> exporterGateCode;
	public static volatile SingularAttribute<VFIofIopP, String> transNo;
	public static volatile SingularAttribute<VFIofIopP, Long> cosFileId;
	public static volatile SingularAttribute<VFIofIopP, Date> comingDate;
	public static volatile SingularAttribute<VFIofIopP, String> productCode;
	public static volatile SingularAttribute<VFIofIopP, String> nswFileCode;
	public static volatile SingularAttribute<VFIofIopP, String> signedData;
	public static volatile SingularAttribute<VFIofIopP, String> exporterName;
	public static volatile SingularAttribute<VFIofIopP, Date> finishDate;
	public static volatile SingularAttribute<VFIofIopP, String> customsBranchCode;
	public static volatile SingularAttribute<VFIofIopP, String> responsiblePersonFax;
	public static volatile SingularAttribute<VFIofIopP, Long> fileType;
	public static volatile SingularAttribute<VFIofIopP, Date> effectiveDate;

}

