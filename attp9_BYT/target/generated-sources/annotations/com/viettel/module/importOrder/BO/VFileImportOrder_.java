package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFileImportOrder.class)
public abstract class VFileImportOrder_ {

	public static volatile SingularAttribute<VFileImportOrder, String> deptName;
	public static volatile SingularAttribute<VFileImportOrder, String> fileName;
	public static volatile SingularAttribute<VFileImportOrder, String> exporterEmail;
	public static volatile SingularAttribute<VFileImportOrder, Long> businessId;
	public static volatile SingularAttribute<VFileImportOrder, String> businessName;
	public static volatile SingularAttribute<VFileImportOrder, String> exporterFax;
	public static volatile SingularAttribute<VFileImportOrder, Date> signDate;
	public static volatile SingularAttribute<VFileImportOrder, Long> isActive;
	public static volatile SingularAttribute<VFileImportOrder, String> importerGateCode;
	public static volatile SingularAttribute<VFileImportOrder, Long> parentFileId;
	public static volatile SingularAttribute<VFileImportOrder, String> fileTypeName;
	public static volatile SingularAttribute<VFileImportOrder, String> responsiblePersonPhone;
	public static volatile SingularAttribute<VFileImportOrder, String> checkPlace;
	public static volatile SingularAttribute<VFileImportOrder, String> businessAddress;
	public static volatile SingularAttribute<VFileImportOrder, String> billNo;
	public static volatile SingularAttribute<VFileImportOrder, String> customsBranchName;
	public static volatile SingularAttribute<VFileImportOrder, Date> modifyDate;
	public static volatile SingularAttribute<VFileImportOrder, String> fileCode;
	public static volatile SingularAttribute<VFileImportOrder, String> taxCode;
	public static volatile SingularAttribute<VFileImportOrder, Long> version;
	public static volatile SingularAttribute<VFileImportOrder, String> goodsOwnerFax;
	public static volatile SingularAttribute<VFileImportOrder, String> exporterGateName;
	public static volatile SingularAttribute<VFileImportOrder, Long> documentTypeCode;
	public static volatile SingularAttribute<VFileImportOrder, Date> checkTime;
	public static volatile SingularAttribute<VFileImportOrder, Long> haveQrCode;
	public static volatile SingularAttribute<VFileImportOrder, String> importerGateName;
	public static volatile SingularAttribute<VFileImportOrder, String> responsiblePersonEmail;
	public static volatile SingularAttribute<VFileImportOrder, Long> isTemp;
	public static volatile SingularAttribute<VFileImportOrder, String> businessFax;
	public static volatile SingularAttribute<VFileImportOrder, String> businessPhone;
	public static volatile SingularAttribute<VFileImportOrder, String> deptCode;
	public static volatile SingularAttribute<VFileImportOrder, Date> startDate;
	public static volatile SingularAttribute<VFileImportOrder, Long> fileId;
	public static volatile SingularAttribute<VFileImportOrder, Long> status;
	public static volatile SingularAttribute<VFileImportOrder, String> signPlace;
	public static volatile SingularAttribute<VFileImportOrder, Long> createDeptId;
	public static volatile SingularAttribute<VFileImportOrder, String> contractNo;
	public static volatile SingularAttribute<VFileImportOrder, String> exporterAddress;
	public static volatile SingularAttribute<VFileImportOrder, Long> creatorId;
	public static volatile SingularAttribute<VFileImportOrder, String> creatorName;
	public static volatile SingularAttribute<VFileImportOrder, String> goodsOwnerAddress;
	public static volatile SingularAttribute<VFileImportOrder, String> goodsOwnerName;
	public static volatile SingularAttribute<VFileImportOrder, String> responsiblePersonAddress;
	public static volatile SingularAttribute<VFileImportOrder, Long> importOrderFileId;
	public static volatile SingularAttribute<VFileImportOrder, String> exporterPhone;
	public static volatile SingularAttribute<VFileImportOrder, Long> numDayProcess;
	public static volatile SingularAttribute<VFileImportOrder, Date> deadline;
	public static volatile SingularAttribute<VFileImportOrder, String> responsiblePersonName;
	public static volatile SingularAttribute<VFileImportOrder, Date> createDate;
	public static volatile SingularAttribute<VFileImportOrder, String> goodsOwnerPhone;
	public static volatile SingularAttribute<VFileImportOrder, String> createDeptName;
	public static volatile SingularAttribute<VFileImportOrder, String> signName;
	public static volatile SingularAttribute<VFileImportOrder, String> goodsOwnerEmail;
	public static volatile SingularAttribute<VFileImportOrder, String> exporterGateCode;
	public static volatile SingularAttribute<VFileImportOrder, String> transNo;
	public static volatile SingularAttribute<VFileImportOrder, Long> cosFileId;
	public static volatile SingularAttribute<VFileImportOrder, Date> comingDate;
	public static volatile SingularAttribute<VFileImportOrder, String> nswFileCode;
	public static volatile SingularAttribute<VFileImportOrder, String> signedData;
	public static volatile SingularAttribute<VFileImportOrder, String> exporterName;
	public static volatile SingularAttribute<VFileImportOrder, Date> finishDate;
	public static volatile SingularAttribute<VFileImportOrder, String> customsBranchCode;
	public static volatile SingularAttribute<VFileImportOrder, String> responsiblePersonFax;
	public static volatile SingularAttribute<VFileImportOrder, Long> fileType;

}

