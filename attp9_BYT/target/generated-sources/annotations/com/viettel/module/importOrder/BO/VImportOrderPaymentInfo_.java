package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VImportOrderPaymentInfo.class)
public abstract class VImportOrderPaymentInfo_ {

	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> createDeptId;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> note;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> paymentConfirm;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> statuspayment;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> businessName;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> creatorId;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> creatorName;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> isActive;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> feeId;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> productName;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> paymentActionUser;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> paymentTypeId;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> businessAddress;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> cosmeticNo;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Date> dateConfirm;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Date> createDate;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> phase;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> createDeptName;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> brandName;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> cost;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Date> modifyDate;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> paymentPerson;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> billCode;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> taxCode;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> paymentInfoId;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> cosFileId;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> documentTypeCode;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> paymentCode;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> nswFileCode;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> billId;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> feeName;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> businessFax;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, String> businessPhone;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Date> paymentDate;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> fileType;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> statusfile;
	public static volatile SingularAttribute<VImportOrderPaymentInfo, Long> fileId;

}

