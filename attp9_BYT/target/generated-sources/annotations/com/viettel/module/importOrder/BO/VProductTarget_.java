package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VProductTarget.class)
public abstract class VProductTarget_ {

	public static volatile SingularAttribute<VProductTarget, String> comments;
	public static volatile SingularAttribute<VProductTarget, Long> cost;
	public static volatile SingularAttribute<VProductTarget, Long> productId;
	public static volatile SingularAttribute<VProductTarget, Long> pass;
	public static volatile SingularAttribute<VProductTarget, String> maxValue;
	public static volatile SingularAttribute<VProductTarget, String> typeName;
	public static volatile SingularAttribute<VProductTarget, Long> isActive;
	public static volatile SingularAttribute<VProductTarget, String> type;
	public static volatile SingularAttribute<VProductTarget, Long> productTargetId;
	public static volatile SingularAttribute<VProductTarget, Date> createdDate;
	public static volatile SingularAttribute<VProductTarget, Long> createdBy;
	public static volatile SingularAttribute<VProductTarget, String> name;
	public static volatile SingularAttribute<VProductTarget, String> testMethod;
	public static volatile SingularAttribute<VProductTarget, Long> id;
	public static volatile SingularAttribute<VProductTarget, String> testValue;
	public static volatile SingularAttribute<VProductTarget, Long> status;

}

