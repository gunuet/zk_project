package com.viettel.module.importOrder.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VimportOrderFileAttach.class)
public abstract class VimportOrderFileAttach_ {

	public static volatile SingularAttribute<VimportOrderFileAttach, String> attachPath;
	public static volatile SingularAttribute<VimportOrderFileAttach, Long> attachCat;
	public static volatile SingularAttribute<VimportOrderFileAttach, Date> dateModify;
	public static volatile SingularAttribute<VimportOrderFileAttach, Long> creatorId;
	public static volatile SingularAttribute<VimportOrderFileAttach, Long> modifierId;
	public static volatile SingularAttribute<VimportOrderFileAttach, Short> isActive;
	public static volatile SingularAttribute<VimportOrderFileAttach, Date> dateCreate;
	public static volatile SingularAttribute<VimportOrderFileAttach, Integer> version;
	public static volatile SingularAttribute<VimportOrderFileAttach, Long> attachType;
	public static volatile SingularAttribute<VimportOrderFileAttach, String> attachCode;
	public static volatile SingularAttribute<VimportOrderFileAttach, String> attachUrl;
	public static volatile SingularAttribute<VimportOrderFileAttach, String> attachName;
	public static volatile SingularAttribute<VimportOrderFileAttach, String> typeFileName;
	public static volatile SingularAttribute<VimportOrderFileAttach, Long> attachId;
	public static volatile SingularAttribute<VimportOrderFileAttach, Long> objectId;

}

