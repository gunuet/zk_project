package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdAttachment.class)
public abstract class IfdAttachment_ {

	public static volatile SingularAttribute<IfdAttachment, Long> attachmentCode;
	public static volatile SingularAttribute<IfdAttachment, String> attachTypeName;
	public static volatile SingularAttribute<IfdAttachment, String> attachPath;
	public static volatile SingularAttribute<IfdAttachment, Date> updateDate;
	public static volatile SingularAttribute<IfdAttachment, Long> attachTypeCode;
	public static volatile SingularAttribute<IfdAttachment, String> attachMohId;
	public static volatile SingularAttribute<IfdAttachment, Long> objectType;
	public static volatile SingularAttribute<IfdAttachment, Long> createBy;
	public static volatile SingularAttribute<IfdAttachment, Long> updateBy;
	public static volatile SingularAttribute<IfdAttachment, String> attachmentName;
	public static volatile SingularAttribute<IfdAttachment, Long> attachId;
	public static volatile SingularAttribute<IfdAttachment, Long> objectId;
	public static volatile SingularAttribute<IfdAttachment, Long> status;
	public static volatile SingularAttribute<IfdAttachment, Date> createDate;

}

