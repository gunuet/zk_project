package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdCertificateProduct.class)
public abstract class IfdCertificateProduct_ {

	public static volatile SingularAttribute<IfdCertificateProduct, String> reason;
	public static volatile SingularAttribute<IfdCertificateProduct, String> note;
	public static volatile SingularAttribute<IfdCertificateProduct, Date> updateDate;
	public static volatile SingularAttribute<IfdCertificateProduct, Long> productCheckMethod;
	public static volatile SingularAttribute<IfdCertificateProduct, Long> pass;
	public static volatile SingularAttribute<IfdCertificateProduct, Long> certificateId;
	public static volatile SingularAttribute<IfdCertificateProduct, String> handlingMeasuresName;
	public static volatile SingularAttribute<IfdCertificateProduct, String> productGroupName;
	public static volatile SingularAttribute<IfdCertificateProduct, String> manufacturerAddress;
	public static volatile SingularAttribute<IfdCertificateProduct, String> productName;
	public static volatile SingularAttribute<IfdCertificateProduct, String> manufacturer;
	public static volatile SingularAttribute<IfdCertificateProduct, Long> createBy;
	public static volatile SingularAttribute<IfdCertificateProduct, String> productCode;
	public static volatile SingularAttribute<IfdCertificateProduct, String> productGroupCode;
	public static volatile SingularAttribute<IfdCertificateProduct, String> checkMethodConfirmNo;
	public static volatile SingularAttribute<IfdCertificateProduct, Long> updateBy;
	public static volatile SingularAttribute<IfdCertificateProduct, Long> handlingMeasuresCode;
	public static volatile SingularAttribute<IfdCertificateProduct, Long> certificateProductId;
	public static volatile SingularAttribute<IfdCertificateProduct, String> confirmAnnounceNo;
	public static volatile SingularAttribute<IfdCertificateProduct, Long> status;
	public static volatile SingularAttribute<IfdCertificateProduct, Date> createDate;

}

