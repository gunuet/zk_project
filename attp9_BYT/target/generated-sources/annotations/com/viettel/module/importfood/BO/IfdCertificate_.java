package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdCertificate.class)
public abstract class IfdCertificate_ {

	public static volatile SingularAttribute<IfdCertificate, String> responesiblePersonName;
	public static volatile SingularAttribute<IfdCertificate, String> responesiblePersonFax;
	public static volatile SingularAttribute<IfdCertificate, Date> updateDate;
	public static volatile SingularAttribute<IfdCertificate, Long> certificateStatus;
	public static volatile SingularAttribute<IfdCertificate, String> responesiblePersonAddress;
	public static volatile SingularAttribute<IfdCertificate, String> exporterAddress;
	public static volatile SingularAttribute<IfdCertificate, Date> comingDateTo;
	public static volatile SingularAttribute<IfdCertificate, String> checkDeptName;
	public static volatile SingularAttribute<IfdCertificate, Long> certificateId;
	public static volatile SingularAttribute<IfdCertificate, String> responesiblePersonPhone;
	public static volatile SingularAttribute<IfdCertificate, String> exporterEmail;
	public static volatile SingularAttribute<IfdCertificate, Short> checkMethod;
	public static volatile SingularAttribute<IfdCertificate, String> goodsOwnerAddress;
	public static volatile SingularAttribute<IfdCertificate, String> goodsOwnerName;
	public static volatile SingularAttribute<IfdCertificate, String> exporterFax;
	public static volatile SingularAttribute<IfdCertificate, String> responesiblePersonEmail;
	public static volatile SingularAttribute<IfdCertificate, Date> signDate;
	public static volatile SingularAttribute<IfdCertificate, Date> certificateCreateDate;
	public static volatile SingularAttribute<IfdCertificate, String> customDeclarationNo;
	public static volatile SingularAttribute<IfdCertificate, String> importerGateCode;
	public static volatile SingularAttribute<IfdCertificate, Date> comingDateFrom;
	public static volatile SingularAttribute<IfdCertificate, Long> updateBy;
	public static volatile SingularAttribute<IfdCertificate, Date> certifaicteFilesNo;
	public static volatile SingularAttribute<IfdCertificate, String> exporterPhone;
	public static volatile SingularAttribute<IfdCertificate, String> checkPlace;
	public static volatile SingularAttribute<IfdCertificate, Date> createDate;
	public static volatile SingularAttribute<IfdCertificate, String> goodsOwnerPhone;
	public static volatile SingularAttribute<IfdCertificate, String> checkDeptCode;
	public static volatile SingularAttribute<IfdCertificate, String> signName;
	public static volatile SingularAttribute<IfdCertificate, String> goodsOwnerEmail;
	public static volatile SingularAttribute<IfdCertificate, String> exporterGateCode;
	public static volatile SingularAttribute<IfdCertificate, Short> isDelete;
	public static volatile SingularAttribute<IfdCertificate, String> fileCode;
	public static volatile SingularAttribute<IfdCertificate, String> certificateStatusName;
	public static volatile SingularAttribute<IfdCertificate, Date> checkTimeFrom;
	public static volatile SingularAttribute<IfdCertificate, String> certificateCreateBy;
	public static volatile SingularAttribute<IfdCertificate, String> goodsOwnerFax;
	public static volatile SingularAttribute<IfdCertificate, Long> createBy;
	public static volatile SingularAttribute<IfdCertificate, String> exporterGateName;
	public static volatile SingularAttribute<IfdCertificate, String> importerGateName;
	public static volatile SingularAttribute<IfdCertificate, Date> checkTimeTo;
	public static volatile SingularAttribute<IfdCertificate, String> exporterName;
	public static volatile SingularAttribute<IfdCertificate, Long> fileId;
	public static volatile SingularAttribute<IfdCertificate, Long> status;

}

