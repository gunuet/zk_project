package com.viettel.module.importfood.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdDept.class)
public abstract class IfdDept_ {

	public static volatile SingularAttribute<IfdDept, String> bank;
	public static volatile SingularAttribute<IfdDept, Long> year;
	public static volatile SingularAttribute<IfdDept, String> deptcode;
	public static volatile SingularAttribute<IfdDept, Long> id;
	public static volatile SingularAttribute<IfdDept, Long> numberincre;
	public static volatile SingularAttribute<IfdDept, Long> accountNumber;

}

