package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdEvaluationComment.class)
public abstract class IfdEvaluationComment_ {

	public static volatile SingularAttribute<IfdEvaluationComment, Long> evaluationId;
	public static volatile SingularAttribute<IfdEvaluationComment, Long> createBy;
	public static volatile SingularAttribute<IfdEvaluationComment, Date> updateDate;
	public static volatile SingularAttribute<IfdEvaluationComment, Long> updateBy;
	public static volatile SingularAttribute<IfdEvaluationComment, Long> evaluationCommentId;
	public static volatile SingularAttribute<IfdEvaluationComment, String> contentEvaluation;
	public static volatile SingularAttribute<IfdEvaluationComment, Long> status;
	public static volatile SingularAttribute<IfdEvaluationComment, Date> createDate;

}

