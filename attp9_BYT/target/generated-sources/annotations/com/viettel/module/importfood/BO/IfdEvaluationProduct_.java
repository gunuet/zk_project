package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdEvaluationProduct.class)
public abstract class IfdEvaluationProduct_ {

	public static volatile SingularAttribute<IfdEvaluationProduct, String> reason;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> note;
	public static volatile SingularAttribute<IfdEvaluationProduct, Date> updateDate;
	public static volatile SingularAttribute<IfdEvaluationProduct, Long> productCheckMethod;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> handlingMeasuresName;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> productGroupName;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> manufacturerAddress;
	public static volatile SingularAttribute<IfdEvaluationProduct, Long> evaluationProductId;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> productName;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> manufacturer;
	public static volatile SingularAttribute<IfdEvaluationProduct, Long> evaluationId;
	public static volatile SingularAttribute<IfdEvaluationProduct, Long> checkResultCode;
	public static volatile SingularAttribute<IfdEvaluationProduct, Long> createBy;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> productCode;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> productGroupCode;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> checkMethodConfirmNo;
	public static volatile SingularAttribute<IfdEvaluationProduct, Long> updateBy;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> checkResultName;
	public static volatile SingularAttribute<IfdEvaluationProduct, Long> handlingMeasuresCode;
	public static volatile SingularAttribute<IfdEvaluationProduct, String> confirmAnnounceNo;
	public static volatile SingularAttribute<IfdEvaluationProduct, Long> status;
	public static volatile SingularAttribute<IfdEvaluationProduct, Date> createDate;

}

