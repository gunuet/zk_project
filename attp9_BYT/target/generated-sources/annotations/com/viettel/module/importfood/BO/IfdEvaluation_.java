package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdEvaluation.class)
public abstract class IfdEvaluation_ {

	public static volatile SingularAttribute<IfdEvaluation, Long> evaluationId;
	public static volatile SingularAttribute<IfdEvaluation, Long> createBy;
	public static volatile SingularAttribute<IfdEvaluation, Date> updateDate;
	public static volatile SingularAttribute<IfdEvaluation, Long> updateBy;
	public static volatile SingularAttribute<IfdEvaluation, String> evaluationContent;
	public static volatile SingularAttribute<IfdEvaluation, Long> fileId;
	public static volatile SingularAttribute<IfdEvaluation, Long> leaderId;
	public static volatile SingularAttribute<IfdEvaluation, Long> status;
	public static volatile SingularAttribute<IfdEvaluation, Date> createDate;

}

