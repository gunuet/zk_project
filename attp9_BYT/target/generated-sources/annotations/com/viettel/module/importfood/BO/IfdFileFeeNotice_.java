package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdFileFeeNotice.class)
public abstract class IfdFileFeeNotice_ {

	public static volatile SingularAttribute<IfdFileFeeNotice, String> deptName;
	public static volatile SingularAttribute<IfdFileFeeNotice, Date> updateDate;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> totalOfFee;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> feeNoticeId;
	public static volatile SingularAttribute<IfdFileFeeNotice, String> fileCode;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> accountNumber;
	public static volatile SingularAttribute<IfdFileFeeNotice, String> bank;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> createBy;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> numberOfProduct;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> updateBy;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> feeDefaut;
	public static volatile SingularAttribute<IfdFileFeeNotice, String> deptCode;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> fileId;
	public static volatile SingularAttribute<IfdFileFeeNotice, Long> status;
	public static volatile SingularAttribute<IfdFileFeeNotice, Date> createDate;

}

