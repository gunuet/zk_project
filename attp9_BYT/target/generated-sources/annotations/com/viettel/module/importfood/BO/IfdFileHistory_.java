package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdFileHistory.class)
public abstract class IfdFileHistory_ {

	public static volatile SingularAttribute<IfdFileHistory, String> note;
	public static volatile SingularAttribute<IfdFileHistory, Long> fileHistoryId;
	public static volatile SingularAttribute<IfdFileHistory, Date> updateDate;
	public static volatile SingularAttribute<IfdFileHistory, Date> endDate;
	public static volatile SingularAttribute<IfdFileHistory, String> receiverName;
	public static volatile SingularAttribute<IfdFileHistory, String> fileStatusName;
	public static volatile SingularAttribute<IfdFileHistory, String> senderDepartment;
	public static volatile SingularAttribute<IfdFileHistory, String> fileCode;
	public static volatile SingularAttribute<IfdFileHistory, String> content;
	public static volatile SingularAttribute<IfdFileHistory, String> senderName;
	public static volatile SingularAttribute<IfdFileHistory, Long> createBy;
	public static volatile SingularAttribute<IfdFileHistory, Long> fileResultId;
	public static volatile SingularAttribute<IfdFileHistory, Long> fileStatus;
	public static volatile SingularAttribute<IfdFileHistory, Long> updateBy;
	public static volatile SingularAttribute<IfdFileHistory, String> receiverDepartment;
	public static volatile SingularAttribute<IfdFileHistory, Long> fileId;
	public static volatile SingularAttribute<IfdFileHistory, Long> status;
	public static volatile SingularAttribute<IfdFileHistory, Date> createDate;

}

