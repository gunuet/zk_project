package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdFilePayment.class)
public abstract class IfdFilePayment_ {

	public static volatile SingularAttribute<IfdFilePayment, String> deptName;
	public static volatile SingularAttribute<IfdFilePayment, Date> updateDate;
	public static volatile SingularAttribute<IfdFilePayment, String> onlPaymentDeptName;
	public static volatile SingularAttribute<IfdFilePayment, Double> cost;
	public static volatile SingularAttribute<IfdFilePayment, String> fileCode;
	public static volatile SingularAttribute<IfdFilePayment, String> onlPaymentDeptCode;
	public static volatile SingularAttribute<IfdFilePayment, Long> paymentType;
	public static volatile SingularAttribute<IfdFilePayment, Long> createBy;
	public static volatile SingularAttribute<IfdFilePayment, String> processName;
	public static volatile SingularAttribute<IfdFilePayment, Long> updateBy;
	public static volatile SingularAttribute<IfdFilePayment, Long> paymentId;
	public static volatile SingularAttribute<IfdFilePayment, Date> processDate;
	public static volatile SingularAttribute<IfdFilePayment, String> bankNo;
	public static volatile SingularAttribute<IfdFilePayment, String> deptCode;
	public static volatile SingularAttribute<IfdFilePayment, Long> fileId;
	public static volatile SingularAttribute<IfdFilePayment, Long> status;
	public static volatile SingularAttribute<IfdFilePayment, Date> createDate;

}

