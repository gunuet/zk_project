package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdFileProcessUser.class)
public abstract class IfdFileProcessUser_ {

	public static volatile SingularAttribute<IfdFileProcessUser, String> sendDeptCode;
	public static volatile SingularAttribute<IfdFileProcessUser, Date> updateDate;
	public static volatile SingularAttribute<IfdFileProcessUser, String> sendRoleCode;
	public static volatile SingularAttribute<IfdFileProcessUser, String> receiveRoleCode;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> fileProcessUserId;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> sendUserId;
	public static volatile SingularAttribute<IfdFileProcessUser, String> receiveDeptCode;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> receiveUserId;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> createBy;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> sendDeptId;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> sendRoleId;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> receiveRoleId;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> updateBy;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> receiveDeptId;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> fileId;
	public static volatile SingularAttribute<IfdFileProcessUser, Long> status;
	public static volatile SingularAttribute<IfdFileProcessUser, Date> createDate;

}

