package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdFileProduct.class)
public abstract class IfdFileProduct_ {

	public static volatile SingularAttribute<IfdFileProduct, Date> updateDate;
	public static volatile SingularAttribute<IfdFileProduct, Long> productCheckMethod;
	public static volatile SingularAttribute<IfdFileProduct, Long> fileProductId;
	public static volatile SingularAttribute<IfdFileProduct, String> fileCode;
	public static volatile SingularAttribute<IfdFileProduct, String> productGroupName;
	public static volatile SingularAttribute<IfdFileProduct, String> manufacturerAddress;
	public static volatile SingularAttribute<IfdFileProduct, String> productName;
	public static volatile SingularAttribute<IfdFileProduct, String> manufacturer;
	public static volatile SingularAttribute<IfdFileProduct, Long> createBy;
	public static volatile SingularAttribute<IfdFileProduct, String> productCode;
	public static volatile SingularAttribute<IfdFileProduct, String> productGroupCode;
	public static volatile SingularAttribute<IfdFileProduct, String> checkMethodConfirmNo;
	public static volatile SingularAttribute<IfdFileProduct, Long> updateBy;
	public static volatile SingularAttribute<IfdFileProduct, Long> fileId;
	public static volatile SingularAttribute<IfdFileProduct, String> confirmAnnounceNo;
	public static volatile SingularAttribute<IfdFileProduct, Long> status;
	public static volatile SingularAttribute<IfdFileProduct, Date> createDate;

}

