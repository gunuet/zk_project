package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdFileRequestCancel.class)
public abstract class IfdFileRequestCancel_ {

	public static volatile SingularAttribute<IfdFileRequestCancel, String> reason;
	public static volatile SingularAttribute<IfdFileRequestCancel, Date> cancelDate;
	public static volatile SingularAttribute<IfdFileRequestCancel, Long> createBy;
	public static volatile SingularAttribute<IfdFileRequestCancel, Date> updateDate;
	public static volatile SingularAttribute<IfdFileRequestCancel, Long> fileRequestCancelId;
	public static volatile SingularAttribute<IfdFileRequestCancel, Long> updateBy;
	public static volatile SingularAttribute<IfdFileRequestCancel, String> fileCode;
	public static volatile SingularAttribute<IfdFileRequestCancel, Long> fileId;
	public static volatile SingularAttribute<IfdFileRequestCancel, Long> status;
	public static volatile SingularAttribute<IfdFileRequestCancel, Date> createDate;

}

