package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdFileResult.class)
public abstract class IfdFileResult_ {

	public static volatile SingularAttribute<IfdFileResult, String> deptName;
	public static volatile SingularAttribute<IfdFileResult, Date> updateDate;
	public static volatile SingularAttribute<IfdFileResult, String> fileStatusName;
	public static volatile SingularAttribute<IfdFileResult, String> fileCode;
	public static volatile SingularAttribute<IfdFileResult, String> processContent;
	public static volatile SingularAttribute<IfdFileResult, Long> createBy;
	public static volatile SingularAttribute<IfdFileResult, Long> fileResultId;
	public static volatile SingularAttribute<IfdFileResult, String> processName;
	public static volatile SingularAttribute<IfdFileResult, String> receiveNo;
	public static volatile SingularAttribute<IfdFileResult, Long> fileStatus;
	public static volatile SingularAttribute<IfdFileResult, Long> updateBy;
	public static volatile SingularAttribute<IfdFileResult, Date> processDate;
	public static volatile SingularAttribute<IfdFileResult, String> deptCode;
	public static volatile SingularAttribute<IfdFileResult, Long> fileId;
	public static volatile SingularAttribute<IfdFileResult, Long> status;
	public static volatile SingularAttribute<IfdFileResult, Date> createDate;

}

