package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdFile.class)
public abstract class IfdFile_ {

	public static volatile SingularAttribute<IfdFile, String> responesiblePersonName;
	public static volatile SingularAttribute<IfdFile, String> responesiblePersonFax;
	public static volatile SingularAttribute<IfdFile, Date> updateDate;
	public static volatile SingularAttribute<IfdFile, String> documentType;
	public static volatile SingularAttribute<IfdFile, String> responesiblePersonAddress;
	public static volatile SingularAttribute<IfdFile, String> exporterAddress;
	public static volatile SingularAttribute<IfdFile, Date> comingDateTo;
	public static volatile SingularAttribute<IfdFile, String> checkDeptName;
	public static volatile SingularAttribute<IfdFile, String> responesiblePersonPhone;
	public static volatile SingularAttribute<IfdFile, String> exporterEmail;
	public static volatile SingularAttribute<IfdFile, Date> fileCreateDate;
	public static volatile SingularAttribute<IfdFile, Long> checkMethod;
	public static volatile SingularAttribute<IfdFile, String> goodsOwnerAddress;
	public static volatile SingularAttribute<IfdFile, String> goodsOwnerName;
	public static volatile SingularAttribute<IfdFile, String> exporterFax;
	public static volatile SingularAttribute<IfdFile, Long> checkDeptId;
	public static volatile SingularAttribute<IfdFile, String> responesiblePersonEmail;
	public static volatile SingularAttribute<IfdFile, Date> signDate;
	public static volatile SingularAttribute<IfdFile, String> customDeclarationNo;
	public static volatile SingularAttribute<IfdFile, String> importerGateCode;
	public static volatile SingularAttribute<IfdFile, Date> comingDateFrom;
	public static volatile SingularAttribute<IfdFile, Long> fileStatus;
	public static volatile SingularAttribute<IfdFile, Long> updateBy;
	public static volatile SingularAttribute<IfdFile, String> exporterPhone;
	public static volatile SingularAttribute<IfdFile, String> checkPlace;
	public static volatile SingularAttribute<IfdFile, Date> createDate;
	public static volatile SingularAttribute<IfdFile, String> goodsOwnerPhone;
	public static volatile SingularAttribute<IfdFile, String> fileCreateBy;
	public static volatile SingularAttribute<IfdFile, String> checkDeptCode;
	public static volatile SingularAttribute<IfdFile, String> signName;
	public static volatile SingularAttribute<IfdFile, String> goodsOwnerEmail;
	public static volatile SingularAttribute<IfdFile, String> exporterGateCode;
	public static volatile SingularAttribute<IfdFile, String> fileStatusName;
	public static volatile SingularAttribute<IfdFile, Long> isDelete;
	public static volatile SingularAttribute<IfdFile, String> fileCode;
	public static volatile SingularAttribute<IfdFile, Date> fileModifiedDate;
	public static volatile SingularAttribute<IfdFile, Date> checkTimeFrom;
	public static volatile SingularAttribute<IfdFile, String> goodsOwnerFax;
	public static volatile SingularAttribute<IfdFile, Long> createBy;
	public static volatile SingularAttribute<IfdFile, String> exporterGateName;
	public static volatile SingularAttribute<IfdFile, String> importerGateName;
	public static volatile SingularAttribute<IfdFile, Date> checkTimeTo;
	public static volatile SingularAttribute<IfdFile, String> exporterName;
	public static volatile SingularAttribute<IfdFile, Long> fileId;
	public static volatile SingularAttribute<IfdFile, Long> status;

}

