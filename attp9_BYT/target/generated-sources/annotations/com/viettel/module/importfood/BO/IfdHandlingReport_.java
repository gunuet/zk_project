package com.viettel.module.importfood.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(IfdHandlingReport.class)
public abstract class IfdHandlingReport_ {

	public static volatile SingularAttribute<IfdHandlingReport, Long> createBy;
	public static volatile SingularAttribute<IfdHandlingReport, Date> updateDate;
	public static volatile SingularAttribute<IfdHandlingReport, Long> updateBy;
	public static volatile SingularAttribute<IfdHandlingReport, Long> handlingReportId;
	public static volatile SingularAttribute<IfdHandlingReport, String> fileCode;
	public static volatile SingularAttribute<IfdHandlingReport, String> content;
	public static volatile SingularAttribute<IfdHandlingReport, Long> fileId;
	public static volatile SingularAttribute<IfdHandlingReport, Long> status;
	public static volatile SingularAttribute<IfdHandlingReport, Date> createDate;

}

