package com.viettel.module.payment.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bill.class)
public abstract class Bill_ {

	public static volatile SingularAttribute<Bill, String> billSign;
	public static volatile SingularAttribute<Bill, String> code;
	public static volatile SingularAttribute<Bill, String> confirmUserName;
	public static volatile SingularAttribute<Bill, Long> creatorId;
	public static volatile SingularAttribute<Bill, String> confirmContent;
	public static volatile SingularAttribute<Bill, String> creatorAddress;
	public static volatile SingularAttribute<Bill, Long> confirmUserId;
	public static volatile SingularAttribute<Bill, String> createtorName;
	public static volatile SingularAttribute<Bill, Date> confirmDate;
	public static volatile SingularAttribute<Bill, Long> total;
	public static volatile SingularAttribute<Bill, Long> paymentTypeId;
	public static volatile SingularAttribute<Bill, String> paymentInfoKeypay;
	public static volatile SingularAttribute<Bill, Long> billId;
	public static volatile SingularAttribute<Bill, Date> paymentDate;
	public static volatile SingularAttribute<Bill, Date> createDate;
	public static volatile SingularAttribute<Bill, Long> status;
	public static volatile SingularAttribute<Bill, String> keypayCode;

}

