package com.viettel.module.payment.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PaymentInfo.class)
public abstract class PaymentInfo_ {

	public static volatile SingularAttribute<PaymentInfo, Long> phase;
	public static volatile SingularAttribute<PaymentInfo, String> note;
	public static volatile SingularAttribute<PaymentInfo, String> deptName;
	public static volatile SingularAttribute<PaymentInfo, String> paymentConfirm;
	public static volatile SingularAttribute<PaymentInfo, Date> updateDate;
	public static volatile SingularAttribute<PaymentInfo, Long> cost;
	public static volatile SingularAttribute<PaymentInfo, String> paymentPerson;
	public static volatile SingularAttribute<PaymentInfo, String> billCode;
	public static volatile SingularAttribute<PaymentInfo, Long> paymentInfoId;
	public static volatile SingularAttribute<PaymentInfo, Long> isActive;
	public static volatile SingularAttribute<PaymentInfo, Long> feeId;
	public static volatile SingularAttribute<PaymentInfo, String> paymentActionUser;
	public static volatile SingularAttribute<PaymentInfo, Long> paymentTypeId;
	public static volatile SingularAttribute<PaymentInfo, String> paymentCode;
	public static volatile SingularAttribute<PaymentInfo, Long> billId;
	public static volatile SingularAttribute<PaymentInfo, String> feeName;
	public static volatile SingularAttribute<PaymentInfo, String> bankNo;
	public static volatile SingularAttribute<PaymentInfo, Long> paymentActionUserId;
	public static volatile SingularAttribute<PaymentInfo, Date> paymentDate;
	public static volatile SingularAttribute<PaymentInfo, Date> dateConfirm;
	public static volatile SingularAttribute<PaymentInfo, String> deptCode;
	public static volatile SingularAttribute<PaymentInfo, Long> fileId;
	public static volatile SingularAttribute<PaymentInfo, Long> status;
	public static volatile SingularAttribute<PaymentInfo, Date> createDate;

}

