package com.viettel.module.payment.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VCosPaymentInfo.class)
public abstract class VCosPaymentInfo_ {

	public static volatile SingularAttribute<VCosPaymentInfo, Long> createDeptId;
	public static volatile SingularAttribute<VCosPaymentInfo, String> note;
	public static volatile SingularAttribute<VCosPaymentInfo, String> paymentConfirm;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> statuspayment;
	public static volatile SingularAttribute<VCosPaymentInfo, String> businessName;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> creatorId;
	public static volatile SingularAttribute<VCosPaymentInfo, String> creatorName;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> isActive;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> feeId;
	public static volatile SingularAttribute<VCosPaymentInfo, String> productName;
	public static volatile SingularAttribute<VCosPaymentInfo, String> paymentActionUser;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> paymentTypeId;
	public static volatile SingularAttribute<VCosPaymentInfo, String> businessAddress;
	public static volatile SingularAttribute<VCosPaymentInfo, String> cosmeticNo;
	public static volatile SingularAttribute<VCosPaymentInfo, Date> dateConfirm;
	public static volatile SingularAttribute<VCosPaymentInfo, Date> createDate;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> phase;
	public static volatile SingularAttribute<VCosPaymentInfo, String> createDeptName;
	public static volatile SingularAttribute<VCosPaymentInfo, String> brandName;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> cost;
	public static volatile SingularAttribute<VCosPaymentInfo, Date> modifyDate;
	public static volatile SingularAttribute<VCosPaymentInfo, String> paymentPerson;
	public static volatile SingularAttribute<VCosPaymentInfo, String> billCode;
	public static volatile SingularAttribute<VCosPaymentInfo, String> taxCode;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> paymentInfoId;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> cosFileId;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> documentTypeCode;
	public static volatile SingularAttribute<VCosPaymentInfo, String> paymentCode;
	public static volatile SingularAttribute<VCosPaymentInfo, String> nswFileCode;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> billId;
	public static volatile SingularAttribute<VCosPaymentInfo, String> feeName;
	public static volatile SingularAttribute<VCosPaymentInfo, String> businessFax;
	public static volatile SingularAttribute<VCosPaymentInfo, String> businessPhone;
	public static volatile SingularAttribute<VCosPaymentInfo, Date> paymentDate;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> fileType;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> statusfile;
	public static volatile SingularAttribute<VCosPaymentInfo, Long> fileId;

}

