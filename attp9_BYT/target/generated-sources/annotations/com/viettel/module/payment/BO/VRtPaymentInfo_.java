package com.viettel.module.payment.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VRtPaymentInfo.class)
public abstract class VRtPaymentInfo_ {

	public static volatile SingularAttribute<VRtPaymentInfo, Long> createDeptId;
	public static volatile SingularAttribute<VRtPaymentInfo, String> note;
	public static volatile SingularAttribute<VRtPaymentInfo, String> paymentConfirm;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> statuspayment;
	public static volatile SingularAttribute<VRtPaymentInfo, String> businessName;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> creatorId;
	public static volatile SingularAttribute<VRtPaymentInfo, String> creatorName;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> isActive;
	public static volatile SingularAttribute<VRtPaymentInfo, String> rapidTestNo;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> feeId;
	public static volatile SingularAttribute<VRtPaymentInfo, String> paymentActionUser;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> paymentTypeId;
	public static volatile SingularAttribute<VRtPaymentInfo, String> rapidTestName;
	public static volatile SingularAttribute<VRtPaymentInfo, String> businessAddress;
	public static volatile SingularAttribute<VRtPaymentInfo, Date> dateConfirm;
	public static volatile SingularAttribute<VRtPaymentInfo, Date> createDate;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> phase;
	public static volatile SingularAttribute<VRtPaymentInfo, String> createDeptName;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> cost;
	public static volatile SingularAttribute<VRtPaymentInfo, Date> modifyDate;
	public static volatile SingularAttribute<VRtPaymentInfo, String> paymentPerson;
	public static volatile SingularAttribute<VRtPaymentInfo, String> billCode;
	public static volatile SingularAttribute<VRtPaymentInfo, String> taxCode;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> paymentInfoId;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> documentTypeCode;
	public static volatile SingularAttribute<VRtPaymentInfo, String> paymentCode;
	public static volatile SingularAttribute<VRtPaymentInfo, String> nswFileCode;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> billId;
	public static volatile SingularAttribute<VRtPaymentInfo, String> feeName;
	public static volatile SingularAttribute<VRtPaymentInfo, String> businessFax;
	public static volatile SingularAttribute<VRtPaymentInfo, String> businessPhone;
	public static volatile SingularAttribute<VRtPaymentInfo, Date> paymentDate;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> fileType;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> statusfile;
	public static volatile SingularAttribute<VRtPaymentInfo, Long> fileId;

}

