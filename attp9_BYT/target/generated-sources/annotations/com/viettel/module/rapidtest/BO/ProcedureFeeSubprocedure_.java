package com.viettel.module.rapidtest.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProcedureFeeSubprocedure.class)
public abstract class ProcedureFeeSubprocedure_ {

	public static volatile SingularAttribute<ProcedureFeeSubprocedure, Long> phase;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, String> subProcedureName;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, Long> cost;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, String> procedureName;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, String> feeName;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, Long> id;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, Long> isActive;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, Long> procedureId;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, Long> feeId;
	public static volatile SingularAttribute<ProcedureFeeSubprocedure, Long> subProcedureId;

}

