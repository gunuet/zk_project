package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtAdditionalRequest.class)
public abstract class RtAdditionalRequest_ {

	public static volatile SingularAttribute<RtAdditionalRequest, Long> createDeptId;
	public static volatile SingularAttribute<RtAdditionalRequest, String> createDeptName;
	public static volatile SingularAttribute<RtAdditionalRequest, Long> creatorId;
	public static volatile SingularAttribute<RtAdditionalRequest, String> creatorName;
	public static volatile SingularAttribute<RtAdditionalRequest, Long> isActive;
	public static volatile SingularAttribute<RtAdditionalRequest, Long> version;
	public static volatile SingularAttribute<RtAdditionalRequest, String> content;
	public static volatile SingularAttribute<RtAdditionalRequest, Long> additionalRequestId;
	public static volatile SingularAttribute<RtAdditionalRequest, Long> commentType;
	public static volatile SingularAttribute<RtAdditionalRequest, Long> attachId;
	public static volatile SingularAttribute<RtAdditionalRequest, Date> createDate;
	public static volatile SingularAttribute<RtAdditionalRequest, Long> fileId;
	public static volatile SingularAttribute<RtAdditionalRequest, Long> status;

}

