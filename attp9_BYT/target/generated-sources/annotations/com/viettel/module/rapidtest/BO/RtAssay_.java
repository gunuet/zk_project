package com.viettel.module.rapidtest.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtAssay.class)
public abstract class RtAssay_ {

	public static volatile SingularAttribute<RtAssay, String> code;
	public static volatile SingularAttribute<RtAssay, String> address;
	public static volatile SingularAttribute<RtAssay, String> phone;
	public static volatile SingularAttribute<RtAssay, Long> createdBy;
	public static volatile SingularAttribute<RtAssay, String> name;
	public static volatile SingularAttribute<RtAssay, Long> id;
	public static volatile SingularAttribute<RtAssay, String> fax;
	public static volatile SingularAttribute<RtAssay, Long> isActive;

}

