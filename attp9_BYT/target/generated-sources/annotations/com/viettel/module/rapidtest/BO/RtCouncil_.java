package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtCouncil.class)
public abstract class RtCouncil_ {

	public static volatile SingularAttribute<RtCouncil, String> role;
	public static volatile SingularAttribute<RtCouncil, Date> createdDate;
	public static volatile SingularAttribute<RtCouncil, String> code;
	public static volatile SingularAttribute<RtCouncil, Long> createdBy;
	public static volatile SingularAttribute<RtCouncil, String> name;
	public static volatile SingularAttribute<RtCouncil, Long> orderBy;
	public static volatile SingularAttribute<RtCouncil, Long> id;
	public static volatile SingularAttribute<RtCouncil, String> position;
	public static volatile SingularAttribute<RtCouncil, Long> isActive;

}

