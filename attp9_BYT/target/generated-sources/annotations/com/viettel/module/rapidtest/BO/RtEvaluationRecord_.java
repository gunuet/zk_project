package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtEvaluationRecord.class)
public abstract class RtEvaluationRecord_ {

	public static volatile SingularAttribute<RtEvaluationRecord, String> businessName;
	public static volatile SingularAttribute<RtEvaluationRecord, String> userName;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> isActive;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> userId;
	public static volatile SingularAttribute<RtEvaluationRecord, String> productName;
	public static volatile SingularAttribute<RtEvaluationRecord, String> legalContent;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> processId;
	public static volatile SingularAttribute<RtEvaluationRecord, String> mainContent;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> legal;
	public static volatile SingularAttribute<RtEvaluationRecord, String> businessAddress;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> attachId;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> evaluationRecordId;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> fileType;
	public static volatile SingularAttribute<RtEvaluationRecord, Date> createDate;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> fileId;
	public static volatile SingularAttribute<RtEvaluationRecord, Long> status;

}

