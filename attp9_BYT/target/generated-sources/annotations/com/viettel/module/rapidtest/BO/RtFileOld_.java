package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtFileOld.class)
public abstract class RtFileOld_ {

	public static volatile SingularAttribute<RtFileOld, String> signPlace;
	public static volatile SingularAttribute<RtFileOld, String> circulatingExtensionNo;
	public static volatile SingularAttribute<RtFileOld, String> rangeOfApplications;
	public static volatile SingularAttribute<RtFileOld, String> precision;
	public static volatile SingularAttribute<RtFileOld, Long> extensionNo;
	public static volatile SingularAttribute<RtFileOld, String> description;
	public static volatile SingularAttribute<RtFileOld, Date> signDate;
	public static volatile SingularAttribute<RtFileOld, String> rapidTestNo;
	public static volatile SingularAttribute<RtFileOld, String> placeOfManufacture;
	public static volatile SingularAttribute<RtFileOld, String> rapidTestChangeNo;
	public static volatile SingularAttribute<RtFileOld, Long> effective;
	public static volatile SingularAttribute<RtFileOld, String> rapidTestName;
	public static volatile SingularAttribute<RtFileOld, String> attachmentsInfo;
	public static volatile SingularAttribute<RtFileOld, Long> propertiesTests;
	public static volatile SingularAttribute<RtFileOld, Date> dateEffect;
	public static volatile SingularAttribute<RtFileOld, Long> id;
	public static volatile SingularAttribute<RtFileOld, String> shelfLife;
	public static volatile SingularAttribute<RtFileOld, String> operatingPrinciples;
	public static volatile SingularAttribute<RtFileOld, String> signName;
	public static volatile SingularAttribute<RtFileOld, String> storageConditions;
	public static volatile SingularAttribute<RtFileOld, String> rapidTestCode;
	public static volatile SingularAttribute<RtFileOld, Long> documentTypeCode;
	public static volatile SingularAttribute<RtFileOld, String> circulatingRapidTestNo;
	public static volatile SingularAttribute<RtFileOld, String> contents;
	public static volatile SingularAttribute<RtFileOld, String> targetTesting;
	public static volatile SingularAttribute<RtFileOld, String> nswFileCode;
	public static volatile SingularAttribute<RtFileOld, String> signedData;
	public static volatile SingularAttribute<RtFileOld, String> limitDevelopment;
	public static volatile SingularAttribute<RtFileOld, Long> statusCode;
	public static volatile SingularAttribute<RtFileOld, Date> dateIssue;
	public static volatile SingularAttribute<RtFileOld, String> pakaging;
	public static volatile SingularAttribute<RtFileOld, String> otherInfos;
	public static volatile SingularAttribute<RtFileOld, Long> fileId;

}

