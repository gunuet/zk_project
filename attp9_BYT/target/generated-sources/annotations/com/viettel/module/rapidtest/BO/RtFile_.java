package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtFile.class)
public abstract class RtFile_ {

	public static volatile SingularAttribute<RtFile, String> signPlace;
	public static volatile SingularAttribute<RtFile, String> circulatingExtensionNo;
	public static volatile SingularAttribute<RtFile, String> rangeOfApplications;
	public static volatile SingularAttribute<RtFile, String> precision;
	public static volatile SingularAttribute<RtFile, Long> extensionNo;
	public static volatile SingularAttribute<RtFile, String> addressOfManufacture;
	public static volatile SingularAttribute<RtFile, String> description;
	public static volatile SingularAttribute<RtFile, Date> signDate;
	public static volatile SingularAttribute<RtFile, String> rapidTestNo;
	public static volatile SingularAttribute<RtFile, String> placeOfManufacture;
	public static volatile SingularAttribute<RtFile, String> signCirculatingDate;
	public static volatile SingularAttribute<RtFile, String> rapidTestChangeNo;
	public static volatile SingularAttribute<RtFile, String> rapidTestName;
	public static volatile SingularAttribute<RtFile, String> attachmentsInfo;
	public static volatile SingularAttribute<RtFile, Long> propertiesTests;
	public static volatile SingularAttribute<RtFile, Date> dateEffect;
	public static volatile SingularAttribute<RtFile, String> stateOfManufacture;
	public static volatile SingularAttribute<RtFile, String> nameOfState;
	public static volatile SingularAttribute<RtFile, String> shelfLife;
	public static volatile SingularAttribute<RtFile, Long> rtFileId;
	public static volatile SingularAttribute<RtFile, String> operatingPrinciples;
	public static volatile SingularAttribute<RtFile, String> signName;
	public static volatile SingularAttribute<RtFile, String> storageConditions;
	public static volatile SingularAttribute<RtFile, String> manufacture;
	public static volatile SingularAttribute<RtFile, String> rapidTestCode;
	public static volatile SingularAttribute<RtFile, Long> documentTypeCode;
	public static volatile SingularAttribute<RtFile, String> circulatingRapidTestNo;
	public static volatile SingularAttribute<RtFile, String> circulatingNo;
	public static volatile SingularAttribute<RtFile, String> contents;
	public static volatile SingularAttribute<RtFile, String> targetTesting;
	public static volatile SingularAttribute<RtFile, String> nswFileCode;
	public static volatile SingularAttribute<RtFile, String> signedData;
	public static volatile SingularAttribute<RtFile, String> limitDevelopment;
	public static volatile SingularAttribute<RtFile, String> signPlaceName;
	public static volatile SingularAttribute<RtFile, Long> shelfLifeMonth;
	public static volatile SingularAttribute<RtFile, Long> fileId;
	public static volatile SingularAttribute<RtFile, Date> dateIssue;
	public static volatile SingularAttribute<RtFile, String> pakaging;
	public static volatile SingularAttribute<RtFile, String> otherInfos;
	public static volatile SingularAttribute<RtFile, String> withdraw;

}

