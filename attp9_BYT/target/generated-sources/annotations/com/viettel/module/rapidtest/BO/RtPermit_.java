package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtPermit.class)
public abstract class RtPermit_ {

	public static volatile SingularAttribute<RtPermit, String> nationName;
	public static volatile SingularAttribute<RtPermit, String> manufactureAdd;
	public static volatile SingularAttribute<RtPermit, Long> permitId;
	public static volatile SingularAttribute<RtPermit, String> businessName;
	public static volatile SingularAttribute<RtPermit, Long> businessId;
	public static volatile SingularAttribute<RtPermit, Date> receiveDate;
	public static volatile SingularAttribute<RtPermit, String> telephone;
	public static volatile SingularAttribute<RtPermit, Date> signDate;
	public static volatile SingularAttribute<RtPermit, Long> isActive;
	public static volatile SingularAttribute<RtPermit, String> productName;
	public static volatile SingularAttribute<RtPermit, String> signerName;
	public static volatile SingularAttribute<RtPermit, String> receiveNo;
	public static volatile SingularAttribute<RtPermit, String> manufactureName;
	public static volatile SingularAttribute<RtPermit, String> receiveDeptName;
	public static volatile SingularAttribute<RtPermit, String> fax;
	public static volatile SingularAttribute<RtPermit, String> matchingTarget;
	public static volatile SingularAttribute<RtPermit, Long> attachId;
	public static volatile SingularAttribute<RtPermit, String> email;
	public static volatile SingularAttribute<RtPermit, Date> effectiveDate;
	public static volatile SingularAttribute<RtPermit, Long> fileId;
	public static volatile SingularAttribute<RtPermit, Long> status;

}

