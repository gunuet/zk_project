package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtTargetTesting.class)
public abstract class RtTargetTesting_ {

	public static volatile SingularAttribute<RtTargetTesting, Date> createdDate;
	public static volatile SingularAttribute<RtTargetTesting, String> rangeOfApplications;
	public static volatile SingularAttribute<RtTargetTesting, Long> createdBy;
	public static volatile SingularAttribute<RtTargetTesting, String> limitDevelopment;
	public static volatile SingularAttribute<RtTargetTesting, String> precision;
	public static volatile SingularAttribute<RtTargetTesting, String> name;
	public static volatile SingularAttribute<RtTargetTesting, Long> id;
	public static volatile SingularAttribute<RtTargetTesting, Long> isActive;
	public static volatile SingularAttribute<RtTargetTesting, Long> fileId;

}

