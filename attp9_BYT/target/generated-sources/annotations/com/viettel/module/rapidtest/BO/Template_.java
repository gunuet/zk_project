package com.viettel.module.rapidtest.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Template.class)
public abstract class Template_ {

	public static volatile SingularAttribute<Template, Long> templateTypeId;
	public static volatile SingularAttribute<Template, String> deptName;
	public static volatile SingularAttribute<Template, String> templateName;
	public static volatile SingularAttribute<Template, String> procedureName;
	public static volatile SingularAttribute<Template, Long> deptId;
	public static volatile SingularAttribute<Template, Long> templateId;
	public static volatile SingularAttribute<Template, Long> attachId;
	public static volatile SingularAttribute<Template, String> templateTypeName;
	public static volatile SingularAttribute<Template, Long> isActive;
	public static volatile SingularAttribute<Template, Long> procedureId;

}

