package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFileRtAttachAll.class)
public abstract class VFileRtAttachAll_ {

	public static volatile SingularAttribute<VFileRtAttachAll, String> attachPath;
	public static volatile SingularAttribute<VFileRtAttachAll, Long> attachCat;
	public static volatile SingularAttribute<VFileRtAttachAll, Date> dateModify;
	public static volatile SingularAttribute<VFileRtAttachAll, Long> creatorId;
	public static volatile SingularAttribute<VFileRtAttachAll, Long> modifierId;
	public static volatile SingularAttribute<VFileRtAttachAll, Long> isSent;
	public static volatile SingularAttribute<VFileRtAttachAll, Long> isActive;
	public static volatile SingularAttribute<VFileRtAttachAll, Date> dateCreate;
	public static volatile SingularAttribute<VFileRtAttachAll, Integer> version;
	public static volatile SingularAttribute<VFileRtAttachAll, Long> attachType;
	public static volatile SingularAttribute<VFileRtAttachAll, String> attachCode;
	public static volatile SingularAttribute<VFileRtAttachAll, String> attachUrl;
	public static volatile SingularAttribute<VFileRtAttachAll, String> attachName;
	public static volatile SingularAttribute<VFileRtAttachAll, String> typeFileName;
	public static volatile SingularAttribute<VFileRtAttachAll, Long> attachId;
	public static volatile SingularAttribute<VFileRtAttachAll, Long> objectId;
	public static volatile SingularAttribute<VFileRtAttachAll, String> attachDes;

}

