package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFileRtAttach.class)
public abstract class VFileRtAttach_ {

	public static volatile SingularAttribute<VFileRtAttach, String> attachPath;
	public static volatile SingularAttribute<VFileRtAttach, Long> attachCat;
	public static volatile SingularAttribute<VFileRtAttach, Date> dateModify;
	public static volatile SingularAttribute<VFileRtAttach, Long> creatorId;
	public static volatile SingularAttribute<VFileRtAttach, Long> modifierId;
	public static volatile SingularAttribute<VFileRtAttach, Long> isActive;
	public static volatile SingularAttribute<VFileRtAttach, Date> dateCreate;
	public static volatile SingularAttribute<VFileRtAttach, Integer> version;
	public static volatile SingularAttribute<VFileRtAttach, Long> attachType;
	public static volatile SingularAttribute<VFileRtAttach, String> attachCode;
	public static volatile SingularAttribute<VFileRtAttach, String> attachUrl;
	public static volatile SingularAttribute<VFileRtAttach, String> attachName;
	public static volatile SingularAttribute<VFileRtAttach, String> typeFileName;
	public static volatile SingularAttribute<VFileRtAttach, Long> attachId;
	public static volatile SingularAttribute<VFileRtAttach, Long> objectId;

}

