package com.viettel.module.rapidtest.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFileRtfile.class)
public abstract class VFileRtfile_ {

	public static volatile SingularAttribute<VFileRtfile, String> fileName;
	public static volatile SingularAttribute<VFileRtfile, String> rangeOfApplications;
	public static volatile SingularAttribute<VFileRtfile, String> precision;
	public static volatile SingularAttribute<VFileRtfile, String> businessName;
	public static volatile SingularAttribute<VFileRtfile, Date> signDate;
	public static volatile SingularAttribute<VFileRtfile, Long> isActive;
	public static volatile SingularAttribute<VFileRtfile, String> rapidTestNo;
	public static volatile SingularAttribute<VFileRtfile, String> placeOfManufacture;
	public static volatile SingularAttribute<VFileRtfile, String> rapidTestChangeNo;
	public static volatile SingularAttribute<VFileRtfile, Long> parentFileId;
	public static volatile SingularAttribute<VFileRtfile, String> attachmentsInfo;
	public static volatile SingularAttribute<VFileRtfile, String> fileTypeName;
	public static volatile SingularAttribute<VFileRtfile, Long> id;
	public static volatile SingularAttribute<VFileRtfile, String> businessAddress;
	public static volatile SingularAttribute<VFileRtfile, String> operatingPrinciples;
	public static volatile SingularAttribute<VFileRtfile, Date> modifyDate;
	public static volatile SingularAttribute<VFileRtfile, String> fileCode;
	public static volatile SingularAttribute<VFileRtfile, String> taxCode;
	public static volatile SingularAttribute<VFileRtfile, Long> version;
	public static volatile SingularAttribute<VFileRtfile, String> rapidTestCode;
	public static volatile SingularAttribute<VFileRtfile, String> circulatingRapidTestNo;
	public static volatile SingularAttribute<VFileRtfile, Long> documentTypeCode;
	public static volatile SingularAttribute<VFileRtfile, String> contents;
	public static volatile SingularAttribute<VFileRtfile, String> targetTesting;
	public static volatile SingularAttribute<VFileRtfile, String> limitDevelopment;
	public static volatile SingularAttribute<VFileRtfile, Long> isTemp;
	public static volatile SingularAttribute<VFileRtfile, Long> shelfLifeMonth;
	public static volatile SingularAttribute<VFileRtfile, String> businessFax;
	public static volatile SingularAttribute<VFileRtfile, String> businessPhone;
	public static volatile SingularAttribute<VFileRtfile, Long> fileId;
	public static volatile SingularAttribute<VFileRtfile, Date> dateIssue;
	public static volatile SingularAttribute<VFileRtfile, String> otherInfos;
	public static volatile SingularAttribute<VFileRtfile, Long> status;
	public static volatile SingularAttribute<VFileRtfile, String> signPlace;
	public static volatile SingularAttribute<VFileRtfile, String> circulatingExtensionNo;
	public static volatile SingularAttribute<VFileRtfile, Long> createDeptId;
	public static volatile SingularAttribute<VFileRtfile, Long> extensionNo;
	public static volatile SingularAttribute<VFileRtfile, Long> creatorId;
	public static volatile SingularAttribute<VFileRtfile, String> creatorName;
	public static volatile SingularAttribute<VFileRtfile, String> description;
	public static volatile SingularAttribute<VFileRtfile, String> rapidTestName;
	public static volatile SingularAttribute<VFileRtfile, Long> propertiesTests;
	public static volatile SingularAttribute<VFileRtfile, Date> dateEffect;
	public static volatile SingularAttribute<VFileRtfile, String> shelfLife;
	public static volatile SingularAttribute<VFileRtfile, Date> createDate;
	public static volatile SingularAttribute<VFileRtfile, String> createDeptName;
	public static volatile SingularAttribute<VFileRtfile, String> signName;
	public static volatile SingularAttribute<VFileRtfile, String> storageConditions;
	public static volatile SingularAttribute<VFileRtfile, String> nswFileCode;
	public static volatile SingularAttribute<VFileRtfile, String> signedData;
	public static volatile SingularAttribute<VFileRtfile, Date> finishDate;
	public static volatile SingularAttribute<VFileRtfile, Long> fileType;
	public static volatile SingularAttribute<VFileRtfile, String> pakaging;
	public static volatile SingularAttribute<VFileRtfile, String> withdraw;

}

