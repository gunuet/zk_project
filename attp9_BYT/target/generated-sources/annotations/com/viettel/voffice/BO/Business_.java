package com.viettel.voffice.BO;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Business.class)
public abstract class Business_ {

	public static volatile SingularAttribute<Business, String> businessLicense;
	public static volatile SingularAttribute<Business, String> businessEstablishYear;
	public static volatile SingularAttribute<Business, String> userTelephone;
	public static volatile SingularAttribute<Business, Long> businessId;
	public static volatile SingularAttribute<Business, String> businessName;
	public static volatile SingularAttribute<Business, String> description;
	public static volatile SingularAttribute<Business, Long> isActive;
	public static volatile SingularAttribute<Business, String> businessNameAlias;
	public static volatile SingularAttribute<Business, String> businessWebsite;
	public static volatile SingularAttribute<Business, String> businessTelephone;
	public static volatile SingularAttribute<Business, String> businessTown;
	public static volatile SingularAttribute<Business, String> businessTypeName;
	public static volatile SingularAttribute<Business, String> userMobile;
	public static volatile SingularAttribute<Business, String> businessProvince;
	public static volatile SingularAttribute<Business, String> userFullname;
	public static volatile SingularAttribute<Business, Long> isCa;
	public static volatile SingularAttribute<Business, String> userEmail;
	public static volatile SingularAttribute<Business, String> businessTaxCode;
	public static volatile SingularAttribute<Business, String> businessAddress;
	public static volatile SingularAttribute<Business, String> manageEmail;
	public static volatile SingularAttribute<Business, String> businessNameEng;
	public static volatile SingularAttribute<Business, Long> businessProvinceId;
	public static volatile SingularAttribute<Business, String> userName;
	public static volatile SingularAttribute<Business, Long> businessTypeId;
	public static volatile SingularAttribute<Business, String> businessDistrict;
	public static volatile SingularAttribute<Business, String> businessFax;
	public static volatile SingularAttribute<Business, String> governingBody;
	public static volatile SingularAttribute<Business, String> businessLawRep;

}

