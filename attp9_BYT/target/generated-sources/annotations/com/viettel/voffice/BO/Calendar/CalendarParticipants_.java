package com.viettel.voffice.BO.Calendar;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalendarParticipants.class)
public abstract class CalendarParticipants_ {

	public static volatile SingularAttribute<CalendarParticipants, Long> participantId;
	public static volatile SingularAttribute<CalendarParticipants, String> deptName;
	public static volatile SingularAttribute<CalendarParticipants, String> acceptNote;
	public static volatile SingularAttribute<CalendarParticipants, Long> calendarId;
	public static volatile SingularAttribute<CalendarParticipants, Long> deptId;
	public static volatile SingularAttribute<CalendarParticipants, Long> acceptStatus;
	public static volatile SingularAttribute<CalendarParticipants, String> userName;
	public static volatile SingularAttribute<CalendarParticipants, Long> userId;
	public static volatile SingularAttribute<CalendarParticipants, Long> participantRole;

}

