package com.viettel.voffice.BO.Calendar;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalendarRef.class)
public abstract class CalendarRef_ {

	public static volatile SingularAttribute<CalendarRef, Long> calendarId;
	public static volatile SingularAttribute<CalendarRef, Long> calendarRefId;
	public static volatile SingularAttribute<CalendarRef, String> refNote;
	public static volatile SingularAttribute<CalendarRef, String> refLink;
	public static volatile SingularAttribute<CalendarRef, Long> attachId;
	public static volatile SingularAttribute<CalendarRef, String> refTitle;

}

