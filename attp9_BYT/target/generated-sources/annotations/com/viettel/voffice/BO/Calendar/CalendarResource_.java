package com.viettel.voffice.BO.Calendar;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CalendarResource.class)
public abstract class CalendarResource_ {

	public static volatile SingularAttribute<CalendarResource, Date> acceptDate;
	public static volatile SingularAttribute<CalendarResource, String> acceptNote;
	public static volatile SingularAttribute<CalendarResource, String> deptName;
	public static volatile SingularAttribute<CalendarResource, Long> resourceId;
	public static volatile SingularAttribute<CalendarResource, Long> calendarId;
	public static volatile SingularAttribute<CalendarResource, String> acceptUserName;
	public static volatile SingularAttribute<CalendarResource, Long> calendarResourceId;
	public static volatile SingularAttribute<CalendarResource, Long> deptId;
	public static volatile SingularAttribute<CalendarResource, String> resourceName;
	public static volatile SingularAttribute<CalendarResource, Long> acceptStatus;
	public static volatile SingularAttribute<CalendarResource, Long> acceptUser;
	public static volatile SingularAttribute<CalendarResource, Long> resourceType;

}

