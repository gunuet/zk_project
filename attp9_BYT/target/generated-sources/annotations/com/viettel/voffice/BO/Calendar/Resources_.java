package com.viettel.voffice.BO.Calendar;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Resources.class)
public abstract class Resources_ {

	public static volatile SingularAttribute<Resources, String> deptName;
	public static volatile SingularAttribute<Resources, Long> resourceId;
	public static volatile SingularAttribute<Resources, String> resourceCode;
	public static volatile SingularAttribute<Resources, Long> deptId;
	public static volatile SingularAttribute<Resources, String> resourceName;
	public static volatile SingularAttribute<Resources, String> resourceInfo;
	public static volatile SingularAttribute<Resources, String> resourceTypeName;
	public static volatile SingularAttribute<Resources, Long> resourceType;
	public static volatile SingularAttribute<Resources, Long> status;

}

