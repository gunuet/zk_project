package com.viettel.voffice.BO.Document;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Attachs.class)
public abstract class Attachs_ {

	public static volatile SingularAttribute<Attachs, String> attachPath;
	public static volatile SingularAttribute<Attachs, String> attachTypeName;
	public static volatile SingularAttribute<Attachs, Long> attachCat;
	public static volatile SingularAttribute<Attachs, Date> dateModify;
	public static volatile SingularAttribute<Attachs, Long> creatorId;
	public static volatile SingularAttribute<Attachs, Long> modifierId;
	public static volatile SingularAttribute<Attachs, Long> isSent;
	public static volatile SingularAttribute<Attachs, Long> isActive;
	public static volatile SingularAttribute<Attachs, Date> dateCreate;
	public static volatile SingularAttribute<Attachs, Long> version;
	public static volatile SingularAttribute<Attachs, Long> attachType;
	public static volatile SingularAttribute<Attachs, String> attachCode;
	public static volatile SingularAttribute<Attachs, String> attachUrl;
	public static volatile SingularAttribute<Attachs, String> attachName;
	public static volatile SingularAttribute<Attachs, Long> attachId;
	public static volatile SingularAttribute<Attachs, Long> objectId;
	public static volatile SingularAttribute<Attachs, String> attachDes;

}

