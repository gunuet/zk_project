package com.viettel.voffice.BO.Document;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BookDocument.class)
public abstract class BookDocument_ {

	public static volatile SingularAttribute<BookDocument, Long> bookNumber;
	public static volatile SingularAttribute<BookDocument, Long> creatorId;
	public static volatile SingularAttribute<BookDocument, String> creatorName;
	public static volatile SingularAttribute<BookDocument, Long> documentId;
	public static volatile SingularAttribute<BookDocument, Long> bookDocumentId;
	public static volatile SingularAttribute<BookDocument, Long> status;
	public static volatile SingularAttribute<BookDocument, Long> bookId;
	public static volatile SingularAttribute<BookDocument, Date> createDate;

}

