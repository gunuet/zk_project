package com.viettel.voffice.BO.Document;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Books.class)
public abstract class Books_ {

	public static volatile SingularAttribute<Books, String> deptName;
	public static volatile SingularAttribute<Books, Long> isDefault;
	public static volatile SingularAttribute<Books, String> prefix;
	public static volatile SingularAttribute<Books, Long> bookObjectTypeId;
	public static volatile SingularAttribute<Books, Long> bookOrder;
	public static volatile SingularAttribute<Books, Long> deptId;
	public static volatile SingularAttribute<Books, Long> currentNumber;
	public static volatile SingularAttribute<Books, Long> prefixBy;
	public static volatile SingularAttribute<Books, String> bookName;
	public static volatile SingularAttribute<Books, Long> bookId;
	public static volatile SingularAttribute<Books, Long> status;

}

