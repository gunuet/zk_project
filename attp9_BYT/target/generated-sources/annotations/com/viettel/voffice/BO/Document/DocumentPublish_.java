package com.viettel.voffice.BO.Document;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DocumentPublish.class)
public abstract class DocumentPublish_ {

	public static volatile SingularAttribute<DocumentPublish, String> documentCode;
	public static volatile SingularAttribute<DocumentPublish, String> deptOutIdsReceive;
	public static volatile SingularAttribute<DocumentPublish, Long> createDeptId;
	public static volatile SingularAttribute<DocumentPublish, Long> documentTypeId;
	public static volatile SingularAttribute<DocumentPublish, String> documentAreaName;
	public static volatile SingularAttribute<DocumentPublish, Long> creatorId;
	public static volatile SingularAttribute<DocumentPublish, String> creatorName;
	public static volatile SingularAttribute<DocumentPublish, String> deptOutNameReceive;
	public static volatile SingularAttribute<DocumentPublish, Long> numberOfDoc;
	public static volatile SingularAttribute<DocumentPublish, String> documentAbstract;
	public static volatile SingularAttribute<DocumentPublish, String> securityTypeName;
	public static volatile SingularAttribute<DocumentPublish, Date> dateCreate;
	public static volatile SingularAttribute<DocumentPublish, Short> isActive;
	public static volatile SingularAttribute<DocumentPublish, String> signerName;
	public static volatile SingularAttribute<DocumentPublish, String> emergencyTypeName;
	public static volatile SingularAttribute<DocumentPublish, Boolean> isLawDocument;
	public static volatile SingularAttribute<DocumentPublish, Long> documentPublishId;
	public static volatile SingularAttribute<DocumentPublish, String> documentTypeName;
	public static volatile SingularAttribute<DocumentPublish, String> deptInIdsReceive;
	public static volatile SingularAttribute<DocumentPublish, Boolean> isDocAnswer;
	public static volatile SingularAttribute<DocumentPublish, String> deptInNameReceive;
	public static volatile SingularAttribute<DocumentPublish, String> createDeptName;
	public static volatile SingularAttribute<DocumentPublish, Long> documentReceiveId;
	public static volatile SingularAttribute<DocumentPublish, String> docRelationIds;
	public static volatile SingularAttribute<DocumentPublish, Long> documentAreaId;
	public static volatile SingularAttribute<DocumentPublish, Long> emergencyTypeId;
	public static volatile SingularAttribute<DocumentPublish, String> documentContent;
	public static volatile SingularAttribute<DocumentPublish, String> version;
	public static volatile SingularAttribute<DocumentPublish, Long> numberOfPage;
	public static volatile SingularAttribute<DocumentPublish, Long> bookId;
	public static volatile SingularAttribute<DocumentPublish, Date> datePublish;
	public static volatile SingularAttribute<DocumentPublish, Long> signerId;
	public static volatile SingularAttribute<DocumentPublish, Long> securityTypeId;
	public static volatile SingularAttribute<DocumentPublish, Long> previousVersion;
	public static volatile SingularAttribute<DocumentPublish, Long> status;
	public static volatile SingularAttribute<DocumentPublish, Long> fileId;

}

