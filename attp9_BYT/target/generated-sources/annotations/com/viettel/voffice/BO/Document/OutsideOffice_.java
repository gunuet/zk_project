package com.viettel.voffice.BO.Document;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OutsideOffice.class)
public abstract class OutsideOffice_ {

	public static volatile SingularAttribute<OutsideOffice, String> officeName;
	public static volatile SingularAttribute<OutsideOffice, String> leader;
	public static volatile SingularAttribute<OutsideOffice, String> deptName;
	public static volatile SingularAttribute<OutsideOffice, String> address;
	public static volatile SingularAttribute<OutsideOffice, Long> officeId;
	public static volatile SingularAttribute<OutsideOffice, String> officeCode;
	public static volatile SingularAttribute<OutsideOffice, String> mobile;
	public static volatile SingularAttribute<OutsideOffice, Long> deptId;
	public static volatile SingularAttribute<OutsideOffice, String> fax;
	public static volatile SingularAttribute<OutsideOffice, String> servicesUrl;
	public static volatile SingularAttribute<OutsideOffice, String> email;
	public static volatile SingularAttribute<OutsideOffice, Long> status;

}

