package com.viettel.voffice.BO.Document;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SendSheet.class)
public abstract class SendSheet_ {

	public static volatile SingularAttribute<SendSheet, String> summary;
	public static volatile SingularAttribute<SendSheet, String> creatorDeptName;
	public static volatile SingularAttribute<SendSheet, String> creatorUserName;
	public static volatile SingularAttribute<SendSheet, Date> sendDate;
	public static volatile SingularAttribute<SendSheet, String> suggest;
	public static volatile SingularAttribute<SendSheet, Long> managerId;
	public static volatile SingularAttribute<SendSheet, String> chiefName;
	public static volatile SingularAttribute<SendSheet, String> title;
	public static volatile SingularAttribute<SendSheet, String> managerName;
	public static volatile SingularAttribute<SendSheet, Long> chiefId;
	public static volatile SingularAttribute<SendSheet, String> approveContent;
	public static volatile SingularAttribute<SendSheet, Long> documentPublishId;
	public static volatile SingularAttribute<SendSheet, Long> sendSheetId;
	public static volatile SingularAttribute<SendSheet, Long> fileId;

}

