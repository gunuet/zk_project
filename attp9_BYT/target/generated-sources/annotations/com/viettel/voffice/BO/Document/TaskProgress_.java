package com.viettel.voffice.BO.Document;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TaskProgress.class)
public abstract class TaskProgress_ {

	public static volatile SingularAttribute<TaskProgress, String> userCreateName;
	public static volatile SingularAttribute<TaskProgress, String> userGroupCreateName;
	public static volatile SingularAttribute<TaskProgress, Date> updateDate;
	public static volatile SingularAttribute<TaskProgress, Long> progressId;
	public static volatile SingularAttribute<TaskProgress, String> commentTask;
	public static volatile SingularAttribute<TaskProgress, Long> isActive;
	public static volatile SingularAttribute<TaskProgress, String> nextStage;
	public static volatile SingularAttribute<TaskProgress, Long> userCreateId;
	public static volatile SingularAttribute<TaskProgress, Long> progressPercent;
	public static volatile SingularAttribute<TaskProgress, Long> userGroupCreateId;
	public static volatile SingularAttribute<TaskProgress, Long> taskId;
	public static volatile SingularAttribute<TaskProgress, Date> createDate;
	public static volatile SingularAttribute<TaskProgress, Long> status;
	public static volatile SingularAttribute<TaskProgress, Long> nextStageId;

}

