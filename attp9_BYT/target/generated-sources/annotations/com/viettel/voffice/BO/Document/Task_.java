package com.viettel.voffice.BO.Document;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Task.class)
public abstract class Task_ {

	public static volatile SingularAttribute<Task, String> userCreateName;
	public static volatile SingularAttribute<Task, Date> updateDate;
	public static volatile SingularAttribute<Task, String> statusStr;
	public static volatile SingularAttribute<Task, String> taskPriorityName;
	public static volatile SingularAttribute<Task, String> description;
	public static volatile SingularAttribute<Task, String> taskTypeName;
	public static volatile SingularAttribute<Task, Long> isActive;
	public static volatile SingularAttribute<Task, Date> completeDate;
	public static volatile SingularAttribute<Task, Long> userGroupId;
	public static volatile SingularAttribute<Task, String> path;
	public static volatile SingularAttribute<Task, Long> taskType;
	public static volatile SingularAttribute<Task, Long> userCreateId;
	public static volatile SingularAttribute<Task, Long> progressPercent;
	public static volatile SingularAttribute<Task, Date> startTime;
	public static volatile SingularAttribute<Task, String> userPerformName;
	public static volatile SingularAttribute<Task, Date> deadline;
	public static volatile SingularAttribute<Task, Date> createDate;
	public static volatile SingularAttribute<Task, String> userHeptName;
	public static volatile SingularAttribute<Task, Long> userPerformId;
	public static volatile SingularAttribute<Task, Long> taskParentId;
	public static volatile SingularAttribute<Task, Long> taskPriorityId;
	public static volatile SingularAttribute<Task, String> usersHelpId;
	public static volatile SingularAttribute<Task, String> userGroupName;
	public static volatile SingularAttribute<Task, String> taskParentName;
	public static volatile SingularAttribute<Task, String> taskName;
	public static volatile SingularAttribute<Task, Long> taskId;
	public static volatile SingularAttribute<Task, Long> status;

}

