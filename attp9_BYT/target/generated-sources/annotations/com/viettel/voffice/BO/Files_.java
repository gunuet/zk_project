package com.viettel.voffice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Files.class)
public abstract class Files_ {

	public static volatile SingularAttribute<Files, Long> createDeptId;
	public static volatile SingularAttribute<Files, String> fileName;
	public static volatile SingularAttribute<Files, Long> flagView;
	public static volatile SingularAttribute<Files, String> reasons;
	public static volatile SingularAttribute<Files, String> notes;
	public static volatile SingularAttribute<Files, Long> businessId;
	public static volatile SingularAttribute<Files, String> businessName;
	public static volatile SingularAttribute<Files, Long> creatorId;
	public static volatile SingularAttribute<Files, String> creatorName;
	public static volatile SingularAttribute<Files, Long> isActive;
	public static volatile SingularAttribute<Files, Long> parentFileId;
	public static volatile SingularAttribute<Files, byte[]> qrCode;
	public static volatile SingularAttribute<Files, Long> numDayProcess;
	public static volatile SingularAttribute<Files, String> fileTypeName;
	public static volatile SingularAttribute<Files, String> businessAddress;
	public static volatile SingularAttribute<Files, Date> deadline;
	public static volatile SingularAttribute<Files, Long> flowId;
	public static volatile SingularAttribute<Files, Date> createDate;
	public static volatile SingularAttribute<Files, String> businessEmail;
	public static volatile SingularAttribute<Files, String> createDeptName;
	public static volatile SingularAttribute<Files, Date> modifyDate;
	public static volatile SingularAttribute<Files, String> fileCode;
	public static volatile SingularAttribute<Files, String> taxCode;
	public static volatile SingularAttribute<Files, Long> version;
	public static volatile SingularAttribute<Files, Long> haveQrCode;
	public static volatile SingularAttribute<Files, String> nswFileCode;
	public static volatile SingularAttribute<Files, Long> isTemp;
	public static volatile SingularAttribute<Files, Long> nextUser;
	public static volatile SingularAttribute<Files, String> businessFax;
	public static volatile SingularAttribute<Files, Date> finishDate;
	public static volatile SingularAttribute<Files, String> businessPhone;
	public static volatile SingularAttribute<Files, Long> fileTypeHS;
	public static volatile SingularAttribute<Files, Long> fileType;
	public static volatile SingularAttribute<Files, Date> startDate;
	public static volatile SingularAttribute<Files, Long> isChange;
	public static volatile SingularAttribute<Files, Long> fileId;
	public static volatile SingularAttribute<Files, Long> status;

}

