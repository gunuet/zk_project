package com.viettel.voffice.BO.Home;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NotifyAction.class)
public abstract class NotifyAction_ {

	public static volatile SingularAttribute<NotifyAction, String> actionUrl;
	public static volatile SingularAttribute<NotifyAction, Long> notifyId;
	public static volatile SingularAttribute<NotifyAction, Long> notifyActionId;
	public static volatile SingularAttribute<NotifyAction, String> actionName;

}

