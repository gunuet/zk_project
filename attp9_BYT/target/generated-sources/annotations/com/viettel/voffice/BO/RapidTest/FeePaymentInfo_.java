package com.viettel.voffice.BO.RapidTest;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FeePaymentInfo.class)
public abstract class FeePaymentInfo_ {

	public static volatile SingularAttribute<FeePaymentInfo, Long> feePaymentTypeId;
	public static volatile SingularAttribute<FeePaymentInfo, Date> updateDate;
	public static volatile SingularAttribute<FeePaymentInfo, String> paymentConfirm;
	public static volatile SingularAttribute<FeePaymentInfo, Long> cost;
	public static volatile SingularAttribute<FeePaymentInfo, Long> procedureCode;
	public static volatile SingularAttribute<FeePaymentInfo, String> paymentPerson;
	public static volatile SingularAttribute<FeePaymentInfo, String> billCode;
	public static volatile SingularAttribute<FeePaymentInfo, Long> isActive;
	public static volatile SingularAttribute<FeePaymentInfo, Long> feeId;
	public static volatile SingularAttribute<FeePaymentInfo, String> commentFee;
	public static volatile SingularAttribute<FeePaymentInfo, String> paymentActionUser;
	public static volatile SingularAttribute<FeePaymentInfo, String> paymentCode;
	public static volatile SingularAttribute<FeePaymentInfo, String> feeName;
	public static volatile SingularAttribute<FeePaymentInfo, Long> feePaymentInfoId;
	public static volatile SingularAttribute<FeePaymentInfo, Long> paymentActionUserId;
	public static volatile SingularAttribute<FeePaymentInfo, Long> attachBillId;
	public static volatile SingularAttribute<FeePaymentInfo, Date> paymentDate;
	public static volatile SingularAttribute<FeePaymentInfo, String> paymentInfo;
	public static volatile SingularAttribute<FeePaymentInfo, Date> dateConfirm;
	public static volatile SingularAttribute<FeePaymentInfo, Long> status;
	public static volatile SingularAttribute<FeePaymentInfo, Date> createDate;
	public static volatile SingularAttribute<FeePaymentInfo, Long> fileId;

}

