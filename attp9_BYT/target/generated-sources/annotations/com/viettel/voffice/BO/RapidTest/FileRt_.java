package com.viettel.voffice.BO.RapidTest;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FileRt.class)
public abstract class FileRt_ {

	public static volatile SingularAttribute<FileRt, String> signPlace;
	public static volatile SingularAttribute<FileRt, String> circulatingExtensionNo;
	public static volatile SingularAttribute<FileRt, String> rangeOfApplications;
	public static volatile SingularAttribute<FileRt, String> precision;
	public static volatile SingularAttribute<FileRt, Long> extensionNo;
	public static volatile SingularAttribute<FileRt, String> businessName;
	public static volatile SingularAttribute<FileRt, String> description;
	public static volatile SingularAttribute<FileRt, Date> signDate;
	public static volatile SingularAttribute<FileRt, Long> isActive;
	public static volatile SingularAttribute<FileRt, String> rapidTestNo;
	public static volatile SingularAttribute<FileRt, String> placeOfManufacture;
	public static volatile SingularAttribute<FileRt, String> rapidTestChangeNo;
	public static volatile SingularAttribute<FileRt, String> rapidTestName;
	public static volatile SingularAttribute<FileRt, String> attachmentsInfo;
	public static volatile SingularAttribute<FileRt, Long> propertiesTests;
	public static volatile SingularAttribute<FileRt, Date> dateEffect;
	public static volatile SingularAttribute<FileRt, String> businessAddress;
	public static volatile SingularAttribute<FileRt, Date> shelfLife;
	public static volatile SingularAttribute<FileRt, Date> createDate;
	public static volatile SingularAttribute<FileRt, String> operatingPrinciples;
	public static volatile SingularAttribute<FileRt, String> signName;
	public static volatile SingularAttribute<FileRt, Date> modifyDate;
	public static volatile SingularAttribute<FileRt, String> taxCode;
	public static volatile SingularAttribute<FileRt, String> storageConditions;
	public static volatile SingularAttribute<FileRt, String> rapidTestCode;
	public static volatile SingularAttribute<FileRt, Long> documentTypeCode;
	public static volatile SingularAttribute<FileRt, String> circulatingRapidTestNo;
	public static volatile SingularAttribute<FileRt, String> contents;
	public static volatile SingularAttribute<FileRt, String> targetTesting;
	public static volatile SingularAttribute<FileRt, String> nswFileCode;
	public static volatile SingularAttribute<FileRt, String> signedData;
	public static volatile SingularAttribute<FileRt, String> limitDevelopment;
	public static volatile SingularAttribute<FileRt, String> businessFax;
	public static volatile SingularAttribute<FileRt, String> businessPhone;
	public static volatile SingularAttribute<FileRt, Long> fileRtId;
	public static volatile SingularAttribute<FileRt, Long> statusCode;
	public static volatile SingularAttribute<FileRt, Date> dateIssue;
	public static volatile SingularAttribute<FileRt, String> pakaging;
	public static volatile SingularAttribute<FileRt, String> otherInfos;

}

