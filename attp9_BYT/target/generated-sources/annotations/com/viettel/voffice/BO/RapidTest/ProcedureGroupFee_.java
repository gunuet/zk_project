package com.viettel.voffice.BO.RapidTest;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProcedureGroupFee.class)
public abstract class ProcedureGroupFee_ {

	public static volatile SingularAttribute<ProcedureGroupFee, Long> groupId;
	public static volatile SingularAttribute<ProcedureGroupFee, Long> procedureGroupFeeId;
	public static volatile SingularAttribute<ProcedureGroupFee, Long> isActive;
	public static volatile SingularAttribute<ProcedureGroupFee, Long> procedureId;
	public static volatile SingularAttribute<ProcedureGroupFee, Long> feeId;

}

