package com.viettel.voffice.BO.RapidTest;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtGroup.class)
public abstract class RtGroup_ {

	public static volatile SingularAttribute<RtGroup, Long> groupType;
	public static volatile SingularAttribute<RtGroup, String> code;
	public static volatile SingularAttribute<RtGroup, Long> groupId;
	public static volatile SingularAttribute<RtGroup, String> name;
	public static volatile SingularAttribute<RtGroup, Long> isActive;

}

