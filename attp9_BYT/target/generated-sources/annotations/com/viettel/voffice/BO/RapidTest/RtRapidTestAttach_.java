package com.viettel.voffice.BO.RapidTest;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RtRapidTestAttach.class)
public abstract class RtRapidTestAttach_ {

	public static volatile SingularAttribute<RtRapidTestAttach, Long> rapidTestAttachId;
	public static volatile SingularAttribute<RtRapidTestAttach, String> attachTypeCode;
	public static volatile SingularAttribute<RtRapidTestAttach, Long> rapidTestId;
	public static volatile SingularAttribute<RtRapidTestAttach, Short> isActive;

}

