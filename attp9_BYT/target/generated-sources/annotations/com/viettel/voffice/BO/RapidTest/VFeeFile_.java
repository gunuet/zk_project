package com.viettel.voffice.BO.RapidTest;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFeeFile.class)
public abstract class VFeeFile_ {

	public static volatile SingularAttribute<VFeeFile, String> deptName;
	public static volatile SingularAttribute<VFeeFile, String> code;
	public static volatile SingularAttribute<VFeeFile, Long> updatedBy;
	public static volatile SingularAttribute<VFeeFile, Long> deptId;
	public static volatile SingularAttribute<VFeeFile, String> description;
	public static volatile SingularAttribute<VFeeFile, String> fileCode;
	public static volatile SingularAttribute<VFeeFile, String> categoryTypeCode;
	public static volatile SingularAttribute<VFeeFile, Date> updatedDate;
	public static volatile SingularAttribute<VFeeFile, Long> isActive;
	public static volatile SingularAttribute<VFeeFile, Date> expiryDate;
	public static volatile SingularAttribute<VFeeFile, Long> isDefault;
	public static volatile SingularAttribute<VFeeFile, Date> createdDate;
	public static volatile SingularAttribute<VFeeFile, Long> createdBy;
	public static volatile SingularAttribute<VFeeFile, Long> sortOrder;
	public static volatile SingularAttribute<VFeeFile, String> name;
	public static volatile SingularAttribute<VFeeFile, String> value;
	public static volatile SingularAttribute<VFeeFile, Long> categoryId;

}

