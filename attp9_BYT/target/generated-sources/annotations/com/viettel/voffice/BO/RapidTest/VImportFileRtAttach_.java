package com.viettel.voffice.BO.RapidTest;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VImportFileRtAttach.class)
public abstract class VImportFileRtAttach_ {

	public static volatile SingularAttribute<VImportFileRtAttach, String> attachPath;
	public static volatile SingularAttribute<VImportFileRtAttach, Long> attachCat;
	public static volatile SingularAttribute<VImportFileRtAttach, Date> dateModify;
	public static volatile SingularAttribute<VImportFileRtAttach, Long> creatorId;
	public static volatile SingularAttribute<VImportFileRtAttach, Long> modifierId;
	public static volatile SingularAttribute<VImportFileRtAttach, Long> isActive;
	public static volatile SingularAttribute<VImportFileRtAttach, Date> dateCreate;
	public static volatile SingularAttribute<VImportFileRtAttach, Integer> version;
	public static volatile SingularAttribute<VImportFileRtAttach, Long> attachType;
	public static volatile SingularAttribute<VImportFileRtAttach, String> attachCode;
	public static volatile SingularAttribute<VImportFileRtAttach, String> attachUrl;
	public static volatile SingularAttribute<VImportFileRtAttach, String> attachName;
	public static volatile SingularAttribute<VImportFileRtAttach, String> typeFileName;
	public static volatile SingularAttribute<VImportFileRtAttach, Long> attachId;
	public static volatile SingularAttribute<VImportFileRtAttach, Long> objectId;

}

