package com.viettel.voffice.BO.RapidTest;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VRapidTestPaymentInfo.class)
public abstract class VRapidTestPaymentInfo_ {

	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> paymentConfirm;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> rangeOfApplications;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> precision;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> businessName;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Date> signDate;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> isActive;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> rapidTestNo;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> feeId;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> placeOfManufacture;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> rapidTestChangeNo;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> attachmentsInfo;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> feePaymentInfoId;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> businessAddress;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> operatingPrinciples;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> feePaymentTypeId;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Date> modifyDate;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> paymentPerson;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> taxCode;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> commentFee;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> rapidTestCode;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> documentTypeCode;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> circulatingRapidTestNo;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> paymentCode;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> contents;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> targetTesting;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> limitDevelopment;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> businessFax;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> businessPhone;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> fileRtId;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> statusCode;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Date> dateIssue;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> otherInfos;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> signPlace;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> circulatingExtensionNo;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> extensionNo;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> description;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> rapidTestName;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> propertiesTests;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Date> dateEffect;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Date> shelfLife;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Date> dateConfirm;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Date> createDate;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> signName;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Long> cost;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> billCode;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> storageConditions;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> nswFileCode;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> signedData;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> feeName;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, Date> paymentDate;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> paymentInfo;
	public static volatile SingularAttribute<VRapidTestPaymentInfo, String> pakaging;

}

