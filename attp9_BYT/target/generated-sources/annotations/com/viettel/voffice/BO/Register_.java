package com.viettel.voffice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Register.class)
public abstract class Register_ {

	public static volatile SingularAttribute<Register, String> businessLicense;
	public static volatile SingularAttribute<Register, String> reason;
	public static volatile SingularAttribute<Register, String> businessEstablishYear;
	public static volatile SingularAttribute<Register, String> businessAdd;
	public static volatile SingularAttribute<Register, String> userTelephone;
	public static volatile SingularAttribute<Register, String> description;
	public static volatile SingularAttribute<Register, Date> dateCreate;
	public static volatile SingularAttribute<Register, String> businessNameAlias;
	public static volatile SingularAttribute<Register, String> businessWebsite;
	public static volatile SingularAttribute<Register, Long> posId;
	public static volatile SingularAttribute<Register, String> businessTelephone;
	public static volatile SingularAttribute<Register, String> businessTown;
	public static volatile SingularAttribute<Register, Long> registerId;
	public static volatile SingularAttribute<Register, String> businessTypeName;
	public static volatile SingularAttribute<Register, String> userMobile;
	public static volatile SingularAttribute<Register, String> businessNameVi;
	public static volatile SingularAttribute<Register, String> businessProvince;
	public static volatile SingularAttribute<Register, String> userEmail;
	public static volatile SingularAttribute<Register, String> businessTaxCode;
	public static volatile SingularAttribute<Register, String> manageEmail;
	public static volatile SingularAttribute<Register, String> businessNameEng;
	public static volatile SingularAttribute<Register, String> managePassword;
	public static volatile SingularAttribute<Register, String> posName;
	public static volatile SingularAttribute<Register, String> userFullName;
	public static volatile SingularAttribute<Register, Long> businessProvinceId;
	public static volatile SingularAttribute<Register, Long> businessTypeId;
	public static volatile SingularAttribute<Register, String> businessDistrict;
	public static volatile SingularAttribute<Register, String> governingBody;
	public static volatile SingularAttribute<Register, String> businessFax;
	public static volatile SingularAttribute<Register, String> userType;
	public static volatile SingularAttribute<Register, String> businessLawRep;
	public static volatile SingularAttribute<Register, Long> status;

}

