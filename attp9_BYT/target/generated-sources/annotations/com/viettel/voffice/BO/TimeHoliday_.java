package com.viettel.voffice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TimeHoliday.class)
public abstract class TimeHoliday_ {

	public static volatile SingularAttribute<TimeHoliday, Long> timeHolidayId;
	public static volatile SingularAttribute<TimeHoliday, Long> dateType;
	public static volatile SingularAttribute<TimeHoliday, String> name;
	public static volatile SingularAttribute<TimeHoliday, String> description;
	public static volatile SingularAttribute<TimeHoliday, Long> isActive;
	public static volatile SingularAttribute<TimeHoliday, Date> timeDate;

}

