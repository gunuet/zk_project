package com.viettel.voffice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFeeProcedure.class)
public abstract class VFeeProcedure_ {

	public static volatile SingularAttribute<VFeeProcedure, Long> phase;
	public static volatile SingularAttribute<VFeeProcedure, String> deptName;
	public static volatile SingularAttribute<VFeeProcedure, String> code;
	public static volatile SingularAttribute<VFeeProcedure, String> cost;
	public static volatile SingularAttribute<VFeeProcedure, Long> updatedBy;
	public static volatile SingularAttribute<VFeeProcedure, Long> deptId;
	public static volatile SingularAttribute<VFeeProcedure, String> description;
	public static volatile SingularAttribute<VFeeProcedure, String> categoryTypeCode;
	public static volatile SingularAttribute<VFeeProcedure, Date> updatedDate;
	public static volatile SingularAttribute<VFeeProcedure, Long> isActive;
	public static volatile SingularAttribute<VFeeProcedure, Long> feeId;
	public static volatile SingularAttribute<VFeeProcedure, Long> procedureId;
	public static volatile SingularAttribute<VFeeProcedure, Date> expiryDate;
	public static volatile SingularAttribute<VFeeProcedure, Long> isDefault;
	public static volatile SingularAttribute<VFeeProcedure, Date> createdDate;
	public static volatile SingularAttribute<VFeeProcedure, Long> createdBy;
	public static volatile SingularAttribute<VFeeProcedure, Long> sortOrder;
	public static volatile SingularAttribute<VFeeProcedure, String> name;
	public static volatile SingularAttribute<VFeeProcedure, Long> id;

}

