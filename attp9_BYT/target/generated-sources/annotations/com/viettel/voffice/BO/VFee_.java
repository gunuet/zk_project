package com.viettel.voffice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VFee.class)
public abstract class VFee_ {

	public static volatile SingularAttribute<VFee, String> deptName;
	public static volatile SingularAttribute<VFee, String> code;
	public static volatile SingularAttribute<VFee, String> cost;
	public static volatile SingularAttribute<VFee, Long> updatedBy;
	public static volatile SingularAttribute<VFee, Long> deptId;
	public static volatile SingularAttribute<VFee, String> description;
	public static volatile SingularAttribute<VFee, String> categoryTypeCode;
	public static volatile SingularAttribute<VFee, Date> updatedDate;
	public static volatile SingularAttribute<VFee, Long> isActive;
	public static volatile SingularAttribute<VFee, Long> feeId;
	public static volatile SingularAttribute<VFee, Date> expiryDate;
	public static volatile SingularAttribute<VFee, Long> isDefault;
	public static volatile SingularAttribute<VFee, Date> createdDate;
	public static volatile SingularAttribute<VFee, Long> createdBy;
	public static volatile SingularAttribute<VFee, Long> sortOrder;
	public static volatile SingularAttribute<VFee, String> name;

}

