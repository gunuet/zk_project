package com.viettel.voffice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VProcedure.class)
public abstract class VProcedure_ {

	public static volatile SingularAttribute<VProcedure, String> deptName;
	public static volatile SingularAttribute<VProcedure, String> code;
	public static volatile SingularAttribute<VProcedure, Long> updatedBy;
	public static volatile SingularAttribute<VProcedure, Long> deptId;
	public static volatile SingularAttribute<VProcedure, String> description;
	public static volatile SingularAttribute<VProcedure, String> categoryTypeCode;
	public static volatile SingularAttribute<VProcedure, Date> updatedDate;
	public static volatile SingularAttribute<VProcedure, Long> isActive;
	public static volatile SingularAttribute<VProcedure, Long> procedureId;
	public static volatile SingularAttribute<VProcedure, Date> expiryDate;
	public static volatile SingularAttribute<VProcedure, Long> isDefault;
	public static volatile SingularAttribute<VProcedure, Date> createdDate;
	public static volatile SingularAttribute<VProcedure, Long> createdBy;
	public static volatile SingularAttribute<VProcedure, Long> sortOrder;
	public static volatile SingularAttribute<VProcedure, String> name;
	public static volatile SingularAttribute<VProcedure, String> value;

}

