package com.viettel.voffice.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VSubProcedure.class)
public abstract class VSubProcedure_ {

	public static volatile SingularAttribute<VSubProcedure, String> deptName;
	public static volatile SingularAttribute<VSubProcedure, String> code;
	public static volatile SingularAttribute<VSubProcedure, Long> updatedBy;
	public static volatile SingularAttribute<VSubProcedure, Long> deptId;
	public static volatile SingularAttribute<VSubProcedure, String> description;
	public static volatile SingularAttribute<VSubProcedure, String> categoryTypeCode;
	public static volatile SingularAttribute<VSubProcedure, Date> updatedDate;
	public static volatile SingularAttribute<VSubProcedure, Long> isActive;
	public static volatile SingularAttribute<VSubProcedure, Date> expiryDate;
	public static volatile SingularAttribute<VSubProcedure, Long> isDefault;
	public static volatile SingularAttribute<VSubProcedure, Date> createdDate;
	public static volatile SingularAttribute<VSubProcedure, Long> createdBy;
	public static volatile SingularAttribute<VSubProcedure, Long> sortOrder;
	public static volatile SingularAttribute<VSubProcedure, String> name;
	public static volatile SingularAttribute<VSubProcedure, String> value;
	public static volatile SingularAttribute<VSubProcedure, Long> subProcedureId;

}

