package com.viettel.voffice.BO.Ycnk;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(YcnkFile.class)
public abstract class YcnkFile_ {

	public static volatile SingularAttribute<YcnkFile, Date> contractDate;
	public static volatile SingularAttribute<YcnkFile, String> importerPhone;
	public static volatile SingularAttribute<YcnkFile, Date> sendDate;
	public static volatile SingularAttribute<YcnkFile, String> exporterNationCode;
	public static volatile SingularAttribute<YcnkFile, String> contractNo;
	public static volatile SingularAttribute<YcnkFile, String> exporterAddress;
	public static volatile SingularAttribute<YcnkFile, String> isDelete1;
	public static volatile SingularAttribute<YcnkFile, String> exporterEmail;
	public static volatile SingularAttribute<YcnkFile, Long> creatorId;
	public static volatile SingularAttribute<YcnkFile, String> importerFax;
	public static volatile SingularAttribute<YcnkFile, String> exporterFax;
	public static volatile SingularAttribute<YcnkFile, String> assignBusinessTaxCode;
	public static volatile SingularAttribute<YcnkFile, String> registerEmail;
	public static volatile SingularAttribute<YcnkFile, String> importerEmail;
	public static volatile SingularAttribute<YcnkFile, String> importerGateCode;
	public static volatile SingularAttribute<YcnkFile, String> importerName;
	public static volatile SingularAttribute<YcnkFile, Date> transDate;
	public static volatile SingularAttribute<YcnkFile, String> exporterPhone;
	public static volatile SingularAttribute<YcnkFile, String> businessTaxCode;
	public static volatile SingularAttribute<YcnkFile, Long> flowId;
	public static volatile SingularAttribute<YcnkFile, String> modifyBy;
	public static volatile SingularAttribute<YcnkFile, String> registerFax;
	public static volatile SingularAttribute<YcnkFile, Date> modifyDate;
	public static volatile SingularAttribute<YcnkFile, String> exporterGateCode;
	public static volatile SingularAttribute<YcnkFile, String> exporterNationName;
	public static volatile SingularAttribute<YcnkFile, String> importerLeaderName;
	public static volatile SingularAttribute<YcnkFile, String> isTemp1;
	public static volatile SingularAttribute<YcnkFile, String> transNo;
	public static volatile SingularAttribute<YcnkFile, Long> version;
	public static volatile SingularAttribute<YcnkFile, String> registerNoiCap;
	public static volatile SingularAttribute<YcnkFile, Date> comingDate;
	public static volatile SingularAttribute<YcnkFile, String> fileTypeCode;
	public static volatile SingularAttribute<YcnkFile, Date> createdDate;
	public static volatile SingularAttribute<YcnkFile, String> exporterGateName;
	public static volatile SingularAttribute<YcnkFile, String> createdBy;
	public static volatile SingularAttribute<YcnkFile, Date> registerNgayCap;
	public static volatile SingularAttribute<YcnkFile, String> nswFileCode;
	public static volatile SingularAttribute<YcnkFile, String> importerGateName;
	public static volatile SingularAttribute<YcnkFile, String> exporterName;
	public static volatile SingularAttribute<YcnkFile, String> importerAddress;
	public static volatile SingularAttribute<YcnkFile, String> registerName;
	public static volatile SingularAttribute<YcnkFile, String> deptCode;
	public static volatile SingularAttribute<YcnkFile, Long> fileId;
	public static volatile SingularAttribute<YcnkFile, Long> statusCode;
	public static volatile SingularAttribute<YcnkFile, Long> registerCmt;

}

