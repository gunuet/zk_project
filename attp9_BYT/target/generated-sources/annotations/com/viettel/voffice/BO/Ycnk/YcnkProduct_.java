package com.viettel.voffice.BO.Ycnk;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(YcnkProduct.class)
public abstract class YcnkProduct_ {

	public static volatile SingularAttribute<YcnkProduct, String> productFacilities;
	public static volatile SingularAttribute<YcnkProduct, String> nationalCode;
	public static volatile SingularAttribute<YcnkProduct, String> purpose;
	public static volatile SingularAttribute<YcnkProduct, Date> expiredDate;
	public static volatile SingularAttribute<YcnkProduct, String> checkMethod;
	public static volatile SingularAttribute<YcnkProduct, Date> confirmAnnounceDate;
	public static volatile SingularAttribute<YcnkProduct, String> checkLicense;
	public static volatile SingularAttribute<YcnkProduct, String> productName;
	public static volatile SingularAttribute<YcnkProduct, String> isDeleted2;
	public static volatile SingularAttribute<YcnkProduct, String> productSienceName;
	public static volatile SingularAttribute<YcnkProduct, Long> baseUnit;
	public static volatile SingularAttribute<YcnkProduct, Long> total;
	public static volatile SingularAttribute<YcnkProduct, String> productDescriptions;
	public static volatile SingularAttribute<YcnkProduct, String> productAddress;
	public static volatile SingularAttribute<YcnkProduct, String> netweightUnitCode;
	public static volatile SingularAttribute<YcnkProduct, String> volumeUnitCode;
	public static volatile SingularAttribute<YcnkProduct, Long> productId;
	public static volatile SingularAttribute<YcnkProduct, String> pakageTypeCode;
	public static volatile SingularAttribute<YcnkProduct, String> package1;
	public static volatile SingularAttribute<YcnkProduct, String> transport;
	public static volatile SingularAttribute<YcnkProduct, Long> netweightPakage;
	public static volatile SingularAttribute<YcnkProduct, Long> version;
	public static volatile SingularAttribute<YcnkProduct, String> isTemp2;
	public static volatile SingularAttribute<YcnkProduct, Long> totalUnitCode;
	public static volatile SingularAttribute<YcnkProduct, String> volume;
	public static volatile SingularAttribute<YcnkProduct, String> productCode;
	public static volatile SingularAttribute<YcnkProduct, Date> createdDate;
	public static volatile SingularAttribute<YcnkProduct, Date> checkTime;
	public static volatile SingularAttribute<YcnkProduct, String> checkAddress;
	public static volatile SingularAttribute<YcnkProduct, Long> netweight;
	public static volatile SingularAttribute<YcnkProduct, String> storePlace;
	public static volatile SingularAttribute<YcnkProduct, String> appraisalIndepend;
	public static volatile SingularAttribute<YcnkProduct, String> currencyCode;
	public static volatile SingularAttribute<YcnkProduct, String> productTypeCode;
	public static volatile SingularAttribute<YcnkProduct, String> confirmAnnounceNo;
	public static volatile SingularAttribute<YcnkProduct, Long> fileId;

}

