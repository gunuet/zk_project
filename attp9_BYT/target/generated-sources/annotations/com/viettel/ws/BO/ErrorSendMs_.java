package com.viettel.ws.BO;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ErrorSendMs.class)
public abstract class ErrorSendMs_ {

	public static volatile SingularAttribute<ErrorSendMs, String> msId;
	public static volatile SingularAttribute<ErrorSendMs, String> msFunction;
	public static volatile SingularAttribute<ErrorSendMs, Date> createdDate;
	public static volatile SingularAttribute<ErrorSendMs, String> msCode;
	public static volatile SingularAttribute<ErrorSendMs, String> msPhase;
	public static volatile SingularAttribute<ErrorSendMs, Long> id;
	public static volatile SingularAttribute<ErrorSendMs, String> actions;
	public static volatile SingularAttribute<ErrorSendMs, Long> msUpdate;
	public static volatile SingularAttribute<ErrorSendMs, Long> fileId;

}

